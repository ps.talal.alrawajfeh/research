#!/usr/bin/env python3

import os
import sys

group_name = sys.argv[1]
project_name=sys.argv[2]
commit_ref_slug=sys.argv[3]

grp= group_name.replace('-', '')
prj=project_name.replace('-', '')
result = f"{grp[:8]}-{prj[:12]}-{commit_ref_slug}"[:40]
result = result[0:-1] if result[-1] == "-" else result

print(result)
