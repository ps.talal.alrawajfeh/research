#!/usr/bin/env python3

import sys
import json
import gitlab
delta = {
    "testname": "",
    "rps": "",
    "sucess": "",
    "duration": "",
    "drps": "",
    "dsucess": "",
    "dduration": "",
    "pass": True,
}

FailPercent=10.0

########################################################
def search(testname, dictlist):
    for result in dictlist:
        if result['testname'] == testname:
            return result
    return None


def format_md(original: float, delta: float, units: str, failed: bool):
    md_string = f'{original}{units} ({delta})'
    if failed:
        return f'**{md_string}**'
    else:
        return f'  {md_string}  '

##################################################################
# MAIN
##################################################################
gitlabProjectID = sys.argv[1]
gitlabToken = sys.argv[2]
gitlabBranch = sys.argv[3]
gitlabShortSHA = sys.argv[4]

if gitlabBranch == 'master':
    print('Master Branch: NO MERGER REQUETS')
    sys.exit(0)

branchFilename = f'k6-results-{gitlabBranch}.json'
masterFilename = f'k6-results-master.json'

gl = gitlab.Gitlab('https://gitlab.com', private_token=gitlabToken)
project = gl.projects.get(gitlabProjectID)

try:
    masterHome = project.wikis.get(masterFilename)
    masterResults = json.loads(masterHome.content)
except:
    masterResults = []

try:
    branchHome = project.wikis.get(branchFilename)
    branchResults = json.loads(branchHome.content)
except:
    branchResults = []

buffer=''                                                                                          
h1 = '| STATUS      | TESTCASE                                      | RPS                        | SUCCESS            | DURATION                   |   PASSED   |'
h2 = '| ----------- | --------------------------------------------- | -------------------------- | ------------------ | -------------------------- | ---------- |'
buffer +=h1+'\n'
buffer +=h2+'\n'

for branch in branchResults:
    master = search(branch['testname'], masterResults)
    if master:
        delta_rps = round((branch['rps']-master['rps'])/master['rps']*100,0) if master['rps'] != 0 else 100
        delta_success = round((branch['success']-master['success'])/master['success']*100,0) if master['success'] != 0 else 100
        delta_duration = round((branch['duration']-master['duration'])/master['duration']*100,0) if master['duration'] != 0 else 100
    else:
        delta_rps = delta_success = delta_duration = 0
        master = {'testname': '', 'rps': 0, 'success': 0, 'duration': 0, 'pass': False}

    md_rps = format_md(branch['rps'], master['rps'], '/s', delta_rps < -1*FailPercent)
    md_success = format_md(branch['success'], master['success'], '%', delta_success < -1*FailPercent)
    md_duration = format_md(branch['duration'], master['duration'], 'ms', delta_duration > FailPercent)
    md_pass = "  Passed  " if branch['pass'] else "**FAILED**"
    md_string = f"| {branch['testname']:45} | {md_rps:>26} | {md_success:>18} | {md_duration:>26} | {md_pass:10} | \n"
    if  delta_rps < -1*FailPercent or delta_success < -1*FailPercent or delta_duration > FailPercent:
        buffer += "| **WORSE**  " + md_string
    elif delta_rps > FailPercent or delta_success > FailPercent or delta_duration < -1*FailPercent:
        buffer += "|   BETTER   " + md_string

print(buffer)

commit = project.commits.get(gitlabShortSHA)
merge_request=commit.merge_requests()
if merge_request:
    mr_iid=merge_request[0]['iid']
    mr=project.mergerequests.get(mr_iid)
    mr.notes.create({'body': buffer})
