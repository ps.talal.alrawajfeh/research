#!/usr/bin/env python3

import json
import sys
import os

req_ps_count = float(os.getenv("K6_RPS_THRESHOLD", 15))
req_status_count = float(os.getenv("K6_SUCCESS_THRESHOLD", 95))
req_duration_count = float(os.getenv("K6_DURATION_THRESHOLD", 500))
test_result = {
    "testname": "",
    "rps": "",
    "success": "",
    "duration": "",
    "pass": True,
}

h1 = f" | TEST SCRIPT                                                  | RPS (>={req_ps_count})     | 200 (>={req_status_count}) | TIME (<={req_duration_count})   |  PASS/FAIL |"
h2 = f" | ------------------------------------------------------------ | -------------- | ---------- | -------------- | ---------- |"


def format_md(original: float, units: str, failed: bool):
    md_string = f'{original}{units}'
    if failed:
        return f'**{md_string}**'
    else:
        return f'  {md_string}  '


all_results = []

infilename = sys.argv[1]
outfilename = sys.argv[2]

if not os.path.exists(f"{outfilename}.md"):
    print(h1)
    print(h2)
    with open(f"{outfilename}.md", "w") as md_file:
        print(h1, file=md_file)
        print(h2, file=md_file)

with open(infilename, "r") as read_file:
    data = json.load(read_file)

basename = infilename.split("/")[-1]
test_result['testname'] = basename.split(".")[0]

metrics = data.get('metrics')
http_reqs = metrics.get('http_reqs')
if http_reqs:
    test_result['rps'] = round(http_reqs.get('rate', 0), 2)
else:
    test_result['rps'] = 0
md_rps = format_md(test_result['rps'], '/s', test_result['rps'] < req_ps_count)

checks = metrics.get('checks')
if checks:
    suc_req = checks.get('passes', 0)
    fal_req = checks.get('fails', 0)
    test_result['success'] = round((suc_req * 100) / (suc_req + fal_req), 2)
else:
    test_result['success'] = 0
md_success = format_md(
    test_result['success'], '%', test_result['success'] < req_status_count)

http_req_duration = metrics.get('http_req_duration')
if http_req_duration:
    test_result['duration'] = round(http_req_duration.get('avg', 0), 2)
else:
    test_result['duration'] = 0
md_duration = format_md(
    test_result['duration'], 'ms', test_result['duration'] > req_duration_count)

if (test_result['rps'] < req_ps_count or test_result['success'] < req_status_count or test_result['duration'] > req_duration_count):
    md_pass = "**FAILED**"
    test_result['pass'] = False
else:
    md_pass = "  Passed  "
    test_result['pass'] = True

md_string = f" | {test_result['testname']:60} | {md_rps:>14} | {md_success:>10} | {md_duration:>14} | {md_pass:10} |"

print(md_string)
with open(f"{outfilename}.md", "a") as md_file:
    print(md_string, file=md_file)

if os.path.exists(f"{outfilename}.json"):
    with open(f"{outfilename}.json", "r") as json_file:
        all_results = json.load(json_file)

all_results.append(test_result)

with open(f"{outfilename}.json", "w") as json_file:
    json.dump(all_results, json_file, indent=4)
