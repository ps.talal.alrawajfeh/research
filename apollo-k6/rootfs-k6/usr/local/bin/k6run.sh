#!/bin/sh

export EXECDIR=$(dirname $0)

test_file=$1
result_file=$(basename ${test_file} .js)".json"

k6  run \
    ${K6_EXTRA_ARGS} \
    --stage ${RAMP_UP_TIME}:${NUMBER_OF_USERS} \
    --stage ${BENCHMARK_TIME}:${NUMBER_OF_USERS} \
    --stage ${RAMP_DOWN_TIME}:0 \
    --summary-export=${K6_RESULTDIR}/${result_file} \
    --env SERVICE_URL=${SERVICE_URL} \
    ${test_file}

${EXECDIR}/k6format-result.py ${K6_RESULTDIR}/${result_file} ${K6_RESULTDIR}/${K6_ALLRESULTS}
