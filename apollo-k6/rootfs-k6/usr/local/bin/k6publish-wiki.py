#!/usr/bin/env python3

import sys
import gitlab
import datetime

def upload_page(filename, projectid, token):
    basename=filename.split("/")[-1]
    resultsBin = open(filename, 'r')
    gl = gitlab.Gitlab('https://gitlab.com', private_token=gitlabToken)
    project = gl.projects.get(gitlabProjectID)

    ## If no home page, create once and read it.
    try:
        homePage = project.wikis.get('home')
    except:
        project.wikis.create({'title': 'home','content': f'### K6 Benchmark Results\n'})
        homePage = project.wikis.get('home')

    try:
        resultpage=project.wikis.get(basename)
        resultpage.delete()
        project.wikis.create({'title': basename,'content': resultsBin.read()})
    except:
        project.wikis.create({'title': basename,'content': resultsBin.read()})
        homePage.content = homePage.content + f'- [{basename}]({basename})\n'
        homePage.save()


##################################################################
## MAIN
##################################################################
gitlabProjectID = sys.argv[1]
gitlabToken = sys.argv[2]
resultsPath = sys.argv[3]

upload_page(f"{resultsPath}.json", gitlabProjectID, gitlabToken)
upload_page(f"{resultsPath}.md",   gitlabProjectID, gitlabToken)
