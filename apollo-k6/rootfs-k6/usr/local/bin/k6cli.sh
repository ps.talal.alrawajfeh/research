#!/bin/sh

k6print_header()
{
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    echo "k6cli K6 command line helper"
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    echo "ServiceURL=       ${SERVICE_URL}"
    echo "Max Users=        ${K6_MAXUSERS}"
    echo "Test Duration=    ${K6_MAXDURATION}"
    echo "All Results=      ${K6_ALLRESULTS}"
    echo ""
    mkdir -p ${K6_RESULTDIR}
}

#############################################################################
## Main
##
if [ $# -lt 2 ]; then
    echo "ERROR: k6cli <service_url> <test_folder>"
    exit 1
fi

export EXECDIR=$(dirname $0)
export SERVICE_URL=${1}
export K6_RESULTDIR=${K6_RESULTDIR:-"./target"}
export K6_ALLRESULTS=${K6_ALLRESUTLS:-"k6-results"}-${CI_COMMIT_REF_NAME}
k6print_header

shift 1
echo " ======================================================================================================================="
for folder in $@
do
    find ${folder} -name "*.js" -type f  -print0 | sort -zn | xargs -0 -I '{}' ${EXECDIR}/k6run.sh '{}' 
done
echo " ======================================================================================================================="

${EXECDIR}/k6publish-wiki.py ${CI_PROJECT_ID} ${PSCI_GITLAB_TOKEN} ${K6_RESULTDIR}/${K6_ALLRESULTS}
${EXECDIR}/k6merge-request.py ${CI_PROJECT_ID} ${PSCI_GITLAB_TOKEN} ${CI_COMMIT_REF_NAME} ${CI_COMMIT_SHORT_SHA}
