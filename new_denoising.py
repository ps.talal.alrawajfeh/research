import gzip
import json
import math
import os
import pickle
import random

import matplotlib.pyplot as plt

os.environ['KERAS_BACKEND'] = 'jax'


# keras = tf.keras
import keras
import cv2
import numpy as np
from keras.callbacks import ModelCheckpoint
from keras.initializers import GlorotNormal, GlorotUniform
from keras.layers import Input, Conv2D, BatchNormalization, AveragePooling2D, \
    UpSampling2D, Concatenate, Layer, ReLU, MaxPooling2D, ZeroPadding2D, Add, GlobalAveragePooling2D, Reshape, Dense, \
    Activation, Multiply
from keras.losses import binary_crossentropy
from keras.metrics import BinaryIoU
from keras.models import Model
from keras.models import load_model
from keras.optimizers import Adam
from keras.utils import Sequence
from tqdm import tqdm

with open('denoising_net_config.json', 'r') as f:
    CONFIG = json.loads(f.read())

INPUT_IMAGES_PATH = CONFIG['input_images_path']
OUTPUT_IMAGES_PATH = CONFIG['output_images_path']
TEST_INPUT_IMAGES_PATHS = tuple(CONFIG['test_input_images_paths'])
TEST_OUTPUT_PATH = CONFIG['test_output_images_path']
INPUT_SHAPE = tuple(CONFIG['input_shape'])
TRAINING_IMAGES_PERCENTAGE = CONFIG['training_images_percentage']
VALIDATION_TEST_IMAGES_PERCENTAGE = CONFIG['validation_test_images_percentage']
MODEL_FILE_NAME = CONFIG['model_file_name']
TRAINING_BATCH_SIZE = CONFIG['training_batch_size']
VALIDATION_BATCH_SIZE = CONFIG['validation_batch_size']
UNET_FILTERS = tuple(CONFIG['unet_filters'])
TRAINING_EPOCHS = CONFIG['training_epochs']
EPSILON = 1e-8


class SqueezeExcite(Layer):
    def __init__(self,
                 reduction_ratio=1,
                 kernel_initializer='glorot_uniform',
                 kernel_regularizer=None):
        super().__init__()
        self.reduction_ratio = reduction_ratio
        self.kernel_initializer = kernel_initializer
        self.kernel_regularizer = kernel_regularizer

    def build(self, input_shape):
        channels = input_shape[-1]

        self.global_average_pooling = GlobalAveragePooling2D()
        self.reshape1 = Reshape((channels,))
        self.fully_connected1 = Dense(channels // self.reduction_ratio,
                                      activation=None,
                                      use_bias=True,
                                      kernel_initializer=self.kernel_initializer,
                                      kernel_regularizer=self.kernel_regularizer)
        self.activation1 = ReLU()
        self.fully_connected2 = Dense(channels,
                                      activation=None,
                                      use_bias=True,
                                      kernel_initializer=self.kernel_initializer,
                                      kernel_regularizer=self.kernel_regularizer)
        self.activation2 = Activation('sigmoid')
        self.reshape2 = Reshape((1, 1, channels))
        self.scale = Multiply()

    def call(self, inputs):
        x = inputs

        x = self.global_average_pooling(x)
        x = self.reshape1(x)
        x = self.fully_connected1(x)
        x = self.activation1(x)
        x = self.fully_connected2(x)
        x = self.activation2(x)
        x = self.reshape2(x)
        return self.scale([inputs, x])

    def get_config(self):
        return {
            'reduction_ratio': self.reduction_ratio,
            'kernel_initializer': self.kernel_initializer,
            'kernel_regularizer': self.kernel_regularizer
        }


class LocationAttention(Layer):
    def __init__(self,
                 filters,
                 kernel_initializer='glorot_uniform',
                 kernel_regularizer=None):
        super().__init__()
        self.filters = filters
        self.kernel_initializer = kernel_initializer
        self.kernel_regularizer = kernel_regularizer

    def build(self, input_shape):
        self.x_transform = Conv2D(self.filters,
                                  1,
                                  strides=1,
                                  use_bias=False,
                                  padding='valid',
                                  kernel_initializer=self.kernel_initializer,
                                  kernel_regularizer=self.kernel_regularizer)
        self.batch_normalization1 = BatchNormalization(epsilon=1.001e-5)

        self.g_transform = Conv2D(self.filters,
                                  1,
                                  strides=1,
                                  use_bias=False,
                                  padding='valid',
                                  kernel_initializer=self.kernel_initializer,
                                  kernel_regularizer=self.kernel_regularizer)
        self.batch_normalization2 = BatchNormalization(epsilon=1.001e-5)

        self.relu = ReLU()

        self.psi_transform = Conv2D(1,
                                    1,
                                    strides=1,
                                    use_bias=False,
                                    padding='valid',
                                    kernel_initializer=self.kernel_initializer,
                                    kernel_regularizer=self.kernel_regularizer)
        self.batch_normalization3 = BatchNormalization(epsilon=1.001e-5)

        self.sigmoid = Activation('sigmoid')
        self.addition = Add()
        self.multiplication = Multiply()

    def call(self, inputs):
        x, g = inputs
        x = self.x_transform(x)
        g = self.g_transform(g)
        psi = self.addition([x, g])
        psi = self.psi_transform(psi)
        return self.multiplication([x, psi])

    def get_config(self):
        return {
            'filters': self.filters,
            'kernel_initializer': self.kernel_initializer,
            'kernel_regularizer': self.kernel_regularizer
        }


class ResidualSqueezeExciteConv2D(Layer):
    def __init__(self,
                 filters,
                 kernel_size=3,
                 strides=1,
                 filters_multiplier=4,
                 reduction_ratio=8,
                 kernel_initializer='glorot_uniform',
                 kernel_regularizer=None,
                 conv_shortcut=False):
        super().__init__()
        self.filters = filters
        self.kernel_size = kernel_size
        self.strides = strides
        self.kernel_initializer = kernel_initializer
        self.kernel_regularizer = kernel_regularizer
        self.conv_shortcut = conv_shortcut
        self.filters_multiplier = filters_multiplier
        self.reduction_ratio = reduction_ratio

    def build(self, input_shape):
        self.pre_activation_batch_normalization = BatchNormalization(epsilon=1.001e-5)
        self.pre_activation_relu = ReLU()

        if self.conv_shortcut:
            self.shortcut = Conv2D(self.filters_multiplier * self.filters,
                                   1,
                                   strides=self.strides,
                                   use_bias=True,
                                   padding='valid',
                                   kernel_initializer=self.kernel_initializer,
                                   kernel_regularizer=self.kernel_regularizer)
        else:
            self.shortcut = MaxPooling2D(1, strides=self.strides) if self.strides > 1 else None

        self.conv1 = Conv2D(self.filters,
                            1,
                            strides=1,
                            use_bias=False,
                            padding='valid',
                            kernel_initializer=self.kernel_initializer,
                            kernel_regularizer=self.kernel_regularizer)
        self.batch_normalization1 = BatchNormalization(epsilon=1.001e-5)
        self.relu1 = ReLU()

        padding_size = (self.kernel_size - 1) // 2
        if self.kernel_size % 2 == 1:
            self.zero_padding_2d = ZeroPadding2D(padding=((padding_size, padding_size),
                                                          (padding_size, padding_size)))
        else:
            remainder = self.kernel_size - padding_size * 2
            self.zero_padding_2d = ZeroPadding2D(padding=((padding_size, remainder),
                                                          (padding_size, remainder)))

        self.conv2 = Conv2D(self.filters,
                            self.kernel_size,
                            strides=self.strides,
                            use_bias=False,
                            padding='valid',
                            kernel_initializer=self.kernel_initializer,
                            kernel_regularizer=self.kernel_regularizer)
        self.batch_normalization2 = BatchNormalization(epsilon=1.001e-5)
        self.relu2 = ReLU()

        self.conv3 = Conv2D(self.filters_multiplier * self.filters,
                            1,
                            strides=1,
                            use_bias=True,
                            padding='valid',
                            kernel_initializer=self.kernel_initializer,
                            kernel_regularizer=self.kernel_regularizer)

        self.squeeze_excite = SqueezeExcite(self.reduction_ratio,
                                            self.kernel_initializer,
                                            self.kernel_regularizer)
        self.addition = Add()

    def call(self, inputs):
        x = inputs
        pre_activation = self.pre_activation_batch_normalization(x)
        pre_activation = self.pre_activation_relu(pre_activation)

        if self.conv_shortcut:
            shortcut = self.shortcut(pre_activation)
        else:
            if self.shortcut is None:
                shortcut = x
            else:
                shortcut = self.shortcut(x)

        x = self.conv1(pre_activation)
        x = self.batch_normalization1(x)
        x = self.relu1(x)

        x = self.zero_padding_2d(x)
        x = self.conv2(x)
        x = self.batch_normalization2(x)
        x = self.relu2(x)

        x = self.conv3(x)
        # x = self.squeeze_excite(x)

        return self.addition([shortcut, x])

    def get_config(self):
        return {
            'filters': self.filters,
            'kernel_size': self.kernel_size,
            'strides': self.strides,
            'filters_multiplier': self.filters_multiplier,
            'reduction_ratio': self.reduction_ratio,
            'kernel_initializer': self.kernel_initializer,
            'kernel_regularizer': self.kernel_regularizer,
            'conv_shortcut': self.conv_shortcut
        }


def conv_batch_norm_relu_layer(input_layer, filters, kernel_size=3, strides=None):
    padding_size = (kernel_size - 1) // 2
    if kernel_size % 2 == 1:
        conv = ZeroPadding2D(padding=((padding_size, padding_size),
                                      (padding_size, padding_size)))(input_layer)
    else:
        remainder = kernel_size - padding_size * 2 - 1
        conv = ZeroPadding2D(padding=((padding_size, padding_size + remainder),
                                      (padding_size, padding_size + remainder)))(input_layer)
    if strides is None:
        conv = Conv2D(filters,
                      kernel_size,
                      kernel_initializer=GlorotUniform(),
                      use_bias=False,
                      padding='valid')(conv)
    else:
        conv = Conv2D(filters,
                      kernel_size,
                      strides=strides,
                      use_bias=False,
                      kernel_initializer=GlorotUniform(),
                      padding='valid')(conv)
    conv = BatchNormalization()(conv)
    conv = ReLU()(conv)
    return conv


def stack_residual_squeeze_excite_conv_blocks(x, filters, blocks, stride=2):
    x = ResidualSqueezeExciteConv2D(filters=filters, conv_shortcut=True)(x)
    for i in range(2, blocks):
        x = ResidualSqueezeExciteConv2D(filters)(x)
    x = ResidualSqueezeExciteConv2D(filters, strides=stride)(x)
    x = conv_batch_norm_relu_layer(x, filters)
    return x


def stacked_conv(input_layer, filters, kernel_size=3):
    conv = conv_batch_norm_relu_layer(input_layer, filters, kernel_size)
    return conv_batch_norm_relu_layer(conv, filters, kernel_size)


def advanced_unet(input_shape=INPUT_SHAPE):
    input_layer = Input(input_shape)

    conv_block0 = stacked_conv(input_layer, 1)
    conv_block1 = stack_residual_squeeze_excite_conv_blocks(conv_block0, 64, 3)
    conv_block2 = stack_residual_squeeze_excite_conv_blocks(conv_block1, 128, 3)
    conv_block3 = stack_residual_squeeze_excite_conv_blocks(conv_block2, 256, 3)
    conv_block4 = stack_residual_squeeze_excite_conv_blocks(conv_block3, 256, 3)

    up_sample1 = UpSampling2D(interpolation='nearest')(conv_block5)
    up_conv_layer1 = stacked_conv(up_sample1, 256)
    # location_attention1 = LocationAttention(filters=256)([conv_block4, up_conv_layer1])
    concatenated1 = Concatenate()([conv_block4, up_conv_layer1])
    decoded1 = stacked_conv(concatenated1, 256)

    up_sample2 = UpSampling2D(interpolation='nearest')(decoded1)
    up_conv_layer2 = stacked_conv(up_sample2, 192)
    # location_attention2 = LocationAttention(filters=192)([conv_block3, up_conv_layer2])
    concatenated2 = Concatenate()([conv_block3, up_conv_layer2])
    decoded2 = stacked_conv(concatenated2, 192)

    up_sample3 = UpSampling2D(interpolation='nearest')(decoded2)
    up_conv_layer3 = stacked_conv(up_sample3, 128)
    # location_attention3 = LocationAttention(filters=128)([conv_block2, up_conv_layer3])
    concatenated3 = Concatenate()([conv_block2, up_conv_layer3])
    decoded3 = stacked_conv(concatenated3, 128)

    up_sample4 = UpSampling2D(interpolation='nearest')(decoded3)
    up_conv_layer4 = stacked_conv(up_sample4, 64)
    # location_attention4 = LocationAttention(filters=64)([conv_block1, up_conv_layer4])
    concatenated4 = Concatenate()([conv_block1, up_conv_layer4])
    decoded4 = stacked_conv(concatenated4, 64)

    up_sample5 = UpSampling2D(interpolation='nearest')(decoded4)
    up_conv_layer5 = stacked_conv(up_sample5, 1)
    # location_attention5 = LocationAttention(filters=1)([input_layer, up_conv_layer5])
    concatenated5 = Concatenate()([conv_block0, up_conv_layer5])
    # decoded5 = stacked_conv(concatenated5, 1)

    concatenated5 = ZeroPadding2D(padding=((1, 1), (1, 1)))(concatenated5)

    output_layer = Conv2D(filters=1,
                          kernel_size=3,
                          strides=1,
                          use_bias=True,
                          padding='valid',
                          kernel_initializer='glorot_uniform',
                          kernel_regularizer=None,
                          activation='sigmoid')(concatenated5)

    return Model(inputs=input_layer, outputs=output_layer)


def u_net(input_shape=INPUT_SHAPE):
    filters = UNET_FILTERS
    # encoder
    input_layer = Input(shape=input_shape)
    batch_norm = BatchNormalization()(input_layer)

    convolutions = []
    encoder = batch_norm
    for i in range(len(filters) - 1):
        conv = stacked_conv(encoder, filters[i])
        encoder = AveragePooling2D((2, 2))(conv)
        convolutions.append(conv)

    # bottleneck
    conv = stacked_conv(encoder, filters[-1], 2)

    # decoder
    decoder = conv
    for i in range(len(filters) - 2, -1, -1):
        up = Concatenate(axis=-1)([UpSampling2D()(decoder), convolutions[i]])
        decoder = stacked_conv(up, filters[i], 3)

    prediction = Conv2D(1,
                        (1, 1),
                        kernel_initializer=GlorotNormal(),
                        use_bias=True,
                        padding='valid',
                        activation='sigmoid')(decoder)

    return Model(inputs=input_layer, outputs=prediction)


def pre_process(image_tensor: np.ndarray, input_shape=INPUT_SHAPE) -> np.ndarray:
    image_tensor = np.array(255 - image_tensor, np.float32)
    return (image_tensor / 255.0).reshape(input_shape)


def read_pickle_compressed(file_path):
    with gzip.open(file_path, 'rb') as f:
        return pickle.load(f)


def read_compressed_image(file_path):
    with gzip.open(file_path, 'rb') as f:
        content = f.read()
    content_array = np.asarray(bytearray(content), dtype=np.uint8)
    return cv2.imdecode(content_array, cv2.IMREAD_GRAYSCALE)


class DataGenerator(Sequence):
    def __init__(self,
                 input_image_paths,
                 output_image_paths,
                 batch_size,
                 **kwargs):
        super().__init__(*kwargs)
        self.count = len(input_image_paths)
        self.input_image_paths = input_image_paths
        self.output_image_paths = output_image_paths
        self.batch_size = batch_size

    def __getitem__(self, index):
        input_image_paths = self.input_image_paths[index * self.batch_size: (index + 1) * self.batch_size]
        input_images = [pre_process(cv2.imread(f, cv2.IMREAD_GRAYSCALE)) for f in input_image_paths]
        output_image_paths = self.output_image_paths[index * self.batch_size: (index + 1) * self.batch_size]
        output_images = [pre_process(cv2.imread(f, cv2.IMREAD_GRAYSCALE)) for f in output_image_paths]
        return np.array(input_images), np.array(output_images)

    def __len__(self):
        return int(math.ceil(self.count / self.batch_size))


def load_image_paths():
    input_image_paths = os.listdir(INPUT_IMAGES_PATH)
    input_image_paths = list(filter(lambda x: x.endswith('.png'), input_image_paths))
    corresponding_image_paths = []
    for f in input_image_paths:
        number = f.split('.')[0].strip()
        corresponding_image_paths.append(os.path.join(OUTPUT_IMAGES_PATH, f'{number}.png'))
    input_image_paths = [os.path.join(INPUT_IMAGES_PATH, f) for f in input_image_paths]
    return input_image_paths, corresponding_image_paths


def train_validation_test_split(input_images, output_images):
    indices = list(range(len(input_images)))
    random.shuffle(indices)
    input_images = [input_images[i] for i in indices]
    output_images = [output_images[i] for i in indices]
    train_images_count = int(len(input_images) * TRAINING_IMAGES_PERCENTAGE)
    validation_test_images_count = int(
        len(input_images) * VALIDATION_TEST_IMAGES_PERCENTAGE)
    return ((input_images[:train_images_count],
             output_images[:train_images_count]),
            (input_images[train_images_count:train_images_count + validation_test_images_count],
             output_images[train_images_count:train_images_count + validation_test_images_count]),
            (input_images[train_images_count + validation_test_images_count:],
             output_images[train_images_count + validation_test_images_count:]))


def cache_object(obj, file):
    with open(file, 'wb') as f:
        pickle.dump(obj, f)


def load_cached(file):
    with open(file, 'rb') as f:
        return pickle.load(f)


def train_model():
    if os.path.isfile(MODEL_FILE_NAME + '.keras'):
        model = load_model(MODEL_FILE_NAME + '.keras', compile=False)
    else:
        model = u_net()
    model.summary()

    model.compile(loss=binary_crossentropy, optimizer=Adam(), metrics=[BinaryIoU()])

    if os.path.isfile('x_train.pickle') and os.path.isfile('y_train.pickle') and \
            os.path.isfile('x_val.pickle') and os.path.isfile('y_val.pickle') and \
            os.path.isfile('x_test.pickle') and os.path.isfile('y_test.pickle'):
        x_train = load_cached('x_train.pickle')
        y_train = load_cached('y_train.pickle')
        x_val = load_cached('x_val.pickle')
        y_val = load_cached('y_val.pickle')
    else:
        input_image_paths, output_image_paths = load_image_paths()
        (x_train, y_train), (x_val, y_val), (x_test, y_test) = train_validation_test_split(
            input_image_paths, output_image_paths)
        cache_object(x_train, 'x_train.pickle')
        cache_object(y_train, 'y_train.pickle')
        cache_object(x_val, 'x_val.pickle')
        cache_object(y_val, 'y_val.pickle')
        cache_object(x_test, 'x_test.pickle')
        cache_object(y_test, 'y_test.pickle')

    train_data_generator = DataGenerator(x_train, y_train, TRAINING_BATCH_SIZE)
    val_data_generator = DataGenerator(x_val, y_val, VALIDATION_BATCH_SIZE)

    model_last_checkpoint_callback = ModelCheckpoint(
        filepath=MODEL_FILE_NAME + '_{epoch:02d}_{loss:0.4f}_{binary_io_u:0.4f}_{val_loss:0.4f}_{val_binary_io_u:0.4f}.keras',
        save_best_only=False
    )

    model_best_checkpoint_callback = ModelCheckpoint(
        filepath=MODEL_FILE_NAME + '.keras',
        save_best_only=True,
        monitor='val_binary_io_u',
        mode='min'
    )

    history = model.fit(train_data_generator,
                        epochs=TRAINING_EPOCHS,
                        validation_data=val_data_generator,
                        callbacks=[model_last_checkpoint_callback,
                                   model_best_checkpoint_callback])

    cache_object(history, MODEL_FILE_NAME + '_history.pickle')


def evaluate_model():
    model = load_model(MODEL_FILE_NAME + '.keras', compile=False)
    model.compile(loss=binary_crossentropy, optimizer=Adam())
    x_test = load_cached('x_test.pickle')
    y_test = load_cached('y_test.pickle')
    test_data_generator = DataGenerator(x_test, y_test, 2)
    results = model.evaluate(test_data_generator)
    cache_object(results, 'evaluation_results.pickle')
    print(results)


def remove_white_border(binary_image):
    mask = (255 - binary_image) > 0

    height, width = binary_image.shape[0:2]
    mask1, mask2 = mask.any(0), mask.any(1)
    x1, x2 = mask1.argmax(), width - mask1[::-1].argmax()
    y1, y2 = mask2.argmax(), height - mask2[::-1].argmax()

    return binary_image[y1:y2, x1:x2]


def threshold_otsu(image):
    return cv2.threshold(image,
                         0,
                         255,
                         cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]


def post_process(output_image):
    output_image = output_image * 255
    output_image[output_image > 255] = 255
    output_image[output_image < 0] = 0
    output_image = np.array(output_image, np.uint8)
    output_image = 255 - output_image
    return threshold_otsu(output_image)


def read_image_grayscale(image_path):
    return cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)


def feed_forward(image, output_path, output_file_name):
    model = load_model(MODEL_FILE_NAME + '.keras', compile=False)
    model.compile(loss=binary_crossentropy, optimizer=Adam())
    if image.shape[1] < 1000:
        image = cv2.resize(image, None, fx=2, fy=2, interpolation=cv2.INTER_CUBIC)
    elif image.shape[1] > 2000:
        image = cv2.resize(image, None, fx=1 / 2, fy=1 / 2, interpolation=cv2.INTER_AREA)

    image = image[-384:, :768]
    input_tensor = np.array([pre_process(image, (384, 768, 1))])

    # uncomment all the lines below to see the outputs of each filter
    output_tensor = model.predict(input_tensor)[0]
    # image1 = output_tensor[:, :, 0]
    # image2 = output_tensor[:, :, 1]
    # image3 = output_tensor[:, :, 2]
    # image4 = output_tensor[:, :, -1]

    # cv2.imwrite(os.path.join(output_path, output_file_name + '_1.png'), post_process(image1))
    # cv2.imwrite(os.path.join(output_path, output_file_name + '_2.png'), post_process(image2))
    # cv2.imwrite(os.path.join(output_path, output_file_name + '_3.png'), post_process(image3))
    cv2.imwrite(os.path.join(output_path, output_file_name + '.png'), post_process(output_tensor, (96, 416)))


def test_model():
    i = 1
    for input_images_path in TEST_INPUT_IMAGES_PATHS:
        print(f'testing {i}/{len(TEST_INPUT_IMAGES_PATHS)}...')
        image_paths = [os.path.join(input_images_path, f) for f in os.listdir(input_images_path)]
        output_path = os.path.join(TEST_OUTPUT_PATH, os.path.basename(input_images_path))
        if not os.path.isdir(output_path):
            os.makedirs(output_path)
        progress_bar = tqdm(total=len(image_paths))
        for image_path in image_paths:
            image = read_image_grayscale(image_path)
            feed_forward(image, output_path, os.path.splitext(os.path.basename(image_path))[0])
            progress_bar.update()
        progress_bar.close()
        i += 1


def create_model_with_new_input_shape(model_path, new_input_shape, output_path):
    # model = load_model(model_path)
    model = u_net()
    model.load_weights(model_path)
    new_model = u_net(new_input_shape)

    for i, layer in enumerate(model.layers):
        new_model.layers[i].set_weights(layer.get_weights())

    new_model.save(output_path)


def test_feed_forward(model_path, image, input_shape, output_path):
    model = load_model(model_path, compile=False)
    model.compile(loss=binary_crossentropy, optimizer=Adam())
    image = cv2.resize(image, (input_shape[1], input_shape[0]), interpolation=cv2.INTER_CUBIC)
    input_tensor = np.array([pre_process(image, input_shape)])

    output_tensor = model(input_tensor)[0].numpy()
    output = output_tensor[:, :, -1]
    cv2.imwrite(output_path, post_process(output))


def test_multiple(model_path, input_path, output_path, input_shape):
    if not os.path.isdir(output_path):
        os.makedirs(output_path)

    model = load_model(model_path, compile=False)
    model.compile(loss=binary_crossentropy, optimizer=Adam())

    files = os.listdir(input_path)
    progress_bar = tqdm(total=len(files))
    for f in files:
        if not f.endswith('.png'):
            continue
        image = cv2.imread(os.path.join(input_path, f), cv2.IMREAD_GRAYSCALE)
        if np.max(image.shape) > 512:
            resize_ratio = 512 / np.max(image.shape)
            image = cv2.resize(image, None, fx=resize_ratio, fy=resize_ratio, interpolation=cv2.INTER_CUBIC)

        width_padding = 512 - image.shape[1]
        width_pad1 = width_padding // 2
        width_pad2 = width_padding - width_pad1
        height_padding = 512 - image.shape[0]
        height_pad1 = height_padding // 2
        height_pad2 = height_padding - height_pad1

        padded_image = np.pad(image,
                              ((height_pad1, height_pad2), (width_pad1, width_pad2)),
                              'constant',
                              constant_values=((255, 255), (255, 255)))

        image = threshold_otsu(image)
        input_tensor = np.array([pre_process(padded_image, input_shape)])

        output_tensor = model.predict(input_tensor) #model(input_tensor)[0].numpy()
        output = output_tensor.reshape((512, 512))
        output = output[height_pad1:height_pad1 + image.shape[0], width_pad1: width_pad1 + image.shape[1]]
        cv2.imwrite(os.path.join(output_path, f), post_process(output))
        progress_bar.update()


def main():
    # model = ResNet50V2()
    # model.summary()
    # keras.utils.plot_model(model, show_shapes=True, show_layer_activations=True, show_layer_names=True)
    # train_model()
    # evaluate_model()
    # test_model()
    # create_model_with_new_input_shape('ACC_model.h5',
    # (384, 768, 1),
    # 'new_model_ACC.h5')
    # test_feed_forward('/home/u764/Downloads/fonts-downloads/new_model.h5',
    #                   cv2.imread(
    #                       '/home/u764/Downloads/checks/Document 2/Name1 1 .jpg',
    #                       cv2.IMREAD_GRAYSCALE),
    #                   (320, 704, 1),
    #                   '/home/u764/Downloads/fonts-downloads/test.png')

    # create_model_with_new_input_shape('./denoising_net_08_0.0017_0.9912_0.0019_0.9903.hdf5',
    #                                   (512, 512, 1),
    #                                   'denoising_net.h5')
    # test_feed_forward('/home/u764/Downloads/denoising_net.h5',
    #                   cv2.imread('/home/u764/Downloads/a400c867-4001-46a0-83b7-a0d8b40c4127.jpg', cv2.IMREAD_GRAYSCALE),
    #                   (512, 512, 1),
    #                   '/home/u764/Downloads/test.png')
    test_multiple('denoising_net.h5',
                  '/home/u764/Downloads/cropped_objects_from_yolo_labels_2/cropped_objects_from_yolo_labels',
                  '/home/u764/Downloads/cropped_objects_from_yolo_labels_2/output',
                  (512, 512, 1))


if __name__ == '__main__':
    main()

