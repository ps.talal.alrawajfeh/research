import 'dart:typed_data';

import 'package:image/image.dart';
import 'dart:io';
import 'dart:math';

import 'package:tuple/tuple.dart';

const ID_CARD_SIZE = Tuple2(750, 476);
const ACCEPTANCE_THRESHOLD = 0.7;
const RED_WEIGHT = 0.299;
const GREEN_WEIGHT = 0.587;
const BLUE_WEIGHT = 1.0 - (RED_WEIGHT + GREEN_WEIGHT);

class LabelDefinition {
  Point location;
  Tuple2<int, int> searchRadius;
  GrayscaleImage labelImage;

  LabelDefinition(this.location, this.searchRadius, this.labelImage);
}

class AutoShootResult {
  double score;
  Map<String, Point> labelLocations;
  Image image;
  AutoShootResult(this.score, this.labelLocations, this.image);
}

double RgbToGrayscale(int r, int g, int b) {
  var value =
      (r * RED_WEIGHT + g * GREEN_WEIGHT + b * BLUE_WEIGHT).roundToDouble();
  return value > 255.0 ? 255.0 : value;
}

class GrayscaleImage {
  int width;
  int height;
  Float32List pixels;

  GrayscaleImage(this.width, this.height, this.pixels);

  static GrayscaleImage fromImage(Image image) {
    return GrayscaleImage(
        image.width,
        image.height,
        Float32List.fromList(image.data
            .map((x) =>
                RgbToGrayscale(x & 0xff, (x >> 8) & 0xff, (x >> 16) & 0xff)
                    .toDouble())
            .toList()));
  }

  static GrayscaleImage fromBinaryImage(Image image) {
    return GrayscaleImage(
        image.width,
        image.height,
        Float32List.fromList(image.data
            .map((x) =>
                RgbToGrayscale(x & 0xff, (x >> 8) & 0xff, (x >> 16) & 0xff)
                            .toDouble() >
                        127.0
                    ? 255.0
                    : 0.0)
            .toList()));
  }
}

num relu(num x) {
  if (x < 0) {
    return 0;
  } else {
    return x;
  }
}

double normalizedCrossCorrelation(
    GrayscaleImage image, GrayscaleImage template, Rectangle rectangle,
    {epsilon = 1e-7}) {
  var width = rectangle.width;
  var height = rectangle.height;

  var area = (width * height).toDouble();

  var x1 = rectangle.left;
  var y1 = rectangle.top;

  var stride1 = image.width;
  var stride2 = template.width;

  var offset1 = width;
  var offset2 = width;

  var i1 = y1 * stride1 + x1;
  var i2 = 0;

  var sum1 = 0.0;
  var sum2 = 0.0;

  for (var y = 0; y < height; y++) {
    for (var x = 0; x < width; x++) {
      sum1 += image.pixels[i1];
      sum2 += template.pixels[i2];

      i1++;
      i2++;
    }

    i1 += stride1 - offset1;
    i2 += stride2 - offset2;
  }

  var mu1 = sum1 / area;
  var mu2 = sum2 / area;

  var dotProduct12 = 0.0;
  var dotProduct11 = 0.0;
  var dotProduct22 = 0.0;

  i1 = y1 * stride1 + x1;
  i2 = 0;

  for (var y = 0; y < height; y++) {
    for (var x = 0; x < width; x++) {
      var zeroCentered1 = image.pixels[i1] - mu1;
      var zeroCentered2 = template.pixels[i2] - mu2;

      dotProduct12 += zeroCentered1 * zeroCentered2;
      dotProduct11 += zeroCentered1 * zeroCentered1;
      dotProduct22 += zeroCentered2 * zeroCentered2;

      i1++;
      i2++;
    }

    i1 += stride1 - offset1;
    i2 += stride2 - offset2;
  }

  if (dotProduct22 < epsilon) {
    if (dotProduct11 == 0 && dotProduct12 == 0) {
      return 1.0;
    }
    return 0.0;
  }

  return dotProduct12 / sqrt(dotProduct11 * dotProduct22);
}

Tuple2<double, Point> NccSearch(GrayscaleImage image, GrayscaleImage template,
    Point location, Tuple2<int, int> searchRadius,
    {double epsilon = 1e-4}) {
  var lx = location.x;
  var ly = location.y;

  var dx = searchRadius.item1;
  var dy = searchRadius.item2;

  var lx1 = relu(lx - dx);
  var ly1 = relu(ly - dy);
  var lx2 = min(lx + template.width + dx, image.width);
  var ly2 = min(ly + template.height + dy, image.height);

  if (lx2 <= lx1 ||
      ly2 <= ly1 ||
      lx2 - lx1 < template.width ||
      ly2 - ly1 < template.height) {
    return Tuple2<double, Point>(0.0, null);
  }

  var widthDifference = lx2 - lx1 - template.width;
  var heightDifference = ly2 - ly1 - template.height;

  var maxCC = -1.0;
  Point bestLocation;

  for (var y = 0; y < heightDifference; y++) {
    for (var x = 0; x < widthDifference; x++) {
      var cc = normalizedCrossCorrelation(image, template,
          Rectangle(lx1 + x, ly1 + y, template.width, template.height));

      if (cc > maxCC) {
        maxCC = cc;
        bestLocation = Point(lx1 + x, ly1 + y);
      }
    }
  }

  return Tuple2<double, Point>(maxCC, bestLocation);
}

double pointsSimilarity(List<List<double>> points1, List<List<double>> points2,
    {double epsilon = 1e-7}) {
  var similarity = 1.0;

  for (var i1 = 0; i1 < points1.length; i1++) {
    for (var i2 = 0; i2 < points2.length; i2++) {
      if (i1 == i2) {
        continue;
      }

      var diffX1 = points1[i1][0] - points1[i2][0];
      var diffY1 = points1[i1][1] - points1[i2][1];

      var diffX2 = points2[i1][0] - points2[i2][0];
      var diffY2 = points2[i1][1] - points2[i2][1];

      var magnitude1 = sqrt(diffX1 * diffX1 + diffY1 * diffY1);
      var magnitude2 = sqrt(diffX2 * diffX2 + diffY2 * diffY2);

      var dotProduct = diffX1 * diffX2 + diffY1 * diffY2;

      similarity *= dotProduct / (magnitude1 * magnitude2);
    }
  }

  return similarity;
}

AutoShootResult autoShoot(Image image, Rectangle rectangle,
    Map<String, LabelDefinition> labelDefinitions) {
  var cropped = copyCrop(
      image, rectangle.left, rectangle.top, rectangle.width, rectangle.height);
  var resized = copyResize(cropped,
      width: ID_CARD_SIZE.item1,
      height: ID_CARD_SIZE.item2,
      interpolation: Interpolation.cubic);

  var grayImage = GrayscaleImage.fromImage(resized);

  var approximateLocations = <List<double>>[];
  var foundLocations = <List<double>>[];
  var labelLocations = <String, Point>{};
  var totalScore = 1.0;

  for (var label in labelDefinitions.keys) {
    var labelDefinition = labelDefinitions[label];

    approximateLocations.add([
      labelDefinition.location.x.toDouble(),
      labelDefinition.location.y.toDouble()
    ]);

    var nccResult = NccSearch(grayImage, labelDefinition.labelImage,
        labelDefinition.location, labelDefinition.searchRadius);

    labelLocations[label] = nccResult.item2;
    foundLocations
        .add([nccResult.item2.x.toDouble(), nccResult.item2.y.toDouble()]);
    totalScore = min(totalScore, nccResult.item1);
  }

  totalScore *= pointsSimilarity(approximateLocations, foundLocations);
  totalScore = sqrt(totalScore);

  return AutoShootResult(totalScore, labelLocations, resized);
}

Image autoCrop(Image image, Map<String, Point> labelLocations,
    Map<String, LabelDefinition> labelDefinitions) {
  var x1 = 0;
  var y1 = 0;
  var x2 = ID_CARD_SIZE.item1;
  var y2 = ID_CARD_SIZE.item2;

  for (var label in labelDefinitions.keys) {
    var originalX = labelDefinitions[label].location.x;
    var originalY = labelDefinitions[label].location.y;

    var x = labelLocations[label].x;
    var y = labelLocations[label].y;

    x1 = max(x1, relu(x - originalX));
    y1 = max(y1, relu(y - originalY));
    x2 = min(x2, x + (ID_CARD_SIZE.item1 - originalX));
    y2 = min(y2, y + (ID_CARD_SIZE.item2 - originalY));
  }

  var result = copyCrop(image, x1, y1, x2 - x1, y2 - y1);

  if (x2 - x1 < ID_CARD_SIZE.item1 || y2 - y1 < ID_CARD_SIZE.item2) {
    result = copyResize(result,
        width: ID_CARD_SIZE.item1,
        height: ID_CARD_SIZE.item2,
        interpolation: Interpolation.cubic);
  }

  return result;
}

Image readImage(String path) {
  return decodeImage(File(path).readAsBytesSync());
}

void main(List<String> arguments) {
  var rectangle = Rectangle(240, 240, 1500, 950);

  var labelDefinitions = {
    'national_number': LabelDefinition(
        Point(618, 293),
        Tuple2<int, int>(35, 35),
        GrayscaleImage.fromBinaryImage(
            readImage('./resources/auto_shot_demo/national_number_label.bmp'))),
    'name': LabelDefinition(
        Point(27, 136),
        Tuple2<int, int>(35, 35),
        GrayscaleImage.fromBinaryImage(
            readImage('./resources/auto_shot_demo/name_label.bmp'))),
    'mother_name': LabelDefinition(
        Point(657, 427),
        Tuple2<int, int>(35, 35),
        GrayscaleImage.fromBinaryImage(
            readImage('./resources/auto_shot_demo/mother_name_label.bmp'))),
    'arabic_name': LabelDefinition(
        Point(681, 103),
        Tuple2<int, int>(35, 35),
        GrayscaleImage.fromBinaryImage(
            readImage('./resources/auto_shot_demo/arabic_name_label.bmp')))
  };

  var image = readImage(
      '/home/u764/Downloads/ekyc-data/drive-download-20200830T074731Z-001/test-auto-shoot.jpg');

  var autoShootResult = autoShoot(image, rectangle, labelDefinitions);
  var cropped = autoCrop(
      autoShootResult.image, autoShootResult.labelLocations, labelDefinitions);

  var start = DateTime.now().millisecondsSinceEpoch;
  for (var i = 0; i < 100; i++) {
    autoShootResult = autoShoot(image, rectangle, labelDefinitions);
  }
  var end = DateTime.now().millisecondsSinceEpoch;
  print((end - start).toDouble() / 100.0);

  File('/home/u764/Downloads/test.jpg')..writeAsBytesSync(encodeJpg(cropped));
}
