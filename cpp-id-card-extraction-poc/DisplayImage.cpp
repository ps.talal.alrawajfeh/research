#include <iostream>
#include <chrono>
#include <opencv2/opencv.hpp>

const float EPSILON = 1e-7;

cv::Mat resizeImageByFactor(const cv::Mat &image, double resizeFactor, int interpolation)
{
    if (abs(resizeFactor) < EPSILON)
    {
        return image;
    }

    cv::Mat resized;
    cv::resize(image,
               resized,
               cv::Size(0, 0),
               resizeFactor,
               resizeFactor,
               interpolation);

    return resized;
}

int getImageMaxDimension(const cv::Mat &image)
{
    if (image.size[0] > image.size[1])
    {
        return image.size[0];
    }
    return image.size[1];
}

cv::Mat resizeImageMaxDimension(const cv::Mat &image, int targetMaxDimension, int interpolation)
{
    auto maxDimension = getImageMaxDimension(image);
    auto ratio = (double)targetMaxDimension / (double)maxDimension;
    return resizeImageByFactor(image, ratio, interpolation);
}

cv::Mat detectText(const cv::Mat &bgrImage)
{
    cv::Mat grayImage;
    cv::cvtColor(bgrImage, grayImage, cv::COLOR_BGR2GRAY);

    cv::Mat smoothed;
    cv::GaussianBlur(grayImage, smoothed, cv::Size(5, 5), 0);

    cv::Mat blackHatKernel = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(21, 21), cv::Point(10, 10));
    cv::Mat blackHat;
    cv::morphologyEx(smoothed, blackHat, cv::MORPH_BLACKHAT, blackHatKernel);

    cv::Mat sobelX;
    cv::Sobel(blackHat, sobelX, CV_64F, 1, 0, 5);
    cv::Mat squareSobelX;
    cv::multiply(sobelX, sobelX, squareSobelX);

    cv::Mat sobelY;
    cv::Sobel(blackHat, sobelY, CV_64F, 0, 1, 5);
    cv::Mat squareSobelY;
    cv::multiply(sobelY, sobelY, squareSobelY);

    cv::Mat squareGradients;
    cv::add(squareSobelX, squareSobelY, squareGradients);

    cv::Mat gradients;
    cv::sqrt(squareGradients, gradients);

    double min, max;
    cv::minMaxLoc(gradients, &min, &max);

    gradients -= min;
    gradients /= (max - min) / 255.0;

    cv::Mat edges;
    gradients.convertTo(edges, CV_8U);

    cv::Mat binaryImage;
    cv::threshold(edges, binaryImage, 0, 255, cv::THRESH_BINARY | cv::THRESH_OTSU);

    return binaryImage;
}

cv::Mat removeConnectedComponents(const cv::Mat &binaryImage,
                                  std::function<bool(int, int, int, int, int)> removalPredicate)
{
    cv::Mat result(binaryImage.size(), binaryImage.type(), cv::Scalar(0));

    cv::Mat labels;
    cv::Mat stats;
    cv::Mat centroids;

    int labelsCount = cv::connectedComponentsWithStats(binaryImage, labels, stats, centroids);

    for (int l = 0; l < labelsCount; l++)
    {
        int x = stats.at<int>(cv::Point(cv::CC_STAT_LEFT, l));
        int y = stats.at<int>(cv::Point(cv::CC_STAT_TOP, l));
        int w = stats.at<int>(cv::Point(cv::CC_STAT_WIDTH, l));
        int h = stats.at<int>(cv::Point(cv::CC_STAT_HEIGHT, l));
        int a = stats.at<int>(cv::Point(cv::CC_STAT_AREA, l));

        if (removalPredicate(x, y, w, h, a))
        {
            continue;
        }

        for (int y = 0; y < binaryImage.size[0]; y++)
        {
            for (int x = 0; x < binaryImage.size[1]; x++)
            {
                if (labels.at<int>(cv::Point(x, y)) == l && binaryImage.at<uchar>(cv::Point(x, y)) == 255)
                {
                    result.at<uchar>(cv::Point(x, y)) = 255;
                }
            }
        }
    }

    return result;
}

cv::Mat getTextMask(const cv::Mat &bgrImage)
{
    const int TEXT_DETECTION_MIN_AREA = 20;
    const double TEXT_DETECTION_MAX_WIDTH_RATIO = 0.25;
    const double TEXT_DETECTION_MAX_HEIGHT_RATIO = 0.25;
    const double TEXT_DETECTION_MAX_AREA_TO_IMAGE_AREA_RATIO = 0.005;
    const int TEXT_DETECTION_CLOSING_KERNEL_SIZE = 5;
    const int TEXT_DETECTION_CLOSING_KERNEL_ITERATIONS = 3;
    const double TEXT_DETECTION_CLOSED_MIN_AREA_TO_IMAGE_AREA_RATIO = 0.025;
    const int TEXT_DETECTION_OPENING_KERNEL_SIZE = 5;
    const int TEXT_DETECTION_OPENING_KERNEL_ITERATIONS = 1;
    const double TEXT_DETECTION_OPENED_MIN_HEIGHT_RATIO = 0.05;
    const double TEXT_DETECTION_OPENED_MIN_WIDTH_RATIO = 0.05;
    const int TEXT_DETECTION_FINAL_CLOSING_KERNEL_SIZE = 7;
    const int TEXT_DETECTION_FINAL_CLOSING_KERNEL_ITERATIONS = 1;

    int imageWidth = bgrImage.size[1];
    int imageHeight = bgrImage.size[0];

    cv::Mat detectedText = detectText(bgrImage);
    cv::Mat cleaned1 = removeConnectedComponents(detectedText,
                                                 [imageWidth,
                                                  imageHeight,
                                                  TEXT_DETECTION_MAX_WIDTH_RATIO,
                                                  TEXT_DETECTION_MAX_HEIGHT_RATIO,
                                                  TEXT_DETECTION_MAX_AREA_TO_IMAGE_AREA_RATIO](int, int, int w, int h, int a)
                                                 {
                                                     return a < TEXT_DETECTION_MIN_AREA ||
                                                            (double)w / (double)imageWidth > TEXT_DETECTION_MAX_WIDTH_RATIO ||
                                                            (double)h / (double)imageHeight > TEXT_DETECTION_MAX_HEIGHT_RATIO ||
                                                            (double)a / (double)(imageWidth * imageHeight) > TEXT_DETECTION_MAX_AREA_TO_IMAGE_AREA_RATIO;
                                                 });

    cv::Mat closingKernel1 = cv::getStructuringElement(cv::MORPH_RECT,
                                                       cv::Size(TEXT_DETECTION_CLOSING_KERNEL_SIZE, TEXT_DETECTION_CLOSING_KERNEL_SIZE),
                                                       cv::Point(TEXT_DETECTION_CLOSING_KERNEL_SIZE / 2, TEXT_DETECTION_CLOSING_KERNEL_SIZE / 2));
    cv::Mat closed1;
    cv::morphologyEx(cleaned1, closed1, cv::MORPH_CLOSE, closingKernel1, cv::Point(-1, -1), TEXT_DETECTION_CLOSING_KERNEL_ITERATIONS);
    
    cv::Mat cleaned2 = removeConnectedComponents(closed1,
                                                 [imageWidth,
                                                  imageHeight,
                                                  TEXT_DETECTION_CLOSED_MIN_AREA_TO_IMAGE_AREA_RATIO](int, int, int, int, int a)
                                                 { return (double)a / (double)(imageWidth * imageHeight) > TEXT_DETECTION_CLOSED_MIN_AREA_TO_IMAGE_AREA_RATIO; });
    cv::Mat openingKernel = cv::getStructuringElement(cv::MORPH_RECT,
                                                      cv::Size(TEXT_DETECTION_OPENING_KERNEL_SIZE, TEXT_DETECTION_OPENING_KERNEL_SIZE),
                                                      cv::Point(TEXT_DETECTION_OPENING_KERNEL_SIZE / 2, TEXT_DETECTION_OPENING_KERNEL_SIZE / 2));
    cv::Mat opened;
    cv::morphologyEx(cleaned2, opened, cv::MORPH_OPEN, openingKernel, cv::Point(-1, -1), TEXT_DETECTION_OPENING_KERNEL_ITERATIONS);

    cv::Mat cleaned3 = removeConnectedComponents(opened, [imageWidth,
                                                          imageHeight,
                                                          TEXT_DETECTION_OPENED_MIN_WIDTH_RATIO,
                                                          TEXT_DETECTION_OPENED_MIN_HEIGHT_RATIO](int, int, int w, int h, int)
                                                 { return (double)w / (double)imageWidth < TEXT_DETECTION_OPENED_MIN_WIDTH_RATIO &&
                                                          (double)h / (double)imageHeight < TEXT_DETECTION_OPENED_MIN_HEIGHT_RATIO; });

    cv::Mat closingKernel2 = cv::getStructuringElement(cv::MORPH_RECT,
                                                       cv::Size(TEXT_DETECTION_FINAL_CLOSING_KERNEL_SIZE, TEXT_DETECTION_FINAL_CLOSING_KERNEL_SIZE),
                                                       cv::Point(TEXT_DETECTION_FINAL_CLOSING_KERNEL_SIZE / 2, TEXT_DETECTION_FINAL_CLOSING_KERNEL_SIZE / 2));
    cv::Mat closed2;
    cv::morphologyEx(cleaned3, closed2, cv::MORPH_CLOSE, closingKernel2, cv::Point(-1, -1), TEXT_DETECTION_FINAL_CLOSING_KERNEL_ITERATIONS);

    return closed2;
}

int main(int, char **)
{
    cv::Mat image;
    image = cv::imread("/home/u764/Development/data/ekyc-data/all_id_cards/IMG_9306.JPG", cv::IMREAD_COLOR);

    cv::Mat resized = resizeImageMaxDimension(image, 1500, cv::INTER_CUBIC);
    // cv::Mat result = getTextMask(resized);

    // cv::imwrite("/home/u764/Downloads/out.bmp", result);

    std::chrono::time_point<std::chrono::system_clock> startTime = std::chrono::system_clock::now();
    int n = 200;
    for (int i = 0; i < n; i++)
    {
        auto result = getTextMask(resized);
    }
    std::chrono::time_point<std::chrono::system_clock> endTime = std::chrono::system_clock::now();

    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(endTime - startTime).count();

    std::cout << (double)duration / (double)n << std::endl;
    return 0;
}
