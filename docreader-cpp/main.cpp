#include <iostream>
#include <opencv2/opencv.hpp>

#define uint8 uint8_t

const float EPSILON = 1e-7;

cv::Mat rescaleToByteValues(cv::Mat &mat);

cv::Mat resizeImageByFactor(const cv::Mat &image, double resizeFactor, int interpolation) {
    if (abs(resizeFactor - 1.0) < EPSILON) {
        return image.clone();
    }
    cv::Mat resized;
    cv::resize(image,
               resized,
               cv::Size(0, 0),
               resizeFactor,
               resizeFactor,
               interpolation);
    return resized;
}

int getImageMaxDimension(const cv::Mat &image) {
    if (image.size[0] > image.size[1]) {
        return image.size[0];
    }
    return image.size[1];
}

cv::Mat resizeImageMaxDimension(const cv::Mat &image, int targetMaxDimension, int interpolation) {
    auto maxDimension = getImageMaxDimension(image);
    auto ratio = (double) targetMaxDimension / (double) maxDimension;
    return resizeImageByFactor(image, ratio, interpolation);
}


cv::Mat edgeDetectionGray(const cv::Mat &image,
                          int edgeKernelSize,
                          int gaussianBlurKernelSize,
                          int gaussianBlurSigma,
                          int medianBlurKernelSize) {
    cv::Mat smoothedMedian;
    if (medianBlurKernelSize > 0) {
        cv::medianBlur(image, smoothedMedian, medianBlurKernelSize);
    } else {
        smoothedMedian = image;
    }

    cv::Mat smoothedGaussian;
    if (gaussianBlurKernelSize > 0) {
        cv::GaussianBlur(smoothedMedian,
                         smoothedGaussian,
                         cv::Size(gaussianBlurKernelSize, gaussianBlurKernelSize),
                         gaussianBlurSigma);
    } else {
        smoothedGaussian = smoothedMedian;
    }

    cv::Mat sobelX;
    cv::Sobel(smoothedGaussian, sobelX, CV_64F, 1, 0, edgeKernelSize);
    cv::Mat sobelY;
    cv::Sobel(smoothedGaussian, sobelY, CV_64F, 0, 1, edgeKernelSize);

    cv::Mat squareSobelX;
    cv::multiply(sobelX, sobelX, squareSobelX);
    cv::Mat squareSobelY;
    cv::multiply(sobelY, sobelY, squareSobelY);

    cv::Mat squareGradients;
    cv::add(squareSobelX, squareSobelY, squareGradients);
    cv::Mat gradients;
    cv::sqrt(squareGradients, gradients);

    return rescaleToByteValues(gradients);
}

cv::Mat ReduceMaxAbs(const cv::Mat &image) {
    cv::Mat absoluteValues = cv::abs(image);

    auto channels = absoluteValues.channels();

    cv::Mat result;
    cv::extractChannel(absoluteValues, result, 0);

    for (int i = 1; i < channels; i++) {
        cv::Mat tempChannel;
        cv::extractChannel(absoluteValues, tempChannel, i);
        result = cv::max(result, tempChannel);
    }

    return result;
}

inline int specialFloor(const double value) {
    const int floorValue = (int) value;
    return floorValue > value ? floorValue - 1 : floorValue;
}


cv::Mat rescaleToByteValues(cv::Mat &mat) {
    double min, max;
    cv::minMaxLoc(mat, &min, &max);
    mat -= min;
    mat /= (max - min) / 255.0;
    cv::Mat edges(mat.rows, mat.cols, CV_8UC1);

    auto *srcPtr = mat.data;
    auto *destPtr = edges.data;

    auto srcStride = mat.step.buf[0];
    auto destStride = edges.step.buf[0];

    auto srcTypeSize = mat.step.buf[1];
    auto srcOffset = srcStride - mat.cols * srcTypeSize;
    auto destOffset = destStride - edges.cols;

    double *currentSrcPtr;

    for (int y = 0; y < mat.rows; y++) {
        currentSrcPtr = (double *) srcPtr;
        for (int x = 0; x < mat.cols; x++) {
            double val = *currentSrcPtr;
            if (val < 0) {
                *destPtr = 0;
            } else if (val > 255) {
                *destPtr = 255;
            } else {
                *destPtr = static_cast<unsigned char>(specialFloor(val));
            }

            currentSrcPtr++;
            srcPtr += srcTypeSize;
            destPtr++;
        }
        srcPtr += srcOffset;
        destPtr += destOffset;
    }

    return edges;
}

cv::Mat edgeDetectionColored(const cv::Mat &image,
                             int edgeKernelSize,
                             int gaussianBlurKernelSize,
                             double gaussianBlurSigma,
                             int medianBlurKernelSize) {
    cv::Mat smoothedMedian;
    if (medianBlurKernelSize > 0) {
        cv::medianBlur(image, smoothedMedian, medianBlurKernelSize);
    } else {
        smoothedMedian = image;
    }

    cv::Mat smoothedGaussian;
    if (gaussianBlurKernelSize > 0) {
        cv::GaussianBlur(smoothedMedian,
                         smoothedGaussian,
                         cv::Size(gaussianBlurKernelSize, gaussianBlurKernelSize),
                         gaussianBlurSigma);
    } else {
        smoothedGaussian = smoothedMedian;
    }

    cv::Mat sobelX;
    cv::Sobel(smoothedGaussian, sobelX, CV_64F, 1, 0, edgeKernelSize);
    cv::Mat reducedX = ReduceMaxAbs(sobelX);

    cv::Mat sobelY;
    cv::Sobel(smoothedGaussian, sobelY, CV_64F, 0, 1, edgeKernelSize);
    cv::Mat reducedY = ReduceMaxAbs(sobelY);

    cv::Mat squareSobelX;
    cv::multiply(reducedX, reducedX, squareSobelX);
    cv::Mat squareSobelY;
    cv::multiply(reducedY, reducedY, squareSobelY);

    cv::Mat squareGradients;
    cv::add(squareSobelX, squareSobelY, squareGradients);
    cv::Mat gradients;
    cv::sqrt(squareGradients, gradients);

    return rescaleToByteValues(gradients);
}


cv::Mat thresholdOtsu(const cv::Mat &image) {
    cv::Mat binary;
    cv::threshold(image, binary, 0, 255, cv::THRESH_BINARY | cv::THRESH_OTSU);
    return binary;
}


cv::Mat textDetection(const cv::Mat &image) {
    cv::Mat grayImage;
    cv::cvtColor(image, grayImage, cv::COLOR_BGR2GRAY);

    cv::Mat smoothed;
    cv::GaussianBlur(grayImage, smoothed, cv::Size(5, 5), 0.0);

    cv::Mat blackHatKernel = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(21, 21));
    cv::Mat blackHat;
    cv::morphologyEx(smoothed, blackHat, cv::MORPH_BLACKHAT, blackHatKernel);

    cv::Mat edges = edgeDetectionGray(blackHat, 5, 0, 0.0, 0);
    return thresholdOtsu(edges);
}


cv::Mat removeConnectedComponents(const cv::Mat &binaryImage,
                                  const std::function<bool(int, int, int, int, int)> &removalPredicate) {
    cv::Mat result(binaryImage.rows, binaryImage.cols, binaryImage.type());

    cv::Mat labels;
    cv::Mat stats;
    cv::Mat centroids;

    auto srcPtr = binaryImage.data;
    auto srcStride = binaryImage.step.buf[0];
    auto srcOffset = srcStride - binaryImage.cols;

    auto destPtr = result.data;
    auto destStride = result.step.buf[0];
    auto destOffset = destStride - result.cols;

    int labelsCount = cv::connectedComponentsWithStats(binaryImage, labels, stats, centroids, CV_32S);

    auto labelsPtr = labels.data;
    auto labelsTypeSize = labels.step.buf[1];
    auto labelsStride = labels.step.buf[0];
    auto labelsOffset = labelsStride - labels.cols * labelsTypeSize;

    std::vector<bool> chosenLabels(labelsCount);

    for (int l = 0; l < labelsCount; l++) {
        int left = stats.at<int>(cv::Point(cv::CC_STAT_LEFT, l));
        int top = stats.at<int>(cv::Point(cv::CC_STAT_TOP, l));
        int width = stats.at<int>(cv::Point(cv::CC_STAT_WIDTH, l));
        int height = stats.at<int>(cv::Point(cv::CC_STAT_HEIGHT, l));
        int area = stats.at<int>(cv::Point(cv::CC_STAT_AREA, l));

        chosenLabels[l] = !removalPredicate(left, top, width, height, area);
    }

    int *currentLabelsPtr;
    for (int y = 0; y < binaryImage.rows; y++) {
        currentLabelsPtr = (int *) labelsPtr;
        for (int x = 0; x < binaryImage.cols; x++) {
            auto label = *currentLabelsPtr;
            if (*srcPtr == 255 && chosenLabels[label]) {
                *destPtr = 255;
            } else {
                *destPtr = 0;
            }

            srcPtr++;
            destPtr++;
            currentLabelsPtr++;
            labelsPtr += labelsTypeSize;
        }
        srcPtr += srcOffset;
        destPtr += destOffset;
        labelsPtr += labelsOffset;
    }

    return result;
}


cv::Mat removeForeground(const cv::Mat &image, int kernelSize, int iterations) {
    cv::Mat kernel = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(kernelSize, kernelSize));
    cv::Mat closed;
    cv::morphologyEx(image,
                     closed,
                     cv::MORPH_CLOSE,
                     kernel,
                     cv::Point(-1, -1),
                     iterations);
    return closed;
}


cv::Mat getTextMask(const cv::Mat &bgrImage) {
    const int TEXT_DETECTION_MIN_AREA = 20;
    const double TEXT_DETECTION_MAX_WIDTH_RATIO = 0.25;
    const double TEXT_DETECTION_MAX_HEIGHT_RATIO = 0.25;
    const double TEXT_DETECTION_MAX_AREA_TO_IMAGE_AREA_RATIO = 0.005;
    const int TEXT_DETECTION_CLOSING_KERNEL_SIZE = 5;
    const int TEXT_DETECTION_CLOSING_KERNEL_ITERATIONS = 3;
    const double TEXT_DETECTION_CLOSED_MIN_AREA_TO_IMAGE_AREA_RATIO = 0.025;
    const int TEXT_DETECTION_OPENING_KERNEL_SIZE = 5;
    const int TEXT_DETECTION_OPENING_KERNEL_ITERATIONS = 1;
    const double TEXT_DETECTION_OPENED_MIN_HEIGHT_RATIO = 0.05;
    const double TEXT_DETECTION_OPENED_MIN_WIDTH_RATIO = 0.05;
    const int TEXT_DETECTION_FINAL_CLOSING_KERNEL_SIZE = 7;
    const int TEXT_DETECTION_FINAL_CLOSING_KERNEL_ITERATIONS = 1;

    int imageWidth = bgrImage.size[1];
    int imageHeight = bgrImage.size[0];

    cv::Mat detectedText = textDetection(bgrImage);
    cv::Mat cleaned1 = removeConnectedComponents(detectedText,
                                                 [imageWidth,
                                                         imageHeight,
                                                         TEXT_DETECTION_MAX_WIDTH_RATIO,
                                                         TEXT_DETECTION_MAX_HEIGHT_RATIO,
                                                         TEXT_DETECTION_MAX_AREA_TO_IMAGE_AREA_RATIO](int, int, int w,
                                                                                                      int h, int a) {
                                                     return a < TEXT_DETECTION_MIN_AREA ||
                                                            (double) w / (double) imageWidth >
                                                            TEXT_DETECTION_MAX_WIDTH_RATIO ||
                                                            (double) h / (double) imageHeight >
                                                            TEXT_DETECTION_MAX_HEIGHT_RATIO ||
                                                            (double) a / (double) (imageWidth * imageHeight) >
                                                            TEXT_DETECTION_MAX_AREA_TO_IMAGE_AREA_RATIO;
                                                 });

    cv::Mat closingKernel1 = cv::getStructuringElement(cv::MORPH_RECT,
                                                       cv::Size(TEXT_DETECTION_CLOSING_KERNEL_SIZE,
                                                                TEXT_DETECTION_CLOSING_KERNEL_SIZE));
    cv::Mat closed1;
    cv::morphologyEx(cleaned1,
                     closed1,
                     cv::MORPH_CLOSE,
                     closingKernel1,
                     cv::Point(-1, -1),
                     TEXT_DETECTION_CLOSING_KERNEL_ITERATIONS);

    cv::Mat cleaned2 = removeConnectedComponents(closed1,
                                                 [imageWidth,
                                                         imageHeight,
                                                         TEXT_DETECTION_CLOSED_MIN_AREA_TO_IMAGE_AREA_RATIO](int, int,
                                                                                                             int, int,
                                                                                                             int a) {
                                                     return (double) a / (double) (imageWidth * imageHeight) >
                                                            TEXT_DETECTION_CLOSED_MIN_AREA_TO_IMAGE_AREA_RATIO;
                                                 });
    cv::Mat openingKernel = cv::getStructuringElement(cv::MORPH_RECT,
                                                      cv::Size(TEXT_DETECTION_OPENING_KERNEL_SIZE,
                                                               TEXT_DETECTION_OPENING_KERNEL_SIZE));
    cv::Mat opened;
    cv::morphologyEx(cleaned2, opened, cv::MORPH_OPEN, openingKernel, cv::Point(-1, -1),
                     TEXT_DETECTION_OPENING_KERNEL_ITERATIONS);

    cv::Mat cleaned3 = removeConnectedComponents(opened, [imageWidth,
            imageHeight,
            TEXT_DETECTION_OPENED_MIN_WIDTH_RATIO,
            TEXT_DETECTION_OPENED_MIN_HEIGHT_RATIO](int, int, int w, int h, int) {
        return (double) w / (double) imageWidth < TEXT_DETECTION_OPENED_MIN_WIDTH_RATIO &&
               (double) h / (double) imageHeight < TEXT_DETECTION_OPENED_MIN_HEIGHT_RATIO;
    });

    cv::Mat closingKernel2 = cv::getStructuringElement(cv::MORPH_RECT,
                                                       cv::Size(TEXT_DETECTION_FINAL_CLOSING_KERNEL_SIZE,
                                                                TEXT_DETECTION_FINAL_CLOSING_KERNEL_SIZE));
    cv::Mat closed2;
    cv::morphologyEx(cleaned3,
                     closed2,
                     cv::MORPH_CLOSE,
                     closingKernel2,
                     cv::Point(-1, -1),
                     TEXT_DETECTION_FINAL_CLOSING_KERNEL_ITERATIONS);

    return closed2;
}


int otsuThreshold(const cv::Mat &grayImage, const cv::Mat &maskImage) {
    const int height = grayImage.rows;
    const int width = grayImage.cols;

    auto grayImagePtr = grayImage.data;
    auto maskImagePtr = maskImage.data;

    auto grayImageStride = grayImage.step.buf[0];
    auto maskImageStride = maskImage.step.buf[0];

    auto grayImageOffset = grayImageStride - grayImage.cols;
    auto maskImageOffset = maskImageStride - maskImage.cols;

    int histogram[256];
    for (int &i: histogram) {
        i = 0;
    }

    int n = 0;

    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {
            if (*maskImagePtr > 0) {
                int i = static_cast<int>(*grayImagePtr);
                histogram[i]++;
                n++;
            }
            grayImagePtr++;
            maskImagePtr++;
        }
        grayImagePtr += grayImageOffset;
        maskImagePtr += maskImageOffset;
    }

    double scale = 1.0 / n;
    double mu = 0.0;

    int *pHistogram = &histogram[0];
    for (int i = 0; i < 256; i++) {
        mu += static_cast<double>(*pHistogram) * static_cast<double>(i);
        pHistogram++;
    }
    mu *= scale;

    double q1 = 0;
    int maxVal = 0;
    double maxSigma = -1;
    double mu1 = 0;

    for (int t = 0; t < 256; t++) {
        double pt = static_cast<double>(histogram[t]) * scale;
        mu1 *= q1;
        q1 += pt;
        double q2 = 1.0 - q1;

        if (std::min(q1, q2) < EPSILON || std::max(q1, q2) > 1.0 - EPSILON) {
            continue;
        }

        mu1 = (mu1 + t * pt) / q1;
        double mu2 = (mu - q1 * mu1) / q2;
        double sigma = q1 * q2 * (mu1 - mu2) * (mu1 - mu2);

        if (sigma > maxSigma) {
            maxSigma = sigma;
            maxVal = t;
        }
    }

    return maxVal;
}

int main(int, char **) {
    cv::Mat image;
    image = cv::imread("/home/u764/Development/data/ekyc-data/PSO ID/Amani F.jpg", cv::IMREAD_COLOR);
    cv::Mat resized = resizeImageMaxDimension(image, 1500, cv::INTER_CUBIC);

    std::chrono::time_point<std::chrono::system_clock> startTime = std::chrono::system_clock::now();

    auto detectedText = getTextMask(resized);
    auto foregroundRemoved = removeForeground(resized, 5, 3);

    cv::Mat luvImage;
    cv::cvtColor(foregroundRemoved, luvImage, cv::COLOR_BGR2Luv);
    auto gradients = edgeDetectionColored(foregroundRemoved, 5, 7, 1.2, 0);

    std::cout << "otsu: " << otsuThreshold(gradients, gradients > 0) << std::endl;

    std::chrono::time_point<std::chrono::system_clock> endTime = std::chrono::system_clock::now();

    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(endTime - startTime).count();

    std::cout << (double) duration / (double) 1 << std::endl;
    cv::imwrite("/home/u764/Development/progressoft/research/docreader-cpp/test.png", detectedText);
    return 0;
}