#!/bin/bash

if [[ -d ./build ]]
then
    rm -rf ./build
fi

if [[ -d ./dist ]]
then
    rm -rf ./dist
fi

if [[ -e ./hello_world.spec ]]
then
    rm -f hello_world.spec
fi

# install python dependencies
python3 -m pip install --user --upgrade -r requirements.txt

# NOTE: this dependency must be installed since without it python3 pyinstaller module doesn't work
sudo apt-get install upx

# compile hello world
$HOME/.local/bin/pyinstaller hello_world.py

# build docker container
docker build --tag hello-world-python .

echo ''
echo ''
echo 'creating docker container of hello-world-python...'
echo ''
echo ''

docker run --rm -it hello-world-python

