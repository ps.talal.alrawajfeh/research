 private static Bitmap ConvoluteOdd(Bitmap inputImage, ConvolutionOptions options)
        {
            try
            {
                if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed)
                    return null;

                int width = inputImage.Width;
                int height = inputImage.Height;
                Rectangle rectLock = new Rectangle(0, 0, width, height);
                Bitmap outputImage = BitmapFactory.CreateGrayImage(width, height);
                BitmapData bmDataOut = outputImage.LockBits(rectLock, ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                BitmapData bmDataIn = inputImage.LockBits(rectLock, ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                int strideIn = bmDataIn.Stride;
                int strideOut = bmDataOut.Stride;
                byte* pOut = (byte*)bmDataOut.Scan0.ToPointer();
                byte* pIn = (byte*)bmDataIn.Scan0.ToPointer();

                int rightEdgeMargine = width - options.LeftMargine;
                int bottomEdgeMargine = height - options.LeftMargine;

                for (int r = 0; r < width; r++)
                {
                    for (int c = 0; c < height; c++)
                    {
                        float sumOfGrayValue = 0;

                        /* image boundaries */
                        if (r < options.LeftMargine || r >= rightEdgeMargine ||
                            c < options.LeftMargine || c >= bottomEdgeMargine)
                        {
                            sumOfGrayValue = options.PreservceEdgesColor ? pIn[(strideIn * c) + r] :
                                                options.EdgeGrayValue;
                        }
                        else
                        {
                            /* Convolution starts here */
                            for (int I = -options.LeftMargine; I <= options.LeftMargine; I++)
                                for (int J = -options.LeftMargine; J <= options.LeftMargine; J++)
                                    sumOfGrayValue += pIn[(strideIn * (c + I)) + (r + J)] *
                                                      options.Kernel[J + options.LeftMargine, I + options.LeftMargine];
                            sumOfGrayValue = Math.Max(0, Math.Min(255, options.Bias + (options.Weight * sumOfGrayValue)));
                        }
                        pOut[(strideOut * c) + r] = (byte)(options.InvertColor ? (255 - sumOfGrayValue) : sumOfGrayValue);
                    }
                }
                inputImage.UnlockBits(bmDataIn);
                outputImage.UnlockBits(bmDataOut);
                return outputImage;
            }
            catch
            {
                return null;
            }
        }
