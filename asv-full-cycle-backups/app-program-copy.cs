using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Progressoft.AppUtils.Core.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.CompilerServices;
using Apache.NMS.ActiveMQ.Threads;
using Microsoft.Diagnostics.Tracing.Stacks;
using Progressoft.AppUtils.Core;
using Progressoft.ASV.RequestProcessor.AsvApis.DocumentVerification;
using Progressoft.ASV.RequestProcessor.DocumentVerification;
using Progressoft.ASV.RequestProcessor.Messaging;
using Progressoft.CommunicationBridge.Core.Messaging;
using Task = System.Threading.Tasks.Task;

[assembly: InternalsVisibleTo("Progressoft.ASV.RequestProcessor.Test")]

namespace Progressoft.ASV.RequestProcessor
{
    internal class AppProgram
    {
        //https://devblogs.microsoft.com/dotnet/net-core-workers-as-windows-services/
        //https://www.hanselman.com/blog/dotnet-new-worker-windows-services-or-linux-systemd-services-in-net-core
        //https://medium.com/@frankkerrigan/creating-a-service-for-both-linux-systemd-and-windows-service-fe4ddfa68597
        //https://keycloak.discourse.group/t/tls-and-cipher-suites/8088

        private static void Main(string[] args)
        {
            var chequeImagesInfoLines =
                File.ReadAllLines("/home/u764/Downloads/automation/files/DemoSignedDocumentImages/chqInfo.txt");
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            for (int i = 0; i < 1040; i++)
            {
                var innerStopWatch = new Stopwatch();
                innerStopWatch.Start();
                var req = RequestSignaturesVerification(i, chequeImagesInfoLines);
                var message = req.Message;
                ProcessBody(message);
                innerStopWatch.Stop();
                Console.WriteLine($"{i}/1040 --> {innerStopWatch.ElapsedMilliseconds} ms");
            }

            stopwatch.Stop();
            Console.WriteLine((double) stopwatch.ElapsedMilliseconds / 1040.0);

            return;
            Console.ForegroundColor = ConsoleColor.Gray;

            var loggerInitializationResult = LoggerFactory.Instance.Initialize();
            if (loggerInitializationResult.IsFailure)
                throw new AppException($"Logging initialization failed: {loggerInitializationResult.FullErrorMessage}");
            Console.Title = "PS-ASV :: Requests Processing Service ...";

            CreateHostBuilder(args).Build().Run();
        }

        private static void ProcessBody(Message<AsvActions, DocumentVerificationRequestBody> message)
        {
            var requestBody = message.Body;
            var signatureVerificationApi = new SignatureVerificationApi();
            var classifyAndEnhanceResult = signatureVerificationApi.ClassifyAndEnhance(requestBody.DocumentId,
                requestBody.Transaction,
                requestBody.Documents);
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            var verificationResult = signatureVerificationApi.Verify(requestBody.DocumentId,
                classifyAndEnhanceResult.Value.TemplateId,
                requestBody.Transaction,
                classifyAndEnhanceResult.Value.EnhancedBinaryImage,
                classifyAndEnhanceResult.Value.SignatureAreas);
            stopwatch.Stop();
            Console.WriteLine($"AppProgram.ProcessBody(Verify) took {stopwatch.ElapsedMilliseconds} ms.");
        }

        private static DocumentVerificationRequestBody CreateSignaturesVerificationRequestBody(int index,
            string[] chequeImagesInfoLines)
        {
            string chqInfoLine = chequeImagesInfoLines[index];
            string[] chqInfo = chqInfoLine.Split(new char[] {','}, StringSplitOptions.RemoveEmptyEntries);
            var accountNumber = chqInfo[1];
            while (accountNumber.Length < 12)
                accountNumber = "0" + accountNumber;
            var documents = new List<SignedDocument>();
            documents.Add(new SignedDocument()
            {
                Format = SignedDocumentImageFormat.BMP,
                Image = File.ReadAllBytes("/home/u764/Downloads/automation/files/DemoSignedDocumentImages/" +
                                          chqInfo[0])
            });

            return new()
            {
                DocumentId = chqInfo[0],
                Transaction = new Transaction()
                {
                    AccountNumber = accountNumber,
                    Amount = (decimal) Randomizer.NextDouble(10, 100000),
                    Currency = "JOD",
                    Date = Iso8601DateTime.Now().ToString(),
                    SerialNumber = index.ToString()
                },
                Documents = documents
            };
        }

        private static MessageHeader<AsvActions> CreateHeader(AsvActions asvActions)
        {
            var recipient = "ASV";
            var sendors = "ECC, ECC".Split(",").Select(s => s.Trim()).Distinct().ToList();

            return MessageHeader<AsvActions>.Create(Randomizer.NextValue(sendors),
                recipient,
                Iso8601DateTime.Now(),
                asvActions,
                MessageModel.Request,
                true).Value;
        }

        private static MessageEnvelope<AsvActions, DocumentVerificationRequestBody> RequestSignaturesVerification(
            int index, string[] chequeImagesInfoLines)
        {
            var reqBody = CreateSignaturesVerificationRequestBody(index, chequeImagesInfoLines);
            if (reqBody == null) return null;

            var reqHeader = CreateHeader(AsvActions.DocumentVerification);
            var reqMessage = Message<AsvActions, DocumentVerificationRequestBody>.Create(reqHeader, reqBody).Value;
            var reqEnvelope = MessageEnvelope<AsvActions, DocumentVerificationRequestBody>.Create(reqMessage,
                Guid.NewGuid().ToString(),
                Iso8601DateTime.Now()).Value;
            return reqEnvelope;
        }


        private static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                .UseWindowsService()
                .UseSystemd()
                .ConfigureServices((hostContext, services) => { services.AddHostedService<AppServiceWorker>(); });
        }
    }
}
