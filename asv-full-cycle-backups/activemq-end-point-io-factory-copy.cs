using Progressoft.AppUtils.Core.CodeDesign;
using Progressoft.AppUtils.Core.Validations;
using Progressoft.CommunicationBridge.Core;
using Progressoft.CommunicationBridge.Core.Implementations.ActiveMQ;
using Progressoft.CommunicationBridge.Core.Messaging;
using Progressoft.CommunicationBridge.Core.Services;
using System;

namespace Progressoft.ASV.RequestProcessor.MessageQueue
{
    public sealed class ActiveMQEndPointIOFactory<TAction> : IEndPointIOFactory<TAction> where TAction : Enum
    {
        private const string INVALID_URL = "URL is null or empty";
        private const string NEGATIVE_MAX_REDELIVERIES_COUNT = "Maximum-redeliveries-count is negative";
        private const string INVALID_USERNAME = "Username is null or empty";
        private const string INVALID_PASSWORD = "Password is null or empty";
        private const string NOT_SSL_URL = "URL should start with 'ssl:'";
        private const string INVALID_QUEUE_NAME = "Queue-name is null or empty";
        private const string NULL_ENDPOINT = "The specified endpoint is null";
        private const string NEGATIVE_REPLY_TIMEOUT = "Negative reply timeout";
        private const string NEGATIVE_DELAY_BETWEEN_REDELIVERIES = "Negative delay between redeliveries";
        private const string NEGATIVE_BACKOFF_MULTIPLIER = "backoff multiplier timeout is negative";

        private readonly string _url;
        private readonly string _username;
        private readonly string _password;
        private readonly TimeSpan _replyTimeOut;
        private readonly int _maxRedeliveriesCount;
        private readonly TimeSpan _delayBetweenRedeliveries;
        private readonly bool _sslProtocol;
        private readonly int _backOffMultiplier;
        private readonly bool _useExponentialBackOff;
        private readonly MessageSerializationFormat _messageFormat;

        private ActiveMQEndPointIOFactory(string url,
                                  string username,
                                  string password,
                                  TimeSpan replyTimeOut,
                                  int maxRedeliveriesCount,
                                  TimeSpan delayBetweenRedeliveries,
                                  bool sslProtocol,
                                    int backOffMultiplier,
                                                                        bool useExponentialBackOff,
                                  MessageSerializationFormat messageFormat)
        {
            _url = url;
            _username = username;
            _password = password;
            _replyTimeOut = replyTimeOut;
            _maxRedeliveriesCount = maxRedeliveriesCount;
            _delayBetweenRedeliveries = delayBetweenRedeliveries;
            _sslProtocol = sslProtocol;
            _backOffMultiplier = backOffMultiplier;
            _useExponentialBackOff = useExponentialBackOff;
            _messageFormat = messageFormat;
        }

        public static Result<ActiveMQEndPointIOFactory<TAction>> Create(string url,
                                                                        TimeSpan replyTimeOut,
                                                                        int maxRedeliveriesCount,
                                                                        TimeSpan delayBetweenRedeliveries,
                                                                        int backOffMultiplier,
                                                                        bool useExponentialBackOff,
                                                                        MessageSerializationFormat messageFormat)
        {
            var validationResult = ValidationTrain.Create()
                                                  .ShouldNotBe(() => string.IsNullOrWhiteSpace(url), INVALID_URL)
                                                   .ShouldNotBe(() => replyTimeOut.TotalMilliseconds < 0, NEGATIVE_REPLY_TIMEOUT)
                                                  .ShouldNotBe(() => maxRedeliveriesCount < 0, NEGATIVE_MAX_REDELIVERIES_COUNT)
                                                  .ShouldNotBe(() => backOffMultiplier < 0, NEGATIVE_BACKOFF_MULTIPLIER)
                                                  .ShouldNotBe(() => delayBetweenRedeliveries.TotalMilliseconds < 0, NEGATIVE_DELAY_BETWEEN_REDELIVERIES)
                                                  .Status;

            if (validationResult.IsFailure)
                return validationResult.BindFail<ActiveMQEndPointIOFactory<TAction>>();

            return Result<ActiveMQEndPointIOFactory<TAction>>.Success(
                new ActiveMQEndPointIOFactory<TAction>(url,
                                                       string.Empty,
                                                       string.Empty,
                                                       replyTimeOut,
                                                       maxRedeliveriesCount,
                                                       delayBetweenRedeliveries,
                                                       false,
                                                       backOffMultiplier,
                                                       useExponentialBackOff,
                                                       messageFormat));
        }

        public static Result<ActiveMQEndPointIOFactory<TAction>> Create(string url,
                                                                        string username,
                                                                        string password,
                                                                        TimeSpan replyTimeOut,
                                                                        int maxRedeliveriesCount,
                                                                        TimeSpan delayBetweenRedeliveries,
                                                                        int backOffMultiplier,
                                                                        bool useExponentialBackOff,
                                                                        MessageSerializationFormat messageFormat)
        {
            var validationResult = ValidationTrain.Create()
                                                  .ShouldNotBe(() => string.IsNullOrWhiteSpace(url), INVALID_URL)
                                                  // .ShouldBe(() => url.StartsWith("ssl:", StringComparison.CurrentCultureIgnoreCase), NOT_SSL_URL)
                                                  .ShouldNotBe(() => string.IsNullOrWhiteSpace(username), INVALID_USERNAME)
                                                  .ShouldNotBe(() => string.IsNullOrWhiteSpace(password), INVALID_PASSWORD)
                                                  .ShouldNotBe(() => replyTimeOut.TotalMilliseconds < 0, NEGATIVE_REPLY_TIMEOUT)
                                                  .ShouldNotBe(() => delayBetweenRedeliveries.TotalMilliseconds < 0, NEGATIVE_DELAY_BETWEEN_REDELIVERIES)
                                                  .ShouldNotBe(() => maxRedeliveriesCount < 0, NEGATIVE_MAX_REDELIVERIES_COUNT)
                                                  .ShouldNotBe(() => backOffMultiplier < 0, NEGATIVE_BACKOFF_MULTIPLIER)
                                                  .Status;

            if (validationResult.IsFailure)
                return validationResult.BindFail<ActiveMQEndPointIOFactory<TAction>>();

            return Result<ActiveMQEndPointIOFactory<TAction>>.Success(
                new ActiveMQEndPointIOFactory<TAction>(url,
                                                       username,
                                                       password,
                                                       replyTimeOut,
                                                       maxRedeliveriesCount,
                                                       delayBetweenRedeliveries,
                                                       true,
                                                        backOffMultiplier,
                                                       useExponentialBackOff,
                                                       messageFormat));
        }

        public Result<EndPoint> CreateEndpoint(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
                return Result<EndPoint>.Fail(INVALID_QUEUE_NAME);

            return _sslProtocol ?
                EndPoint.Create(_url, name, _username, _password) :
                EndPoint.Create(_url, name);
        }

        public Result<IEndPointWriter<TAction, AcknowledgmentBody>> CreateAcknowledgmentWriter(EndPoint ackEndpoint)
        {
            return CreateWriter<AcknowledgmentBody>(ackEndpoint);
        }

        public Result<IEndPointWriter<TAction, TResponseBody>> CreateWriter<TResponseBody>(EndPoint responseEndpoint) where TResponseBody : MessageBody
        {
            if (responseEndpoint == null)
                return Result<IEndPointWriter<TAction, TResponseBody>>.Fail(NULL_ENDPOINT);

            var responseWriterResult = ActiveMQWriter<TAction, TResponseBody>.Create(responseEndpoint,
                                                                                     _replyTimeOut,
                                                                                     _maxRedeliveriesCount,
                                                                                     _delayBetweenRedeliveries,
                                                                                     _backOffMultiplier,
                                                                                     _useExponentialBackOff,
                                                                                     _messageFormat);
            if (responseWriterResult.IsFailure)
                return responseWriterResult.BindFail<IEndPointWriter<TAction, TResponseBody>>();

            return Result<IEndPointWriter<TAction, TResponseBody>>.Success(responseWriterResult.Value);
        }

        public Result<IEndPointReader<TAction, TRequestBody>> CreateReader<TRequestBody>(EndPoint requestEndpoint, ReadOptions readOptions) where TRequestBody : MessageBody
        {
            if (requestEndpoint == null)
                return Result<IEndPointReader<TAction, TRequestBody>>.Fail(NULL_ENDPOINT);

            var responseWriterResult = ActiveMQReader<TAction, TRequestBody>.Create(requestEndpoint,
                                                                                       readOptions,
                                                                                       _messageFormat);
            if (responseWriterResult.IsFailure)
                return responseWriterResult.BindFail<IEndPointReader<TAction, TRequestBody>>();

            return Result<IEndPointReader<TAction, TRequestBody>>.Success(responseWriterResult.Value);
        }
    }
}
