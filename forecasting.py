import calendar
import itertools
import json
import math
import os
import pickle
import random
import sys
import time
from datetime import datetime

import numpy as np
import tensorflow as tf
from matplotlib import pyplot as plt
from tqdm import tqdm
from xgboost import XGBRegressor


def build_fourier_model(n, p):
    a0 = tf.Variable(tf.random.normal((1,), dtype=tf.float32), name='a0')

    parameters = [a0]

    for i in range(1, n + 1):
        ai = tf.Variable(tf.random.normal((1,), dtype=tf.float32), name=f'a{i}')
        bi = tf.Variable(tf.random.normal((1,), dtype=tf.float32), name=f'b{i}')
        parameters.extend([ai, bi])

    @tf.function
    def fourier_model(x):
        result = a0 / tf.constant(2.0)
        for j in range(1, n + 1):
            aj = parameters[1 + 2 * (j - 1)]
            bj = parameters[1 + 2 * (j - 1) + 1]
            c = tf.constant(2 * math.pi * j / p, dtype=tf.float32)
            result = result + aj * tf.math.cos(c * x) + bj * tf.math.sin(c * x)
        return result

    return fourier_model, parameters


def build_linear_model():
    a = tf.Variable(tf.random.normal((1,), dtype=tf.float32), name='a')
    b = tf.Variable(tf.random.normal((1,), dtype=tf.float32), name='b')

    @tf.function
    def linear_model(x):
        return a * x + b

    return linear_model, [a, b]


def format_date_presentation(date_string):
    date_components = date_string.split('/')
    return str(int(date_components[1])) + '/' + date_components[2][2:]


def main():
    data = [('30/09/2020', 19.578),
            ('28/10/2020', 15.360),
            ('30/11/2020', 17.076),
            ('31/12/2020', 32.190),
            ('01/02/2021', 41.986),
            ('27/02/2021', 56.478),
            ('03/04/2021', 43.426),
            ('03/05/2021', 35.586),
            ('18/05/2021', 15.516),
            ('16/06/2021', 15.828),
            ('15/07/2021', 19.118),
            ('17/08/2021', 24.454),
            ('21/09/2021', 18.558),
            ('06/11/2021', 14.190),
            ('16/11/2021', 13.722),
            ('16/12/2021', 20.598),
            ('20/01/2022', 39.346),
            ('16/02/2022', 50.738),
            ('23/03/2022', 34.114),
            ('17/04/2022', 32.918),
            ('18/05/2022', 14.258)]

    dates = []
    values = []

    for date_string, value in data:
        date_components = [int(x) for x in date_string.split('/')]
        dates.append(datetime(year=date_components[2], month=date_components[1], day=date_components[0]))
        values.append(value)

    # ------------------------------------------------

    # time_deltas = []
    # for i in range(2, len(dates)):
    #     time_deltas.append((dates[i] - dates[i - 1]).total_seconds())
    #
    # mu = np.mean(time_deltas)

    # print([poisson.pmf(i, mu=mu/ (60*60*24)) for i in range(30)])
    # print(mu / (60*60*24))
    # exit()

    # ------------------------------------------------

    date_ticks = [format_date_presentation(x[0]) for x in data]
    plt.xticks([i for i in range(len(values))], date_ticks)
    plt.plot(values)
    plt.show()

    fourier_model, fourier_parameters = build_fourier_model(10, 12)
    linear_model, linear_parameters = build_linear_model()
    learning_rate = 0.01

    @tf.function
    def combined_model(x):
        return fourier_model(x) + linear_model(x)

    combined_parameters = fourier_parameters + linear_parameters

    values_x = np.array([i for i in range(len(values))], np.float32)
    values = np.array(values, np.float32)

    for epoch in range(1000):
        print(f'epoch: {epoch}')

        x = tf.constant(values_x)
        y_true = tf.constant(values)

        with tf.GradientTape() as tape:
            y_pred = combined_model(x)
            loss = tf.losses.mse(y_true, y_pred)

        gradients = [float(gradient.numpy().ravel()) for gradient in tape.gradient(loss, combined_parameters)]

        # clip gradients
        for j in range(len(gradients)):
            if gradients[j] > 6.0:
                gradients[j] = 6.0
            if gradients[j] < -6.0:
                gradients[j] = -6.0

        for j in range(len(combined_parameters)):
            parameter = combined_parameters[j]
            parameter_gradient = gradients[j]

            parameter.assign_sub(tf.constant(np.array([parameter_gradient * learning_rate], np.float32)))

        y_pred = combined_model(tf.constant(values_x))
        predictions = y_pred.numpy()
        print(f'loss: {float(tf.losses.mse(values, predictions).numpy())}')

    # ------------------------------------------------

    print(f'forecast for next time step: {float(combined_model(tf.constant(21.0)).numpy())}')

    # ------------------------------------------------

    y_pred = combined_model(tf.constant(values_x))
    predictions = y_pred.numpy()
    plt.plot(values, color='blue')
    plt.plot(predictions, color='red')
    plt.xticks([i for i in range(len(values))], date_ticks)
    plt.legend(['true', 'predictions'], loc="lower right")
    plt.show()

    # ------------------------------------------------

    start_date_components = data[-12][0].split('/')
    current_month = int(start_date_components[1])
    current_year = int(start_date_components[2])

    new_dates = [str(current_month) + '/' + str(current_year)[2:]]
    for i in range(23):
        current_month += 1
        if current_month > 12:
            current_month = 1
            current_year += 1
        new_dates.append(str(current_month) + '/' + str(current_year)[2:])

    plt.xticks([i for i in range(24)], new_dates)
    new_values_x = np.array([19 + i for i in range(13)], np.float32)
    new_predictions = combined_model(tf.constant(new_values_x)).numpy()
    plt.plot(list(range(12)), values[-12:], color='blue')
    plt.plot(list(range(11, 24)), new_predictions, color='red')
    plt.legend(['true', 'predictions'], loc="lower right")
    plt.show()


def fit_and_evaluate_model(training_data,
                           evaluation_data,
                           xgboost_parameters,
                           feature_vector_length=1):
    x_train = []
    y_train = []
    for i in range(feature_vector_length, len(training_data)):
        feature_vector = training_data[i - feature_vector_length:i]
        x_train.append(feature_vector)
        y_train.append([training_data[i]])

    x_train = np.array(x_train, np.float32)
    y_train = np.array(y_train, np.float32)

    model = XGBRegressor(**xgboost_parameters)
    model.fit(x_train, y_train)

    concatenated_evaluation_data = training_data[-feature_vector_length:] + evaluation_data

    x_test = []
    y_test = []
    for i in range(feature_vector_length, len(concatenated_evaluation_data)):
        feature_vector = concatenated_evaluation_data[i - feature_vector_length:i]
        x_test.append(feature_vector)
        y_test.append([concatenated_evaluation_data[i]])

    x_test = np.array(x_test, np.float32)
    y_test = np.array(y_test, np.float32)
    y_hat = model.predict(x_test)

    y_test_flat = y_test.ravel()
    y_hat_flat = y_hat.ravel()
    error = np.mean(np.abs(y_hat_flat - y_test_flat) / y_test_flat) * 100.0

    return model, error


def fit_and_evaluate_model_v2(training_data,
                              evaluation_data,
                              xgboost_parameters,
                              feature_vector_length=1):
    x_train = []
    y_train = []
    for i in range(feature_vector_length, len(training_data)):
        feature_vector = training_data[i - feature_vector_length:i]
        x_train.append(feature_vector)
        y_train.append([training_data[i]])

    x_train = np.array(x_train, np.float32)
    y_train = np.array(y_train, np.float32)

    model = XGBRegressor(**xgboost_parameters)
    model.fit(x_train, y_train)

    concatenated_evaluation_data = training_data[-feature_vector_length:] + evaluation_data

    x_test = []
    y_test = []
    for i in range(feature_vector_length, len(concatenated_evaluation_data)):
        feature_vector = concatenated_evaluation_data[i - feature_vector_length:i]
        x_test.append(feature_vector)
        y_test.append([concatenated_evaluation_data[i]])

    x_test = np.array(x_test, np.float32)
    y_test = np.array(y_test, np.float32)
    y_hat = model.predict(x_test)

    y_test_flat = y_test.ravel()
    y_hat_flat = y_hat.ravel()
    error = np.mean(np.abs(y_hat_flat - y_test_flat) / y_test_flat) * 100.0

    return model, error


def find_optimal_xgboost_model_with_trend(data, evaluation_split=0.2):
    max_feature_vector_length = min(7, len(data) // 2)
    min_moving_average_window_size = 3
    max_moving_average_window_size = min(7, int(round(len(data) * evaluation_split)))
    if max_moving_average_window_size % 2 == 0:
        max_moving_average_window_size -= 1

    best_feature_vector_length = -1
    min_error = sys.float_info.max
    best_smoothed = None
    best_ma_window_size = -1
    best_xgboost_parameters = None

    xgboost_parameters_grid = {
        'learning_rate': [0.01, 0.001],
        'max_depth': [2, 3, 5, 7],
        'min_child_weight': [1, 3, 5, 7],
        'gamma': [0.1, 0.2, 0.3, 0.4],
        'colsample_bytree': [0.1, 0.3, 0.5],
        'n_estimators': [1000, 2000]
    }

    total_parameter_combinations = max_feature_vector_length
    total_parameter_combinations *= (max_moving_average_window_size - min_moving_average_window_size + 1)

    total_parameter_grid_combinations = 1
    parameter_index_key = {}
    parameters_grid_indices = []
    i = 0
    for key in xgboost_parameters_grid:
        n = len(xgboost_parameters_grid[key])
        parameters_grid_indices.append(range(n))
        parameter_index_key[i] = key
        total_parameter_grid_combinations *= n
        i += 1
    parameter_combinations = itertools.product(*parameters_grid_indices)
    total_parameter_combinations *= total_parameter_grid_combinations

    for ma_window_size in range(min_moving_average_window_size, max_moving_average_window_size + 1):
        if ma_window_size % 2 == 0:
            continue

        smoothed = []

        half_ma_window_size = ma_window_size // 2
        for i in range(half_ma_window_size, len(data) - half_ma_window_size):
            average = 0.0
            for j in range(-half_ma_window_size, half_ma_window_size + 1):
                average += data[i + j]
            average /= ma_window_size
            smoothed.append(average)

        evaluation_data_size = int(round(len(smoothed) * evaluation_split))
        training_data_size = len(smoothed) - evaluation_data_size

        smoothed_training_data = smoothed[:training_data_size]
        smoothed_evaluation_data = smoothed[training_data_size:]

        end_pos = half_ma_window_size + training_data_size
        non_smoothed_training_data = data[half_ma_window_size: end_pos]
        non_smoothed_evaluation_data = data[end_pos: end_pos + evaluation_data_size]

        residual_training_data = [x - y for x, y in zip(non_smoothed_training_data, smoothed_training_data)]
        residual_evaluation_data = [x - y for x, y in zip(non_smoothed_evaluation_data, smoothed_evaluation_data)]

        for feature_vector_length in range(1, max_feature_vector_length + 1):
            if feature_vector_length >= len(residual_evaluation_data):
                continue

            for parameter_combination in parameter_combinations:
                xgboost_parameters = {}
                for i in range(len(parameter_combination)):
                    param_key = parameter_index_key[i]
                    xgboost_parameters[param_key] = xgboost_parameters_grid[param_key][parameter_combination[i]]

                model, error = fit_and_evaluate_model(residual_training_data,
                                                      residual_evaluation_data,
                                                      xgboost_parameters,
                                                      feature_vector_length)
                if error < min_error:
                    min_error = error
                    best_feature_vector_length = feature_vector_length
                    best_ma_window_size = ma_window_size
                    best_smoothed = smoothed
                    best_xgboost_parameters = xgboost_parameters

    half_ma_window_size = best_ma_window_size // 2
    a, b = np.polyfit(list(range(half_ma_window_size, len(data) - half_ma_window_size)), best_smoothed, 1)
    residual_training_data = [x - y for x, y in
                              zip(data[half_ma_window_size: len(data) - half_ma_window_size], best_smoothed)]

    x_train = []
    y_train = []
    for i in range(best_feature_vector_length, len(residual_training_data)):
        feature_vector = residual_training_data[i - best_feature_vector_length:i]
        x_train.append(feature_vector)
        y_train.append([residual_training_data[i]])

    x_train = np.array(x_train, np.float32)
    y_train = np.array(y_train, np.float32)

    model = XGBRegressor(**best_xgboost_parameters)
    model.fit(x_train, y_train)

    parameters = {
        'linear_trend_parameters': (a, b),
        'feature_vector_length': best_feature_vector_length,
        'ma_window_size': best_ma_window_size
    }
    parameters.update(best_xgboost_parameters)

    return model, parameters, min_error


def find_optimal_xgboost_model_without_trend(data, evaluation_split=0.2):
    max_feature_vector_length = min(20, len(data) // 2)
    best_feature_vector_length = -1
    min_error = sys.float_info.max
    best_xgboost_parameters = None

    # xgboost_parameters_grid = {
    #     'learning_rate': [0.01, 0.001],
    #     'max_depth': [2, 3, 5, 7],
    #     'min_child_weight': [1, 3, 5, 7],
    #     'gamma': [0.1, 0.2, 0.3, 0.4],
    #     'colsample_bytree': [0.1, 0.3, 0.5, 0.7, 1.0],
    #     'n_estimators': [1000, 2000]
    # }

    xgboost_parameters_grid = {
        'learning_rate': [0.01],
        'max_depth': [7],
        'min_child_weight': [1],
        'gamma': [0.4],
        'colsample_bytree': [1.0],
        'n_estimators': [2000]
    }

    # xgboost_parameters_grid = {
    #     'learning_rate': [0.01],
    #     'max_depth': [7],
    #     'min_child_weight': [1],
    #     'gamma': [0.4],
    #     'colsample_bytree': [0.5],
    #     'n_estimators': [2000]
    # }

    total_parameter_combinations = max_feature_vector_length

    total_parameter_grid_combinations = 1
    parameter_index_key = {}
    parameters_grid_indices = []
    i = 0
    for key in xgboost_parameters_grid:
        n = len(xgboost_parameters_grid[key])
        parameters_grid_indices.append(range(n))
        parameter_index_key[i] = key
        total_parameter_grid_combinations *= n
        i += 1
    total_parameter_combinations *= total_parameter_grid_combinations

    evaluation_data_size = int(round(len(data) * evaluation_split))
    training_data_size = len(data) - evaluation_data_size

    training_data = data[:training_data_size]
    evaluation_data = data[training_data_size:]

    for feature_vector_length in range(1, max_feature_vector_length + 1):
        if feature_vector_length >= len(evaluation_data):
            continue

        parameter_combinations = itertools.product(*parameters_grid_indices)

        for parameter_combination in parameter_combinations:
            xgboost_parameters = {}
            for i in range(len(parameter_combination)):
                param_key = parameter_index_key[i]
                xgboost_parameters[param_key] = xgboost_parameters_grid[param_key][parameter_combination[i]]

            model, error = fit_and_evaluate_model(training_data,
                                                  evaluation_data,
                                                  xgboost_parameters,
                                                  feature_vector_length)
            print(error)

            if error < min_error:
                min_error = error
                best_feature_vector_length = feature_vector_length
                best_xgboost_parameters = xgboost_parameters

    x_train = []
    y_train = []
    for i in range(best_feature_vector_length, len(data)):
        feature_vector = data[i - best_feature_vector_length:i]
        x_train.append(feature_vector)
        y_train.append([data[i]])

    x_train = np.array(x_train, np.float32)
    y_train = np.array(y_train, np.float32)

    model = XGBRegressor(**best_xgboost_parameters)
    model.fit(x_train, y_train)

    parameters = {
        'feature_vector_length': best_feature_vector_length,
    }
    parameters.update(best_xgboost_parameters)

    return model, parameters, min_error


def recursive_multi_step_forecast_with_trend(data, steps, model, parameters):
    ma_window_size = parameters['ma_window_size']
    feature_vector_length = parameters['feature_vector_length']
    a, b = parameters['linear_trend_parameters']

    smoothed = []

    half_ma_window_size = ma_window_size // 2
    for i in range(half_ma_window_size, len(data) - half_ma_window_size):
        average = 0.0
        for j in range(-half_ma_window_size, half_ma_window_size + 1):
            average += data[i + j]
        average /= ma_window_size
        smoothed.append(average)

    smoothed_extrapolation = [a * (len(data) - half_ma_window_size + i) + b for i in range(half_ma_window_size)]

    smoothed_data = smoothed[half_ma_window_size - feature_vector_length:] + smoothed_extrapolation
    non_smoothed_data = data[-feature_vector_length:]
    residual_data = [x - y for x, y in zip(non_smoothed_data, smoothed_data)]

    multi_step_residuals = []
    multi_step_trend = []

    for i in range(steps):
        next_prediction_residual = float(model.predict(np.array([residual_data]))[0])
        next_prediction_linear = a * (len(data) + i) + b

        multi_step_residuals.append(next_prediction_residual)
        multi_step_trend.append(next_prediction_linear)
        residual_data = residual_data[1:] + [next_prediction_residual]

    multi_step_residuals = np.array(multi_step_residuals, np.float32)
    multi_step_trend = np.array(multi_step_trend, np.float32)

    return list(multi_step_residuals + multi_step_trend)


def recursive_multi_step_forecast_without_trend(data, steps, model, parameters):
    feature_vector_length = parameters['feature_vector_length']

    forecast_data = data[-feature_vector_length:]
    multi_step = []train_and_evaluate_final_model

    for i in range(steps):
        next_prediction = float(model.predict(np.array([forecast_data]))[0])
        multi_step.append(next_prediction)
        forecast_data = forecast_data[1:] + [next_prediction]

    return multi_step


def test_xgboost():
    # start_date = datetime(2020, 1, 1)
    # dates = [start_date]
    #
    # current_date = start_date
    # for i in range(1, 7000):
    #     current_date += timedelta(days=2 * i + random.randint(-2, 2))
    #     dates.append(current_date)
    #
    # for date in dates:
    #     print(date)

    # dates = [datetime(2020, 1, 15),
    #          datetime(2020, 8, 1),
    #          datetime(2020, 9, 30),
    #          datetime(2021, 1, 15),
    #          datetime(2021, 8, 1),
    #          datetime(2021, 9, 30),
    #          datetime(2022, 1, 15),
    #          datetime(2022, 8, 1),
    #          datetime(2022, 9, 30),
    #          datetime(2023, 1, 15),
    #          datetime(2023, 8, 1),
    #          datetime(2023, 9, 30),
    #          datetime(2024, 1, 15),
    #          datetime(2024, 8, 1),
    #          datetime(2024, 9, 30),
    #          datetime(2025, 1, 15),
    #          datetime(2025, 8, 1),
    #          datetime(2025, 9, 30)]

    # time_deltas = []
    #
    # for i in range(1, len(dates)):
    #     time_delta = dates[i] - dates[i - 1]
    #     time_deltas.append(time_delta.days)

    time_deltas = []

    for i in range(1, 10000):
        time_deltas.append(3 * i + random.randint(-2, 2))

    start_time = time.time()
    model, parameters, error = find_optimal_xgboost_model_with_trend(time_deltas)
    end_time = time.time()

    print(f'model error: {error}')
    print(f'training time: {end_time - start_time} seconds')

    next_predictions = recursive_multi_step_forecast_with_trend(time_deltas, 5, model, parameters)

    # current_total = 0
    # for next_prediction in next_predictions:
    #     current_total += int(round(next_prediction))
    #     print('next date:', dates[-1] + timedelta(days=current_total))
    #
    plt.plot(list(range(len(time_deltas))), time_deltas, color='blue')
    plt.plot(list(range(len(time_deltas), len(time_deltas) + 5)), next_predictions, color='red')
    plt.show()


def test_data_set():
    users_data = dict()

    if not os.path.isfile('data.pickle'):
        progress_bar = tqdm(total=1056321)
        with open('/home/u764/Downloads/trans.csv', 'r') as f:
            f.readline()  # ignore header
            progress_bar.update()
            line = f.readline()
            while line != '':
                row = line.split(';')
                if len(row) < 6:
                    line = f.readline()
                    progress_bar.update()
                    continue
                user_id = int(row[1])
                transaction_date = int(row[2])
                transaction_type = row[4].strip().upper()
                transaction_amount = float(row[5])
                if user_id not in users_data:
                    users_data[user_id] = dict()
                    user_dict = users_data[user_id]
                else:
                    user_dict = users_data[user_id]
                if transaction_type not in user_dict:
                    user_dict[transaction_type] = dict()
                if transaction_date not in user_dict[transaction_type]:
                    user_dict[transaction_type][transaction_date] = 0.0
                user_dict[transaction_type][transaction_date] += transaction_amount
                progress_bar.update()
                line = f.readline()
        progress_bar.close()

        with open('data.pickle', 'wb') as f:
            pickle.dump(users_data, f)
    else:
        with open('data.pickle', 'rb') as f:
            users_data = pickle.load(f)

    user_data = users_data[[*users_data][2]]
    transaction_data = user_data[[*user_data][0]]

    dates_list = [*transaction_data]
    dates_list.sort()

    xs = []
    ys = []
    for i in range(len(dates_list)):
        xs.append(i)
        ys.append(transaction_data[dates_list[i]])

    plt.plot(xs, ys)
    plt.show()

    print(xs)
    print(ys)

    start_time = time.time()
    model, parameters, error = find_optimal_xgboost_model_without_trend(ys)
    end_time = time.time()
    print(f'time: {end_time - start_time}')
    print(f'time: {end_time - start_time}')
    print(f'error: {error}')
    print('parameters:')
    print(json.dumps(parameters, indent=4))

    next_predictions = recursive_multi_step_forecast_without_trend(ys, 30, model, parameters)
    plt.plot(xs, ys, color='blue')
    plt.plot(list(range(len(dates_list), len(dates_list) + 30)), next_predictions, color='red')
    plt.show()


def test_ecc_data_set():
    account_payments = dict()
    totals = dict()

    with open('/home/u764/Downloads/first_export.csv', 'r') as f:
        f.readline()  # ignore header
        line = f.readline()
        while line != '':
            row = line.split(',')
            if len(row) < 6:
                line = f.readline()
                continue

            if '-' not in row[12]:
                line = f.readline()
                continue

            account_number = row[6].strip().upper()
            cheque_date = datetime.strptime(row[12].strip(), '%d-%b-%y')
            cheque_amount = float(row[11])

            if account_number not in account_payments:
                account_payments[account_number] = dict()
            if cheque_date not in account_payments[account_number]:
                account_payments[account_number][cheque_date] = 0.0
            if cheque_date not in totals:
                totals[cheque_date] = 0.0
            account_payments[account_number][cheque_date] += cheque_amount
            totals[cheque_date] += cheque_amount
            line = f.readline()

    # account_data = account_payments[0]

    dates_list = [*totals]
    dates_list.sort()
    print(dates_list)
    exit()
    first_date = dates_list[0].replace(day=1)
    last_date = dates_list[-1]
    days_in_month = calendar.monthrange(last_date.year, last_date.month)[1]
    last_date = last_date.replace(day=days_in_month)

    xs = []
    ys = []
    i = 0
    current_date = first_date
    while current_date <= last_date:
        days_in_month = calendar.monthrange(current_date.year, current_date.month)[1]

        current_sum = 0.0
        for j in range(1, 11):
            date_time = datetime(year=current_date.year, month=current_date.month, day=j)
            if date_time in totals:
                current_sum += totals[date_time]
        xs.append(i)
        ys.append(current_sum)
        i += 1

        current_sum = 0.0
        for j in range(11, days_in_month - 10):
            date_time = datetime(year=current_date.year, month=current_date.month, day=j)
            if date_time in totals:
                current_sum += totals[date_time]
        xs.append(i)
        ys.append(current_sum)
        i += 1

        current_sum = 0.0
        for j in range(days_in_month - 10, days_in_month + 1):
            date_time = datetime(year=current_date.year, month=current_date.month, day=j)
            if date_time in totals:
                current_sum += totals[date_time]
        xs.append(i)
        ys.append(current_sum)
        i += 1

        next_year = current_date.year
        next_month = current_date.month + 1
        if next_month > 12:
            next_month = 1
            next_year += 1
        current_date = datetime(year=next_year, month=next_month, day=1)

    xs = xs[:15]
    ys = ys[:15]
    plt.xticks(list(range(1, len(xs) + 1)))
    plt.plot(np.array(xs) + 1, ys)
    plt.show()

    print(xs)
    print(ys)

    start_time = time.time()
    model, parameters, error = find_optimal_xgboost_model_without_trend(ys)
    end_time = time.time()
    print(f'time: {end_time - start_time}')
    print(f'time: {end_time - start_time}')
    print(f'error: {error}')
    print('parameters:')
    print(json.dumps(parameters, indent=4))

    next_predictions = recursive_multi_step_forecast_without_trend(ys, 30, model, parameters)
    plt.plot(xs, ys, color='blue')
    plt.plot(list(range(len(dates_list), len(dates_list) + 30)), next_predictions, color='red')
    plt.show()


if __name__ == '__main__':
    # main()
    # test_xgboost()
    test_data_set()
    # test_ecc_data_set()
