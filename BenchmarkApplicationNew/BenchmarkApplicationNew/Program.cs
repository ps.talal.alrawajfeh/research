﻿using System.Diagnostics;
using Progressoft.AppUtils.Core.CodeDesign;
using Progressoft.Drawing.Core;

namespace BenchmarkApplicationNew;

internal class Program
{
    static void Main(string[] args)
    {
        // dotnet nuget add source https://gitlab.com/api/v4/projects/21171357/packages/nuget/index.json -n Apollo -u nuget_server -p GqwU1qysgLEFLRxRWQfj --store-password-in-clear-text
        
        var atbDocumentDeNoisingService = new AtbDocumentDeNoisingService();

        // using var bitmap = new Bitmap("./Page 1.png");
        // bitmap.Save(out byte[] buffer);
        // var stopwatch = new Stopwatch();
        //
        // stopwatch.Start();
        //
        // int limit = 100;
        // Result<byte[]> deNoise = Result<byte[]>.Fail("");
        // for (int i = 0; i < limit; i++)
        // {
        //     deNoise = atbDocumentDeNoisingService.DeNoise(buffer, "hello");
        // }
        //
        // stopwatch.Stop();
        // var stopwatchElapsedMilliseconds = stopwatch.ElapsedMilliseconds;
        // var inferenceTime = ((double) stopwatchElapsedMilliseconds) / ((double) limit);
        // Console.WriteLine($"Time: {(long) inferenceTime}");
        //
        // new Bitmap(deNoise.Value).Save("out.bmp");

        for(int i = 1; i < 1001; i++)
        {
            var image = new Bitmap($"/home/u764/Downloads/cheques/cheque_{i}.png");
            image.Save(out byte[] buffer);
            var deNoised = new Bitmap(atbDocumentDeNoisingService.DeNoise(buffer, "hello").Value);
            deNoised.Save($"/home/u764/Downloads/outputs/cheque_{i}_denoised.png");
        }

        // if (!File.Exists("autoencoder.xml"))
        // {
        //     Console.WriteLine("model does not exist");
        //     return;
        // }
        //
        // var modelId = "densenet201";
        // var modelInfoResult = OpenVinoModelInfo.Create(modelId,
        //     File.ReadAllText("autoencoder.xml"),
        //     File.ReadAllBytes("autoencoder.bin"));
        //
        // if (modelInfoResult.IsFailure)
        // {
        //     Console.WriteLine("Error creating model info");
        //     Console.WriteLine(modelInfoResult.ErrorException);
        //     return;
        // }
        //
        // var modelInfo = modelInfoResult.Value;
        // var registerModelResult = OpenVinoApi.RegisterModel(modelInfo, OpenVinoDevice.CPU);
        //
        // if (registerModelResult.IsFailure)
        // {
        //     Console.WriteLine("Error registering model");
        //     Console.WriteLine(modelInfoResult.ErrorException);
        //     return;
        // }
        //
        //
        // List<OpenVinoTensor> tensors = new List<OpenVinoTensor>();
        //
        // float[][][][] data = (float[][][][]) ArrayUtils.CreateArray<float>(1, 200, 748, 1);
        //
        // tensors.Add(OpenVinoTensor.Create("input_1:0", data).Value);
        //
        // var stopwatch = new Stopwatch();
        //
        // stopwatch.Start();
        //
        // int limit = 1000;
        // for (int i = 0; i < limit; i++)
        // {
        //     var inferenceResult = OpenVinoApi.Infer(modelId, OpenVinoTensorMap.Create(tensors).Value);
        //
        //     if (inferenceResult.IsFailure)
        //     {
        //         Console.WriteLine("Error inferring result");
        //         Console.WriteLine(modelInfoResult.ErrorException);
        //         return;
        //     }
        //
        //     var output = inferenceResult.Value;
        //     if (output.TensorMap.Count != 1)
        //     {
        //         throw new Exception("Expected 1 inference output");
        //     }
        // }
        //
        // stopwatch.Stop();
        // var stopwatchElapsedMilliseconds = stopwatch.ElapsedMilliseconds;
        // var inferenceTime = ((double) stopwatchElapsedMilliseconds) / ((double) limit);
        // Console.WriteLine($"Time: {(long) inferenceTime}");
    }
}