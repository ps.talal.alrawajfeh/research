using System.Drawing;
using Progressoft.AppUtils.Core.CodeDesign;
using Progressoft.AppUtils.Core.Linq;
using Progressoft.AppUtils.Core.Validations;
using Progressoft.Drawing.Core;
using Progressoft.Imaging.Core;
using Progressoft.ML.ImageDenoising.Core.Documents;
using Progressoft.ML.ImageDenoisingNet.Core.Documents;
using Progressoft.ML.NeuralNetwork.Core;

namespace BenchmarkApplicationNew;

public class AtbDocumentDeNoisingService
{
    private const string OPEN_VINO_NOT_SUPPORTED = "Openvino is not supported.";
    private const string NULL_OR_EMPTY_DOCUMENT = "The given document image is null or empty";
    private const string NULL_OR_WHITESPACED_TEMPLATE_ID = "The given template-id is null or whitespaced";

    private const string INPUT_TENSOR_NAME = "input_1";
    private const string OUTPUT_TENSOR_NAME = "conv2d_18/Sigmoid";
    private const int INPUT_SHAPE_WIDTH = 400;
    private const int INPUT_SHAPE_HEIGHT = 200;
    private const string TF_MODEL_FILE_NAME = "ChequeClearing_enc.pb";
    private const string ONNX_MODEL_FILE_NAME = "ChequeClearing_enc.onnx";
    private const string XML_MODEL_FILE_NAME = "ChequeClearing_enc.xml";
    private const string BIN_MODEL_FILE_NAME = "ChequeClearing_enc.bin";

    private readonly DocDenoisingNet _docDenoisingNet;

    public AtbDocumentDeNoisingService()
    {
        _docDenoisingNet = new DocDenoisingNet(CreateNetworkOptions(PredictionEnginePlatform.TensorFlow));
    }

    public Result<byte[]> DeNoise(byte[] document, string templateId)
    {
        var validationResult = ValidationTrain.Create()
            .ShouldNotBe(() => document.NullOrEmpty(), NULL_OR_EMPTY_DOCUMENT)
            .ShouldNotBe(() => string.IsNullOrWhiteSpace(templateId), NULL_OR_WHITESPACED_TEMPLATE_ID)
            .Status;

        if (validationResult.IsFailure)
            return validationResult.BindFail<byte[]>();


        var conversionResult = ConvertDocumentBytesToImage(document);
        if (conversionResult.IsFailure)
            return conversionResult.BindFail<byte[]>();

        var convertTo8BitsResult = ConvertTo8Bits(conversionResult.Value);
        conversionResult.Value.Dispose();
        if (convertTo8BitsResult.IsFailure)
            return convertTo8BitsResult.BindFail<byte[]>();

        var denoisingInputResult = CreateInput(convertTo8BitsResult.Value);
        convertTo8BitsResult.Value.Dispose();
        if (denoisingInputResult.IsFailure)
            return denoisingInputResult.BindFail<byte[]>();
        var predictionResult = _docDenoisingNet.Predict(denoisingInputResult.Value);
        denoisingInputResult.Value.Dispose();
        if (predictionResult.IsFailure)
            return predictionResult.BindFail<byte[]>();

        var imageToBytesResult = ConvertDocumentImageToBytes(predictionResult.Value.DocumentImage);
        predictionResult.Value.Dispose();
        if (imageToBytesResult.IsFailure)
            return imageToBytesResult.BindFail<byte[]>();

        return Result<byte[]>.Success(imageToBytesResult.Value);
    }

    private Result<DocDenoisingNetInput> CreateInput(Bitmap document)
    {
        Bitmap resized;
        try
        {
            if (document.Size.Width > 1000 && document.Size.Height > 500)
            {
                resized = GeometricTransforms.ResizeBiCubic(document,
                    0.5,
                    0.5);
            }
            else
            {
                resized = document.Clone() as Bitmap;
            }
        }
        catch (Exception ex)
        {
            return Result<DocDenoisingNetInput>.Fail(ex.Message);
        }

        return DocDenoisingNetInput.Create(resized,
            new Rectangle(resized.Width - 400,
                resized.Height - 200,
                400,
                200),
            false);
    }

    private static Result<Bitmap> ConvertDocumentBytesToImage(byte[] document)
    {
        Bitmap documentImage;
        try
        {
            documentImage = new Bitmap(document);
        }
        catch (Exception ex)
        {
            return Result<Bitmap>.Fail(ex);
        }

        return Result<Bitmap>.Success(documentImage);
    }

    private static Result<byte[]> ConvertDocumentImageToBytes(Bitmap document)
    {
        byte[] documentBytes;
        try
        {
            document.Save(out byte[] buffer, ImageFormat.PNG);
            documentBytes = buffer;
        }
        catch (Exception ex)
        {
            return Result<byte[]>.Fail(ex);
        }

        return Result<byte[]>.Success(documentBytes);
    }

    private Result<Bitmap> ConvertTo8Bits(Bitmap document)
    {
        try
        {
            return Result<Bitmap>.Success(ColorDepthConversion.ConvertTo8BitsByAveraging(document));
        }
        catch (Exception ex)
        {
            return Result<Bitmap>.Fail(ex);
        }
    }

    private static DocDenoisingNetOptions CreateNetworkOptions(
        PredictionEnginePlatform predictionPlatform)
    {
        var preProcessor = (DocDenoisingNetInput input) =>
        {
            Bitmap binaryImage = Binarization.OpenCvOtsu(input.DocumentImage);
            Bitmap bmpReadyToScale = DocDenoisingNetUtils.CropWorkingArea(input.WorkingArea, input.DocumentImage);
            Bitmap finalInput = GeometricTransforms.ResizeBiCubic(bmpReadyToScale,
                INPUT_SHAPE_WIDTH,
                INPUT_SHAPE_HEIGHT);
            bmpReadyToScale.Dispose();
            return ((object) binaryImage, finalInput);
        };

        var postProcessor = (
            Bitmap bmpNetOutput,
            DocDenoisingNetInput input, object payload) =>
        {
            Bitmap binaryImage = payload as Bitmap;
            Bitmap bmpNetScalingInverse =
                GeometricTransforms.ResizeBiCubic(bmpNetOutput,
                    input.WorkingArea.Width,
                    input.WorkingArea.Height);
            Bitmap bmpNetRestored = DocDenoisingNetUtils.RestoreWorkingArea(bmpNetScalingInverse,
                input.WorkingArea,
                binaryImage,
                out Rectangle adjustedWorkingArea);
            Bitmap finalOutput = Binarization.OpenCvOtsu(bmpNetRestored);
            bmpNetScalingInverse.Dispose();
            bmpNetRestored.Dispose();
            return DenoisingPostProcessingOutput.Create(
                finalOutput,
                binaryImage,
                adjustedWorkingArea
            ).Value;
        };

        switch (predictionPlatform)
        {
            case PredictionEnginePlatform.TensorFlow:
                return DocDenoisingNetOptions.CreateTensorflow(TF_MODEL_FILE_NAME,
                    INPUT_TENSOR_NAME,
                    OUTPUT_TENSOR_NAME,
                    new Size(INPUT_SHAPE_WIDTH, INPUT_SHAPE_HEIGHT),
                    preProcessor,
                    postProcessor).Value;

            case PredictionEnginePlatform.Onnx:
                return DocDenoisingNetOptions.CreateOnnx(ONNX_MODEL_FILE_NAME,
                    INPUT_TENSOR_NAME,
                    OUTPUT_TENSOR_NAME,
                    new Size(INPUT_SHAPE_WIDTH, INPUT_SHAPE_HEIGHT),
                    preProcessor,
                    postProcessor).Value;

            case PredictionEnginePlatform.OpenVino:
                return DocDenoisingNetOptions.CreateOpenVino(XML_MODEL_FILE_NAME,
                    BIN_MODEL_FILE_NAME,
                    INPUT_TENSOR_NAME,
                    OUTPUT_TENSOR_NAME,
                    new Size(INPUT_SHAPE_WIDTH, INPUT_SHAPE_HEIGHT),
                    preProcessor,
                    postProcessor).Value;

            default:
                throw new NotImplementedException(OPEN_VINO_NOT_SUPPORTED);
        }
    }
}