#pragma once

#include <string>
#include <stdio.h>
#include "Model.h"
#include "Utilities.h"
#include <inference_engine.hpp>

using namespace std;
using namespace Utilities;
using namespace InferenceEngine;

class ModelInfo
{
public:
	string name;
	Model model;

	bool operator==(const ModelInfo &obj2) const
	{
		return (trim_copy(to_lower(this->name)) == trim_copy(to_lower(obj2.name)));
	}

	ModelInfo(string name, InferenceEngine::Model &model)
	{
		this->name = name;
		this->model = model;
	}
};
