#pragma once
#include <string>
#include <stdio.h>
#include <inference_engine.hpp>
#include "Utilities.h"

using namespace std;
using namespace Utilities;

namespace InferenceEngine {

	class Model {
	public:
		Core IECore();
		CNNNetwork Network();
		ExecutableNetwork ExecutableNet();

		Model() {
		}

		static Model Create(const string& xmlPath,
			const string& binPath, const string& deviceName, string& errorDesc);

	private:
		Core _ieCore;
		CNNNetwork _network;
		ExecutableNetwork _executableNet;

		Model(Core& core, CNNNetwork& network, ExecutableNetwork& executableNet) {
			_ieCore = core;
			_network = network;
			_executableNet = executableNet;
		}

	};
}
