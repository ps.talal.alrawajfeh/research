#include "Interface.h"
using namespace std;

char* RegisterModel(const char* pModelName, const char* pXmlPath, const char* pBinPath, const char* pDeviceName)
{
	string _operationResult = ModelsManager::CreateAndRegisterModel(string(pModelName),
															string(pXmlPath),
															string(pBinPath),
															string(pDeviceName));
		
	return strdup(_operationResult.c_str());
}

char* UnregisterModel(const char* pModelName) {
	string _operationResult = ModelsManager::UnregisterModel(string(pModelName));
	return strdup(_operationResult.c_str());
}

bool DoesModelNameExist(const char* pModelName) {
	return ModelsManager::ModelNameExist(string(pModelName));
}

/*
- ModelId,
	- [IN,OUT] string[] inputNames, //the list of input names
	- [IN,OUT] int[] inputSizes,  //the size of each input value
	- [IN] int inputCount; //the count of the inputs of the network

	- [IN,OUT] float[] inputValues, //the concatenated input values
	- int totalInputValueSize, //the total size of the inputValues , you can detect the batch size: totalInputValueSize/sum(inputSizes)

		- [out] string[] OutputNames //the list of output names
	- [out] int[] OutputSizes //the size of each output value
	- [out] int outputCount; //the count of the outputs of the network

	- [out] float[] InferResult //the concatenated output values
	- [out] int totalOutputsizes //the total size of the outputValues , you can detect the batch size: totalOutputValueSize/sum(OutputSizes)

	outputDimensionsCounts :: [3]
	outputDimesions:: [200,748,1]

*/

char* Infer(char* pModelName, char** inputNames, int* inputSizes, int inputCount, float* inputValues, int totalInputSizes,
	char*& outputNames, int*& outputDimesions, int*& outputDimensionsCounts, int& outputCount, float*& inferResult, int& totalOutputSizes)
{
	
	Batch input;
	int inputSizesSummation = 0;
	for (int x = 0; x < inputCount; x++) inputSizesSummation += inputSizes[x];
	input.TensorMapsSize = totalInputSizes / inputSizesSummation;
	input.TensorMaps = new TensorMap[input.TensorMapsSize];
	int globalInputsCounter = 0;
	for (int x = 0; x < input.TensorMapsSize; x++)
	{
		input.TensorMaps[x].TensorsSize = inputCount;
		input.TensorMaps[x].Tensors = new Tensor[inputCount];

		for (int y = 0; y < inputCount; y++) { //foreach tensor
			input.TensorMaps[x].Tensors[y].Name = string(inputNames[y]);
			input.TensorMaps[x].Tensors[y].ValueSize = inputSizes[y];
			input.TensorMaps[x].Tensors[y].Value = new float[inputSizes[y]];
			for (int i = 0; i < inputSizes[y]; i++)
				input.TensorMaps[x].Tensors[y].Value[i] = inputValues[globalInputsCounter++];
		}
	}
	Batch output;
	string modelName = string(pModelName);
	string _operationResult = ModelsManager::Infer(modelName, input, output);
	if (_operationResult != "") return strdup(_operationResult.c_str());

	//char*& outputNames, int*& outputDimesions, int*& outputDimensionsCounts, int& outputCount, 
	//float*& inferResult, int& totalOutputSizes
	if (output.TensorMapsSize < 1) return strdup(string("The infer did not return any output.").c_str());
	string concatOutputName ="";
	
	outputDimensionsCounts = new int[output.TensorMaps[0].TensorsSize];
	outputCount = output.TensorMaps[0].TensorsSize;
	int singleTensorMapSize = 0;
	int totalDimsCount = 0;
	vector<int> tempDimsValues;
	for (int x = 0; x < output.TensorMaps[0].TensorsSize; x++) {		
		concatOutputName += output.TensorMaps[0].Tensors[x].Name + ";";		
		singleTensorMapSize += output.TensorMaps[0].Tensors[x].ValueSize;
		outputDimensionsCounts[x] = output.TensorMaps[0].Tensors[x].Dims.size();
		totalDimsCount += output.TensorMaps[0].Tensors[x].Dims.size();
		for (int y = 0; y < output.TensorMaps[0].Tensors[x].Dims.size(); y++)
			tempDimsValues.push_back(output.TensorMaps[0].Tensors[x].Dims[y]);
	}
	//define the outputDimesions
	outputDimesions = new int[tempDimsValues.size()];
	for (int x = 0; x < tempDimsValues.size(); x++)
		outputDimesions[x] = tempDimsValues[x];
	tempDimsValues.clear();

	outputNames = strdup(concatOutputName.c_str());
	totalOutputSizes = singleTensorMapSize * output.TensorMapsSize;
	inferResult = new float[totalOutputSizes];
	int globalResultValuesCounter = 0;
	for (int batch = 0; batch < output.TensorMapsSize; batch++) {
		for (int tm = 0; tm < output.TensorMaps[batch].TensorsSize; tm++) {
			for (int t = 0; t < output.TensorMaps[batch].Tensors[tm].ValueSize; t++)
				inferResult[globalResultValuesCounter++] = output.TensorMaps[batch].Tensors[tm].Value[t];
		}
	}

	for (int batch = 0; batch < output.TensorMapsSize; batch++) {
		for (int tm = 0; tm < output.TensorMaps[batch].TensorsSize; tm++) {
				delete[] output.TensorMaps[batch].Tensors[tm].Value;		
		}
		delete[] output.TensorMaps[batch].Tensors;
	}
	delete[] output.TensorMaps;

	for (int batch = 0; batch < input.TensorMapsSize; batch++) {
		for (int tm = 0; tm < input.TensorMaps[batch].TensorsSize; tm++) {
			delete[] input.TensorMaps[batch].Tensors[tm].Value;
		}
		delete[] input.TensorMaps[batch].Tensors;
	}
	delete[] input.TensorMaps;
	
	return strdup(_operationResult.c_str());


}



void ReleaseMemory(float*& inputArray) {
	delete[] inputArray;	
}