#include "Inference.h"
using namespace InferenceEngine;

#define TENSOR_NUMBER unsigned long long

bool DoesInputInfoExistInModelAndBatch(Model& model, Batch& input)
{
	//get all input names in model
	vector<string> vecModelInputNames;
	vector<string> vecBatchInputNames;
	for (const auto& p : model.Network().getInputsInfo())
		vecModelInputNames.push_back(p.first);
	if (input.TensorMapsSize < 1) return false;

	for (int y = 0; y < input.TensorMaps[0].TensorsSize; y++)
		vecBatchInputNames.push_back(input.TensorMaps[0].Tensors[y].Name);

	return vectors_has_same_contents(vecModelInputNames, vecBatchInputNames);
}

vector<string> GetInputNames(const Batch& input)
{
	vector<string> names;
	for (int i = 0; i < input.TensorMaps[0].TensorsSize; i++)
		names.push_back(input.TensorMaps[0].Tensors[i].Name);
	return names;
}

int GetValuesSizeBasedOnInput(const string& inputName, const Batch& input)
{
	for (int t = 0; t < input.TensorMaps[0].TensorsSize; t++)
	{
		if (string_equals(to_lower(input.TensorMaps[0].Tensors[t].Name), to_lower(inputName)))
			return input.TensorMaps[0].Tensors[t].ValueSize;
	}
}

//void ExtractInputValuesFromBatch(string inputName, const Batch& input,
//			TENSOR_NUMBER channel, TENSOR_NUMBER width, TENSOR_NUMBER height,
//			float*& extractedValues, int& extractedCount)
//{
//	int batchCount = input.TensorMapsSize;
//	extractedCount = batchCount * GetValuesSizeBasedOnInput(inputName, input);
//	extractedValues = new float[extractedCount];
//	for (int batchIndex = 0; batchIndex < batchCount; batchIndex++) {
//		for (int t = 0; t < input.TensorMaps[batchIndex].TensorsSize; t++)
//		{
//			if (string_equals(to_lower(input.TensorMaps[batchIndex].Tensors[t].Name), to_lower(inputName))) {
//				int s = input.TensorMaps[batchIndex].Tensors[t].ValueSize;
//				//from HWC to CHW :: (h*width*channel) + (w*channel) + c
//				int i = 0;
//				for (int h = 0; h < height; h++)
//				{
//					for (int w = 0; w < width; w++)
//					{
//						for (int c = 0; c < channel; c++) {
//							extractedValues[i++] = 
//								input.TensorMaps[batchIndex].Tensors[t].Value[(h * width * channel) + (w * channel) + c];
//						}
//					}
//				}
//				/*for (int i = 0; i < s; i++)
//					extractedValues[ctr++] = input.TensorMaps[batchIndex].Tensors[t].Value[i];*/
//			}
//		}
//	}
//}


void ExtractInputValuesFromBatch(string inputName, const Batch& input, int batchIndex,
	TENSOR_NUMBER channel, TENSOR_NUMBER width, TENSOR_NUMBER height, float*& extractedValues)
{
	int extractedCount = GetValuesSizeBasedOnInput(inputName, input);
	extractedValues = new float[extractedCount];
	TENSOR_NUMBER batchStartIndex = batchIndex * width * height * channel;
	int tensorSize = input.TensorMaps[batchIndex].TensorsSize;
	int widthTimesHeight = width * height;
	for (int t = 0; t < tensorSize; t++)
	{
		if (string_equals(to_lower(input.TensorMaps[batchIndex].Tensors[t].Name), to_lower(inputName))) {			
			//from HWC to CHW :: (h*width*channel) + (w*channel) + c
			int i = 0;
			for (int h = 0; h < height; h++)
			{
				int hTimesWidth = h * width;
				int hTimesWidthTimesChannelsCount = hTimesWidth * channel;
				for (int w = 0; w < width; w++)
				{
					int hTimesWidthPlusWidth = hTimesWidth + w;
					int wTimesChannel = w * channel;
					for (int c = 0; c < channel; c++) {
						extractedValues[(c * widthTimesHeight) + hTimesWidthPlusWidth] =
							input.TensorMaps[batchIndex].Tensors[t].Value[hTimesWidthTimesChannelsCount + wTimesChannel + c];
					}
				}
			}

		}
	}

}


Batch InitializeOutputTensors(Batch& input, int outputTensorMapCount)
{
	Batch output;
	output.TensorMapsSize = input.TensorMapsSize;
	output.TensorMaps = new TensorMap[output.TensorMapsSize];
	for (int x = 0; x < output.TensorMapsSize; x++)
	{
		TensorMap tm;
		tm.TensorsSize = outputTensorMapCount;
		tm.Tensors = new Tensor[outputTensorMapCount];
		output.TensorMaps[x] = tm;
	}
	return output;
}

InferenceEngine::TensorDesc CreateTensorDescription(vector<size_t> dims, string input_name, int batchIndex, 
											int inputIndex, Batch& input, float*& inputValues) {

	InferenceEngine::TensorDesc tensorDesc;
	if (dims.size() == 4) {
		TENSOR_NUMBER channel = dims[1];
		TENSOR_NUMBER height = dims[2];
		TENSOR_NUMBER width = dims[3];
		
		ExtractInputValuesFromBatch(input_name, input, batchIndex, channel, height, width, inputValues);
		tensorDesc = InferenceEngine::TensorDesc(InferenceEngine::Precision::FP32,
			{ 1, //batch
			channel, height,width },
			InferenceEngine::Layout::NHWC); //Batch, height, width, channel

	}
	else if (dims.size() == 2) {
		TENSOR_NUMBER scalarLength = dims[1];
		inputValues = new float[scalarLength];

		memcpy(inputValues, input.TensorMaps[batchIndex].Tensors[inputIndex].Value, scalarLength * sizeof(float));
		tensorDesc = InferenceEngine::TensorDesc(InferenceEngine::Precision::FP32,
			{ 1, scalarLength },
			InferenceEngine::Layout::NC); //Batch, scalar value
	}
	else if (dims.size() == 1) {
		TENSOR_NUMBER scalarLength = dims[0];
		inputValues = new float[scalarLength];

		memcpy(inputValues, input.TensorMaps[batchIndex].Tensors[inputIndex].Value, scalarLength * sizeof(float));
		tensorDesc = InferenceEngine::TensorDesc(InferenceEngine::Precision::FP32,
			{ scalarLength },
			InferenceEngine::Layout::SCALAR);
	}
	else {
		throw std::logic_error("CreateTensorDescription failed: Input dims count not implemented");
	}
	return tensorDesc;
}

Tensor CreateOutputTensor(string tensorName, vector<size_t> dims, float* output_data)
{
	Tensor tensor;
	tensor.Name = tensorName;
	if (dims.size() == 4)
	{
		size_t batchSize = dims[0];
		size_t channels = dims[1];
		size_t height = dims[2];
		size_t width = dims[3];

		tensor.Dims.push_back(height);
		tensor.Dims.push_back(width);
		tensor.Dims.push_back(channels);

		tensor.ValueSize = height * width * channels;
		tensor.Value = new float[tensor.ValueSize];
		int tensor_index_ctr = 0;
		for (int h = 0; h < height; h++)
		{
			for (int w = 0; w < width; w++)
			{
				for (int c = 0; c < channels; c++) {
					tensor.Value[tensor_index_ctr++] = output_data[(c * width * height) + (h * width) + w];
				}
			}
		}
	}
	else if (dims.size() == 2)
	{
		size_t outputLength = dims[1];
		tensor.Dims.push_back(outputLength);

		tensor.Value = new float[outputLength];
		tensor.ValueSize = outputLength;
		for (int x = 0; x < outputLength; x++)
			tensor.Value[x] = output_data[x];
	}
	else {
		throw std::logic_error("FillOutputIntoTensor failed: Output dims count not implemented");
	}

	return tensor;
}

Batch Inference::Infer(Model& model, Batch& input) {
	
	if (!DoesInputInfoExistInModelAndBatch(model, input)) 
		throw std::logic_error("Input batch has different inputs from the model.");
	
	vector<string> inputNames = GetInputNames(input);
	
	//----------------------Define output info-----------------------------------------
	Batch output = InitializeOutputTensors(input, (int)model.Network().getOutputsInfo().size());

	//--------------------- Run one batch at time -----------------------
	for (int batchIndex = 0; batchIndex < input.TensorMapsSize; batchIndex++)
	{
		//------------------------------- 1. Prepare input ----------------------------------
		model.Network().setBatchSize(1);
		BlobMap inputMap;
		float** inputValues = new float* [inputNames.size()];
		
		int inputNameCounter = 0;
		
		for (const auto& p : model.Network().getInputsInfo())
		{
			pair<string, Blob::Ptr> tmp;
			tmp.first = p.first; //name			
			auto dims = p.second->getTensorDesc().getDims();

			InferenceEngine::TensorDesc tensorDesc = CreateTensorDescription(dims,
							p.first, batchIndex, inputNameCounter, input, inputValues[inputNameCounter]);
			
			

			tmp.second = InferenceEngine::make_shared_blob<float>(tensorDesc, inputValues[inputNameCounter]);
			p.second->setPrecision(Precision::FP32);
			inputMap.insert(tmp);
			inputNameCounter++;
		}

		model.Network().getOutputsInfo().begin()->second->setPrecision(Precision::FP32);
		
		//--------------------------- 2- Infer --------------------------------------	
		InferRequest infer_request = model.ExecutableNet().CreateInferRequest();
		infer_request.SetInput(inputMap);
		infer_request.Infer();

		//--------------------------3- delete input data ----------------------------

		for (int x = 0; x < inputNames.size(); x++)
			delete[] inputValues[x];
		delete[] inputValues;

		//-------------------------4- Get output values --------------------------------------------

		int tensorMapCounter = 0;
		
		for (const auto& p : model.Network().getOutputsInfo())
		{			
			Blob::Ptr outputPtr = infer_request.GetBlob(p.first);
			MemoryBlob::CPtr moutput = as<MemoryBlob>(outputPtr);
			auto moutputHolder = moutput->rmap();
			const auto output_data = moutputHolder.as<const PrecisionTrait<Precision::FP32>::value_type*>();		
			Tensor tensor = CreateOutputTensor(p.first, moutput->getTensorDesc().getDims(), (float*)output_data);
			output.TensorMaps[batchIndex].Tensors[tensorMapCounter] = tensor;

			tensorMapCounter++;
		}
	}

	return output;
	
}



