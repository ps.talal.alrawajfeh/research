#pragma once
#include <string>
#include <stdio.h>
#include <algorithm>
#include <fstream>
#include <vector>

using namespace std;

namespace Utilities
{
#ifndef UTILITIES
#define UTILITIES

	static inline string to_upper(const string &str)
	{
		string upper = str;
		for_each(upper.begin(), upper.end(), [](char &c)
				 { c = ::toupper(c); });
		return upper;
	}

	static inline string to_lower(const string &str)
	{
		string lower = str;
		std::transform(lower.begin(), lower.end(), lower.begin(),
					   [](unsigned char c)
					   { return std::tolower(c); });
		return lower;
	}

	static inline void ltrim(std::string &s)
	{
		s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](unsigned char ch)
										{ return !std::isspace(ch); }));
	}

	// trim from end (in place)
	static inline void rtrim(std::string &s)
	{
		s.erase(std::find_if(s.rbegin(), s.rend(), [](unsigned char ch)
							 { return !std::isspace(ch); })
					.base(),
				s.end());
	}

	// trim from both ends (in place)
	static inline string trim(std::string &s)
	{
		ltrim(s);
		rtrim(s);
		return s;
	}

	// trim from both ends (copying)
	static inline std::string trim_copy(std::string s)
	{
		trim(s);
		return s;
	}

	static inline bool string_equals(const string &a, const string &b)
	{
		unsigned int sz = a.size();
		if (b.size() != sz)
			return false;
		for (unsigned int i = 0; i < sz; ++i)
			if (tolower(a[i]) != tolower(b[i]))
				return false;
		return true;
	}

	static inline bool file_exist(const string &file)
	{
		std::ifstream infile(file);
		return infile.good();
	}

	static inline bool vectors_has_same_contents(vector<string> vec1, vector<string> vec2)
	{
		if (vec1.size() != vec2.size())
			return false;
		int size = vec1.size();

		for (int x = 0; x < size; x++)
		{
			bool foundMatch = false;
			for (int y = 0; y < size; y++)
			{
				string lower1 = to_lower(vec1[y]);
				string lower2 = to_lower(vec2[x]);
				lower1 = trim(lower1);
				lower2 = trim(lower2);
				if (lower1.compare(lower2) == 0)
				{
					foundMatch = true;
					break;
				}
			}
			if (!foundMatch)
				return false;
		}

		return true;
	}

#endif
}