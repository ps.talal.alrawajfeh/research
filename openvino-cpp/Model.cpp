#include "Model.h"

using namespace InferenceEngine;

Core Model::IECore() { return _ieCore; }

CNNNetwork Model::Network() { return _network; }

ExecutableNetwork Model::ExecutableNet() { return _executableNet; };

Model Model::Create(const string &xmlPath,
					const string &binPath, const string &deviceName, string &errorDesc)
{
	errorDesc = "";
	if (!file_exist(xmlPath))
		errorDesc = "xmlpath does not exist.";
	else if (!file_exist(binPath))
		errorDesc = "binPath does not exist.";
	if (errorDesc != "")
		return Model();

	Core ie;
	CNNNetwork network = ie.ReadNetwork(xmlPath, binPath);
	const std::map<std::string, std::string> dyn_config =
		{{PluginConfigParams::KEY_DYN_BATCH_ENABLED, PluginConfigParams::YES}}; //enable dynamic batching for future
	//network.setBatchSize(2); //if set > 1 , it will throw an exception
	//ExecutableNetwork executable_network = ie.LoadNetwork(network, to_upper(deviceName), dyn_config);
	ExecutableNetwork executable_network = ie.LoadNetwork(network, to_upper(deviceName));
	for (const auto &p : network.getOutputsInfo())
		p.second->setPrecision(Precision::FP32);
	for (const auto &p : network.getInputsInfo())
		p.second->setPrecision(Precision::FP32);
	return Model(ie, network, executable_network);
}
