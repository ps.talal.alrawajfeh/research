#pragma once
#include <string>
#include <stdio.h>
#include <vector>
#include <inference_engine.hpp>
#include "Batch.h"
#include "Model.h"

using namespace std;
using namespace Utilities;

namespace InferenceEngine {
	class Inference {
	public:
		static Batch Infer(Model& model, Batch& input);		
	};
}

