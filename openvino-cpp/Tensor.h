#pragma once

#include <string>
#include <stdio.h>

using namespace std;

namespace InferenceEngine {
	class Tensor {

	public:
		string Name;
		float* Value;
		int ValueSize;
		vector<int> Dims;
	};

}