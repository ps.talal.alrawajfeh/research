#include "ModelsManager.h"

vector<ModelInfo> ModelsManager::_models = {};


Model ModelsManager::GetModel(const string& name)
{	
	string preparedName = trim_copy(to_lower(name));
	for(int i = 0; i < ModelsManager::_models.size(); i++)
		if (string_equals(trim_copy(to_lower(ModelsManager::_models.at(i).name)), preparedName))
			return ModelsManager::_models.at(i).model;
	throw std::logic_error("Model does not exist");
}

bool ModelsManager::ModelNameExist(const string& name) {
	string preparedName = trim_copy(to_lower(name));	
	
	for (int i = 0; i < ModelsManager::_models.size(); i++) {		
		if (string_equals(trim_copy(to_lower(ModelsManager::_models.at(i).name)), preparedName))
			return true;
	}
	return false;
}

string ModelsManager::CreateAndRegisterModel(const string& modelName, const string& xmlPath,
	const string& binPath, const string& deviceName)
{
	string errorDesc = "";
	try {

		if (ModelNameExist(modelName)) return "Model name already exist.";
		
		Model model = Model::Create(xmlPath, binPath, deviceName, errorDesc);
		if (errorDesc == "")
			ModelsManager::_models.push_back(ModelInfo(modelName, model));
		else
			return errorDesc;
	}
	catch (const std::exception& ex) {
		return "Create and register model failed: " + string(ex.what())  +". ";
	}
	return "";
}


string ModelsManager::UnregisterModel(const string& name) {
	try {
		int index = -1;
		string preparedName = trim_copy(to_lower(name));
		for (int i = 0; i < ModelsManager::_models.size(); i++) {
			if (string_equals(trim_copy(to_lower(ModelsManager::_models.at(i).name)), preparedName))
			{
				index = i;
				break;
			}
		}
		if (index != -1)
			ModelsManager::_models.erase(ModelsManager::_models.begin() + index);

		return (index == -1) ? "Model does not exist." : "";
	}
	catch (const std::exception& ex) {
		return "UnregisterModel model failed: " + string(ex.what()) + ". ";
	}
}

string ModelsManager::Infer(string& name, Batch& input, Batch& output)
{
	if (!ModelsManager::ModelNameExist(name)) return "Model name does not exist";
	Model model = ModelsManager::GetModel(name);
	output = Inference::Infer(model,input);
	return "";
}