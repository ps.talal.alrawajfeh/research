#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <memory>
#include <sys/stat.h> 
#include <stdio.h>
#include "ModelsManager.h"

extern "C" char* RegisterModel(const char* pModelName, const char* xmlPath, const char* binPath, const char* deviceName);
extern "C" char* UnregisterModel(const char* pModelName);
extern "C" bool DoesModelNameExist(const char* pModelName);
extern "C" char* Infer(char* pModelName, char** inputNames, int* inputSizes, int inputCount, float* inputValues, int totalInputSizes,
		char*& outputNames, int*& outputDimesions, int*& outputDimensionsCounts, int& outputCount, float*& inferResult, int& totalOutputSizes);
extern "C" void ReleaseMemory(float*& inputArray);