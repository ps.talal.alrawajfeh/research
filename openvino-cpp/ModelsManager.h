#pragma once

#include <stdio.h>
#include <vector>
#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
#include <memory>
#include <sys/stat.h> 
#include "Modelnfo.h"
#include "Model.h"
#include "Utilities.h"
#include "Batch.h"
#include "Inference.h"

using namespace std;
using namespace Utilities;

class ModelsManager {
public:	
	static string CreateAndRegisterModel(const string& modelName, const string& xmlPath, 
					const string& binPath, const string& deviceName);	
	static bool ModelNameExist(const string& name);
	static string UnregisterModel(const string& name);
	static Model GetModel(const string& name);
	static string Infer(string& name, Batch& input, Batch& output);
	

private:
	static vector<ModelInfo> _models;
};
