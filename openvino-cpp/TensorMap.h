#pragma once

#include <stdio.h>
#include "Tensor.h"

using namespace std;

namespace InferenceEngine {
	class TensorMap {

	public:
		Tensor* Tensors;
		int TensorsSize;
	};
}
