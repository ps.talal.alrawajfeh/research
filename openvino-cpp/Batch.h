#pragma once

#include <stdio.h>
#include "TensorMap.h"

using namespace std;

namespace InferenceEngine {
	class Batch {

	public:
		TensorMap* TensorMaps;
		int TensorMapsSize;
	};
}

