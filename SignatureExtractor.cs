﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace CBJSignatureExtractor
{
    class SignatureExtractor
    {
        private const double RedWeight = 0.299;
        private const double GreenWeight = 0.587;
        private const double BlueWeight = 1.0 - (RedWeight + GreenWeight);
        private const double Epsilon = 1E-07D;
        private const int GrayScaleDepth = 256;

        private static readonly (int Width, int Height) Type1Size = (730, 200);
        private static readonly (int Width, int Height) Type2Size = (450, 400);
        private static readonly (int Width, int Height) Type3Size = (580, 300);

        private static readonly Func<double, double> Type1HeightWidthRatioFuzzifier =
            TruthFuzzifier(0.275, 0.05, 0.4, 0.15);

        private static readonly Func<double, double> Type2HeightWidthRatioFuzzifier =
            TruthFuzzifier(0.88, 0.03, 0.95, 0.8);

        private static readonly Func<double, double> Type3HeightWidthRatioFuzzifier =
            TruthFuzzifier(0.52, 0.025, 0.6, 0.45);


        static int ConvertRgbToGrayScale(int r, int g, int b)
        {
            return (int) Math.Round(r * RedWeight + g * GreenWeight + b * BlueWeight);
        }

        static byte[,] BitmapToMatrix(Bitmap bitmap)
        {
            var width = bitmap.Width;
            var height = bitmap.Height;
            var image = new byte[height, width];

            for (var y = 0; y < height; y++)
            {
                for (var x = 0; x < width; x++)
                {
                    var pixel = bitmap.GetPixel(x, y);
                    image[y, x] = (byte) ConvertRgbToGrayScale(pixel.R, pixel.G, pixel.B);
                }
            }

            return image;
        }

        static Func<double, double> LinearInterpolation(
            List<double> xs,
            List<double> ys,
            double defaultValue)
        {
            return x =>
            {
                if (x - Epsilon < xs[0] || x + Epsilon > xs.Last())
                {
                    return defaultValue;
                }

                if (Math.Abs(x - xs[0]) < Epsilon)
                {
                    return ys[0];
                }

                for (var i = 0; i < xs.Count - 1; i++)
                {
                    if (xs[i] < x && x <= xs[i + 1])
                    {
                        var dy = ys[i + 1] - ys[i];
                        var dx = xs[i + 1] - xs[i];
                        return dy * (x - xs[i]) / dx + ys[i];
                    }
                }

                return ys.Last();
            };
        }

        static Func<double, double> TruthFuzzifier(
            double valueMean,
            double valueStd,
            double valueMax,
            double valueMin)
        {
            return LinearInterpolation(
                new List<double> {0.0, valueMin, valueMean - valueStd, valueMean + valueStd, valueMax, 1.0},
                new List<double> {0.0, 0.5, 0.99, 0.99, 0.5, 0.0},
                0.0);
        }

        static double VerticalSum(byte[,] image, int x, int y1, int y2)
        {
            var sum = 0.0;

            if (y2 < 0)
            {
                return 0.0;
            }

            for (var y = Math.Max(y1, 0); y < Math.Min(y2, image.GetLength(0)); y++)
            {
                sum += image[y, x];
            }

            return sum;
        }

        static double HorizontalSum(byte[,] image, int y, int x1, int x2)
        {
            var sum = 0.0;

            if (x2 < 0)
            {
                return 0.0;
            }

            for (var x = Math.Max(x1, 0); x < Math.Min(x2, image.GetLength(1)); x++)
            {
                sum += image[y, x];
            }

            return sum;
        }

        static double[] VerticalHistogram(byte[,] image)
        {
            var height = image.GetLength(0);
            var width = image.GetLength(1);

            var sums = new double[width];

            for (var x = 0; x < width; x++)
            {
                sums[x] = VerticalSum(image, x, 0, height);
            }

            return sums;
        }

        static int Type1ImageSignaturesSplitX(byte[,] image)
        {
            var height = image.GetLength(0);
            var width = image.GetLength(1);

            var histogram = VerticalHistogram(image);
            histogram = histogram.Select(x => Math.Floor(x / 5.0) * 5.0)
                .ToArray();

            var minXs = new List<int>();
            var minValue = (double) height;

            for (var x = width / 4; x < 3 * width / 4; x++)
            {
                if (histogram[x] < minValue)
                {
                    minValue = histogram[x];
                    minXs = new List<int> {x};
                }
                else if (Math.Abs(histogram[x] - minValue) < Epsilon)
                {
                    minXs.Add(x);
                }
            }

            return (int) Math.Round((double) minXs.Sum() / minXs.Count);
        }

        static List<List<int[]>> GetConnectedRegions(byte[,] image, byte foregroundColor = 255)
        {
            var height = image.GetLength(0);
            var width = image.GetLength(1);

            var visitsMatrix = new bool[height, width];
            var regions = new List<List<int[]>>();

            for (var y = 0; y < height; y++)
            {
                for (var x = 0; x < width; x++)
                {
                    if (image[y, x] == foregroundColor && visitsMatrix[y, x] == false)
                    {
                        regions.Add(GetConnectedRegion(image, new[] {x, y}, visitsMatrix, foregroundColor));
                    }
                }
            }

            return regions;
        }

        private static List<int[]> GetConnectedRegion(byte[,] image,
            int[] startPoint,
            bool[,] visitsMatrix,
            byte foregroundColor)
        {
            var height = image.GetLength(0);
            var width = image.GetLength(1);

            if (image[startPoint[1], startPoint[0]] != foregroundColor)
            {
                return new List<int[]>();
            }

            List<int[]> region = new List<int[]> {startPoint};
            Queue<Tuple<int, int>> queue = new Queue<Tuple<int, int>>();
            queue.Enqueue(new Tuple<int, int>(startPoint[0], startPoint[1]));

            while (queue.Count > 0)
            {
                var parent = queue.Dequeue();
                visitsMatrix[parent.Item2, parent.Item1] = true;

                for (var dy = -1; dy < 2; dy++)
                {
                    for (var dx = -1; dx < 2; dx++)
                    {
                        var x = parent.Item1;
                        var y = parent.Item2;

                        x += dx;
                        y += dy;

                        if (x < 0
                            || x >= width
                            || y < 0
                            || y >= height
                            || visitsMatrix[y, x])
                        {
                            continue;
                        }

                        var point = new Tuple<int, int>(x, y);
                        if (image[y, x] != foregroundColor || queue.Contains(point)) continue;

                        queue.Enqueue(point);
                        region.Add(new[] {x, y});
                    }
                }
            }

            return region;
        }

        static byte[,] Threshold(byte[,] image, int threshold)
        {
            var height = image.GetLength(0);
            var width = image.GetLength(1);

            var imageCopy = new byte[height, width];

            for (var y = 0; y < height; y++)
            {
                for (var x = 0; x < width; x++)
                {
                    imageCopy[y, x] = image[y, x] > threshold ? (byte) 255 : (byte) 0;
                }
            }

            return imageCopy;
        }

        static byte[,] OtsuThreshold(byte[,] image)
        {
            var height = image.GetLength(0);
            var width = image.GetLength(1);

            var histogram = new int[GrayScaleDepth];
            for (var y = 0; y < height; y++)
            {
                for (var x = 0; x < width; x++)
                {
                    histogram[image[y, x]]++;
                }
            }

            double scale = 1.0 / (height * width);
            double mu = histogram.Select((x, i) => x * i).Sum() * scale;

            double q1 = 0;
            var maxVal = 0;
            double maxSigma = -1;
            double mu1 = 0;

            for (int t = 0; t < GrayScaleDepth; t++)
            {
                var pt = histogram[t] * scale;
                mu1 *= q1;
                q1 += pt;
                var q2 = 1.0 - q1;

                if (Math.Min(q1, q2) < Epsilon || Math.Max(q1, q2) > 1.0 - Epsilon)
                {
                    continue;
                }

                mu1 = (mu1 + t * pt) / q1;
                double mu2 = (mu - q1 * mu1) / q2;

                var sigma = q1 * q2 * (mu1 - mu2) * (mu1 - mu2);
                if (sigma > maxSigma)
                {
                    maxSigma = sigma;
                    maxVal = t;
                }
            }

            return Threshold(image, maxVal);
        }

        static int SpecialFloor(double value)
        {
            var floor = (int) value;
            if (floor > value)
            {
                floor--;
            }

            return floor;
        }

        static byte[,] ResizeNearestNeighbors(byte[,] srcMatrix, (int, int) destSize)
        {
            var srcHeight = srcMatrix.GetLength(0);
            var srcWidth = srcMatrix.GetLength(1);
            var (destWidth, destHeight) = destSize;

            if (srcWidth == 0 || srcHeight == 0 || destWidth <= 0 || destHeight <= 0)
            {
                return new byte[0, 0];
            }

            var fx = destWidth / (double) srcWidth;
            var fy = destHeight / (double) srcHeight;

            var ifx = 1.0 / fx;
            var ify = 1.0 / fy;

            var destMatrix = new byte[destHeight, destWidth];

            for (var y = 0; y < destHeight; y++)
            {
                for (var x = 0; x < destWidth; x++)
                {
                    var srcX = SpecialFloor(ifx * x);
                    var srcY = SpecialFloor(ify * y);

                    srcX = Math.Max(Math.Min(srcX, srcWidth - 1), 0);
                    srcY = Math.Max(Math.Min(srcY, srcHeight - 1), 0);

                    destMatrix[y, x] = srcMatrix[srcY, srcX];
                }
            }

            return destMatrix;
        }


        static byte[,] RectangularDilation(byte[,] image, (int, int) kernelShape)
        {
            int height = image.GetLength(0);
            int width = image.GetLength(1);
            byte[,] dilated = new byte[height, width];

            if (kernelShape.Item1 % 2 == 0
                || kernelShape.Item2 % 2 == 0
                || kernelShape.Item1 < 0
                || kernelShape.Item2 < 0)
            {
                throw new ArgumentException("kernel must have positive odd dimensions");
            }

            var originY = kernelShape.Item1 / 2;
            var originX = kernelShape.Item2 / 2;

            for (var y = 0; y < height; y++)
            {
                for (var x = 0; x < width; x++)
                {
                    var imageX1 = Math.Max(x - originX, 0);
                    var imageY1 = Math.Max(y - originY, 0);
                    var imageX2 = Math.Min(x + originX + 1, width);
                    var imageY2 = Math.Min(y + originY + 1, height);

                    byte value = 0;
                    for (var yk = imageY1; yk < imageY2; yk++)
                    {
                        if (value == 1)
                        {
                            break;
                        }

                        for (var xk = imageX1; xk < imageX2; xk++)
                        {
                            if (image[yk, xk] == 1)
                            {
                                value = 1;
                                break;
                            }
                        }
                    }

                    dilated[y, x] = value;
                }
            }

            return dilated;
        }

        static byte[,] Apply(byte[,] image, Func<byte, byte> lambda)
        {
            var height = image.GetLength(0);
            var width = image.GetLength(1);

            byte[,] result = new byte[height, width];

            for (var y = 0; y < height; y++)
            {
                for (var x = 0; x < width; x++)
                {
                    result[y, x] = lambda(image[y, x]);
                }
            }

            return result;
        }

        static (int, int, int, int) GetRegionRectangle(List<int[]> region, int imageWidth, int imageHeight)
        {
            var minX = imageWidth;
            var minY = imageHeight;
            var maxX = 0;
            var maxY = 0;

            foreach (var point in region)
            {
                var x = point[0];
                var y = point[1];

                minX = Math.Min(minX, x);
                minY = Math.Min(minY, y);
                maxX = Math.Max(maxX, x);
                maxY = Math.Max(maxY, y);
            }

            return (minX, minY, maxX, maxY);
        }

        static byte[,] FilterOutExtremeRegions(byte[,] binaryImage)
        {
            var width = binaryImage.GetLength(1);
            var height = binaryImage.GetLength(0);
            var area = width * height;

            var dilated = RectangularDilation(binaryImage, (3, 3));
            var regions = GetConnectedRegions(dilated, 1);
            var rectangles =
                regions.Select(region => GetRegionRectangle(region, width, height)).ToList();

            var filteredRegions = new List<List<int[]>>();

            for (var i = 0; i < regions.Count; i++)
            {
                var (x1, y1, x2, y2) = rectangles[i];
                var region = regions[i];

                var rectWidth = x2 - x1;
                var rectHeight = y2 - y1;

                var heightRatio = (double) rectHeight / height;
                var widthRatio = (double) rectWidth / width;

                if (rectWidth <= 10 && rectHeight <= 10)
                {
                    continue;
                }

                if (heightRatio >= 0.85 || widthRatio >= 0.85)
                    continue;

                if ((double) rectWidth * rectHeight / area >= 0.7)
                    continue;

                if ((double) rectWidth / rectHeight < 0.05 || (double) rectHeight / rectWidth < 0.05)
                    continue;

                var pixelsToImageAreaRatio = (double) region.Count / area;

                if (pixelsToImageAreaRatio >= 0.85 || pixelsToImageAreaRatio < 0.00015)
                    continue;

                filteredRegions.Add(region);
            }

            byte[,] result = new byte[height, width];

            foreach (var region in filteredRegions)
            {
                foreach (var point in region)
                {
                    var x = point[0];
                    var y = point[1];

                    if (binaryImage[y, x] == 1)
                    {
                        result[y, x] = 1;
                    }
                }
            }

            return result;
        }

        static (byte[,] Result, int ExpectedSignaturesY) RemoveText(
            byte[,] binaryImage,
            double threshold = 0.8)
        {
            var width = binaryImage.GetLength(1);
            var height = binaryImage.GetLength(0);

            var dilated = RectangularDilation(binaryImage, (1, 27));

            var regions = GetConnectedRegions(dilated, 1);
            var rectangles =
                regions.Select(region => GetRegionRectangle(region, width, height)).ToList();

            var filteredRegions = new List<List<int[]>>();

            var expectedSignaturesY = 0;

            for (var i = 0; i < regions.Count; i++)
            {
                var (x1, y1, x2, y2) = rectangles[i];
                var region = regions[i];
                var rectWidth = x2 - x1;
                var rectHeight = y2 - y1;

                var pixelsToRectangleAreaRatio = (double) region.Count / (rectWidth * rectHeight);
                if (pixelsToRectangleAreaRatio > threshold && rectWidth > rectHeight)
                {
                    if (y2 >= expectedSignaturesY && y2 < height / 2)
                    {
                        expectedSignaturesY = y2;
                    }

                    continue;
                }

                filteredRegions.Add(region);
            }

            byte[,] result = new byte[height, width];

            foreach (var region in filteredRegions)
            {
                foreach (var point in region)
                {
                    var x = point[0];
                    var y = point[1];

                    if (binaryImage[y, x] == 1)
                    {
                        result[y, x] = 1;
                    }
                }
            }

            return (result, expectedSignaturesY);
        }

        static bool IsRectangleValid((int, int, int, int) rectangle, int imageWidth, int imageHeight)
        {
            var (x1, y1, x2, y2) = rectangle;

            if (x1 < 0
                || y1 < 0
                || x2 < 0
                || y2 < 0
                || x2 <= x1
                || y2 <= y1
                || x2 > imageWidth
                || y2 > imageHeight)
            {
                return false;
            }

            return true;
        }

        static byte[,] Crop(byte[,] image, (int, int, int, int) rectangle)
        {
            var (x1, y1, x2, y2) = rectangle;
            var height = image.GetLength(0);
            var width = image.GetLength(1);

            if (x2 == -1)
            {
                x2 = width;
            }

            if (y2 == -1)
            {
                y2 = height;
            }

            if (!IsRectangleValid((x1, y1, x2, y2), width, height))
            {
                return new byte[0, 0];
            }

            var rectWidth = x2 - x1;
            var rectHeight = y2 - y1;

            var cropped = new byte[rectHeight, rectWidth];

            for (var y = y1; y < y2; y++)
            {
                for (var x = x1; x < x2; x++)
                {
                    cropped[y - y1, x - x1] = image[y, x];
                }
            }

            return cropped;
        }

        static (int, int, int, int) ForegroundMinimalBoundingRectangle(byte[,] binaryImage)
        {
            var height = binaryImage.GetLength(0);
            var width = binaryImage.GetLength(1);

            int x1 = 0, y1 = 0, x2 = width - 1, y2 = height - 1;

            while (y1 < height &&
                   Enumerable.Range(0, width)
                       .Select(x => binaryImage[y1, x])
                       .All(x => x == 0))
            {
                y1++;
            }

            while (x1 < width &&
                   Enumerable.Range(0, height)
                       .Select(y => binaryImage[y, x1])
                       .All(y => y == 0))
            {
                x1++;
            }

            while (y2 >= 0 &&
                   Enumerable.Range(0, width)
                       .Select(x => binaryImage[y2, x])
                       .All(x => x == 0))
            {
                y2--;
            }

            while (x2 >= 0 &&
                   Enumerable.Range(0, height)
                       .Select(y => binaryImage[y, x2])
                       .All(y => y == 0))
            {
                x2--;
            }

            if (x1 >= x2 || y1 >= y2)
            {
                return (0, 0, 0, 0);
            }

            return (x1, y1, x2, y2);
        }

        static (int, int, int, int) AutoCorrectRectangle(byte[,] binaryImage, (int, int, int, int) rectangle)
        {
            var height = binaryImage.GetLength(0);
            var width = binaryImage.GetLength(1);

            var (x1, y1, x2, y2) = rectangle;

            var rectWidth = x2 - x1;
            var rectHeight = y2 - y1;

            while (y1 > 0 &&
                   Enumerable.Range(x1, rectWidth)
                       .Select(x => binaryImage[y1 - 1, x])
                       .Any(x => x != 0))
            {
                y1--;
            }

            while (x1 > 0 &&
                   Enumerable.Range(y1, rectHeight)
                       .Select(y => binaryImage[y, x1 - 1])
                       .Any(y => y != 0))
            {
                x1--;
            }

            while (y2 < height &&
                   Enumerable.Range(x1, rectWidth)
                       .Select(x => binaryImage[y2, x])
                       .Any(x => x != 0))
            {
                y2++;
            }

            while (x2 < width &&
                   Enumerable.Range(y1, rectHeight)
                       .Select(y => binaryImage[y, x2])
                       .All(y => y != 0))
            {
                x2++;
            }

            if (x1 >= x2 || y1 >= y2)
            {
                return (0, 0, 0, 0);
            }

            return (x1, y1, x2, y2);
        }

        static Bitmap CropBitmap(Bitmap bitmap, Rectangle rectangle)
        {
            if (rectangle.X < 0
                || rectangle.Y < 0
                || rectangle.Width == 0
                || rectangle.Height == 0
                || rectangle.Width > bitmap.Width
                || rectangle.Height > bitmap.Height)
            {
                return null;
            }

            Bitmap cropped = new Bitmap(rectangle.Width, rectangle.Height);

            using var graphics = Graphics.FromImage(cropped);

            graphics.DrawImage(bitmap,
                new Rectangle(0, 0, cropped.Width, cropped.Height),
                rectangle,
                GraphicsUnit.Pixel);

            return cropped;
        }

        static (int, int, int, int) RescaleRectangle(
            (int, int, int, int) rectangle,
            (int, int) srcImageSize,
            (int, int) destinationImageSize)
        {
            var x1 = (int) Math.Floor((double) rectangle.Item1 / srcImageSize.Item1 * destinationImageSize.Item1);
            var y1 = (int) Math.Floor((double) rectangle.Item2 / srcImageSize.Item2 * destinationImageSize.Item2);
            var x2 = (int) Math.Ceiling((double) rectangle.Item3 / srcImageSize.Item1 * destinationImageSize.Item1);
            var y2 = (int) Math.Ceiling((double) rectangle.Item4 / srcImageSize.Item2 * destinationImageSize.Item2);

            return (x1, y1, x2, y2);
        }

        static (byte[,], int, bool) CheckAndAdjustForLine(
            byte[,] binaryImage,
            int axis,
            int direction,
            double threshold = 0.7)
        {
            bool isLineDetected = false;

            if (binaryImage.GetLength(axis) == 0)
            {
                return (binaryImage, 0, isLineDetected);
            }

            var maxValueI = -1;
            var maxValue = 0.0;

            IEnumerable<int> loopRange;
            var axisLength = binaryImage.GetLength(axis);
            if (axisLength < 8)
            {
                if (direction == 1)
                {
                    return (binaryImage, 0, isLineDetected);
                }

                return (binaryImage, axisLength, isLineDetected);
            }

            if (direction == 1)
            {
                loopRange = Enumerable.Range(0, axisLength / 4);
            }
            else if (direction == -1)
            {
                loopRange = Enumerable.Range(1, axisLength / 4 - 1).Select(x => axisLength - x);
            }
            else
            {
                throw new ArgumentException($"direction {direction} is invalid");
            }

            var denominator = binaryImage.GetLength(1 - axis);

            var height = binaryImage.GetLength(0);
            var width = binaryImage.GetLength(1);

            foreach (var i in loopRange)
            {
                var sum = axis == 1
                    ? VerticalSum(binaryImage, i, 0, height)
                    : HorizontalSum(binaryImage, i, 0, width);

                var value = sum / denominator;

                if (value > threshold && value > maxValue)
                {
                    isLineDetected = true;
                    maxValue = value;
                    maxValueI = i;
                }
            }

            int minValueI;
            if (maxValueI == -1)
            {
                if (direction == 1)
                {
                    minValueI = 0;
                }
                else
                {
                    minValueI = axisLength - 1;
                }
            }
            else
            {
                minValueI = maxValueI;
                var minValue = 1.0;

                if (direction == 1)
                {
                    loopRange = Enumerable.Range(maxValueI, axisLength / 4 - maxValueI);
                }
                else
                {
                    var start = axisLength - maxValueI;
                    loopRange = Enumerable.Range(start, axisLength / 4 - start).Select(x => axisLength - x);
                }

                foreach (var i in loopRange)
                {
                    var sum = axis == 1
                        ? VerticalSum(binaryImage, i, 0, height)
                        : HorizontalSum(binaryImage, i, 0, width);

                    var value = sum / denominator;

                    if (value < minValue)
                    {
                        minValue = value;
                        minValueI = i;
                    }
                }
            }

            if (direction == 1)
            {
                if (axis == 1)
                {
                    return (Crop(binaryImage, (minValueI, 0, -1, -1)), minValueI, isLineDetected);
                }

                return (Crop(binaryImage, (0, minValueI, -1, -1)), minValueI, isLineDetected);
            }

            if (axis == 1)
            {
                return (Crop(binaryImage, (0, 0, minValueI + 1, -1)), minValueI, isLineDetected);
            }

            return (Crop(binaryImage, (0, 0, -1, minValueI + 1)), minValueI, isLineDetected);
        }

        static (byte[,], (int, int, int, int)) CheckAndAdjustForRectangle(byte[,] binaryImage,
            bool allLinesMustExist = true,
            double threshold = 0.7)
        {
            var defaultRectangle = (0, 0, binaryImage.GetLength(1), binaryImage.GetLength(0));

            var (newBinaryImage, x1, isLineDetected) = CheckAndAdjustForLine(binaryImage, 1, 1, threshold);
            if (!isLineDetected && allLinesMustExist)
            {
                return (binaryImage, defaultRectangle);
            }

            int y1, x2, y2;

            (newBinaryImage, x2, isLineDetected) = CheckAndAdjustForLine(newBinaryImage, 1, -1, threshold);
            if (!isLineDetected && allLinesMustExist)
            {
                return (binaryImage, defaultRectangle);
            }

            (newBinaryImage, y1, isLineDetected) = CheckAndAdjustForLine(newBinaryImage, 0, 1, threshold);
            if (!isLineDetected && allLinesMustExist)
            {
                return (binaryImage, defaultRectangle);
            }

            (newBinaryImage, y2, isLineDetected) = CheckAndAdjustForLine(newBinaryImage, 0, -1, threshold);
            if (!isLineDetected && allLinesMustExist)
            {
                return (binaryImage, defaultRectangle);
            }

            return (newBinaryImage, (x1, y1, x2, y2));
        }

        static (int, int, int, int) CalculateRelativeRectangle((int, int, int, int) rectangle,
            params (int, int)[] startPoints)
        {
            var (x1, y1, x2, y2) = rectangle;

            foreach (var (x, y) in startPoints)
            {
                x1 += x;
                x2 += x;

                y1 += y;
                y2 += y;
            }

            return (x1, y1, x2, y2);
        }

        static bool IsEmpty(byte[,] binaryImage)
        {
            var height = binaryImage.GetLength(0);
            var width = binaryImage.GetLength(1);

            for (var y = 0; y < height; y++)
            {
                for (var x = 0; x < width; x++)
                {
                    if (binaryImage[y, x] != 0)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        static List<Bitmap> ExtractSignaturesFromType1(Bitmap bitmap)
        {
            var image = BitmapToMatrix(bitmap);
            var resized = ResizeNearestNeighbors(image, Type1Size);
            var thresh = OtsuThreshold(resized);
            var inverted = Apply(thresh, x => (byte) (255 - x));
            var binaryImage = Apply(inverted, x => (byte) (x / 255));

            var cleanedImage = FilterOutExtremeRegions(binaryImage);
            if (IsEmpty(cleanedImage))
            {
                return new List<Bitmap> {bitmap};
            }

            var cleanedImageCopy = cleanedImage.Clone() as byte[,];
            var (result, expectedSignaturesY) = RemoveText(cleanedImage);
            if (!IsEmpty(result))
            {
                cleanedImage = result;
            }

            var width = cleanedImage.GetLength(1);
            var height = cleanedImage.GetLength(0);

            var minSum = (double) width;
            for (var y = expectedSignaturesY; y < height / 2; y++)
            {
                var sum = HorizontalSum(cleanedImage, y, 0, width);
                if (sum < minSum)
                {
                    minSum = sum;
                    expectedSignaturesY = y;
                }
            }

            var splitX = Type1ImageSignaturesSplitX(binaryImage);
            var leftImage = Crop(cleanedImage, (0, expectedSignaturesY, splitX, height));
            var rightImage = Crop(cleanedImage, (splitX, expectedSignaturesY, width, height));
            var leftRectangle = ForegroundMinimalBoundingRectangle(leftImage);
            var rightRectangle = ForegroundMinimalBoundingRectangle(rightImage);

            leftRectangle.Item2 += expectedSignaturesY;
            leftRectangle.Item4 += expectedSignaturesY;

            rightRectangle.Item2 += expectedSignaturesY;
            rightRectangle.Item4 += expectedSignaturesY;

            rightRectangle.Item1 += splitX;
            rightRectangle.Item3 += splitX;

            leftRectangle = AutoCorrectRectangle(cleanedImageCopy, leftRectangle);
            rightRectangle = AutoCorrectRectangle(cleanedImageCopy, rightRectangle);

            List<Bitmap> signatures = new List<Bitmap>();

            foreach (var rectangle in new[] {leftRectangle, rightRectangle})
            {
                var croppedImage = Crop(binaryImage, rectangle);
                croppedImage = RectangularDilation(croppedImage, (3, 3));
                (int, int, int, int) adjustedForLines;
                (croppedImage, adjustedForLines) = CheckAndAdjustForRectangle(croppedImage, true, 0.9);
                var minimalBoundingRectangle = ForegroundMinimalBoundingRectangle(croppedImage);

                minimalBoundingRectangle = CalculateRelativeRectangle(minimalBoundingRectangle,
                    (adjustedForLines.Item1, adjustedForLines.Item2),
                    (rectangle.Item1, rectangle.Item2));

                var (x1, y1, x2, y2) = RescaleRectangle(minimalBoundingRectangle,
                    Type1Size,
                    (bitmap.Width, bitmap.Height));

                if (x2 - x1 == 0 || y2 - y1 == 0)
                {
                    continue;
                }

                signatures.Add(CropBitmap(bitmap, new Rectangle(
                    Math.Max(x1 - 2, 0),
                    Math.Max(y1 - 2, 0),
                    Math.Min(x2 - x1 + 2, bitmap.Width),
                    Math.Min(y2 - y1 + 2, bitmap.Width))));
            }

            return signatures;
        }

        static double EuclideanDistance((int X, int Y) point1, (int X, int Y) point2)
        {
            return Math.Sqrt(Math.Pow((double) point1.X - point2.X, 2.0) + Math.Pow((double) point1.Y - point2.Y, 2.0));
        }

        static IEnumerable<(int, int)> Combinations(IEnumerable<int> items)
        {
            var list = items.ToList();

            for (var i = 0; i < list.Count - 1; i++)
            {
                for (var j = i + 1; j < list.Count; j++)
                {
                    yield return (list[i], list[j]);
                }
            }
        }

        static (int, int) Center((int X, int Y) point1, (int X, int Y) point2)
        {
            return ((point1.X + point2.X) / 2, (point1.Y + point2.Y) / 2);
        }

        static (int, int, int, int) Union(
            (int X1, int Y1, int X2, int Y2) rect1,
            (int X1, int Y1, int X2, int Y2) rect2)
        {
            return (Math.Min(rect1.X1, rect2.X1),
                Math.Min(rect1.Y1, rect2.Y1),
                Math.Max(rect1.X2, rect2.X2),
                Math.Max(rect1.Y2, rect2.Y2));
        }

        static List<Bitmap> ExtractSignaturesFromType2(Bitmap bitmap,
            bool removeText = false,
            bool resizeInput = true)
        {
            var image = BitmapToMatrix(bitmap);
            var resized = resizeInput ? ResizeNearestNeighbors(image, Type2Size) : image;
            var thresh = OtsuThreshold(resized);
            var inverted = Apply(thresh, x => (byte) (255 - x));
            var binaryImage = Apply(inverted, x => (byte) (x / 255));

            var cleanedImage = FilterOutExtremeRegions(binaryImage);

            if (IsEmpty(cleanedImage))
            {
                return new List<Bitmap> {bitmap};
            }

            if (removeText)
            {
                var result = RemoveText(cleanedImage).Result;
                if (!IsEmpty(result))
                {
                    cleanedImage = result;
                }
            }

            var height = cleanedImage.GetLength(0);
            var width = cleanedImage.GetLength(1);

            var regions = GetConnectedRegions(cleanedImage, 1);
            var rectangles =
                regions.Select(region => GetRegionRectangle(region, width, height)).ToList();

            while (rectangles.Count > 2)
            {
                var distancesWithIndices = new List<(double, int, int)>();

                foreach (var (r1Index, r2Index) in Combinations(Enumerable.Range(0, rectangles.Count)))
                {
                    var (r1X1, r1Y1, r1X2, r1Y2) = rectangles[r1Index];
                    var (r2X1, r2Y1, r2X2, r2Y2) = rectangles[r2Index];

                    var distance = EuclideanDistance(
                        Center((r1X1, r1Y1), (r1X2, r1Y2)),
                        Center((r2X1, r2Y1), (r2X2, r2Y2)));

                    distancesWithIndices.Add((distance, r1Index, r2Index));
                }

                distancesWithIndices.Sort((x, y) => x.Item1.CompareTo(y.Item1));

                var bestDistanceWithIndices = distancesWithIndices.First();

                var rectangle1 = rectangles[bestDistanceWithIndices.Item2];
                var rectangle2 = rectangles[bestDistanceWithIndices.Item3];

                var newRectangle = Union(rectangle1, rectangle2);

                rectangles.RemoveAll(rect => rect == rectangle1 || rect == rectangle2);
                rectangles.Add(newRectangle);
            }

            List<Bitmap> signatures = new List<Bitmap>();

            foreach (var rectangle in rectangles)
            {
                var (x1, y1, x2, y2) = resizeInput
                    ? RescaleRectangle(rectangle,
                        Type2Size,
                        (bitmap.Width, bitmap.Height))
                    : rectangle;

                if (x2 - x1 == 0 || y2 - y1 == 0)
                {
                    continue;
                }

                signatures.Add(CropBitmap(bitmap, new Rectangle(
                    Math.Max(x1 - 2, 0),
                    Math.Max(y1 - 2, 0),
                    Math.Min(x2 - x1 + 2, bitmap.Width),
                    Math.Min(y2 - y1 + 2, bitmap.Width))));
            }

            return signatures;
        }

        static List<Bitmap> ExtractSignaturesFromType3(Bitmap bitmap)
        {
            var image = BitmapToMatrix(bitmap);
            var resized = ResizeNearestNeighbors(image, Type3Size);
            var thresh = OtsuThreshold(resized);
            var inverted = Apply(thresh, x => (byte) (255 - x));
            var binaryImage = Apply(inverted, x => (byte) (x / 255));
            var dilated = RectangularDilation(binaryImage, (3, 3));

            var height = binaryImage.GetLength(0);
            var width = binaryImage.GetLength(1);

            var maxVerticalSum = 0.0;
            var maxVerticalSumX = -1;
            for (var x = width / 3; x < width * 2 / 3; x++)
            {
                var sum = VerticalSum(dilated, x, 0, height);
                if (sum > maxVerticalSum)
                {
                    maxVerticalSum = sum;
                    maxVerticalSumX = x;
                }
            }

            var maxHorizontalSum = 0.0;
            var maxHorizontalSumY = -1;
            for (var y = height / 3; y < height * 2 / 3; y++)
            {
                var sum = HorizontalSum(dilated, y, 0, width);
                if (sum > maxHorizontalSum)
                {
                    maxHorizontalSum = sum;
                    maxHorizontalSumY = y;
                }
            }

            var rectangle1 = (0, 0, maxVerticalSumX, maxHorizontalSumY);
            var rectangle2 = (maxVerticalSumX, 0, width, maxHorizontalSumY);
            var rectangle3 = (0, maxHorizontalSumY, maxVerticalSumX, height);
            var rectangle4 = (maxVerticalSumX, maxHorizontalSumY, width, height);

            var imageRectanglePairs = new[]
            {
                (Crop(dilated, rectangle1), rectangle1),
                (Crop(dilated, rectangle2), rectangle2),
                (Crop(dilated, rectangle3), rectangle3),
                (Crop(dilated, rectangle4), rectangle4)
            };

            List<Bitmap> signatures = new List<Bitmap>();

            foreach (var (cropped, rectangle) in imageRectanglePairs)
            {
                var (adjusted, adjustedRectangle) = CheckAndAdjustForRectangle(cropped, false, 0.8);
                var minimalBoundingRectangle = ForegroundMinimalBoundingRectangle(adjusted);

                minimalBoundingRectangle = CalculateRelativeRectangle(minimalBoundingRectangle,
                    (adjustedRectangle.Item1, adjustedRectangle.Item2),
                    (rectangle.Item1, rectangle.Item2));

                var (x1, y1, x2, y2) = RescaleRectangle(minimalBoundingRectangle,
                    Type3Size,
                    (bitmap.Width, bitmap.Height));

                if (x2 - x1 == 0 || y2 - y1 == 0)
                {
                    continue;
                }

                signatures.Add(CropBitmap(bitmap, new Rectangle(x1, y1, x2 - x1, y2 - y1)));
            }

            return signatures;
        }

        static List<Bitmap> ExtractSignaturesFromUnknownType(Bitmap bitmap)
        {
            return ExtractSignaturesFromType2(bitmap, true, false);
        }

        public static List<Bitmap> ExtractSignatures(Bitmap bitmap)
        {
            var heightWidthRatio = (double) bitmap.Height / bitmap.Width;

            if (Type1HeightWidthRatioFuzzifier(heightWidthRatio) >= 0.8)
            {
                return ExtractSignaturesFromType1(bitmap);
            }

            if (Type2HeightWidthRatioFuzzifier(heightWidthRatio) >= 0.8)
            {
                return ExtractSignaturesFromType2(bitmap);
            }

            if (Type3HeightWidthRatioFuzzifier(heightWidthRatio) >= 0.8)
            {
                return ExtractSignaturesFromType3(bitmap);
            }

            return ExtractSignaturesFromUnknownType(bitmap);
        }
    }
}