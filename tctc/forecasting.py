import math
from datetime import timedelta
from typing import Callable

import numpy as np
from matplotlib import pyplot as plt
from scipy.linalg import lstsq


def mse(y_true: np.ndarray, y_pred: np.ndarray) -> float:
    return float(np.mean(np.square(y_true - y_pred)))


def evaluate_time_series_model_index_based(model: Callable[[np.ndarray], np.ndarray],
                                           training_data_size: int,
                                           evaluation_data: np.ndarray) -> float:
    predictions = model(np.arange(training_data_size + 1,
                                  training_data_size + 1 + evaluation_data.shape[0]).astype(np.float32))
    return mse(evaluation_data, predictions)


def fourier_series(time_series,
                   num_coefficients=20):
    count = len(time_series)
    coefficients = np.fft.fft(time_series)

    orders = np.arange(coefficients.shape[0], dtype=np.float32)

    magnitude_spectrum = np.abs(coefficients)
    sorted_indices = np.argsort(magnitude_spectrum)[::-1]

    truncated_indices = sorted_indices[:num_coefficients]
    coefficients = np.expand_dims(np.conj(coefficients[truncated_indices]), axis=0)
    orders = np.expand_dims(orders[truncated_indices], axis=0)

    const = -2 * complex(0, 1) * math.pi / count
    return lambda t: np.sum(coefficients * np.exp(const * np.expand_dims(t, axis=-1) * orders), axis=-1).real / count


def fit_constant_trend(time_series: np.ndarray) -> Callable[[np.ndarray], np.ndarray]:
    c = np.mean(time_series)
    return lambda x: np.ones(x.shape) * c


class FourierSeriesForecastingModel:
    def __init__(self, train_series):
        self.train_series = train_series
        self._update_forecasting_model()

    def _update_forecasting_model(self):
        self.trend_function = fit_constant_trend(self.train_series)
        indices = np.arange(0, len(self.train_series), dtype=np.float32) + 1
        trend_removed_series = self.train_series - self.trend_function(indices)
        self.seasonality = fourier_series(trend_removed_series)
        self.next_index = indices[-1]

    def forecast(self):
        x = np.array([self.next_index])
        return self.seasonality(x)[0] + self.trend_function(x + 1)[0]

    def update(self, x):
        self.train_series = np.concatenate([self.train_series, [x]], axis=0)

    def forecast_steps(self, steps):
        x = np.arange(self.next_index, self.next_index + steps, dtype=np.float32)
        return self.seasonality(x) + self.trend_function(x + 1)


def dates_to_time_series(dates):
    differences = []

    for i in range(1, len(dates)):
        time_delta = dates[i] - dates[i - 1]
        differences.append(time_delta.days)

    return np.array(differences, np.float32)


def dot_product(array1: np.ndarray, array2: np.ndarray) -> np.ndarray:
    return np.sum(array1 * array2)


def sample_autocovariance(time_series: np.ndarray,
                          lag: int) -> float:
    h = abs(lag)
    average = np.mean(time_series)
    sum_of_products = dot_product(time_series[h:] - average, time_series[:time_series.shape[0] - h] - average)
    return sum_of_products / time_series.shape[0]


def sample_autocorrelation(time_series: np.ndarray,
                           lag: int) -> float:
    return sample_autocovariance(time_series, lag) / sample_autocovariance(time_series, 0)


def forecast_next_dates(last_date,
                        forecasting_model,
                        number_of_steps):
    next_steps = forecasting_model.forecast_steps(number_of_steps)
    current_date = last_date
    forecasted_dates = []

    for i in range(next_steps.shape[0]):
        current_date += timedelta(days=int(round(next_steps[i])))
        forecasted_dates.append(current_date)

    return forecasted_dates


def moving_average_centered(sequence: np.ndarray,
                            window_size: int,
                            keep_same_size: bool = True,
                            damp_extremes: bool = False) -> np.ndarray:
    if window_size % 2 == 0:
        raise ValueError('window_size must odd')
    half_window_size = window_size // 2
    sequence_copy = np.array(sequence, np.float32)
    kernel = np.ones(window_size, np.float32)
    if damp_extremes:
        kernel[0] = 0.5
        kernel[-1] = 0.5
    averages = np.convolve(sequence_copy, kernel, 'valid') / window_size
    if keep_same_size:
        sequence_copy[half_window_size: sequence.shape[0] - half_window_size] = averages
        return sequence_copy
    return averages


def moving_average(sequence: np.ndarray,
                   window_size: int,
                   keep_same_size: bool = True) -> np.ndarray:
    half_window_size = window_size // 2
    if window_size % 2 == 0:
        return moving_average_centered(sequence, half_window_size * 2 + 1, keep_same_size, True)
    else:
        return moving_average_centered(sequence, window_size, keep_same_size, False)


def find_seasonality_period(time_series):
    count = time_series.shape[0]
    indices = np.arange(count)
    trend_weights = np.polyfit(indices, time_series, deg=1)
    trend_function = np.poly1d(trend_weights)

    trend = trend_function(indices)
    de_trended_time_series = time_series - trend
    max_period = min(365, int(math.floor(count / 2)))

    acf_values = [sample_autocorrelation(de_trended_time_series, i) for i in range(max_period + 1)]
    acf_values = np.array(acf_values)
    acf_values *= np.power(np.arange(1, len(acf_values) + 1).astype(np.float64) / len(acf_values), 1 / 4)

    if np.argmax(acf_values[1:]) + 1 == len(acf_values) - 1:
        best_period = len(acf_values) - 1
    else:
        new_acf_values = [0]
        for i in range(1, len(acf_values) - 1):
            if acf_values[i] > max(acf_values[i - 1], acf_values[i + 1]):
                new_acf_values.append(acf_values[i])
            else:
                if acf_values[i] < 0:
                    new_acf_values.append(acf_values[i])
                else:
                    new_acf_values.append(-acf_values[i])

        best_period = np.argmax(new_acf_values)

    if best_period == 0:
        best_period = np.argmax(acf_values[1:]) + 1

    return best_period, max_period


def fit_fourier_series_with_linear_trend(series,
                                         period_tolerance=0):
    best_period, max_period = find_seasonality_period(series)

    start_period = int(math.floor(best_period * (1 - period_tolerance)))
    end_period = int(math.ceil(best_period * (1 + period_tolerance)))

    indices = np.arange(series.shape[0]).astype(np.float64)
    indices_2_pi = 2 * math.pi * indices

    min_error = None
    best_coefficients = None
    best_period = None
    best_number_of_coefficients = None

    for period in range(start_period, end_period + 1):
        frequencies = np.expand_dims(indices_2_pi / period, axis=0)
        for number_of_coefficients in range(1, period + 1):
            angles = frequencies * np.expand_dims(np.arange(number_of_coefficients).astype(np.float32), axis=-1)
            design_matrix = np.concatenate([[np.ones(series.shape[0], np.float64), indices.copy()],
                                            np.sin(angles),
                                            np.cos(angles)],
                                           axis=0).transpose()

            try:
                coefficients, res, _, _ = lstsq(design_matrix, series)
                error = np.mean(np.square(np.sum(design_matrix * coefficients, axis=-1) - series))
                if min_error is None or min_error > error:
                    min_error = error
                    best_coefficients = coefficients
                    best_period = period
                    best_number_of_coefficients = number_of_coefficients
            except:
                pass

    return min_error, best_period, best_number_of_coefficients, best_coefficients


def main():
    # x = np.array([1.0, 2.0, 5.0, 3.0, 2.0, 1.0, 2.0, 5.0, 3.0, 2.0, 1.0, 2.0, 5.0, 3.0, 2.0, 1.0, 2.0])
    # x += np.arange(x.shape[0])
    # 13 has a problem also 18
    with open('/home/u764/Development/data/tsa/fadi-tsa/fadi7.csv', 'r') as f:
        series = f.read()
    series = series.split('\n')
    series = [float(s) for s in series if s.strip() != '']
    series = np.array(series)

    plt.plot(series)
    plt.show()
    exit()

    (min_error,
     best_period,
     best_number_of_coefficients,
     best_coefficients) = fit_fourier_series_with_linear_trend(series)

    print(min_error)
    print(best_period)

    steps = series.shape[0]
    forecasts = []
    for i in range(0, series.shape[0] + steps):
        current_value = best_coefficients[0]
        current_value += best_coefficients[1] * i
        offset = 2
        for k in range(best_number_of_coefficients):
            current_value += best_coefficients[offset + k] * math.sin(2 * math.pi * k * i / best_period)
        offset = 2 + best_number_of_coefficients
        for k in range(best_number_of_coefficients):
            current_value += best_coefficients[offset + k] * math.cos(2 * math.pi * k * i / best_period)
        forecasts.append(current_value)
    forecasts = np.array(forecasts)
    #
    # print(forecasts)
    # print('------')
    # print(min_error)
    # print(best_coefficients)
    # print(best_period)
    # print(best_number_of_coefficients)

    plt.plot(np.arange(series.shape[0]), series, color='blue')
    plt.plot(np.arange(forecasts.shape[0]), forecasts, color='red')
    plt.show()


if __name__ == '__main__':
    main()
