import datetime
import math
from enum import Enum

import numpy as np
import pandas as pd
from dateutil.relativedelta import relativedelta
from scipy.stats import lognorm
from tqdm import tqdm


def parse_date(date_string):
    year, month, day = date_string.split('-')
    year = int(year) + 2000
    month = int(month)
    day = int(day)
    return datetime.date(year=year, month=month, day=day)


def parse_csv_file(file_path):
    data = pd.read_csv(file_path)
    data_array = data.to_numpy()
    transactions_time_series = []

    for i in range(len(data_array)):
        row = data_array[i]
        date_amount_bfd_tuple = (parse_date(row[12]), float(row[8]), int(row[6]), int(row[5]))
        transactions_time_series.append(date_amount_bfd_tuple)

    transactions_time_series.sort(key=lambda x: x[0])
    return transactions_time_series


def filter_transactions_by_pay_and_bfd_number(transactions, pay_account_number, bfd_account_number):
    result = []

    for i in range(len(transactions)):
        transaction = transactions[i]
        if transaction[2] == pay_account_number and transaction[3] == bfd_account_number:
            result.append(transaction)

    return result


class LogNormalDistribution:
    def __init__(self, values):
        np_values = np.array(values)
        positive_values = np_values[np_values > 0]
        log_values = np.log(positive_values)
        self.current_sum = np.sum(log_values)
        self.current_n = np.count_nonzero(np_values > 0)
        self.current_square_sum = np.sum(np.square(log_values))
        self.mu = np.sum(log_values) / self.current_n
        self.sigma = max(0.0, np.sum(np.square(log_values - self.mu)) / max(1, self.current_n - 1))
        self.distribution = self.get_distribution()

    def get_distribution(self):
        mu = self._calculate_mu()
        sigma = self._calculate_sigma(mu)
        return lognorm(loc=0, s=sigma, scale=math.exp(mu))

    def _calculate_mu(self):
        return self.mu

    def _calculate_sigma(self, mu):
        return self.sigma

    # def _calculate_mu(self):
    #     return self.current_sum / self.current_n
    #
    # def _calculate_sigma(self, mu):
    #     sigma_n = self.current_n - 1
    #     if sigma_n < 1:
    #         sigma_n = 1
    #     return (self.current_square_sum - self.current_n * math.pow(mu, 2)) / sigma_n

    def update_distribution(self, value):
        if value <= 0:
            return
        log_value = math.log(value)
        self.current_sum += log_value
        self.current_n += 1
        self.current_square_sum += log_value ** 2
        self.distribution = self.get_distribution()

    def calculate_p_value(self, value):
        if value < 1e-7:
            return 1.0
        if self.sigma <= 1e-7:
            if abs(math.log(value) - self.mu) <= 1e-7:
                return 1.0
            return max(0.0, 1.0 - abs(math.log(value) - self.mu) / 0.5 * self.mu)
        return 1 - self.distribution.cdf(value)

    def calculate_probability(self, range_start, range_end):
        if range_start < 1e-7:
            start_cdf = 0.0
        else:
            start_cdf = self.distribution.cdf(range_start)
        return self.distribution.cdf(range_end) - start_cdf

    def calculate_likelihood(self, value):
        if self.sigma <= 1e-7:
            if abs(math.log(value) - self.mu) <= 1e-7:
                return 1.0
            return max(0.0, 1.0 - abs(math.log(value) - self.mu) / 0.5 * self.mu)
        likelihood = self.distribution.pdf(value) / self.distribution.pdf(math.exp(self.mu - self.sigma ** 2))
        return likelihood


class AmountsAndCountsAggregate:
    def __init__(self,
                 time_series,
                 start_date,
                 time_delta):
        super(AmountsAndCountsAggregate, self).__init__()

        self.current_amount_sum = 0.0
        self.current_count = 0
        self.aggregated_amounts_series = []
        self.aggregated_counts_series = []

        self.current_date = start_date
        self.next_date = self.current_date + time_delta
        self.time_delta = time_delta

        for date, amount in time_series:
            self.update_amount_and_count_aggregates(date, amount)

    def get_current_amount_series(self):
        if self.current_count == 0:
            return self.aggregated_amounts_series
        return self.aggregated_amounts_series + [(self.current_date, self.current_amount_sum)]

    def get_current_count_series(self):
        if self.current_count == 0:
            return self.aggregated_counts_series
        return self.aggregated_counts_series + [(self.current_date, self.current_count)]

    def get_last_amount(self):
        if self.current_count == 0:
            return self.aggregated_amounts_series[-1]
        return self.current_amount_sum

    def get_last_count(self):
        if self.current_count == 0:
            return self.aggregated_counts_series[-1]
        return self.current_count

    def update_amount_and_count_aggregates(self, date, amount):
        while True:
            if self.current_date <= date < self.current_date + self.time_delta:
                self.current_amount_sum += amount
                self.current_count += 1
                break
            else:
                self.aggregated_amounts_series.append((self.current_date, self.current_amount_sum))
                self.aggregated_counts_series.append((self.current_date, self.current_count))
                self.current_amount_sum = 0.0
                self.current_count = 0
                self.current_date = self.next_date
                self.next_date += self.time_delta


class TransactionClass(Enum):
    Stp = 0
    Anomaly = 1
    UnusualBfd = 2
    Unknown = 3


def classify_transaction(historical_transactions,
                         transaction,
                         rejection_threshold=0.05):
    if len(historical_transactions) < 1:
        return TransactionClass.UnusualBfd, 0.0

    if len(historical_transactions) < 2:
        return TransactionClass.Unknown, 0.0

    train_amounts = [amount for _, amount in historical_transactions]
    amounts_distribution = LogNormalDistribution(train_amounts)

    date = transaction[0]
    amount = transaction[1]

    p_value = amounts_distribution.calculate_p_value(amount)
    if p_value < rejection_threshold:
        return TransactionClass.Anomaly, 0.0

    first_date = historical_transactions[0][0]
    weekly_amounts_and_counts_aggregate = AmountsAndCountsAggregate(historical_transactions + [(date, amount)],
                                                                    datetime.date.fromisocalendar(
                                                                        year=first_date.isocalendar().year,
                                                                        week=first_date.isocalendar().week,
                                                                        day=1),
                                                                    datetime.timedelta(weeks=1))
    if len(weekly_amounts_and_counts_aggregate.get_current_amount_series()) < 2:
        return TransactionClass.Unknown, 0.0
    weekly_amounts_series = weekly_amounts_and_counts_aggregate.get_current_amount_series()[:-1]
    weekly_amounts_distribution = LogNormalDistribution([x for _, x in weekly_amounts_series])
    weekly_counts_series = weekly_amounts_and_counts_aggregate.get_current_count_series()[:-1]
    weekly_counts_distribution = LogNormalDistribution([x for _, x in weekly_counts_series])
    p_value = weekly_amounts_distribution.calculate_p_value(
        weekly_amounts_and_counts_aggregate.get_last_amount())
    if p_value < rejection_threshold:
        return TransactionClass.Anomaly, 0.0
    p_value = weekly_counts_distribution.calculate_p_value(weekly_amounts_and_counts_aggregate.get_last_count())
    if p_value < rejection_threshold:
        return TransactionClass.Anomaly, 0.0

    monthly_amounts_and_counts_aggregate = AmountsAndCountsAggregate(historical_transactions + [(date, amount)],
                                                                     datetime.date(year=first_date.year,
                                                                                   month=first_date.month,
                                                                                   day=1),
                                                                     relativedelta(months=1))
    if len(monthly_amounts_and_counts_aggregate.get_current_amount_series()) < 2:
        return TransactionClass.Unknown, 0.0
    monthly_amounts_series = monthly_amounts_and_counts_aggregate.get_current_amount_series()[:-1]
    monthly_amounts_distribution = LogNormalDistribution([x for _, x in monthly_amounts_series])
    monthly_counts_series = monthly_amounts_and_counts_aggregate.get_current_count_series()[:-1]
    monthly_counts_distribution = LogNormalDistribution([x for _, x in monthly_counts_series])
    p_value = monthly_amounts_distribution.calculate_p_value(
        monthly_amounts_and_counts_aggregate.get_last_amount())
    if p_value < rejection_threshold:
        return TransactionClass.Anomaly, 0.0
    p_value = monthly_counts_distribution.calculate_p_value(
        monthly_amounts_and_counts_aggregate.get_last_count())
    if p_value < rejection_threshold:
        return TransactionClass.Anomaly, 0.0

    confidence = amounts_distribution.calculate_likelihood(amount)
    # if confidence <= 1e-7:
    #     print(historical_transactions)
    #     print(transaction)
    #     print('---------')

    return TransactionClass.Stp, confidence


def format_percentage(percentage):
    return round(percentage * 100, 2)


def main(rejection_threshold=0.05):
    transactions_time_series = parse_csv_file('cheques_in_import_3.csv')

    historical_transactions_length = int(len(transactions_time_series) * 0.7)

    historical_transactions = transactions_time_series[:historical_transactions_length]
    test_transactions = transactions_time_series[historical_transactions_length:]

    historical_transactions_dict = dict()
    for date, amount, pay_account_number, bfd_account_number in historical_transactions:
        if amount < 1e-7:
            continue
        if pay_account_number not in historical_transactions_dict:
            historical_transactions_dict[pay_account_number] = dict()
        if bfd_account_number not in historical_transactions_dict[pay_account_number]:
            historical_transactions_dict[pay_account_number][bfd_account_number] = []
        historical_transactions_dict[pay_account_number][bfd_account_number].append((date, amount))

    # stp_transactions = []
    # anomaly_transactions = []
    # unusual_bfd_transactions = []

    stp_50_transactions = 0
    stp_60_transactions = 0
    stp_70_transactions = 0
    stp_80_transactions = 0
    stp_90_transactions = 0
    anomaly_transactions = 0
    unusual_bfd_transactions = 0
    unknown_transactions = 0
    insufficient_history_transactions = 0
    stp_direct = 0
    test_transactions_count = len(test_transactions)

    progress_bar = tqdm(total=test_transactions_count)
    for test_date, test_amount, test_pay_account_number, test_bfd_account_number in test_transactions:
        # filtered_historical_transactions = filter_transactions_by_pay_and_bfd_number(historical_transactions,
        #                                                                              test_pay_account_number,
        #                                                                              test_bfd_account_number)

        if test_pay_account_number not in historical_transactions_dict:
            historical_transactions_dict[test_pay_account_number] = dict()
        if test_bfd_account_number not in historical_transactions_dict[test_pay_account_number]:
            historical_transactions_dict[test_pay_account_number][test_bfd_account_number] = []

        filtered_historical_transactions = historical_transactions_dict[test_pay_account_number][
            test_bfd_account_number]

        transaction = (test_date,
                       test_amount)

        if len(filtered_historical_transactions) < 4:
            insufficient_history_transactions += 1
            historical_transactions_dict[test_pay_account_number][test_bfd_account_number].append(transaction)
            progress_bar.update()
            continue

        transaction_class, stp_confidence = classify_transaction(filtered_historical_transactions,
                                                                 transaction,
                                                                 rejection_threshold)

        if transaction_class == TransactionClass.Stp:
            stp_direct += 1
            if stp_confidence >= 0.5:
                stp_50_transactions += 1
            if stp_confidence >= 0.6:
                stp_60_transactions += 1
            if stp_confidence >= 0.7:
                stp_70_transactions += 1
            if stp_confidence >= 0.8:
                stp_80_transactions += 1
            if stp_confidence >= 0.9:
                stp_90_transactions += 1
        elif transaction_class == TransactionClass.Anomaly:
            anomaly_transactions += 1
        elif transaction_class == TransactionClass.UnusualBfd:
            unusual_bfd_transactions += 1
        else:
            unknown_transactions += 1

        historical_transactions_dict[test_pay_account_number][test_bfd_account_number].append(transaction)
        progress_bar.update()
    progress_bar.close()

    print(f'number of test transactions: {test_transactions_count}')
    print(f'number of transactions with insufficient history: {format_percentage(insufficient_history_transactions / test_transactions_count)}%')
    print(f'anomaly threshold: {format_percentage(rejection_threshold)}%')
    print(f'anomalous transactions: {format_percentage((anomaly_transactions + unknown_transactions) / test_transactions_count)}%')
    # print(f'transactions with unusual bfd: {format_percentage(unusual_bfd_transactions / test_transactions_count)}%')
    # print(f'stp direct transactions: {format_percentage(stp_direct / test_transactions_count)}%')
    print(f'stp transactions (with thresh 50): {format_percentage(stp_50_transactions / test_transactions_count)}%')
    print(f'stp transactions (with thresh 60): {format_percentage(stp_60_transactions / test_transactions_count)}%')
    print(f'stp transactions (with thresh 70): {format_percentage(stp_70_transactions / test_transactions_count)}%')
    print(f'stp transactions (with thresh 80): {format_percentage(stp_80_transactions / test_transactions_count)}%')
    print(f'stp transactions (with thresh 90): {format_percentage(stp_90_transactions / test_transactions_count)}%')
    # print(f'unknown: {format_percentage(unknown_transactions / test_transactions_count)}%')
    print('================================================================================\n')


if __name__ == '__main__':
    main(0.2)
    main(0.3)
    main(0.4)
    main(0.5)
