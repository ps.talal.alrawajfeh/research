import datetime
import multiprocessing
import os
import random
import time
from concurrent.futures.process import ProcessPoolExecutor

import pandas as pd
import plotly.express as px
from tqdm import tqdm


def parse_date(date_string):
    year, month, day = date_string.split('-')
    year = int(year) + 2000
    month = int(month)
    day = int(day)
    return datetime.date(year=year, month=month, day=day)
    # month, day, year = date_string.split('/')
    # month = int(month)
    # day = int(day)
    # year = int(year)
    # return datetime.date(year=year, month=month, day=day)


def generate_aggregated_daily_amounts_and_counts_plots(account_dir, account_series):
    date_agg_amount_dict = dict()
    date_agg_counts_dict = dict()

    for i in range(len(account_series)):
        date, amount = account_series[i]
        if date not in date_agg_amount_dict:
            date_agg_amount_dict[date] = 0.0
            date_agg_counts_dict[date] = 0
        date_agg_amount_dict[date] += amount
        date_agg_counts_dict[date] += 1

    sorted_dates = [*date_agg_amount_dict]
    sorted_dates.sort()
    first_date = sorted_dates[0]
    last_date = sorted_dates[-1]
    delta = last_date - first_date
    all_dates = []
    all_amounts = []
    all_counts = []
    for i in range(delta.days + 1):
        date = first_date + datetime.timedelta(days=i)
        if date in date_agg_amount_dict:
            all_dates.append(date)
            all_amounts.append(date_agg_amount_dict[date])
            all_counts.append(date_agg_counts_dict[date])
        else:
            all_dates.append(date)
            all_amounts.append(0.0)
            all_counts.append(0)

    fig = px.line(x=all_dates, y=all_amounts)
    fig.write_html(os.path.join(account_dir, 'aggregated_daily_amounts_plot.html'))
    fig = px.line(x=all_dates, y=all_counts)
    fig.write_html(os.path.join(account_dir, 'aggregated_daily_counts_plot.html'))


def get_aggregated_daily_amounts(account_series, account_dir):
    date_agg_amount_dict = dict()
    for i in range(len(account_series)):
        date, amount = account_series[i]
        if date not in date_agg_amount_dict:
            date_agg_amount_dict[date] = 0
        date_agg_amount_dict[date] += amount
    dates = [*date_agg_amount_dict]
    dates.sort()
    amounts = [date_agg_amount_dict[x] for x in dates]

    # with open(os.path.join(account_dir, 'aggregated_amounts_time_series.csv'), 'w') as f:
    #     f.write('date, amount\n')
    #     for i in range(len(dates)):
    #         f.write(f'{dates[i]}, {amounts[i]}\n')

    # fig = px.line(x=dates, y=amounts)
    # fig.write_html(os.path.join(account_dir, 'daily_time_series_plot.html'))
    return date_agg_amount_dict


def generate_original_time_series_report(account_dir, account_series):
    with open(os.path.join(account_dir, 'original_time_series.csv'), 'w') as f:
        f.write('date, amount\n')
        for date, amount in account_series:
            f.write(f'{date}, {amount}\n')
    dates = [date_amount_tuple[0] for date_amount_tuple in account_series]
    amounts = [date_amount_tuple[1] for date_amount_tuple in account_series]
    fig = px.line(x=dates, y=amounts)
    fig.write_html(os.path.join(account_dir, 'original_time_series_plot.html'))


def generate_original_amounts_histogram(account_series, account_dir):
    amounts = [date_amount_tuple[1] for date_amount_tuple in account_series]
    amount_df = pd.DataFrame(amounts, columns=['amounts'])
    fig = px.histogram(amount_df, x='amounts', nbins=20)
    fig.write_html(os.path.join(account_dir, 'original_cheque_amounts_histogram.html'))


def generate_aggregated_amounts_histogram(date_agg_amount_dict, account_dir):
    amounts = date_agg_amount_dict.values()
    amount_df = pd.DataFrame(amounts, columns=['amounts'])
    fig = px.histogram(amount_df, x='amounts', nbins=20)
    fig.write_html(os.path.join(account_dir, 'aggregated_cheque_amounts_histogram.html'))


def generate_bfd_account_numbers_counts_chart(account_rows, account_dir):
    bfd_account_numbers = []
    for row in account_rows:
        bfd_account_numbers.append(row[5])
    bfd_account_number_counts = dict()
    for bfd_account_number in bfd_account_numbers:
        if bfd_account_number not in bfd_account_number_counts:
            bfd_account_number_counts[bfd_account_number] = 0
        bfd_account_number_counts[bfd_account_number] += 1
    data_frame_array = []
    for bfd_account_number in bfd_account_number_counts:
        data_frame_array.append([str(bfd_account_number),
                                 bfd_account_number_counts[bfd_account_number]])
    with open(os.path.join(account_dir, 'bfd_account_numbers.csv'), 'w') as f:
        f.write('bfd_account_number, count\n')
        for i in range(len(data_frame_array)):
            f.write(f'{data_frame_array[i][0]}, {data_frame_array[i][1]}\n')
    data_frame = pd.DataFrame(data_frame_array, columns=['bfd_account_numbers', 'counts'])
    fig = px.bar(data_frame, x='bfd_account_numbers', y='counts')
    fig.write_html(os.path.join(account_dir, 'bfd_account_numbers_counts.html'))


def generate_monthly_aggregated_amounts_and_counts_plots(account_dir, account_series):
    min_date = account_series[0][0]
    current_month = min_date.month
    current_year = min_date.year
    current_sum = 0.0
    current_count = 0
    monthly_aggregated_amounts = []
    monthly_aggregated_counts = []

    i = 0
    while i < len(account_series):
        date, amount = account_series[i]
        if date.month == current_month and date.year == current_year:
            current_sum += amount
            current_count += 1
            i += 1
        else:
            current_date = datetime.date(year=current_year, month=current_month, day=1)
            monthly_aggregated_amounts.append([current_date,
                                               current_sum])
            monthly_aggregated_counts.append([current_date, current_count])
            current_sum = 0.0
            current_count = 0
            current_month += 1
            if current_month > 12:
                current_month = 1
                current_year += 1

    current_date = datetime.date(year=current_year, month=current_month, day=1)
    monthly_aggregated_amounts.append([current_date,
                                       current_sum])
    monthly_aggregated_counts.append([current_date,
                                      current_count])

    dates = [date for date, _ in monthly_aggregated_amounts]
    amounts = [amount for _, amount in monthly_aggregated_amounts]
    counts = [count for _, count in monthly_aggregated_counts]
    fig = px.line(x=dates, y=amounts)
    fig.write_html(os.path.join(account_dir, 'monthly_aggregated_amounts_plot.html'))
    fig = px.line(x=dates, y=counts)
    fig.write_html(os.path.join(account_dir, 'monthly_aggregated_counts_plot.html'))


def last_week_of_year(year):
    last_week = datetime.date(year, 12, 28)
    return last_week.isocalendar().week


def generate_weekly_aggregated_amounts_and_counts_plots(account_dir, account_series):
    min_date = account_series[0][0]
    current_week = min_date.isocalendar().week
    current_year = min_date.year
    current_sum = 0.0
    current_count = 0
    weekly_aggregated_amounts = []
    weekly_aggregated_counts = []

    i = 0
    while i < len(account_series):
        date, amount = account_series[i]
        if date.isocalendar().week == current_week and date.isocalendar().year == current_year:
            current_sum += amount
            current_count += 1
            i += 1
        else:
            current_date = datetime.date.fromisocalendar(year=current_year, week=current_week, day=1)
            weekly_aggregated_amounts.append([current_date,
                                              current_sum])
            weekly_aggregated_counts.append([current_date, current_count])
            current_sum = 0.0
            current_count = 0
            current_week += 1
            if current_week > last_week_of_year(current_year):
                current_week = 1
                current_year += 1

    current_date = datetime.date.fromisocalendar(year=current_year, week=current_week, day=1)
    weekly_aggregated_amounts.append([current_date,
                                      current_sum])
    weekly_aggregated_counts.append([current_date, current_count])

    dates = [date for date, _ in weekly_aggregated_amounts]
    amounts = [amount for _, amount in weekly_aggregated_amounts]
    counts = [count for _, count in weekly_aggregated_counts]
    fig = px.line(x=dates, y=amounts)
    fig.write_html(os.path.join(account_dir, 'weekly_aggregated_amounts_plot.html'))
    fig = px.line(x=dates, y=counts)
    fig.write_html(os.path.join(account_dir, 'weekly_aggregated_counts_plot.html'))


def generate_biweekly_aggregated_amounts_and_counts_plots(account_dir, account_series):
    try:
        min_date = account_series[0][0]
        current_week = min_date.isocalendar().week
        current_year = min_date.year
        current_sum = 0.0
        current_count = 0
        weekly_aggregated_amounts = []
        weekly_aggregated_counts = []

        current_week_date = datetime.date.fromisocalendar(year=current_year,
                                                          week=current_week,
                                                          day=1)

        i = 0
        while i < len(account_series):
            date, amount = account_series[i]
            if current_week_date <= date < current_week_date + datetime.timedelta(weeks=2):
                current_sum += amount
                current_count += 1
                i += 1
            else:
                weekly_aggregated_amounts.append([current_week_date,
                                                  current_sum])
                weekly_aggregated_counts.append([current_week_date, current_count])
                current_sum = 0.0
                current_count = 0
                current_week_date += datetime.timedelta(weeks=2)
        weekly_aggregated_amounts.append([current_week_date,
                                          current_sum])
        weekly_aggregated_counts.append([current_week_date, current_count])

        dates = [date for date, _ in weekly_aggregated_amounts]
        amounts = [amount for _, amount in weekly_aggregated_amounts]
        counts = [count for _, count in weekly_aggregated_counts]
        fig = px.line(x=dates, y=amounts)
        fig.write_html(os.path.join(account_dir, 'biweekly_aggregated_amounts_plot.html'))

        fig = px.line(x=dates, y=counts)
        fig.write_html(os.path.join(account_dir, 'biweekly_aggregated_counts_plot.html'))
    except Exception as e:
        print(e)


def generate_account_reports(account_number,
                             output_path,
                             account_series,
                             account_rows,
                             account_dir):
    if not os.path.isdir(account_dir):
        os.makedirs(account_dir)

    # generate_original_time_series_report(account_dir, account_series)
    # date_agg_amount_dict = get_aggregated_daily_amounts(account_series, account_dir)
    # generate_original_amounts_histogram(account_series, account_dir)
    # generate_aggregated_amounts_histogram(date_agg_amount_dict, account_dir)
    # generate_bfd_account_numbers_counts_chart(account_rows, account_dir)
    generate_aggregated_daily_amounts_and_counts_plots(account_dir, account_series)
    generate_monthly_aggregated_amounts_and_counts_plots(account_dir, account_series)
    generate_weekly_aggregated_amounts_and_counts_plots(account_dir, account_series)
    generate_biweekly_aggregated_amounts_and_counts_plots(account_dir, account_series)

    # uncomment to generate zipped archive
    # shutil.make_archive(os.path.join(output_path, f'{account_number}'),
    #                     'zip',
    #                     account_dir)
    #
    # shutil.rmtree(account_dir)


def update_progress_bar_from_completed_futures(futures,
                                               progress_bar):
    while len(futures) > 0:
        indices = []
        for i in range(len(futures)):
            if futures[i].done():
                indices.append(i)
                progress_bar.update()
        indices.sort(reverse=True)
        for i in indices:
            futures.pop(i)
        time.sleep(1)


def parse_csv_file(file_path):
    data = pd.read_csv(file_path)
    data_array = data.to_numpy()
    accounts_amount_time_series = dict()
    accounts_transactions = dict()
    for i in range(len(data_array)):
        row = data_array[i]
        account_number = row[6]
        date_amount_tuple = (parse_date(row[12]), float(row[8]))
        if account_number not in accounts_amount_time_series:
            accounts_amount_time_series[account_number] = [date_amount_tuple]
        else:
            accounts_amount_time_series[account_number].append(date_amount_tuple)
        if account_number not in accounts_transactions:
            accounts_transactions[account_number] = [row]
        else:
            accounts_transactions[account_number].append(row)
    return accounts_amount_time_series, accounts_transactions


def main():
    print('parsing data...')
    file_path = 'cheques_in_import_3.csv'
    # data = pd.read_csv('/home/u764/Downloads/repeated.csv')
    output_path = '/home/u764/Downloads/tctc/account-transactions'

    if not os.path.isdir(output_path):
        os.makedirs(output_path)

    accounts_amount_time_series, accounts_transactions = parse_csv_file(file_path)

    unique_account_numbers = [*accounts_amount_time_series]
    random.shuffle(unique_account_numbers)

    with ProcessPoolExecutor(max_workers=multiprocessing.cpu_count() - 2) as executor:
        print('extracting information...')
        futures = []
        for account_number in unique_account_numbers:
            account_series = accounts_amount_time_series[account_number]
            account_series.sort(key=lambda x: x[0])
            account_dir = os.path.join(output_path, str(account_number))
            account_rows = accounts_transactions[account_number]

            # comment the following two lines to include all accounts
            if len(account_rows) < 300:
                continue

            future = executor.submit(generate_account_reports,
                                     account_number,
                                     output_path,
                                     account_series,
                                     account_rows,
                                     account_dir)
            futures.append(future)

            # comment the following two lines to include all accounts
            if len(futures) == 100:
                break

        progress_bar = tqdm(total=len(futures))
        update_progress_bar_from_completed_futures(futures, progress_bar)
        progress_bar.close()


if __name__ == '__main__':
    main()
