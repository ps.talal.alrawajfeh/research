To understand the contents of this folder read the following points: 

* Directory names are account numbers (cheque pay account number).
* In each directory you will find the following files:
	* 'original_time_series_plot.html': plot of the time series from the transactions dates and amounts (note that a date may appear more than once)
	* 'original_time_series.csv': the same data in the plot file but in csv format
	* 'original_cheque_amounts_histogram.html': histogram of the transaction amounts distribution
	* 'bfd_account_numbers_counts.html': box plot of the bfd account number occurrences among hte transactions
	* 'bfd_account_numbers.csv': all the bfd account numbers that occurred in the transactions with their number of occurrence
	* 'aggregated_amounts_time_series_plot.html': plot of the time series from the transactions aggregated by the same date with the amount sum (note that each date only appears once)
	* 'aggregated_amounts_time_series.csv': the same data in the plot file but in csv format
	* 'aggregated_cheque_amounts_histogram.html': histogram of the transaction amounts that are aggregated by the same date
	* 'all_dates_aggregated_amounts_time_series_plot.html': plot of the time series from the transactions aggregated by the same date with the amount sum including all the dates within the range of the first and last transaction. If the date does not appear in the data the amount associated with it will be zero
	* 'all_dates_aggregated_amounts_time_series.csv': the same data in the plot file but in csv format
