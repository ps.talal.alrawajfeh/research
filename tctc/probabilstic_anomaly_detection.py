import datetime
import json
import math
import os.path
import sys
import time
from datetime import timedelta

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from dateutil.relativedelta import relativedelta
from scipy.stats import lognorm
from sklearn.cluster import DBSCAN
from sklearn.ensemble import IsolationForest
from sklearn.mixture import BayesianGaussianMixture
from sklearn.svm import OneClassSVM
from terminaltables import AsciiTable
from tqdm import tqdm
from sklearn.covariance import MinCovDet, EllipticEnvelope

DEFAULT_ZERO_STD_AMOUNT_TOLERANCE = 0.1

from forecasting import FourierSeriesForecastingModel, dates_to_time_series

EPSILON = 1e-6
HISTORICAL_TRANSACTIONS_PERCENTAGE = 0.7
DEFAULT_ANOMALY_THRESHOLDS = range(1, 100)  # [1, 5, 10, 20, 30, 40, 50]
ANOMALY_FIELD_NAMES = ['amount_anomalies',
                       'weekly_amount_anomalies',
                       'weekly_count_anomalies',
                       'monthly_amount_anomalies',
                       'monthly_count_anomalies']


def aggregate_amounts_and_counts(amount_time_series,
                                 start_date,
                                 time_delta):
    current_amount_sum = 0.0
    current_count = 0
    aggregated_amounts_series = []
    aggregated_counts_series = []

    current_date = start_date
    next_date = current_date + time_delta

    i = 0
    while i < len(amount_time_series):
        date, amount = amount_time_series[i]

        if current_date <= date < current_date + time_delta:
            current_amount_sum += amount
            current_count += 1
            i += 1
        else:
            aggregated_amounts_series.append((current_date, current_amount_sum))
            aggregated_counts_series.append((current_date, current_count))
            current_amount_sum = 0.0
            current_count = 0
            current_date = next_date
            next_date += time_delta

    aggregated_amounts_series.append((current_date, current_amount_sum))
    aggregated_counts_series.append((current_date, current_count))

    return aggregated_amounts_series, aggregated_counts_series


class AmountsAndCountsAggregate:
    def __init__(self,
                 time_series,
                 start_date,
                 time_delta):
        super(AmountsAndCountsAggregate, self).__init__()

        self.current_amount_sum = 0.0
        self.current_count = 0
        self.aggregated_amounts_series = []
        self.aggregated_counts_series = []

        self.current_date = start_date
        self.next_date = self.current_date + time_delta
        self.time_delta = time_delta

        for date, amount in time_series:
            self.update_amount_and_count_aggregates(date, amount)

    def get_current_amount_series(self):
        if self.current_count == 0:
            return self.aggregated_amounts_series
        return self.aggregated_amounts_series + [(self.current_date, self.current_amount_sum)]

    def get_current_count_series(self):
        if self.current_count == 0:
            return self.aggregated_counts_series
        return self.aggregated_counts_series + [(self.current_date, self.current_count)]

    def get_last_amount(self):
        if self.current_count == 0:
            return self.aggregated_amounts_series[-1]
        return self.current_amount_sum

    def get_last_count(self):
        if self.current_count == 0:
            return self.aggregated_counts_series[-1]
        return self.current_count

    def update_amount_and_count_aggregates(self, date, amount):
        while True:
            if self.current_date <= date < self.current_date + self.time_delta:
                self.current_amount_sum += amount
                self.current_count += 1
                break
            else:
                self.aggregated_amounts_series.append((self.current_date, self.current_amount_sum))
                self.aggregated_counts_series.append((self.current_date, self.current_count))
                self.current_amount_sum = 0.0
                self.current_count = 0
                self.current_date = self.next_date
                self.next_date += self.time_delta


def daily_aggregated_amounts_and_counts(amount_time_series):
    return aggregate_amounts_and_counts(amount_time_series,
                                        amount_time_series[0][0],
                                        timedelta(days=1))


def weekly_aggregated_amounts_and_counts(amount_time_series):
    first_date = amount_time_series[0][0]
    return aggregate_amounts_and_counts(amount_time_series,
                                        datetime.date.fromisocalendar(year=first_date.isocalendar().year,
                                                                      week=first_date.isocalendar().week,
                                                                      day=1),
                                        timedelta(weeks=1))


def biweekly_aggregated_amounts_and_counts(amount_time_series):
    first_date = amount_time_series[0][0]
    return aggregate_amounts_and_counts(amount_time_series,
                                        datetime.date.fromisocalendar(year=first_date.isocalendar().year,
                                                                      week=first_date.isocalendar().week,
                                                                      day=1),
                                        timedelta(weeks=2))


def monthly_aggregated_amounts_and_counts(amount_time_series):
    first_date = amount_time_series[0][0]
    return aggregate_amounts_and_counts(amount_time_series,
                                        datetime.date(year=first_date.year,
                                                      month=first_date.month,
                                                      day=1),
                                        relativedelta(months=1))


def quarterly_aggregated_amounts_and_counts(amount_time_series):
    first_date = amount_time_series[0][0]
    return aggregate_amounts_and_counts(amount_time_series,
                                        datetime.date(year=first_date.year,
                                                      month=first_date.month,
                                                      day=1),
                                        relativedelta(months=4))


def calculate_time_series_histogram(time_series, bins):
    values = [value for _, value in time_series]
    return np.histogram(values, bins=bins)[0]


#
# def parse_date(date_string):
#     year, month, day = date_string.split('-')
#     year = int(year) + 2000
#     month = int(month)
#     day = int(day)
#     return datetime.date(year=year, month=month, day=day)
#
#
# def parse_csv_file(file_path):
#     data = pd.read_csv(file_path)
#     # print(data.columns)
#     # exit()
#     data_array = data.to_numpy()
#     accounts_amount_time_series = dict()
#     accounts_transactions = dict()
#
#     for i in range(len(data_array)):
#         row = data_array[i]
#         account_number = row[6]
#         date_amount_bfd_tuple = (parse_date(row[12]), float(row[8]), row[5])
#         if account_number not in accounts_amount_time_series:
#             accounts_amount_time_series[account_number] = [date_amount_bfd_tuple]
#         else:
#             accounts_amount_time_series[account_number].append(date_amount_bfd_tuple)
#
#         if account_number not in accounts_transactions:
#             accounts_transactions[account_number] = [row]
#         else:
#             accounts_transactions[account_number].append(row)
#
#     return accounts_amount_time_series, accounts_transactions
#

def calculate_log_normal_parameters(values):
    log_values = np.log(values)
    mu = np.mean(log_values)
    sigma = np.sum(np.square(log_values - mu)) / (len(values) - 1)
    return mu, sigma


def get_log_normal_distribution(values):
    mu, sigma = calculate_log_normal_parameters(values)
    return lognorm(loc=0, s=sigma, scale=math.exp(mu))


#
#
# class LogNormalDistribution:
#     def __init__(self, values):
#         np_values = np.array(values)
#         positive_values = np_values[np_values > 0]
#         log_values = np.log(positive_values)
#         self.current_sum = np.sum(log_values)
#         self.current_n = len(positive_values)
#         self.current_square_sum = np.sum(np.square(log_values))
#         self.distribution = self.get_distribution()
#
#     def get_distribution(self):
#         mu = self._calculate_mu()
#         sigma = self._calculate_sigma(mu)
#         return lognorm(loc=0, s=sigma, scale=math.exp(mu))
#
#     def _calculate_mu(self):
#         return self.current_sum / self.current_n
#
#     def _calculate_sigma(self, mu):
#         sigma_n = self.current_n - 1
#         if sigma_n < 1:
#             sigma_n = 1
#         return (self.current_square_sum - self.current_n * mu ** 2) / sigma_n
#
#     def update_distribution(self, value):
#         if value <= 0:
#             return
#         log_value = math.log(value)
#         self.current_sum += log_value
#         self.current_n += 1
#         self.current_square_sum += log_value ** 2
#         self.distribution = self.get_distribution()
#
#     def calculate_p_value(self, value):
#         if value < 1e-7:
#             return 1.0
#         return 1 - self.distribution.cdf(value)
#
#     def calculate_probability(self, range_start, range_end):
#         if range_start < 1e-7:
#             start_cdf = 0.0
#         else:
#             start_cdf = self.distribution.cdf(range_start)
#         return self.distribution.cdf(range_end) - start_cdf
#

def train_test_split(time_series, test_percentage):
    train_percentage = 1 - test_percentage
    train_len = len(time_series) * train_percentage
    train_len = int(math.ceil(train_len))
    return time_series[:train_len], time_series[train_len:]


#
# def test_model_on_time_series(time_series,
#                               rejection_threshold=0.05,
#                               amount_range=0.5,
#                               stp_threshold=0.7,
#                               test_data_percentage=0.3):
#     train_amount_time_series, test_amount_time_series = train_test_split(time_series,
#                                                                          test_data_percentage)
#     rejected_amounts = 0
#     rejected_bfd = 0
#     stp_90_count = 0
#     stp_80_count = 0
#     stp_70_count = 0
#     stp_60_count = 0
#     stp_50_count = 0
#     for date, amount, bfd_account in test_amount_time_series:
#         filtered_series = []
#         for train_date, train_amount, train_bfd_account in train_amount_time_series:
#             if train_bfd_account != bfd_account:
#                 continue
#             filtered_series.append((train_date, train_amount))
#
#         if len(filtered_series) == 0:
#             rejected_bfd += 1
#             train_amount_time_series.append((date, amount, bfd_account))
#             continue
#
#         train_amounts = [amount for date, amount in filtered_series]
#         amounts_distribution = LogNormalDistribution(train_amounts)
#
#         p_value = amounts_distribution.calculate_p_value(amount)
#         if p_value < rejection_threshold:
#             rejected_amounts += 1
#             train_amount_time_series.append((date, amount, bfd_account))
#             continue
#
#         first_date = filtered_series[0][0]
#         weekly_amounts_and_counts_aggregate = AmountsAndCountsAggregate(filtered_series + [(date, amount)],
#                                                                         datetime.date.fromisocalendar(
#                                                                             year=first_date.isocalendar().year,
#                                                                             week=first_date.isocalendar().week,
#                                                                             day=1),
#                                                                         timedelta(weeks=1))
#
#         if len(weekly_amounts_and_counts_aggregate.get_current_amount_series()) < 2:
#             train_amount_time_series.append((date, amount, bfd_account))
#             continue
#
#         weekly_amounts_series = weekly_amounts_and_counts_aggregate.get_current_amount_series()[:-1]
#         weekly_amounts_distribution = LogNormalDistribution([x for _, x in weekly_amounts_series])
#         weekly_counts_series = weekly_amounts_and_counts_aggregate.get_current_count_series()[:-1]
#         weekly_counts_distribution = LogNormalDistribution([x for _, x in weekly_counts_series])
#         p_value = weekly_amounts_distribution.calculate_p_value(
#             weekly_amounts_and_counts_aggregate.get_last_amount())
#         if p_value < rejection_threshold:
#             rejected_amounts += 1
#             train_amount_time_series.append((date, amount, bfd_account))
#             continue
#         p_value = weekly_counts_distribution.calculate_p_value(weekly_amounts_and_counts_aggregate.get_last_count())
#         if p_value < rejection_threshold:
#             rejected_amounts += 1
#             train_amount_time_series.append((date, amount, bfd_account))
#             continue
#
#         monthly_amounts_and_counts_aggregate = AmountsAndCountsAggregate(filtered_series + [(date, amount)],
#                                                                          datetime.date(year=first_date.year,
#                                                                                        month=first_date.month,
#                                                                                        day=1),
#                                                                          relativedelta(months=1))
#
#         if len(monthly_amounts_and_counts_aggregate.get_current_amount_series()) < 2:
#             train_amount_time_series.append((date, amount, bfd_account))
#             continue
#
#         monthly_amounts_series = monthly_amounts_and_counts_aggregate.get_current_amount_series()[:-1]
#         monthly_amounts_distribution = LogNormalDistribution([x for _, x in monthly_amounts_series])
#         monthly_counts_series = monthly_amounts_and_counts_aggregate.get_current_count_series()[:-1]
#         monthly_counts_distribution = LogNormalDistribution([x for _, x in monthly_counts_series])
#
#         p_value = monthly_amounts_distribution.calculate_p_value(
#             monthly_amounts_and_counts_aggregate.get_last_amount())
#         if p_value < rejection_threshold:
#             rejected_amounts += 1
#             train_amount_time_series.append((date, amount, bfd_account))
#             continue
#         p_value = monthly_counts_distribution.calculate_p_value(
#             monthly_amounts_and_counts_aggregate.get_last_count())
#         if p_value < rejection_threshold:
#             rejected_amounts += 1
#             train_amount_time_series.append((date, amount, bfd_account))
#             continue
#
#         confidence = amounts_distribution.calculate_probability(amount * (1 - amount_range),
#                                                                 amount * (1 + amount_range))
#         if confidence > 0.5:
#             stp_50_count += 1
#         if confidence > 0.6:
#             stp_60_count += 1
#         if confidence > 0.7:
#             stp_70_count += 1
#         if confidence > 0.8:
#             stp_80_count += 1
#         if confidence > 0.9:
#             stp_90_count += 1
#         train_amount_time_series.append((date, amount, bfd_account))
#
#     test_count = len(test_amount_time_series)
#     return rejected_amounts, rejected_bfd, (
#         stp_50_count, stp_60_count, stp_70_count, stp_80_count, stp_90_count), test_count


def test_model_on_time_series(time_series,
                              rejection_threshold=0.05,
                              amount_range=0.5,
                              stp_threshold=0.7,
                              test_data_percentage=0.3):
    train_amount_time_series, test_amount_time_series = train_test_split(time_series,
                                                                         test_data_percentage)
    rejected_amounts = 0
    rejected_bfd = 0
    stp_90_count = 0
    stp_80_count = 0
    stp_70_count = 0
    stp_60_count = 0
    stp_50_count = 0
    for date, amount in test_amount_time_series:
        train_amounts = [amount for date, amount in train_amount_time_series]
        amounts_distribution = LogNormalDistribution(train_amounts)

        p_value = amounts_distribution.calculate_p_value(amount)
        if p_value < rejection_threshold:
            rejected_amounts += 1
            train_amount_time_series.append((date, amount))
            continue

        first_date = train_amount_time_series[0][0]
        weekly_amounts_and_counts_aggregate = AmountsAndCountsAggregate(train_amount_time_series + [(date, amount)],
                                                                        datetime.date.fromisocalendar(
                                                                            year=first_date.isocalendar().year,
                                                                            week=first_date.isocalendar().week,
                                                                            day=1),
                                                                        timedelta(weeks=1))

        if len(weekly_amounts_and_counts_aggregate.get_current_amount_series()) < 2:
            train_amount_time_series.append((date, amount))
            continue

        weekly_amounts_series = weekly_amounts_and_counts_aggregate.get_current_amount_series()[:-1]
        weekly_amounts_distribution = LogNormalDistribution([x for _, x in weekly_amounts_series])
        weekly_counts_series = weekly_amounts_and_counts_aggregate.get_current_count_series()[:-1]
        weekly_counts_distribution = LogNormalDistribution([x for _, x in weekly_counts_series])
        p_value = weekly_amounts_distribution.calculate_p_value(
            weekly_amounts_and_counts_aggregate.get_last_amount())
        if p_value < rejection_threshold:
            rejected_amounts += 1
            train_amount_time_series.append((date, amount))
            continue
        p_value = weekly_counts_distribution.calculate_p_value(weekly_amounts_and_counts_aggregate.get_last_count())
        if p_value < rejection_threshold:
            rejected_amounts += 1
            train_amount_time_series.append((date, amount))
            continue

        monthly_amounts_and_counts_aggregate = AmountsAndCountsAggregate(train_amount_time_series + [(date, amount)],
                                                                         datetime.date(year=first_date.year,
                                                                                       month=first_date.month,
                                                                                       day=1),
                                                                         relativedelta(months=1))

        if len(monthly_amounts_and_counts_aggregate.get_current_amount_series()) < 2:
            train_amount_time_series.append((date, amount))
            continue

        monthly_amounts_series = monthly_amounts_and_counts_aggregate.get_current_amount_series()[:-1]
        monthly_amounts_distribution = LogNormalDistribution([x for _, x in monthly_amounts_series])
        monthly_counts_series = monthly_amounts_and_counts_aggregate.get_current_count_series()[:-1]
        monthly_counts_distribution = LogNormalDistribution([x for _, x in monthly_counts_series])

        p_value = monthly_amounts_distribution.calculate_p_value(
            monthly_amounts_and_counts_aggregate.get_last_amount())
        if p_value < rejection_threshold:
            rejected_amounts += 1
            train_amount_time_series.append((date, amount))
            continue
        p_value = monthly_counts_distribution.calculate_p_value(
            monthly_amounts_and_counts_aggregate.get_last_count())
        if p_value < rejection_threshold:
            rejected_amounts += 1
            train_amount_time_series.append((date, amount))
            continue

        confidence = amounts_distribution.calculate_probability(amount * (1 - amount_range),
                                                                amount * (1 + amount_range))
        confidence = amounts_distribution.calculate_p_value(amount)
        if confidence > 0.5:
            stp_50_count += 1
        if confidence > 0.6:
            stp_60_count += 1
        if confidence > 0.7:
            stp_70_count += 1
        if confidence > 0.8:
            stp_80_count += 1
        if confidence > 0.9:
            stp_90_count += 1
        train_amount_time_series.append((date, amount))

    test_count = len(test_amount_time_series)
    return rejected_amounts, rejected_bfd, (
        stp_50_count, stp_60_count, stp_70_count, stp_80_count, stp_90_count), test_count


def test_one_class_svm_model(time_series):
    feature_length = 9
    test_data_percentage = 0.3

    train_amount_time_series, test_amount_time_series = train_test_split(time_series,
                                                                         test_data_percentage)
    first_date = train_amount_time_series[0][0]

    weekly_amounts_and_counts_aggregate = AmountsAndCountsAggregate(train_amount_time_series,
                                                                    datetime.date.fromisocalendar(
                                                                        year=first_date.isocalendar().year,
                                                                        week=first_date.isocalendar().week,
                                                                        day=1),
                                                                    timedelta(weeks=1))

    weekly_amounts_series = weekly_amounts_and_counts_aggregate.get_current_amount_series()[1:-1]
    weekly_counts_series = weekly_amounts_and_counts_aggregate.get_current_count_series()[1:-1]

    train_amounts = [amount for date, amount in weekly_amounts_series]
    test_amounts = [amount for date, amount in weekly_amounts_series]
    amounts_windows = []
    for i in range(feature_length):
        amounts_windows.append(train_amounts[i: len(train_amounts) + i - feature_length + 1])
    amounts_windows = np.expand_dims(amounts_windows, -1)
    amounts_windows = np.concatenate(amounts_windows, -1)
    amount_model = OneClassSVM(gamma='auto').fit(amounts_windows)

    train_counts = [count for date, count in weekly_counts_series]
    test_counts = [count for date, count in weekly_counts_series]
    counts_windows = []
    for i in range(feature_length):
        counts_windows.append(train_counts[i: len(train_counts) + i - feature_length + 1])
    counts_windows = np.expand_dims(counts_windows, -1)
    counts_windows = np.concatenate(counts_windows, -1)
    dbscan = DBSCAN(eps=0.57)
    count_model = OneClassSVM(gamma='auto').fit(counts_windows)

    error = 0
    for i in range(len(train_amounts) - feature_length + 1):
        if i < feature_length - 1:  # i = 0, 2 - 1 = 1, 0 < 1
            window = train_amounts[-feature_length + i + 1:] + test_amounts[:i + 1]
            print(window)
            print(amount_model.predict([window]))

    return error, len(test_amount_time_series)


def test_forecasting_model(amount_time_series,
                           amount_range_percentage=0.5,
                           tolerance=0.5,
                           test_percentage=0.3):
    train_amount_time_series, test_amount_time_series = train_test_split(amount_time_series,
                                                                         test_percentage)

    error = 0
    test_count = len(test_amount_time_series)
    counts = []
    for i in range(len(test_amount_time_series)):
        test_date, test_amount, test_bfd_account = test_amount_time_series[i]

        min_amount = test_amount * (1 - amount_range_percentage)
        max_amount = test_amount * (1 + amount_range_percentage)

        filtered_series = dict()

        for j in range(len(train_amount_time_series)):
            date, amount, bfd_account = train_amount_time_series[j]

            if min_amount <= amount <= max_amount and int(bfd_account) == int(test_bfd_account):
                if date not in filtered_series:
                    filtered_series[date] = amount

        filtered_dates = [*filtered_series]
        filtered_dates.sort()

        counts.append(len(filtered_dates))

        if len(filtered_dates) < 2:
            error += 1
            continue
        # continue
        date_deltas_series = dates_to_time_series(filtered_dates)
        model = FourierSeriesForecastingModel(date_deltas_series)
        next_delta = model.forecast()
        # mu = np.mean(date_deltas_series)
        # sigma = np.std(date_deltas_series)
        # next_delta = date_deltas_series[-1] + mu
        # tolerance_start = int(round(next_delta - sigma * (1 + tolerance)))
        # tolerance_end = int(round(next_delta + sigma * (1 + tolerance)))

        tolerance_start = int(round(next_delta * (1 + tolerance)))
        tolerance_end = int(round(next_delta * (1 + tolerance)))

        current_delta = (test_date - filtered_dates[-1]).days

        # if len(filtered_dates) >= 10:
        #     file_name = str(uuid.uuid1())
        #     points_count = len(date_deltas_series)
        #     plt.plot(list(range(points_count)), date_deltas_series)
        #     plt.title(f'number of points: {points_count}')
        #     # plt.show()
        #     path = f'/home/u764/Downloads/test/{points_count}'
        #     if not os.path.isdir(path):
        #         os.makedirs(path)
        #     plt.savefig(os.path.join(path, f'{file_name}.jpg'), dpi=100)
        #     plt.close('all')
        if current_delta < tolerance_start or current_delta > tolerance_end:
            error += 1

        train_amount_time_series.append(test_amount_time_series[0])

    return error, test_count, counts


def get_upper_and_lower_bounds(time_series,
                               window_size):
    lower_bounds = []
    upper_bounds = []
    for i in range(window_size, len(time_series) - 1):
        window = time_series[i - window_size: i]
        amounts = [amount for date, amount in window]
        mean = np.mean(amounts)
        std = np.std(amounts)
        lower_bound = mean - std
        upper_bound = mean + std
        lower_bounds.append((time_series[i][0], lower_bound))
        upper_bounds.append((time_series[i][0], upper_bound))

    return lower_bounds, upper_bounds


def draw_aggregations(account_number, amount_time_series):
    if not os.path.isdir(f'/home/u764/Downloads/test/{account_number}'):
        os.makedirs(f'/home/u764/Downloads/test/{account_number}')
    first_date = amount_time_series[0][0]
    weekly_amounts_and_counts_aggregate = AmountsAndCountsAggregate(amount_time_series,
                                                                    datetime.date.fromisocalendar(
                                                                        year=first_date.isocalendar().year,
                                                                        week=first_date.isocalendar().week,
                                                                        day=1),
                                                                    timedelta(weeks=1))
    series = weekly_amounts_and_counts_aggregate.get_current_amount_series()
    plt.plot([date for date, amount in series], [amount for date, amount in series])
    lower_bounds, upper_bounds = get_upper_and_lower_bounds(series, 10)
    plt.plot([date for date, amount in lower_bounds], [amount for date, amount in lower_bounds], color='red')
    plt.plot([date for date, amount in upper_bounds], [amount for date, amount in upper_bounds], color='red')
    plt.title('weekly amounts')
    plt.savefig(os.path.join(f'/home/u764/Downloads/test/{account_number}', f'weekly_amounts.jpg'), dpi=100)
    plt.close('all')
    series = weekly_amounts_and_counts_aggregate.get_current_count_series()
    plt.plot([date for date, amount in series], [amount for date, amount in series])
    lower_bounds, upper_bounds = get_upper_and_lower_bounds(series, 10)
    plt.plot([date for date, amount in lower_bounds], [amount for date, amount in lower_bounds], color='red')
    plt.plot([date for date, amount in upper_bounds], [amount for date, amount in upper_bounds], color='red')
    plt.title('weekly counts')
    plt.savefig(os.path.join(f'/home/u764/Downloads/test/{account_number}', f'weekly_counts.jpg'), dpi=100)
    plt.close('all')
    plt.show()
    monthly_amounts_and_counts_aggregate = AmountsAndCountsAggregate(amount_time_series,
                                                                     datetime.date(year=first_date.year,
                                                                                   month=first_date.month,
                                                                                   day=1),
                                                                     relativedelta(months=1))
    series = monthly_amounts_and_counts_aggregate.get_current_amount_series()
    plt.plot([date for date, amount in series], [amount for date, amount in series])
    lower_bounds, upper_bounds = get_upper_and_lower_bounds(series, 10)
    plt.plot([date for date, amount in lower_bounds], [amount for date, amount in lower_bounds], color='red')
    plt.plot([date for date, amount in upper_bounds], [amount for date, amount in upper_bounds], color='red')
    plt.title('monthly amounts')
    plt.savefig(os.path.join(f'/home/u764/Downloads/test/{account_number}', f'monthly_amounts.jpg'), dpi=100)
    plt.close('all')
    series = monthly_amounts_and_counts_aggregate.get_current_count_series()
    lower_bounds, upper_bounds = get_upper_and_lower_bounds(series, 10)
    plt.plot([date for date, amount in lower_bounds], [amount for date, amount in lower_bounds], color='red')
    plt.plot([date for date, amount in upper_bounds], [amount for date, amount in upper_bounds], color='red')
    plt.plot([date for date, amount in series], [amount for date, amount in series])
    plt.title('monthly counts')
    plt.savefig(os.path.join(f'/home/u764/Downloads/test/{account_number}', f'monthly_counts.jpg'), dpi=100)
    plt.close('all')
    plt.show()


def main():
    accounts_amount_time_series, accounts_transactions = parse_csv_file('cheques_in_import_3.csv')

    total_error = 0
    total_count = 0
    total_rejected_amounts = 0
    total_rejected_bfd = 0
    total_stp_90_count = 0
    total_stp_80_count = 0
    total_stp_70_count = 0
    total_stp_60_count = 0
    total_stp_50_count = 0
    progress_bar = tqdm(total=len(accounts_amount_time_series))
    accounts_count = 0
    all_counts = []

    series_lengths = []
    for account_number in accounts_amount_time_series:
        # if len(accounts_amount_time_series[account_number]) > 200:
        #     continue
        series_lengths.append(len(accounts_amount_time_series[account_number]))
        if len(accounts_amount_time_series[account_number]) >= 4:
            amount_time_series = accounts_amount_time_series[account_number]
            amount_time_series.sort(key=lambda x: x[0])
            rejected_amounts, rejected_bfd, stp_counts, test_count = test_model_on_time_series(amount_time_series)

            # error, test_count = test_one_class_svm_model(amount_time_series)
            # error, test_count, counts = test_forecasting_model(amount_time_series)
            # all_counts.extend(counts)
            total_error += rejected_amounts + rejected_bfd
            total_rejected_amounts += rejected_amounts
            total_rejected_bfd += rejected_bfd
            total_count += test_count
            total_stp_90_count += stp_counts[4]
            total_stp_80_count += stp_counts[3]
            total_stp_70_count += stp_counts[2]
            total_stp_60_count += stp_counts[1]
            total_stp_50_count += stp_counts[0]

            # print(total_error / max(1, total_count))
            # draw_aggregations(account_number, amount_time_series)
            # attributes_dict = dict()
            #
            # transactions = accounts_transactions[account_number]
            # for transaction in transactions:
            #     key = tuple(transaction[1:7])
            #     if key not in attributes_dict:
            #         attributes_dict[key] = 1
            #     else:
            #         attributes_dict[key] += 1
            #
            # all_counts.append(np.mean(list(attributes_dict.values())))

            accounts_count += 1
        progress_bar.update()
    progress_bar.close()
    print(accounts_count)
    print('total error', total_error / max(1, total_count))
    print('rejection rate (amounts): ', total_rejected_amounts / max(1, total_count))
    print('rejection rate (bfd): ', total_rejected_bfd / max(1, total_count))
    print('stp rate 90%: ', total_stp_90_count / max(1, total_count))
    print('stp rate 80%: ', total_stp_80_count / max(1, total_count))
    print('stp rate 70%: ', total_stp_70_count / max(1, total_count))
    print('stp rate 60%: ', total_stp_60_count / max(1, total_count))
    print('stp rate 50%: ', total_stp_50_count / max(1, total_count))
    # plt.hist(all_counts, bins=len(np.unique(np.round(all_counts))))
    # plt.show()

    # series_lengths = np.array(series_lengths)
    # print(len(series_lengths[series_lengths > 1]))
    # plt.hist(series_lengths, bins=len(np.unique(series_lengths)))
    # plt.show()


def main3():
    accounts_amount_time_series, accounts_transactions = parse_csv_file('cheques_in_import_3.csv')
    data_point_counts = []
    for account_number in accounts_amount_time_series:
        amount_time_series = accounts_amount_time_series[account_number]
        amount_time_series.sort(key=lambda x: x[0])
        train_amount_time_series, test_amount_time_series = train_test_split(amount_time_series,
                                                                             0.3)
        for date, amount, bfd_account in test_amount_time_series:
            filtered_series = []
            for train_date, train_amount, train_bfd_account in train_amount_time_series:
                if train_bfd_account != bfd_account:
                    continue
                filtered_series.append((train_date, train_amount))

            data_point_counts.append(len(filtered_series))

    print(np.mean(data_point_counts))
    print(np.std(data_point_counts))

    print(np.count_nonzero(np.array(data_point_counts) < 20) / len(data_point_counts))


def main4():
    accounts_amount_time_series, accounts_transactions = parse_csv_file('cheques_in_import_3.csv')
    data_point_counts = []
    series_lengths = []
    for account_number in accounts_amount_time_series:
        amount_time_series = accounts_amount_time_series[account_number]
        amount_time_series.sort(key=lambda x: x[0])
        series_lengths.append(len(amount_time_series))
        bfd_transactions = dict()
        for date, amount, bfd_account in amount_time_series:
            if bfd_account not in bfd_transactions:
                bfd_transactions[bfd_account] = []
            bfd_transactions[bfd_account].append((date, amount))

        for bfd_account in bfd_transactions:
            data_point_counts.append(len(bfd_transactions[bfd_account]))

    print(np.mean(data_point_counts))
    print(np.std(data_point_counts))
    print(np.count_nonzero(np.array(data_point_counts) <= 1) / len(data_point_counts))
    print(np.count_nonzero(np.array(data_point_counts) >= 4) / len(data_point_counts))
    print(len(data_point_counts))
    print('----')
    print(np.mean(series_lengths))
    print(np.std(series_lengths))
    print('====')


def log_normal(mu, sigma, x):
    if x <= 0:
        return 0.0

    return math.exp(-(math.log(x) - mu) ** 2 / (2 * sigma ** 2)) / (x * sigma * math.sqrt(2 * math.pi))


def gmm_bic_score(estimator, sample):
    return -estimator.bic(sample)


def calculate_mixture_model_parameters(sample, mixtures, tolerance=0.001):
    # param_grid = {
    #     'n_components': range(1, min(7, len(sample) // 2)),
    # }
    # print(len(sample))
    # grid_search = GridSearchCV(
    #     BayesianGaussianMixture(), param_grid=param_grid, scoring=gmm_bic_score, cv=5
    # )
    arr = np.array(sample, np.float32).reshape(-1, 1)
    # result = grid_search.fit(arr)

    result = BayesianGaussianMixture(n_components=min(6, len(sample) // 2)).fit(arr)
    # print(result.best_params_)
    n_clusters_ = (np.round(result.weights_, 2) > 0).sum()
    print(n_clusters_)


def main2():
    calculate_mixture_model_parameters(
        [99, 100, 101, 102, 100, 100, 100, 99, 10000, 10001, 10050, 10000, 10000], 2)


def main5():
    account_to_transaction_time_series_dict, accounts_transactions = parse_csv_file('cheques_in_import_3.csv')

    total_error = 0
    total_count = 0
    total_rejected_amounts = 0
    total_rejected_bfd = 0
    total_stp_90_count = 0
    total_stp_80_count = 0
    total_stp_70_count = 0
    total_stp_60_count = 0
    total_stp_50_count = 0
    progress_bar = tqdm(total=len(account_to_transaction_time_series_dict))
    accounts_count = 0
    all_counts = []

    for pay_account_number in account_to_transaction_time_series_dict:
        transaction_time_series = account_to_transaction_time_series_dict[pay_account_number]
        transaction_time_series.sort(key=lambda x: x[0])
        bfd_to_transactions_dict = dict()
        for date, amount, bfd_account_number in transaction_time_series:
            if bfd_account_number not in bfd_to_transactions_dict:
                bfd_to_transactions_dict[bfd_account_number] = []
            bfd_to_transactions_dict[bfd_account_number].append((date, amount))

        for bfd_account_number in bfd_to_transactions_dict:
            amount_time_series = bfd_to_transactions_dict[bfd_account_number]
            if len(amount_time_series) >= 4:
                amount_time_series.sort(key=lambda x: x[0])
                rejected_amounts, rejected_bfd, stp_counts, test_count = test_model_on_time_series(amount_time_series)

                total_error += rejected_amounts + rejected_bfd
                total_rejected_amounts += rejected_amounts
                total_rejected_bfd += rejected_bfd
                total_count += test_count
                total_stp_90_count += stp_counts[4]
                total_stp_80_count += stp_counts[3]
                total_stp_70_count += stp_counts[2]
                total_stp_60_count += stp_counts[1]
                total_stp_50_count += stp_counts[0]

        accounts_count += 1
        progress_bar.update()
    progress_bar.close()
    print(accounts_count)
    print('total error', total_error / max(1, total_count))
    print('rejection rate (amounts): ', total_rejected_amounts / max(1, total_count))
    print('rejection rate (bfd): ', total_rejected_bfd / max(1, total_count))
    print('stp rate 90%: ', total_stp_90_count / max(1, total_count))
    print('stp rate 80%: ', total_stp_80_count / max(1, total_count))
    print('stp rate 70%: ', total_stp_70_count / max(1, total_count))
    print('stp rate 60%: ', total_stp_60_count / max(1, total_count))
    print('stp rate 50%: ', total_stp_50_count / max(1, total_count))


class DynamicHistogram:
    def __init__(self,
                 data_points,
                 distance_metric=lambda x, y: abs(x - y),
                 max_distance=None,
                 distance_range_ratio=10):
        if (max_distance is None and distance_range_ratio is None) or \
                (max_distance is not None and distance_range_ratio is not None):
            raise ValueError(
                'One of max_distance and distance_range_ratio should be None and the other one should not be None')
        self.data_points = data_points
        self.distance_metric = distance_metric

        self.distance_range_ratio = distance_range_ratio
        if max_distance is None:
            self.max_distance = None
        else:
            self.max_distance = max_distance

        self._initialize_histogram()

    def _initialize_histogram(self):
        data_points_copy = self.data_points.copy()
        self.data_points_bin_labels = dict()
        self.current_label = 0
        self.labels_count = 0

        while len(data_points_copy) > 0:
            point = data_points_copy.pop(0)
            if point in self.data_points_bin_labels:
                continue
            self.data_points_bin_labels[point] = self.current_label
            queue = [point]
            while len(queue) > 0:
                current_point = queue.pop(0)
                for i in range(len(self.data_points)):
                    point = self.data_points[i]
                    if self.max_distance is not None:
                        max_distance = self.max_distance
                    else:
                        max_distance = np.max([current_point, point], axis=0) / self.distance_range_ratio
                    if point not in self.data_points_bin_labels and \
                            self.distance_metric(current_point, point) < max_distance:
                        self.data_points_bin_labels[point] = self.current_label
                        queue.append(point)
            self.current_label += 1
            self.labels_count += 1

        self.current_label -= 1
        self._calculate_histogram_and_frequencies()

    def _calculate_histogram_and_frequencies(self):
        self.histogram = [list() for _ in range(self.labels_count)]
        self.frequencies = [0 for _ in range(self.labels_count)]

        for point in self.data_points:
            label = self.data_points_bin_labels[point]
            self.histogram[label].append(point)
            self.frequencies[label] += 1

        # sorting_array = [(np.mean(self.histogram[i]), self.histogram[i], self.frequencies[i])
        #                  for i in range(self.labels_count)]
        # sorting_array.sort(key=lambda x: x[0])
        #
        # self.histogram = [x[1] for x in sorting_array]
        # self.frequencies = [x[2] for x in sorting_array]

    def get_histogram_and_frequencies(self):
        return self.histogram, self.frequencies

    def get_point_histogram_bin_and_frequency(self, new_data_point):
        is_found = False
        label = None
        for point in self.data_points:
            if self.max_distance is not None:
                max_distance = self.max_distance
            else:
                max_distance = np.max([new_data_point, point], axis=0) / self.distance_range_ratio
            if self.distance_metric(new_data_point, point) < max_distance:
                label = self.data_points_bin_labels[point]
                is_found = True
                break
        if not is_found:
            return [], 0
        return self.histogram[label], self.frequencies[label]

    def get_max_frequency(self):
        return np.max(self.frequencies)


def calculate_likelihood(histogram, data_point):
    histogram_bin, frequency = histogram.get_point_histogram_bin_and_frequency(data_point)
    max_frequency = histogram.get_max_frequency()

    return frequency / max_frequency


def group_transactions_by_pay_and_bfd_numbers(transactions):
    historical_transactions_dict = dict()
    for date, amount, pay_account_number, bfd_account_number in transactions:
        if amount < EPSILON:
            continue
        if pay_account_number not in historical_transactions_dict:
            historical_transactions_dict[pay_account_number] = dict()
        if bfd_account_number not in historical_transactions_dict[pay_account_number]:
            historical_transactions_dict[pay_account_number][bfd_account_number] = []
        historical_transactions_dict[pay_account_number][bfd_account_number].append((date, amount))
    return historical_transactions_dict


def initialize_statistics_dict():
    statistics = {
        'unusual_bfd_transactions': 0,
        'insufficient_history_transactions': 0,
        'rejected_transactions': 0
    }
    for anomaly_threshold in DEFAULT_ANOMALY_THRESHOLDS:
        key = f'anomaly_{anomaly_threshold}_thresh'
        statistics[key] = {
            'anomalous_transactions': 0
        }
        for i in range(len(ANOMALY_FIELD_NAMES)):
            statistics[key][ANOMALY_FIELD_NAMES[i]] = 0

    return statistics


def get_amounts_and_counts_series(amounts_and_counts_aggregate):
    if len(amounts_and_counts_aggregate.get_current_amount_series()) < 2:
        return None

    amounts_series = [x for _, x in amounts_and_counts_aggregate.get_current_amount_series()[:-1]]
    counts_series = [x for _, x in amounts_and_counts_aggregate.get_current_count_series()[:-1]]

    return amounts_series, counts_series


class LogNormalDistribution:
    def __init__(self,
                 values,
                 zero_std_amount_tolerance=DEFAULT_ZERO_STD_AMOUNT_TOLERANCE):
        np_values = np.array(values, np.float64)
        non_negative_values = np_values[np_values > EPSILON]
        non_negative_values[non_negative_values < 1.0] = 1.0
        log_values = np.log(non_negative_values)
        self.zero_std_amount_tolerance = zero_std_amount_tolerance
        self.mu = np.mean(log_values)
        self.sigma = np.max([0.0, np.std(log_values)])
        self.distribution = self._get_distribution()

    def _get_distribution(self):
        return lognorm(loc=0, s=self.sigma, scale=math.exp(self.mu))

    def _calculate_probability_for_zero_std(self, value):
        exp_mu = math.exp(self.mu)
        return max(0.0, 1.0 - abs(value - exp_mu) / self.zero_std_amount_tolerance * exp_mu)

    def _likelihood_ratio(self, value):
        return self.distribution.pdf(value) / self.distribution.pdf(math.exp(self.mu - self.sigma ** 2))

    def calculate_p_value(self, value):
        if value < EPSILON:
            return 1.0
        if self.sigma <= EPSILON:
            return self._calculate_probability_for_zero_std(value)
        return 1 - self.distribution.cdf(value)

    def calculate_confidence(self, value):
        if self.sigma <= EPSILON:
            return self._calculate_probability_for_zero_std(value)
        if value < EPSILON:
            value = EPSILON
        return self._likelihood_ratio(value)


def calculate_value_likelihood(historical_values, new_value):
    if len(historical_values) >= 20:
        train_histogram = DynamicHistogram(historical_values)
        amount_likelihood = calculate_likelihood(train_histogram, new_value)
    else:
        amount_likelihood = LogNormalDistribution(historical_values).calculate_confidence(new_value)
    return amount_likelihood


def calculate_p_value(historical_values, new_value):
    return LogNormalDistribution(historical_values).calculate_p_value(new_value)


def calculate_transaction_likelihood_value(historical_transactions,
                                           transaction):
    if len(historical_transactions) < 4:
        return None

    train_amounts = [amount for _, amount in historical_transactions]

    date = transaction[0]
    amount = transaction[1]

    amount_likelihood = calculate_value_likelihood(train_amounts, amount)

    first_date = historical_transactions[0][0]
    weekly_amounts_and_counts_aggregate = AmountsAndCountsAggregate(historical_transactions + [(date, amount)],
                                                                    datetime.date.fromisocalendar(
                                                                        year=first_date.isocalendar().year,
                                                                        week=first_date.isocalendar().week, day=1),
                                                                    datetime.timedelta(weeks=1))

    result = get_amounts_and_counts_series(weekly_amounts_and_counts_aggregate)
    if result is None:
        return result
    weekly_amounts, weekly_counts = result
    weekly_amount_likelihood = calculate_p_value(weekly_amounts,
                                                 weekly_amounts_and_counts_aggregate.get_last_amount())
    weekly_count_likelihood = calculate_p_value(weekly_counts,
                                                weekly_amounts_and_counts_aggregate.get_last_count())

    monthly_amounts_and_counts_aggregate = AmountsAndCountsAggregate(historical_transactions + [(date, amount)],
                                                                     datetime.date(year=first_date.year,
                                                                                   month=first_date.month, day=1),
                                                                     relativedelta(months=1))

    result = get_amounts_and_counts_series(monthly_amounts_and_counts_aggregate)
    if result is None:
        return result
    monthly_amounts, monthly_counts = result

    monthly_amount_likelihood = calculate_p_value(monthly_amounts,
                                                  monthly_amounts_and_counts_aggregate.get_last_amount())
    monthly_count_likelihood = calculate_p_value(monthly_counts,
                                                 monthly_amounts_and_counts_aggregate.get_last_count())

    return (amount_likelihood,
            weekly_amount_likelihood,
            weekly_count_likelihood,
            monthly_amount_likelihood,
            monthly_count_likelihood)


def calculate_anomaly_report_statistics(transactions_time_series):
    historical_transactions_length = int(len(transactions_time_series) * HISTORICAL_TRANSACTIONS_PERCENTAGE)

    historical_transactions = transactions_time_series[:historical_transactions_length]
    test_transactions = transactions_time_series[historical_transactions_length:]
    historical_transactions_groups = group_transactions_by_pay_and_bfd_numbers(historical_transactions)
    test_transactions_count = len(test_transactions)

    statistics = initialize_statistics_dict()
    statistics['historical_transactions'] = historical_transactions_length
    statistics['test_transactions'] = test_transactions_count

    progress_bar = tqdm(total=test_transactions_count)
    for test_date, test_amount, test_pay_account_number, test_bfd_account_number in test_transactions:
        if test_pay_account_number not in historical_transactions_groups:
            historical_transactions_groups[test_pay_account_number] = dict()
        if test_bfd_account_number not in historical_transactions_groups[test_pay_account_number]:
            historical_transactions_groups[test_pay_account_number][test_bfd_account_number] = []

        pay_to_bfd_historical_transactions = historical_transactions_groups[test_pay_account_number][
            test_bfd_account_number]

        if len(pay_to_bfd_historical_transactions) < 1:
            statistics['unusual_bfd_transactions'] += 1

        if len(pay_to_bfd_historical_transactions) < 4:
            statistics['insufficient_history_transactions'] += 1

        transaction = (test_date,
                       test_amount,
                       test_pay_account_number,
                       test_bfd_account_number)
        result = calculate_transaction_likelihood_value([(t[0], t[1])
                                                         for t in pay_to_bfd_historical_transactions],
                                                        transaction[:2])

        if result is None:
            historical_transactions_groups[test_pay_account_number][test_bfd_account_number].append(transaction)
            statistics['rejected_transactions'] += 1
            progress_bar.update()
            continue
        likelihood = result

        for anomaly_threshold in DEFAULT_ANOMALY_THRESHOLDS:
            key = f'anomaly_{anomaly_threshold}_thresh'
            anomaly_percentage_threshold = anomaly_threshold / 100
            if min(likelihood) < anomaly_percentage_threshold:
                statistics[key]['anomalous_transactions'] += 1
            for i in range(len(likelihood)):
                if likelihood[i] < anomaly_percentage_threshold:
                    statistics[key][ANOMALY_FIELD_NAMES[i]] += 1
                    continue

        historical_transactions_groups[test_pay_account_number][test_bfd_account_number].append(transaction)
        progress_bar.update()
    progress_bar.close()

    return statistics


# def main6():
#     accounts_amount_time_series, accounts_transactions = parse_csv_file('cheques_in_import_3.csv')
#     data_point_counts = []
#     series_lengths = []
#     for account_number in accounts_amount_time_series:
#         amount_time_series = accounts_amount_time_series[account_number]
#         if len(amount_time_series) < 100:
#             continue
#         # amount_time_series.sort(key=lambda x: x[0])
#         date_amount_dict = dict()
#         date_count_dict = dict()
#
#         monthly_amount_dict = dict()
#         for d, a, _ in amount_time_series:
#             if (d.month, d.year) not in monthly_amount_dict:
#                 monthly_amount_dict[(d.month, d.year)] = 0
#             monthly_amount_dict[(d.month, d.year)] += a
#             if d not in date_amount_dict:
#                 date_amount_dict[d] = 0
#                 date_count_dict[d] = 0
#             date_amount_dict[d] += a
#             date_count_dict[d] += 1
#
#         date_amount_time_series = []
#         date_count_time_series = []
#         monthly_amount_time_series = []
#         for d in date_amount_dict:
#             date_amount_time_series.append((d, date_amount_dict[d]))
#             date_count_time_series.append((d, date_count_dict[d]))
#
#         for month, year in monthly_amount_dict:
#             monthly_amount_time_series.append(
#                 (datetime.date(year=year, month=month, day=1), monthly_amount_dict[(month, year)]))
#         date_amount_time_series.sort(key=lambda x: x[0])
#         date_count_time_series.sort(key=lambda x: x[0])
#         monthly_amount_time_series.sort(key=lambda x: x[0])
#
#         dates = [d for d, a in date_amount_time_series]
#         amounts = [a for d, a in date_amount_time_series]
#         counts = [c for d, c in date_count_time_series]
#
#         dates = dates_to_time_series(dates)
#
#         # plt.plot(np.arange(len(dates)), dates)
#         # plt.title('dates')
#         # plt.show()
#         # plt.plot(np.arange(len(amounts)), amounts)
#         # plt.title('amounts')
#         # plt.show()
#         # plt.plot(np.arange(len(counts)), counts)
#         # plt.title('counts')
#         # plt.show()
#         # plt.plot(np.arange(len(monthly_amount_time_series)), [a for d, a in monthly_amount_time_series])
#         # plt.show()
#         # amounts_array = np.array(amounts).reshape(-1, 1)
#         #
#         # n = int(len(amounts_array) * 0.7)
#         # # clf = IsolationForest(random_state=0).fit(amounts_array[:n])
#         # # clf = OneClassSVM(gamma=10, kernel='rbf').fit(amounts_array[:n])
#         # clf = EllipticEnvelope().fit(amounts_array[:n])
#         # print(np.count_nonzero(clf.predict(amounts_array[n:]).ravel() == -1) / (len(amounts_array) - n))
#         amounts = [round(a, 3) for a in amounts]
#         n = int(len(amounts) * 0.7)
#         threshold = 0.05
#
#         outliers = 0
#
#         dynamic_histogram = DynamicHistogram(amounts[:n])
#         for i in range(n, len(amounts)):
#             if calculate_likelihood(dynamic_histogram, amounts[i]) < threshold:
#                 outliers += 1
#
#         print(outliers / len(amounts))
#         print('-----------------')
#
#
# if __name__ == '__main__':
#     main6()


def parse_date(date_string):
    year, month, day = date_string.split('-')
    year = int(year) + 2000
    month = int(month)
    day = int(day)
    return datetime.date(year=year, month=month, day=day)


def parse_csv_file(file_path):
    data = pd.read_csv(file_path)
    data_array = data.to_numpy()
    transactions_time_series = []

    for i in range(len(data_array)):
        row = data_array[i]
        # chq_date, chq_amount, chq_pay_accno, chq_bfd_accno
        date_amount_bfd_tuple = (parse_date(row[12]), float(row[8]), int(row[6]), int(row[5]))
        transactions_time_series.append(date_amount_bfd_tuple)

    transactions_time_series.sort(key=lambda x: x[0])
    return transactions_time_series


def main10():
    # transactions_time_series = parse_csv_file('cheques_in_import_3.csv')
    # statistics = calculate_anomaly_report_statistics(transactions_time_series)
    # with open('./results.json', 'w') as f:
    #     json.dump(statistics, f, indent=4)
    with open('./results.json', 'r') as f:
        statistics = json.load(f)

    print(f'Historical Transactions #: {statistics["historical_transactions"]}')
    test_transactions = statistics["test_transactions"]
    print(f'Test Transactions #: {test_transactions}')
    insufficient_history_percentage = round(statistics["rejected_transactions"] / test_transactions * 100, 2)
    print(f'Insufficient History: {insufficient_history_percentage}%')
    print('')
    table = [['Threshold %', 'Anomalies %', 'Coverage %', 'Amount Anomalies %', 'Weekly Amount Anomalies %',
              'Weekly Count Anomalies %', 'Monthly Amount Anomalies %', 'Monthly Count Anomalies %']]
    for anomaly_threshold in DEFAULT_ANOMALY_THRESHOLDS:
        key = f'anomaly_{anomaly_threshold}_thresh'
        anomaly_percentage = round(statistics[key]['anomalous_transactions'] / test_transactions * 100, 2)
        coverage_percentage = round(100 - (anomaly_percentage + insufficient_history_percentage), 2)
        row = [str(anomaly_threshold), str(anomaly_percentage), str(coverage_percentage)]
        for i in range(len(ANOMALY_FIELD_NAMES)):
            row.append(round(statistics[key][ANOMALY_FIELD_NAMES[i]] / test_transactions * 100, 2))
        table.append(row)

    print(AsciiTable(table).table)


def main11():
    dist = DynamicHistogram([100.0, 110.0, 120.0, 500.0, 550, 1000.0, 1105, 1200])
    print(dist.frequencies)
    print(dist.histogram)
    print(dist.get_point_histogram_bin_and_frequency(105))


def main12():
    transactions_time_series = parse_csv_file('cheques_in_import_3.csv')
    historical_transactions_length = int(len(transactions_time_series) * HISTORICAL_TRANSACTIONS_PERCENTAGE)

    historical_transactions = transactions_time_series[:historical_transactions_length]
    test_transactions = transactions_time_series[historical_transactions_length:]
    historical_transactions_groups = group_transactions_by_pay_and_bfd_numbers(historical_transactions)
    test_transactions_count = len(test_transactions)

    statistics = initialize_statistics_dict()
    statistics['historical_transactions'] = historical_transactions_length
    statistics['test_transactions'] = test_transactions_count

    # progress_bar = tqdm(total=test_transactions_count)
    count = 0
    good_coefficient_of_variation_count = 0
    confidences = []
    within_range_count = 0
    for test_date, test_amount, test_pay_account_number, test_bfd_account_number in test_transactions:
        if test_pay_account_number not in historical_transactions_groups:
            historical_transactions_groups[test_pay_account_number] = dict()
        if test_bfd_account_number not in historical_transactions_groups[test_pay_account_number]:
            historical_transactions_groups[test_pay_account_number][test_bfd_account_number] = []

        pay_to_bfd_historical_transactions = historical_transactions_groups[test_pay_account_number][
            test_bfd_account_number]

        test_transaction = (test_date,
                            test_amount,
                            test_pay_account_number,
                            test_bfd_account_number)

        if len(pay_to_bfd_historical_transactions) < 1:
            statistics['unusual_bfd_transactions'] += 1

        if len(pay_to_bfd_historical_transactions) < 4:
            statistics['insufficient_history_transactions'] += 1
            historical_transactions_groups[test_pay_account_number][test_bfd_account_number].append(test_transaction)
            continue


        # if test_amount <= EPSILON:
        #     continue

        amount_ratio = 0.5
        min_amount = test_amount * (1 - amount_ratio)
        max_amount = test_amount * (1 + amount_ratio)

        filtered_transactions = []

        for transaction in pay_to_bfd_historical_transactions:
            if min_amount <= transaction[1] <= max_amount:
                filtered_transactions.append(transaction)

        if len(filtered_transactions) < 3:
            confidences.append(0.0)
            historical_transactions_groups[test_pay_account_number][test_bfd_account_number].append(test_transaction)
            continue

        date_deltas = dates_to_time_series([t[0] for t in filtered_transactions])
        sigma = np.std(date_deltas)
        mu = np.mean(date_deltas)
        coefficient_of_variation = sigma / mu * (1 + 1 / (4 * len(date_deltas)))
        if coefficient_of_variation < 0.1:
            good_coefficient_of_variation_count += 1
        # print(coefficient_of_variation, " => ", date_deltas)

        last_date = filtered_transactions[-1][0]
        difference = (test_date - last_date).days
        # confidence = LogNormalDistribution(date_deltas).calculate_confidence(difference)
        # confidence = sigma / abs(difference - mu)
        # confidences.append(confidence)
        tolerance = 17.0
        min_delta = max(0.0, mu - tolerance * sigma)
        max_delta = mu + tolerance * sigma
        if min_delta <= difference <= max_delta:
            within_range_count += 1

        coefficient = 0.03
        absolute_z_score = abs(difference - mu) / sigma
        confidence = min(1.0, max(0.0, 1.0 - coefficient * absolute_z_score))
        confidences.append(confidence)
        # print(date_deltas, difference, (min_delta, max_delta), min_delta <= difference <= max_delta)
        # time.sleep(1)
        count += 1
        historical_transactions_groups[test_pay_account_number][test_bfd_account_number].append(test_transaction)

    print(statistics['insufficient_history_transactions'] / test_transactions_count)
    confidences = np.array(confidences, np.float64)
    print("anomalies: ", np.count_nonzero(confidences < 0.01) / test_transactions_count)
    print(np.count_nonzero(confidences >= 0.01) / test_transactions_count)
    print(np.count_nonzero(confidences >= 0.05) / test_transactions_count)
    print(np.count_nonzero(confidences >= 0.1) / test_transactions_count)
    print(np.count_nonzero(confidences >= 0.2) / test_transactions_count)
    print(np.count_nonzero(confidences >= 0.3) / test_transactions_count)
    print(np.count_nonzero(confidences >= 0.4) / test_transactions_count)
    print(np.count_nonzero(confidences >= 0.5) / test_transactions_count)
    # print(good_coefficient_of_variation_count / test_transactions_count)
    print(within_range_count / test_transactions_count)


if __name__ == '__main__':
    main12()
