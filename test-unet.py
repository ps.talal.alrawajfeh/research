import cv2
import tensorflow as tf
from tensorflow.keras.models import load_model
import numpy as np
import os


def remove_white_border(binary_image):
    mask = (255 - binary_image) > 0

    height, width = binary_image.shape[0:2]
    mask1, mask2 = mask.any(0), mask.any(1)
    x1, x2 = mask1.argmax(), width - mask1[::-1].argmax()
    y1, y2 = mask2.argmax(), height - mask2[::-1].argmax()

    return binary_image[y1:y2, x1:x2]


def change_image_quality(image, quality=50):
    encoded = cv2.imencode(
        '.jpg', image, [int(cv2.IMWRITE_JPEG_QUALITY), quality])[1]
    return cv2.imdecode(encoded, cv2.IMREAD_GRAYSCALE)


def main():
    # image = cv2.imread('test.bmp', cv2.IMREAD_GRAYSCALE)
    # image = cv2.resize(image, (748, 200), interpolation=cv2.INTER_CUBIC)
    # cv2.imwrite('test-opencv.bmp', image)
    # image = 255 - remove_white_border(255 - image)
    # cv2.imwrite('test.bmp', image)
    # return
    # autoencoder = load_model('./downloads/autoencoder_last.h5', compile=False)
    autoencoder = load_model('model_39_0.0046_0.0183.h5', compile=False)

    # image = cv2.imread('./samples/sample-243.bmp', cv2.IMREAD_GRAYSCALE)
    # image = cv2.resize(image, (400, 200), interpolation=cv2.INTER_CUBIC)
    # image = np.array(255 - image, np.float32) / 255.0

    # result = autoencoder.predict(np.array([image.reshape(200, 400, 1)]))[0]

    # result = result.reshape(200, 400)
    # result *= 255
    # result = 255 - np.array(result, np.uint8)
    # result = cv2.threshold(
    #     result, 0, 255, cv2.THRESH_OTSU + cv2.THRESH_BINARY)[1]

    # cv2.imwrite('./autoencoder-vs-output/sample-243-output.bmp', result)

    # input_dir = './samples/'
    input_dir = './samples/'
    # output_dir = './autoencoder-vs-output/'
    # output_dir = './unet2-output/'
    # output_dir = './unet-output/'
    output_dir = './test-out/'
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
        
    files = os.listdir(input_dir)
    for f in files:
        image = cv2.imread(input_dir + f, cv2.IMREAD_GRAYSCALE)
        #----------
        # image = cv2.resize(image, (None, None), fx=0.75, fy=0.75, interpolation=cv2.INTER_CUBIC)
        # image = image[-200:, -400:]
        #----------
        image = cv2.resize(image, (400, 200), interpolation=cv2.INTER_CUBIC)
        image = np.array(255 - image, np.float32) / 255.0

        result = autoencoder.predict(np.array([image.reshape(200, 400, 1)]))[0]

        result = result.reshape(200, 400)
        result *= 255
        result = 255 - np.array(result, np.uint8)
        result = cv2.threshold(
            result, 0, 255, cv2.THRESH_OTSU + cv2.THRESH_BINARY)[1]

        cv2.imwrite(output_dir + f, result)


def separate_results():
    output_dir = './unet-output/'
    base_clean_dir = './BaseClean/'
    move_dir = './unet-output-gen/'
    output_files = os.listdir(output_dir)
    base_clean = os.listdir(base_clean_dir)

    base_clean_names = [os.path.splitext(x)[0] for x in base_clean]

    if not os.path.isdir(move_dir):
        os.makedirs(move_dir)

    generalization = []
    for f in output_files:
        f_name = os.path.splitext(f)[0]
        if f_name in base_clean_names:
            continue
        generalization.append(f)

    for f in generalization:
        os.rename(output_dir + f, move_dir + f)


def combine_output_with_samples():
    input_dir = './samples/'
    output_dir = './unet-output/'

    output_files = os.listdir(output_dir)

    for f in output_files:
        image1 = cv2.imread(input_dir + f, cv2.IMREAD_GRAYSCALE)
        image2 = cv2.imread(output_dir + f, cv2.IMREAD_GRAYSCALE)

        cv2.imwrite(output_dir + f, np.concatenate([image1, image2], axis=0))


if __name__ == '__main__':
    main()
    # separate_results()
    # combine_output_with_samples()
