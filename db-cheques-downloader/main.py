import os
import shutil
from os import listdir

import cv2
import cx_Oracle
import numpy as np
from matplotlib import pyplot as plt


def main1():
    dsn = cx_Oracle.makedsn("localhost", 1521, sid="EE")

    cheque_info_dict = dict()
    cheque_image_dict = dict()

    i = 0
    with cx_Oracle.connect(user='cheques_out',
                           password='cheques_out',
                           dsn=dsn) as connection:
        with connection.cursor() as cur:
            cur.execute("select CHQ_OID, CHQ_PAY_ACCNO, CHQ_SRLNO from ECC_CHEQUES_OUT")
            while True:
                row = cur.fetchone()
                if row is None:
                    break
                cheque_oid = row[0].hex()
                cheque_account_number = row[1]
                cheque_serial_number = row[2]
                cheque_info_dict[cheque_oid] = [cheque_account_number, cheque_serial_number]
                i += 1
                print(i)

        i = 0
        chunk_size = 65536
        with connection.cursor() as cur:
            cur.execute("select CHQ_OID, IMGTYP_CD, CHQIMG_IMAGE from ECC_CHQIMAGES_OUT")
            while True:
                row = cur.fetchone()
                if row is None:
                    break
                cheque_image_type = row[1]
                if cheque_image_type != 0:
                    continue
                cheque_oid = row[0].hex()
                cheque_image = row[2]

                # offset = 1
                # chunks = []
                # while True:
                #     data = cheque_image.read(offset, chunk_size)
                #     if data:
                #         chunks.append(data)
                #     if len(data) < chunk_size:
                #         break
                #     offset += chunk_size
                #
                # cheque_image_dict[cheque_oid] = b''.join(chunks)

                cheque_image_dict[cheque_oid] = cheque_image.read()
                i += 1
                print(i)

    print('done fetching data.')
    print('saving images...')
    base_dir = '/home/u764/Downloads/cheques/'
    for cheque_oid in cheque_info_dict:
        if cheque_oid not in cheque_image_dict:
            continue

        account_number, serial_number = cheque_info_dict[cheque_oid]
        file_name = cheque_oid + '_' + account_number + '_' + serial_number + '.jpg'

        with open(base_dir + file_name, 'wb') as f:
            f.write(cheque_image_dict[cheque_oid])
    print('done.')


def main():
    dsn = cx_Oracle.makedsn("localhost", 1521, sid="EE")

    base_dir = '/home/u764/Downloads/arbk-palestine-decrytped/'
    files = os.listdir(base_dir)
    destination_dir = '/home/u764/Downloads/out/'

    i = 0
    with cx_Oracle.connect(user='cheques_out',
                           password='cheques_out',
                           dsn=dsn) as connection:
        with connection.cursor() as cur:
            cur.execute("select CHQ_OID from ECC_CHEQUES_OUT where PAY_BANK_CD = 73")
            while True:
                row = cur.fetchone()
                if row is None:
                    break
                cheque_oid = row[0].hex().upper()
                for file in files:
                    if file[:len(cheque_oid)].upper() == cheque_oid:
                        shutil.copy(base_dir + file, destination_dir + file)
                i += 1
                print(i)


def view_image(image):
    plt.imshow(image, plt.cm.gray)
    plt.show()


def average_images():
    base_dir = '/home/u764/Downloads/cheques-73/'
    files = listdir(base_dir)
    original_images = [cv2.imread(base_dir + file, cv2.IMREAD_GRAYSCALE) for file in files]
    average_width = int(round(np.mean([image.shape[1] for image in original_images])))
    average_height = int(round(np.mean([image.shape[0] for image in original_images])))
    images = [cv2.resize(image, (average_width, average_height), interpolation=cv2.INTER_CUBIC) for image in
              original_images]
    image_tensor = np.array(images)
    average_image = np.mean(image_tensor, axis=0)
    std_image = np.std(image_tensor, axis=0)

    mu = np.mean(images)
    sigma = np.std(images)
    # average_image = cv2.blur(average_image, (7, 7))

    average_image = cv2.GaussianBlur(average_image, (5, 5), 0)
    std_image = cv2.GaussianBlur(std_image, (5, 5), 0)

    out_dir = '/home/u764/Downloads/attempt-73/'
    for image in images:
        image = cv2.GaussianBlur(image, (3, 3), 0)
        image = np.array(image, np.float32)
        image = image * sigma / np.std(image)
        image = image + mu - np.mean(image)
        view_image(np.array(image, np.uint8))
        view_image(255 - np.array(average_image - image > std_image, np.uint8))


def binarize():
    image = cv2.imread('/home/u764/Downloads/TESTS/sauvola-23', cv2.IMREAD_GRAYSCALE)
    image = cv2.GaussianBlur(image, (5, 5), 0)

    dilated = cv2.dilate(image, np.ones((21, 21), np.uint8))
    dilated = cv2.GaussianBlur(dilated, (5, 5), 0)
    view_image(dilated)
    # view_image(cv2.erode(image, np.ones((5, 5), np.uint8)))
    new_image = np.array(dilated, np.float32)
    new_image = new_image - np.array(image, np.float32)
    new_image[new_image < 0] = 0
    new_image[new_image > 255] = 255
    subtracted = 255 - np.array(new_image, np.uint8)
    binarized = cv2.threshold(subtracted, 0, 255, cv2.THRESH_OTSU)[1]
    view_image(binarized)


if __name__ == '__main__':
    # main()
    # average_images()
    binarize()
