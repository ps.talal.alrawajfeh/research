History:

{
    "oid": "1",
    "serial": "55",
    "bfdAccount": "17",
    "payAccount": "7",
    "date": "2023-01-01T09:00:07.278Z",
    "presentmentDate": "2023-08-24T09:00:07.278Z",
    "amount": 200,
    "sequence": 1,
    "analyzerSensitivityMode": "Moderate"
}

{
    "oid": "1",
    "serial": "56",
    "bfdAccount": "17",
    "payAccount": "7",
    "date": "2023-01-15T09:00:07.278Z",
    "presentmentDate": "2023-08-24T09:00:07.278Z",
    "amount": 550,
    "sequence": 1,
    "analyzerSensitivityMode": "Moderate"
}

{
    "oid": "1",
    "serial": "57",
    "bfdAccount": "17",
    "payAccount": "7",
    "date": "2023-02-01T09:00:07.278Z",
    "presentmentDate": "2023-08-24T09:00:07.278Z",
    "amount": 260,
    "sequence": 1,
    "analyzerSensitivityMode": "Moderate"
}

{
    "oid": "1",
    "serial": "58",
    "bfdAccount": "17",
    "payAccount": "7",
    "date": "2023-02-15T09:00:07.278Z",
    "presentmentDate": "2023-08-24T09:00:07.278Z",
    "amount": 500,
    "sequence": 1,
    "analyzerSensitivityMode": "Moderate"
}

{
    "oid": "1",
    "serial": "59",
    "bfdAccount": "17",
    "payAccount": "7",
    "date": "2023-03-01T09:00:07.278Z",
    "presentmentDate": "2023-08-24T09:00:07.278Z",
    "amount": 200,
    "sequence": 1,
    "analyzerSensitivityMode": "Moderate"
}

{
    "oid": "1",
    "serial": "60",
    "bfdAccount": "17",
    "payAccount": "7",
    "date": "2023-03-15T09:00:07.278Z",
    "presentmentDate": "2023-08-24T09:00:07.278Z",
    "amount": 520,
    "sequence": 1,
    "analyzerSensitivityMode": "Moderate"
}

{
    "oid": "1",
    "serial": "61",
    "bfdAccount": "17",
    "payAccount": "7",
    "date": "2023-04-01T09:00:07.278Z",
    "presentmentDate": "2023-08-24T09:00:07.278Z",
    "amount": 250,
    "sequence": 1,
    "analyzerSensitivityMode": "Moderate"
}

{
    "oid": "1",
    "serial": "62",
    "bfdAccount": "17",
    "payAccount": "7",
    "date": "2023-04-15T09:00:07.278Z",
    "presentmentDate": "2023-08-24T09:00:07.278Z",
    "amount": 450,
    "sequence": 1,
    "analyzerSensitivityMode": "Moderate"
}


----------------------------------------

Update status requests:

{
    "oid": "1",
    "serial": "55",
    "payAccount": "7",
    "isAccepted": true
}

{
    "oid": "1",
    "serial": "56",
    "payAccount": "7",
    "isAccepted": true
}

{
    "oid": "1",
    "serial": "57",
    "payAccount": "7",
    "isAccepted": true
}

{
    "oid": "1",
    "serial": "58",
    "payAccount": "7",
    "isAccepted": true
}

{
    "oid": "1",
    "serial": "59",
    "payAccount": "7",
    "isAccepted": true
}

{
    "oid": "1",
    "serial": "60",
    "payAccount": "7",
    "isAccepted": true
}

{
    "oid": "1",
    "serial": "61",
    "payAccount": "7",
    "isAccepted": true
}

{
    "oid": "1",
    "serial": "62",
    "payAccount": "7",
    "isAccepted": true
}


----------------------------------------

The following case should fail:
{
    "oid": "1",
    "serial": "63",
    "bfdAccount": "17",
    "payAccount": "7",
    "date": "2023-05-01T09:00:07.278Z",
    "presentmentDate": "2023-08-24T09:00:07.278Z",
    "amount": 325,
    "sequence": 1,
    "analyzerSensitivityMode": "Moderate"
}

The following case should pass:
{
    "oid": "1",
    "serial": "64",
    "bfdAccount": "17",
    "payAccount": "7",
    "date": "2023-05-01T09:00:07.278Z",
    "presentmentDate": "2023-08-24T09:00:07.278Z",
    "amount": 325,
    "sequence": 1,
    "analyzerSensitivityMode": "Lean"
}

