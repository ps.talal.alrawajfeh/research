import json
import os


request = {
    "oid": "1",
    "serial": "",
    "bfdAccount": "",
    "payAccount": "",
    "date": "YYYY-MM-ddT09:00:07.278Z",
    "presentmentDate": "2023-08-24T09:00:07.278Z",
    "amount": 0,
    "sequence": 1,
    "analyzerSensitivityMode": "Moderate",
}

update_status_request = {
    "oid": "1",
    "serial": "",
    "payAccount": "",
    "isAccepted": True,
}


def main():
    with open("source.txt", "r") as f:
        test_cases = f.read()

    test_cases = test_cases.split("#")

    current_pay_account_id = 1
    current_bfd_account_id = len(test_cases) + 1
    serial = 1

    if not os.path.isdir("requests"):
        os.makedirs("requests")

    for test_case in test_cases:
        lines = test_case.split("\n")
        lines = [l.strip() for l in lines]
        lines = [l for l in lines if l != ""]

        test_name_i1 = lines[0].find("[")
        test_name_i2 = lines[0].find("]")
        test_name = lines[0][test_name_i1 + 1 : test_name_i2].strip()

        update_status_requests = []

        started_with_new_transactions = False
        with open(os.path.join("requests", f"{test_name}.txt"), "w") as f:
            f.write('History:\n\n')
            i = 1
            while i < len(lines):
                request["analyzerSensitivityMode"] = "Moderate"
                request["date"] = "YYYY-MM-ddT09:00:07.278Z"

                line = lines[i]
                if line.startswith("~") and not started_with_new_transactions:
                    started_with_new_transactions = True
                    f.write("\n----------------------------------------\n\n")

                    f.write('Update status requests:\n\n')
                    for status_request in update_status_requests:
                        f.write(json.dumps(status_request, indent=4))
                        f.write("\n\n")

                    f.write("\n----------------------------------------\n\n")


                if line.startswith("~"):
                    expected_result_code = line[1]
                    if expected_result_code.upper() == "F":
                        f.write("The following case should fail:\n")
                    else:
                        f.write("The following case should pass:\n")
                    analyzer_sensitivity_mode = line[2]
                    if analyzer_sensitivity_mode.upper() == "L":
                        request["analyzerSensitivityMode"] = "Lean"
                    elif analyzer_sensitivity_mode.upper() == "M":
                        request["analyzerSensitivityMode"] = "Moderate"
                    else:
                        request["analyzerSensitivityMode"] = "Strict"
                    line = line[3:]

                date, amount = line.split("=>")
                date = date.strip()
                amount = int(amount.strip())

                day, month, year = date.split("/")
                while len(day) < 2:
                    day = f"0{day}"
                while len(month) < 2:
                    month = f"0{month}"

                request["amount"] = amount
                request["date"] = request["date"].replace("YYYY", year)
                request["date"] = request["date"].replace("MM", month)
                request["date"] = request["date"].replace("dd", day)

                request["serial"] = str(serial)
                request["payAccount"] = str(current_pay_account_id)
                request["bfdAccount"] = str(current_bfd_account_id)

                f.write(json.dumps(request, indent=4))
                f.write("\n\n")

                status_request = update_status_request.copy()
                status_request["serial"] = str(serial)
                status_request["payAccount"] = str(current_pay_account_id)
                update_status_requests.append(status_request)

                serial += 1
                i += 1

        current_pay_account_id += 1
        current_bfd_account_id += 1


if __name__ == "__main__":
    main()
