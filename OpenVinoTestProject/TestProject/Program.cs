﻿using System.Drawing;
using System.Runtime.InteropServices;

namespace TestProject
{
    internal unsafe class Program
    {
        public struct ModelOutput
        {
            public readonly bool IsSuccess;
            public readonly string ErrorDescription;
            public readonly List<string>? OutputTensorsNames;
            public readonly List<long[]>? OutputTensorsDimensions;
            public readonly List<float[]>? OutputTensorsData;

            public ModelOutput(bool isSuccess,
                string errorDescription,
                List<string>? outputTensorsNames,
                List<long[]>? outputTensorsDimensions,
                List<float[]>? outputTensorsData)
            {
                IsSuccess = isSuccess;
                ErrorDescription = errorDescription;
                OutputTensorsNames = outputTensorsNames;
                OutputTensorsDimensions = outputTensorsDimensions;
                OutputTensorsData = outputTensorsData;
            }

            public ModelOutput(bool isSuccess,
                string errorDescription)
            {
                IsSuccess = isSuccess;
                ErrorDescription = errorDescription;
                OutputTensorsNames = null;
                OutputTensorsDimensions = null;
                OutputTensorsData = null;
            }
        }

        public struct ModelInput
        {
            public readonly List<string> InputTensorsNames;
            public readonly List<long[]> InputTensorsDimensions;
            public readonly List<float[]> InputTensorsData;

            public ModelInput(List<string> inputTensorsNames,
                List<long[]> inputTensorsDimensions,
                List<float[]> inputTensorsData)
            {
                InputTensorsNames = inputTensorsNames;
                InputTensorsDimensions = inputTensorsDimensions;
                InputTensorsData = inputTensorsData;
            }
        }

        public static ModelOutput Infer(string modelId, ModelInput modelInput)
        {
            long inputTensorsNamesTotalMemorySize = 0;
            long inputTensorsDimensionsTotalMemorySize = 0;
            long inputTensorsDataTotalMemorySize = 0;
            long numberOfInputTensors = modelInput.InputTensorsNames.Count;
            var inputTensorsDimensionsLengths = new long[numberOfInputTensors];

            for (int i = 0; i < numberOfInputTensors; i++)
            {
                inputTensorsNamesTotalMemorySize += modelInput.InputTensorsNames[i].Length + 1;
                var inputTensorDimensions = modelInput.InputTensorsDimensions[i];
                long size = 1;
                foreach (var dimension in inputTensorDimensions)
                {
                    size *= dimension;
                }

                inputTensorsDataTotalMemorySize += size;
                var length = inputTensorDimensions.Length;
                inputTensorsDimensionsLengths[i] = length;
                inputTensorsDimensionsTotalMemorySize += length;
            }

            var inputTensorsNames = new byte[inputTensorsNamesTotalMemorySize];
            var inputTensorsDimensions = new long[inputTensorsDimensionsTotalMemorySize];
            var inputTensorsData = new float[inputTensorsDataTotalMemorySize];

            OpenVinoExternsLinux.InferenceResult inferenceResult;

            fixed (byte* pInputTensorsNames = &inputTensorsNames[0])
            {
                var pCurrentInputTensorName = pInputTensorsNames;
                fixed (long* pInputTensorsDimensions = &inputTensorsDimensions[0])
                {
                    var pCurrentInputTensorDimensions = pInputTensorsDimensions;
                    fixed (long* pInputTensorsDimensionsLengths = &inputTensorsDimensionsLengths[0])
                    {
                        fixed (float* pInputTensorsData = &inputTensorsData[0])
                        {
                            var pCurrentInputTensorData = pInputTensorsData;
                            for (int i = 0; i < numberOfInputTensors; i++)
                            {
                                var modelInputTensorsName = modelInput.InputTensorsNames[i];
                                var inputTensorName = System.Text.Encoding.ASCII.GetBytes(modelInputTensorsName);
                                Marshal.Copy(inputTensorName,
                                    0,
                                    (IntPtr) pCurrentInputTensorName,
                                    inputTensorName.Length);
                                pCurrentInputTensorName += inputTensorName.Length;
                                *pCurrentInputTensorName = 0;
                                pCurrentInputTensorName++;

                                var modelInputTensorDimensions = modelInput.InputTensorsDimensions[i];
                                foreach (var dimensions in modelInputTensorDimensions)
                                {
                                    *pCurrentInputTensorDimensions = dimensions;
                                    pCurrentInputTensorDimensions++;
                                }

                                var modelInputTensorData = modelInput.InputTensorsData[i];
                                Marshal.Copy(modelInputTensorData,
                                    0,
                                    (IntPtr) pCurrentInputTensorData,
                                    modelInputTensorData.Length);
                                pCurrentInputTensorData += modelInputTensorData.Length;
                            }

                            inferenceResult = OpenVinoExternsLinux.Infer(modelId,
                                pInputTensorsNames,
                                pInputTensorsDimensions,
                                pInputTensorsDimensionsLengths,
                                numberOfInputTensors,
                                pInputTensorsData);
                        }
                    }
                }
            }

            if (!inferenceResult.isSuccess)
            {
                var modelOutput = new ModelOutput(false,
                    Marshal.PtrToStringAnsi(inferenceResult.errorDescription) ?? string.Empty);
                OpenVinoExternsLinux.DisposeInferenceResult(inferenceResult);
                return modelOutput;
            }

            var outputTensorsNames = new List<string>();
            var outputTensorsDimensions = new List<long[]>();
            var outputTensorsData = new List<float[]>();

            byte* pOutputTensorsNames = inferenceResult.outputTensorsNames;
            long* pOutputTensorsDimensions = inferenceResult.outputTensorsDimensions;
            long* pOutputTensorsDimensionsLengths = inferenceResult.outputTensorsDimensionsLengths;
            float* pOutputTensorsData = inferenceResult.outputTensorsData;

            for (int i = 0; i < inferenceResult.numberOfOutputTensors; i++)
            {
                string outputTensorName = Marshal.PtrToStringAnsi((IntPtr) pOutputTensorsNames) ?? string
                    .Empty;
                pOutputTensorsNames += outputTensorName.Length + 1;
                outputTensorsNames.Add(outputTensorName);

                var outputTensorDimensionsLength = *pOutputTensorsDimensionsLengths;
                long[] outputTensorDimensions = new long[outputTensorDimensionsLength];
                pOutputTensorsDimensionsLengths++;
                outputTensorsDimensions.Add(outputTensorDimensions);

                long outputTensorSize = 1;
                for (int j = 0; j < outputTensorDimensionsLength; j++)
                {
                    outputTensorDimensions[j] = *pOutputTensorsDimensions;
                    outputTensorSize *= outputTensorDimensions[j];
                    pOutputTensorsDimensions++;
                }

                float[] outputTensorData = new float[outputTensorSize];
                Marshal.Copy((IntPtr) pOutputTensorsData, outputTensorData, 0, (int) outputTensorSize);
                pOutputTensorsData += outputTensorSize;
                outputTensorsData.Add(outputTensorData);
            }

            OpenVinoExternsLinux.DisposeInferenceResult(inferenceResult);

            return new ModelOutput(false,
                String.Empty,
                outputTensorsNames,
                outputTensorsDimensions,
                outputTensorsData);
        }

        static void Main(string[] args)
        {
            Console.WriteLine(File.Exists("./ImageInpainting.xml"));
            Console.WriteLine(System.Environment.CurrentDirectory);

            OpenVinoExternsLinux.VoidResult result = OpenVinoExternsLinux.RegisterModel("hello",
                "./ImageInpainting.xml",
                "./ImageInpainting.bin",
                "CPU");
            
            Console.WriteLine("Beginning of program");
            Console.WriteLine(result.isSuccess);
            Console.WriteLine(result.errorDescription);

            // Console.WriteLine("1: " + result.isSuccess);
            // Console.WriteLine("2: " + Marshal.PtrToStringAnsi(result.errorDescription));
            // OpenVinoExternsLinux.DisposeVoidResult(result);
            //
            // Console.WriteLine("3: " + OpenVinoExternsLinux.DoesModelExist("hello"));
            // result = OpenVinoExternsLinux.UnregisterModel("hello");
            // Console.WriteLine("4: " + result.isSuccess);
            // OpenVinoExternsLinux.DisposeVoidResult(result);
            //
            // Console.WriteLine("5: " + OpenVinoExternsLinux.DoesModelExist("hello"));
            //
            // result = OpenVinoExternsLinux.UnregisterModel("hello");
            // Console.WriteLine("6: " + result.isSuccess);
            // Console.WriteLine("7: " + Marshal.PtrToStringAnsi(result.errorDescription));
            // OpenVinoExternsLinux.DisposeVoidResult(result);

            Console.WriteLine("Beginning of inference");

            List<string> inputNames = new List<string> {"input_mask:0", "inputs_img:0"};
            List<long[]> inputDimensions = new List<long[]>
            {
                new long[] {1, 128, 256, 1},
                new long[] {1, 128, 256, 1}
            };
            List<float[]> tensorData = new List<float[]> {new float[128 * 256], new float[128 * 256]};

            var modelInput = new ModelInput(inputNames,
                inputDimensions,
                tensorData);

            var modelOutput = Infer("hello", modelInput);

            Console.WriteLine(modelOutput.IsSuccess);
            Console.WriteLine(modelOutput.ErrorDescription);

            foreach (var name in modelOutput.OutputTensorsNames)
            {
                Console.WriteLine(name);
            }

            foreach (var dimensions in modelOutput.OutputTensorsDimensions)
            {
                foreach (var d in dimensions)
                {
                    Console.Write(d);
                    Console.Write(", ");
                }

                Console.Write("\b\b  ");
                Console.WriteLine("");
            }

            foreach (var data in modelOutput.OutputTensorsData)
            {
                for (int i = 0; i < 10; i++)
                {
                    Console.Write(data[i]);
                    Console.Write(", ");
                }

                Console.Write("\b\b  ");
                Console.WriteLine("");
            }
        }
    }
}