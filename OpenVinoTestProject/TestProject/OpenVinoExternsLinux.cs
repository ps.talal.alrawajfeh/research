using System.Runtime.InteropServices;

namespace TestProject;

public unsafe class OpenVinoExternsLinux
{
    private const string DLL_PATH = @"libOpenVinoNative.so";

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct VoidResult
    {
        public readonly bool isSuccess;
        public readonly IntPtr errorDescription;
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct InferenceResult
    {
        public readonly byte* outputTensorsNames;
        public readonly long* outputTensorsDimensions;
        public readonly long* outputTensorsDimensionsLengths;
        public readonly long numberOfOutputTensors;
        public readonly float* outputTensorsData;
        public readonly bool isSuccess;
        public readonly IntPtr errorDescription;
    }

    [DllImport(DLL_PATH,
        CharSet = CharSet.Ansi,
        CallingConvention = CallingConvention.Cdecl,
        EntryPoint = "RegisterModel")]
    public static extern VoidResult RegisterModel(string modelId,
        string xmlPath,
        string binPath,
        string deviceName);

    [DllImport(DLL_PATH,
        CharSet = CharSet.Ansi,
        CallingConvention = CallingConvention.Cdecl,
        EntryPoint = "UnregisterModel")]
    public static extern VoidResult UnregisterModel(string modelId);

    [DllImport(DLL_PATH,
        CharSet = CharSet.Ansi,
        CallingConvention = CallingConvention.Cdecl,
        EntryPoint = "DoesModelExist")]
    public static extern bool DoesModelExist(string modelId);

    [DllImport(DLL_PATH,
        CharSet = CharSet.Ansi,
        CallingConvention = CallingConvention.Cdecl,
        EntryPoint = "DisposeVoidResult")]
    public static extern void DisposeVoidResult(VoidResult voidResult);

    [DllImport(DLL_PATH,
        CharSet = CharSet.Ansi,
        CallingConvention = CallingConvention.Cdecl,
        EntryPoint = "Infer")]
    public static extern InferenceResult Infer(string modelId,
        byte* inputTensorsNames,
        long* inputTensorsDimensions,
        long* inputTensorsDimensionsLengths,
        long numberOfInputTensors,
        float* inputTensorsData);

    [DllImport(DLL_PATH,
        CharSet = CharSet.Ansi,
        CallingConvention = CallingConvention.Cdecl,
        EntryPoint = "DisposeInferenceResult")]
    public static extern void DisposeInferenceResult(InferenceResult inferenceResult);
}