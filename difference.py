#!/usr/bin/python3.8

import numpy as np
import cv2
import os
import math
import itertools
from matplotlib import pyplot as plt
from scipy import interpolate
from scipy import ndimage


EPSILON = 1e-7


def image_classes_from_path(path):
    if not os.path.isdir(path):
        return dict()

    files = os.listdir(path)

    image_classes = dict()

    for f in files:
        c = f[0:9]

        if len(c) != 9:
            continue

        if c not in image_classes:
            image_classes[c] = []
        image_path = os.path.join(path, f)

        try:
            cv2.imread(image_path)
            image_classes[c].append(image_path)
        except:
            pass

    return image_classes


def print_image_classes_statistics(image_classes):
    print(f'classes: {len(image_classes)}')

    total_images = 0

    for c in image_classes:
        total_images += len(image_classes[c])

    average_images_per_class = int(
        math.ceil(total_images / len(image_classes)))

    print(f'total number of images: {total_images}')
    print(f'average images per class: {average_images_per_class}')


def filter_image_classes(image_classes):
    filtered_image_classes = dict()

    for c in image_classes:
        if len(image_classes[c]) >= 2:
            filtered_image_classes[c] = image_classes[c]

    return filtered_image_classes


def l2_norm(image):
    return np.sqrt(np.sum(np.square(image)))


def euclidean_distance(image1, image2):
    return l2_norm(image1 - image2)


def euclidean_similarity(image1, image2):
    distance = euclidean_distance(image1, image2)
    return 1.0 - distance / np.sqrt(np.prod(np.shape(image1)))


def cosine_similarity(image1, image2):
    return np.sum(image1 * image2) / (l2_norm(image1) * l2_norm(image2))


def normalized_cross_correlation(image1, image2):
    mean1 = np.mean(image1)
    mean2 = np.mean(image2)

    zero_centered1 = image1 - mean1
    zero_centered2 = image2 - mean2

    std1 = np.std(image1)
    std2 = np.std(image2)

    if std1 < EPSILON and std2 < EPSILON:
        if abs(mean1 - mean2) < EPSILON:
            return 1.0
        else:
            return 0.0
    elif std1 < EPSILON or std2 < EPSILON:
        return 0.0

    return np.abs(np.sum(zero_centered1 * zero_centered2) / (std1 * std2 * np.prod(np.shape(image1))))


def threshold(image):
    return cv2.threshold(
        image,
        0,
        255,
        cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]


def remove_white_border(binary_image):
    mask = (255 - binary_image) > 0

    height, width = binary_image.shape[0:2]
    mask1, mask2 = mask.any(0), mask.any(1)
    x1, x2 = mask1.argmax(), width - mask1[::-1].argmax()
    y1, y2 = mask2.argmax(), height - mask2[::-1].argmax()

    return binary_image[y1:y2, x1:x2]


def clever_crop(image):
    binary_image = threshold(image)
    mask = (255 - binary_image) > 0

    height, width = binary_image.shape[0:2]
    mask1, mask2 = mask.any(0), mask.any(1)
    x1, x2 = mask1.argmax(), width - mask1[::-1].argmax()
    y1, y2 = mask2.argmax(), height - mask2[::-1].argmax()

    return image[y1:y2, x1:x2]


def pre_process(image):
    image = remove_white_border(threshold(image))

    inverted = 255 - image
    return inverted / 255.0


def read_image(image_path):
    return cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)


def to_int(number):
    return int(round(number))


def rotate(image, angle):
    (h, w) = image.shape[:2]
    center_x, center_y = w / 2, h / 2

    rotation_matrix = cv2.getRotationMatrix2D((center_x, center_y),
                                              angle,
                                              1.0)

    rad = math.radians(angle)
    sin = abs(math.sin(rad))
    cos = abs(math.cos(rad))

    new_w = h * sin + w * cos
    new_h = h * cos + w * sin

    rotation_matrix[0, 2] += new_w / 2 - center_x
    rotation_matrix[1, 2] += new_h / 2 - center_y

    warped = cv2.warpAffine(image,
                            rotation_matrix,
                            (to_int(new_w), to_int(new_h)),
                            borderValue=(255, 255, 255),
                            flags=cv2.INTER_CUBIC)

    warped[warped < 0] = 0
    warped[warped > 255] = 255

    return np.array(warped, np.uint8)


def center_of_mass(image):
    return ndimage.measurements.center_of_mass(image)


def main():
    image_classes = image_classes_from_path('./signatures')
    print_image_classes_statistics(image_classes)
    image_classes = filter_image_classes(image_classes)
    print_image_classes_statistics(image_classes)

    similarities1 = []
    similarities2 = []
    similarities3 = []

    for c in image_classes:
        images = [pre_process(read_image(image)) for image in image_classes[c]]

        for image1, image2 in itertools.combinations(images, 2):
            c1 = np.array(center_of_mass(image1), np.int)
            c2 = np.array(center_of_mass(image2), np.int)

            image1_shape = image1.shape
            image2_shape = image2.shape

            if c1[1] != c2[1]:
                diff = abs(c1[1] - c2[1])
                
                if c1[1] > c2[1]:
                    image2 = np.pad(image2, ((0, 0), (diff, 0)),
                                    'constant',
                                    constant_values=((0, 0), (0, 0)))
                else:
                    image1 = np.pad(image1, ((0, 0), (diff, 0)),
                                    'constant',
                                    constant_values=((0, 0), (0, 0)))
                
            if image1_shape[1] - c1[1] != image2_shape[1] - c2[1]:
                diff = abs((image1_shape[1] - c1[1]) - (image2_shape[1] - c2[1]))
                
                if image1_shape[1] - c1[1] > image2_shape[1] - c2[1]:
                    image2 = np.pad(image2, ((0, 0), (0, diff)),
                                    'constant',
                                    constant_values=((0, 0), (0, 0)))
                else:
                    image1 = np.pad(image1, ((0, 0), (0, diff)),
                                    'constant',
                                    constant_values=((0, 0), (0, 0)))
            
            if c1[0] != c2[0]:
                diff = abs(c1[0] - c2[0])

                if c1[0] > c2[0]:
                    image2 = np.pad(image2, ((diff, 0), (0, 0)),
                                    'constant',
                                    constant_values=((0, 0), (0, 0)))
                else:
                    image1 = np.pad(image1, ((diff, 0), (0, 0)),
                                    'constant',
                                    constant_values=((0, 0), (0, 0)))

            if image1_shape[0] - c1[0] != image2_shape[0] - c2[0]:
                diff = abs((image1_shape[0] - c1[0]) - (image2_shape[0] - c2[0]))

                if image1_shape[0] - c1[0] > image2_shape[0] - c2[0]:
                    image2 = np.pad(image2, ((0, diff), (0, 0)),
                                    'constant',
                                    constant_values=((0, 0), (0, 0)))
                else:
                    image1 = np.pad(image1, ((0, diff), (0, 0)),
                                    'constant',
                                    constant_values=((0, 0), (0, 0)))

            similarity = euclidean_similarity(image1, image2) * 100
            if similarity > 100:
                similarity = 100
            if similarity < 0:
                similarity = 0
            similarities1.append(similarity)

            similarity = cosine_similarity(image1, image2) * 100
            if similarity > 100:
                similarity = 100
            if similarity < 0:
                similarity = 0
            similarities2.append(similarity)

            similarity = normalized_cross_correlation(image1, image2) * 100
            if similarity > 100:
                similarity = 100
            if similarity < 0:
                similarity = 0
            similarities3.append(similarity)

        similarities1.extend([100] * len(images))
        similarities2.extend([100] * len(images))
        similarities3.extend([100] * len(images))

    similarities1 = np.array(similarities1)
    similarities2 = np.array(similarities2)
    similarities3 = np.array(similarities3)

    mapping = interpolate.interp1d(
        [0, np.mean(similarities1), 100], [0, 50, 100])
    similarities1 = mapping(similarities1)

    mapping = interpolate.interp1d(
        [0, np.mean(similarities2), 100], [0, 50, 100])
    similarities2 = mapping(similarities2)

    mapping = interpolate.interp1d(
        [0, np.mean(similarities3), 100], [0, 50, 100])
    similarities3 = mapping(similarities3)

    #bins = [int(5 * i) for i in range(0, 100 // 5)]
    bins = list(range(101))
    # print(len(similarities1[similarities1 == 100]))
    # plt.hist(similarities1, bins, color='red', label='euclidean similarity')
    # plt.show()
    # plt.hist(similarities2, bins, color='green', label='cosine similarity')
    # plt.show()
    # plt.hist(similarities3, bins, color='blue', label='normalized cross correlation')
    # plt.show()

    similarities =  (similarities1 + similarities2 + similarities3) / 3
    # similarities = math.pow(
    #     similarities1 * similarities2 * similarities3, 1 / 3)
    mapping = interpolate.interp1d([0, 100], [70, 100])
    similarities = mapping(similarities)

    plt.hist(similarities, bins, color='red')
    plt.show()


if __name__ == "__main__":
    main()
