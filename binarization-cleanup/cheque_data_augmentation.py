import random
from enum import Enum

import cv2
import numpy as np
from matplotlib import pyplot as plt
from scipy.interpolate import interp1d

EPSILON = 1e-7


class GradientDirection(Enum):
    HORIZONTAL = 0
    VERTICAL = 1


def random_sign():
    return (-1) ** random.randint(1, 10)


def apply_gradient(image, min_value, max_value, direction=GradientDirection.HORIZONTAL, inverted=False):
    result = image.astype(np.float32)
    grad_sign = random_sign()

    if direction == GradientDirection.HORIZONTAL:
        grad = (max_value - min_value) / result.shape[0]
        i = 0

        def predicate(x):
            return x < result.shape[0]

        def update(x):
            return x + 1

        if inverted:
            i = result.shape[0] - 1

            def predicate(x):
                return x > 0

            def update(x):
                return x - 1

        while predicate(i):
            grad_i = grad * i
            result[i, :] += grad_sign * grad_i
            i = update(i)
    else:
        return apply_gradient(result.T, min_value, max_value, inverted=inverted).T

    result[result > 255] = 255
    result[result < 0] = 0
    result = result.astype(np.uint8)

    return result


def change_brightness(image, alpha):
    result = image.astype(np.float32)
    result += alpha
    result[result > 255] = 255
    result[result < 0] = 0
    return result.astype(np.uint8)


def change_contrast(image, beta):
    result = image.astype(np.float32)
    result *= beta
    result[result > 255] = 255
    result[result < 0] = 0
    return result.astype(np.uint8)


def add_gaussian_noise(image, mu, sigma):
    noise = np.random.normal(mu, sigma, size=image.shape)
    result = image.astype(np.float32)
    result += noise
    result[result > 255] = 255
    result[result < 0] = 0
    return result.astype(np.uint8)


def otsu_threshold(gray_image: np.ndarray) -> np.ndarray:
    return cv2.threshold(gray_image,
                         0,
                         255,
                         cv2.THRESH_BINARY + cv2.THRESH_OTSU)[0]


def draw_foreground_on_background_smoothly(foreground_image,
                                           background_image,
                                           position,
                                           alpha=0.25):
    x, y = position

    background_image_part = background_image[y:y + foreground_image.shape[0], x:x + foreground_image.shape[1]]
    inverted_background = 255 - background_image_part
    inverted_foreground = 255 - foreground_image

    background_mean = np.mean(inverted_background)
    background_std = np.std(inverted_background)
    foreground_threshold = otsu_threshold(inverted_foreground[inverted_foreground > 0])

    light_values_mask = (inverted_foreground > 0) & (inverted_foreground <= foreground_threshold)

    if np.any(light_values_mask):
        light_values = inverted_foreground[light_values_mask]
        light_values_mean = np.mean(light_values)
        light_values_min = np.min(light_values)
        min_mapped = max(min(background_mean - alpha * background_std, 255), 0)
        max_mapped = max(min(background_mean + alpha * background_std, 255), 0)
        interpolator = interp1d([light_values_min, light_values_mean, foreground_threshold, 255],
                                [min_mapped, background_mean, max_mapped, 255])
    else:
        interpolator = lambda x: x

    mapped_foreground = np.array(inverted_foreground)
    mapped_foreground[mapped_foreground > 0] = interpolator(mapped_foreground[mapped_foreground > 0])

    foreground_mask = mapped_foreground > 0
    inverted_background[foreground_mask] = mapped_foreground[foreground_mask]

    background_image_copy = np.array(background_image)
    background_image_copy[y:y + foreground_image.shape[0], x:x + foreground_image.shape[1]] = 255 - inverted_background
    return background_image_copy


def blend_foreground_on_background(foreground_image,
                                   background_image,
                                   position,
                                   foreground_color='auto',
                                   alpha=0.25,
                                   beta=0.9):
    x, y = position

    background_image_part = background_image[y:y + foreground_image.shape[0], x:x + foreground_image.shape[1]]
    inverted_background = 255 - background_image_part
    inverted_foreground = 255 - foreground_image

    background_mean = np.mean(inverted_background)
    background_std = np.std(inverted_background)
    foreground_threshold = otsu_threshold(inverted_foreground[inverted_foreground > 0])

    light_values_mask = (inverted_foreground > 0) & (inverted_foreground <= foreground_threshold)

    if isinstance(foreground_color, str) and foreground_color == 'auto':
        foreground_color = np.max(inverted_foreground)

    if np.any(light_values_mask):
        light_values = inverted_foreground[light_values_mask]
        light_values_mean = np.mean(light_values)
        light_values_min = np.min(light_values)
        min_mapped = max(min(background_mean - alpha * background_std, 255), 0)
        max_mapped = max(min(background_mean + alpha * background_std, 255), 0)
        interpolator = interp1d([light_values_min, light_values_mean, foreground_threshold, 255],
                                [min_mapped, background_mean, max_mapped, foreground_color])
    else:
        interpolator = np.frompyfunc(lambda x: foreground_color, 1, 1)

    mapped_foreground = np.array(inverted_foreground)
    mapped_foreground[mapped_foreground > 0] = interpolator(mapped_foreground[mapped_foreground > 0])

    foreground_mask = mapped_foreground > 0
    inverted_background[foreground_mask] = mapped_foreground[foreground_mask]

    background_image_copy = np.array(background_image)
    image1 = background_image_copy[y:y + foreground_image.shape[0], x:x + foreground_image.shape[1]].astype(np.float32)
    image2 = (255 - inverted_background).astype(np.float32)

    result = (1.0 - beta) * image1 + beta * image2
    result[result < 0] = 0
    result[result > 255] = 255
    background_image_copy[y:y + foreground_image.shape[0], x:x + foreground_image.shape[1]] = result.astype(np.uint8)

    return background_image_copy


def smooth_foreground(foreground_image,
                      resize_factor=3,
                      blurring_kernel_size=3):
    result = cv2.resize(foreground_image,
                        (foreground_image.shape[1] * resize_factor,
                         foreground_image.shape[0] * resize_factor),
                        interpolation=cv2.INTER_NEAREST)
    result = cv2.GaussianBlur(result,
                              (blurring_kernel_size,
                               blurring_kernel_size),
                              0)
    result = cv2.resize(result,
                        (result.shape[1] // resize_factor,
                         result.shape[0] // resize_factor),
                        interpolation=cv2.INTER_AREA)
    return result


def change_image_quality(image, quality):
    encoded = cv2.imencode('.jpg', image, [int(cv2.IMWRITE_JPEG_QUALITY), quality])[1]
    return cv2.imdecode(encoded, cv2.IMREAD_GRAYSCALE)


def main():
    # image = cv2.imread('/home/u764/Development/00002590100102011030240504210R_gray.bmp', cv2.IMREAD_GRAYSCALE)
    image = cv2.imread('/home/u764/Development/data/PS-ASV_OriginalSignatures/signatures/2d0284e0a_2.bmp',
                       cv2.IMREAD_GRAYSCALE)
    image = change_image_quality(smooth_foreground(image), 70)

    background = cv2.imread('/home/u764/Development/samples/003521401001020110389210148R_gray.bmp',
                            cv2.IMREAD_GRAYSCALE)

    plt.imshow(blend_foreground_on_background(image, background, (0, 0), alpha=0.5, beta=0.9), plt.cm.gray)
    plt.show()


if __name__ == '__main__':
    main()
