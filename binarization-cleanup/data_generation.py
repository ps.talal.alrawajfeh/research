import os
import random

import cv2
import numpy as np
from PIL import Image
from PIL import ImageDraw, ImageFont
from tqdm import tqdm

INPUT_SIZE = (400, 200)
DEFAULT_BACKGROUND_COLOR_RANGE = (128, 255)
DEFAULT_FOREGROUND_COLOR_RANGE = (0, 127)
LINE_Y_RANGE = (130, 170)
LINE_X_RANGE = (0, 400)
LINE_Y_MARGIN = 50
LINE_X_MARGIN = 50
LINE_THICKNESS_RANGE = (1, 3)
BOX_MIN_WIDTH = 200
BOX_MAX_WIDTH = 350
BOX_MIN_HEIGHT = 100
BOX_MAX_HEIGHT = 180
PRINTED_TEXT_FONT_SIZE_RANGE = (10, 20)
NUMBER_OF_SAMPLES = 200000
IMAGE_QUALITY_RANGE = (50, 99)

SIGNATURES_PATH = '/home/u764/Development/data/PS-ASV_OriginalSignatures/signatures'
FONTS_PATH = './resources/fonts'
INPUT_IMAGES_PATH = '/home/u764/Downloads/input-images'
OUTPUT_IMAGES_PATH = '/home/u764/Downloads/output-images'


def otsu_threshold(gray_image: np.ndarray) -> np.ndarray:
    return cv2.threshold(gray_image,
                         0,
                         255,
                         cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]


def create_background_image(size: tuple[int, int] = INPUT_SIZE,
                            background_color_range: tuple[int, int] = DEFAULT_BACKGROUND_COLOR_RANGE) -> np.ndarray:
    image_shape = (size[1], size[0])
    random_matrix = np.random.uniform(low=float(background_color_range[0]),
                                      high=float(background_color_range[1]),
                                      size=image_shape)

    random_matrix[random_matrix > 255.0] = 255
    random_matrix[random_matrix < 0.0] = 0

    return np.array(random_matrix, np.uint8)


def draw_foreground_on_background(foreground_image: np.ndarray,
                                  background_image: np.ndarray,
                                  position: tuple[int, int],
                                  foreground_color_range: tuple[int, int] = DEFAULT_FOREGROUND_COLOR_RANGE):
    binary_foreground_image = foreground_image
    if np.any(foreground_image > 0):
        binary_foreground_image = otsu_threshold(foreground_image)

    foreground_image_mask = binary_foreground_image == 0

    foreground_colors = np.random.uniform(low=foreground_color_range[0],
                                          high=foreground_color_range[1],
                                          size=foreground_image.shape)

    x, y = position
    final_image = np.array(background_image)

    drawing_part = final_image[y: y + foreground_image.shape[0], x: x + foreground_image.shape[1]]
    drawing_part[foreground_image_mask] = foreground_colors[foreground_image_mask]

    return final_image


def draw_foreground_on_background_randomly(foreground_image: np.ndarray,
                                           background_image: np.ndarray,
                                           x_range: tuple[int, int],
                                           y_range: tuple[int, int],
                                           foreground_color_range: tuple[int, int] = DEFAULT_FOREGROUND_COLOR_RANGE,
                                           resize_foreground_if_necessary: bool = True):
    width = foreground_image.shape[1]
    height = foreground_image.shape[0]
    resized_foreground_image = foreground_image
    if foreground_image.shape[0] > background_image.shape[0] or \
            foreground_image.shape[1] > background_image.shape[1]:
        if resize_foreground_if_necessary:
            width = random.randint(background_image.shape[1] // 2, background_image.shape[1])
            height = random.randint(background_image.shape[0] // 2, background_image.shape[0])
            resized_foreground_image = cv2.resize(foreground_image,
                                                  (width, height),
                                                  interpolation=cv2.INTER_CUBIC)
        else:
            width = max(background_image.shape[1], foreground_image.shape[1])
            height = max(background_image.shape[0], foreground_image.shape[0])
            resized_foreground_image = foreground_image[:height, :width]

    x = random.randint(max(0, min(x_range[0], background_image.shape[1] - width)),
                       min(x_range[1], background_image.shape[1] - width))
    y = random.randint(max(0, min(y_range[0], background_image.shape[0] - height)),
                       min(y_range[1], background_image.shape[0] - height))

    return draw_foreground_on_background(resized_foreground_image,
                                         background_image,
                                         (x, y),
                                         foreground_color_range)


def draw_line_on_background_randomly(background_image: np.ndarray,
                                     x1_range: tuple[int, int],
                                     y1_range: tuple[int, int],
                                     x2_range: tuple[int, int],
                                     y2_range: tuple[int, int],
                                     line_thickness_range: tuple[int, int],
                                     foreground_color_range: tuple[int, int] = DEFAULT_FOREGROUND_COLOR_RANGE):
    x1 = random.randint(x1_range[0], x1_range[1])
    x2 = random.randint(x2_range[0], x2_range[1])
    y1 = random.randint(y1_range[0], y1_range[1])
    y2 = random.randint(y2_range[0], y2_range[1])
    line_thickness = random.randint(line_thickness_range[0], line_thickness_range[1])
    foreground_width = abs(x2 - x1)
    foreground_height = abs(y2 - y1)

    foreground_image = np.ones((foreground_height, foreground_width), np.uint8) * 255
    if y1 <= y2:
        cv2.line(foreground_image, (0, 0), (foreground_width - 1, foreground_height - 1), 0, line_thickness)
    else:
        cv2.line(foreground_image, (0, foreground_height - 1), (foreground_width - 1, 0), 0, line_thickness)

    return draw_foreground_on_background(foreground_image,
                                         background_image,
                                         (min(x1, x2), min(y1, y2)),
                                         foreground_color_range)


def draw_box_on_background_randomly(background_image: np.ndarray,
                                    x_range: tuple[int, int],
                                    y_range: tuple[int, int],
                                    width_range: tuple[int, int],
                                    height_range: tuple[int, int],
                                    line_thickness_range: tuple[int, int],
                                    foreground_color_range: tuple[int, int] = DEFAULT_FOREGROUND_COLOR_RANGE):
    width = random.randint(width_range[0], width_range[1])
    height = random.randint(height_range[0], height_range[1])

    foreground_image = np.ones((height, width), np.uint8) * 255
    line_thickness = random.randint(line_thickness_range[0], line_thickness_range[1])

    foreground_image[:line_thickness, :] = 0
    foreground_image[:, :line_thickness] = 0
    foreground_image[-line_thickness:, :] = 0
    foreground_image[:, -line_thickness:] = 0

    return draw_foreground_on_background_randomly(foreground_image,
                                                  background_image,
                                                  x_range,
                                                  y_range,
                                                  foreground_color_range,
                                                  False)


def remove_white_border(binary_image):
    mask = (255 - binary_image) > 0

    height, width = binary_image.shape[0:2]
    mask1, mask2 = mask.any(0), mask.any(1)
    x1, x2 = mask1.argmax(), width - mask1[::-1].argmax()
    y1, y2 = mask2.argmax(), height - mask2[::-1].argmax()

    return binary_image[y1:y2, x1:x2]


def get_image_box_without_white_border(binary_image):
    mask = (255 - binary_image) > 0

    height, width = binary_image.shape[0:2]
    mask1, mask2 = mask.any(0), mask.any(1)
    x1, x2 = mask1.argmax(), width - mask1[::-1].argmax()
    y1, y2 = mask2.argmax(), height - mask2[::-1].argmax()

    return x1, y1, x2, y2


def get_printed_text(text,
                     font_path,
                     font_size):
    image = np.zeros((1, 1), np.uint8)
    font = ImageFont.truetype(font_path, font_size)

    image = Image.fromarray(image, 'L')
    draw = ImageDraw.Draw(image)
    width, height = draw.textsize(text, font=font)

    image = np.ones((height, width), np.uint8) * 255
    image = Image.fromarray(image, 'L')
    draw = ImageDraw.Draw(image)

    draw.text((0, 0), text, fill=0, font=font)
    image = np.asarray(image).astype(np.uint8)
    x1, y1, x2, y2 = get_image_box_without_white_border(otsu_threshold(image))

    return image[y1:y2, x1:x2]


def random_bool():
    return random.randint(1, 100) >= 50


class MinimumRecurrenceSampler:
    def _refresh(self):
        self.items_copy = [x for x in self.items]
        random.shuffle(self.items_copy)

    def __init__(self, items):
        self.items = items
        self.items_copy = [x for x in items]
        random.shuffle(self.items_copy)

    def next(self):
        if len(self.items_copy) == 0:
            self._refresh()
        return self.items_copy.pop(0)


class FontsSampler:
    def __init__(self, font_families_base_path=FONTS_PATH):
        font_families = filter(lambda d: os.path.isdir(os.path.join(font_families_base_path, d)),
                               os.listdir(font_families_base_path))
        font_families_paths = [os.path.join(font_families_base_path, d) for d in font_families]

        font_samplers = []
        for font_family_path in font_families_paths:
            fonts = filter(lambda f: os.path.isfile(os.path.join(font_family_path, f)) and
                                     (f.lower().endswith('.ttf') or
                                      f.lower().endswith('.otf')),
                           os.listdir(font_family_path))
            fonts_paths = [os.path.join(font_family_path, f) for f in fonts]
            font_samplers.append(MinimumRecurrenceSampler(fonts_paths))

        self.font_family_sampler = MinimumRecurrenceSampler(font_samplers)

    def next(self):
        font_family = self.font_family_sampler.next()
        return font_family.next()


class SignaturesSampler:
    def __init__(self, signatures_path=SIGNATURES_PATH):
        paths = [os.path.join(signatures_path, f) for f in os.listdir(signatures_path)]
        signatures = []

        for p in paths:
            image = cv2.imread(p, cv2.IMREAD_GRAYSCALE)
            if image is not None:
                signatures.append(image)

        self.signatures_sampler = MinimumRecurrenceSampler(signatures)

    def next(self):
        return self.signatures_sampler.next()


def change_image_quality(image, quality):
    encoded = cv2.imencode('.jpg', image, [int(cv2.IMWRITE_JPEG_QUALITY), quality])[1]
    return cv2.imdecode(encoded, cv2.IMREAD_GRAYSCALE)


def main():
    signatures_sampler = SignaturesSampler()
    fonts_sampler = FontsSampler()

    if not os.path.isdir(INPUT_IMAGES_PATH):
        os.makedirs(INPUT_IMAGES_PATH)
    if not os.path.isdir(OUTPUT_IMAGES_PATH):
        os.makedirs(OUTPUT_IMAGES_PATH)

    progress_bar = tqdm(total=NUMBER_OF_SAMPLES)

    for i in range(NUMBER_OF_SAMPLES):
        signature = signatures_sampler.next()
        threshold = random.randint(55, 200)
        background_image = create_background_image(background_color_range=(threshold + 1, 255))
        foreground_color_range = (0, threshold)

        generated_image = draw_foreground_on_background_randomly(signature,
                                                                 background_image,
                                                                 (0, background_image.shape[1]),
                                                                 (0, background_image.shape[0]),
                                                                 foreground_color_range)
        clean_image = np.array(generated_image)
        clean_image[clean_image > threshold] = 255
        clean_image[clean_image <= threshold] = 0

        line_y = random.randint(LINE_Y_RANGE[0],
                                LINE_Y_RANGE[1])

        if random_bool():
            generated_image = draw_line_on_background_randomly(
                generated_image,
                (LINE_X_RANGE[0], LINE_X_RANGE[0] + LINE_X_MARGIN),
                (line_y - LINE_Y_MARGIN, line_y - 1),
                (LINE_X_RANGE[1] - LINE_X_MARGIN, LINE_X_RANGE[1] - 1),
                (line_y - LINE_Y_MARGIN, line_y - 1),
                LINE_THICKNESS_RANGE,
                foreground_color_range)
        else:
            generated_image = draw_box_on_background_randomly(
                generated_image,
                (0, background_image.shape[1]),
                (1, background_image.shape[0]),
                (BOX_MIN_WIDTH, BOX_MAX_WIDTH),
                (BOX_MIN_HEIGHT, BOX_MAX_HEIGHT),
                LINE_THICKNESS_RANGE,
                foreground_color_range)

        printed_text = get_printed_text('Signature',
                                        fonts_sampler.next(),
                                        random.randint(PRINTED_TEXT_FONT_SIZE_RANGE[0],
                                                       PRINTED_TEXT_FONT_SIZE_RANGE[1]))

        generated_image = draw_foreground_on_background_randomly(printed_text,
                                                                 generated_image,
                                                                 (0, background_image.shape[1]),
                                                                 (0, background_image.shape[0]),
                                                                 foreground_color_range)

        if random_bool():
            if random_bool():
                generated_image = cv2.blur(generated_image, (3, 3))
            else:
                generated_image = cv2.GaussianBlur(generated_image, (3, 3), 0)

        if random_bool():
            generated_image = change_image_quality(generated_image, random.randint(IMAGE_QUALITY_RANGE[0],
                                                                                   IMAGE_QUALITY_RANGE[1]))

        input_image_path = os.path.join(INPUT_IMAGES_PATH, f'sample-{i}.bmp')
        output_image_path = os.path.join(OUTPUT_IMAGES_PATH, f'sample-{i}.bmp')

        cv2.imwrite(input_image_path, generated_image)
        cv2.imwrite(output_image_path, clean_image)

        progress_bar.update()
    progress_bar.close()


if __name__ == '__main__':
    main()
