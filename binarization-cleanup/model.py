import math
import os
import pickle
import random
import shutil
from enum import Enum

import cv2
import numpy as np
import tensorflow as tf
from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.keras.layers import Input, Conv2D, BatchNormalization, concatenate, Conv2DTranspose, ReLU, Add, \
    Multiply, Activation
from tensorflow.keras.losses import binary_crossentropy
from tensorflow.keras.models import Model
from tensorflow.keras.models import load_model
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.utils import Sequence

INPUT_IMAGES_PATH = 'C:\\Users\\USER\\Desktop\\UNet\\input'
OUTPUT_IMAGES_PATH = 'C:\\Users\\USER\\Desktop\\UNet\\output'
INPUT_SIZE = (400, 200)
INPUT_SHAPE = (INPUT_SIZE[1], INPUT_SIZE[0], 1)
TRAINING_IMAGES_PERCENTAGE = 0.7
VALIDATION_TEST_IMAGES_PERCENTAGE = 0.15
EPSILON = 1e-8

MODEL_FILE_NAME = 'model'

TRAINING_BATCH_SIZE = 2
VALIDATION_BATCH_SIZE = 2


def dice_coefficient(y_true, y_pred):
    y_true_f = tf.reshape(y_true, [-1])
    y_pred_f = tf.reshape(y_pred, [-1])
    intersection = tf.reduce_sum(y_true_f * y_pred_f)
    return (2.0 * intersection + 1.0) / (tf.reduce_sum(y_true_f) + tf.reduce_sum(y_pred_f) + 1.0)


# def custom_loss_function(y_true, y_pred):
#     return binary_crossentropy(y_true, y_pred)  - dice_coefficient(y_true, y_pred) + 1.0


# def custom_loss_function(y_true, y_pred):
#     return tf.reduce_mean(4.0 * (0.5 - 0.5 * y_pred * y_pred - 0.5 * (y_pred - 1) * (y_pred - 1))) - dice_coefficient(y_true, y_pred) + 1.0


def conv_batch_norm_relu_layer(input_layer, filters, kernel=(3, 3), strides=None):
    if strides is None:
        conv = Conv2D(filters,
                      kernel,
                      kernel_initializer='he_normal',
                      use_bias=False,
                      padding='same')(input_layer)
    else:
        conv = Conv2D(filters,
                      kernel,
                      strides=strides,
                      use_bias=False,
                      kernel_initializer='he_normal',
                      padding='same')(input_layer)
    conv = BatchNormalization()(conv)
    conv = ReLU()(conv)
    return conv


def stacked_conv(input_layer, filters, kernel=(3, 3)):
    conv = conv_batch_norm_relu_layer(input_layer, filters, kernel)
    return conv_batch_norm_relu_layer(conv, filters, kernel)


def recurrent_residual_conv(input_layer, filters, kernel=(3, 3), t=2):
    conv = Conv2D(filters,
                  kernel,
                  kernel_initializer='he_normal',
                  use_bias=False,
                  padding='same')
    output = input_layer
    for i in range(t):
        if i == 0:
            output = conv(output)
            output = BatchNormalization()(output)
            output = ReLU()(output)
        output = Add()([output, input_layer])
        output = conv(output)
        output = BatchNormalization()(output)
        output = ReLU()(output)
    return output


# def recurrent_residual_conv(input_layer, filters, kernel=(3, 3), t=2):
#     output = input_layer
#     for i in range(t):
#         if i == 0:
#             output = conv_batch_norm_relu_layer(output, filters, kernel)
#             output = conv_batch_norm_relu_layer(output, filters, kernel)
#         output = Add()([output, input_layer])
#         output = conv_batch_norm_relu_layer(output, filters, kernel)
#         output = conv_batch_norm_relu_layer(output, filters, kernel)
#     return output


def stacked_recurrent_residual_conv(input_layer, filters, kernel=(3, 3), t=2):
    conv = Conv2D(filters,
                  (1, 1),
                  kernel_initializer='he_normal',
                  padding='same')(input_layer)
    conv = recurrent_residual_conv(conv, filters, kernel, t)
    return recurrent_residual_conv(conv, filters, kernel, t)


def deconv_batch_norm_relu_layer(input_layer, filters, kernel=(3, 3)):
    conv = Conv2DTranspose(filters,
                           kernel,
                           strides=(2, 2),
                           kernel_initializer='he_normal',
                           use_bias=False,
                           padding='same')(input_layer)
    conv = BatchNormalization()(conv)
    conv = ReLU()(conv)
    return conv


def attention_layer(g, x, filters):
    g_conv = conv_batch_norm_relu_layer(g, filters, (1, 1))
    x_conv = conv_batch_norm_relu_layer(x, filters, (1, 1))
    psi = Add()([g_conv, x_conv])
    psi = ReLU()(psi)
    psi = Conv2D(1,
                 (1, 1),
                 kernel_initializer='he_normal',
                 use_bias=False,
                 padding='same')(psi)
    psi = BatchNormalization()(psi)
    psi = Activation(tf.sigmoid)(psi)
    return Multiply()([x, psi])


# def u_net():
#     filters = [32, 64, 128, 256]
#     # encoder
#     input_layer = Input(shape=INPUT_SHAPE)
#     encoder_conv_layers = []
#     current_layer = input_layer
#     for i in range(len(filters) - 1):
#         n = filters[i]
#         conv = stacked_conv(current_layer, n)
#         encoder_conv_layers.append(conv)
#         current_layer = conv_batch_norm_relu_layer(conv, n, (3, 3), strides=(2, 2))
#     # bottleneck
#     current_layer = stacked_conv(current_layer, filters[-1], (3, 3))
#     # decoder
#     for i in range(len(filters) - 2, -1, -1):
#         n = filters[i]
#         encoder_conv = encoder_conv_layers[i]
#         # encoder_conv = Dropout(0.5)(encoder_conv)
#         decoder_deconv = deconv_batch_norm_relu_layer(current_layer, n, (3, 3))
#         encoder_decoder_attention = attention_layer(decoder_deconv, encoder_conv, n)
#         up = concatenate([decoder_deconv, encoder_decoder_attention], axis=-1)
#         current_layer = stacked_conv(up, n, (3, 3))
#     # output
#     prediction = Conv2D(1,
#                         (1, 1),
#                         kernel_initializer='he_normal',
#                         use_bias=False,
#                         padding='same',
#                         activation='sigmoid')(current_layer)
#     return Model(inputs=input_layer, outputs=prediction)

def u_net():
    filters = [32, 64, 128, 256]
    # encoder
    input_layer = Input(shape=INPUT_SHAPE)
    batch_norm = BatchNormalization()(input_layer)

    conv1 = stacked_conv(batch_norm, filters[0])
    pool1 = conv_batch_norm_relu_layer(
        conv1, filters[0], (3, 3), strides=(2, 2))
    conv2 = stacked_conv(pool1, filters[1])
    pool2 = conv_batch_norm_relu_layer(
        conv2, filters[1], (3, 3), strides=(2, 2))
    conv3 = stacked_conv(pool2, filters[2])
    pool3 = conv_batch_norm_relu_layer(
        conv3, filters[2], (3, 3), strides=(2, 2))
    # bottleneck
    conv4 = stacked_conv(pool3, filters[3], (3, 3))
    # decoder
    up1 = concatenate([deconv_batch_norm_relu_layer(
        conv4, filters[3], (3, 3)), conv3], axis=-1)
    conv5 = stacked_conv(up1, filters[2], (3, 3))
    up2 = concatenate([deconv_batch_norm_relu_layer(
        conv5, filters[2], (3, 3)), conv2], axis=-1)
    conv6 = stacked_conv(up2, filters[1], (3, 3))
    up3 = concatenate([deconv_batch_norm_relu_layer(
        conv6, filters[1], (3, 3)), conv1], axis=-1)
    conv7 = stacked_conv(up3, filters[0], (3, 3))
    # output
    prediction = Conv2D(1,
                        (1, 1),
                        kernel_initializer='he_normal',
                        use_bias=True,
                        padding='same',
                        activation='sigmoid')(conv7)
    return Model(inputs=input_layer, outputs=prediction)


def pre_process(image_tensor: np.ndarray) -> np.ndarray:
    image_tensor = 255.0 - np.array(image_tensor, np.float32)
    return (image_tensor / 255.0).reshape(INPUT_SHAPE)


class DataGenerator(Sequence):
    def __init__(self,
                 input_image_paths,
                 output_image_paths,
                 batch_size):
        self.count = len(input_image_paths)
        self.input_image_paths = input_image_paths
        self.output_image_paths = output_image_paths
        self.batch_size = batch_size

    def __getitem__(self, index):
        input_image_paths = self.input_image_paths[index *
                                                   self.batch_size: (index + 1) * self.batch_size]
        input_images = [pre_process(cv2.imread(
            f, cv2.IMREAD_GRAYSCALE)) for f in input_image_paths]

        output_image_paths = self.output_image_paths[index *
                                                     self.batch_size: (index + 1) * self.batch_size]
        output_images = [pre_process(cv2.imread(
            f, cv2.IMREAD_GRAYSCALE)) for f in output_image_paths]

        return np.array(input_images), np.array(output_images)

    def __len__(self):
        return math.ceil(self.count / self.batch_size)


def load_image_paths():
    input_image_paths = os.listdir(INPUT_IMAGES_PATH)
    output_image_paths = os.listdir(OUTPUT_IMAGES_PATH)

    input_image_paths = filter(lambda x: x.endswith('.png'), input_image_paths)
    output_image_paths = filter(
        lambda x: x.endswith('.png'), output_image_paths)

    input_image_paths = [os.path.join(INPUT_IMAGES_PATH, f)
                         for f in input_image_paths]
    output_image_paths = [os.path.join(
        OUTPUT_IMAGES_PATH, f) for f in output_image_paths]

    return input_image_paths, output_image_paths


def train_validation_test_split(input_images, output_images):
    indices = list(range(len(input_images)))
    random.shuffle(indices)

    input_images = [input_images[i] for i in indices]
    output_images = [output_images[i] for i in indices]

    train_images_count = int(len(input_images) * TRAINING_IMAGES_PERCENTAGE)
    validation_test_images_count = int(
        len(input_images) * VALIDATION_TEST_IMAGES_PERCENTAGE)

    return ((input_images[:train_images_count],
             output_images[:train_images_count]),
            (input_images[train_images_count:train_images_count + validation_test_images_count],
             output_images[train_images_count:train_images_count + validation_test_images_count]),
            (input_images[train_images_count + validation_test_images_count:],
             output_images[train_images_count + validation_test_images_count:]))


def cache_object(obj, file):
    with open(file, 'wb') as f:
        pickle.dump(obj, f)


def load_cached(file):
    with open(file, 'rb') as f:
        return pickle.load(f)


class ModelLoadingMode(Enum):
    MinimumMetricValue = 0
    MaximumMetricValue = 1


def rename_best_model(mode=ModelLoadingMode.MinimumMetricValue):
    all_models = list(filter(lambda x: x[-3:] == '.h5', os.listdir(os.curdir)))
    all_metrics = []

    for model in all_models:
        i1 = model.rfind('_')
        i2 = model.rfind('.')

        if i1 == -1 or i2 == -1:
            all_metrics.append(None)
            continue

        metric = model[i1 + 1: i2]

        try:
            all_metrics.append(float(metric))
        except ValueError:
            all_metrics.append(None)

    model_metric_pairs = [[all_models[i], all_metrics[i]]
                          for i in range(len(all_models)) if all_metrics[i] is not None]
    if mode == ModelLoadingMode.MaximumMetricValue:
        best_index = np.array(model_metric_pairs)[:, 1:2].flatten().argmax()
    else:
        best_index = np.array(model_metric_pairs)[:, 1:2].flatten().argmin()

    shutil.copy(model_metric_pairs[best_index][0], MODEL_FILE_NAME + '.h5')


def train_model():
    unet_model = u_net()
    unet_model.summary()

    unet_model.compile(loss=binary_crossentropy, optimizer=Adam())

    if os.path.isfile('x_train.pickle') and os.path.isfile('y_train.pickle') and \
            os.path.isfile('x_val.pickle') and os.path.isfile('y_val.pickle') and \
            os.path.isfile('x_test.pickle') and os.path.isfile('y_test.pickle'):
        x_train = load_cached('x_train.pickle')
        y_train = load_cached('y_train.pickle')
        x_val = load_cached('x_val.pickle')
        y_val = load_cached('y_val.pickle')
    else:
        input_image_paths, output_image_paths = load_image_paths()
        (x_train, y_train), (x_val, y_val), (x_test, y_test) = train_validation_test_split(
            input_image_paths, output_image_paths)
        cache_object(x_train, 'x_train.pickle')
        cache_object(y_train, 'y_train.pickle')
        cache_object(x_val, 'x_val.pickle')
        cache_object(y_val, 'y_val.pickle')
        cache_object(x_test, 'x_test.pickle')
        cache_object(y_test, 'y_test.pickle')

    train_data_generator = DataGenerator(x_train, y_train, TRAINING_BATCH_SIZE)
    val_data_generator = DataGenerator(x_val, y_val, VALIDATION_BATCH_SIZE)

    model_last_checkpoint_callback = ModelCheckpoint(
        filepath=MODEL_FILE_NAME + '_{epoch:02d}_{loss:0.4f}_{val_loss:0.4f}.h5',
        save_best_only=False)

    history = unet_model.fit(train_data_generator,
                             epochs=50,
                             validation_data=val_data_generator,
                             callbacks=[model_last_checkpoint_callback])

    cache_object(history, MODEL_FILE_NAME + '_history.pickle')
    rename_best_model()


def evaluate_model():
    unet_model = load_model(MODEL_FILE_NAME + '.h5', compile=False)
    unet_model.compile(loss=binary_crossentropy, optimizer=Adam())

    x_test = load_cached('x_test.pickle')
    y_test = load_cached('y_test.pickle')

    test_data_generator = DataGenerator(x_test, y_test, 2)

    results = unet_model.evaluate(test_data_generator)

    cache_object(results, 'evaluation_results.pickle')
    print(results)


if __name__ == '__main__':
    train_model()
    evaluate_model()
