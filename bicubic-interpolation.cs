public static int Floor(double value)
{
    var floor = (int) value;
    return floor > value ? floor - 1 : floor;
}

public static double InterpolateCubic(double x)
{
    const double a = -0.75;

    var fx = Math.Abs(x);

    return fx >= 0 && fx <= 1
        ? ((a + 2) * fx - (a + 3)) * fx * fx + 1
        : fx > 1 && fx <= 2
            ? ((a * fx - 5 * a) * fx + 8 * a) * fx - 4 * a
            : 0.0;
}

public static unsafe Bitmap ResizeBiCubicInterpolation(Bitmap image, int dstWidth, int dstHeight)
{
    Bitmap result = null;
    BitmapData resultData = null;
    BitmapData imageData = null;

    try
    {
        int srcWidth = image.Width;
        int srcHeight = image.Height;

        result = BitmapFactory.CreateImage(dstWidth, dstHeight, image.PixelFormat);

        imageData = image.LockBits(new Rectangle(0, 0, srcWidth, srcHeight),
            ImageLockMode.ReadWrite,
            image.PixelFormat);
        resultData = result.LockBits(new Rectangle(0, 0, dstWidth, dstHeight),
            ImageLockMode.ReadWrite,
            result.PixelFormat);

        int channels = Math.Max(1, Image.GetPixelFormatSize(imageData.PixelFormat) / 8);

        double scaleX = (double) dstWidth / (double) srcWidth;
        double scaleY = (double) dstHeight / (double) srcHeight;

        byte* imagePtr = (byte*) (void*) imageData.Scan0;
        byte* resultPtr = (byte*) (void*) resultData.Scan0;

        double invScaleX = 1.0 / scaleX;
        double invScaleY = 1.0 / scaleY;

        int imageStride = imageData.Stride;
        int resultStride = resultData.Stride;
        int resultOffset = resultStride - dstWidth * channels;

        double[] hKernelCoefficients = new double[dstWidth * 4];
        int[] hKernelX = new int[dstWidth * 4];
        int[] hKernelI = new int[dstWidth * 4];

        double[] vKernelCoefficients = new double[dstHeight * 4];
        int[] vKernelY = new int[dstWidth * 4];
        int[] vKernelI = new int[dstWidth * 4];

        int dY, dX, c;
        double sX, sY;
        double dX1, dX2, dX3, dX4, dY1, dY2, dY3, dY4;
        double kX1, kX2, kX3, kX4, kY1, kY2, kY3, kY4;
        int iX1, iX2, iX3, iX4, iY1, iY2, iY3, iY4;
        int x1, x2, x3, x4, y1, y2, y3, y4;
        bool bX1, bX2, bX3, bX4;
        bool bY1, bY2, bY3, bY4;

        double k11,
            k12,
            k13,
            k14,
            k21,
            k22,
            k23,
            k24,
            k31,
            k32,
            k33,
            k34,
            k41,
            k42,
            k43,
            k44;

        bool b11,
            b12,
            b13,
            b14,
            b21,
            b22,
            b23,
            b24,
            b31,
            b32,
            b33,
            b34,
            b41,
            b42,
            b43,
            b44;


        byte* ptr11,
            ptr12,
            ptr13,
            ptr14,
            ptr21,
            ptr22,
            ptr23,
            ptr24,
            ptr31,
            ptr32,
            ptr33,
            ptr34,
            ptr41,
            ptr42,
            ptr43,
            ptr44;

        double v, diffV;
        int intV;

        fixed (double* pVKernelCoefficients = &vKernelCoefficients[0])
        {
            fixed (int* pVKernelY = &vKernelY[0])
            {
                fixed (int* pVKernelI = &vKernelI[0])
                {
                    fixed (double* pHKernelCoefficients = &hKernelCoefficients[0])
                    {
                        fixed (int* pHKernelX = &hKernelX[0])
                        {
                            fixed (int* pHKernelI = &hKernelI[0])
                            {
                                double* pTmpVKernelCoefficients = pVKernelCoefficients;
                                int* pTmpVKernelY = pVKernelY;
                                int* pTmpVKernelI = pVKernelI;

                                double* pTmpHKernelCoefficients = pHKernelCoefficients;
                                int* pTmpHKernelX = pHKernelX;
                                int* pTmpHKernelI = pHKernelI;

                                for (int x = 0; x < dstWidth; x++)
                                {
                                    sX = (x + 0.5) * invScaleX - 0.5;
                                    var sXFloor = Floor(sX);

                                    dX1 = 1 + sX - sXFloor;
                                    dX2 = sX - sXFloor;
                                    dX3 = sXFloor + 1 - sX;
                                    dX4 = sXFloor + 2 - sX;

                                    *pTmpHKernelCoefficients = InterpolateCubic(dX1);
                                    pTmpHKernelCoefficients++;
                                    *pTmpHKernelCoefficients = InterpolateCubic(dX2);
                                    pTmpHKernelCoefficients++;
                                    *pTmpHKernelCoefficients = InterpolateCubic(dX3);
                                    pTmpHKernelCoefficients++;
                                    *pTmpHKernelCoefficients = InterpolateCubic(dX4);
                                    pTmpHKernelCoefficients++;

                                    x1 = (int) (sX - dX1);
                                    x2 = (int) (sX - dX2);
                                    x3 = (int) (sX + dX3);
                                    x4 = (int) (sX + dX4);

                                    *pTmpHKernelX = x1;
                                    pTmpHKernelX++;
                                    *pTmpHKernelX = x2;
                                    pTmpHKernelX++;
                                    *pTmpHKernelX = x3;
                                    pTmpHKernelX++;
                                    *pTmpHKernelX = x4;
                                    pTmpHKernelX++;

                                    *pTmpHKernelI = x1 * channels;
                                    pTmpHKernelI++;
                                    *pTmpHKernelI = x2 * channels;
                                    pTmpHKernelI++;
                                    *pTmpHKernelI = x3 * channels;
                                    pTmpHKernelI++;
                                    *pTmpHKernelI = x4 * channels;
                                    pTmpHKernelI++;
                                }

                                for (int y = 0; y < dstHeight; y++)
                                {
                                    sY = (y + 0.5) * invScaleY - 0.5;
                                    var sYFloor = Floor(sY);

                                    dY1 = 1 + sY - sYFloor;
                                    dY2 = sY - sYFloor;
                                    dY3 = sYFloor + 1 - sY;
                                    dY4 = sYFloor + 2 - sY;

                                    *pTmpVKernelCoefficients = InterpolateCubic(dY1);
                                    pTmpVKernelCoefficients++;
                                    *pTmpVKernelCoefficients = InterpolateCubic(dY2);
                                    pTmpVKernelCoefficients++;
                                    *pTmpVKernelCoefficients = InterpolateCubic(dY3);
                                    pTmpVKernelCoefficients++;
                                    *pTmpVKernelCoefficients = InterpolateCubic(dY4);
                                    pTmpVKernelCoefficients++;

                                    y1 = (int) (sY - dY1);
                                    y2 = (int) (sY - dY2);
                                    y3 = (int) (sY + dY3);
                                    y4 = (int) (sY + dY4);

                                    *pTmpVKernelY = y1;
                                    pTmpVKernelY++;
                                    *pTmpVKernelY = y2;
                                    pTmpVKernelY++;
                                    *pTmpVKernelY = y3;
                                    pTmpVKernelY++;
                                    *pTmpVKernelY = y4;
                                    pTmpVKernelY++;

                                    *pTmpVKernelI = y1 * imageStride;
                                    pTmpVKernelI++;
                                    *pTmpVKernelI = y2 * imageStride;
                                    pTmpVKernelI++;
                                    *pTmpVKernelI = y3 * imageStride;
                                    pTmpVKernelI++;
                                    *pTmpVKernelI = y4 * imageStride;
                                    pTmpVKernelI++;
                                }

                                pTmpVKernelCoefficients = pVKernelCoefficients;
                                pTmpVKernelY = pVKernelY;
                                pTmpVKernelI = pVKernelI;

                                for (dY = 0; dY < dstHeight; dY++)
                                {
                                    kY1 = *pTmpVKernelCoefficients;
                                    pTmpVKernelCoefficients++;
                                    kY2 = *pTmpVKernelCoefficients;
                                    pTmpVKernelCoefficients++;
                                    kY3 = *pTmpVKernelCoefficients;
                                    pTmpVKernelCoefficients++;
                                    kY4 = *pTmpVKernelCoefficients;
                                    pTmpVKernelCoefficients++;

                                    y1 = *pTmpVKernelY;
                                    pTmpVKernelY++;
                                    y2 = *pTmpVKernelY;
                                    pTmpVKernelY++;
                                    y3 = *pTmpVKernelY;
                                    pTmpVKernelY++;
                                    y4 = *pTmpVKernelY;
                                    pTmpVKernelY++;

                                    iY1 = *pTmpVKernelI;
                                    pTmpVKernelI++;
                                    iY2 = *pTmpVKernelI;
                                    pTmpVKernelI++;
                                    iY3 = *pTmpVKernelI;
                                    pTmpVKernelI++;
                                    iY4 = *pTmpVKernelI;
                                    pTmpVKernelI++;

                                    bY1 = y1 < 0 || y1 >= srcHeight;
                                    bY2 = y2 < 0 || y2 >= srcHeight;
                                    bY3 = y3 < 0 || y3 >= srcHeight;
                                    bY4 = y4 < 0 || y4 >= srcHeight;

                                    pTmpHKernelCoefficients = pHKernelCoefficients;
                                    pTmpHKernelX = pHKernelX;
                                    pTmpHKernelI = pHKernelI;

                                    for (dX = 0; dX < dstWidth; dX++)
                                    {
                                        kX1 = *pTmpHKernelCoefficients;
                                        pTmpHKernelCoefficients++;
                                        kX2 = *pTmpHKernelCoefficients;
                                        pTmpHKernelCoefficients++;
                                        kX3 = *pTmpHKernelCoefficients;
                                        pTmpHKernelCoefficients++;
                                        kX4 = *pTmpHKernelCoefficients;
                                        pTmpHKernelCoefficients++;

                                        x1 = *pTmpHKernelX;
                                        pTmpHKernelX++;
                                        x2 = *pTmpHKernelX;
                                        pTmpHKernelX++;
                                        x3 = *pTmpHKernelX;
                                        pTmpHKernelX++;
                                        x4 = *pTmpHKernelX;
                                        pTmpHKernelX++;

                                        iX1 = *pTmpHKernelI;
                                        pTmpHKernelI++;
                                        iX2 = *pTmpHKernelI;
                                        pTmpHKernelI++;
                                        iX3 = *pTmpHKernelI;
                                        pTmpHKernelI++;
                                        iX4 = *pTmpHKernelI;
                                        pTmpHKernelI++;

                                        bX1 = x1 < 0 || x1 >= srcWidth;
                                        bX2 = x2 < 0 || x2 >= srcWidth;
                                        bX3 = x3 < 0 || x3 >= srcWidth;
                                        bX4 = x4 < 0 || x4 >= srcWidth;

                                        k11 = kY1 * kX1;
                                        k21 = kY2 * kX1;
                                        k31 = kY3 * kX1;
                                        k41 = kY4 * kX1;
                                        k12 = kY1 * kX2;
                                        k22 = kY2 * kX2;
                                        k32 = kY3 * kX2;
                                        k42 = kY4 * kX2;
                                        k13 = kY1 * kX3;
                                        k23 = kY2 * kX3;
                                        k33 = kY3 * kX3;
                                        k43 = kY4 * kX3;
                                        k14 = kY1 * kX4;
                                        k24 = kY2 * kX4;
                                        k34 = kY3 * kX4;
                                        k44 = kY4 * kX4;

                                        b11 = bY1 || bX1;
                                        b21 = bY2 || bX1;
                                        b31 = bY3 || bX1;
                                        b41 = bY4 || bX1;
                                        b12 = bY1 || bX2;
                                        b22 = bY2 || bX2;
                                        b32 = bY3 || bX2;
                                        b42 = bY4 || bX2;
                                        b13 = bY1 || bX3;
                                        b23 = bY2 || bX3;
                                        b33 = bY3 || bX3;
                                        b43 = bY4 || bX3;
                                        b14 = bY1 || bX4;
                                        b24 = bY2 || bX4;
                                        b34 = bY3 || bX4;
                                        b44 = bY4 || bX4;

                                        ptr11 = imagePtr + iY1 + iX1;
                                        ptr21 = imagePtr + iY2 + iX1;
                                        ptr31 = imagePtr + iY3 + iX1;
                                        ptr41 = imagePtr + iY4 + iX1;
                                        ptr12 = imagePtr + iY1 + iX2;
                                        ptr22 = imagePtr + iY2 + iX2;
                                        ptr32 = imagePtr + iY3 + iX2;
                                        ptr42 = imagePtr + iY4 + iX2;
                                        ptr13 = imagePtr + iY1 + iX3;
                                        ptr23 = imagePtr + iY2 + iX3;
                                        ptr33 = imagePtr + iY3 + iX3;
                                        ptr43 = imagePtr + iY4 + iX3;
                                        ptr14 = imagePtr + iY1 + iX4;
                                        ptr24 = imagePtr + iY2 + iX4;
                                        ptr34 = imagePtr + iY3 + iX4;
                                        ptr44 = imagePtr + iY4 + iX4;

                                        for (c = 0; c < channels; c++)
                                        {
                                            v = (b11 ? 0.0 : k11 * (ptr11 + c)[0]) +
                                                (b21 ? 0.0 : k21 * (ptr21 + c)[0]) +
                                                (b31 ? 0.0 : k31 * (ptr31 + c)[0]) +
                                                (b41 ? 0.0 : k41 * (ptr41 + c)[0]) +
                                                (b12 ? 0.0 : k12 * (ptr12 + c)[0]) +
                                                (b22 ? 0.0 : k22 * (ptr22 + c)[0]) +
                                                (b32 ? 0.0 : k32 * (ptr32 + c)[0]) +
                                                (b42 ? 0.0 : k42 * (ptr42 + c)[0]) +
                                                (b13 ? 0.0 : k13 * (ptr13 + c)[0]) +
                                                (b23 ? 0.0 : k23 * (ptr23 + c)[0]) +
                                                (b33 ? 0.0 : k33 * (ptr33 + c)[0]) +
                                                (b43 ? 0.0 : k43 * (ptr43 + c)[0]) +
                                                (b14 ? 0.0 : k14 * (ptr14 + c)[0]) +
                                                (b24 ? 0.0 : k24 * (ptr24 + c)[0]) +
                                                (b34 ? 0.0 : k34 * (ptr34 + c)[0]) +
                                                (b44 ? 0.0 : k44 * (ptr44 + c)[0]);

                                            v = Math.Min(255.0, Math.Max(0.0, v));
                                            intV = (int) v;
                                            diffV = v - intV;

                                            resultPtr[0] =
                                                (byte) (uint) (diffV > 0.5 || diffV == 0.5 && (intV & 1) != 0
                                                    ? intV + 1
                                                    : intV);

                                            resultPtr++;
                                        }
                                    }

                                    resultPtr += resultOffset;
                                }
                            }
                        }
                    }
                }
            }
        }

        return result;
    }
    catch
    {
        return result;
    }
    finally
    {
        if (imageData != null)
        {
            image.UnlockBits(imageData);
        }

        if (result != null && resultData != null)
        {
            result.UnlockBits(resultData);
        }
    }
}
