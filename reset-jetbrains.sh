#!/bin/bash

cd ~/
rm -rf .local/share/JetBrains
rm -rf .config/JetBrains
rm -rf .cache/JetBrains
rm -rf ~/.java/.userPrefs/prefs.xml
rm -rf ~/.java/.userPrefs/jetbrains


