import cv2
import numpy as np
import math

import re

from PIL import Image
from tesserocr import PyTessBaseAPI, PSM, OEM


EXTRACTION_MAX_SIDE_LENGTH = 750
MAX_MICR_LINE_PART_DISTANCE_FROM_CENTER_LINE = 5
MAX_MICR_LINE_DETECTION_ANGLE = 25
MICR_LINE_MIN_WIDTH_TO_IMAGE_WIDTH_RATIO = 0.45
MICR_LINE_MAX_WIDTH_TO_HEIGHT_RATIO = 0.06
MICR_CHARACTERS = 'ABCD0123456789'


class TesseractOcrEngine:
    __configured_models = dict()

    def __init__(self):
        pass

    @staticmethod
    def read(image,
             lang='eng',
             psm=PSM.RAW_LINE,
             oem=OEM.LSTM_ONLY,
             white_list=''):
        if image is None or lang is None or psm is None or oem is None:
            return None

        model_key = (lang, psm, oem)
        if model_key in TesseractOcrEngine.__configured_models:
            model = TesseractOcrEngine.__configured_models[model_key]
        else:
            model = PyTessBaseAPI(lang=lang, psm=psm, oem=oem)
            TesseractOcrEngine.__configured_models[model_key] = model

        model.SetVariable('tessedit_char_whitelist', white_list)
        model.SetImage(Image.fromarray(image))
        return model.GetUTF8Text()


def triangle_area(point1,
                  point2,
                  point3):
    x1, y1 = point1
    x2, y2 = point2
    x3, y3 = point3
    return math.fabs(0.5 * (x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)))


def calculate_4_polygon_area(point1,
                             point2,
                             point3,
                             point4):
    return triangle_area(point1, point2, point3) + \
        triangle_area(point1, point4, point3)


def estimate_rectangle_width_height_from_vertices(vertices,
                                                  id_card_width_height_ratio):
    area = calculate_4_polygon_area(*vertices)
    height = math.sqrt(area / id_card_width_height_ratio)
    width = id_card_width_height_ratio * height
    return int(math.floor(width)), int(math.floor(height))


def warp_perspective(image,
                     original_points,
                     target_points,
                     target_width,
                     target_height):
    original_points = np.float32(original_points)
    target_points = np.float32(target_points)
    perspective_transform_matrix = cv2.getPerspectiveTransform(original_points, target_points)
    return cv2.warpPerspective(image,
                               perspective_transform_matrix,
                               (target_width, target_height),
                               flags=cv2.INTER_CUBIC)


def correct_rectangle_perspective(image,
                                  warped_vertices,
                                  rectangle_width_height_ratio=None):
    if rectangle_width_height_ratio is not None:
        target_width, target_height = estimate_rectangle_width_height_from_vertices(warped_vertices,
                                                                                    rectangle_width_height_ratio)
    else:
        point1, point2, point3, point4 = sort_points_clockwise(warped_vertices)
        side1_length = euclidean_distance(point1, point2)
        corresponding_side1_length = euclidean_distance(point4, point3)
        side2_length = euclidean_distance(point2, point3)
        corresponding_side2_length = euclidean_distance(point1, point4)
        target_width = max(side1_length, corresponding_side1_length)
        target_height = max(side2_length, corresponding_side2_length)
        if target_width < target_height:
            target_width, target_height = target_height, target_width
        target_width = int(round(target_width))
        target_height = int(round(target_height))

    target_rectangle_vertices = [(0, 0),
                                 (target_width - 1, 0),
                                 (target_width - 1, target_height - 1),
                                 (0, target_height - 1)]
    return warp_perspective(image,
                            warped_vertices,
                            target_rectangle_vertices,
                            target_width,
                            target_height)


def auto_correct_vertices_order(vertices):
    point1, point2, point3, point4 = vertices
    side1_length = euclidean_distance(point1, point2)
    corresponding_side1_length = euclidean_distance(point4, point3)
    side2_length = euclidean_distance(point2, point3)
    corresponding_side2_length = euclidean_distance(point1, point4)
    if max(side1_length, corresponding_side1_length) < min(side2_length, corresponding_side2_length):
        point1, point2, point3, point4 = point2, point3, point4, point1
    point1, point2, point3, point4 = point3, point4, point1, point2
    return np.array([point1,
                     point2,
                     point3,
                     point4])


def adjust_angle_by_quadrant(angle,
                             point):
    x, y = point
    if x < 0 and y > 0:
        return 180 - angle
    if x < 0 and y < 0:
        return 180 + angle
    if x > 0 and y < 0:
        return 360 - angle
    return angle


def compute_angle(origin,
                  point):
    x1 = point[0] - origin[0]
    y1 = origin[1] - point[1]
    if math.fabs(x1) > 1e-6:
        theta = math.fabs(math.atan(y1 / x1)) / math.pi * 180
    else:
        theta = 90
    return adjust_angle_by_quadrant(theta, (x1, y1))


def euclidean_distance(x, y):
    return np.sqrt(np.sum(np.square(np.array(x, np.float32) - np.array(y, np.float32))))


def compute_center_of_gravity(points):
    points_array = np.array(points, np.float32)
    return np.array([np.mean(points_array[:, :1]), np.mean(points_array[:, 1:])])


def sort_points_clockwise(points):
    origin = compute_center_of_gravity(points)
    point_angle_pairs = [[points[i], compute_angle(origin, points[i])]
                         for i in range(len(points))]
    point_angle_pairs.sort(key=lambda x: x[1], reverse=True)
    return np.array([p[0] for p in point_angle_pairs])


def filter_connected_components(binary_image,
                                connected_components_stats_filter,
                                stats_with_background_label=False):
    labels_count, labeled_image, stats, centroids = cv2.connectedComponentsWithStats(binary_image, 8, cv2.CV_32S)
    chosen_labels = connected_components_stats_filter(stats)
    chosen_labels = np.where(chosen_labels)[0]
    if not stats_with_background_label:
        chosen_labels += 1
    result_image = np.zeros(binary_image.shape, np.uint8)
    for i in range(chosen_labels.shape[0]):
        label = chosen_labels[i]
        x, y, w, h = stats[label, :4]
        mask = labeled_image[y:y + h, x:x + w] == label
        result_image[y:y + h, x: x + w][mask] = 255
    return result_image


def otsu_threshold(image_gray):
    return cv2.threshold(image_gray,
                         0,
                         255,
                         cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]


def smear_micr_horizontally(gray_image):
    black_hat_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (7, 7))
    black_hat_morphology = cv2.morphologyEx(gray_image, cv2.MORPH_BLACKHAT, black_hat_kernel)

    gradients = cv2.Sobel(black_hat_morphology, cv2.CV_64F, 1, 0, ksize=7)
    gradients = np.abs(gradients)

    min_gradient = np.min(gradients)
    max_gradient = np.max(gradients)
    gradients = (gradients - min_gradient) / (max_gradient - min_gradient) * 255
    gradients[gradients > 255] = 255

    binary_image = otsu_threshold(gradients.astype(np.uint8))
    binary_image = cv2.morphologyEx(binary_image, cv2.MORPH_CLOSE, np.ones((5, 5), np.uint8))
    filter_connected_components(binary_image, lambda stats: stats[1:, -1] > 20)
    binary_image = cv2.morphologyEx(binary_image, cv2.MORPH_CLOSE, np.ones((1, 15), np.uint8))

    binary_image = cv2.erode(binary_image, np.ones((1, 15), np.uint8))
    filter_connected_components(binary_image, lambda stats: stats[1:, -1] > 200)
    binary_image = cv2.dilate(binary_image, np.ones((1, 15), np.uint8))

    return cv2.dilate(binary_image, np.ones((5, 5), np.uint8))


def get_rotated_rect_angle(rotated_area_rect):
    box = cv2.boxPoints(rotated_area_rect)
    box = sort_points_clockwise(box)
    distances = [(euclidean_distance(box[i], box[(i + 1) % 4]), i) for i in range(4)]
    distances.sort(key=lambda x: x[0], reverse=True)
    best_i = distances[0][1]
    point1 = (box[best_i - 1] + box[best_i]) / 2
    point2 = (box[best_i + 1] + box[(best_i + 2) % 4]) / 2
    if point1[0] > point2[0]:
        point1, point2 = point2, point1
    result = compute_angle(point1, point2)
    return result, (point1, point2)


def extract_micr_field(image):
    max_side_length = np.max(image.shape)
    resize_factor = EXTRACTION_MAX_SIDE_LENGTH / max_side_length
    resized_image = cv2.resize(image,
                               None,
                               fx=resize_factor,
                               fy=resize_factor,
                               interpolation=cv2.INTER_CUBIC)
    offset = int(round(resized_image.shape[0] * 3 / 4))
    resized_image = resized_image[offset:, :]
    image_luv = cv2.cvtColor(resized_image, cv2.COLOR_BGR2LUV)
    image_l = image_luv[:, :, 0]
    image_gray = cv2.GaussianBlur(image_l, (3, 3), 0)
    smeared = smear_micr_horizontally(image_gray)

    contours, hierarchy = cv2.findContours(smeared, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    max_area = -1
    largest_contour = None
    for contour in contours:
        contour_area = cv2.moments(contour)['m00']
        if contour_area > max_area:
            min_area_rect = cv2.minAreaRect(contour)
            angle, (point1, point2) = get_rotated_rect_angle(min_area_rect)
            if angle < MAX_MICR_LINE_DETECTION_ANGLE or 180 - angle < MAX_MICR_LINE_DETECTION_ANGLE:
                max_area = contour_area
                largest_contour = contour, min_area_rect, angle, (point1, point2)

    if largest_contour is None:
        return None

    best_contour, best_min_area_rect, best_angle, (best_point1, best_point2) = largest_contour

    best_x1, best_y1 = best_point1
    best_x2, best_y2 = best_point2

    best_left = min(best_x1, best_x2)
    best_right = max(best_x1, best_x2)

    selected_contours = []
    contour_centers = []
    for contour in contours:
        min_area_rect = cv2.minAreaRect(contour)
        center_x, center_y = min_area_rect[0]
        angle, (point1, point2) = get_rotated_rect_angle(min_area_rect)

        x1, y1 = point1
        x2, y2 = point2
        left = min(x1, x2)
        right = max(x1, x2)

        if (angle < MAX_MICR_LINE_DETECTION_ANGLE or 180 - angle < MAX_MICR_LINE_DETECTION_ANGLE) and \
                (right < best_left or left > best_right):
            selected_contours.append((contour, min_area_rect))
            contour_centers.append((center_x, center_y))

    if len(selected_contours) > 0:
        contour_centers = np.array(contour_centers, np.float32)
        denominator = euclidean_distance(best_point1, best_point2)
        x1, y1 = best_point1
        x2, y2 = best_point2
        x0 = contour_centers[:, 0]
        y0 = contour_centers[:, 1]
        distances = np.abs((x2 - x1) * (y1 - y0) - (x1 - x0) * (y2 - y1)) / denominator
        closest_contour_indices = np.where(distances < MAX_MICR_LINE_PART_DISTANCE_FROM_CENTER_LINE)[0].ravel()
        micr_contours = []
        for i in closest_contour_indices:
            micr_contours.append(selected_contours[i][0])
        micr_contours.append(best_contour)
        micr_contours = np.concatenate(micr_contours, axis=0)
    else:
        micr_contours = best_contour

    total_min_area_react = cv2.minAreaRect(micr_contours)
    (center_x, center_y), (width, height), angle = total_min_area_react
    width += 5
    height += 5
    total_min_area_react = ((center_x, center_y), (width, height), angle)
    box_points = np.array(cv2.boxPoints(total_min_area_react), int)
    adjusted_points = sort_points_clockwise(box_points)
    adjusted_points = auto_correct_vertices_order(adjusted_points).astype(np.float32)
    adjusted_points[:, 1] += offset
    adjusted_points /= resize_factor
    corrected_field_image = correct_rectangle_perspective(image,
                                                          adjusted_points,
                                                          None)
    return cv2.cvtColor(corrected_field_image, cv2.COLOR_BGR2RGB)


def calculate_micr_score(micr_line_image):
    black_hat_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (21, 21))
    image_luv = cv2.cvtColor(micr_line_image, cv2.COLOR_BGR2LUV)
    image_l = image_luv[:, :, 0]
    black_hat_morphology = cv2.morphologyEx(image_l, cv2.MORPH_BLACKHAT, black_hat_kernel)
    binary_image = otsu_threshold(black_hat_morphology)
    binary_image = cv2.dilate(binary_image, np.ones((5, 5), np.uint8))
    labels_count, labeled_image, stats, centroids = cv2.connectedComponentsWithStats(binary_image, 8,
                                                                                     cv2.CV_32S)
    for i in range(1, len(stats)):
        x, y, w, h, _ = stats[i]
        if h > binary_image.shape[0] // 2:
            binary_image[0:binary_image.shape[0], x:x + w] = 255
    return np.count_nonzero(binary_image) / (binary_image.shape[0] * binary_image.shape[1])


def pre_process_text_field(extracted_field_image):
    resized = cv2.resize(extracted_field_image, None, fx=0.5, fy=0.5, interpolation=cv2.INTER_CUBIC)
    image_luv = cv2.cvtColor(resized, cv2.COLOR_BGR2LUV)
    image_l = image_luv[:, :, 0]
    black_hat_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (21, 21))
    black_hat_morphology = cv2.morphologyEx(image_l, cv2.MORPH_BLACKHAT, black_hat_kernel)
    binary_image = 255 - otsu_threshold(black_hat_morphology)
    return cv2.GaussianBlur(binary_image, (3, 3), 0)


def extract_and_read_micr(cheque_image):
    extracted_field_image1 = extract_micr_field(cheque_image)

    if extracted_field_image1 is not None:
        width_height_ratio1 = extracted_field_image1.shape[0] / extracted_field_image1.shape[1]
        width_ratio1 = extracted_field_image1.shape[1] / cheque_image.shape[1]
        if width_height_ratio1 <= MICR_LINE_MAX_WIDTH_TO_HEIGHT_RATIO and \
                width_ratio1 >= MICR_LINE_MIN_WIDTH_TO_IMAGE_WIDTH_RATIO:
            enhanced_field_image1 = pre_process_text_field(extracted_field_image1)
            ocr_text1, ocr_confidence1 = TesseractOcrEngine.read_with_confidence(enhanced_field_image1,
                                                                                 lang='e13b')
            score1 = calculate_micr_score(extracted_field_image1)
            score1 *= ocr_confidence1 / 100
        else:
            ocr_text1 = None
            ocr_confidence1 = None
            score1 = None
            extracted_field_image1 = None
            enhanced_field_image1 = None
    else:
        ocr_text1 = None
        ocr_confidence1 = None
        score1 = None
        extracted_field_image1 = None
        enhanced_field_image1 = None

    rotated_image = cv2.rotate(cheque_image, cv2.ROTATE_180)
    extracted_field_image2 = extract_micr_field(rotated_image)
    if extracted_field_image2 is not None:
        width_height_ratio2 = extracted_field_image2.shape[0] / extracted_field_image2.shape[1]
        width_ratio2 = extracted_field_image2.shape[1] / cheque_image.shape[1]
        if width_height_ratio2 <= MICR_LINE_MAX_WIDTH_TO_HEIGHT_RATIO and \
                width_ratio2 >= MICR_LINE_MIN_WIDTH_TO_IMAGE_WIDTH_RATIO:
            enhanced_field_image2 = pre_process_text_field(extracted_field_image2)
            ocr_text2, ocr_confidence2 = TesseractOcrEngine.read_with_confidence(enhanced_field_image2,
                                                                                 lang='e13b')
            score2 = calculate_micr_score(extracted_field_image2)
            score2 *= ocr_confidence2 / 100
        else:
            ocr_text2 = None
            ocr_confidence2 = None
            score2 = None
            extracted_field_image2 = None
            enhanced_field_image2 = None
    else:
        ocr_text2 = None
        ocr_confidence2 = None
        score2 = None
        extracted_field_image2 = None
        enhanced_field_image2 = None

    if score1 is None and score2 is None:
        return None
    else:
        if score1 is None:
            ocr_text = ocr_text2
            ocr_confidence = ocr_confidence2
            extracted_field_image = extracted_field_image2
            enhanced_field_image = enhanced_field_image2
            corrected_image = rotated_image
        elif score2 is None:
            ocr_text = ocr_text1
            ocr_confidence = ocr_confidence1
            extracted_field_image = extracted_field_image1
            enhanced_field_image = enhanced_field_image1
            corrected_image = cheque_image
        elif score1 < score2:
            ocr_text = ocr_text2
            ocr_confidence = ocr_confidence2
            corrected_image = rotated_image
            extracted_field_image = extracted_field_image2
            enhanced_field_image = enhanced_field_image2
        else:
            ocr_text = ocr_text1
            ocr_confidence = ocr_confidence1
            extracted_field_image = extracted_field_image1
            enhanced_field_image = enhanced_field_image1
            corrected_image = cheque_image

        ocr_text = ocr_text.strip().upper()
        ocr_text = ''.join([c for c in ocr_text if c in MICR_CHARACTERS])

    return {
        'ocr_text': ocr_text,
        'ocr_confidence': ocr_confidence,
        'extracted_field_image': extracted_field_image,
        'enhanced_field_image': enhanced_field_image,
        'corrected_image': corrected_image
    }
