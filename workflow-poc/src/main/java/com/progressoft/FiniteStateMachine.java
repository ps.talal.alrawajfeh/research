package com.progressoft;

import java.util.Collection;

public interface FiniteStateMachine {
    State getInitialState();

    State getCurrentState();

    Result<Result.Void> transition(Action action);

    Collection<State> getTerminalStates();
}
