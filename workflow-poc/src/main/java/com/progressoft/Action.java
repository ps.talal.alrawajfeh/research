package com.progressoft;

public interface Action {
    State getDestinationState();
}
