package com.progressoft;

import java.util.Collection;

public interface State {
    Collection<Action> getAvailableActions();
}
