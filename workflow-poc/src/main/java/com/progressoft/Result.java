package com.progressoft;

public class Result<T> {
    private T t;
    private String errorMessage;
    private boolean isSuccess;

    private Result(T t, boolean isSuccess, String errorMessage) {
        this.t = t;
        this.isSuccess = isSuccess;
        this.errorMessage = errorMessage;
    }

    public static <T> Result<T> ofSuccess(T t) {
        return new Result<>(t, true, "");
    }

    public static <T> Result<T> ofFailure(String errorMessage) {
        return new Result<>(null, false, errorMessage);
    }

    public T getT() {
        return t;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public static class Void {
        public static final Void VOID = new Void();

        private Void() {
        }
    }
}
