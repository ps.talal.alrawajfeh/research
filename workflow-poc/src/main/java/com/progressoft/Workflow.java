package com.progressoft;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public class Workflow implements FiniteStateMachine {
    private State initialState;
    private List<State> terminalStates;
    private State currentState;

    public Workflow(State initialState, List<State> terminalStates) {
        this.initialState = initialState;
        this.terminalStates = terminalStates;
        this.currentState = initialState;
    }

    @Override
    public State getInitialState() {
        return initialState;
    }

    @Override
    public State getCurrentState() {
        return currentState;
    }

    @Override
    public Result<Result.Void> transition(Action action) {
        Optional<Action> availableAction = getCurrentState()
                .getAvailableActions()
                .stream()
                .filter(a -> a.equals(action))
                .findFirst();

        if (availableAction.isPresent()) {
            this.currentState = action.getDestinationState();
            return Result.ofSuccess(Result.Void.VOID);
        }

        return Result.ofFailure("invalid action");
    }

    @Override
    public Collection<State> getTerminalStates() {
        return terminalStates;
    }
}
