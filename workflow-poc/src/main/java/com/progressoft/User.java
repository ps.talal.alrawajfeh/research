package com.progressoft;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public class User {
    public static final Action CREATE = UserPendingApprovalState::new;
    public static final Action APPROVE = UserApprovedState::new;
    public static final Action REJECT = UserRejectedState::new;
    public static final State INITIAL_STATE = new UserCreationInitialState();
    public static final State APPROVED_STATE = new UserApprovedState();
    public static final State REJECTED_STATE = new UserRejectedState();

    private Workflow userWorkflow;

    public User() {
        this.userWorkflow = new Workflow(INITIAL_STATE,
                Arrays.asList(APPROVED_STATE, REJECTED_STATE));
    }

    public static class UserCreationInitialState implements State {
        @Override
        public Collection<Action> getAvailableActions() {
            return Collections.singleton(CREATE);
        }
    }

    public static class UserPendingApprovalState implements State {
        @Override
        public Collection<Action> getAvailableActions() {
            return Arrays.asList(APPROVE, REJECT);
        }

    }

    private static class UserApprovedState implements State {
        @Override
        public Collection<Action> getAvailableActions() {
            return Collections.emptySet();
        }
    }

    private static class UserRejectedState implements State {
        @Override
        public Collection<Action> getAvailableActions() {
            return Collections.emptySet();
        }
    }
}
