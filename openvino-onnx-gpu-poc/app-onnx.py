import random
import time

import numpy as np
import onnxruntime as ort


def main():
    densenet_session = ort.InferenceSession('./models/densenet.onnx', providers=['CUDAExecutionProvider'])
    unet_session = ort.InferenceSession('./models/unet.onnx', providers=['CUDAExecutionProvider'])
    triplet_session = ort.InferenceSession('./models/triplet.onnx', providers=['CUDAExecutionProvider'])
    siamese_session = ort.InferenceSession('./models/siamese.onnx', providers=['CUDAExecutionProvider'])

    print(f'using device: {ort.get_device()}')

    sum_densenet = 0
    sum_unet = 0
    sum_siamese = 0
    sum_triplet = 0
    n = 0
    while True:
        n += 1
        print('inferring unet...')
        start_time = time.time()
        for _ in range(100):
            input_tensor = np.random.normal(size=(1, 224, 416, 1)).astype(np.float32)
            outputs = unet_session.run(None, {'input_1': input_tensor})
        duration = time.time() - start_time
        sum_unet += duration / 100
        print(sum_unet / n)
        print('inferring densenet...')
        start_time = time.time()
        for _ in range(100):
            input_tensor = np.random.normal(size=(1, 224, 224, 3)).astype(np.float32)
            outputs = densenet_session.run(None, {'input_1:0': input_tensor})
        duration = time.time() - start_time
        sum_densenet += duration / 100
        print(sum_densenet / n)
        print('inferring triplet...')
        start_time = time.time()
        for _ in range(1000):
            input_tensor = np.random.normal(size=(1, 1920)).astype(np.float32)
            outputs = triplet_session.run(None, {'input_1:0': input_tensor})
        duration = time.time() - start_time
        sum_triplet += duration / 1000
        print(sum_triplet / n)
        print('inferring siamese...')
        start_time = time.time()
        for _ in range(1000):
            input_tensor1 = np.random.normal(size=(1, 1920)).astype(np.float32)
            input_tensor2 = np.random.normal(size=(1, 1920)).astype(np.float32)
            outputs = siamese_session.run(None, {'input_1:0': input_tensor1, 'input_2:0': input_tensor2})
        duration = time.time() - start_time
        sum_siamese += duration / 1000
        print(sum_siamese / n)
        time.sleep(random.uniform(0.1, 1.0))
        print('========================================')


if __name__ == '__main__':
    main()
