import random
import time

import numpy as np
import tensorflow as tf


def main():
    densenet_model = tf.saved_model.load('./models-tf/densenet')
    unet_model = tf.saved_model.load('./models-tf/unet')
    triplet_model = tf.saved_model.load('./models-tf/triplet')
    siamese_model = tf.saved_model.load('./models-tf/siamese')

    sum_densenet = 0
    sum_unet = 0
    sum_siamese = 0
    sum_triplet = 0
    n = 0
    while True:
        n += 1
        print('inferring unet...')
        start_time = time.time()
        for _ in range(100):
            input_tensor = np.random.normal(size=(1, 224, 416, 1)).astype(np.float32)
            outputs = unet_model(input_tensor)
        duration = time.time() - start_time
        sum_unet += duration / 100
        print(sum_unet / n)
        print('inferring densenet...')
        start_time = time.time()
        for _ in range(100):
            input_tensor = np.random.normal(size=(1, 224, 224, 3)).astype(np.float32)
            outputs = densenet_model(input_tensor)
        duration = time.time() - start_time
        sum_densenet += duration / 100
        print(sum_densenet / n)
        print('inferring triplet...')
        start_time = time.time()
        for _ in range(1000):
            input_tensor = np.random.normal(size=(1, 1920)).astype(np.float32)
            outputs = triplet_model(input_tensor)
        duration = time.time() - start_time
        sum_triplet += duration / 1000
        print(sum_triplet / n)
        print('inferring siamese...')
        start_time = time.time()
        for _ in range(1000):
            input_tensor1 = np.random.normal(size=(1, 1920)).astype(np.float32)
            input_tensor2 = np.random.normal(size=(1, 1920)).astype(np.float32)
            outputs = siamese_model([input_tensor1, input_tensor2])
        duration = time.time() - start_time
        sum_siamese += duration / 1000
        print(sum_siamese / n)
        print('========================================')
        time.sleep(random.uniform(0.1, 1.0))


if __name__ == '__main__':
    main()
