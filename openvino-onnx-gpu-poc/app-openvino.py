import random
import time

import numpy as np
from openvino.runtime import Core

DEFAULT_DEVICE = 'CPU'


def main():
    ie = Core()

    densenet_model = ie.read_model(model='./models-ov/densenet.xml')
    densenet_model = ie.compile_model(model=densenet_model, device_name=DEFAULT_DEVICE)
    densenet_output_layer = densenet_model.output(0)

    unet_model = ie.read_model(model='./models-ov/unet.xml')
    unet_model = ie.compile_model(model=unet_model, device_name=DEFAULT_DEVICE)
    unet_output_layer = unet_model.output(0)

    siamese_model = ie.read_model(model='./models-ov/siamese.xml')
    siamese_model = ie.compile_model(model=siamese_model, device_name=DEFAULT_DEVICE)
    siamese_output_layer = siamese_model.output(0)

    triplet_model = ie.read_model(model='./models-ov/triplet.xml')
    triplet_model = ie.compile_model(model=triplet_model, device_name=DEFAULT_DEVICE)
    triplet_output_layer = triplet_model.output(0)

    sum_densenet = 0
    sum_unet = 0
    sum_siamese = 0
    sum_triplet = 0
    n = 0
    while True:
        n += 1
        print('inferring unet...')
        start_time = time.time()
        for _ in range(100):
            input_tensor = np.random.normal(size=(1, 224, 416, 1)).astype(np.float32)
            outputs = unet_model(input_tensor)[unet_output_layer]
        duration = time.time() - start_time
        sum_densenet += duration / 100
        print(sum_densenet / n)
        print('inferring densenet...')
        start_time = time.time()
        for _ in range(100):
            input_tensor = np.random.normal(size=(1, 3, 224, 224)).astype(np.float32)
            outputs = densenet_model(input_tensor)[densenet_output_layer]
        duration = time.time() - start_time
        sum_unet += duration / 100
        print(sum_unet / n)
        print('inferring triplet...')
        start_time = time.time()
        for _ in range(1000):
            input_tensor = np.random.normal(size=(1, 1920)).astype(np.float32)
            outputs = triplet_model(input_tensor)[triplet_output_layer]
        duration = time.time() - start_time
        sum_triplet += duration / 1000
        print(sum_triplet / n)
        print('inferring siamese...')
        start_time = time.time()
        for _ in range(1000):
            input_tensor1 = np.random.normal(size=(1, 1920)).astype(np.float32)
            input_tensor2 = np.random.normal(size=(1, 1920)).astype(np.float32)
            outputs = siamese_model([input_tensor1, input_tensor2])[siamese_output_layer]
        duration = time.time() - start_time
        sum_siamese += duration / 1000
        print(sum_siamese / n)
        print('========================================')
        time.sleep(random.uniform(0.1, 1.0))


if __name__ == '__main__':
    main()
