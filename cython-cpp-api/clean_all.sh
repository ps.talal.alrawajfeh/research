#!/bin/bash

chmod a+rwx ./model-training/clean.sh
./model-training/clean.sh

chmod a+rwx ./model-feeding/clean.sh
./model-feeding/clean.sh

chmod a+rwx ./model-cython-api/clean.sh
./model-cython-api/clean.sh
