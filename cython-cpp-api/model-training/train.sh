#!/bin/bash

pip3 install --user -r requirements.txt

chmod a+rwx train.py
./train.py

if [[ -d output ]]
then
    rm -rf output
fi

mkdir output

mv model.h5 output/model.h5
