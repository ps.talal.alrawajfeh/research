#!/bin/bash

cd model-training
pip3 install --user -r requirements.txt
cd ..

cd model-feeding
pip3 install --user -r requirements.txt
cd ..

cd model-cython-api
pip3 install --user -r requirements.txt
cd ..
