#!/bin/bash

if [[ ! -f model.h5 ]]
then
	if [[ ! -f ../model-training/output/model.h5 ]]
	then
		chmod a+rwx ../model-training/train.sh
	fi
	cp ../model-training/output/model.h5 ./model.h5
fi

pip3 install --user -r requirements.txt
