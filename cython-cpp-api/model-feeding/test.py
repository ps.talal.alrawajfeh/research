#!/usr/bin/python3

from model_feed import predict_image


if __name__ == '__main__':
    with open('three.jpg', 'rb') as three:
        print('\n===== testing model on three.jpg =====\n')
        prediction = predict_image(three.read())
        if prediction == 3:
            print('\npredicted image three.jpg as 3 correctly\n')
        else:
            print('\ndid not predict image three.jpg as 3 but as ' + str(prediction) + '!\n')
