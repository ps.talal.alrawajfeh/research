#!/bin/bash

if [[ ! -f output/model_api.h ]]
then
    chmod a+rwx ./build.sh
    ./build.sh
fi

if [[ ! -f output/test ]]
then
    chmod a+rwx ./test_build.sh
    ./test_build.sh
fi

if [[ ! -f model.h5 ]]
then
	if [[ ! -f ../model-training/output/model.h5 ]]
	then
		chmod a+rwx ../model-training/train.sh
	fi
	cp ../model-training/output/model.h5 ./output/model.h5
fi

cp ../model-feeding/three.jpg ./output/three.jpg

cd output
chmod a+rwx test
export PYTHONPATH=$(pwd)
export PATH=$PATH:$PYTHONPATH/model_api.cpython-37m-x86_64-linux-gnu.so:$PYTHONPATH/model_feed.cpython-37m-x86_64-linux-gnu.so
./test
cd ..
