#!/usr/bin/python3

from setuptools import setup, find_packages
from setuptools.extension import Extension
from Cython.Build import cythonize
from Cython.Distutils import build_ext


extensions = [
    Extension(
        "model_feed",
        ["model_feed.py"]
    ),
    Extension(
        "model_api",
        ["model_api.pyx"]
    )
]

setup(
    name="model_api",
    packages=find_packages(),
    cmdclass={'build_ext': build_ext},
    ext_modules=cythonize(extensions, language_level="3", language="c++")
)
