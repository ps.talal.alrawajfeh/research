#!/bin/bash

chmod a+rwx prepare.sh
./prepare.sh

python3 build.py build_ext

mkdir output

mv model_api.h output/model_api.h
mv build/lib.linux-x86_64-*/* output/
mv build/temp.linux-x86_64-*/* output/

rm -rf build
rm model_api.c
rm model_feed.c