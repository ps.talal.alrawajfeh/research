#!/usr/bin/python3

from model_feed import predict_image


cdef public void say_hello():
    print("HELLOOOOO\n")


cdef public int predict_from_bytes(bytes image_buffer):
    return predict_image(image_buffer)
