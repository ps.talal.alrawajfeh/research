#!/bin/bash

if [[ ! -f model_feed.py ]]
then
    cp ../model-feeding/model_feed.py ./model_feed.py
fi

pip3 install --user -r requirements.txt
