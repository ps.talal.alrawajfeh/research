#!/usr/bin/python3

import cv2
import numpy as np
from keras import backend as K
from keras.models import load_model

IMG_ROWS, IMG_COLS = 28, 28


def pre_process_batch(batch):
    model_input = np.array(batch)

    if K.image_data_format() == 'channels_first':
        model_input = model_input.reshape(model_input.shape[0], 1, IMG_ROWS, IMG_COLS)
    else:
        model_input = model_input.reshape(model_input.shape[0], IMG_ROWS, IMG_COLS, 1)

    model_input = model_input.astype('float32')
    model_input /= 255

    return model_input


def pre_process(image):
    return pre_process_batch([image])


def read_image_from_bytes(image_bytes):
    return cv2.imdecode(np.fromstring(image_bytes, np.uint8), 0)


def predict(image):
    model = load_model('model.h5')
    predictions = model.predict(pre_process(image))
    return int(predictions[0].argmax())


def predict_image(image_bytes):
    return predict(read_image_from_bytes(image_bytes))
