#!/bin/bash

g++ -Ioutput/ -c test.cpp -o test.o $(python3-config --cflags)
g++ -Loutput/ output/model_api.o test.o -o test $(python3-config --ldflags) -fno-lto -lavdevice
rm test.o
mv test output/
