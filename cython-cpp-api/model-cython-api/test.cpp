#include <Python.h>
#include <iostream>
#include <fstream>
#include <iterator>
#include <algorithm>
#include <vector>
#include "model_api.h"

int main(int argc, char* argv[])
{
	int status = PyImport_AppendInittab("model", PyInit_model_api);

	if (status == -1)
	{
		return -1;
	}

	Py_Initialize();

	PyObject* module = PyImport_ImportModule("model_api");

	if (module == NULL)
	{
		Py_Finalize();
		return -1;
	}

	std::ifstream input("three.jpg", std::ios::binary);
	std::vector<char> buffer(std::istreambuf_iterator<char>(input), {});
	char* image = &buffer[0];

	PyObject *image_bytes = PyBytes_FromStringAndSize(image, buffer.size());
	
	say_hello();
 	std::cout << predict_from_bytes(image_bytes) << std::endl;
	
	Py_Finalize();
	
	return 0;
}

