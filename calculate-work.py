import datetime
from datetime import timedelta


def main():
    with open(
        "progresssoft_time_and_activities_report_2023-01-01_to_2023-12-16.csv", "r"
    ) as f:
        content = f.read()

    lines = content.split("\n")
    date_time_dict = dict()
    for l in lines[1:]:
        if l.strip() == "":
            continue
        columns = l.split(",")
        work_time = columns[8].replace('"', "").strip().split(":")
        date_time_dict[columns[2].replace('"', "").strip()] = int(
            work_time[0]
        ) * 60 + int(work_time[1])

    with open("official-holidays.txt", "r") as f:
        content = f.read()
    lines = content.split("\n")
    holidays = []
    for l in lines:
        if l.strip() == "":
            continue
        parts = l.strip().split("/")
        holidays.append(f"{parts[2]}-{parts[1]}-{parts[0]}")

    with open("lectures.txt", "r") as f:
        content = f.read()
        lines = content.split("\n")
    additional_time_dict = dict()
    for l in lines:
        if l.strip() == "":
            continue
        parts = l.strip().split("/")
        additional_time_dict[f"{parts[2]}-{parts[1]}-{parts[0]}"] = 5 * 60

    print(holidays)
    current_day = datetime.date(2023, 1, 1)
    number_of_vacations = 0
    total_worked_time = 0
    total_expected_worked_time = 0
    while True:
        if current_day.weekday() == 4 or current_day.weekday() == 5:
            current_day += timedelta(days=1)
            continue
        current_day_key = str(current_day)
        if (
            current_day_key not in date_time_dict
            and current_day_key not in holidays
            and current_day_key not in additional_time_dict
        ):
            print(current_day_key)
            number_of_vacations += 1
        
        if current_day_key in date_time_dict:
            total_worked_time += date_time_dict[current_day_key]
        if current_day_key in additional_time_dict:
            total_worked_time += additional_time_dict[current_day_key]
        if current_day_key in holidays:
            total_worked_time += 8 * 60

        total_expected_worked_time += 7 * 60

        current_day += timedelta(days=1)
        if current_day.year == 2024:
            break

    print(number_of_vacations)
    print((total_worked_time + number_of_vacations * 8 * 60 - total_expected_worked_time) / (60 * 8))


if __name__ == "__main__":
    main()
