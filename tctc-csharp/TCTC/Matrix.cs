using System.Text;

namespace TCTC;

public class Matrix
{
    public int Rows { get; }
    public int Columns { get; }

    public double[] Data { get; }

    private Matrix()
    {
        /* left empty */
    }

    private Matrix(int rows, int columns, IReadOnlyList<double> data)
    {
        Rows = rows;
        Columns = columns;
        Data = new double[data.Count];
        for (int i = 0; i < data.Count; i++)
        {
            Data[i] = data[i];
        }
    }

    private void _validateRowIndex(int rowIndex, string variableName)
    {
        if (rowIndex < 0 || rowIndex >= Rows)
        {
            throw new ArgumentException($"{variableName} is invalid.");
        }
    }

    private void _validateRowsCount()
    {
        if (Rows == 0)
        {
            throw new ArgumentException("A matrix should have at least one row.");
        }
    }

    private void _validateColumnsCount()
    {
        if (Columns == 0)
        {
            throw new ArgumentException("A matrix should have at least one column.");
        }
    }

    public Matrix(IReadOnlyList<double[]> matrix)
    {
        Rows = matrix.Count;
        _validateRowsCount();

        Columns = matrix[0].Length;
        _validateColumnsCount();

        var columns = Columns;
        var rows = Rows;
        
        Data = new double[rows * columns];
        var data = Data;
        
        int dataIndex = 0;
        for (int i = 1; i < rows; i++)
        {
            if (matrix[i].Length != columns)
            {
                throw new ArgumentException("A matrix should have the same number of columns for each row.");
            }

            for (int j = 0; j < columns; j++)
            {
                data[dataIndex] = matrix[i][j];
                dataIndex++;
            }
        }
    }

    public Matrix(double[,] matrix)
    {
        if (matrix.Rank != 2)
        {
            throw new ArgumentException("A matrix should be a two-dimensional.");
        }

        Rows = matrix.GetLength(0);
        _validateRowsCount();

        Columns = matrix.GetLength(1);
        _validateColumnsCount();

        var columns = Columns;
        var rows = Rows;
        
        Data = new double[rows * columns];
        var data = Data;
        
        int dataIndex = 0;
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < columns; j++)
            {
                data[dataIndex] = matrix[i, j];
                dataIndex++;
            }
        }
    }

    public Matrix(List<double> vector)
    {
        Rows = vector.Count;
        Columns = 1;
        Data = vector.ToArray();
    }

    public Matrix(int rows, int columns)
    {
        Rows = rows;
        Columns = columns;

        _validateRowsCount();
        _validateColumnsCount();

        Data = new double[Rows * Columns];
        var data = Data;
        
        for (int i = 0; i < data.Length; i++)
        {
            data[i] = 0.0;
        }
    }

    public Matrix(int rows, int columns, Random randomNumberGenerator, double min, double max)
    {
        Rows = rows;
        Columns = columns;

        _validateRowsCount();
        _validateColumnsCount();

        if (min >= max)
        {
            throw new ArgumentException("min should be strictly less than max");
        }

        Data = new double[Rows * Columns];
        var data = Data;
        
        for (int i = 0; i < data.Length; i++)
        {
            data[i] = min + (max - min) * randomNumberGenerator.NextDouble();
        }
    }

    public Matrix(int rows, int columns, double initialValue)
    {
        Rows = rows;
        Columns = columns;

        _validateRowsCount();
        _validateColumnsCount();

        Data = new double[Rows * Columns];
        var data = Data;
        for (int i = 0; i < data.Length; i++)
        {
            data[i] = initialValue;
        }
    }

    public void SwapRows(int rowIndex1, int rowIndex2)
    {
        if (rowIndex1 == rowIndex2)
        {
            return;
        }

        _validateRowIndex(rowIndex1, "rowIndex1");
        _validateRowIndex(rowIndex2, "rowIndex2");

        var columns = Columns;
        int dataIndex1 = columns * rowIndex1;
        int dataIndex2 = columns * rowIndex2;
        var data = Data;
        
        for (int j = 0; j < columns; j++)
        {
            (data[dataIndex1], data[dataIndex2]) = (data[dataIndex2], data[dataIndex1]);
            dataIndex1++;
            dataIndex2++;
        }
    }

    public void MultiplyRowByValue(int rowIndex, double value)
    {
        _validateRowIndex(rowIndex, "rowIndex");

        var columns = Columns;
        int dataIndex = columns * rowIndex;
        var data = Data;
        
        for (int j = 0; j < columns; j++)
        {
            data[dataIndex] *= value;
            dataIndex++;
        }
    }

    public void AddMultipleOfRowToTargetRow(
        int targetRowIndex,
        int rowIndex,
        double rowFactor)
    {
        _validateRowIndex(targetRowIndex, "resultantRowIndex");
        _validateRowIndex(rowIndex, "otherRowIndex");

        var columns = Columns;
        int dataIndex1 = columns * targetRowIndex;
        int dataIndex2 = columns * rowIndex;
        var data = Data;
        
        for (int j = 0; j < columns; j++)
        {
            data[dataIndex1] += rowFactor * data[dataIndex2];
            dataIndex1++;
            dataIndex2++;
        }
    }

    public Matrix Copy()
    {
        return new Matrix(Rows, Columns, Data);
    }

    public double Determinant()
    {
        var rows = Rows;
        var columns = Columns;
        
        if (rows != columns)
        {
            throw new ArgumentException("The determinant can be only calculated for a square matrix.");
        }

        Matrix gaussianEliminationMatrix = Copy();
        var data = gaussianEliminationMatrix.Data;

        double determinant = 1.0;

        int currentRowDataIndex = 0;
        for (int i = 0; i < rows; i++)
        {
            if (data[currentRowDataIndex + i] == 0)
            {
                int rowSwapDataIndex = currentRowDataIndex + rows;
                bool isFound = false;
                int j;
                for (j = i + 1; j < rows; j++)
                {
                    if (data[rowSwapDataIndex + i] != 0)
                    {
                        isFound = true;
                        break;
                    }

                    rowSwapDataIndex += columns;
                }

                if (isFound)
                {
                    gaussianEliminationMatrix.SwapRows(i, j);
                    determinant *= -1;
                }
                else
                {
                    return 0.0;
                }
            }

            if (i < rows - 1)
            {
                double value = data[currentRowDataIndex + i];
                gaussianEliminationMatrix.MultiplyRowByValue(i, 1.0 / value);
                determinant *= value;

                int rowDataIndex = currentRowDataIndex + columns;
                for (int j = i + 1; j < rows; j++)
                {
                    double factor = data[rowDataIndex + i];
                    gaussianEliminationMatrix.AddMultipleOfRowToTargetRow(j, i, -factor);
                    rowDataIndex += columns;
                }
            }
            else
            {
                determinant *= data[currentRowDataIndex + i];
            }


            currentRowDataIndex += columns;
        }

        return determinant;
    }

    public Matrix SolveLeastSquares(Matrix y)
    {
        if (y.Columns != 1)
        {
            throw new ArgumentException("The matrix y should be a column vector.");
        }

        if (y.Rows != Rows)
        {
            throw new ArgumentException("The matrix y should have the same number of rows.");
        }

        var decomposition = Svd();

        var singularMatrix = decomposition.SingularMatrix;
        var singularMatrixColumns = singularMatrix.Columns;
        var singularMatrixRows = singularMatrix.Rows;
        var singularPseudoInverseMatrix = new Matrix(singularMatrixColumns, singularMatrixRows);
        
        for (int i = 0; i < Math.Min(singularMatrixRows, singularMatrixColumns); i++)
        {
            var singularValue = singularMatrix[i, i];
            if (singularValue > 0.0)
            {
                singularPseudoInverseMatrix[i, i] = 1.0 / singularValue;
            }
        }

        return decomposition.RightEigenVectors.MatMul(
            singularPseudoInverseMatrix.MatMul(decomposition.LeftEigenVectors.Transpose().MatMul(y)));
    }

    public Matrix Solve(Matrix y)
    {
        var columns = Columns;
        var rows = Rows;
        
        if (rows != columns)
        {
            throw new ArgumentException("Solving can only be done for a square matrix.");
        }

        if (y.Columns != 1)
        {
            throw new ArgumentException("The matrix y should be a column vector.");
        }

        if (y.Rows != rows)
        {
            throw new ArgumentException("The matrix y should have the same number of rows.");
        }

        Matrix gaussianEliminationMatrix = Copy();
        Matrix solutionMatrix = y.Copy();
        var data = gaussianEliminationMatrix.Data;

        int currentRowDataIndex = 0;
        for (int i = 0; i < rows; i++)
        {
            if (data[currentRowDataIndex + i] == 0)
            {
                int rowSwapDataIndex = currentRowDataIndex + rows;
                bool isFound = false;
                int j;
                for (j = i + 1; j < rows; j++)
                {
                    if (data[rowSwapDataIndex + i] != 0)
                    {
                        isFound = true;
                        break;
                    }

                    rowSwapDataIndex += columns;
                }

                if (isFound)
                {
                    gaussianEliminationMatrix.SwapRows(i, j);
                    solutionMatrix.SwapRows(i, j);
                }
                else
                {
                    return null;
                }
            }

            double value = data[currentRowDataIndex + i];
            gaussianEliminationMatrix.MultiplyRowByValue(i, 1.0 / value);
            solutionMatrix.MultiplyRowByValue(i, 1.0 / value);

            if (i < rows - 1)
            {
                int rowDataIndex = currentRowDataIndex + columns;
                for (int j = i + 1; j < rows; j++)
                {
                    double factor = data[rowDataIndex + i];
                    gaussianEliminationMatrix.AddMultipleOfRowToTargetRow(j, i, -factor);
                    solutionMatrix.AddMultipleOfRowToTargetRow(j, i, -factor);
                    rowDataIndex += columns;
                }
            }

            currentRowDataIndex += columns;
        }

        currentRowDataIndex = columns * (rows - 1);
        for (int i = rows - 1; i >= 0; i--)
        {
            int rowDataIndex = currentRowDataIndex - columns;
            for (int j = i - 1; j >= 0; j--)
            {
                double factor = data[rowDataIndex + i];
                gaussianEliminationMatrix.AddMultipleOfRowToTargetRow(j, i, -factor);
                solutionMatrix.AddMultipleOfRowToTargetRow(j, i, -factor);
                rowDataIndex -= columns;
            }

            currentRowDataIndex -= columns;
        }

        return solutionMatrix;
    }

    private (Matrix FirstEigenVector, double FirstSingularValue) _getFirstEigenVectorAndSingularValue(
        Matrix hermitianMatrix,
        int iterations = 100,
        double epsilon = 1e-7)
    {
        Matrix powerMatrix = hermitianMatrix;
        for (int i = 0; i < iterations; i++)
        {
            powerMatrix = powerMatrix.MatMul(hermitianMatrix) / powerMatrix.Norm();
        }

        var firstEigenVector = powerMatrix.GetColumnVector(0);
        var norm = firstEigenVector.Norm();
        if (norm < epsilon)
        {
            return (new Matrix(firstEigenVector.Rows, 1), 0.0);
        }

        firstEigenVector /= norm;

        var firstValue = firstEigenVector.Data[0];
        if (Math.Abs(firstValue) < epsilon)
        {
            return (firstEigenVector, 0.0);
        }

        var firstSingularValue = powerMatrix.Data[0] / (firstValue * firstValue);
        return (firstEigenVector, Math.Sqrt(firstSingularValue));
    }

    public Matrix GetColumnVector(int index)
    {
        Matrix vector = new Matrix(Rows, 1);
        var vectorData = vector.Data;
        var data = Data;
        int dataIndex = index;
        var columns = Columns;
        
        for (int i = 0; i < Rows; i++)
        {
            vectorData[i] = data[dataIndex];
            dataIndex += columns;
        }

        return vector;
    }

    public static Matrix ConcatenateColumnVectors(List<Matrix> vectors)
    {
        var vectorsCount = vectors.Count;
        if (vectorsCount == 0)
        {
            throw new ArgumentException("Given an empty list.");
        }

        int rows = vectors[0].Rows;
        Matrix result = new Matrix(rows, vectorsCount);
        var resultData = result.Data;

        for (int i = 0; i < vectorsCount; i++)
        {
            var currentVector = vectors[i];
            var currentVectorData = currentVector.Data;

            if (currentVector.Rows != rows)
            {
                throw new ArgumentException("All column vectors should have the same number of rows.");
            }

            if (currentVector.Columns != 1)
            {
                throw new ArgumentException("All matrices should be column vectors.");
            }

            int resultDataIndex = i;
            for (int j = 0; j < rows; j++)
            {
                resultData[resultDataIndex] = currentVectorData[j];
                resultDataIndex += vectorsCount;
            }
        }

        return result;
    }

    private void _completeOrthonormalVectorsToBasis(List<Matrix> orthonormalVectors, int dimension, double epsilon)
    {
        for (int i = 0; i < dimension; i++)
        {
            var standardBasisVector = new Matrix(dimension, 1);
            var standardBasisVectorData = standardBasisVector.Data;

            standardBasisVectorData[i] = 1;

            var newBasisVector = standardBasisVector;
            for (int j = 0; j < orthonormalVectors.Count; j++)
            {
                var eigenVector = orthonormalVectors[i];
                newBasisVector -= eigenVector * standardBasisVector.Dot(eigenVector);
            }

            if (newBasisVector.Norm() < epsilon)
            {
                continue;
            }

            orthonormalVectors.Add(newBasisVector / newBasisVector.Norm());
            if (orthonormalVectors.Count == dimension)
            {
                break;
            }
        }
    }

    public (Matrix LeftEigenVectors, Matrix SingularMatrix, List<double> SingularValues, Matrix RightEigenVectors) Svd(
        int iterations = 1000,
        double epsilon = 1e-7)
    {
        var rows = Rows;
        var columns = Columns;
        
        int numberOfSingularValues = Math.Min(rows, columns);

        Matrix currentSquareMatrix = Transpose().MatMul(this);

        var (rightEigenVector, singularValue) =
            _getFirstEigenVectorAndSingularValue(currentSquareMatrix, iterations, epsilon);

        if (Math.Abs(singularValue) < epsilon)
        {
            Matrix emptyLeftEigenVectors = new Matrix(rows, rows);
            Matrix emptySingularMatrix = new Matrix(rows, columns);
            Matrix emptyRightEigenVectors = new Matrix(columns, columns);

            for (int i = 0; i < rows; i++)
            {
                emptyLeftEigenVectors[i, i] = 1.0;
            }

            for (int i = 0; i < columns; i++)
            {
                emptyRightEigenVectors[i, i] = 1.0;
            }

            return (emptyLeftEigenVectors, emptySingularMatrix, new List<double>(), emptyRightEigenVectors);
        }

        var leftEigenVector = MatMul(rightEigenVector) / singularValue;

        List<Matrix> rightEigenVectorsList = new List<Matrix> { rightEigenVector };
        List<Matrix> leftEigenVectorsList = new List<Matrix>() { leftEigenVector };
        List<double> singularValues = new List<double> { singularValue };

        currentSquareMatrix -= rightEigenVector.MatMul(rightEigenVector.Transpose()) * (singularValue * singularValue);

        for (int i = 1; i < numberOfSingularValues; i++)
        {
            (rightEigenVector, singularValue) =
                _getFirstEigenVectorAndSingularValue(currentSquareMatrix, iterations, epsilon);

            if (Math.Abs(singularValue) < epsilon)
            {
                break;
            }

            leftEigenVector = MatMul(rightEigenVector) / singularValue;

            rightEigenVectorsList.Add(rightEigenVector);
            singularValues.Add(singularValue);
            leftEigenVectorsList.Add(leftEigenVector);
            currentSquareMatrix -=
                rightEigenVector.MatMul(rightEigenVector.Transpose()) * (singularValue * singularValue);
        }

        while (singularValues.Count < numberOfSingularValues)
        {
            singularValues.Add(0.0);
        }

        if (rightEigenVectorsList.Count < columns)
        {
            _completeOrthonormalVectorsToBasis(rightEigenVectorsList, columns, epsilon);
        }

        var rightEigenVectors = ConcatenateColumnVectors(rightEigenVectorsList);

        if (leftEigenVectorsList.Count < rows)
        {
            _completeOrthonormalVectorsToBasis(leftEigenVectorsList, rows, epsilon);
        }

        var leftEigenVectors = ConcatenateColumnVectors(leftEigenVectorsList);

        var singularMatrix = new Matrix(rows, columns);

        for (int i = 0; i < numberOfSingularValues; i++)
        {
            var value = singularValues[i];
            singularMatrix[i, i] = value;
        }

        return (leftEigenVectors, singularMatrix, singularValues, rightEigenVectors);
    }


    private static void _ensureMatricesHaveSameShape(Matrix matrix1, Matrix matrix2)
    {
        if (matrix1.Rows != matrix2.Rows)
        {
            throw new ArgumentException("Both matrices must have the same number of rows.");
        }

        if (matrix1.Columns != matrix2.Columns)
        {
            throw new ArgumentException("Both matrices must have the same number of columns.");
        }
    }

    public static Matrix operator +(Matrix matrix1, Matrix matrix2)
    {
        _ensureMatricesHaveSameShape(matrix1, matrix2);
        Matrix result = matrix1.Copy();
        var resultData = result.Data;
        var matrix2Data = matrix2.Data;

        for (int i = 0; i < resultData.Length; i++)
        {
            resultData[i] += matrix2Data[i];
        }

        return result;
    }

    public static Matrix operator -(Matrix matrix1, Matrix matrix2)
    {
        _ensureMatricesHaveSameShape(matrix1, matrix2);
        Matrix result = matrix1.Copy();
        var resultData = result.Data;
        var matrix2Data = matrix2.Data;

        for (int i = 0; i < resultData.Length; i++)
        {
            resultData[i] -= matrix2Data[i];
        }

        return result;
    }

    public static Matrix operator *(Matrix matrix1, Matrix matrix2)
    {
        _ensureMatricesHaveSameShape(matrix1, matrix2);
        Matrix result = matrix1.Copy();
        var resultData = result.Data;
        var matrix2Data = matrix2.Data;

        for (int i = 0; i < resultData.Length; i++)
        {
            resultData[i] *= matrix2Data[i];
        }

        return result;
    }

    public static Matrix operator /(Matrix matrix1, Matrix matrix2)
    {
        _ensureMatricesHaveSameShape(matrix1, matrix2);
        Matrix result = matrix1.Copy();
        var resultData = result.Data;
        var matrix2Data = matrix2.Data;

        for (int i = 0; i < resultData.Length; i++)
        {
            resultData[i] /= matrix2Data[i];
        }

        return result;
    }


    public static Matrix operator +(Matrix matrix, double scalar)
    {
        Matrix result = matrix.Copy();
        var resultData = result.Data;

        for (int i = 0; i < resultData.Length; i++)
        {
            resultData[i] += scalar;
        }

        return result;
    }

    public static Matrix operator -(Matrix matrix, double scalar)
    {
        Matrix result = matrix.Copy();
        var resultData = result.Data;

        for (int i = 0; i < resultData.Length; i++)
        {
            resultData[i] -= scalar;
        }

        return result;
    }

    public static Matrix operator *(Matrix matrix, double scalar)
    {
        Matrix result = matrix.Copy();
        var resultData = result.Data;

        for (int i = 0; i < resultData.Length; i++)
        {
            resultData[i] *= scalar;
        }

        return result;
    }

    public static Matrix operator /(Matrix matrix, double scalar)
    {
        Matrix result = matrix.Copy();
        var resultData = result.Data;

        for (int i = 0; i < resultData.Length; i++)
        {
            resultData[i] /= scalar;
        }

        return result;
    }

    public Matrix MatMul(Matrix otherMatrix)
    {
        if (otherMatrix.Rows != Columns)
        {
            throw new ArgumentException("otherMatrix rows must be equal to the number of columns");
        }

        var otherMatrixColumns = otherMatrix.Columns;
        Matrix result = new Matrix(Rows, otherMatrixColumns);
        var otherMatrixData = otherMatrix.Data;
        var data = Data;
        var resultData = result.Data;
        
        var resultRows = result.Rows;
        var resultColumns = result.Columns;
        var columns = Columns;
        
        int resultDataIndex = 0;
        int currentRowDataIndex = 0;
        for (int i = 0; i < resultRows; i++)
        {
            for (int j = 0; j < resultColumns; j++)
            {
                double sum = 0.0;
                int otherRowDataIndex = 0;
                
                for (int k = 0; k < columns; k++)
                {
                    sum += data[currentRowDataIndex + k] * otherMatrixData[otherRowDataIndex + j];
                    otherRowDataIndex += otherMatrixColumns;
                }

                resultData[resultDataIndex] = sum;

                resultDataIndex++;
            }

            currentRowDataIndex += columns;
        }

        return result;
    }


    public Matrix Transpose()
    {
        var columns = Columns;
        var rows = Rows;
        
        Matrix result = new Matrix(columns, rows);
        var resultData = result.Data;
        var data = Data;

        int resultDataIndex = 0;
        var resultRows = result.Rows;
        var resultColumns = result.Columns;
        
        for (int i = 0; i < resultRows; i++)
        {
            int currenDataIndex = 0;
            
            for (int j = 0; j < resultColumns; j++)
            {
                resultData[resultDataIndex + j] = data[currenDataIndex + i];
                currenDataIndex += columns;
            }

            resultDataIndex += resultColumns;
        }

        return result;
    }

    public double Norm()
    {
        double sum = 0.0;
        var data = Data;

        for (int i = 0; i < data.Length; i++)
        {
            var value = data[i];
            sum += value * value;
        }

        return Math.Max(0.0, Math.Sqrt(Math.Max(0.0, sum)));
    }

    public Matrix Square()
    {
        Matrix result = new Matrix(Rows, Columns);
        var data = Data;
        var resultData = result.Data;

        for (int i = 0; i < resultData.Length; i++)
        {
            resultData[i] = data[i] * data[i];
        }

        return result;
    }

    public Matrix Sqrt()
    {
        Matrix result = new Matrix(Rows, Columns);
        var resultData = result.Data;
        var data = Data;

        for (int i = 0; i < resultData.Length; i++)
        {
            resultData[i] = Math.Sqrt(data[i]);
        }

        return result;
    }

    public double Dot(Matrix matrix)
    {
        if (matrix.Rows != Rows)
        {
            throw new ArgumentException("Matrices should have the same number of rows");
        }

        if (matrix.Columns != Columns)
        {
            throw new ArgumentException("Matrices should have the same number of columns");
        }

        var result = this * matrix;

        double sum = 0.0;
        foreach (var value in result.Data)
        {
            sum += value;
        }

        return sum;
    }

    public double Sum()
    {
        double sum = 0.0;
        foreach (var value in Data)
        {
            sum += value;
        }

        return sum;
    }

    public double Mean()
    {
        return Sum() / (Rows * Columns);
    }

    public double Var()
    {
        double sumOfSquares = 0.0;
        foreach (var value in Data)
        {
            sumOfSquares += value * value;
        }

        var mean = Mean();
        return sumOfSquares / (Rows * Columns) - mean * mean;
    }

    public double Std()
    {
        return Math.Sqrt(Var());
    }

    public Matrix Power(double exponent)
    {
        Matrix result = new Matrix(Rows, Columns);
        var resultData = result.Data;
        
        for (int i = 0; i < resultData.Length; i++)
        {
            resultData[i] = Math.Pow(resultData[i], exponent);
        }

        return result;
    }

    public double this[int i, int j]
    {
        get => Data[i * Columns + j];
        set => Data[i * Columns + j] = value;
    }

    public override string ToString()
    {
        StringBuilder result = new StringBuilder("");

        result.Append("[");
        int dataIndex = 0;
        for (int i = 0; i < Rows; i++)
        {
            if (i > 0)
            {
                result.Append(" ");
            }

            result.Append("[");

            for (int j = 0; j < Columns; j++)
            {
                result.Append($"{Data[dataIndex]}");
                if (j < Columns - 1)
                {
                    result.Append(", ");
                }

                dataIndex++;
            }

            result.Append("]");
            if (i < Rows - 1)
            {
                result.Append(",\n");
            }
        }

        result.Append("]");
        return result.ToString();
    }
}