using System.Text;

namespace TCTC;

public class TimeSeries<T>
{
    public List<DateTime> DateTimes { get; }
    public List<T> Values { get; }
    public int Count { get; }

    private TimeSeries()
    {
        /* left empty */
    }

    private TimeSeries(List<DateTime> dateTimes, List<T> values, int count)
    {
        DateTimes = dateTimes;
        Values = values;
        Count = count;
    }

    public static TimeSeries<E> FromDateTimeValueTuples<E>(List<(DateTime, E)> dateTimeValueTuples)
    {
        List<(DateTime, E)> dateTimeValueTuplesCopy = new List<(DateTime, E)>(dateTimeValueTuples);

        dateTimeValueTuplesCopy.Sort((tuple1, tuple2) => tuple1.Item1.CompareTo(tuple2.Item1));

        var dateTimes = new List<DateTime>();
        var values = new List<E>();

        foreach (var dateTimeValueTuple in dateTimeValueTuplesCopy)
        {
            dateTimes.Add(dateTimeValueTuple.Item1);
            values.Add(dateTimeValueTuple.Item2);
        }

        var count = dateTimeValueTuples.Count;

        return new TimeSeries<E>(dateTimes, values, count);
    }

    public static TimeSeries<E> FromDateTimesAndValues<E>(List<DateTime> dateTimes, List<E> values)
    {
        if (dateTimes.Count != values.Count)
        {
            throw new ArgumentException("dateTimes and values must be of the same length.");
        }

        List<(DateTime, E)> dateTimeValueTuples = new List<(DateTime, E)>();

        for (int i = 0; i < dateTimes.Count; i++)
        {
            dateTimeValueTuples.Add((dateTimes[i], values[i]));
        }

        return FromDateTimeValueTuples(dateTimeValueTuples);
    }

    public (DateTime, T) this[int index] => (DateTimes[index], Values[index]);

    public TimeSeries<T> Add(DateTime dateTime, T value)
    {
        List<DateTime> dateTimes = new List<DateTime>(DateTimes);
        List<T> values = new List<T>(Values);

        if (Count == 0)
        {
            dateTimes.Add(dateTime);
            values.Add(value);
            return FromDateTimesAndValues(dateTimes, values);
        }
        
        var lastDate = dateTimes.Last();
        dateTimes.Add(dateTime);
        values.Add(value);

        if (lastDate.CompareTo(dateTime) < 0)
        {
            return new TimeSeries<T>(dateTimes, values, dateTimes.Count);
        }

        return FromDateTimesAndValues(dateTimes, values);
    }

    public TimeSeries<T> Slice(int startIndex, int endIndex)
    {
        var count = endIndex - startIndex;
        List<DateTime> dateTimes = new List<DateTime>(DateTimes.Skip(startIndex).Take(count));
        List<T> values = new List<T>(Values.Skip(startIndex).Take(count));

        return new TimeSeries<T>(
            dateTimes,
            values,
            count);
    }

    public override string ToString()
    {
        StringBuilder result = new StringBuilder("");

        result.Append("[");
        for (int i = 0; i < Count; i++)
        {
            result.Append($"({DateTimes[i]}, {Values[i]})");
            if (i < Count - 1)
            {
                result.Append(", ");
            }
        }

        result.Append("]");


        return result.ToString();
    }
}