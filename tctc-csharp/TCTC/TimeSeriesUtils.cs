namespace TCTC;

public class TimeSeriesUtils
{
    public static (double Slope, double Intercept) FitLinearFunction(List<double> timeSeries)
    {
        Matrix designMatrix = new Matrix(timeSeries.Count, 2);
        var designMatrixData = designMatrix.Data;
        
        int designMatrixDataIndex = 0;
        for (int i = 0; i < timeSeries.Count; i++)
        {
            designMatrixData[designMatrixDataIndex] = i;
            designMatrixDataIndex++;
            designMatrixData[designMatrixDataIndex] = 1.0;
            designMatrixDataIndex++;
        }

        Matrix y = new Matrix(timeSeries);

        var result = RegressionSolver.SolveExact(designMatrix, y);

        return (result.Solution.Data[0], result.Solution.Data[1]);
    }

    public static double EvaluateLinearFunction(double slope, double intercept, double x)
    {
        return slope * x + intercept;
    }

    public static double SampleAutoCovariance(List<double> timeSeries, int lag)
    {
        int n = Math.Abs(lag);

        double mean = 0.0;
        foreach (var value in timeSeries)
        {
            mean += value;
        }

        var count = timeSeries.Count;
        mean /= count;

        double sum = 0.0;
        for (int i = 0; i < count - n; i++)
        {
            sum += (timeSeries[n + i] - mean) * (timeSeries[i] - mean);
        }

        return sum / count;
    }

    public static double SampleAutoCorrelation(List<double> timeSeries, int lag)
    {
        return SampleAutoCovariance(timeSeries, lag) / SampleAutoCovariance(timeSeries, 0);
    }

    private static int _argMax(List<double> values)
    {
        var maxValue = values[0];
        var maxIndex = 0;

        for (int i = 1; i < values.Count; i++)
        {
            if (maxValue < values[i])
            {
                maxValue = values[i];
                maxIndex = i;
            }
        }

        return maxIndex;
    }

    public static (int Period, int MaxPeriod) FindSeasonalityPeriod(List<double> timeSeries)
    {
        var count = timeSeries.Count;

        var trendParameters = FitLinearFunction(timeSeries);
        var deTrendedSeries = new List<double>();

        for (int i = 0; i < count; i++)
        {
            var value = timeSeries[i];
            var trend = trendParameters.Slope * i + trendParameters.Intercept;
            deTrendedSeries.Add(value - trend);
        }

        var maxPeriod = Math.Min(365, count / 2);

        List<double> acfValues = new List<double>();
        for (int i = 0; i <= maxPeriod; i++)
        {
            acfValues.Add(SampleAutoCorrelation(deTrendedSeries, i));
        }

        var acfValuesCount = acfValues.Count;

        List<double> mappedAcfValues = new List<double>();
        for (int i = 1; i <= acfValuesCount; i++)
        {
            var value = Math.Pow(i / (double)acfValuesCount, 1.0 / 4.0);
            value *= acfValues[i - 1];
            mappedAcfValues.Add(value);
        }

        if (_argMax(acfValues.Skip(1).ToList()) + 1 == acfValuesCount - 1)
        {
            return (acfValuesCount - 1, maxPeriod);
        }

        var newMappedAcfValues = new List<double>();
        newMappedAcfValues.Add(0);

        for (int i = 1; i < acfValuesCount - 1; i++)
        {
            var currentValue = mappedAcfValues[i];
            if (currentValue > Math.Max(mappedAcfValues[i - 1], mappedAcfValues[i + 1]))
            {
                newMappedAcfValues.Add(currentValue);
            }
            else
            {
                if (currentValue < 0)
                {
                    newMappedAcfValues.Add(currentValue);
                }
                else
                {
                    newMappedAcfValues.Add(-currentValue);
                }
            }
        }

        var bestPeriod = _argMax(newMappedAcfValues);
        if (bestPeriod == 0)
        {
            bestPeriod = _argMax(mappedAcfValues.Skip(1).ToList()) + 1;
        }

        return (bestPeriod, maxPeriod);
    }
}