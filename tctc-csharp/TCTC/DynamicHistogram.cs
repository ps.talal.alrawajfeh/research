using System.Text;

namespace TCTC;

public class DynamicHistogram
{
    private double DistancePercentage;

    public List<List<double>> Histogram { get; }
    public List<int> Frequencies { get; }

    private DynamicHistogram()
    {
        /* left empty */
    }

    private DynamicHistogram(
        double distancePercentage,
        List<List<double>> histogram,
        List<int> frequencies)
    {
        DistancePercentage = distancePercentage;
        Histogram = histogram;
        Frequencies = frequencies;
    }

    public int MaxFrequency()
    {
        return Frequencies.Max();
    }

    public int GetValueBinIndex(double newValue)
    {
        int bestIndex = -1;
        double minDistance = 0.0;

        for (int index = 0; index < Histogram.Count; index++)
        {
            foreach (var value in Histogram[index])
            {
                var maxDistance = Math.Max(value * DistancePercentage, newValue * DistancePercentage);
                var distance = Math.Abs(newValue - value);
                if (distance < maxDistance)
                {
                    if (bestIndex < 0 || distance < minDistance)
                    {
                        minDistance = distance;
                        bestIndex = index;
                    }
                }
            }
        }

        return bestIndex;
    }

    public static DynamicHistogram FromValues(double[] values, double distancePercentage = 0.1)
    {
        List<double> valuesList = new List<double>(values);
        valuesList.Sort();
        valuesList.Reverse();

        List<List<double>> histogram = new List<List<double>>();
        List<int> frequencies = new List<int>();

        histogram.Add(new List<double>());
        int currentIndex = 0;
        histogram[currentIndex].Add(valuesList[0]);
        frequencies.Add(1);

        for (int i = 1; i < valuesList.Count; i++)
        {
            if (valuesList[i - 1] * (1 - distancePercentage) <= valuesList[i])
            {
                histogram[currentIndex].Add(valuesList[i]);
                frequencies[currentIndex]++;
            }
            else
            {
                currentIndex++;
                histogram.Add(new List<double>());
                frequencies.Add(1);
                histogram[currentIndex].Add(valuesList[i]);
            }
        }

        return new DynamicHistogram(
            distancePercentage,
            histogram,
            frequencies);
    }
    
    public override string ToString()
    {
        StringBuilder result = new StringBuilder("");

        result.Append("Histogram: [");
        for (int i = 0; i < Histogram.Count; i++)
        {
            result.Append("[");
            for (int j = 0; j < Histogram[i].Count; j++)
            {
                result.Append(Histogram[i][j]);
                if (j < Histogram[i].Count - 1)
                {
                    result.Append(", ");
                }
            }
            result.Append("]");
            if (i < Histogram.Count - 1)
            {
                result.Append(", ");
            }
        }
        result.Append("]\n");
        
        result.Append("Frequencies: [");
        for (int i = 0; i < Frequencies.Count; i++)
        {
            result.Append(Frequencies[i].ToString());
            if (i < Frequencies.Count - 1)
            {
                result.Append(", ");
            }
        }
        result.Append("]");

        return result.ToString();
    }

}