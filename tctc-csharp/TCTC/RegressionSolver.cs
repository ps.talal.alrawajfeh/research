namespace TCTC;

public class RegressionSolver
{
    private static Matrix ComputeCoefficientsMatrix(Matrix designMatrix, double l2Regularization = 0.0)
    {
        var numberOfParameters = designMatrix.Columns;
        Matrix coefficientsMatrix = new Matrix(numberOfParameters, numberOfParameters);
        var n = designMatrix.Rows;
        var designMatrixData = designMatrix.Data;
        var coefficientsMatrixData = coefficientsMatrix.Data;

        int coefficientsMatrixDataIndex = 0;
        for (int row = 0; row < numberOfParameters; row++)
        {
            for (int col = 0; col < numberOfParameters; col++)
            {
                int designMatrixDataIndex = 0;
                double sum = 0.0;
                for (int i = 0; i < n; i++)
                {
                    sum += designMatrixData[designMatrixDataIndex + col] *
                           designMatrixData[designMatrixDataIndex + row];
                    designMatrixDataIndex += numberOfParameters;
                }

                if (row == col)
                {
                    sum += n * l2Regularization;
                }

                coefficientsMatrixData[coefficientsMatrixDataIndex] = sum;
                coefficientsMatrixDataIndex++;
            }
        }

        return coefficientsMatrix;
    }

    private static Matrix ComputeConstantsMatrix(Matrix designMatrix, Matrix y)
    {
        var numberOfParameters = designMatrix.Columns;
        Matrix constantsMatrix = new Matrix(numberOfParameters, 1);
        var constantsMatrixData = constantsMatrix.Data;
        var n = designMatrix.Rows;
        var yData = y.Data;
        var designMatrixData = designMatrix.Data;

        int constantsMatrixDataIndex = 0;
        for (int col = 0; col < numberOfParameters; col++)
        {
            int designMatrixDataIndex = 0;
            int yDataIndex = 0;
            double sum = 0.0;
            for (int i = 0; i < n; i++)
            {
                sum += yData[yDataIndex] * designMatrixData[designMatrixDataIndex + col];
                yDataIndex++;
                designMatrixDataIndex += numberOfParameters;
            }

            constantsMatrixData[constantsMatrixDataIndex] = sum;

            constantsMatrixDataIndex++;
        }

        return constantsMatrix;
    }

    public static (double Error, double RegularizedError) CalculateError(
        Matrix designMatrix,
        Matrix y,
        Matrix parameters,
        double l2Regularization)
    {
        var predictions = designMatrix.MatMul(parameters);
        var error = (predictions - y).Norm();
        error *= error;
        error /= designMatrix.Rows;

        var regularizedError = parameters.Norm() * l2Regularization;

        return (error, regularizedError);
        // double error = 0.0;
        // int designMatrixDataIndex = 0;
        // int yDataIndex = 0;
        // for (int i = 0; i < designMatrix.Rows; i++)
        // {
        //     double sum = 0.0;
        //
        //     int parametersDataIndex = 0;
        //     for (int j = 0; j < designMatrix.Columns; j++)
        //     {
        //         sum += parameters.Data[parametersDataIndex] * designMatrix.Data[designMatrixDataIndex];
        //         parametersDataIndex++;
        //         designMatrixDataIndex++;
        //     }
        //
        //     sum -= y.Data[yDataIndex];
        //     sum *= sum;
        //     error += sum;
        //     yDataIndex++;
        // }
        //
        // error /= 2 * designMatrix.Rows;
        //
        // double regularizedError = error;
        // double regularizationTerm = 0.0;
        // int dataIndex = 0;
        // for (int j = 0; j < designMatrix.Columns; j++)
        // {
        //     var param = parameters.Data[dataIndex];
        //     regularizationTerm += param * param;
        //     dataIndex++;
        // }
        //
        // regularizationTerm *= l2Regularization / 2;
        // regularizedError += regularizationTerm;
        //
        // return (error, regularizedError);
    }

    public static (double Error, double RegularizedError, Matrix Solution) SolveLeastSquares(
        Matrix designMatrix,
        Matrix y)
    {
        var solution = designMatrix.SolveLeastSquares(y);
        var errorResult = CalculateError(designMatrix, y, solution, 0.0);
        return (errorResult.Error, errorResult.RegularizedError, solution);
    }

    public static (double Error, double RegularizedError, Matrix Solution) SolveExact(
        Matrix designMatrix,
        Matrix y,
        double l2Regularization = 0.0,
        double epsilon = 1e-7)
    {
        var coefficientsMatrix = ComputeCoefficientsMatrix(designMatrix, l2Regularization);
        if (Math.Abs(coefficientsMatrix.Determinant()) < epsilon)
        {
            return (double.MaxValue, double.MaxValue, null);
        }

        var constantsMatrix = ComputeConstantsMatrix(designMatrix, y);
        var solution = coefficientsMatrix.Solve(constantsMatrix);
        var errorResult = CalculateError(designMatrix, y, solution, l2Regularization);
        return (errorResult.Error, errorResult.RegularizedError, solution);
    }

    public static (double Error, double RegularizedError, Matrix Solution) SolveApproximate(
        Matrix designMatrix,
        Matrix y,
        Random randomNumberGenerator,
        double l2Regularization = 0.0,
        double learningRate = 0.1,
        double rmsPropDiscountingFactor = 0.9,
        double rmsPropEpsilon = 1e-7,
        int maxIterations = 1000,
        double stopError = 1e-7,
        double stopGradientNorm = 1e-7,
        double earlyStoppingThreshold = 1e-10,
        int earlyStoppingPatience = 10)
    {
        var coefficientsMatrix = ComputeCoefficientsMatrix(designMatrix, l2Regularization);
        var constantsMatrix = ComputeConstantsMatrix(designMatrix, y);

        var solution = new Matrix(designMatrix.Columns, 1, randomNumberGenerator, -1.0, 1.0);

        List<(double Error, double RegularizedError, Matrix Solution)> solutionsAndErrors = new();

        int earlyStoppingPatienceCount = 0;

        Matrix? gradientMeanSquare = null;
        for (int i = 0; i < maxIterations; i++)
        {
            var gradient = coefficientsMatrix.MatMul(solution) - constantsMatrix;

            if (gradientMeanSquare == null)
            {
                gradientMeanSquare = gradient.Square();
            }
            else
            {
                gradientMeanSquare = gradientMeanSquare * rmsPropDiscountingFactor +
                                     gradient.Square() * (1 - rmsPropDiscountingFactor);
            }

            solution -= gradient / (gradientMeanSquare.Sqrt() + rmsPropEpsilon) * learningRate;
            var errorResult = CalculateError(designMatrix, y, solution, l2Regularization);
            if (Math.Abs(errorResult.RegularizedError) < stopError)
            {
                return (errorResult.Error, errorResult.RegularizedError, solution);
            }

            solutionsAndErrors.Add((errorResult.Error, errorResult.RegularizedError, solution));

            if (solutionsAndErrors.Count > 2)
            {
                var currentError = errorResult.RegularizedError;
                var previousError = solutionsAndErrors[^2].RegularizedError;
                if (Math.Abs(currentError - previousError) < earlyStoppingThreshold)
                {
                    earlyStoppingPatienceCount++;
                }
                else
                {
                    earlyStoppingPatienceCount = 0;
                }
            }

            if (Math.Abs(gradient.Norm()) < stopGradientNorm)
            {
                break;
            }

            if (earlyStoppingPatienceCount == earlyStoppingPatience)
            {
                break;
            }
        }

        var solutionWithMinError = solutionsAndErrors[0];
        var minError = solutionsAndErrors[0].RegularizedError;

        foreach (var solutionAndError in solutionsAndErrors)
        {
            if (solutionAndError.RegularizedError < minError)
            {
                solutionWithMinError = solutionAndError;
                minError = solutionAndError.RegularizedError;
            }
        }

        return solutionWithMinError;
    }
}