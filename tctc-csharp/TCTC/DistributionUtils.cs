namespace TCTC;

public class DistributionUtils
{
    public static double Erf(double x, int powerSeriesTerms = 50, double epsilon = 1e-13)
    {
        /*
         * The implementation of the erf function is based on https://en.wikipedia.org/wiki/Error_function
         * To evaluate the integral, the most efficient and accurate method was by using the power series expansion of
         * the integral. You can find more details by consulting:
         * https://proofwiki.org/wiki/Power_Series_Expansion_for_Error_Function
         */

        if (x < 0.0)
        {
            return -Erf(-x);
        }

        if (x > 6.0)
        {
            return 1.0;
        }

        double currentPower = x;
        double currentExponent = 1.0;
        double currentFactorial = 1.0;
        double currentSign = 1.0;
        double currentIndex = 0.0;
        double integral = currentSign * currentPower / (currentExponent * currentFactorial);

        for (int i = 1; i <= powerSeriesTerms; i++)
        {
            currentExponent += 2.0;
            currentIndex += 1.0;
            currentFactorial *= currentIndex;
            currentPower *= x * x;
            currentSign *= -1.0;
            double currentTerm = currentPower / (currentExponent * currentFactorial);
            if (currentTerm <= epsilon)
            {
                break;
            }
            integral += currentSign * currentTerm;
        }

        return 2.0 * integral / Math.Sqrt(Math.PI);
    }
    
    public static double MapPValueToConfidence(double pValue, double pValueThreshold)
    {
        if (0 <= pValue && pValue <= pValueThreshold)
        {
            return Math.Min(1.0, Math.Max(0.0, 0.5 / pValueThreshold * pValue));
        }

        return Math.Min(1.0, Math.Max(0.0, 0.5 + 0.5 / (1.0 - pValueThreshold) * (pValue - pValueThreshold)));
    }
}