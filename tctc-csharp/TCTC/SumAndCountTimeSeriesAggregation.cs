using System.Text;

namespace TCTC;

public enum TimePeriod
{
    Daily,
    Weekly,
    Monthly
}

public class SumAndCountTimeSeriesAggregation
{
    public TimeSeries<int> CountsTimeSeries { get; }
    public TimeSeries<double> SumsTimeSeries { get; }
    public TimePeriod TimePeriod { get; }

    private SumAndCountTimeSeriesAggregation()
    {
        /* left empty */
    }

    private SumAndCountTimeSeriesAggregation(
        TimeSeries<double> sumsTimeSeries,
        TimeSeries<int> countsTimeSeries,
        TimePeriod timePeriod)
    {
        SumsTimeSeries = sumsTimeSeries;
        CountsTimeSeries = countsTimeSeries;
        TimePeriod = timePeriod;
    }

    public int Length()
    {
        return SumsTimeSeries.Count;
    }

    public static SumAndCountTimeSeriesAggregation FromTimeSeries(TimeSeries<double> timeSeries, TimePeriod timePeriod)
    {
        var firstDateTime = timeSeries.DateTimes[0];

        Func<DateTime, DateTime> initialDateProvider;
        Func<DateTime, DateTime> nextDateProvider;

        if (timePeriod == TimePeriod.Daily)
        {
            initialDateProvider = d => new DateTime(d.Date.Ticks);
            nextDateProvider = d => d.AddDays(1);
        }
        else if (timePeriod == TimePeriod.Weekly)
        {
            initialDateProvider = d =>
            {
                var daysToSubtract = (7 + (d.DayOfWeek - DayOfWeek.Friday)) % 7;
                var startOfCurrentWeek = d.AddDays(-1 * daysToSubtract);
                return new DateTime(startOfCurrentWeek.Date.Ticks);
            };
            nextDateProvider = d => d.AddDays(7);
        }
        else
        {
            initialDateProvider = d => new DateOnly(d.Year, d.Month, 1).ToDateTime(TimeOnly.MinValue);
            nextDateProvider = d => d.AddMonths(1);
        }

        var currentDate = initialDateProvider(firstDateTime);
        var nextDate = nextDateProvider(currentDate);

        List<DateTime> aggregationDateTimes = new List<DateTime>();
        List<double> sums = new List<double>();
        List<int> counts = new List<int>();

        double sum = 0.0;
        int count = 0;

        for (int i = 0; i < timeSeries.Count; i++)
        {
            var dateTime = timeSeries.DateTimes[i];

            while (nextDate.CompareTo(dateTime) <= 0 || currentDate.CompareTo(dateTime) > 0)
            {
                if (count > 0)
                {
                    aggregationDateTimes.Add(currentDate);
                    sums.Add(sum);
                    counts.Add(count);

                    sum = 0.0;
                    count = 0;
                }

                currentDate = nextDate;
                nextDate = nextDateProvider(nextDate);
            }

            sum += timeSeries.Values[i];
            count++;
        }
    
        if (count > 0)
        {
            aggregationDateTimes.Add(currentDate);
            sums.Add(sum);
            counts.Add(count);
        }

        var countsTimeSeries = TimeSeries<int>.FromDateTimesAndValues(aggregationDateTimes, counts);
        var sumsTimeSeries = TimeSeries<double>.FromDateTimesAndValues(aggregationDateTimes, sums);

        return new SumAndCountTimeSeriesAggregation(
            sumsTimeSeries,
            countsTimeSeries,
            timePeriod);
    }

    public override string ToString()
    {
        StringBuilder result = new StringBuilder("");

        result.Append("Counts Series: [");
        for (int i = 0; i < CountsTimeSeries.Count; i++)
        {
            result.Append($"{(CountsTimeSeries.DateTimes[i], CountsTimeSeries.Values[i])}");
            if (i < CountsTimeSeries.Count - 1)
            {
                result.Append(", ");
            }
        }

        result.Append("]\n");

        result.Append("Sums Series: [");
        for (int i = 0; i < SumsTimeSeries.Count; i++)
        {
            result.Append($"{(SumsTimeSeries.DateTimes[i], SumsTimeSeries.Values[i])}");
            if (i < SumsTimeSeries.Count - 1)
            {
                result.Append(", ");
            }
        }

        result.Append("]\n");

        if (TimePeriod == TimePeriod.Daily)
        {
            result.Append("Time Period: Daily");
        }
        else if (TimePeriod == TimePeriod.Weekly)
        {
            result.Append("Time Period: Weekly");
        }
        else if (TimePeriod == TimePeriod.Monthly)
        {
            result.Append("Time Period: Monthly");
        }

        return result.ToString();
    }
}