namespace TCTC;

public class FourierSeriesModel
{
    public int Period { get; }
    public List<double> Coefficients { get; }

    private FourierSeriesModel()
    {
        /* left empty */
    }

    public FourierSeriesModel(
        int period,
        List<double> coefficients)
    {
        Period = period;
        Coefficients = coefficients;
    }

    public double Forecast(int timeIndex)
    {
        double angle = 2 * Math.PI * timeIndex / Period;

        double result = Coefficients[0] * timeIndex + Coefficients[1];

        var fourierCoefficients = (Coefficients.Count - 2) / 2;

        for (int j = 0; j < fourierCoefficients; j++)
        {
            result += Coefficients[2 + j] * Math.Sin(angle * (j +  1));
        }

        for (int j = 0; j < fourierCoefficients; j++)
        {
            result += Coefficients[2 + fourierCoefficients + j] * Math.Cos(angle * (j + 1));
        }

        return result;
    }
}

public class FourierSeriesForecasting
{
    public static (double Error, FourierSeriesModel Model) FitFourierSeriesModelWithLinearTrend(
        List<double> timeSeries,
        double periodTolerance = 0.0)
    {
        var seasonality = TimeSeriesUtils.FindSeasonalityPeriod(timeSeries);

        var startPeriod = (int)Math.Floor(seasonality.Period * (1 - periodTolerance));
        var endPeriod = (int)Math.Ceiling(seasonality.Period * (1 + periodTolerance));

        var minError = double.MaxValue;
        var bestPeriod = 0;
        List<double> bestCoefficients = null;

        var y = new Matrix(timeSeries);
        for (int period = startPeriod; period <= endPeriod; period++)
        {
            for (int numberOfCoefficients = 1; numberOfCoefficients <= period; numberOfCoefficients++)
            {
                Matrix designMatrix = new Matrix(timeSeries.Count, 2 + numberOfCoefficients * 2);
                var designMatrixData = designMatrix.Data;
                var designMatrixRows = designMatrix.Rows;

                int designMatrixDataIndex = 0;
                for (int i = 0; i < designMatrixRows; i++)
                {
                    double angle = 2 * Math.PI * i / period;

                    designMatrixData[designMatrixDataIndex] = i;
                    designMatrixDataIndex++;
                    designMatrixData[designMatrixDataIndex] = 1.0;
                    designMatrixDataIndex++;

                    for (int j = 1; j <= numberOfCoefficients; j++)
                    {
                        designMatrixData[designMatrixDataIndex] = Math.Sin(angle * j);
                        designMatrixDataIndex++;
                    }

                    for (int j = 1; j <= numberOfCoefficients; j++)
                    {
                        designMatrixData[designMatrixDataIndex] = Math.Cos(angle * j);
                        designMatrixDataIndex++;
                    }
                }

                var leastSquaresSolution = RegressionSolver.SolveLeastSquares(designMatrix, y);
                if (leastSquaresSolution.Solution != null && leastSquaresSolution.Error < minError)
                {
                    bestCoefficients = leastSquaresSolution.Solution.Data.ToList();
                    minError = leastSquaresSolution.Error;
                    bestPeriod = period;
                }

                var solution = RegressionSolver.SolveExact(designMatrix, y);
                if (solution.Solution != null && solution.Error < minError)
                {
                    bestCoefficients = solution.Solution.Data.ToList();
                    minError = solution.Error;
                    bestPeriod = period;
                }
            }
        }

        if (bestCoefficients == null)
        {
            for (int period = startPeriod; period <= endPeriod; period++)
            {
                for (int numberOfCoefficients = 1; numberOfCoefficients <= period; numberOfCoefficients++)
                {
                    Matrix designMatrix = new Matrix(timeSeries.Count, 2 + numberOfCoefficients * 2);
                    var designMatrixData = designMatrix.Data;
                    var designMatrixRows = designMatrix.Rows;
                    
                    int designMatrixDataIndex = 0;
                    for (int i = 0; i < designMatrixRows; i++)
                    {
                        double angle = 2 * Math.PI * i / period;

                        designMatrixData[designMatrixDataIndex] = i;
                        designMatrixDataIndex++;
                        designMatrixData[designMatrixDataIndex] = 1;
                        designMatrixDataIndex++;

                        for (int j = 1; j <= numberOfCoefficients; j++)
                        {
                            designMatrixData[designMatrixDataIndex] = Math.Sin(angle * j);
                            designMatrixDataIndex++;
                        }

                        for (int j = 1; j <= numberOfCoefficients; j++)
                        {
                            designMatrixData[designMatrixDataIndex] = Math.Cos(angle * j);
                            designMatrixDataIndex++;
                        }
                    }

                    var solution =
                        RegressionSolver.SolveApproximate(designMatrix, y, new Random(DateTime.Now.Millisecond));
                    if (solution.Solution != null && solution.Error < minError)
                    {
                        bestCoefficients = solution.Solution.Data.ToList();
                        minError = solution.Error;
                        bestPeriod = period;
                    }
                }
            }
        }

        return (minError, new FourierSeriesModel(bestPeriod, bestCoefficients));
    }
}