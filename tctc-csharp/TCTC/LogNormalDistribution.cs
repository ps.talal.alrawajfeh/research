namespace TCTC;

public class LogNormalDistribution
{
    public double Mu { get; }
    public double Sigma { get; }
    public double ZeroStdRangePercentage { get; }

    private const double EPSILON = 1e-7;

    private LogNormalDistribution()
    {
        /* left empty */
    }

    public LogNormalDistribution(double mu, double sigma, double zeroStdRangePercentage = 1.0)
    {
        if (sigma < 0.0)
        {
            throw new ArgumentException("sigma must be a positive value");
        }

        Mu = mu;
        Sigma = sigma;
        ZeroStdRangePercentage = zeroStdRangePercentage;
    }

    private double _ensureValueIsWithinTheCorrectRange(double value)
    {
        return Math.Min(1.0, Math.Max(0.0, value));
    }


    public double Density(double x)
    {
        if (x < EPSILON)
        {
            return 0.0;
        }

        var zeroCentered = Math.Log(x) - Mu;
        return _ensureValueIsWithinTheCorrectRange(Math.Exp(-zeroCentered * zeroCentered / (2 * Sigma * Sigma)) /
                                                   (Math.Sqrt(2 * Math.PI) * Sigma * x));
    }

    public double CumulativeDistribution(double x)
    {
        if (x < EPSILON)
        {
            return 0.0;
        }

        return _ensureValueIsWithinTheCorrectRange(0.5 *
                                                   (1 + DistributionUtils.Erf((Math.Log(x) - Mu) /
                                                                              (Math.Sqrt(2) * Sigma))));
    }

    public double Mode()
    {
        return Math.Exp(Mu - Sigma * Sigma);
    }

    private double CalculateProbabilityForZeroStd(double x)
    {
        var expMu = Math.Exp(Mu);
        return _ensureValueIsWithinTheCorrectRange(1.0 - Math.Abs(x - expMu) / (ZeroStdRangePercentage * expMu));
    }

    public double PValue(double x)
    {
        if (x < EPSILON)
        {
            return 1.0;
        }

        if (Sigma <= EPSILON)
        {
            return CalculateProbabilityForZeroStd(x);
        }

        return _ensureValueIsWithinTheCorrectRange(1 - CumulativeDistribution(x));
    }

    private double LikelihoodRatio(double x)
    {
        return Density(x) / Density(Mode());
    }

    public double Confidence(double x)
    {
        if (Sigma <= EPSILON)
        {
            return CalculateProbabilityForZeroStd(x);
        }

        if (x < EPSILON)
        {
            return _ensureValueIsWithinTheCorrectRange(LikelihoodRatio(EPSILON));
        }

        return _ensureValueIsWithinTheCorrectRange(LikelihoodRatio(x));
    }

    public static LogNormalDistribution MaximumLikelihoodEstimation(
        double[] values,
        double zeroStdRangePercentage)
    {
        int n = values.Length;

        if (n == 0)
        {
            throw new ArgumentException("values array should not be empty.");
        }

        double mu = 0;
        double sigma = 0.0;

        for (int i = 0; i < n; i++)
        {
            double value = values[i];

            value = value <= 1.0 ? 0.0 : Math.Log(value);

            mu += value;
            sigma += value * value;
        }

        mu /= n;
        sigma = Math.Max(0.0, Math.Sqrt(Math.Max(0.0, sigma / n - mu * mu)));
        return new LogNormalDistribution(mu, sigma, zeroStdRangePercentage);
    }

    public override string ToString()
    {
        return $"Mu: {Mu}, Sigma: {Sigma}, ZeroStdRangePercentage: {ZeroStdRangePercentage}";
    }
}