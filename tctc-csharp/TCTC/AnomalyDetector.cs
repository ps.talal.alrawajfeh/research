using System.Text;

namespace TCTC;

public enum AnomalyDetectionDecision
{
    Normal,
    Anomaly,
    InsufficientHistory
}

public class AnomalyDetectionDetailedInformation
{
    public AnomalyDetectionDecision Decision { get; }
    public double AmountConfidence { get; }
    public double DailyAmountPValue { get; }
    public double DailyCountPValue { get; }
    public double WeeklyAmountPValue { get; }
    public double WeeklyCountPValue { get; }
    public double MonthlyAmountPValue { get; }
    public double MonthlyCountPValue { get; }
    public double DateConfidence { get; }

    public double AmountsWithinAmountRangeBasedConfidence { get; }

    public AnomalyDetectionDetailedInformation
    (
        AnomalyDetectionDecision decision,
        double amountConfidence,
        double dailyAmountPValue,
        double dailyCountPValue,
        double weeklyAmountPValue,
        double weeklyCountPValue,
        double monthlyAmountPValue,
        double monthlyCountPValue,
        double dateConfidence,
        double amountsWithinAmountRangeBasedConfidence
    ) => (
        Decision,
        AmountConfidence,
        DailyAmountPValue,
        DailyCountPValue,
        WeeklyAmountPValue,
        WeeklyCountPValue,
        MonthlyAmountPValue,
        MonthlyCountPValue,
        DateConfidence,
        AmountsWithinAmountRangeBasedConfidence
    ) = (
        decision,
        amountConfidence,
        dailyAmountPValue,
        dailyCountPValue,
        weeklyAmountPValue,
        weeklyCountPValue,
        monthlyAmountPValue,
        monthlyCountPValue,
        dateConfidence,
        amountsWithinAmountRangeBasedConfidence
    );

    public override string ToString()
    {
        StringBuilder stringBuilder = new StringBuilder("");
        stringBuilder.Append("Decision: ");
        if (Decision == AnomalyDetectionDecision.Normal)
        {
            stringBuilder.Append("Normal\n");
        }
        else if (Decision == AnomalyDetectionDecision.Anomaly)
        {
            stringBuilder.Append("Anomaly\n");
        }
        else
        {
            stringBuilder.Append("Insufficient History\n");
        }

        stringBuilder.Append($"Amount Confidence: {Math.Round(AmountConfidence * 100, 2)}%\n");
        stringBuilder.Append($"Daily Amount P-Value: {Math.Round(DailyAmountPValue * 100, 2)}%\n");
        stringBuilder.Append($"Daily Count P-Value: {Math.Round(DailyCountPValue * 100, 2)}%\n");
        stringBuilder.Append($"Weekly Amount P-Value: {Math.Round(WeeklyAmountPValue * 100, 2)}%\n");
        stringBuilder.Append($"Weekly Count P-Value: {Math.Round(WeeklyCountPValue * 100, 2)}%\n");
        stringBuilder.Append($"Monthly Amount P-Value: {Math.Round(MonthlyAmountPValue * 100, 2)}%\n");
        stringBuilder.Append($"Monthly Count P-Value: {Math.Round(MonthlyCountPValue * 100, 2)}%\n");
        stringBuilder.Append($"Date Confidence: {Math.Round(DateConfidence * 100, 2)}%\n");
        stringBuilder.Append(
            $"Amounts Within Amount Range Based Confidence: {Math.Round(AmountsWithinAmountRangeBasedConfidence * 100, 2)}%");

        return stringBuilder.ToString();
    }
}

public class AnomalyDetector
{
    public double ConfidenceThreshold { get; }
    public double PValueThreshold { get; }
    public double DynamicHistogramDistancePercentage { get; }
    public double LogNormalZeroStdRangePercentage { get; }
    public int HistogramConfidenceMethodMinimumBins { get; }
    public int HistogramConfidenceMethodMinimumHistoryLength { get; }
    public int MinimumHistoryLength { get; }
    public int MinimumAggregationsLength { get; }

    public double AmountRangeRatio { get; }
    public double ZScoreCoefficient { get; }
    public double DefaultSigma { get; }

    private const double EPSILON = 1e-7;

    private readonly AnomalyDetectionDetailedInformation _insufficientHistoryInformation =
        new AnomalyDetectionDetailedInformation(
            AnomalyDetectionDecision.InsufficientHistory,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0);

    private AnomalyDetector()
    {
        /* left empty */
    }

    public AnomalyDetector(
        double confidenceThreshold,
        double pValueThreshold,
        double dynamicHistogramDistancePercentage = 0.15,
        double logNormalZeroStdRangePercentage = 0.6,
        int histogramConfidenceMethodMinimumBins = 2,
        int histogramConfidenceMethodMinimumHistoryLength = 5,
        int minimumHistoryLength = 4,
        int minimumAggregationsLength = 2,
        double amountRangeRatio = 0.3,
        double zScoreCoefficient = 0.15,
        double defaultSigma = 3.0)
    {
        ConfidenceThreshold = confidenceThreshold;
        PValueThreshold = pValueThreshold;
        DynamicHistogramDistancePercentage = dynamicHistogramDistancePercentage;
        LogNormalZeroStdRangePercentage = logNormalZeroStdRangePercentage;
        HistogramConfidenceMethodMinimumBins = histogramConfidenceMethodMinimumBins;
        HistogramConfidenceMethodMinimumHistoryLength = histogramConfidenceMethodMinimumHistoryLength;
        MinimumHistoryLength = minimumHistoryLength;
        MinimumAggregationsLength = minimumAggregationsLength;
        AmountRangeRatio = amountRangeRatio;
        ZScoreCoefficient = zScoreCoefficient;
        DefaultSigma = defaultSigma;
    }

    private double CalculateValueConfidence(TimeSeries<double> history, double value)
    {
        var historicalValues = history.Values.ToArray();
        var dynamicHistogram = DynamicHistogram.FromValues(
            historicalValues,
            DynamicHistogramDistancePercentage);

        double confidence;
        var histogramBins = dynamicHistogram.Histogram.Count;
        if (histogramBins < HistogramConfidenceMethodMinimumBins ||
            history.Count < HistogramConfidenceMethodMinimumHistoryLength)
        {
            confidence = LogNormalDistribution.MaximumLikelihoodEstimation(historicalValues,
                LogNormalZeroStdRangePercentage).Confidence(value);
        }
        else
        {
            int index = dynamicHistogram.GetValueBinIndex(value);
            if (index < 0)
            {
                confidence = 0.0;
            }
            else
            {
                confidence = dynamicHistogram.Frequencies[index] / (double)dynamicHistogram.MaxFrequency();
            }
        }

        return confidence;
    }

    private (bool, double, double) CalculateSumsAndCountsAggregationPValue(
        SumAndCountTimeSeriesAggregation sumAndCountTimeSeriesAggregation)
    {
        var length = sumAndCountTimeSeriesAggregation.Length();

        if (length < MinimumAggregationsLength)
        {
            return (false, 0.0, 0.0);
        }

        var historicalSums = sumAndCountTimeSeriesAggregation.SumsTimeSeries.Slice(
            0,
            length - 1);

        var historicalCounts = sumAndCountTimeSeriesAggregation.CountsTimeSeries.Slice(
            0,
            length - 1);

        var lastSum = sumAndCountTimeSeriesAggregation.SumsTimeSeries[length - 1].Item2;
        double lastSumPValue;
        if (lastSum < historicalSums.Values.Max())
        {
            lastSumPValue = 1.0;
        }
        else
        {
            lastSumPValue = LogNormalDistribution.MaximumLikelihoodEstimation(
                historicalSums.Values.ToArray(),
                LogNormalZeroStdRangePercentage).PValue(lastSum);
        }

        var lastCount = (double)sumAndCountTimeSeriesAggregation.CountsTimeSeries[length - 1].Item2;
        double lastCountPValue;
        if (lastCount < historicalCounts.Values.Max())
        {
            lastCountPValue = 1.0;
        }
        else
        {
            lastCountPValue = LogNormalDistribution.MaximumLikelihoodEstimation(
                historicalCounts.Values.Select(x => (double)x).ToArray(),
                LogNormalZeroStdRangePercentage).PValue(lastCount);
        }

        return (true, lastSumPValue, lastCountPValue);
    }

    private TimeSeries<double> FilterTimeSeriesWithinAmountRange(TimeSeries<double> timeSeries, double amount,
        double ratio)
    {
        double minAmount = amount * (1 - ratio);
        double maxAmount = amount * (1 + ratio);

        List<(DateTime, double)> filteredDateTimeAmountTuples = new List<(DateTime, double)>();

        for (int i = 0; i < timeSeries.Count; i++)
        {
            var (currentDate, currentAmount) = timeSeries[i];

            if (currentAmount >= minAmount && currentAmount <= maxAmount)
            {
                filteredDateTimeAmountTuples.Add((currentDate, currentAmount));
            }
        }

        return TimeSeries<double>.FromDateTimeValueTuples(filteredDateTimeAmountTuples);
    }

    private TimeSeries<int> DatesToDaysSeries<T>(TimeSeries<T> timeSeries)
    {
        List<(DateTime, int)> dateTimeDayTuples = new List<(DateTime, int)>();

        for (int i = 1; i < timeSeries.Count; i++)
        {
            var previousDateTime = timeSeries.DateTimes[i - 1];
            var timeSpan = timeSeries.DateTimes[i] - previousDateTime;
            dateTimeDayTuples.Add((previousDateTime, timeSpan.Days));
        }

        return TimeSeries<int>.FromDateTimeValueTuples(dateTimeDayTuples);
    }

    public AnomalyDetectionDetailedInformation Detect(TimeSeries<double> history, DateTime dateTime, double amount)
    {
        if (history.Count < MinimumHistoryLength)
        {
            return _insufficientHistoryInformation;
        }

        var amountConfidence = CalculateValueConfidence(history, amount);

        var historyWithNewDateTimeAndAmount = history.Add(dateTime, amount);

        var dailySumsAndCountsAggregation = SumAndCountTimeSeriesAggregation.FromTimeSeries(
            historyWithNewDateTimeAndAmount,
            TimePeriod.Daily);
        var (isValidDailySumsAndCounts, dailyAmountPValue, dailyCountPValue) =
            CalculateSumsAndCountsAggregationPValue(dailySumsAndCountsAggregation);
        if (!isValidDailySumsAndCounts)
        {
            return _insufficientHistoryInformation;
        }

        var weeklySumsAndCountsAggregation = SumAndCountTimeSeriesAggregation.FromTimeSeries(
            historyWithNewDateTimeAndAmount,
            TimePeriod.Weekly);
        var (isValidWeeklySumsAndCounts, weeklyAmountPValue, weeklyCountPValue) =
            CalculateSumsAndCountsAggregationPValue(weeklySumsAndCountsAggregation);
        if (!isValidWeeklySumsAndCounts)
        {
            return _insufficientHistoryInformation;
        }

        var monthlySumsAndCountsAggregation = SumAndCountTimeSeriesAggregation.FromTimeSeries(
            historyWithNewDateTimeAndAmount,
            TimePeriod.Monthly);
        var (isValidMonthlySumsAndCounts, monthlyAmountPValue, monthlyCountPValue) =
            CalculateSumsAndCountsAggregationPValue(monthlySumsAndCountsAggregation);
        if (!isValidMonthlySumsAndCounts)
        {
            return _insufficientHistoryInformation;
        }

        var filteredTimeSeries = FilterTimeSeriesWithinAmountRange(history, amount, AmountRangeRatio);
        double dateConfidence = 0.0;
        double amountsWithinAmountRangeBasedConfidence = 0.0;
        if (filteredTimeSeries.Count >= 2)
        {
            var daysSeries = DatesToDaysSeries(filteredTimeSeries);

            double mu = 0.0;
            double sigma = 0.0;
            foreach (var days in daysSeries.Values)
            {
                mu += days;
                sigma += days * days;
            }

            mu /= daysSeries.Count;
            sigma = Math.Max(0.0, Math.Sqrt(Math.Max(0.0, sigma / daysSeries.Count - mu * mu)));

            var currentDays = (dateTime - filteredTimeSeries.DateTimes.Last()).Days;
            if (sigma <= EPSILON)
            {
                sigma = DefaultSigma;
            }

            var zScore = Math.Abs(currentDays - mu) / sigma;
            dateConfidence = Math.Min(1.0, Math.Max(0.0, 1.0 - ZScoreCoefficient * zScore));

            amountsWithinAmountRangeBasedConfidence = CalculateValueConfidence(filteredTimeSeries, amount);
        }

        List<double> pValues = new List<double>()
        {
            dailyAmountPValue,
            dailyCountPValue,
            weeklyAmountPValue,
            weeklyCountPValue,
            monthlyAmountPValue,
            monthlyCountPValue
        };

        // Console.WriteLine(filteredTimeSeries);
        // Console.WriteLine(amount);
        // Console.WriteLine($"{ConfidenceThreshold}, {dateConfidence}, {amountsWithinAmountRangeBasedConfidence}");
        if (pValues.Min() < PValueThreshold ||
            amountConfidence < ConfidenceThreshold ||
            dateConfidence < ConfidenceThreshold ||
            amountsWithinAmountRangeBasedConfidence < ConfidenceThreshold)
        {
            return new AnomalyDetectionDetailedInformation(AnomalyDetectionDecision.Anomaly,
                amountConfidence,
                dailyAmountPValue,
                dailyCountPValue,
                weeklyAmountPValue,
                weeklyCountPValue,
                monthlyAmountPValue,
                monthlyCountPValue,
                dateConfidence,
                amountsWithinAmountRangeBasedConfidence);
        }

        return new AnomalyDetectionDetailedInformation(AnomalyDetectionDecision.Normal,
            amountConfidence,
            dailyAmountPValue,
            dailyCountPValue,
            weeklyAmountPValue,
            weeklyCountPValue,
            monthlyAmountPValue,
            monthlyCountPValue,
            dateConfidence,
            amountsWithinAmountRangeBasedConfidence);
    }
}