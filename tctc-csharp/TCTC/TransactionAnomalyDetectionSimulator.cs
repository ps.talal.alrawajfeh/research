using System.Runtime.InteropServices;

namespace TCTC;

public class AnomalyDetectionSimulationResults
{
    public int InsufficientHistoryTransactions { get; }
    public int HistoricalTransactions { get; }
    public int TestTransactions { get; }
    public Dictionary<(int PValueThreshold, int ConfidenceThreshold), int> AnomalousTransactionsPerThresholds;

    private AnomalyDetectionSimulationResults()
    {
        /* left empty */
    }

    public AnomalyDetectionSimulationResults(
        int insufficientHistoryTransactions,
        int historicalTransactions,
        int testTransactions,
        Dictionary<(int PValueThreshold, int ConfidenceThreshold), int> anomalousTransactionsPerThresholds)
    {
        InsufficientHistoryTransactions = insufficientHistoryTransactions;
        HistoricalTransactions = historicalTransactions;
        TestTransactions = testTransactions;
        AnomalousTransactionsPerThresholds = anomalousTransactionsPerThresholds;
    }
}

public class TransactionAnomalyDetectionSimulator
{
    public AnomalyDetector AnomalyDetector { get; }
    public double HistoricalTransactionsPercentage { get; }
    private const double EPSILON = 1e-7;

    private TransactionAnomalyDetectionSimulator()
    {
        /* left empty */
    }

    public TransactionAnomalyDetectionSimulator(
        AnomalyDetector anomalyDetector,
        double historicalTransactionsPercentage)
    {
        AnomalyDetector = anomalyDetector;
        HistoricalTransactionsPercentage = historicalTransactionsPercentage;
    }

    private Dictionary<(int PValueThreshold, int ConfidenceThreshold), int> _initializeAnomalousTransactionDictionary()
    {
        Dictionary<(int PValueThreshold, int ConfidenceThreshold), int> anomalousTransactionsPerThresholds =
            new Dictionary<(int PValueThreshold, int ConfidenceThreshold), int>();

        for (int pValueThreshold = 1; pValueThreshold < 100; pValueThreshold++)
        {
            for (int confidenceThreshold = 1; confidenceThreshold < 100; confidenceThreshold++)
            {
                anomalousTransactionsPerThresholds.Add(
                    (PValueThreshold: pValueThreshold, ConfidenceThreshold: confidenceThreshold), 0);
            }
        }

        return anomalousTransactionsPerThresholds;
    }

    private Dictionary<long, Dictionary<long, List<Transaction>>> _groupTransactionsByPayAndBfdAccountNumbers(
        List<Transaction> transactions)
    {
        var transactionGroups = new Dictionary<long, Dictionary<long, List<Transaction>>>();

        foreach (var transaction in transactions)
        {
            if (transaction.Amount < EPSILON)
            {
                continue;
            }

            if (!transactionGroups.ContainsKey(transaction.PayAccountNumber))
            {
                transactionGroups.Add(transaction.PayAccountNumber, new Dictionary<long, List<Transaction>>());
            }

            if (!transactionGroups[transaction.PayAccountNumber].ContainsKey(transaction.BfdAccountNumber))
            {
                transactionGroups[transaction.PayAccountNumber]
                    .Add(transaction.BfdAccountNumber, new List<Transaction>());
            }

            transactionGroups[transaction.PayAccountNumber][transaction.BfdAccountNumber].Add(transaction);
        }

        return transactionGroups;
    }

    public AnomalyDetectionSimulationResults Simulate(List<Transaction> transactions)
    {
        int historicalTransactionsCount = (int)Math.Floor(transactions.Count * HistoricalTransactionsPercentage);

        List<Transaction> sortedTransactions = new List<Transaction>(transactions);
        sortedTransactions.Sort((x, y) => x.Date.CompareTo(y.Date));

        List<Transaction> historicalTransactions = sortedTransactions.Take(historicalTransactionsCount).ToList();
        List<Transaction> testTransactions = sortedTransactions.Skip(historicalTransactionsCount).ToList();

        int testTransactionsCount = testTransactions.Count;
        var historicalTransactionGroups = _groupTransactionsByPayAndBfdAccountNumbers(historicalTransactions);
        var anomalousTransactionDictionary = _initializeAnomalousTransactionDictionary();

        int insufficientHistoryTransactions = 0;

        int processedCount = 0;
        int currentPercentage = 0;

        foreach (var transaction in testTransactions)
        {
            if (!historicalTransactionGroups.ContainsKey(transaction.PayAccountNumber))
            {
                historicalTransactionGroups.Add(transaction.PayAccountNumber,
                    new Dictionary<long, List<Transaction>>());
            }

            if (!historicalTransactionGroups[transaction.PayAccountNumber].ContainsKey(transaction.BfdAccountNumber))
            {
                historicalTransactionGroups[transaction.PayAccountNumber]
                    .Add(transaction.BfdAccountNumber, new List<Transaction>());
            }

            var payToBfdHistoricalTransactions =
                historicalTransactionGroups[transaction.PayAccountNumber][transaction.BfdAccountNumber];

            var payToBfdHistoricalTimeSeries = TimeSeries<double>.FromDateTimesAndValues(
                payToBfdHistoricalTransactions.Select(x => x.Date).ToList(),
                payToBfdHistoricalTransactions.Select(x => x.Amount).ToList());

            var anomalyDetectionResults =
                AnomalyDetector.Detect(payToBfdHistoricalTimeSeries, transaction.Date, transaction.Amount);

            if (anomalyDetectionResults.Decision == AnomalyDetectionDecision.InsufficientHistory)
            {
                insufficientHistoryTransactions++;
            }
            else
            {
                for (int pValueThreshold = 1; pValueThreshold < 100; pValueThreshold++)
                {
                    var pValueThresholdPercentage = pValueThreshold / 100.0;

                    List<double> pValues = new List<double>()
                    {
                        // anomalyDetectionResults.DailyAmountPValue,
                        // anomalyDetectionResults.DailyCountPValue,
                        DistributionUtils.MapPValueToConfidence(anomalyDetectionResults.WeeklyAmountPValue,
                            pValueThresholdPercentage),
                        DistributionUtils.MapPValueToConfidence(anomalyDetectionResults.WeeklyCountPValue,
                            pValueThresholdPercentage),
                        DistributionUtils.MapPValueToConfidence(anomalyDetectionResults.MonthlyAmountPValue,
                            pValueThresholdPercentage),
                        DistributionUtils.MapPValueToConfidence(anomalyDetectionResults.MonthlyCountPValue,
                            pValueThresholdPercentage)
                    };

                    bool pValueIsAnomaly = pValues.Min() < pValueThresholdPercentage;

                    for (int confidenceThreshold = 1; confidenceThreshold < 100; confidenceThreshold++)
                    {
                        var confidenceThresholdPercentage = confidenceThreshold / 100.0;
                        if (pValueIsAnomaly ||
                            anomalyDetectionResults.AmountConfidence < confidenceThresholdPercentage ||
                            anomalyDetectionResults.DateConfidence < confidenceThresholdPercentage)
                        {
                            anomalousTransactionDictionary[
                                (PValueThreshold: pValueThreshold, ConfidenceThreshold: confidenceThreshold)]++;
                        }
                    }
                }
            }

            historicalTransactionGroups[transaction.PayAccountNumber][transaction.BfdAccountNumber].Add(transaction);

            processedCount++;
            var processedPercentage = (int)Math.Floor(processedCount * 100.0 / testTransactionsCount);
            if (processedPercentage > currentPercentage)
            {
                currentPercentage = processedPercentage;
                Console.WriteLine($"{currentPercentage}%");
            }
        }

        Console.WriteLine("100%");

        return new AnomalyDetectionSimulationResults(
            insufficientHistoryTransactions,
            historicalTransactionsCount,
            testTransactionsCount,
            anomalousTransactionDictionary);
    }
}