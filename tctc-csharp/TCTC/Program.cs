﻿using System.Diagnostics;
using System.Runtime.InteropServices;

namespace TCTC;

class Program
{
    private static DateTime _parseDate(string dateString)
    {
        var parts = dateString.Split("-");
        var year = int.Parse(parts[0]) + 2000;
        var month = int.Parse(parts[1]);
        var day = int.Parse(parts[2]);
        return new DateOnly(year, month, day).ToDateTime(TimeOnly.MinValue);
    }

    private static List<Transaction> _parseCSVFile(string filePath)
    {
        var lines = File.ReadAllLines(filePath);
        List<Transaction> transactions = new List<Transaction>();

        foreach (var line in lines.Skip(1))
        {
            if (line.Trim() == "")
            {
                continue;
            }

            var parts = line.Split(",");
            if (parts.Length < 13)
            {
                continue;
            }

            Transaction transaction = new Transaction(
                _parseDate(parts[12]),
                double.Parse(parts[8]),
                long.Parse(parts[6]),
                long.Parse(parts[5]));

            transactions.Add(transaction);
        }

        return transactions;
    }

    static void Main(string[] args)
    {
        // Stopwatch stopwatch1 = new Stopwatch();
        // stopwatch1.Start();
        // for (int i = 0; i < 10000; i++)
        // {
        //     double x = DistributionUtils.Erf(1);
        // }
        //
        // stopwatch1.Stop();
        //
        // Console.WriteLine(stopwatch1.ElapsedMilliseconds / 10000.0);

        // var values = new[] { 100.0, 110.0,120.0, 500.0, 550, 1000.0, 1105, 1200, 1020 };

        // var values = new[] { 100.0, 100.0, 100.0, 100.0, 100.0, 100.0, 100.0, 100.0 };
        //
        // var hist = DynamicHistogram.FromValues(values);
        //
        // Console.WriteLine(hist.GetValueBinIndex(95));
        //
        // PrintHistogram(hist);

        // DateTime dateTime = DateTime.Now;
        //
        // Calendar calendar = new GregorianCalendar();
        //
        // Console.WriteLine(calendar.GetWeekOfYear(dateTime, CalendarWeekRule.FirstDay, DayOfWeek.Friday));
        // int diff = (7 + (dateTime.DayOfWeek - DayOfWeek.Friday)) % 7;
        // var addDays = dateTime.AddDays(-1 * diff);
        //
        // Console.WriteLine(new DateTime(addDays.Date.Ticks));

        // List<DateTime> dateTimes = new List<DateTime>();
        //
        // dateTimes.Add(new DateTime(2023, 7, 03, 1, 0, 0));
        // dateTimes.Add(new DateTime(2023, 8, 11, 1, 0, 0));
        // dateTimes.Add(new DateTime(2023, 8, 12, 2, 0, 0));
        // dateTimes.Add(new DateTime(2023, 9, 21, 3, 0, 0));
        // dateTimes.Add(new DateTime(2023, 9, 22, 4, 0, 0));
        // dateTimes.Add(new DateTime(2023, 9, 23, 5, 0, 0));
        //
        // List<double> values = new List<double>() { 10.0, 15.0, 11.0, 5.0, 1.0, 6.0 };
        //
        // AnomalyDetector anomalyDetector = new AnomalyDetector(0.05, 0.05);
        //
        // var result = anomalyDetector.Detect(
        //     TimeSeries<double>.FromDateTimesAndValues(dateTimes, values),
        //     new DateTime(2023, 10, 28, 12, 0, 0),
        //     15.0);
        //
        // Console.WriteLine(result);

        // Console.WriteLine(DateTime.Now);
        // var transactions = _parseCSVFile("/home/u764/Development/progressoft/research/tctc/cheques_in_import_3.csv");
        //
        // var simulator = new TransactionAnomalyDetectionSimulator(new AnomalyDetector(1.0, 1.0), 0.7);
        //
        // var results = simulator.Simulate(transactions);
        //
        // Console.WriteLine($"Historical Transactions #: {results.HistoricalTransactions}");
        // Console.WriteLine($"Test Transactions #: {results.TestTransactions}");
        // var insufficientHistoryPercentage = results.InsufficientHistoryTransactions / (double)results.TestTransactions;
        // insufficientHistoryPercentage = Math.Round(insufficientHistoryPercentage * 100.0, 2);
        // Console.WriteLine($"Insufficient History: {insufficientHistoryPercentage}");
        //
        // Console.WriteLine("");
        //
        // for (int i = 1; i < 100; i++)
        // {
        //     var anomaliesPercentage =
        //         results.AnomalousTransactionsPerThresholds[(PValueThreshold: i, ConfidenceThreshold: i)] /
        //         (double)results.TestTransactions;
        //     var anomalyPercentage = Math.Round(anomaliesPercentage * 100.0, 2);
        //     var coveragePercentage = Math.Round(100.0 - (anomalyPercentage + insufficientHistoryPercentage), 2);
        //     Console.WriteLine($"Threshold: {i}%, Anomalies: {anomalyPercentage}%, Coverage: {coveragePercentage}%");
        // }
        //
        // Console.WriteLine(DateTime.Now);

        // var matrix = new Matrix(new double[,]
        // {
        //     { 3, -1, 14 },
        //     { 2, 2, 3 },
        //     { 1, -12, -18 },
        // });
        // var matrix1 = new Matrix(new[,]
        // {
        //     { 3, 2, -1 },
        //     { 2, -2, 4 },
        //     { -1, 0.5, -1 }
        // });
        // Console.WriteLine(matrix.SolveLeastSquares(new Matrix(new double[,] { { 1 }, { -2 }, { 0 } })));

        // var matrix2 = new Matrix(new[,]
        // {
        //     { 1},
        //     { 5 },
        //     { 9.0 }
        // });
        // Console.WriteLine(matrix1.MatMul(matrix2));

        // double a = 5.0;
        // double b = -10.0;
        //
        // Matrix designMatrix = new Matrix(100, 2);
        // Matrix y = new Matrix(100, 1);
        //
        // Random random = new Random(123);
        //
        // for (int i = 0; i < 100; i++)
        // {
        //     var x = random.NextDouble() * 20 - 10;
        //     designMatrix[i, 0] = x;
        //     designMatrix[i, 1] = 1.0;
        //     y[i, 0] = a * x + b + random.NextDouble();
        // }
        //
        // Console.WriteLine(RegressionSolver.SolveApproximate(designMatrix, y, random));

        // var timeSeries = new List<double>() { 1.1, 2.05, 4.96, 1.02, 2.11, 5.03, 0.9, 2.0, 5.0, 1.0, 2.01, 5.0 };
        //
        // var result = FourierSeriesForecasting.FitFourierSeriesModelWithLinearTrend(timeSeries);
        //
        // // Console.WriteLine(TimeSeriesUtils.FindSeasonalityPeriod(timeSeries));
        // Console.WriteLine(result.Error);
        // Console.WriteLine(result.Model.Period);
        // result.Model.Coefficients.ForEach(x => Console.WriteLine(x));
        //
        // Console.WriteLine("--------------------------------");
        // for (int i = 0; i < 100; i++)
        // {
        //     Console.WriteLine(result.Model.Forecast(i));
        // }

        // From here --->
        var random = new Random();
        
        // int patternMaxLength = 365;
        // int patternMaxValue = 100;
        // int timeSeriesLength = 2000;
        int patternMaxLength = 20;
        int patternMaxValue = 30;
        int timeSeriesLength = 100;
        double noiseRatio = 0.05;
        
        int n = 0;
        double totalError = 0.0;
        var stopWatch = new Stopwatch();
        stopWatch.Start();
        for (int i = 0; i < 1000; i++)
        {
            var patternLength = random.NextInt64(1, patternMaxLength);
            List<double> pattern = new List<double>();
            for (int j = 0; j < patternLength; j++)
            {
                pattern.Add(random.NextInt64(1, patternMaxValue));
            }
        
            var repetitions = timeSeriesLength / patternLength;
            var timeSeries = new List<double>();
            for (int j = 0; j < repetitions; j++)
            {
                for (int k = 0; k < patternLength; k++)
                {
                    var value = pattern[k] * (1 + random.NextDouble() * noiseRatio * 2 - noiseRatio);
                    timeSeries.Add(value);
                }
            }
        
            var trainingDataSize = timeSeries.Count / 2;
            var result =
                FourierSeriesForecasting.FitFourierSeriesModelWithLinearTrend(
                    timeSeries.Take(trainingDataSize).ToList());
        
            var validationDataSize = timeSeries.Count - trainingDataSize;
        
            var error = 0;
            for (int j = 0; j < validationDataSize; j++)
            {
                var forecasted = result.Model.Forecast(trainingDataSize + j);
                var actual = timeSeries[trainingDataSize + j];
        
                if (Math.Abs(actual) < 1e-7)
                {
                    if (Math.Abs(forecasted - actual) > 0.2)
                    {
                        error++;
                    }
                }
                else if (Math.Abs((forecasted - actual) / actual) > 0.2)
                {
                    error++;
                }
            }
        
            totalError += (double)error / (double)validationDataSize;
            n++;
        }
        stopWatch.Stop();
        Console.WriteLine(totalError / (double)n);
        Console.WriteLine(stopWatch.ElapsedMilliseconds);
        // <----- to here

        // var matrix1 = new Matrix(new[,]
        // {
        //     { 1.0, 2.0, 3.0 },
        //     { 4.0, 5.0, 6.0 },
        //     { 7.0, 8.0, 9.0 }
        // });
        //
        // var stopWatch = new Stopwatch();
        // stopWatch.Start();
        // for (int i = 0; i < 10000; i++)
        // {
        //     matrix1.Svd();
        // }
        // stopWatch.Stop();
        // Console.WriteLine((double) stopWatch.ElapsedMilliseconds / 10000.0);
        // Console.WriteLine("------------------");
        //
        // var valueTuple = matrix1.Svd();
        //
        // Console.WriteLine(valueTuple.SingularValues[0]);
        // Console.WriteLine(valueTuple.SingularValues[1]);
        // Console.WriteLine(valueTuple.SingularValues[2]);
        // Console.WriteLine(valueTuple.LeftEigenVectors);
        // Console.WriteLine(valueTuple.RightEigenVectors);
        //
        // Console.WriteLine(valueTuple.SingularMatrix);
        // Console.WriteLine("-----------------");
        //
        // Console.WriteLine(
        //     valueTuple.LeftEigenVectors.MatMul(
        //         valueTuple.SingularMatrix.MatMul(valueTuple.RightEigenVectors.Transpose())));
    }
}