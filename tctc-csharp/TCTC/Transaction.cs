namespace TCTC;

public class Transaction
{
    public DateTime Date { get; }
    public double Amount { get; }
    public long PayAccountNumber { get; }
    public long BfdAccountNumber { get; }

    private Transaction()
    {
        /* left empty */
    }

    public Transaction(DateTime date, double amount, long payAccountNumber, long bfdAccountNumber)
    {
        Date = date;
        Amount = amount;
        PayAccountNumber = payAccountNumber;
        BfdAccountNumber = bfdAccountNumber;
    }
}