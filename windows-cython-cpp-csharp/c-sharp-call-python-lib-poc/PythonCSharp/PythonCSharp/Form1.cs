﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PythonCSharp
{
    public partial class Form1 : Form
    {
        [DllImport("MYLIB", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        static extern int predict([MarshalAs(UnmanagedType.LPArray)] byte[] image_buff, int buff_size);
        

        public Form1()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            var openFileDialog = new OpenFileDialog();
            openFileDialog.ShowDialog();
            var filePath = openFileDialog.FileName;
            pictureBox1.Image = new Bitmap(filePath);
        }

        public static byte[] ImageToByte(Image img)
        {
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(img, typeof(byte[]));
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            byte[] img = ImageToByte(pictureBox1.Image);
            lblResult.Text = predict(img, img.Length).ToString();
        }
    }
}
