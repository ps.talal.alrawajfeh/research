import cv2
import numpy as np
from keras import backend as K
from keras.models import load_model

IMG_ROWS, IMG_COLS = 28, 28


def pre_process_batch(batch):
    model_input = np.array(batch)

    if K.image_data_format() == 'channels_first':
        model_input = model_input.reshape(model_input.shape[0], 1, IMG_ROWS, IMG_COLS)
    else:
        model_input = model_input.reshape(model_input.shape[0], IMG_ROWS, IMG_COLS, 1)

    model_input = model_input.astype('float32')
    model_input /= 255

    return model_input


def pre_process(image):
    return pre_process_batch([image])


def read_images_from_bytes(images):
    inputs = [np.fromstring(img, np.uint8) for img in images]
    return [cv2.imdecode(i, 0) for i in inputs]


def predict(image):
    model = load_model('model.h5')
    predictions = model.predict(pre_process(image))
    return int(predictions[0].argmax())


# cdef int predict_image(char *image_bytes):
#     return predict(read_images_from_bytes([image_bytes])[0])


cdef public void say_hello():
    print("Hello World!\n")