#include <Python.h>
#include <stdio.h>
#include "model.h"

int main(int argc, char *argv[])
{
    int status = PyImport_AppendInittab("model", PyInit_model);

    if (status == -1)
    {
        return -1;
    }

    Py_Initialize();

    PyObject *module = PyImport_ImportModule("model");

    if (module == NULL)
    {
        Py_Finalize();
        return -1;
    }

    say_hello();

    Py_Finalize();
    return 0;
}
