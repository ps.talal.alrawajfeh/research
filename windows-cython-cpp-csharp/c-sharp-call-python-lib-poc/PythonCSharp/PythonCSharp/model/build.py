#!/usr/bin/python3.7

from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

ext_modules = [Extension("model", ["model.pyx"])]

setup(
    name = 'Cython Example',
    cmdclass = {'build_ext': build_ext},
    ext_modules = ext_modules
)

