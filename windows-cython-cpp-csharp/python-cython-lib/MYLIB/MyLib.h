#pragma once

#ifdef MyLib_EXPORTS
#define MyLib_API __declspec(dllexport)
#else
#define MyLib_API __declspec(dllimport)
#endif

extern "C" MyLib_API int predict(const char *image_buff, int buff_size);