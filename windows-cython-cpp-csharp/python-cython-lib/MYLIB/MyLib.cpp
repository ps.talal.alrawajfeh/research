#include "pch.h"
#include "MyLib.h"
#include <Python.h>
#include <iostream>
#include <fstream>
#include <iterator>
#include <algorithm>
#include <vector>
#include "model.h"

int predict(const char* image_buff, int buff_size)
{
	int status = PyImport_AppendInittab("model", PyInit_model);

	if (status == -1)
	{
		return -1;
	}

	Py_Initialize();

	PyObject* module = PyImport_ImportModule("model");

	if (module == NULL)
	{
		Py_Finalize();
		return -1;
	}

	// First Test
	say_hello();

	int val = predict_image(PyBytes_FromStringAndSize(image_buff, buff_size));

	Py_Finalize();

	return val;
}
