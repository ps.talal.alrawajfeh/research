@echo off

del model.c
del model.h
del model.obj
del model.cp37-win_amd64.lib
del model.cp37-win_amd64.exp
del model.cp37-win_amd64.pyd

rmdir /q /s build
rmdir /q /s output