@echo off

rmdir /q /s output

python build.py clean
python build.py build_ext --inplace

move build\temp.win-amd64-3.7\Release\model.cp37-win_amd64.exp .\
move build\temp.win-amd64-3.7\Release\model.cp37-win_amd64.lib .\
move build\temp.win-amd64-3.7\Release\model.obj .\

rmdir /q /s build

mkdir output

del model.c

move model.obj output\
move model.h output\
move model.cp37-win_amd64.lib output\
move model.cp37-win_amd64.exp output\
move model.cp37-win_amd64.pyd output\

echo Done!