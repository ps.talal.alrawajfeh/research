#include <Python.h>
#include <iostream>
#include <fstream>
#include <iterator>
#include <algorithm>
#include <vector>
#include "model.h"

int main(int argc, char* argv[])
{
	int status = PyImport_AppendInittab("model", PyInit_model);

	if (status == -1)
	{
		return -1;
	}

	Py_Initialize();

	PyObject* module = PyImport_ImportModule("model");

	if (module == NULL)
	{
		Py_Finalize();
		return -1;
	}

	// First Test
	say_hello();


	// Second Test
	std::ifstream input("three.jpg", std::ios::binary);
	std::vector<char> buffer(std::istreambuf_iterator<char>(input), {});
	char* img = &buffer[0];

	std::cout << predict_image(PyBytes_FromStringAndSize(img, buffer.size())) << std::endl;

	Py_Finalize();
	
	return 0;
}
