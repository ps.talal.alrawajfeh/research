            RandomXor randomXor = new RandomXor();

            var files = Directory.GetFiles("/home/u764/Downloads/aub_denoising/only");
            foreach (var file in files)
            {
                var bytes = File.ReadAllBytes(file);
                var encrypted = randomXor.Encrypt(bytes);

                var fileName = Path.GetFileName(file);
                var parts = fileName.Split(".");

                var newFileName = parts[0] + "_enc." + parts[1];
                var newPath = Path.GetDirectoryName(file) + "/encrypted/" + newFileName;
                
                File.WriteAllBytes(newPath, encrypted.Value);
            }
