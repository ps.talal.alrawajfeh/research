#!/bin/bash

lib_path=$(pwd)/lib
export PATH=$PATH:$lib_path
export LIBRARY_PATH=$LIBRARY_PATH:$lib_path
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$lib_path

gcc -Iinclude -Llib hello_tf.c -ltensorflow -o hello_tf

./hello_tf

