#include <stdio.h>
#include <jpeglib.h>
#include <stdlib.h>
#include <string.h>
#include <tensorflow/c/c_api.h>                                                           

int width = 28;
int height = 28;
int bytes_per_pixel = 3;   /* or 1 for GRACYSCALE images */
int color_space = JCS_RGB; /* or JCS_GRAYSCALE for grayscale images */

void free_buffer(void* data, size_t length) {                                             
        free(data);                                                                       
}                                                                                         

TF_Buffer* read_file(const char* file) {                                                  
  FILE *f = fopen(file, "rb");
  fseek(f, 0, SEEK_END);
  long fsize = ftell(f);                                                                  
  fseek(f, 0, SEEK_SET);  //same as rewind(f);                                            

  void* data = malloc(fsize);                                                             
  fread(data, fsize, 1, f);
  fclose(f);

  TF_Buffer* buf = TF_NewBuffer();                                                        
  buf->data = data;
  buf->length = fsize;                                                                    
  buf->data_deallocator = free_buffer;                                                    
  return buf;
} 

int read_jpeg_file(char *filename, unsigned long *raw_image_ptr)
{
	struct jpeg_decompress_struct cinfo;
	struct jpeg_error_mgr jerr;

	JSAMPROW row_pointer[1];
	
	FILE *infile = fopen(filename, "rb");
	unsigned long location = 0;
	int i = 0;
	
	if (!infile)
	{
		printf("Error opening jpeg file %s\n!", filename);
		return -1;
	}

	cinfo.err = jpeg_std_error(&jerr);

	jpeg_create_decompress(&cinfo);
	jpeg_stdio_src(&cinfo, infile);
	jpeg_read_header(&cinfo, TRUE);
	jpeg_start_decompress(&cinfo);

	unsigned char *raw_image = (unsigned char *) malloc(cinfo.output_width * cinfo.output_height * cinfo.num_components);
    *raw_image_ptr = (unsigned long) &raw_image[0];

	row_pointer[0] = (unsigned char *) malloc(cinfo.output_width * cinfo.num_components);

	while(cinfo.output_scanline < cinfo.image_height)
    {
		jpeg_read_scanlines(&cinfo, row_pointer, 1);
		for(i=0; i < cinfo.image_width * cinfo.num_components; i++) 
		    raw_image[location++] = row_pointer[0][i];
	}

	jpeg_finish_decompress(&cinfo);
	jpeg_destroy_decompress(&cinfo);
	free(row_pointer[0]);
	fclose(infile);

	return 1;
}

int main(int argc, char* argv[]) {
    unsigned long *raw_image_ptr = (unsigned long *) malloc(sizeof(unsigned char *));
    read_jpeg_file("three.jpg", raw_image_ptr);
    unsigned char *raw_image = (unsigned char *) *raw_image_ptr;

    float input_image[28 * 28];

    for(int i = 0; i < 28 * 28; i++) {
        input_image[i] = (float) raw_image[i];
        input_image[i] /= 255.0F;
    }

    TF_Buffer* graph_def = read_file("./model/tf_model.pb");                      
    TF_Graph* graph = TF_NewGraph();

    TF_Status* status = TF_NewStatus();                                                     
    TF_ImportGraphDefOptions* opts = TF_NewImportGraphDefOptions();                         
    TF_GraphImportGraphDef(graph, graph_def, opts, status);
    TF_DeleteImportGraphDefOptions(opts);
    
    if (TF_GetCode(status) != TF_OK) {
        return 1;
    }

    TF_SessionOptions* sess_opts = TF_NewSessionOptions();
    TF_Session* session = TF_NewSession(graph, sess_opts, status);
    TF_DeleteSessionOptions(sess_opts);

    if (TF_GetCode(status) != TF_OK) {
        return 1;
    }

    TF_Output input, target, output;

    input.oper = TF_GraphOperationByName(graph, "conv2d_1_input");
    input.index = 0;
    output.oper = TF_GraphOperationByName(graph, "dense_2/Softmax");
    output.index = 0;

    // TF_Operation *init_op = TF_GraphOperationByName(graph, "init");
    // const TF_Operation* init_ops[1] = { init_op };
    // TF_SessionRun(session, NULL,
    //         NULL, NULL, 0,
    //         NULL, NULL, 0,
    //         init_ops, 1,
    //         NULL, status);

    // if (TF_GetCode(status) != TF_OK) {
    //     return 1;
    // }

    const int64_t dims[4] = {1, 28, 28, 1};
    const size_t nbytes = sizeof(float) * 28 * 28;
    TF_Tensor* t = TF_AllocateTensor(TF_FLOAT, dims, 4, nbytes);
    memcpy(TF_TensorData(t), &input_image, nbytes);

    TF_Output inputs[1] = { input };
    TF_Tensor* input_values[1] = {t};
    TF_Output outputs[1] = { output };
    TF_Tensor* output_values[1] = {NULL};

    TF_SessionRun(session, NULL, inputs, input_values, 1, outputs,
                    output_values, 1,
                    NULL, 0, NULL, status);
    TF_DeleteTensor(t);

    if (TF_GetCode(status) != TF_OK) {
        return 1;
    }

    float* predictions = (float*) malloc (sizeof(float) * 10);
    memcpy(predictions, TF_TensorData(output_values[0]), sizeof(float) * 10);
    TF_DeleteTensor(output_values[0]);

    for(int i = 0; i < 10; i++) {
        printf("class #%d prob = %f\n", i, predictions[i]);
    }

    free(predictions);

    TF_DeleteStatus(status);
    TF_DeleteBuffer(graph_def);                                                             
    TF_DeleteGraph(graph);                                                                  

    free(raw_image);
    free(raw_image_ptr);

    return 0;
}
