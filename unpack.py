import zipfile
import os
import shutil
from tqdm import tqdm

TARGET_DIRECTORY = "/home/u764/Downloads/output"


def main():
    archives = os.listdir(".")
    archives = [a for a in archives if a.lower().strip().endswith(".zip")]

    progress_bar = tqdm(total=len(archives))
    for archive in archives:
        font_name = os.path.splitext(archive)[0]
        extraction_dir = f"{TARGET_DIRECTORY}/{font_name}"
        with zipfile.ZipFile(archive, "r") as zip:
            zip.extractall(f"{extraction_dir}/")

        if os.path.isdir(f"{extraction_dir}/static"):
            files = os.listdir(f"{extraction_dir}/static")
            files = [f"{extraction_dir}/static/{f}" for f in files]
        else:
            files = os.listdir(extraction_dir)
            files = [f"{extraction_dir}/{f}" for f in files]

        files = [
            f
            for f in files
            if f.lower().strip().endswith(".otf") or f.lower().strip().endswith(".ttf")
        ]

        os.makedirs(f"{extraction_dir}_temp")
        for f in files:
            file_name = os.path.basename(f)
            os.rename(f, f"{extraction_dir}_temp/{file_name}")

        shutil.rmtree(f"{extraction_dir}")
        os.rename(f"{extraction_dir}_temp", f"{extraction_dir}")
        progress_bar.update()
    progress_bar.close()


if __name__ == "__main__":
    main()
