package com.progressoft;

import java.util.Collections;
import java.util.List;

public class Workflow {
    String id;
    Status status;

    public void setId(String id) {
        this.id = id;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public static class Action {
        private String group;
        private Status destination;

        public Action(String group, Status destination) {
            this.group = group;
            this.destination = destination;
        }

        public String getGroup() {
            return group;
        }

        public void setGroup(String group) {
            this.group = group;
        }

        public Status getDestination() {
            return destination;
        }

        public void setDestination(Status destination) {
            this.destination = destination;
        }
    }

    public enum Status {
        APPROVED("approved"),
        PENDING("pending", Collections.singletonList(new Action("checker", ))),
        INITIAL("initial", Collections.singletonList(new Action("maker", PENDING)));

        private String name;
        private List<Action> actions;

        private Status(String name, List<Action> actions) {
            this.name = name;
        }
    }
}
