package com.progressoft;

public class User {
    private String username;
    private String password;
    private boolean active;
    private Workflow workflow;

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public boolean isActive() {
        return active;
    }

    public Workflow getWorkflow() {
        return workflow;
    }

    public void setWorkflow(Workflow workflow) {
        this.workflow = workflow;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
