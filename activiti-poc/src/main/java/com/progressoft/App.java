package com.progressoft;

import org.camunda.bpm.engine.*;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.task.Task;

import java.sql.SQLException;
import java.util.*;

public class App {
    private static final List<User> userDatabase = new ArrayList<>();

    public static void main(String[] args) throws SQLException {
        //-----  bootstrap process engine
        ProcessEngine processEngine = ProcessEngineConfiguration.createStandaloneInMemProcessEngineConfiguration()
                .setDatabaseSchemaUpdate(ProcessEngineConfiguration.DB_SCHEMA_UPDATE_TRUE)
                .setJdbcUrl("jdbc:h2:mem:test")
                .setJdbcUsername("test")
                .setJdbcPassword("test")
                .buildProcessEngine();

        //----- load bpmn file
        RepositoryService repositoryService = processEngine.getRepositoryService();
        repositoryService
                .createDeployment()
                .addClasspathResource("process.bpmn")
                .deploy();

        //----- frontend form
        Scanner scanner = new Scanner(System.in);
        System.out.print("\nusername: ");
        String username = scanner.nextLine();
        System.out.print("\npassword: ");
        String password = scanner.nextLine();

        //----- start the workflow by creating user
        RuntimeService runtimeService = processEngine.getRuntimeService();
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("user_workflow");

        TaskService taskService = processEngine.getTaskService();
        List<Task> tasks = taskService.createTaskQuery()
                .processInstanceId(processInstance.getId())
                .taskCandidateGroup("maker")
                .list();

        Task createUserTask = tasks.get(0);

        Map<String, Object> userForm = new HashMap<>();
        userForm.put("username", username);
        userForm.put("password", password);

        taskService.complete(createUserTask.getId(), userForm);

        //----- save user to database
        User user = new User(username, password);
        user.setProcessInstance(processInstance);
        userDatabase.add(user);

        //----- approve user by checker
        taskService = processEngine.getTaskService();
        tasks = taskService.createTaskQuery()
                .processInstanceId(user.getProcessInstance().getId())
                .taskCandidateGroup("checker")
                .list();
        Task userApprovalTask = tasks.get(0);

        Map<String, Object> taskInfo = new HashMap<>();
        taskInfo.put("approved", false);
        taskService.complete(userApprovalTask.getId(), taskInfo);
    }
}
