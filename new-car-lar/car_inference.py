import cv2
import matplotlib.pyplot as plt
import numpy as np

from car_icr import build_lstm_model, predict_batch, pre_process

SETTINGS = {
    'car_box_start_point_percentages': (0.7135, 0.323),
    'car_box_size_percentages': (0.272, 0.113),
    'car_box_target_width': 200,
    'car_box_target_height': 36
}


def crop_area(background,
              start_point_percentages,
              size_percentages):
    x = start_point_percentages[0]
    y = start_point_percentages[1]
    w = size_percentages[0]
    h = size_percentages[1]

    x *= background.shape[1]
    y *= background.shape[0]
    w *= background.shape[1]
    h *= background.shape[0]

    x = int(round(x))
    y = int(round(y))
    w = int(round(w))
    h = int(round(h))

    return background.copy()[y: y + h, x:x + w]


def main():
    model, rnn_time_steps = build_lstm_model()
    model.load_weights('/home/u764/Downloads/fonts-downloads/car1/car_checkpoint_3_01_0.6974_1.0837.h5')

    image = cv2.imread('/home/u764/Downloads/POC-Cheques/final/doc6-1-final.bmp', cv2.IMREAD_GRAYSCALE)

    car_box = crop_area(image,
                        SETTINGS['car_box_start_point_percentages'],
                        SETTINGS['car_box_size_percentages'])
    car_box = cv2.resize(car_box,
                         (SETTINGS['car_box_target_width'],
                          SETTINGS['car_box_target_height']),
                         interpolation=cv2.INTER_CUBIC)
    labels, probabilities = predict_batch(model, np.array([pre_process(255 - car_box)]), rnn_time_steps)

    plt.imshow(car_box, plt.cm.gray)
    plt.title(f'label: {labels[0]}  -  confidence: {round(probabilities[0][0] * 100, 2)}')
    plt.show()


if __name__ == '__main__':
    main()
