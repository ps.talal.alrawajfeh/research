import gzip
import math
import os
import random
from enum import Enum

import cv2
import editdistance
import numpy as np
import tensorflow as tf

keras = tf.keras
from keras import Input, Model
from keras.callbacks import LambdaCallback, ModelCheckpoint
from keras.layers import Conv2D, MaxPooling2D, Dense, Reshape, LSTM, add, concatenate, BatchNormalization, Activation, \
    Conv2DTranspose
from keras.optimizers import Adam
from keras.utils import Sequence
from keras.activations import selu
from keras.initializers import LecunNormal
from tqdm import tqdm
import tensorflow as tf

SETTINGS = {
    'rnn_output_start_index': 2,
    'input_width': 1000,
    'input_height': 32,
    'max_label_length': 180,
    'classes': 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789,.#*-=/\| ',
    'lar_train_data_path': './data/train',
    'lar_validation_data_path': './data/validation',
    'lar_test_data_path': './data/test',
    'lar_embedding_dropout_rate': 0.5,
    'lar_training_batch_size': 32,
    'lar_evaluation_batch_size': 128,
    'lar_learning_rates_per_training_cycle': [
        0.001,
        0.0001,
        0.00001
    ],
    'lar_leaky_ReLU_alpha': 0.05,
    'lar_target_rnn_time_steps': 250,
    'model_file_name': 'lar',
    'epochs_per_training_cycle': 7
}

BLANK_CLASS = len(SETTINGS['classes'])

LABEL_CONCAT_CHAR = ''

TRAINING_CYCLES = len(SETTINGS['lar_learning_rates_per_training_cycle'])
TRAINING_BATCH_SIZE = SETTINGS['lar_training_batch_size']
EVALUATION_BATCH_SIZE = SETTINGS['lar_evaluation_batch_size']


class DataType(Enum):
    TRAIN_DATA = 1
    VALIDATION_DATA = 2
    TEST_DATA = 3


def label_to_classes(label):
    return [SETTINGS['classes'].find(l) for l in label]


def get_input_shape():
    return SETTINGS['input_height'], SETTINGS['input_width'], 1


def pre_process(image):
    return cv2.resize(image,
                      (SETTINGS['input_width'],
                       SETTINGS['input_height']),
                      interpolation=cv2.INTER_CUBIC).astype(np.float32).reshape(get_input_shape()) / 255.0


def read_compressed_image(file_path):
    with gzip.open(file_path, 'rb') as f:
        content = f.read()
    content_array = np.asarray(bytearray(content), dtype=np.uint8)
    return cv2.imdecode(content_array, cv2.IMREAD_GRAYSCALE)


def ctc_loss(y_true, y_pred):
    logits = y_pred[:, SETTINGS['rnn_output_start_index']:, :]
    input_length = tf.repeat(SETTINGS['lar_target_rnn_time_steps'] - SETTINGS['rnn_output_start_index'],
                             tf.shape(y_true)[0])
    label_length = tf.math.count_nonzero(y_true, axis=-1)
    return tf.reduce_mean(tf.nn.ctc_loss(y_true,
                                         logits,
                                         label_length,
                                         input_length,
                                         logits_time_major=False,
                                         blank_index=BLANK_CLASS))


class DataGenerator(Sequence):
    def __init__(self,
                 input_image_paths,
                 labels,
                 batch_size):
        count = len(input_image_paths)
        self.count = count
        self.batch_size = batch_size

        indices = list(range(count))
        random.shuffle(indices)
        self.input_image_paths = [input_image_paths[i] for i in indices]
        self.labels = [labels[i] for i in indices]

    def __getitem__(self, index):
        input_image_paths = self.input_image_paths[index * self.batch_size: (index + 1) * self.batch_size]
        input_images = [pre_process(read_compressed_image(f)) for f in input_image_paths]

        output_labels = self.labels[index * self.batch_size: (index + 1) * self.batch_size]
        output_labels = [np.array(label_to_classes(label), np.uint8) for label in output_labels]

        output_labels = np.array([np.append(y, [BLANK_CLASS] * (SETTINGS['max_label_length'] - len(y)))
                                  for y in output_labels],
                                 dtype=np.uint8)
        return np.array(input_images), output_labels

    def __len__(self):
        return int(math.ceil(self.count / self.batch_size))

    def on_epoch_end(self):
        indices = list(range(self.count))
        random.shuffle(indices)
        self.input_image_paths = [self.input_image_paths[i] for i in indices]
        self.labels = [self.labels[i] for i in indices]


def path_from_data_type(data_type):
    if data_type == DataType.TRAIN_DATA:
        path = SETTINGS['lar_train_data_path']
    elif data_type == DataType.VALIDATION_DATA:
        path = SETTINGS['lar_validation_data_path']
    else:
        path = SETTINGS['lar_test_data_path']
    return path


def stacked_convolutions(filters, kernel_size, input_layer, convolutions=2, padding='same'):
    convolution = input_layer
    for i in range(convolutions):
        convolution = Conv2D(filters,
                             (kernel_size, kernel_size),
                             padding=padding,
                             kernel_initializer=LecunNormal())(convolution)
        convolution = BatchNormalization()(convolution)
        convolution = Activation(selu)(convolution)

    return convolution


def deconv_batch_norm_relu_layer(input_layer, filters, kernel=(3, 3)):
    conv = Conv2DTranspose(filters,
                           kernel,
                           strides=(2, 2),
                           kernel_initializer=LecunNormal(),
                           use_bias=False,
                           padding='same')(input_layer)
    conv = BatchNormalization()(conv)
    conv = Activation(selu)(conv)
    return conv


def build_model():
    input_shape = get_input_shape()
    input_data = Input(name='input_data',
                       shape=input_shape)

    embedding = BatchNormalization()(input_data)

    # filters = [32, 64, 128, 256]
    # unet_convolutions = []
    # encoder = embedding
    # for i in range(len(filters) - 1):
    #     conv = stacked_convolutions(filters[i], 3, encoder)
    #     encoder = MaxPooling2D(pool_size=(2, 2))(conv)
    #     unet_convolutions.append(conv)
    # # bottleneck
    # conv = stacked_convolutions(filters[-1], 2, encoder)
    # # decoder
    # decoder = conv
    # for i in range(len(filters) - 2, -1, -1):
    #     up = tf.keras.layers.Concatenate(axis=-1)(
    #         [deconv_batch_norm_relu_layer(decoder, filters[i], (3, 3)), unet_convolutions[i]])
    #     decoder = stacked_convolutions(filters[i], 3, up)
    # unet_output = Conv2D(1,
    #                      (1, 1),
    #                      kernel_initializer=LecunNormal(),
    #                      use_bias=True,
    #                      padding='same',
    #                      activation='sigmoid')(decoder)

    embedding = stacked_convolutions(32, 3, embedding)
    embedding = MaxPooling2D(pool_size=(2, 2))(embedding)

    embedding = stacked_convolutions(64, 3, embedding)
    embedding = MaxPooling2D(pool_size=(2, 2))(embedding)

    embedding = stacked_convolutions(128, 3, embedding)
    embedding = MaxPooling2D(pool_size=(2, 1))(embedding)

    embedding = stacked_convolutions(256, 3, embedding)
    embedding = MaxPooling2D(pool_size=(2, 1))(embedding)

    embedding = stacked_convolutions(512, 3, embedding)
    embedding = MaxPooling2D(pool_size=(2, 1))(embedding)

    embedding = Reshape(target_shape=(SETTINGS['lar_target_rnn_time_steps'], 512))(embedding)

    embedding = Dense(512, kernel_initializer=LecunNormal())(embedding)
    embedding = BatchNormalization()(embedding)
    # embedding = Dropout(SETTINGS['lar_embedding_dropout_rate'])(embedding)
    embedding = Activation(selu, name='embedding')(embedding)

    rnn_size = 512
    lstm1 = LSTM(rnn_size,
                 return_sequences=True,
                 kernel_initializer=LecunNormal(),
                 name='lstm1')(embedding)

    backward_lstm1 = LSTM(rnn_size,
                          return_sequences=True,
                          go_backwards=True,
                          kernel_initializer=LecunNormal(),
                          name='backward_lstm1')(embedding)

    lstm1_merged = add([lstm1, backward_lstm1])

    lstm2 = LSTM(rnn_size,
                 return_sequences=True,
                 kernel_initializer=LecunNormal(),
                 name='lstm2')(lstm1_merged)

    backward_lstm2 = LSTM(rnn_size,
                          return_sequences=True,
                          go_backwards=True,
                          kernel_initializer=LecunNormal(),
                          name='backward_lstm2')(lstm1_merged)

    # the additional class is reserved for the ctc loss blank character
    classes = len(SETTINGS['classes']) + 1

    # the output of this layer is required for the tensorflow ctc_beam_search_decoder
    logits_layer = Dense(classes,
                         kernel_initializer=LecunNormal(),
                         name='logits')(concatenate([lstm2, backward_lstm2]))

    return Model(inputs=[input_data],
                 outputs=logits_layer)


def update_report(line='', report_file=SETTINGS['model_file_name'] + '_report.txt'):
    with open(report_file, 'a') as f:
        f.write(line + '\n')


def load_images_paths_and_labels(data_type=DataType.TRAIN_DATA):
    path = path_from_data_type(data_type)
    files = os.listdir(path)

    image_files = [f for f in files if f.endswith('.png')]

    image_paths = []
    labels = []
    for image_file in image_files:
        image_file_path = os.path.join(path, image_file)
        if not os.path.isfile(image_file_path):
            continue
        file_name = os.path.splitext(image_file)[0]
        label_file_path = os.path.join(path, f'{file_name}.txt')
        if not os.path.isfile(label_file_path):
            continue
        image_paths.append(image_file_path)
        with open(label_file_path, 'r') as file:
            label = file.read()
        labels.append(label.strip())
    return image_paths, labels


def train(model, cycle_number):
    train_files, train_labels = load_images_paths_and_labels(DataType.TRAIN_DATA)
    validation_files, validation_labels = load_images_paths_and_labels(DataType.TRAIN_DATA)

    all_models_checkpoint = ModelCheckpoint(f'{SETTINGS["model_file_name"]}_checkpoint_{cycle_number}' +
                                            '_{epoch:02d}_{loss:0.4f}_{val_loss:0.4f}.h5')
    best_model_checkpoint = ModelCheckpoint(f'{SETTINGS["model_file_name"]}_checkpoint_best_{cycle_number}.h5',
                                            monitor='val_loss',
                                            save_best_only=True,
                                            mode='min')

    update_report('======= training results =======')

    update_report_callback = LambdaCallback(on_epoch_end=lambda epoch, logs: update_report(
        f'cycle: {cycle_number} - epoch: {epoch} - loss: {logs["loss"]} - validation loss: {logs["val_loss"]}'
    ))

    model.fit(DataGenerator(train_files,
                            train_labels,
                            TRAINING_BATCH_SIZE),
              validation_data=DataGenerator(validation_files,
                                            validation_labels,
                                            EVALUATION_BATCH_SIZE),
              epochs=SETTINGS['epochs_per_training_cycle'],
              callbacks=[all_models_checkpoint,
                         best_model_checkpoint,
                         update_report_callback])

    update_report()


def predict_batch(model, images, greedy=False, merge_repeated=False):
    input_tensor = np.array([pre_process(image) for image in images])
    predictions = model.predict(input_tensor)[:, 2:, :]

    time_steps = SETTINGS['lar_target_rnn_time_steps'] - 2

    decoded_batch = []
    probabilities = []
    for prediction in predictions:
        if greedy:
            softmax = tf.nn.softmax(prediction)
            decoded = tf.argmax(softmax, axis=-1)
            max_prob = tf.math.reduce_max(softmax, axis=-1)
            max_prob_log = tf.math.log(max_prob + 1e-7)
            prob = tf.reduce_sum(max_prob_log).numpy()
            prob = np.exp(prob)
            probabilities.append(prob)
            decoded_batch.append(decoded.numpy().tolist())
        else:
            decoded, log_prob = tf.nn.ctc_beam_search_decoder(
                prediction.reshape((time_steps, 1, len(SETTINGS['classes']) + 1)),
                [time_steps])
            probabilities.append(np.exp(log_prob[0]))
            decoded_batch.append(tf.sparse.to_dense(decoded[0]).numpy()[0].tolist())

    labels = []
    for seq in decoded_batch:
        print(seq)
        if merge_repeated:
            start_i = 0
            merged = [seq[start_i]]
            for i in range(1, len(seq)):
                if seq[i] != seq[start_i]:
                    merged.append(seq[i])
                    start_i = i
            labels.append(LABEL_CONCAT_CHAR.join([SETTINGS['classes'][c] for c in merged if c != BLANK_CLASS]).strip())
        else:
            labels.append(LABEL_CONCAT_CHAR.join([SETTINGS['classes'][c] for c in seq if c != BLANK_CLASS]).strip())

    return labels, probabilities


def evaluate(model):
    test_files, test_labels = load_images_paths_and_labels(DataType.TEST_DATA)

    update_report('======= evaluation results =======')

    results = model.evaluate_generator(DataGenerator(test_files,
                                                     test_labels,
                                                     EVALUATION_BATCH_SIZE))

    update_report(f'test loss: {results}')

    evaluate_model(model, test_files, test_labels)


def evaluate_model(model, test_files, test_labels):
    distances_mean = 0.0
    distance_ratios_mean = 0.0
    amount_overall_accuracy = 0

    test_data_generator = DataGenerator(test_files,
                                        test_labels,
                                        EVALUATION_BATCH_SIZE)
    batches = len(test_data_generator)
    progress_bar = tqdm(total=batches)

    n = 0
    for i in range(batches):
        image_batch, labels_batch = test_data_generator[i]
        predictions, probabilities = predict_batch(model, image_batch)

        for j in range(len(predictions)):
            predicted = predictions[j]
            label = labels_batch[j]

            edit_distance = editdistance.eval(predicted, label)
            distances_mean += edit_distance
            distance_ratios_mean += edit_distance / len(label)

            if label == predicted:
                amount_overall_accuracy += 1

        n += len(labels_batch)
        progress_bar.update()

    progress_bar.close()

    distances_mean /= n
    distance_ratios_mean /= n
    amount_overall_accuracy /= n

    update_report(f'average edit distances: {distances_mean}')
    update_report(f'character recognition error rate: {distance_ratios_mean}')
    update_report(f'character recognition accuracy: {100 - distance_ratios_mean * 100}')
    update_report(f'label recognition error rate: {1 - amount_overall_accuracy}')
    update_report(f'label recognition accuracy: {amount_overall_accuracy * 100}')

    return distances_mean, distance_ratios_mean, amount_overall_accuracy


def run_pipeline():
    for cycle in range(1, TRAINING_CYCLES + 1):
        keras.backend.clear_session()

        print(f'cycle: {cycle}')
        model = build_model()
        if cycle > 1:
            model.load_weights(f'{SETTINGS["model_file_name"]}_checkpoint_best_{cycle - 1}.h5')

        model.compile(loss=ctc_loss,
                      optimizer=Adam(learning_rate=SETTINGS['lar_learning_rates_per_training_cycle'][cycle],
                                     clipnorm=4.0))

        print('\ntraining model...\n')
        train(model, cycle)

    keras.backend.clear_session()
    model = build_model()
    model.load_weights(f'{SETTINGS["model_file_name"]}_checkpoint_best_{TRAINING_CYCLES}.h5')
    print('\nevaluating model...\n')
    evaluate(model)


if __name__ == '__main__':
    run_pipeline()
