import math
import multiprocessing
import os
import random
import time
import traceback
from concurrent.futures import ThreadPoolExecutor

import cv2
import numpy as np
from num2words import num2words
from tqdm import tqdm

from image_utils import InfiniteSampler, is_image, crop_background_area, \
    alter_image_brightness_and_contrast_randomly, to_int, change_foreground_color_and_blend_foreground_on_background, \
    insert_foreground, randomize_foreground, random_add_gaussian_noise, \
    speckle_image, random_blur, compress_image, print_text_cropped, fade_foreground_on_background_while_preserving_color

SETTINGS = {
    'lar_bank_notes_currency': [
        'dinar',
        'JOD',
        'dinar',
        'BHD',
        'dollar',
        'USD',
        'euro',
        'EUR',
        'yen',
        'JPY',
        'pound',
        'GBP',
        'franc',
        'CHF',
        'dirham',
        'AED',
        'rial',
        'YER',
        'OMR',
        'riyal',
        'SAR',
        'naira',
        'NGN'],
    'lar_bank_notes_fractional_currency': ['piastre', 'fils', 'qirsh', 'cent', 'penny', 'rappen', 'sen', 'kobo'],
    'lar_character_classes': 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789,.#*-=/\\| ',
    'lar_stop_words': ['only'],
    'lar_min_bank_notes_fraction': 0,
    'lar_max_bank_notes_fraction': 999,
    'lar_line_initial_width': 1000,
    'lar_line_initial_height': 64,
    'lar_line_target_width': 512,
    'lar_line_target_height': 32,
    'lar_bank_notes_numbers_percentage': 0.1,
    'lar_bank_notes_fractions_numbers_percentage': 0.25,
    'lar_templates_path': './PS-Templates',
    'lar_handwritten_fonts_path': './fonts/handwritten',
    'lar_printed_fonts_path': './fonts/printed',
    'lar_generated_data_path': './data',
    'lar_training_data_size': 500000,
    'lar_validation_data_size': 100000,
    'lar_test_data_size': 100000,
    'lar_cheque_area_error_percentage': 0.01,
    'lar_line1_start_point_percentages': (0.0267, 0.286),
    'lar_line1_size_percentages': (0.667, 0.09143),
    'lar_line2_start_point_percentages': (0.0267, 0.357),
    'lar_line2_size_percentages': (0.667, 0.09143),
    'lar_random_sequence_max_length': 80,
    'lar_min_target_character_width': 4,
    'lar_min_font_size': 20,
    'lar_max_font_size': 44,
}


def random_bool():
    return random.choice((True, False))


def make_currency_plural(currency):
    if currency[-1] == 's':
        return currency
    if currency[-1] == 'y':
        currency = currency[:-1] + 'ies'
    else:
        currency += 's'
    return currency


def random_make_currency_plural(currency):
    if random_bool():
        currency = make_currency_plural(currency)
    return currency


def random_bank_notes_currency():
    currency = random.choice(SETTINGS['lar_bank_notes_currency'])
    return random_make_currency_plural(currency)


def random_bank_notes_fractional_currency():
    currency = random.choice(SETTINGS['lar_bank_notes_fractional_currency'])
    return random_make_currency_plural(currency)


def random_number_to_lar_label():
    while True:
        intervals = [(0, 9999),
                     (10000, 99999),
                     (100000, 999999),
                     (1000000, 9999999)]

        interval = random.choice(intervals)
        number = random.randint(interval[0], interval[1])

        if random.uniform(0, 1) < SETTINGS['lar_bank_notes_numbers_percentage']:
            label = str(number)
        else:
            label = num2words(number)
            if random_bool():
                label = label.replace('-', ' ').replace(',', '')

        if random_bool():
            if random_bool():
                label += ' ' + random_bank_notes_currency()
            if random_bool():
                label += ' ' + random.choice(SETTINGS['lar_stop_words'])
            if random_bool():
                if random_bool():
                    conjunction = ','
                else:
                    conjunction = ''
                if random_bool():
                    conjunction += ' and no '
                else:
                    conjunction += ' and zero '
                label += conjunction + random_bank_notes_fractional_currency()
        else:
            label += ' ' + random_bank_notes_currency()
            if random_bool():
                label += ' ' + random.choice(SETTINGS['lar_stop_words'])
            if random_bool():
                if random_bool():
                    label += ','
                label += ' and'

            fraction = random.randint(SETTINGS['lar_min_bank_notes_fraction'],
                                      SETTINGS['lar_max_bank_notes_fraction'])
            if random.uniform(0, 1) < SETTINGS['lar_bank_notes_fractions_numbers_percentage']:
                fraction_type = random.choice([1, 2, 3])
                if fraction_type == 1:
                    label += ' ' + str(fraction)
                elif fraction_type == 2:
                    label += ' 0.' + str(fraction)
                else:
                    slash_choice = random.choice(['/', '\\'])
                    label += ' ' + str(fraction) + f'{slash_choice}1' + '0' * len(str(fraction))
            else:
                if random_bool():
                    fraction_words = num2words(fraction).replace('-', ' ').replace(',', '')
                else:
                    fraction_words = num2words(fraction)
                label += ' ' + fraction_words
            if random_bool():
                label += ' ' + random_bank_notes_fractional_currency()
            if random_bool():
                label += ' ' + random.choice(SETTINGS['lar_stop_words'])

        capitalization_mode = random.choice([1, 2, 3, 4])
        if capitalization_mode == 1:
            label = label.upper()
        elif capitalization_mode == 2:
            label = label.lower()
        elif capitalization_mode == 3:
            label = ' '.join([word[:1].upper() + word[1:] for word in label.split(' ')])
        else:
            label = ''.join([x.upper() if random_bool() else x for x in label])

        if random_bool():
            label += '.'

        if random_bool():
            enclosing_choice = random.choice(['*', '-', '=', '#'])
            left_enclosing = enclosing_choice * random.randint(1, 5)
            if random_bool():
                left_enclosing += random.choice(['/', '\\', '|'])
            if random_bool():
                label = left_enclosing + ' ' + label
            else:
                label = left_enclosing + label

        if random_bool():
            right_enclosing = ''
            if random_bool():
                right_enclosing += random.choice(['/', '\\', '|'])
            enclosing_choice = random.choice(['*', '-', '=', '#'])
            right_enclosing += enclosing_choice * random.randint(1, 5)
            if random_bool():
                label += ' ' + right_enclosing
            else:
                label += right_enclosing

        if 2 * SETTINGS['lar_line_target_width'] / len(label) >= SETTINGS['lar_min_target_character_width']:
            break

    return label


def draw_printed_text_on_background(text,
                                    background,
                                    font_supplier):
    if text.strip() == '':
        return

    background_mean = np.mean(background)
    background_std = np.std(background)
    color_upper_limit = max(min(to_int(background_mean - 2 * background_std), to_int(background_mean - 20)), 0)
    color_lower_limit = random.randint(0, max(min(to_int(background_mean - 3 * background_std),
                                                  to_int(background_mean - 30)), 0))

    foreground_color_type = random.choice([1, 2, 3, 4, 5])

    if foreground_color_type == 3:
        foreground_color = 0
    else:
        foreground_color = random.randint(color_lower_limit, color_upper_limit)

    min_character_width = int(
        math.ceil(SETTINGS['lar_min_target_character_width'] * SETTINGS['lar_line_initial_width']
                  / SETTINGS['lar_line_target_width']))

    foreground_image = print_text_cropped(text,
                                          random.choice(font_supplier.next()),
                                          random.randint(SETTINGS['lar_min_font_size'],
                                                         SETTINGS['lar_max_font_size']),
                                          foreground_color,
                                          (SETTINGS['lar_line_initial_width'],
                                           SETTINGS['lar_line_initial_height']),
                                          SETTINGS['lar_max_font_size'],
                                          min_character_width=min_character_width)

    x = random.randint(0, background.shape[1] - foreground_image.shape[1])
    y = random.randint(0, background.shape[0] - foreground_image.shape[0])

    if foreground_color_type == 1:
        fade_foreground_on_background_while_preserving_color(background,
                                                             foreground_image,
                                                             (x, y),
                                                             random.uniform(0.6, 0.8))
    elif foreground_color_type == 2:
        change_foreground_color_and_blend_foreground_on_background(background,
                                                                   foreground_image,
                                                                   (x, y),
                                                                   foreground_color,
                                                                   random.uniform(0.6, 0.8))
    elif foreground_color_type == 3:
        foreground_image = randomize_foreground(foreground_image, color_lower_limit, color_upper_limit)
        insert_foreground(background,
                          foreground_image,
                          (x, y))
    elif foreground_color_type == 4:
        foreground_image[foreground_image < 255] = foreground_color
        insert_foreground(background,
                          foreground_image,
                          (x, y))
    else:
        foreground_image[foreground_image < 255] = foreground_color
        change_foreground_color_and_blend_foreground_on_background(background,
                                                                   foreground_image,
                                                                   (x, y),
                                                                   foreground_color,
                                                                   random.uniform(0.6, 0.8))


def apply_random_effects(image):
    if random_bool():
        if random_bool():
            image = random_add_gaussian_noise(image,
                                              0,
                                              random.randint(10, 20),
                                              (-20, 20))
        else:
            image = speckle_image(image, 0.01, random.randint(0, 255))

        if random_bool():
            image = random_blur(image)
        else:
            image = compress_image(image, random.randint(50, 100))
    return image


def generate_sample(template_sampler,
                    font_suppliers):
    while True:
        try:
            template = cv2.imread(template_sampler.next(), cv2.IMREAD_GRAYSCALE)

            if random.uniform(0, 1) < 0.2:
                template = np.ones(template.shape, np.uint8) * 255

            lar_line1_background = crop_background_area(template,
                                                        SETTINGS['lar_line1_start_point_percentages'],
                                                        SETTINGS['lar_line1_size_percentages'],
                                                        SETTINGS['lar_cheque_area_error_percentage'])

            lar_line2_background = crop_background_area(template,
                                                        SETTINGS['lar_line2_start_point_percentages'],
                                                        SETTINGS['lar_line2_size_percentages'],
                                                        SETTINGS['lar_cheque_area_error_percentage'])

            lar_line1_background = cv2.resize(lar_line1_background,
                                              (SETTINGS['lar_line_initial_width'], SETTINGS['lar_line_initial_height']),
                                              interpolation=cv2.INTER_CUBIC)
            lar_line1_background = alter_image_brightness_and_contrast_randomly(lar_line1_background)

            lar_line2_background = cv2.resize(lar_line2_background,
                                              (SETTINGS['lar_line_initial_width'], SETTINGS['lar_line_initial_height']),
                                              interpolation=cv2.INTER_CUBIC)
            lar_line2_background = alter_image_brightness_and_contrast_randomly(lar_line2_background)

            prob = random.uniform(0, 1)
            if prob <= 0.8:
                lar_label = random_number_to_lar_label()
            else:
                lar_label = ''
                for i in range(random.randint(1, SETTINGS['lar_random_sequence_max_length'] * 3 // 4)):
                    lar_label += random.choice(SETTINGS['lar_character_classes'])

            max_characters_per_line = int(
                math.floor(SETTINGS['lar_line_target_width'] / SETTINGS['lar_min_target_character_width']))

            if len(lar_label) > max_characters_per_line:
                line2_label_index = random.randint(len(lar_label) - max_characters_per_line * 3 // 4,
                                                   max_characters_per_line * 3 // 4)

                i = lar_label.find(' ', line2_label_index)
                if i == -1:
                    i = lar_label.rfind(' ')
                if i == -1:
                    i = line2_label_index
                line2_label_index = i

                line1_label = lar_label[:line2_label_index]
                line2_label = lar_label[line2_label_index:]
            else:
                if random_bool():
                    line1_label = ''
                    line2_label = lar_label
                else:
                    line1_label = lar_label
                    line2_label = ''

            line1_label = ''.join([l for l in line1_label if l in SETTINGS['lar_character_classes']]).strip()
            line2_label = ''.join([l for l in line2_label if l in SETTINGS['lar_character_classes']]).strip()

            font_supplier = random.choice(font_suppliers)
            draw_printed_text_on_background(line1_label, lar_line1_background, font_supplier)
            draw_printed_text_on_background(line2_label, lar_line2_background, font_supplier)
            lar_line1_background = apply_random_effects(lar_line1_background)
            lar_line2_background = apply_random_effects(lar_line2_background)
            lar_line1_background = cv2.resize(lar_line1_background,
                                              (SETTINGS['lar_line_target_width'], SETTINGS['lar_line_target_height']),
                                              interpolation=cv2.INTER_CUBIC)
            lar_line2_background = cv2.resize(lar_line2_background,
                                              (SETTINGS['lar_line_target_width'], SETTINGS['lar_line_target_height']),
                                              interpolation=cv2.INTER_CUBIC)

            return (lar_line1_background, line1_label), (lar_line2_background, line2_label)
        except Exception as e:
            print(f"An exception occurred: {str(e)}")
            traceback.print_exc()


def load_font_paths(path):
    font_paths = os.listdir(path)
    font_paths = [f for f in font_paths if f.lower().endswith('.ttf') or f.lower().endswith('.otf')]
    font_paths = [os.path.join(path, f) for f in font_paths]
    return font_paths


def parallel_data_generation(output_data_count,
                             start_index,
                             progress_bar,
                             generation_path,
                             data_generation_function,
                             *function_args):
    progress_lock = multiprocessing.Lock()
    indices = list(range(start_index, start_index + output_data_count))
    random.shuffle(indices)

    if not os.path.isdir(generation_path):
        os.makedirs(generation_path)

    def wrapped_data_generation_function(*args):
        (line1_image, line1_label), (line2_image, line2_label) = data_generation_function(*args)

        with progress_lock:
            current_index = indices.pop()

        cv2.imwrite(os.path.join(generation_path, f'{current_index}_1.png'), 255 - line1_image)
        with open(os.path.join(generation_path, f'{current_index}_1.txt'), 'w') as f:
            f.write(line1_label)

        cv2.imwrite(os.path.join(generation_path, f'{current_index}_2.png'), 255 - line2_image)
        with open(os.path.join(generation_path, f'{current_index}_2.txt'), 'w') as f:
            f.write(line2_label)

        with progress_lock:
            progress_bar.update()

    while len(indices) > 0:
        with ThreadPoolExecutor(max_workers=multiprocessing.cpu_count() * 2) as executor:
            for _ in range(len(indices)):
                executor.submit(wrapped_data_generation_function,
                                *function_args)
        time.sleep(1)


def load_font_families(path):
    families_dirs = os.listdir(path)
    families = []
    for f in families_dirs:
        family_path = os.path.join(path, f)
        if not os.path.isdir(family_path):
            continue
        families.append(load_font_paths(family_path))
    return families


def main():
    paths = os.listdir(SETTINGS['lar_templates_path'])
    paths = [os.path.join(SETTINGS['lar_templates_path'], p) for p in paths]
    paths = [p for p in paths if is_image(p)]
    template_sampler = InfiniteSampler(paths)

    font_supplier1 = InfiniteSampler(load_font_families(SETTINGS['lar_handwritten_fonts_path']))
    font_supplier2 = InfiniteSampler(load_font_families(SETTINGS['lar_printed_fonts_path']))

    progress_bar = tqdm(total=SETTINGS['lar_training_data_size'])
    parallel_data_generation(SETTINGS['lar_training_data_size'],
                             1,
                             progress_bar,
                             os.path.join(SETTINGS['lar_generated_data_path'], 'train'),
                             generate_sample,
                             template_sampler,
                             [font_supplier1, font_supplier2])
    progress_bar.close()

    progress_bar = tqdm(total=SETTINGS['lar_validation_data_size'])
    parallel_data_generation(SETTINGS['lar_validation_data_size'],
                             1,
                             progress_bar,
                             os.path.join(SETTINGS['lar_generated_data_path'], 'validation'),
                             generate_sample,
                             template_sampler,
                             [font_supplier1, font_supplier2])
    progress_bar.close()

    progress_bar = tqdm(total=SETTINGS['lar_test_data_size'])
    parallel_data_generation(SETTINGS['lar_test_data_size'],
                             1,
                             progress_bar,
                             os.path.join(SETTINGS['lar_generated_data_path'], 'test'),
                             generate_sample,
                             template_sampler,
                             [font_supplier1, font_supplier2])
    progress_bar.close()


if __name__ == '__main__':
    main()
