import os

import cv2
import numpy as np
from matplotlib import pyplot as plt

from image_utils import remove_white_border_non_binary, print_text_cropped
from sequence_generator import SETTINGS, number_to_words, make_currency_plural, load_emnist_classes


def calculate_longest_lar_label():
    longest_currency = ''
    max_length = 0
    for i in range(len(SETTINGS['lar_bank_notes_currency'])):
        currency = SETTINGS['lar_bank_notes_currency'][i]
        if len(currency) > max_length:
            max_length = len(currency)
            longest_currency = currency

    longest_fractional_currency = ''
    max_length = 0
    for i in range(len(SETTINGS['lar_bank_notes_fractional_currency'])):
        currency = SETTINGS['lar_bank_notes_fractional_currency'][i]
        if len(currency) > max_length:
            max_length = len(currency)
            longest_fractional_currency = currency

    longest_bank_notes_number = ''
    max_length = 0
    for i in range(10 ** (SETTINGS['lar_min_bank_notes_digits'] - 1), 10 ** (SETTINGS['lar_max_bank_notes_digits'])):
        bank_notes_number = number_to_words(i)
        if len(bank_notes_number) > max_length:
            max_length = len(bank_notes_number)
            longest_bank_notes_number = bank_notes_number

    longest_fractional_number = ''
    max_length = 0
    for i in range(SETTINGS['lar_min_bank_notes_fraction'], SETTINGS['lar_max_bank_notes_fraction'] + 1):
        bank_notes_fraction = number_to_words(i)
        if len(bank_notes_fraction) > max_length:
            max_length = len(bank_notes_fraction)
            longest_fractional_number = bank_notes_fraction

    longest_lar_label = f'{longest_bank_notes_number} {make_currency_plural(longest_currency)} only and ' \
                        f'{longest_fractional_number} {make_currency_plural(longest_fractional_currency)} only'

    print(longest_lar_label)
    print(len(longest_lar_label))  # it is 149


def calculate_emnist_image_statistics():
    data_classes = load_emnist_classes()[0]
    widths = []
    heights = []
    for c in data_classes:
        for p in data_classes[c]:
            image = remove_white_border_non_binary(cv2.imread(p, cv2.IMREAD_GRAYSCALE))
            widths.append(image.shape[1])
            heights.append(image.shape[0])

    print(f'min width: {np.min(widths)}')
    print(f'mean width: {np.mean(widths)}')
    print(f'std width {np.std(widths)}')
    print(f'max width: {np.max(widths)}')

    print(f'min height: {np.min(heights)}')
    print(f'mean height: {np.mean(heights)}')
    print(f'std height {np.std(heights)}')
    print(f'max height: {np.max(heights)}')


def visualize_fonts():
    font_paths = os.listdir(SETTINGS['lar_handwritten_fonts_path'])
    font_paths = [f for f in font_paths if f.endswith('.ttf') or f.endswith('.otf')]
    font_paths = [os.path.join(SETTINGS['lar_handwritten_fonts_path'], f) for f in font_paths]

    min_font_size = SETTINGS['lar_min_font_size']
    max_font_size = SETTINGS['lar_max_font_size']

    for path in font_paths:
        print(path)
        n = (max_font_size - min_font_size) // 2 + 1
        fig, axs = plt.subplots(n, 1)
        for i in range(min_font_size, max_font_size + 1, 2):
            axs_index = (i - min_font_size) // 2
            axs[axs_index].imshow(
                print_text_cropped("seventy three dollars only and three hundred and seventy three piastres only",
                                   path,
                                   i,
                                   0,
                                   (SETTINGS['lar_line_width'], SETTINGS['lar_line_height']),
                                   SETTINGS['lar_max_font_size']),
                plt.cm.gray)
        plt.show()
