import mimetypes
import os
import random

import cv2
import numpy as np
from PIL import Image, ImageDraw, ImageFont
from scipy.interpolate import interp1d


def get_extensions_for_type(general_type):
    for ext in mimetypes.types_map:
        if mimetypes.types_map[ext].split('/')[0] == general_type:
            yield ext.lower()


def memoize(function):
    memo = {}

    def wrapper(*args):
        if args in memo:
            return memo[args]
        else:
            rv = function(*args)
            memo[args] = rv
            return rv

    return wrapper


@memoize
def get_image_extensions():
    return tuple(get_extensions_for_type('image'))


def file_extension(path):
    return os.path.splitext(os.path.basename(path))[1].lower()


def is_image(path):
    return file_extension(path) in get_image_extensions()


def to_int(number):
    return int(round(number))


def rotate(image, angle):
    (h, w) = image.shape
    center_x, center_y = w / 2, h / 2

    rotation_matrix = cv2.getRotationMatrix2D((center_x, center_y),
                                              angle,
                                              1.0)
    cos = np.abs(rotation_matrix[0, 0])
    sin = np.abs(rotation_matrix[0, 1])

    new_w = h * sin + w * cos
    new_h = h * cos + w * sin

    rotation_matrix[0, 2] += new_w / 2 - center_x
    rotation_matrix[1, 2] += new_h / 2 - center_y

    return cv2.warpAffine(image,
                          rotation_matrix,
                          (to_int(new_w), to_int(new_h)),
                          borderValue=(255),
                          flags=cv2.INTER_CUBIC)


def threshold_otsu(image):
    return cv2.threshold(image,
                         0,
                         255,
                         cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]


def remove_white_border_non_binary(image):
    binary_image = threshold_otsu(image)

    mask = (255 - binary_image) > 0

    height, width = binary_image.shape[0:2]
    mask1, mask2 = mask.any(0), mask.any(1)
    x1, x2 = mask1.argmax(), width - mask1[::-1].argmax()
    y1, y2 = mask2.argmax(), height - mask2[::-1].argmax()

    return image[y1:y2, x1:x2]


def random_bool():
    return random.choice((True, False))


def crop_background_area(background,
                         start_point_percentages,
                         size_percentages,
                         error_percentage):
    x = start_point_percentages[0]
    y = start_point_percentages[1]
    w = size_percentages[0]
    h = size_percentages[1]

    if random_bool():
        x += random.uniform(-error_percentage, error_percentage)
        y += random.uniform(-error_percentage, error_percentage)
        w += random.uniform(-error_percentage, error_percentage)
        h += random.uniform(-error_percentage, error_percentage)

    x *= background.shape[1]
    y *= background.shape[0]
    w *= background.shape[1]
    h *= background.shape[0]

    x = int(round(x))
    y = int(round(y))
    w = int(round(w))
    h = int(round(h))

    return background.copy()[y: y + h, x:x + w]


class InfiniteSampler:
    def __init__(self, items):
        self.items = items
        self.indices = self._get_random_indices()

    def next(self):
        if len(self.indices) == 0:
            self.indices = self._get_random_indices()
        index = self.indices.pop()
        return self.items[index]

    def _get_random_indices(self):
        indices = list(range(len(self.items)))
        random.shuffle(indices)
        return indices


def correct_image_values(image):
    image[image > 255] = 255
    image[image < 0] = 0


def change_brightness(image, delta):
    result = image.astype(np.int32)
    result += delta
    correct_image_values(result)
    return result.astype(np.uint8)


def change_contrast(image, coefficient):
    result = image.astype(np.float64)
    result *= coefficient
    correct_image_values(result)
    return result.astype(np.uint8)


def alter_image_brightness_and_contrast_randomly(image,
                                                 delta_range=(-25, 25),
                                                 coefficient_range=(0.75, 1.25)):
    if random.choice([0, 1]) == 0:
        return image
    choice = random.choice([0, 1, 2, 3])
    if choice == 0:
        return change_brightness(image,
                                 random.randint(delta_range[0],
                                                delta_range[1]))
    if choice == 1:
        return change_contrast(image,
                               random.uniform(coefficient_range[0],
                                              coefficient_range[1]))
    if choice == 2:
        return change_contrast(change_brightness(image,
                                                 random.randint(delta_range[0],
                                                                delta_range[1])),
                               random.uniform(coefficient_range[0],
                                              coefficient_range[1]))
    return change_brightness(change_contrast(image,
                                             random.uniform(coefficient_range[0],
                                                            coefficient_range[1])),
                             random.randint(delta_range[0],
                                            delta_range[1]))


def insert_foreground(image,
                      foreground,
                      position):
    x, y = position
    image_part = image[y:y + foreground.shape[0], x:x + foreground.shape[1]]
    foreground_mask = foreground < 255
    result = image_part.copy()
    result[foreground_mask] = foreground[foreground_mask]
    image[y:y + foreground.shape[0], x:x + foreground.shape[1]] = result


def randomize_foreground(image,
                         min_color,
                         max_color):
    mask = image == 255
    result = np.random.randint(min_color, max_color, image.shape).astype(np.uint8)
    result[mask] = 255
    return result


def extract_foreground(image):
    result = np.array(image)
    binary = threshold_otsu(result)
    binary = cv2.erode(binary, np.ones((3, 3), np.uint8))
    result[(binary == 255)] = 255
    return result


def change_foreground_color_and_blend_foreground_on_background(image,
                                                               foreground,
                                                               position,
                                                               new_foreground_color,
                                                               alpha=0.75):
    x, y = position
    width, height = foreground.shape[1], foreground.shape[0]
    background_part = image[y:y + height, x:x + width]
    background_mean = np.mean(background_part)

    foreground_mask = foreground < 255
    if np.any(foreground_mask):
        foreground = foreground.astype(np.float64)
        masked_foreground = foreground[foreground_mask]
        min_foreground = np.min(masked_foreground)
        interpolator = interp1d([min_foreground, 255],
                                [new_foreground_color, background_mean])

        foreground[foreground_mask] = interpolator(masked_foreground)

        blended = foreground * alpha + background_part.astype(np.float64) * (1.0 - alpha)
        correct_image_values(blended)
        blended = blended.astype(np.uint8)
    else:
        blended = np.zeros(background_part.shape, np.uint8)

    result = background_part.copy()
    result[foreground_mask] = blended[foreground_mask]
    image[y:y + height, x:x + width] = result


def fade_foreground_on_background_while_preserving_color(image,
                                                         foreground,
                                                         position,
                                                         alpha=0.75):
    x, y = position
    width, height = foreground.shape[1], foreground.shape[0]
    background_part = image[y:y + height, x:x + width]
    background_mean = np.mean(background_part)

    non_background_mask = foreground < 255
    foreground_color = np.min(foreground)
    fade_mask = (foreground > foreground_color) & non_background_mask
    foreground_mask = (foreground == foreground_color) & non_background_mask

    if np.any(fade_mask):
        foreground_float = foreground.astype(np.float64)
        masked_foreground = foreground_float[fade_mask]
        interpolator = interp1d([foreground_color, 255],
                                [foreground_color, background_mean])

        foreground_float[fade_mask] = interpolator(masked_foreground)

        fade = foreground_float * alpha + background_part.astype(np.float64) * (1.0 - alpha)
        correct_image_values(fade)
        fade = fade.astype(np.uint8)
    else:
        fade = np.zeros(background_part.shape, np.uint8)

    result = background_part.copy()
    result[fade_mask] = fade[fade_mask]

    if np.any(foreground_mask):
        result[foreground_mask] = foreground[foreground_mask]

    image[y:y + height, x:x + width] = result


def crop_foreground_non_binary(image, threshold=None):
    if threshold is None:
        binary_image = threshold_otsu(image)
    else:
        binary_image = np.array(image)
        binary_image[binary_image < threshold] = 0
        binary_image[binary_image >= threshold] = 255

    mask = (255 - binary_image) > 0

    height, width = binary_image.shape[0:2]
    mask1, mask2 = mask.any(0), mask.any(1)
    x1, x2 = mask1.argmax(), width - mask1[::-1].argmax()
    y1, y2 = mask2.argmax(), height - mask2[::-1].argmax()

    return image[y1:y2, x1:x2]


def print_text_cropped(text,
                       font_path,
                       desired_font_size,
                       foreground_color,
                       background_size,
                       max_font_size,
                       min_character_width=8,
                       printing_safety_margins=0.25):
    width_upper_bound, height_upper_bound = background_size

    width, height, font = None, None, None
    font_size = desired_font_size
    good_font_settings = False
    tested_font_sizes = []
    while 0 < font_size <= max_font_size:
        if font_size in tested_font_sizes:
            break
        font = ImageFont.FreeTypeFont(font_path, font_size)
        left, top, right, bottom = font.getbbox(text)
        width = right - left
        height = bottom - top
        tested_font_sizes.append(font_size)
        if width / len(text) < min_character_width:
            font_size += 1
            continue
        if height <= height_upper_bound and width <= width_upper_bound:
            good_font_settings = True
            break
        font_size -= 1

    if font_size <= 0 or not good_font_settings:
        raise Exception(f'could not achieve required bounds with font: {font_path}, and size: {desired_font_size}, and text: {text}')

    height_margin = to_int(height * printing_safety_margins)
    width_margin = to_int(width * printing_safety_margins)

    image = np.ones((height + height_margin * 2, width + width_margin * 2), np.uint8) * 255
    image = Image.fromarray(image, 'L')
    draw = ImageDraw.Draw(image)
    left, top, right, bottom = font.getbbox(text)

    offset_x = -left + width_margin
    offset_y = -top + height_margin

    draw.text((offset_x, offset_y), text, fill=foreground_color, font=font)

    return crop_foreground_non_binary(np.array(image, np.uint8), 255)


def random_add_gaussian_noise(image, mean=0, variance=1, interval=None):
    if random_bool():
        return image
    width, height = image.shape
    gaussian = np.random.normal(mean,
                                variance,
                                (width, height)).astype(np.float64)
    if interval is not None:
        gaussian[gaussian < interval[0]] = interval[0]
        gaussian[gaussian > interval[1]] = interval[1]
    gaussian = image + gaussian
    gaussian[gaussian > 255] = 255
    gaussian[gaussian < 0] = 0
    return gaussian.astype(np.uint8)


def compress_image(image, quality):
    encoded = cv2.imencode('.jpg', image, [int(cv2.IMWRITE_JPEG_QUALITY), quality])[1]
    return cv2.imdecode(encoded, cv2.IMREAD_GRAYSCALE)


def random_coordinate(image):
    return random.randint(0, image.shape[0] - 1), random.randint(0, image.shape[1] - 1)


def speckle_image(image, fraction, color=0):
    coordinates = [random_coordinate(image)
                   for _ in range(to_int(image.size * fraction))]
    image_copy = np.array(image)
    for coord in coordinates:
        image_copy[coord[0], coord[1]] = color
    return image_copy


def random_blur(image):
    if random_bool():
        return image

    blur_lambdas = [lambda img: cv2.blur(img, (3, 3)),
                    lambda img: cv2.medianBlur(img, 3),
                    lambda img: cv2.GaussianBlur(img, (3, 3), 0)]

    return random.choice(blur_lambdas)(image)
