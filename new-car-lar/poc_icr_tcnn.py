#!/usr/bin/python3.6
import math
import os
import random
from enum import Enum

import cv2
import numpy as np
import tensorflow as tf
from scipy.interpolate import PchipInterpolator

keras = tf.keras
from keras import Input, Model
from keras.callbacks import LearningRateScheduler, LambdaCallback, ModelCheckpoint
# from keras.saving import load_model
from keras.layers import Conv2D, MaxPooling2D, Dense, Reshape, BatchNormalization, LeakyReLU, \
    Lambda, Concatenate

from keras.optimizers import Adam
from keras.utils import Sequence
import tensorflow as tf

SETTINGS = {
    'rnn_output_start_index': 2,
    'input_width': 256,
    'input_height': 32,
    'max_label_length': 32,
    'classes': 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ ',
    'lar_train_data_path': './data/train',
    'lar_validation_data_path': './data/validation',
    'lar_test_data_path': './data/test',
    'lar_embedding_dropout_rate': 0.5,
    'lar_batch_size': 32,
    'lar_epoch_learning_rate_mappings': {
        '0': 0.1,
        '15': 0.01,
        '30': 0.001,
        '40': 0.0001
    },
    'lar_leaky_ReLU_alpha': 0.05,
    'lar_target_rnn_time_steps': 64,
}

BLANK_CLASS = 0

LABEL_CONCAT_CHAR = ' '

BATCH_SIZE = SETTINGS['lar_batch_size']

MODEL_PLOT_NAME = 'lar.png'
MODEL_FILE_NAME = 'lar'
MAX_POOL_SIZE = 2


class DataType(Enum):
    TRAIN_DATA = 1
    VALIDATION_DATA = 2
    TEST_DATA = 3


def calculate_learning_rates(epoch_learning_rate_mappings):
    epochs = np.array([int(e) for e in epoch_learning_rate_mappings], np.float64)
    learning_rates = np.array([epoch_learning_rate_mappings[e] for e in epoch_learning_rate_mappings], np.float64)
    interpolator = PchipInterpolator(epochs, learning_rates)

    x = np.arange(min(epochs), max(epochs) + 1)
    y = interpolator(x)

    epoch_learning_rate_map = dict()
    for i in range(len(x)):
        epoch_learning_rate_map[x[i]] = y[i]

    return epoch_learning_rate_map


EPOCH_LEARNING_RATE_MAP = calculate_learning_rates(SETTINGS['lar_epoch_learning_rate_mappings'])
EPOCHS = len(EPOCH_LEARNING_RATE_MAP)


def date_label_to_classes(label):
    return [SETTINGS['classes'].find(l) + 1 for l in label]


def get_input_shape():
    return SETTINGS['input_height'], SETTINGS['input_width'], 1


def pre_process(image):
    return cv2.resize(image,
                      (SETTINGS['input_width'],
                       SETTINGS['input_height']),
                      interpolation=cv2.INTER_CUBIC).astype(np.float32).reshape(get_input_shape()) / 255.0


def read_compressed_image(file_path):
    # with gzip.open(file_path, 'rb') as f:
    #     content = f.read()
    # content_array = np.asarray(bytearray(content), dtype=np.uint8)
    # return cv2.imdecode(content_array, cv2.IMREAD_GRAYSCALE)
    return cv2.imread(file_path, cv2.IMREAD_GRAYSCALE)


def ctc_loss(y_true, y_pred):
    logits = y_pred[:, SETTINGS['rnn_output_start_index']:, :]
    input_length = tf.repeat(SETTINGS['lar_target_rnn_time_steps'] - SETTINGS['rnn_output_start_index'],
                             tf.shape(y_true)[0])
    label_length = tf.math.count_nonzero(y_true, axis=-1)
    return tf.reduce_mean(tf.nn.ctc_loss(y_true,
                                         logits,
                                         label_length,
                                         input_length,
                                         logits_time_major=False,
                                         blank_index=0))


class DataGenerator(Sequence):
    def __init__(self,
                 input_image_paths,
                 labels,
                 batch_size):
        count = len(input_image_paths)
        self.count = count
        self.batch_size = batch_size

        indices = list(range(count))
        random.shuffle(indices)
        self.input_image_paths = [input_image_paths[i] for i in indices]
        self.labels = [labels[i] for i in indices]

    def __getitem__(self, index):
        input_image_paths = self.input_image_paths[index * self.batch_size: (index + 1) * self.batch_size]
        input_images = [pre_process(read_compressed_image(f)) for f in input_image_paths]

        output_labels = self.labels[index * self.batch_size: (index + 1) * self.batch_size]
        output_labels = [np.array(date_label_to_classes(label), np.uint8) for label in output_labels]

        output_labels = np.array([np.append(y, [BLANK_CLASS] * (SETTINGS['max_label_length'] - len(y)))
                                  for y in output_labels],
                                 dtype=np.uint8)
        return np.array(input_images), output_labels

    def __len__(self):
        return int(math.ceil(self.count / self.batch_size))

    def on_epoch_end(self):
        indices = list(range(self.count))
        random.shuffle(indices)
        self.input_image_paths = [self.input_image_paths[i] for i in indices]
        self.labels = [self.labels[i] for i in indices]


def path_from_data_type(data_type):
    if data_type == DataType.TRAIN_DATA:
        path = SETTINGS['lar_train_data_path']
    elif data_type == DataType.VALIDATION_DATA:
        path = SETTINGS['lar_validation_data_path']
    else:
        path = SETTINGS['lar_test_data_path']
    return path


def convolutional_sub_sampling(filters, input_layer):
    sub_sample = Conv2D(filters,
                        kernel_size=5,
                        strides=2,
                        padding='same',
                        kernel_initializer='glorot_normal')(input_layer)
    sub_sample = BatchNormalization()(sub_sample)
    return LeakyReLU(SETTINGS['lar_leaky_ReLU_alpha'])(sub_sample)


def stacked_convolutions(filters, kernel_size, input_layer, convolutions=2, padding='same'):
    convolution = input_layer
    for i in range(convolutions):
        convolution = Conv2D(filters,
                             (kernel_size, kernel_size),
                             padding=padding,
                             kernel_initializer='glorot_normal')(convolution)
        convolution = BatchNormalization()(convolution)
        convolution = LeakyReLU(SETTINGS['lar_leaky_ReLU_alpha'])(convolution)

    return convolution


def build_model():
    model_file = MODEL_FILE_NAME + '.h5'
    # if os.path.isfile(model_file):
    #     return load_model(model_file)

    input_shape = get_input_shape()
    input_data = Input(name='input_data',
                       shape=input_shape)

    embedding = BatchNormalization()(input_data)

    embedding = stacked_convolutions(16, 3, embedding)
    embedding = MaxPooling2D(pool_size=(2, 2))(embedding)

    embedding = stacked_convolutions(32, 3, embedding)
    embedding = MaxPooling2D(pool_size=(2, 2))(embedding)

    embedding = stacked_convolutions(64, 3, embedding)
    embedding = MaxPooling2D(pool_size=(2, 1))(embedding)

    embedding = stacked_convolutions(128, 3, embedding)
    embedding = MaxPooling2D(pool_size=(2, 1))(embedding)

    embedding = stacked_convolutions(256, 3, embedding)
    embedding = MaxPooling2D(pool_size=(2, 1))(embedding)

    embedding = Reshape(target_shape=(SETTINGS['lar_target_rnn_time_steps'], 256))(embedding)

    embedding = Dense(256, kernel_initializer='glorot_normal')(embedding)
    embedding = BatchNormalization()(embedding)
    # embedding = Dropout(SETTINGS['lar_embedding_dropout_rate'])(embedding)
    embedding = LeakyReLU(SETTINGS['lar_leaky_ReLU_alpha'], name='embedding')(embedding)

    tcnn = embedding
    dilations = 3  # int(math.ceil(math.log2(SETTINGS['lar_target_rnn_time_steps'])))

    previous = embedding
    for _ in range(6):
        tcnn_reverse = Lambda(lambda x: tf.reverse(x, axis=[0]))(tcnn)

        for i in range(dilations):
            tcnn = tf.keras.layers.Conv1D(filters=256,
                                          kernel_size=2,
                                          dilation_rate=2 ** i,
                                          kernel_initializer='glorot_normal',
                                          padding='causal')(tcnn)
            tcnn = BatchNormalization()(tcnn)
            tcnn = LeakyReLU(SETTINGS['lar_leaky_ReLU_alpha'])(tcnn)

        for i in range(dilations):
            tcnn_reverse = tf.keras.layers.Conv1D(filters=256,
                                                  kernel_size=2,
                                                  dilation_rate=2 ** i,
                                                  kernel_initializer='glorot_normal',
                                                  padding='causal')(tcnn_reverse)
            tcnn_reverse = BatchNormalization()(tcnn_reverse)
            tcnn_reverse = LeakyReLU(SETTINGS['lar_leaky_ReLU_alpha'])(tcnn_reverse)
        tcnn_reverse = Lambda(lambda x: tf.reverse(x, axis=[0]))(tcnn_reverse)

        tcnn = Concatenate()([tcnn, tcnn_reverse])
        previous = tf.keras.layers.Conv1D(filters=512,
                                          kernel_size=1,
                                          kernel_initializer='glorot_normal',
                                          padding='causal')(previous)
        previous = BatchNormalization()(previous)
        previous = LeakyReLU(SETTINGS['lar_leaky_ReLU_alpha'])(previous)
        tcnn = Lambda(lambda x: x[0] + x[1])([tcnn, previous])
        previous = tcnn

    # the additional class is reserved for the ctc loss blank character
    classes = len(SETTINGS['classes']) + 1

    # the output of this layer is required for the tensorflow ctc_beam_search_decoder
    logits_layer = Dense(classes,
                         kernel_initializer='glorot_normal',
                         name='logits')(tcnn)

    return Model(inputs=[input_data],
                 outputs=logits_layer)


def update_report(line='', report_file=MODEL_FILE_NAME + '_report.txt'):
    with open(report_file, 'a') as f:
        f.write(line + '\n')


def load_images_paths(data_type=DataType.TRAIN_DATA):
    path = path_from_data_type(data_type)
    files = os.listdir(path)
    files = [f for f in files if f.endswith('.png')]
    paths = [os.path.join(path, f) for f in files]
    return [p for p in paths if os.path.isfile(p)]


def load_labels(data_type=DataType.TRAIN_DATA):
    path = path_from_data_type(data_type)
    files = os.listdir(path)
    files = [f for f in files if f.endswith('.txt')]
    paths = [os.path.join(path, f) for f in files]
    return [p for p in paths if os.path.isfile(p)]


def load_images_paths_and_labels(data_type=DataType.TRAIN_DATA):
    path = path_from_data_type(data_type)
    files = os.listdir(path)

    image_files = [f for f in files if f.endswith('.png')]

    image_paths = []
    labels = []
    for image_file in image_files:
        image_file_path = os.path.join(path, image_file)
        if not os.path.isfile(image_file_path):
            continue
        file_name = os.path.splitext(image_file)[0]
        label_file_path = os.path.join(path, f'{file_name}.txt')
        if not os.path.isfile(label_file_path):
            continue
        image_paths.append(image_file_path)
        with open(label_file_path, 'r') as file:
            label = file.read()
        labels.append(label.strip())
    return image_paths, labels


def train(model):
    train_files, train_labels = load_images_paths_and_labels(DataType.TRAIN_DATA)
    validation_files, validation_labels = load_images_paths_and_labels(DataType.TRAIN_DATA)

    last_model_checkpoint = ModelCheckpoint(MODEL_FILE_NAME + '_checkpoint.h5')
    best_model_checkpoint = ModelCheckpoint(MODEL_FILE_NAME + '_checkpoint_{epoch:02d}_{val_loss:0.4f}.h5',
                                            monitor='val_loss',
                                            save_best_only=True,
                                            mode='min')

    learning_rate_scheduler = LearningRateScheduler(lambda e: EPOCH_LEARNING_RATE_MAP[e], verbose=0)

    update_report('======= training results =======')

    update_report_callback = LambdaCallback(on_epoch_end=lambda epoch, logs: update_report(
        f'epoch: {epoch} - loss: {logs["loss"]} - validation loss: {logs["val_loss"]}'
    ))

    model.fit(DataGenerator(train_files,
                            train_labels,
                            BATCH_SIZE),
              validation_data=DataGenerator(validation_files,
                                            validation_labels,
                                            256),
              epochs=EPOCHS,
              callbacks=[last_model_checkpoint,
                         best_model_checkpoint,
                         # learning_rate_scheduler,
                         update_report_callback])

    model.save(MODEL_FILE_NAME + '.h5')
    update_report()


def run_pipeline():
    model = build_model()
    model.summary()
    # plot_model(model,
    #            to_file=MODEL_PLOT_NAME,
    #            show_shapes=True)

    model.compile(loss=ctc_loss,
                  #   optimizer=SGD(learning_rate=0.0001,
                  #                 momentum=0.9,
                  #                 nesterov=True,
                  #                 clipnorm=5))
                  optimizer=Adam(learning_rate=0.001,
                                 clipnorm=4.0))

    print('\ntraining model...\n')
    train(model)


if __name__ == '__main__':
    run_pipeline()

    # _, _, base_model, _ = build_model()
    # image1 = cv2.imread('./data/test/lar/13.jpg', cv2.IMREAD_GRAYSCALE)
    # image2 = cv2.imread('./data/test/lar/14.jpg', cv2.IMREAD_GRAYSCALE)
    # print(predict_batch(base_model, [image1, image2], True))
