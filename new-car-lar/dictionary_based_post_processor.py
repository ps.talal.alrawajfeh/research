import sys

import numpy as np
import textdistance

ENGLISH_AMOUNTS_DICTIONARY = [
    'zero',
    'one',
    'two',
    'three',
    'four',
    'five',
    'six',
    'seven',
    'eight',
    'nine',
    'ten',
    'eleven',
    'twelve',
    'thirteen',
    'fourteen',
    'fifteen',
    'sixteen',
    'seventeen',
    'eighteen',
    'nineteen',
    'twenty',
    'thirty',
    'forty',
    'fifty',
    'sixty',
    'seventy',
    'eighty',
    'ninety',
    'hundred',
    'thousand',
    'million',
    'only',
    'and'
]

ARABIC_AMOUNTS_DICTIONARY = [
    'صفر',
    'واحد',
    'اثنان',
    'اثنين',
    'ثلاثة',
    'ثلاث',
    'اربعة',
    'اربع',
    'خمسة',
    'خمس',
    'ستة',
    'ست',
    'سبعة',
    'سبع',
    'ثمانية',
    'ثمان',
    'تسعة',
    'تسع',
    'عشرة',
    'عشر',
    'احد',
    'اثنا',
    'عشرون',
    'ثلاثون',
    'اربعون',
    'خمسون',
    'ستون',
    'سبعون',
    'ثمانون',
    'تسعون',
    'مئة',
    'مائة',
    'مئتان',
    'مئتا',
    'الف',
    'الاف',
    'الفان',
    'مليون',
    'مليونان',
    'ملايين',
    'و',
    'فقط',
    'لا',
    'غير'
]


# TODO: Add currencies here to the dictionaries then in the words_to_number_converter.py script add them to special
# dictionaries called "whole_currencies" and "fractional_currencies" and if the number is proceeded with a whole
# currency than it is kept as is and if it is proceeded with a fractional currency it is divided by the proper fraction

def word_of_min_distance(text, dictionary):
    min_distance = sys.maxsize
    best_word = None
    for word in dictionary:
        distance = textdistance.damerau_levenshtein(text, word)
        if distance < min_distance:
            min_distance = distance
            best_word = word
    return best_word, min_distance


def find_non_overlapping_start_end_pairs_with_min_cost(text,
                                                       dictionary,
                                                       segments_distances_matrix,
                                                       path_dict,
                                                       start_index=0):
    max_index = len(text)
    min_cost = sys.maxsize
    best_start_end_pairs = None

    if start_index == max_index:
        return 0, []

    for start in range(start_index, max_index):
        for end in range(start + 1, max_index + 1):
            if end in path_dict:
                cost, start_end_pairs = path_dict[end]
            else:
                cost, start_end_pairs = find_non_overlapping_start_end_pairs_with_min_cost(text,
                                                                                           dictionary,
                                                                                           segments_distances_matrix,
                                                                                           path_dict,
                                                                                           end)
                path_dict[end] = (cost, start_end_pairs)

            min_distance = segments_distances_matrix[start, end]
            ignored_characters = start - start_index
            cost = ignored_characters + min_distance + cost
            other_cost = ignored_characters + min_distance + max_index - end

            if other_cost < min_cost:
                min_cost = cost
                best_start_end_pairs = [(start, end)]

            if cost < min_cost:
                min_cost = cost
                best_start_end_pairs = [(start, end)] + start_end_pairs

    return min_cost, best_start_end_pairs


def correct_text(text, dictionary):
    max_index = len(text) + 1
    segments_distances_matrix = np.zeros((max_index, max_index), np.int64)
    best_words_matrix = []
    for i in range(max_index):
        best_words_matrix.append([])
        for j in range(max_index):
            best_words_matrix[i].append('')

    max_int64 = np.iinfo(np.int64).max
    for start in range(0, max_index):
        for end in range(0, max_index):
            if end <= start:
                segments_distances_matrix[start, end] = max_int64
                continue

            segment = text[start:end]
            best_word, min_distance = word_of_min_distance(segment, dictionary)
            segments_distances_matrix[start, end] = min_distance
            best_words_matrix[start][end] = best_word

    path_dict = dict()
    min_cost, best_start_end_pairs = find_non_overlapping_start_end_pairs_with_min_cost(text,
                                                                                        dictionary,
                                                                                        segments_distances_matrix,
                                                                                        path_dict)

    if max_index - 1 <= min_cost:
        return []

    words = []
    for start, end in best_start_end_pairs:
        words.append(best_words_matrix[start][end])
    return words


def post_process(text, language='en'):
    if language == 'en':
        text = text.replace('-', ' ').replace('_', ' ').replace(',', ' ')
        text = ''.join([c for c in text if c in 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ '])
        text = text.lower().strip()
        dictionary = ENGLISH_AMOUNTS_DICTIONARY
    elif language == 'ar':
        text = text.replace('-', ' ').replace('،', ' ')
        text = text.replace(' و', ' و ').replace('أ', 'ا').replace('آ', 'ا').replace('إ', 'ا')
        text = ''.join([c for c in text if c in 'ا ب ت ث ج ح خ د ذ ر ز س ش ص ض ط ظ ع غ ف ق ك ل م ن ه و ي ئ ة'])
        text = text.replace('مائة', ' مئة ')
        text = text.replace('مئتان', ' مئتان ')
        text = text.strip()
        dictionary = ARABIC_AMOUNTS_DICTIONARY
    else:
        raise Exception(f'Dictionary not defined for language: {language}.')

    words = text.split(' ')
    new_words = []
    for word in words:
        if word.strip() == '':
            continue

        closest_words = correct_text(word, dictionary)
        new_words.extend(closest_words)

    return ' '.join(new_words).replace('مائة', 'مئة')
