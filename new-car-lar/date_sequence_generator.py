import math
import multiprocessing
import os
import random
import time
import traceback
from concurrent.futures import ThreadPoolExecutor

import cv2
import numpy as np
from tqdm import tqdm

from image_utils import InfiniteSampler, is_image, crop_background_area, \
    alter_image_brightness_and_contrast_randomly, to_int, change_foreground_color_and_blend_foreground_on_background, \
    insert_foreground, randomize_foreground, random_add_gaussian_noise, \
    speckle_image, random_blur, compress_image, print_text_cropped, fade_foreground_on_background_while_preserving_color

SETTINGS = {
    'date_character_classes': 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789,.#*-=/\\| ',
    'date_box_initial_width': 400,
    'date_box_initial_height': 72,
    'date_box_target_width': 200,
    'date_box_target_height': 36,
    'date_templates_path': './PS-Templates',
    'date_handwritten_fonts_path': './fonts/handwritten',
    'date_printed_fonts_path': './fonts/printed',
    'date_generated_data_path': './data',
    'date_training_data_size': 500,
    'date_validation_data_size': 100,
    'date_test_data_size': 100,
    'date_cheque_area_error_percentage': 0.01,
    'date_box_start_point_percentages': (0.7135, 0.0806),
    'date_box_size_percentages': (0.272, 0.113),
    'date_min_target_character_width': 4,
    'date_random_sequence_max_length': 32,
    'date_min_font_size': 20,
    'date_max_font_size': 44,
}


def random_bool():
    return random.choice((True, False))


def random_date_label():
    while True:
        formats = ['dmy', 'mdy', 'ymd', 'ydm']
        date_format = random.choice(formats)

        day_index = date_format.find('d')
        month_index = date_format.find('m')
        year_index = date_format.find('y')

        date_parts = ['', '', '']

        date_day = str(random.randint(1, 31))
        date_month = random.randint(1, 12)
        date_year = str(random.randint(1970, 2100))

        if random_bool():
            if date_month == 1:
                date_month = 'january'
            elif date_month == 2:
                date_month = 'february'
            elif date_month == 3:
                date_month = 'march'
            elif date_month == 4:
                date_month = 'april'
            elif date_month == 5:
                date_month = 'may'
            elif date_month == 6:
                date_month = 'june'
            elif date_month == 7:
                date_month = 'july'
            elif date_month == 8:
                date_month = 'august'
            elif date_month == 9:
                date_month = 'september'
            elif date_month == 10:
                date_month = 'october'
            elif date_month == 11:
                date_month = 'november'
            else:
                date_month = 'december'

            if random_bool():
                date_month = date_month[:3]

            capitalization_mode = random.choice([1, 2, 3, 4])
            if capitalization_mode == 1:
                date_month = date_month.upper()
            elif capitalization_mode == 2:
                date_month = date_month.lower()
            elif capitalization_mode == 3:
                date_month = ' '.join([word[:1].upper() + word[1:] for word in date_month.split(' ')])
            else:
                date_month = ''.join([x.upper() if random_bool() else x for x in date_month])
        else:
            date_month = str(date_month)

        date_parts[day_index] = str(date_day)
        date_parts[month_index] = date_month
        date_parts[year_index] = str(date_year)

        if len(date_parts[day_index]) < 2 and random_bool():
            date_parts[day_index] = '0' * (2 - len(date_parts[day_index])) + date_parts[day_index]
        if len(date_parts[month_index]) < 2 and random_bool():
            date_parts[month_index] = '0' * (2 - len(date_parts[month_index])) + date_parts[month_index]

        separators = ',.#*-=/\\| '
        if random_bool():
            label = random.choice(separators).join(date_parts)
        else:
            label = ''.join(date_parts)

        if random_bool():
            total_spaces = random.randint(1, 50 - len(label))
            index_spaces_dict = dict()
            for i in range(0, len(label) - 1):
                spaces = random.randint(0, total_spaces)
                index_spaces_dict[i] = spaces
                total_spaces -= spaces
                if total_spaces == 0:
                    break

            new_label = ''
            for i in range(0, len(label)):
                new_label += label[i]
                if i in index_spaces_dict:
                    new_label += ' ' * index_spaces_dict[i]
            label = new_label

        if 2 * SETTINGS['date_box_target_width'] / len(label) >= SETTINGS['date_min_target_character_width']:
            break

    return label


def draw_printed_text_on_background(text,
                                    background,
                                    font_supplier):
    if text.strip() == '':
        return

    background_mean = np.mean(background)
    background_std = np.std(background)
    color_upper_limit = max(min(to_int(background_mean - 2 * background_std), to_int(background_mean - 20)), 0)
    color_lower_limit = random.randint(0, max(min(to_int(background_mean - 3 * background_std),
                                                  to_int(background_mean - 30)), 0))

    foreground_color_type = random.choice([1, 2, 3, 4, 5])

    if foreground_color_type == 3:
        foreground_color = 0
    else:
        foreground_color = random.randint(color_lower_limit, color_upper_limit)

    min_character_width = int(
        math.ceil(SETTINGS['date_min_target_character_width'] * SETTINGS['date_box_initial_width']
                  / SETTINGS['date_box_target_width']))

    foreground_image = print_text_cropped(text,
                                          random.choice(font_supplier.next()),
                                          random.randint(SETTINGS['date_min_font_size'],
                                                         SETTINGS['date_max_font_size']),
                                          foreground_color,
                                          (SETTINGS['date_box_initial_width'],
                                           SETTINGS['date_box_initial_height']),
                                          SETTINGS['date_max_font_size'],
                                          min_character_width=min_character_width)

    x = random.randint(0, background.shape[1] - foreground_image.shape[1])
    y = random.randint(0, background.shape[0] - foreground_image.shape[0])

    if foreground_color_type == 1:
        fade_foreground_on_background_while_preserving_color(background,
                                                             foreground_image,
                                                             (x, y),
                                                             random.uniform(0.6, 0.8))
    elif foreground_color_type == 2:
        change_foreground_color_and_blend_foreground_on_background(background,
                                                                   foreground_image,
                                                                   (x, y),
                                                                   foreground_color,
                                                                   random.uniform(0.6, 0.8))
    elif foreground_color_type == 3:
        foreground_image = randomize_foreground(foreground_image, color_lower_limit, color_upper_limit)
        insert_foreground(background,
                          foreground_image,
                          (x, y))
    elif foreground_color_type == 4:
        foreground_image[foreground_image < 255] = foreground_color
        insert_foreground(background,
                          foreground_image,
                          (x, y))
    else:
        foreground_image[foreground_image < 255] = foreground_color
        change_foreground_color_and_blend_foreground_on_background(background,
                                                                   foreground_image,
                                                                   (x, y),
                                                                   foreground_color,
                                                                   random.uniform(0.6, 0.8))


def apply_random_effects(image):
    if random_bool():
        if random_bool():
            image = random_add_gaussian_noise(image,
                                              0,
                                              random.randint(10, 20),
                                              (-20, 20))
        else:
            image = speckle_image(image, 0.01, random.randint(0, 255))

        if random_bool():
            image = random_blur(image)
        else:
            image = compress_image(image, random.randint(50, 100))
    return image


def generate_sample(template_sampler,
                    font_suppliers):
    while True:
        try:
            template = cv2.imread(template_sampler.next(), cv2.IMREAD_GRAYSCALE)

            if random.uniform(0, 1) < 0.2:
                template = np.ones(template.shape, np.uint8) * 255

            date_box_background = crop_background_area(template,
                                                       SETTINGS['date_box_start_point_percentages'],
                                                       SETTINGS['date_box_size_percentages'],
                                                       SETTINGS['date_cheque_area_error_percentage'])

            date_box_background = cv2.resize(date_box_background,
                                             (SETTINGS['date_box_initial_width'], SETTINGS['date_box_initial_height']),
                                             interpolation=cv2.INTER_CUBIC)
            date_box_background = alter_image_brightness_and_contrast_randomly(date_box_background)

            prob = random.uniform(0, 1)
            if prob <= 0.8:
                date_label = random_date_label()
            else:
                date_label = ''
                for i in range(random.randint(1, SETTINGS['date_random_sequence_max_length'])):
                    date_label += random.choice(SETTINGS['date_character_classes'])

            date_label = ''.join([l for l in date_label if l in SETTINGS['date_character_classes']]).strip()

            font_supplier = random.choice(font_suppliers)
            draw_printed_text_on_background(date_label, date_box_background, font_supplier)
            date_box_background = apply_random_effects(date_box_background)
            date_box_background = cv2.resize(date_box_background,
                                             (SETTINGS['date_box_target_width'], SETTINGS['date_box_target_height']),
                                             interpolation=cv2.INTER_CUBIC)

            return date_box_background, date_label
        except Exception as e:
            print(f"An exception occurred: {str(e)}")
            traceback.print_exc()


def load_font_paths(path):
    font_paths = os.listdir(path)
    font_paths = [f for f in font_paths if f.lower().endswith('.ttf') or f.lower().endswith('.otf')]
    font_paths = [os.path.join(path, f) for f in font_paths]
    return font_paths


def parallel_data_generation(output_data_count,
                             start_index,
                             progress_bar,
                             generation_path,
                             data_generation_function,
                             *function_args):
    progress_lock = multiprocessing.Lock()
    indices = list(range(start_index, start_index + output_data_count))
    random.shuffle(indices)

    if not os.path.isdir(generation_path):
        os.makedirs(generation_path)

    def wrapped_data_generation_function(*args):
        date_box_image, date_label = data_generation_function(*args)

        with progress_lock:
            current_index = indices.pop()

        cv2.imwrite(os.path.join(generation_path, f'{current_index}.png'), 255 - date_box_image)
        with open(os.path.join(generation_path, f'{current_index}.txt'), 'w') as f:
            f.write(date_label)
        with progress_lock:
            progress_bar.update()

    while len(indices) > 0:
        with ThreadPoolExecutor(max_workers=multiprocessing.cpu_count() * 2) as executor:
            for _ in range(len(indices)):
                executor.submit(wrapped_data_generation_function,
                                *function_args)
        time.sleep(1)


def load_font_families(path):
    families_dirs = os.listdir(path)
    families = []
    for f in families_dirs:
        family_path = os.path.join(path, f)
        if not os.path.isdir(family_path):
            continue
        families.append(load_font_paths(family_path))
    return families


def main():
    paths = os.listdir(SETTINGS['date_templates_path'])
    paths = [os.path.join(SETTINGS['date_templates_path'], p) for p in paths]
    paths = [p for p in paths if is_image(p)]
    template_sampler = InfiniteSampler(paths)

    font_supplier1 = InfiniteSampler(load_font_families(SETTINGS['date_handwritten_fonts_path']))
    font_supplier2 = InfiniteSampler(load_font_families(SETTINGS['date_printed_fonts_path']))

    progress_bar = tqdm(total=SETTINGS['date_training_data_size'])
    parallel_data_generation(SETTINGS['date_training_data_size'],
                             1,
                             progress_bar,
                             os.path.join(SETTINGS['date_generated_data_path'], 'train'),
                             generate_sample,
                             template_sampler,
                             [font_supplier1, font_supplier2])
    progress_bar.close()

    progress_bar = tqdm(total=SETTINGS['date_validation_data_size'])
    parallel_data_generation(SETTINGS['date_validation_data_size'],
                             1,
                             progress_bar,
                             os.path.join(SETTINGS['date_generated_data_path'], 'validation'),
                             generate_sample,
                             template_sampler,
                             [font_supplier1, font_supplier2])
    progress_bar.close()

    progress_bar = tqdm(total=SETTINGS['date_test_data_size'])
    parallel_data_generation(SETTINGS['date_test_data_size'],
                             1,
                             progress_bar,
                             os.path.join(SETTINGS['date_generated_data_path'], 'test'),
                             generate_sample,
                             template_sampler,
                             [font_supplier1, font_supplier2])
    progress_bar.close()


if __name__ == '__main__':
    main()
