import math
import multiprocessing
import os
import random
import time
import traceback
from concurrent.futures import ThreadPoolExecutor

import cv2
import numpy as np
from tqdm import tqdm

from image_utils import InfiniteSampler, is_image, crop_background_area, \
    alter_image_brightness_and_contrast_randomly, to_int, change_foreground_color_and_blend_foreground_on_background, \
    insert_foreground, randomize_foreground, random_add_gaussian_noise, \
    speckle_image, random_blur, compress_image, print_text_cropped, fade_foreground_on_background_while_preserving_color

SETTINGS = {
    'car_character_classes': '0123456789,.#*-=/\\| ',
    'car_box_initial_width': 400,
    'car_box_initial_height': 72,
    'car_box_target_width': 200,
    'car_box_target_height': 36,
    'car_templates_path': './PS-Templates',
    'car_handwritten_fonts_path': './fonts/handwritten',
    'car_printed_fonts_path': './fonts/printed',
    'car_generated_data_path': './data',
    'car_training_data_size': 500000,
    'car_validation_data_size': 100000,
    'car_test_data_size': 100000,
    'car_cheque_area_error_percentage': 0.01,
    'car_box_start_point_percentages': (0.7135, 0.323),
    'car_box_size_percentages': (0.272, 0.113),
    'car_min_target_character_width': 4,
    'car_random_sequence_max_length': 32,
    'car_min_font_size': 20,
    'car_max_font_size': 44,
}


def random_bool():
    return random.choice((True, False))


def random_car_label():
    while True:
        intervals = [(0, 9999),
                     (10000, 99999),
                     (100000, 999999),
                     (1000000, 9999999)]

        interval = random.choice(intervals)
        number = random.randint(interval[0], interval[1])

        label = str(number)

        if random_bool():
            for i in range(len(label) - 3, -1, -3):
                if i == 0:
                    break
                label = label[:i] + ',' + label[i:]

        if random_bool():
            fraction = str(random.randint(0, 999))
            if random_bool():
                fraction = '0' * (3 - len(fraction)) + fraction
            label += '.' + fraction

        if random_bool():
            enclosing_choice = random.choice(['*', '-', '=', '#'])
            left_enclosing = enclosing_choice * random.randint(1, 5)
            if random_bool():
                left_enclosing += random.choice(['/', '\\', '|'])
            if random_bool():
                label = left_enclosing + ' ' + label
            else:
                label = left_enclosing + label

        if random_bool():
            right_enclosing = ''
            if random_bool():
                right_enclosing += random.choice(['/', '\\', '|'])
            enclosing_choice = random.choice(['*', '-', '=', '#'])
            right_enclosing += enclosing_choice * random.randint(1, 5)
            if random_bool():
                label += ' ' + right_enclosing
            else:
                label += right_enclosing

        if random_bool():
            for i in range(len(label) - 1, -1, -1):
                if random_bool():
                    label = label[:i] + ' ' + label[i:]

        if 2 * SETTINGS['car_box_target_width'] / len(label) >= SETTINGS['car_min_target_character_width']:
            break

    return label


def draw_printed_text_on_background(text,
                                    background,
                                    font_supplier):
    if text.strip() == '':
        return

    background_mean = np.mean(background)
    background_std = np.std(background)
    color_upper_limit = max(min(to_int(background_mean - 2 * background_std), to_int(background_mean - 20)), 0)
    color_lower_limit = random.randint(0, max(min(to_int(background_mean - 3 * background_std),
                                                  to_int(background_mean - 30)), 0))

    foreground_color_type = random.choice([1, 2, 3, 4, 5])

    if foreground_color_type == 3:
        foreground_color = 0
    else:
        foreground_color = random.randint(color_lower_limit, color_upper_limit)

    min_character_width = int(
        math.ceil(SETTINGS['car_min_target_character_width'] * SETTINGS['car_box_initial_width']
                  / SETTINGS['car_box_target_width']))

    foreground_image = print_text_cropped(text,
                                          random.choice(font_supplier.next()),
                                          random.randint(SETTINGS['car_min_font_size'],
                                                         SETTINGS['car_max_font_size']),
                                          foreground_color,
                                          (SETTINGS['car_box_initial_width'],
                                           SETTINGS['car_box_initial_height']),
                                          SETTINGS['car_max_font_size'],
                                          min_character_width=min_character_width)

    x = random.randint(0, background.shape[1] - foreground_image.shape[1])
    y = random.randint(0, background.shape[0] - foreground_image.shape[0])

    if foreground_color_type == 1:
        fade_foreground_on_background_while_preserving_color(background,
                                                             foreground_image,
                                                             (x, y),
                                                             random.uniform(0.6, 0.8))
    elif foreground_color_type == 2:
        change_foreground_color_and_blend_foreground_on_background(background,
                                                                   foreground_image,
                                                                   (x, y),
                                                                   foreground_color,
                                                                   random.uniform(0.6, 0.8))
    elif foreground_color_type == 3:
        foreground_image = randomize_foreground(foreground_image, color_lower_limit, color_upper_limit)
        insert_foreground(background,
                          foreground_image,
                          (x, y))
    elif foreground_color_type == 4:
        foreground_image[foreground_image < 255] = foreground_color
        insert_foreground(background,
                          foreground_image,
                          (x, y))
    else:
        foreground_image[foreground_image < 255] = foreground_color
        change_foreground_color_and_blend_foreground_on_background(background,
                                                                   foreground_image,
                                                                   (x, y),
                                                                   foreground_color,
                                                                   random.uniform(0.6, 0.8))


def apply_random_effects(image):
    if random_bool():
        if random_bool():
            image = random_add_gaussian_noise(image,
                                              0,
                                              random.randint(10, 20),
                                              (-20, 20))
        else:
            image = speckle_image(image, 0.01, random.randint(0, 255))

        if random_bool():
            image = random_blur(image)
        else:
            image = compress_image(image, random.randint(50, 100))
    return image


def generate_sample(template_sampler,
                    font_suppliers):
    while True:
        try:
            template = cv2.imread(template_sampler.next(), cv2.IMREAD_GRAYSCALE)

            if random.uniform(0, 1) < 0.2:
                template = np.ones(template.shape, np.uint8) * 255

            car_box_background = crop_background_area(template,
                                                      SETTINGS['car_box_start_point_percentages'],
                                                      SETTINGS['car_box_size_percentages'],
                                                      SETTINGS['car_cheque_area_error_percentage'])

            car_box_background = cv2.resize(car_box_background,
                                            (SETTINGS['car_box_initial_width'], SETTINGS['car_box_initial_height']),
                                            interpolation=cv2.INTER_CUBIC)
            car_box_background = alter_image_brightness_and_contrast_randomly(car_box_background)

            prob = random.uniform(0, 1)
            if prob <= 0.8:
                car_label = random_car_label()
            else:
                car_label = ''
                for i in range(random.randint(1, SETTINGS['car_random_sequence_max_length'])):
                    car_label += random.choice(SETTINGS['car_character_classes'])

            car_label = ''.join([l for l in car_label if l in SETTINGS['car_character_classes']]).strip()

            font_supplier = random.choice(font_suppliers)
            draw_printed_text_on_background(car_label, car_box_background, font_supplier)
            car_box_background = apply_random_effects(car_box_background)
            car_box_background = cv2.resize(car_box_background,
                                            (SETTINGS['car_box_target_width'], SETTINGS['car_box_target_height']),
                                            interpolation=cv2.INTER_CUBIC)

            return car_box_background, car_label
        except Exception as e:
            print(f"An exception occurred: {str(e)}")
            traceback.print_exc()


def load_font_paths(path):
    font_paths = os.listdir(path)
    font_paths = [f for f in font_paths if f.lower().endswith('.ttf') or f.lower().endswith('.otf')]
    font_paths = [os.path.join(path, f) for f in font_paths]
    return font_paths


def parallel_data_generation(output_data_count,
                             start_index,
                             progress_bar,
                             generation_path,
                             data_generation_function,
                             *function_args):
    progress_lock = multiprocessing.Lock()
    indices = list(range(start_index, start_index + output_data_count))
    random.shuffle(indices)

    if not os.path.isdir(generation_path):
        os.makedirs(generation_path)

    def wrapped_data_generation_function(*args):
        car_box_image, car_label = data_generation_function(*args)

        with progress_lock:
            current_index = indices.pop()

        cv2.imwrite(os.path.join(generation_path, f'{current_index}.png'), 255 - car_box_image)
        with open(os.path.join(generation_path, f'{current_index}.txt'), 'w') as f:
            f.write(car_label)
        with progress_lock:
            progress_bar.update()

    while len(indices) > 0:
        with ThreadPoolExecutor(max_workers=multiprocessing.cpu_count() * 2) as executor:
            for _ in range(len(indices)):
                executor.submit(wrapped_data_generation_function,
                                *function_args)
        time.sleep(1)


def load_font_families(path):
    families_dirs = os.listdir(path)
    families = []
    for f in families_dirs:
        family_path = os.path.join(path, f)
        if not os.path.isdir(family_path):
            continue
        families.append(load_font_paths(family_path))
    return families


def main():
    paths = os.listdir(SETTINGS['car_templates_path'])
    paths = [os.path.join(SETTINGS['car_templates_path'], p) for p in paths]
    paths = [p for p in paths if is_image(p)]
    template_sampler = InfiniteSampler(paths)

    font_supplier1 = InfiniteSampler(load_font_families(SETTINGS['car_handwritten_fonts_path']))
    font_supplier2 = InfiniteSampler(load_font_families(SETTINGS['car_printed_fonts_path']))

    progress_bar = tqdm(total=SETTINGS['car_training_data_size'])
    parallel_data_generation(SETTINGS['car_training_data_size'],
                             1,
                             progress_bar,
                             os.path.join(SETTINGS['car_generated_data_path'], 'train'),
                             generate_sample,
                             template_sampler,
                             [font_supplier1, font_supplier2])
    progress_bar.close()

    progress_bar = tqdm(total=SETTINGS['car_validation_data_size'])
    parallel_data_generation(SETTINGS['car_validation_data_size'],
                             1,
                             progress_bar,
                             os.path.join(SETTINGS['car_generated_data_path'], 'validation'),
                             generate_sample,
                             template_sampler,
                             [font_supplier1, font_supplier2])
    progress_bar.close()

    progress_bar = tqdm(total=SETTINGS['car_test_data_size'])
    parallel_data_generation(SETTINGS['car_test_data_size'],
                             1,
                             progress_bar,
                             os.path.join(SETTINGS['car_generated_data_path'], 'test'),
                             generate_sample,
                             template_sampler,
                             [font_supplier1, font_supplier2])
    progress_bar.close()


if __name__ == '__main__':
    main()
