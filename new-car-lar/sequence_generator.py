import gzip
import math
import multiprocessing
import os
import random
import string
import time
from concurrent.futures import ThreadPoolExecutor

import cv2
import numpy as np
from num2words import num2words
from tqdm import tqdm

from image_utils import rotate, remove_white_border_non_binary, InfiniteSampler, is_image, crop_background_area, \
    alter_image_brightness_and_contrast_randomly, to_int, change_foreground_color_and_blend_foreground_on_background, \
    insert_foreground, randomize_foreground, change_brightness, threshold_otsu, random_add_gaussian_noise, \
    speckle_image, random_blur, compress_image, print_text_cropped

SETTINGS = {
    'lar_min_bank_notes_digits': 1,
    'lar_max_bank_notes_digits': 7,
    'lar_bank_notes_currency': ['dinar', 'dollar', 'euro', 'yen', 'pound', 'franc', 'dirham', 'rial', 'riyal'],
    'lar_bank_notes_fractional_currency': ['piastre', 'fils', 'qirsh', 'cent', 'penny', 'rappen', 'sen'],
    'lar_stop_words': ['only'],
    'lar_min_bank_notes_fraction': 0,
    'lar_max_bank_notes_fraction': 999,
    'lar_line_width': 1000,
    'lar_line_height': 64,
    'lar_bank_notes_numbers_percentage': 0.1,
    'lar_bank_notes_fractions_numbers_percentage': 0.25,
    'lar_emnist_dataset_path': './emnist',
    'lar_templates_path': './PS-Templates',
    'lar_handwritten_fonts_path': './lar_handwritten_fonts',
    'lar_printed_font_paths': './fonts',
    'lar_generated_data_path': './data',
    'lar_training_data_size': 1000,
    'lar_validation_data_size': 100,
    'lar_test_data_size': 100,
    'lar_min_space_width': 20,
    'lar_max_space_width': 40,
    'lar_min_character_rotation_angle': -10,
    'lar_max_character_rotation_angle': 10,
    'lar_min_character_resize_percentage': 0.75,
    'lar_max_character_resize_percentage': 1.25,
    'lar_min_margin_size': 0,
    'lar_max_margin_size': 100,
    'lar_min_space_between_characters': 0,
    'lar_max_space_between_characters': 15,
    'lar_average_character_max_side_length': 40,
    'lar_character_min_downscale_side_length': 20,
    'lar_character_min_y_displacement': -10,
    'lar_character_max_y_displacement': 10,
    'lar_cheque_area_error_percentage': 0.01,
    'lar_line1_start_point_percentages': (0.0267, 0.286),
    'lar_line1_size_percentages': (0.667, 0.09143),
    'lar_line2_start_point_percentages': (0.0267, 0.357),
    'lar_line2_size_percentages': (0.667, 0.09143),
    'lar_max_characters_per_line': 80,  # 160 / 2
    'lar_min_font_size': 20,
    'lar_max_font_size': 44,
    'lar_min_character_width': 8
}

EPSILON = 1e-6


def normalize_word(word):
    return ''.join([x for x in word.lower() if x in string.ascii_letters])


def number_to_words(number):
    distinct_words = []
    spelled_number = num2words(number, lang='en').split(' ')
    for word in spelled_number:
        if word.find('-') != -1:
            components = word.split('-')
            distinct_words.extend([normalize_word(word) for word in components])
        else:
            distinct_words.append(normalize_word(word))
    return ' '.join(distinct_words)


def random_bool():
    return random.choice((True, False))


def make_currency_plural(currency):
    if currency[-1] == 's':
        return currency
    if currency[-1] == 'y':
        currency = currency[:-1] + 'ies'
    else:
        currency += 's'
    return currency


def random_make_currency_plural(currency):
    if random_bool():
        currency = make_currency_plural(currency)
    return currency


def random_bank_notes_currency():
    currency = random.choice(SETTINGS['lar_bank_notes_currency'])
    return random_make_currency_plural(currency)


def random_bank_notes_fractional_currency():
    currency = random.choice(SETTINGS['lar_bank_notes_fractional_currency'])
    return random_make_currency_plural(currency)


def random_number_to_lar_label():
    min_digits = SETTINGS['lar_min_bank_notes_digits']
    max_digits = SETTINGS['lar_max_bank_notes_digits']

    intervals = []
    for i in range(min_digits, max_digits + 1):
        if i == 1:
            current_min = 0
        else:
            current_min = 10 ** (i - 1)
        current_max = 10 ** i - 1
        intervals.append((current_min, current_max))

    interval_index = random.normalvariate(len(intervals) / 2, len(intervals) / 4)
    interval_index = max(0.0, interval_index)
    interval_index = min(interval_index, len(intervals) - 0.5)
    interval_index = int(math.floor(interval_index))

    interval = intervals[interval_index]
    number = random.randint(interval[0], interval[1])

    if random.uniform(0, 1) < SETTINGS['lar_bank_notes_numbers_percentage']:
        label = str(number)
    else:
        label = number_to_words(number)

    if random_bool():
        if random_bool():
            label += ' ' + random_bank_notes_currency()
        if random_bool():
            label += ' ' + random.choice(SETTINGS['lar_stop_words'])
    else:
        label += ' ' + random_bank_notes_currency()
        if random_bool():
            label += ' ' + random.choice(SETTINGS['lar_stop_words'])
        if random_bool():
            label += ' and'

        fraction = random.randint(SETTINGS['lar_min_bank_notes_fraction'],
                                  SETTINGS['lar_max_bank_notes_fraction'])
        if random.uniform(0, 1) < SETTINGS['lar_bank_notes_fractions_numbers_percentage']:
            label += ' ' + str(fraction)
        else:
            label += ' ' + number_to_words(fraction)
        if random_bool():
            label += ' ' + random_bank_notes_fractional_currency()
        if random_bool():
            label += ' ' + random.choice(SETTINGS['lar_stop_words'])

    capitalization_mode = random.choice([1, 2, 3, 4])
    if capitalization_mode == 1:
        label = label.upper()
    elif capitalization_mode == 2:
        label = label.lower()
    elif capitalization_mode == 3:
        label = ' '.join([word[:1].upper() + word[1:] for word in label.split(' ')])
    else:
        label = ''.join([x.upper() if random_bool() else x for x in label])

    return label


def load_emnist_classes():
    dataset_path = SETTINGS['lar_emnist_dataset_path']
    classes = os.listdir(dataset_path)
    classes = [c for c in classes if os.path.isdir(os.path.join(dataset_path, c))]

    training_data_classes = dict()
    evaluation_data_classes = dict()

    for c in classes:
        c_hex = int(c, 16)
        c_path = os.path.join(dataset_path, c)
        training_dir_path = os.path.join(c_path, f'train_{c}')
        evaluation_dir_paths = [os.path.join(c_path, f'hsf_{i}') for i in list(range(0, 5)) + list(range(6, 8))]

        training_image_paths = os.listdir(training_dir_path)
        training_image_paths = [os.path.join(training_dir_path, p) for p in training_image_paths if p.endswith('.png')]

        evaluation_image_paths = []
        for dir_path in evaluation_dir_paths:
            image_paths = os.listdir(dir_path)
            image_paths = [os.path.join(dir_path, p) for p in image_paths if p.endswith('.png')]
            evaluation_image_paths.extend(image_paths)

        training_data_classes[chr(c_hex)] = training_image_paths
        evaluation_data_classes[chr(c_hex)] = evaluation_image_paths

    return training_data_classes, evaluation_data_classes


def label_to_image_coordinate_pairs(label, data_classes):
    if label.strip() == '':
        return []

    total_width = 0
    max_height = 0
    character_images = []
    image_resize_ratios = []
    spaces_between_characters = []
    for i in range(len(label)):
        c = label[i]
        if c == ' ':
            space_width = random.randint(SETTINGS['lar_min_space_width'],
                                         SETTINGS['lar_max_space_width'])
            image = np.ones((SETTINGS['lar_line_height'], space_width), np.uint8) * 255
        else:
            image = np.zeros((0, 0), np.uint8)
            while image.shape[0] < 1 or image.shape[1] < 1:
                image = cv2.imread(random.choice(data_classes[c]), cv2.IMREAD_GRAYSCALE)
                if random_bool():
                    image = rotate(image, random.randint(SETTINGS['lar_min_character_rotation_angle'],
                                                         SETTINGS['lar_max_character_rotation_angle']))
                image = remove_white_border_non_binary(image)

        size_adjustment_ratio = 1.0
        if c != ' ':
            max_side_length = max(image.shape[1], image.shape[0])
            if max_side_length < SETTINGS['lar_average_character_max_side_length']:
                size_adjustment_ratio = SETTINGS['lar_average_character_max_side_length'] / max_side_length

        width_resize_ratio = 1.0
        height_resize_ratio = 1.0
        if c != ' ' and random_bool():
            min_percentage = SETTINGS['lar_min_character_resize_percentage']
            if random_bool():
                if image.shape[1] < SETTINGS['lar_character_min_downscale_side_length']:
                    min_percentage = 1.0
                width_resize_ratio = random.uniform(min_percentage,
                                                    SETTINGS['lar_max_character_resize_percentage'])
            if random_bool():
                if image.shape[1] < SETTINGS['lar_character_min_downscale_side_length']:
                    min_percentage = 1.0
                height_resize_ratio = random.uniform(min_percentage,
                                                     SETTINGS['lar_max_character_resize_percentage'])
        width_resize_ratio *= size_adjustment_ratio
        height_resize_ratio *= size_adjustment_ratio
        image_resize_ratios.append([width_resize_ratio, height_resize_ratio])

        space_between_characters = 0
        if 0 < i < len(label) - 1:
            space_between_characters = random.uniform(SETTINGS['lar_min_space_between_characters'],
                                                      SETTINGS['lar_max_space_between_characters'])
            spaces_between_characters.append(space_between_characters)

        character_images.append(image)
        total_width += image.shape[1] * width_resize_ratio + space_between_characters
        max_height = max(max_height, image.shape[0] * height_resize_ratio)

    left_margin = random.uniform(SETTINGS['lar_min_margin_size'],
                                 SETTINGS['lar_max_margin_size'])
    right_margin = random.uniform(SETTINGS['lar_min_margin_size'],
                                  SETTINGS['lar_max_margin_size'])

    total_width += left_margin + right_margin

    if total_width > SETTINGS['lar_line_width']:
        width_resize_ratio = SETTINGS['lar_line_width'] / total_width
        for i in range(len(image_resize_ratios)):
            image_resize_ratios[i][0] *= width_resize_ratio
            if 0 < i < len(label) - 1:
                spaces_between_characters[i - 1] *= width_resize_ratio
        left_margin *= width_resize_ratio
        right_margin *= width_resize_ratio

    if max_height > SETTINGS['lar_line_height']:
        height_resize_ratio = SETTINGS['lar_line_height'] / max_height
        for i in range(len(image_resize_ratios)):
            image_resize_ratios[i][1] *= height_resize_ratio

    character_coordinates = []

    left_margin = int(math.floor(left_margin))
    right_margin = int(math.floor(right_margin))

    current_x = left_margin

    previous_y = random.randint(0, SETTINGS['lar_line_height'])
    previous_height = random.randint(previous_y, SETTINGS['lar_line_height'])

    current_total_truncation_error = 0.0
    for i in range(len(label)):
        image = character_images[i]
        width_ratio, height_ratio = image_resize_ratios[i]
        c = label[i]

        new_width_exact = image.shape[1] * width_ratio
        new_width = int(math.floor(new_width_exact))
        current_total_truncation_error += new_width_exact - new_width
        if current_total_truncation_error >= 1.0:
            new_width += 1
            current_total_truncation_error -= 1.0

        new_height = int(math.floor(image.shape[0] * height_ratio))

        if c == ' ':
            image = np.ones((new_height, new_width), np.uint8) * 255
        elif abs(width_ratio - 1.0) > EPSILON or abs(height_ratio - 1.0) > EPSILON:
            image = cv2.resize(image, (new_width, new_height), interpolation=cv2.INTER_CUBIC)

        x = current_x
        max_y = SETTINGS['lar_line_height'] - image.shape[0]
        y = previous_y + previous_height - image.shape[0]
        y += random.randint(SETTINGS['lar_character_min_y_displacement'],
                            SETTINGS['lar_character_max_y_displacement'])
        y = min(max_y, max(0, y))

        space_between_characters = 0
        if 0 < i < len(label) - 1:
            exact_space_between_characters = spaces_between_characters[i - 1]
            space_between_characters = int(math.floor(exact_space_between_characters))
            current_total_truncation_error += exact_space_between_characters - space_between_characters
            if current_total_truncation_error >= 1.0:
                space_between_characters += 1
                current_total_truncation_error -= 1.0
            x += space_between_characters

        character_coordinates.append([x, y])
        current_x += space_between_characters + image.shape[1]
        character_images[i] = image
        previous_y = y
        previous_height = image.shape[0]

    current_x += right_margin

    if current_x < SETTINGS['lar_line_width'] and random_bool():
        start_x = random.randint(0, SETTINGS['lar_line_width'] - current_x)
        for i in range(len(label)):
            character_coordinates[i][0] += start_x

    return list(zip(character_images, character_coordinates))


def draw_image_coordinate_pairs_on_background(image_coordinate_pairs, background):
    if len(image_coordinate_pairs) == 0:
        return

    background_mean = np.mean(background)
    background_std = np.std(background)
    color_upper_limit = max(min(to_int(background_mean - 2 * background_std), to_int(background_mean - 10)), 0)
    color_lower_limit = random.randint(0, max(min(to_int(background_mean - 3 * background_std),
                                                  to_int(background_mean - 20)), 0))

    foreground_color_type = random.choice([1, 2, 3])
    for foreground_image, (x, y) in image_coordinate_pairs:
        if foreground_color_type == 1:
            foreground_image = change_brightness(threshold_otsu(foreground_image),
                                                 random.randint(color_lower_limit, color_upper_limit))
            insert_foreground(background,
                              foreground_image,
                              (x, y))
        elif foreground_color_type == 2:
            change_foreground_color_and_blend_foreground_on_background(background,
                                                                       foreground_image,
                                                                       (x, y),
                                                                       random.randint(color_lower_limit,
                                                                                      color_upper_limit))
        else:
            foreground_image = randomize_foreground(foreground_image, color_lower_limit, color_upper_limit)
            insert_foreground(background,
                              foreground_image,
                              (x, y))


def draw_printed_text_on_background(text,
                                    background,
                                    font_supplier):
    if text.strip() == '':
        return

    background_mean = np.mean(background)
    background_std = np.std(background)
    color_upper_limit = max(min(to_int(background_mean - 2 * background_std), to_int(background_mean - 10)), 0)
    color_lower_limit = random.randint(0, max(min(to_int(background_mean - 3 * background_std),
                                                  to_int(background_mean - 20)), 0))

    foreground_color = random.randint(color_lower_limit, color_upper_limit)
    foreground_image = print_text_cropped(text,
                                          font_supplier.next(),
                                          random.randint(SETTINGS['lar_min_font_size'],
                                                         SETTINGS['lar_max_font_size']),
                                          foreground_color,
                                          (SETTINGS['lar_line_width'],
                                           SETTINGS['lar_line_height']),
                                          SETTINGS['lar_max_font_size'])

    x = random.randint(0, background.shape[1] - foreground_image.shape[1])
    y = random.randint(0, background.shape[0] - foreground_image.shape[0])

    foreground_color_type = random.choice([1, 2, 3])
    if foreground_color_type == 1:
        foreground_image = change_brightness(threshold_otsu(foreground_image),
                                             foreground_color)
        insert_foreground(background,
                          foreground_image,
                          (x, y))
    elif foreground_color_type == 2:
        change_foreground_color_and_blend_foreground_on_background(background,
                                                                   foreground_image,
                                                                   (x, y),
                                                                   foreground_color)
    else:
        foreground_image = randomize_foreground(foreground_image, color_lower_limit, color_upper_limit)
        insert_foreground(background,
                          foreground_image,
                          (x, y))


def generate_sample(template_sampler,
                    emnist_data_classes,
                    font_suppliers):
    while True:
        try:
            template = cv2.imread(template_sampler.next(), cv2.IMREAD_GRAYSCALE)

            lar_line1_background = crop_background_area(template,
                                                        SETTINGS['lar_line1_start_point_percentages'],
                                                        SETTINGS['lar_line1_size_percentages'],
                                                        SETTINGS['lar_cheque_area_error_percentage'])

            lar_line2_background = crop_background_area(template,
                                                        SETTINGS['lar_line2_start_point_percentages'],
                                                        SETTINGS['lar_line2_size_percentages'],
                                                        SETTINGS['lar_cheque_area_error_percentage'])

            lar_line1_background = cv2.resize(lar_line1_background,
                                              (SETTINGS['lar_line_width'], SETTINGS['lar_line_height']),
                                              interpolation=cv2.INTER_CUBIC)
            lar_line1_background = alter_image_brightness_and_contrast_randomly(lar_line1_background)

            lar_line2_background = cv2.resize(lar_line2_background,
                                              (SETTINGS['lar_line_width'], SETTINGS['lar_line_height']),
                                              interpolation=cv2.INTER_CUBIC)
            lar_line2_background = alter_image_brightness_and_contrast_randomly(lar_line2_background)

            lar_label = random_number_to_lar_label()

            if len(lar_label) > SETTINGS['lar_max_characters_per_line']:
                line2_label_index = random.randint(len(lar_label) - SETTINGS['lar_max_characters_per_line'],
                                                   SETTINGS['lar_max_characters_per_line'])
                i = lar_label.find(' ', line2_label_index)
                if i == -1:
                    i = lar_label.rfind(' ')
                if i == -1:
                    i = len(lar_label)
                else:
                    i += random.randint(0, 1)
                line2_label_index = i

                line1_label = lar_label[:line2_label_index]
                line2_label = lar_label[line2_label_index:]
            else:
                if random_bool():
                    line1_label = ''
                    line2_label = lar_label
                else:
                    line1_label = lar_label
                    line2_label = ''

            if random_bool():
                line1_image_coordinate_pairs = label_to_image_coordinate_pairs(line1_label, emnist_data_classes)
                line2_image_coordinate_pairs = label_to_image_coordinate_pairs(line2_label, emnist_data_classes)
                draw_image_coordinate_pairs_on_background(line1_image_coordinate_pairs, lar_line1_background)
                draw_image_coordinate_pairs_on_background(line2_image_coordinate_pairs, lar_line2_background)
            else:
                font_supplier = random.choice(font_suppliers)
                draw_printed_text_on_background(line1_label, lar_line1_background, font_supplier)
                draw_printed_text_on_background(line2_label, lar_line2_background, font_supplier)
            lar_combined_line = np.concatenate([lar_line1_background, lar_line2_background], axis=-1)

            if random_bool():
                if random_bool():
                    lar_combined_line = random_add_gaussian_noise(lar_combined_line, 0, random.randint(10, 20),
                                                                  (-20, 20))
                else:
                    lar_combined_line = speckle_image(lar_combined_line, 0.01, random.randint(0, 255))

                if random_bool():
                    lar_combined_line = random_blur(lar_combined_line)
                else:
                    lar_combined_line = compress_image(lar_combined_line, random.randint(50, 100))

            return lar_combined_line, lar_label
        except Exception as e:
            print(f"An exception occurred: {str(e)}")


def load_font_paths(path):
    font_paths = os.listdir(path)
    font_paths = [f for f in font_paths if f.endswith('.ttf') or f.endswith('.otf')]
    font_paths = [os.path.join(path, f) for f in font_paths]
    return font_paths


def parallel_data_generation(output_data_count,
                             start_index,
                             progress_bar,
                             generation_path,
                             data_generation_function,
                             *function_args):
    progress_lock = multiprocessing.Lock()
    indices = list(range(start_index, start_index + output_data_count))
    random.shuffle(indices)

    if not os.path.isdir(generation_path):
        os.makedirs(generation_path)

    def wrapped_data_generation_function(*args):
        image, label = data_generation_function(*args)

        with progress_lock:
            current_index = indices.pop()

        with open(os.path.join(generation_path, f'{current_index}.txt'), 'w') as f:
            f.write(label)
        _, buffer = cv2.imencode('.png', 255 - image)
        with gzip.open(os.path.join(generation_path, f'{current_index}.png'), 'wb') as f:
            f.write(buffer)

        with progress_lock:
            progress_bar.update()

    while len(indices) > 0:
        with ThreadPoolExecutor(max_workers=multiprocessing.cpu_count() * 2) as executor:
            for _ in range(len(indices)):
                executor.submit(wrapped_data_generation_function,
                                *function_args)
        time.sleep(1)


def main():
    training_data_classes, evaluation_data_classes = load_emnist_classes()

    paths = os.listdir(SETTINGS['lar_templates_path'])
    paths = [os.path.join(SETTINGS['lar_templates_path'], p) for p in paths]
    paths = [p for p in paths if is_image(p)]
    template_sampler = InfiniteSampler(paths)
    font_supplier1 = InfiniteSampler(load_font_paths(SETTINGS['lar_handwritten_fonts_path']))
    font_supplier2 = InfiniteSampler(load_font_paths(SETTINGS['lar_printed_font_paths']))

    progress_bar = tqdm(total=SETTINGS['lar_training_data_size'])
    parallel_data_generation(SETTINGS['lar_training_data_size'],
                             1,
                             progress_bar,
                             os.path.join(SETTINGS['lar_generated_data_path'], 'train'),
                             generate_sample,
                             template_sampler,
                             training_data_classes,
                             [font_supplier1, font_supplier2])
    progress_bar.close()

    progress_bar = tqdm(total=SETTINGS['lar_validation_data_size'])
    parallel_data_generation(SETTINGS['lar_validation_data_size'],
                             1,
                             progress_bar,
                             os.path.join(SETTINGS['lar_generated_data_path'], 'validation'),
                             generate_sample,
                             template_sampler,
                             evaluation_data_classes,
                             [font_supplier1, font_supplier2])
    progress_bar.close()

    progress_bar = tqdm(total=SETTINGS['lar_test_data_size'])
    parallel_data_generation(SETTINGS['lar_test_data_size'],
                             1,
                             progress_bar,
                             os.path.join(SETTINGS['lar_generated_data_path'], 'test'),
                             generate_sample,
                             template_sampler,
                             evaluation_data_classes,
                             [font_supplier1, font_supplier2])
    progress_bar.close()


if __name__ == '__main__':
    main()
