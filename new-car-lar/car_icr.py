import math
import os
import pickle
import random
from enum import Enum

import cv2
import editdistance
import numpy as np
import tensorflow as tf

keras = tf.keras

from keras import Input, Model
from keras.callbacks import LambdaCallback, ModelCheckpoint
from keras.layers import Conv2D, MaxPooling2D, Dense, Reshape, LSTM, BatchNormalization, LeakyReLU, \
    Concatenate, UpSampling2D, Conv1D, ZeroPadding2D
from keras.initializers import GlorotNormal, GlorotUniform, HeUniform
from keras.optimizers import Adam

from keras.utils import Sequence
from tqdm import tqdm
import tensorflow as tf

SETTINGS = {
    'input_width': 200,
    'input_height': 36,
    'classes': '0123456789,.#*-=/\\| ',
    'car_train_data_path': './data/train',
    'car_validation_data_path': './data/validation',
    'car_test_data_path': './data/test',
    'car_training_batch_size': 64,
    'car_evaluation_batch_size': 128,
    'car_learning_rates_per_training_cycle': [
        0.001,
        0.0001,
        0.00001
    ],
    'model_file_name': 'car',
    'epochs_per_training_cycle': 20,
    'rnn_output_start_index': 0,
}

BLANK_CLASS = len(SETTINGS['classes'])

LABEL_CONCAT_CHAR = ''

TRAINING_CYCLES = len(SETTINGS['car_learning_rates_per_training_cycle'])
TRAINING_BATCH_SIZE = SETTINGS['car_training_batch_size']
EVALUATION_BATCH_SIZE = SETTINGS['car_evaluation_batch_size']

random.seed(12345)
np.random.seed(12345)
tf.random.set_seed(12345)


class DataType(Enum):
    TRAIN_DATA = 1
    VALIDATION_DATA = 2
    TEST_DATA = 3


def get_input_shape():
    return SETTINGS['input_height'], SETTINGS['input_width'], 1


def pre_process(image):
    return image.reshape(get_input_shape()).astype(np.float32) / 127.5 - 1.0


def ctc_loss_factory(rnn_time_steps):
    def ctc_loss(y_true, y_pred):
        logits = y_pred[:, SETTINGS['rnn_output_start_index']:, :]
        input_length = tf.repeat(rnn_time_steps - SETTINGS['rnn_output_start_index'], tf.shape(y_true)[0])
        label_length = tf.math.count_nonzero(y_true != BLANK_CLASS, axis=-1)
        return tf.nn.ctc_loss(y_true,
                              logits,
                              label_length,
                              input_length,
                              logits_time_major=False,
                              blank_index=BLANK_CLASS)

    return ctc_loss


class DataGenerator(Sequence):
    def __init__(self,
                 input_image_paths,
                 labels,
                 max_label_length,
                 batch_size):
        count = len(input_image_paths)
        self.count = count
        self.batch_size = batch_size
        self.max_label_length = max_label_length
        indices = list(range(count))
        random.shuffle(indices)
        self.input_image_paths = [input_image_paths[i] for i in indices]
        self.labels = [labels[i] for i in indices]

    def __getitem__(self, index):
        input_image_paths = self.input_image_paths[index * self.batch_size: (index + 1) * self.batch_size]
        input_images = [pre_process(cv2.imread(f, cv2.IMREAD_GRAYSCALE)) for f in input_image_paths]
        output_labels = self.labels[index * self.batch_size: (index + 1) * self.batch_size]

        return np.array(input_images, np.float32), np.array(output_labels, np.uint8)

    def __len__(self):
        return int(math.ceil(self.count / self.batch_size))

    def on_epoch_end(self):
        indices = list(range(self.count))
        random.shuffle(indices)
        self.input_image_paths = [self.input_image_paths[i] for i in indices]
        self.labels = [self.labels[i] for i in indices]


def path_from_data_type(data_type):
    if data_type == DataType.TRAIN_DATA:
        path = SETTINGS['car_train_data_path']
    elif data_type == DataType.VALIDATION_DATA:
        path = SETTINGS['car_validation_data_path']
    else:
        path = SETTINGS['car_test_data_path']
    return path


def conv_batch_norm_leaky_relu_layer(filters, kernel_shape, input_layer, padding='same'):
    convolution = Conv2D(filters,
                         kernel_shape,
                         padding=padding,
                         use_bias=False,
                         kernel_initializer=GlorotNormal())(input_layer)
    convolution = BatchNormalization()(convolution)
    return LeakyReLU(0.05)(convolution)


def conv1d_batch_norm_leaky_relu_layer(filters, kernel_size, dilation_rate, input_layer, padding='same'):
    convolution = Conv1D(filters,
                         kernel_size,
                         dilation_rate=dilation_rate,
                         padding=padding,
                         use_bias=False,
                         kernel_initializer=HeUniform())(input_layer)
    convolution = BatchNormalization()(convolution)
    return LeakyReLU(0.05)(convolution)


def conv_net(input_layer, input_width, unet_conv_filters):
    time_steps = input_width
    batch_norm = BatchNormalization()(input_layer)
    # feature level 1
    conv1 = conv_batch_norm_leaky_relu_layer(unet_conv_filters, (3, 3), batch_norm)
    pool1 = MaxPooling2D(pool_size=(2, 2))(conv1)
    # feature level 2
    conv2 = conv_batch_norm_leaky_relu_layer(unet_conv_filters, (3, 3), pool1)
    pool2 = MaxPooling2D(pool_size=(2, 2))(conv2)
    # feature level 3
    conv3 = conv_batch_norm_leaky_relu_layer(unet_conv_filters, (2, 3), pool2, padding='valid')
    pool3 = MaxPooling2D(pool_size=(2, 2))(conv3)
    # feature level 4
    conv4 = conv_batch_norm_leaky_relu_layer(unet_conv_filters, (3, 3), pool3)
    pool4 = MaxPooling2D(pool_size=(2, 2))(conv4)
    # bottleneck
    conv5 = conv_batch_norm_leaky_relu_layer(unet_conv_filters, (2, 2), pool4)
    # combine bottleneck with feature level 4
    upsample1 = UpSampling2D()(conv5)
    concat1 = Concatenate(axis=-1)([upsample1, conv4])
    conv6 = conv_batch_norm_leaky_relu_layer(unet_conv_filters, (3, 3), concat1)
    # combine previous with feature level 3
    upsample2 = UpSampling2D()(conv6)
    concat2 = Concatenate(axis=-1)([upsample2, conv3])
    conv7 = conv_batch_norm_leaky_relu_layer(unet_conv_filters, (3, 3), concat2)
    # combine previous with feature level 2
    upsample3 = UpSampling2D()(conv7)
    upsample3 = ZeroPadding2D(padding=(1, 2))(upsample3)
    concat3 = Concatenate(axis=-1)([upsample3, conv2])
    conv8 = conv_batch_norm_leaky_relu_layer(unet_conv_filters, (3, 3), concat3)
    # combine previous with feature level 1
    upsample4 = UpSampling2D()(conv8)
    concat4 = Concatenate(axis=-1)([upsample4, conv1])
    conv9 = conv_batch_norm_leaky_relu_layer(unet_conv_filters, (3, 3), concat4)
    # compress vertically 1
    pool5 = MaxPooling2D(pool_size=(2, 2), strides=(2, 2))(conv9)
    time_steps //= 2
    # compress vertically 2
    conv11 = conv_batch_norm_leaky_relu_layer(unet_conv_filters * 2, (3, 1), pool5)
    pool6 = MaxPooling2D(pool_size=(2, 1), strides=(2, 1))(conv11)
    # compress vertically 3
    conv12 = conv_batch_norm_leaky_relu_layer(unet_conv_filters * 3, (2, 1), pool6, padding='valid')
    pool7 = MaxPooling2D(pool_size=(2, 1), strides=(2, 1))(conv12)
    # compress vertically 4
    conv13 = conv_batch_norm_leaky_relu_layer(unet_conv_filters * 4, (2, 1), pool7)
    pool8 = MaxPooling2D(pool_size=(2, 1), strides=(2, 1))(conv13)
    # output sequence
    last_conv_layer_filters = unet_conv_filters * 5
    conv14 = conv_batch_norm_leaky_relu_layer(last_conv_layer_filters, (2, 1), pool8, padding='valid')
    return conv14, last_conv_layer_filters, time_steps


def build_lstm_model(unet_conv_filters=64, rnn_size=256):
    input_shape = get_input_shape()
    input_layer = Input(name='input_layer',
                        shape=input_shape)

    time_steps = input_shape[1]

    embedding, last_conv_layer_filters, time_steps = conv_net(input_layer, time_steps, unet_conv_filters)

    print(f'model time steps: {time_steps}')
    update_report(f'model time steps: {time_steps}')

    embedding = Reshape(target_shape=(time_steps, last_conv_layer_filters))(embedding)
    embedding = Dense(rnn_size,
                      use_bias=False,
                      kernel_initializer=GlorotNormal())(embedding)
    embedding = BatchNormalization()(embedding)
    embedding = LeakyReLU(0.05)(embedding)
    # embedding = Dropout(0.5)(embedding)

    lstm1 = LSTM(rnn_size,
                 return_sequences=True)(embedding)
    backward_lstm1 = LSTM(rnn_size,
                          return_sequences=True,
                          go_backwards=True)(embedding)
    lstm1_merged = Concatenate()([lstm1, backward_lstm1])

    lstm2 = LSTM(rnn_size,
                 return_sequences=True)(lstm1_merged)
    backward_lstm2 = LSTM(rnn_size,
                          return_sequences=True,
                          go_backwards=True)(lstm1_merged)
    lstm2_merged = Concatenate()([lstm2, backward_lstm2])

    # the additional class is reserved for the ctc loss blank character
    classes = len(SETTINGS['classes']) + 1

    # the output of this layer is required for the tensorflow ctc_beam_search_decoder
    logits_layer = Dense(classes,
                         kernel_initializer=GlorotNormal(),
                         name='logits')(lstm2_merged)

    return Model(inputs=[input_layer], outputs=logits_layer), time_steps


def build_fully_conv_model(unet_conv_filters=64, rnn_size=256):
    input_shape = get_input_shape()
    input_layer = Input(name='input_layer',
                        shape=input_shape)

    time_steps = input_shape[1]

    embedding, last_conv_layer_filters, time_steps = conv_net(input_layer, time_steps, unet_conv_filters)

    print(f'model time steps: {time_steps}')
    update_report(f'model time steps: {time_steps}')

    embedding = Reshape(target_shape=(time_steps, last_conv_layer_filters))(embedding)
    embedding = Dense(rnn_size,
                      use_bias=False,
                      kernel_initializer=HeUniform())(embedding)
    embedding = BatchNormalization()(embedding)
    embedding = LeakyReLU(0.05)(embedding)

    embedding = conv1d_batch_norm_leaky_relu_layer(rnn_size, 3, 1, embedding)
    embedding = conv1d_batch_norm_leaky_relu_layer(rnn_size, 3, 1, embedding)

    embedding = conv1d_batch_norm_leaky_relu_layer(rnn_size, 3, 2, embedding)
    embedding = conv1d_batch_norm_leaky_relu_layer(rnn_size, 3, 2, embedding)

    embedding = conv1d_batch_norm_leaky_relu_layer(rnn_size, 3, 4, embedding)
    embedding = conv1d_batch_norm_leaky_relu_layer(rnn_size, 3, 4, embedding)

    embedding = conv1d_batch_norm_leaky_relu_layer(rnn_size, 3, 8, embedding)
    embedding = conv1d_batch_norm_leaky_relu_layer(rnn_size, 3, 8, embedding)

    # the additional class is reserved for the ctc loss blank character
    classes = len(SETTINGS['classes']) + 1

    # the output of this layer is required for the tensorflow ctc_beam_search_decoder
    logits_layer = Dense(classes,
                         kernel_initializer=GlorotUniform(),
                         name='logits')(embedding)

    return Model(inputs=[input_layer], outputs=logits_layer), time_steps


def update_report(line='', report_file=SETTINGS['model_file_name'] + '_report.txt'):
    with open(report_file, 'a') as f:
        f.write(line + '\n')


def load_images_paths_and_labels(data_type=DataType.TRAIN_DATA):
    path = path_from_data_type(data_type)
    files = os.listdir(path)

    image_files = [f for f in files if f.endswith('.png')]

    max_label_length = 0
    image_paths = []
    labels = []
    for image_file in image_files:
        image_file_path = os.path.join(path, image_file)
        if not os.path.isfile(image_file_path):
            continue
        file_name = os.path.splitext(image_file)[0]
        label_file_path = os.path.join(path, f'{file_name}.txt')
        if not os.path.isfile(label_file_path):
            continue
        image_paths.append(image_file_path)
        with open(label_file_path, 'r') as file:
            label = file.read().strip()
        if len(label) > max_label_length:
            max_label_length = len(label)
        labels.append(label)

    encoded_labels = []
    label_index_dict = dict()
    for i, c in enumerate(SETTINGS['classes']):
        label_index_dict[c] = i

    for label in labels:
        encoded_label = [label_index_dict[l] for l in label]
        encoded_label += [BLANK_CLASS] * (max_label_length - len(encoded_label))
        encoded_labels.append(encoded_label)

    return image_paths, encoded_labels, max_label_length


def train(model,
          cycle_number,
          train_files,
          train_labels,
          validation_files,
          validation_labels,
          max_label_length):
    all_models_checkpoint = ModelCheckpoint(f'{SETTINGS["model_file_name"]}_checkpoint_{cycle_number}' +
                                            '_{epoch:02d}_{loss:0.4f}_{val_loss:0.4f}.h5')
    best_model_checkpoint = ModelCheckpoint(f'{SETTINGS["model_file_name"]}_checkpoint_best_{cycle_number}.h5',
                                            monitor='val_loss',
                                            save_best_only=True,
                                            mode='min')

    update_report('======= training results =======')

    update_report_callback = LambdaCallback(on_epoch_end=lambda epoch, logs: update_report(
        f'cycle: {cycle_number} - epoch: {epoch} - loss: {logs["loss"]} - validation loss: {logs["val_loss"]}'
    ))

    model.fit(DataGenerator(train_files,
                            train_labels,
                            max_label_length,
                            TRAINING_BATCH_SIZE),
              validation_data=DataGenerator(validation_files,
                                            validation_labels,
                                            max_label_length,
                                            EVALUATION_BATCH_SIZE),
              epochs=SETTINGS['epochs_per_training_cycle'],
              callbacks=[all_models_checkpoint,
                         best_model_checkpoint,
                         update_report_callback])

    update_report()


def predict_batch(model, images, rnn_time_steps, greedy=False, merge_repeated=False):
    predictions = model(images).numpy()[:, SETTINGS['rnn_output_start_index']:, :]
    time_steps = rnn_time_steps - SETTINGS['rnn_output_start_index']

    decoded_batch = []
    probabilities = []
    for prediction in predictions:
        if greedy:
            softmax = tf.nn.softmax(prediction)
            decoded = tf.argmax(softmax, axis=-1)
            max_prob = tf.math.reduce_max(softmax, axis=-1)
            max_prob_log = tf.math.log(max_prob + 1e-7)
            prob = tf.reduce_sum(max_prob_log).numpy()
            prob = np.exp(prob)
            probabilities.append(prob)
            decoded_batch.append(decoded.numpy().tolist())
        else:
            decoded, log_prob = tf.nn.ctc_beam_search_decoder(
                prediction.reshape((time_steps, 1, len(SETTINGS['classes']) + 1)),
                [time_steps])
            probabilities.append(np.exp(log_prob[0]))
            decoded_batch.append(tf.sparse.to_dense(decoded[0]).numpy()[0].tolist())

    labels = []
    for seq in decoded_batch:
        if merge_repeated:
            start_i = 0
            merged = [seq[start_i]]
            for i in range(1, len(seq)):
                if seq[i] != seq[start_i]:
                    merged.append(seq[i])
                    start_i = i
            labels.append(LABEL_CONCAT_CHAR.join([SETTINGS['classes'][c] for c in merged if c != BLANK_CLASS]).strip())
        else:
            labels.append(LABEL_CONCAT_CHAR.join([SETTINGS['classes'][c] for c in seq if c != BLANK_CLASS]).strip())

    return labels, probabilities


def evaluate(model, test_files, test_labels, rnn_time_steps, max_label_length):
    update_report('======= evaluation results =======')

    results = model.evaluate_generator(DataGenerator(test_files,
                                                     test_labels,
                                                     max_label_length,
                                                     EVALUATION_BATCH_SIZE))

    update_report(f'test loss: {results}')

    evaluate_model(model,
                   test_files,
                   test_labels,
                   rnn_time_steps,
                   max_label_length)


def evaluate_model(model,
                   test_files,
                   test_labels,
                   rnn_time_steps,
                   max_label_length):
    distances_mean = 0.0
    distance_ratios_mean = 0.0
    amount_overall_accuracy = 0

    test_data_generator = DataGenerator(test_files,
                                        test_labels,
                                        max_label_length,
                                        EVALUATION_BATCH_SIZE)
    batches = len(test_data_generator)
    progress_bar = tqdm(total=batches)

    n = 0
    for i in range(batches):
        image_batch, labels_batch = test_data_generator[i]
        predictions, probabilities = predict_batch(model, image_batch, rnn_time_steps)

        for j in range(len(predictions)):
            predicted = predictions[j]
            label = labels_batch[j]
            label = LABEL_CONCAT_CHAR.join([SETTINGS['classes'][c] for c in label if c != BLANK_CLASS]).strip()

            edit_distance = editdistance.eval(predicted, label)
            distances_mean += edit_distance

            if len(label) == 0:
                distance_ratios_mean += edit_distance
            else:
                distance_ratios_mean += edit_distance / len(label)

            if label == predicted:
                amount_overall_accuracy += 1

        n += len(labels_batch)
        progress_bar.update()

    progress_bar.close()

    distances_mean /= n
    distance_ratios_mean /= n
    amount_overall_accuracy /= n

    update_report(f'average edit distances: {distances_mean}')
    update_report(f'character recognition error rate: {distance_ratios_mean}')
    update_report(f'character recognition accuracy: {100 - distance_ratios_mean * 100}')
    update_report(f'label recognition error rate: {1 - amount_overall_accuracy}')
    update_report(f'label recognition accuracy: {amount_overall_accuracy * 100}')

    return distances_mean, distance_ratios_mean, amount_overall_accuracy


def serialize_object(obj, file):
    with open(file, 'wb') as f:
        pickle.dump(obj, f)


def deserialize_object(file):
    with open(file, 'rb') as f:
        return pickle.load(f)


def run_pipeline():
    if not os.path.isfile('train_data.pickle') or \
            not os.path.isfile('validation_data.pickle') or \
            not os.path.isfile('test_data.pickle'):
        train_data = load_images_paths_and_labels(DataType.TRAIN_DATA)
        validation_data = load_images_paths_and_labels(DataType.VALIDATION_DATA)
        test_data = load_images_paths_and_labels(DataType.TEST_DATA)

        serialize_object(train_data, 'train_data.pickle')
        serialize_object(validation_data, 'validation_data.pickle')
        serialize_object(test_data, 'test_data.pickle')
    else:
        train_data = deserialize_object('train_data.pickle')
        validation_data = deserialize_object('validation_data.pickle')
        test_data = deserialize_object('test_data.pickle')

    train_files, train_labels, max_label_length1 = train_data
    validation_files, validation_labels, max_label_length2 = validation_data
    test_files, test_labels, max_label_length3 = test_data

    max_label_length = max(max_label_length1, max_label_length2, max_label_length3)
    print(f'max label length: {max_label_length}')
    update_report(f'max label length: {max_label_length}')

    for cycle in range(1, TRAINING_CYCLES + 1):
        keras.backend.clear_session()
        model, rnn_time_steps = build_lstm_model()
        model.summary()
        # plot_model(model,
        #            to_file='car.png',
        #            show_shapes=True)
        print(f'\ncycle: {cycle}')
        if cycle > 1:
            model.load_weights(f'{SETTINGS["model_file_name"]}_checkpoint_best_{cycle - 1}.h5')
        model.compile(loss=ctc_loss_factory(rnn_time_steps),
                      optimizer=Adam(learning_rate=SETTINGS['car_learning_rates_per_training_cycle'][cycle - 1],
                                     clipnorm=4.0))

        print('\ntraining model...\n')
        train(model,
              cycle,
              train_files,
              train_labels,
              validation_files,
              validation_labels,
              max_label_length)

    keras.backend.clear_session()
    model, rnn_time_steps = build_lstm_model()
    model.load_weights(f'{SETTINGS["model_file_name"]}_checkpoint_best_{TRAINING_CYCLES}.h5')
    model.compile(loss=ctc_loss_factory(rnn_time_steps),
                  optimizer=Adam())
    print('\nevaluating model...\n')
    evaluate(model,
             test_files,
             test_labels,
             rnn_time_steps,
             max_label_length)


if __name__ == '__main__':
    run_pipeline()
