import os
import uuid
import shutil
import json


def get_unique_id():
    return str(uuid.uuid1()).lower()


def main():
    accounts = os.listdir(".")
    accounts = [d for d in accounts if os.path.isdir(d)]

    if not os.path.isdir("DataFiles"):
        os.makedirs("DataFiles")

    accounts_dict = {"Accounts": []}

    for i in range(len(accounts)):
        account_number = accounts[i]
        account_name = f"Account {i + 1}"
        account_status = "Normal"

        files = os.listdir(account_number)
        signature_images = [f for f in files if f.startswith(account_number)]

        signatories = []
        for j in range(len(signature_images)):
            signature_image = signature_images[j]
            signature_id = get_unique_id()
            signature_image_path = f"{signature_id}.jpg"

            signatory_id = get_unique_id()
            signatory_name = f"Signatory {j + 1}"
            signatory_notes = "signatory note"

            shutil.copy(
                os.path.join(account_number, signature_image),
                os.path.join("DataFiles", signature_image_path),
            )

            signatories.append(
                {
                    "Id": signatory_id,
                    "Name": signatory_name,
                    "Notes": signatory_notes,
                    "Signatures": [
                        {
                            "Id": signature_id,
                            "ImagePath": signature_image_path,
                        }
                    ],
                }
            )

        accounts_dict["Accounts"].append(
            {
                "Number": account_number,
                "Name": account_name,
                "Status": account_status,
                "Signatories": signatories,
                "AmountRules": [],
            }
        )

    with open("DataFiles/SigDemoSystem.json", "w") as f:
        json.dump(accounts_dict, f)


if __name__ == "__main__":
    main()
