import os
import uuid
import shutil


def main():
    target_dir = "./templates"
    if not os.path.isdir(target_dir):
        os.makedirs(target_dir)

    for root, dirs, files in os.walk("."):
        for file in files:
            if file.endswith('.PAK'):
                new_name = str(uuid.uuid4())
                path = os.path.join(root, file)
                shutil.copy(path, os.path.join(target_dir, f"{new_name}"))


if __name__ == "__main__":
    main()
