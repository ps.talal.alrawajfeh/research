#!/usr/bin/python3.6

import pandas as pd
from sklearn import linear_model

# the points (1, 2), (3, 4), (5, 6) must be written with each coordinate in a separate list
x_train = pd.DataFrame.from_dict({'x1': [1, 3, 5], 'x2': [2, 4, 6]})
y_train = pd.DataFrame.from_dict({'y': [3, 7, 11]})

regressor = linear_model.LinearRegression()

model = regressor.fit(x_train, y_train)
model.fit(x_train, y_train)

x_test = pd.DataFrame.from_dict({'x1': [7, 9], 'x2': [8, 10]})
y_test = pd.DataFrame.from_dict({'y': [15, 19]})

predictions = model.predict(x_test)

accuracy = 0
for i in range(len(predictions)):
    if predictions[i] == y_test['y'][i]:
        accuracy += 1

accuracy /= len(predictions)

print('accuracy:', accuracy * 100)
