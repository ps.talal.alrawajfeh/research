# K6 Performance Report

## **CPU Benchmark**

| Number of Users | Account Loading | Signatures Image Loading | Document Verification |
| --------------- | --------------- | ------------------------ | ----------------------|
| 1               | `7.15ms`        | `1.0ms`                  | `735ms`               |
| 5               | `10.4ms`        | `1.2ms`                  | `2s`                  |
| 10              | `13.8ms`        | `2.2ms`                  | `4.1s`                |
| 15              | `18.5ms`        | `3.15ms`                 | `6.3s`                |
| 20              | `22.5ms`        | `4.2ms`                  | `8.1s`                |

<br/>
<br/>

## **GPU Benchmark**

| Number of Users | Document Verification |
| --------------- | ----------------------|
| 1               | `225ms`               |
| 5               | `360ms`               |
| 10              | `620ms`               |
| 15              | `900ms`               |
| 20              | `1.32s`               |

<br/>
<br/>
<br/>

# Models Performance Report

## **CPU Benchmark**

| Model       | Average Time |
| ----------- | ------------ |
| DenseNet    | `100ms`      |
| Triplet     | `1ms`        |
| Siamese     | `2.5ms`      |
| Autoencoder | `500ms`      |

<br/>
<br/>


## **GPU Benchmark**

| Model       | Average Time |
| ----------- | ------------ |
| DenseNet    | `42ms`       |
| Triplet     | `0.5ms`      |
| Siamese     | `1.5ms`      |
| Autoencoder | `170ms`      |
