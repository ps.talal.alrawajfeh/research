import cv2
import numpy as np
import math
import time


def main():
    image = cv2.imread('test.bmp', cv2.IMREAD_GRAYSCALE)
    # image = cv2.imread('test-net-core.bmp', cv2.IMREAD_GRAYSCALE)

    resized1 = cv2.resize(image, (748 * 4, 200 * 4),
                          interpolation=cv2.INTER_CUBIC)

    # start = time.time()
    # for i in range(10000):
    #     resized1 = cv2.resize(image, (748 * 4, 200 * 4),
    #                           interpolation=cv2.INTER_CUBIC)
    # end = time.time()

    # print((end - start) / 10000)

    resized2 = cv2.imread('test-net-core.bmp', cv2.IMREAD_GRAYSCALE)

    print(1.0 - np.count_nonzero(resized1 != resized2) / np.prod(resized1.shape))
    # return
    coords = np.transpose(np.where(resized1 != resized2))
    print(coords)

    print('------------------------')

    y = 0
    x = 0
    c = 0

    color1 = resized1[y, x]
    color2 = resized2[y, x]

    inv_scale_x = image.shape[1] / 748
    inv_scale_y = image.shape[0] / 200

    sx = (x + 0.5) * inv_scale_x - 0.5
    sy = (y + 0.5) * inv_scale_y - 0.5

    sxf = math.floor(sx)
    syf = math.floor(sy)

    ix1 = 1 + sx - sxf
    ix2 = sx - sxf
    ix3 = sxf + 1 - sx
    ix4 = sxf + 2 - sx

    iy1 = 1 + sy - syf
    iy2 = sy - syf
    iy3 = syf + 1 - sy
    iy4 = syf + 2 - sy

    x1 = int(sx - ix1)
    x2 = int(sx - ix2)
    x3 = int(sx + ix3)
    x4 = int(sx + ix4)

    y1 = int(sy - iy1)
    y2 = int(sy - iy2)
    y3 = int(sy + iy3)
    y4 = int(sy + iy4)

    print((x, y))
    print((x1, y1), (x2, y2), (x3, y3), (x4, y4))

    print('------------------------')
    try:
        print(image[y1, x1])
    except:
        print('')

    try:
        print(image[y2, x2])
    except:
        print('')

    try:
        print(image[y3, x3])
    except:
        print('')

    try:
        print(image[y4, x4])
    except:
        pass
    print('------------------------')
    print(color1)
    print(color2)


if __name__ == '__main__':
    main()
