import json
import os
import pickle
import string
from datetime import datetime
from enum import Enum

import numpy as np
import sqlalchemy as db
from scipy.spatial import distance
from sklearn.cluster import MiniBatchKMeans
from sklearn.decomposition import TruncatedSVD
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import Normalizer
from tqdm import tqdm

from issues_search import query_table_as_df, combine_into_one_document, prepare_projects, prepare_lookups, \
    normalize_text

with open('config.json', 'r') as config_file:
    config = json.load(config_file)
SUPPORT_DB_URL = config['support_db_url']
CLUSTERING_TOP_NUMBER_OF_ISSUES_TO_DISPLAY = config['clustering_top_number_of_issues_to_display']

MAX_NUMBER_OF_KEYWORDS = 128
MIN_DOCUMENT_FREQUENCY = 2
MAX_DOCUMENT_FREQUENCY = 0.5
CLUSTERING_MIN_ISSUE_COUNT = 100
ISSUES_FEATURES_FACTOR = 0.2
ISSUES_CLUSTERS_FACTOR = 0.1
FINAL_FEATURE_VECTOR_SIZE = 128
CLUSTERING_BATCH_SIZE = 100
MIN_FEATURES_COUNT = 130


class ClusteringStatus(Enum):
    EmbeddingsGenerated = 1
    IssuesClustered = 2
    ClustersAnnotated = 3


def serialize_object(obj, file):
    with open(file, 'wb') as f:
        pickle.dump(obj, f)


def deserialize_object(file):
    with open(file, 'rb') as f:
        return pickle.load(f)


def load_jira_issues(engine, connection):
    print('retrieving issues...')
    jira_issues_metadata = db.MetaData()
    jira_issues = db.Table('jiraissue',
                           jira_issues_metadata,
                           autoload=True,
                           autoload_with=engine)
    return query_table_as_df(connection, jira_issues, True)


def load_projects(engine, connection):
    projects_metadata = db.MetaData()
    projects = db.Table('project',
                        projects_metadata,
                        autoload=True,
                        autoload_with=engine)
    print('retrieving projects...')
    projects_df = query_table_as_df(connection, projects)
    projects_dict = prepare_projects(projects_df)
    return projects_dict


def load_lookups(engine, connection):
    lookups_metadata = db.MetaData()
    lookups = db.Table('lookups',
                       lookups_metadata,
                       autoload=True,
                       autoload_with=engine)
    print('retrieving lookups...')
    lookups_df = query_table_as_df(connection, lookups)
    countries_dict, products_dict, customers_dict = prepare_lookups(lookups_df)
    return countries_dict, customers_dict, products_dict


def get_product(project_key, products_dict, countries_dict):
    if project_key[:2] in countries_dict:
        product_key = project_key[2:4]
        if product_key in products_dict:
            product = products_dict[product_key]
        elif project_key[2:] in products_dict:
            product_key = project_key[2:]
            product = products_dict[product_key]
        else:
            product_key = ''
            product = ''
    elif project_key in products_dict:
        product_key = project_key
        product = products_dict[product_key]
    else:
        product_key = ''
        product = ''

    return product_key, product


def generate_embeddings_with_tfidf():
    engine = db.create_engine(SUPPORT_DB_URL)
    connection = engine.connect()

    jira_issues_df = load_jira_issues(engine, connection)
    projects_dict = load_projects(engine, connection)
    countries_dict, _, products_dict = load_lookups(engine, connection)

    ascii_characters = set(string.printable)
    issues_dict = dict()

    print('preparing issues...')
    progress_bar = tqdm(total=len(jira_issues_df))
    for i in range(len(jira_issues_df)):
        issue = jira_issues_df.iloc[i]
        progress_bar.update()
        if issue['summary'] is None and issue['description'] is None:
            continue

        project_id = int(issue['project'])
        project = projects_dict[project_id]
        project_key = str(project['key']).strip().lower()
        product_key, _ = get_product(project_key, products_dict, countries_dict)

        if product_key.strip() == '':
            continue

        document = combine_into_one_document([issue['summary'], issue['description']])
        document = ''.join(filter(lambda x: x in ascii_characters, document))

        text = ' '.join(normalize_text(document))
        if product_key not in issues_dict:
            issues_dict[product_key] = {
                'issue_ids': [int(issue['id'])],
                'documents': [text]
            }
        else:
            issues_dict[product_key]['issue_ids'].append(int(issue['id']))
            issues_dict[product_key]['documents'].append(text)
    progress_bar.close()

    print('generating feature vectors...')
    product_keys_to_remove = []
    for product_key in issues_dict:
        issues_count = len(issues_dict[product_key]['issue_ids'])
        if issues_count < CLUSTERING_MIN_ISSUE_COUNT:
            product_keys_to_remove.append(product_key)
            continue
        max_features = int(issues_count * ISSUES_FEATURES_FACTOR)
        if max_features < MIN_FEATURES_COUNT:
            max_features = MIN_FEATURES_COUNT
        print(f'features: {max_features}')
        vectorizer = TfidfVectorizer(max_df=MAX_DOCUMENT_FREQUENCY,
                                     max_features=max_features,
                                     min_df=MIN_DOCUMENT_FREQUENCY,
                                     token_pattern=r'(?u)\b[A-Za-z]+\b',
                                     stop_words='english',
                                     use_idf=True)
        documents = issues_dict[product_key]['documents']
        if len(documents) < MIN_DOCUMENT_FREQUENCY:
            product_keys_to_remove.append(product_key)
            continue
        try:
            issue_vectors = vectorizer.fit_transform(documents)
        except:
            product_keys_to_remove.append(product_key)
            continue
        svd = TruncatedSVD(FINAL_FEATURE_VECTOR_SIZE)
        normalizer = Normalizer(copy=False)
        lsa = make_pipeline(svd, normalizer)
        issue_vectors = lsa.fit_transform(issue_vectors)
        issues_dict[product_key]['issue_vectors'] = issue_vectors
        issues_dict[product_key]['issues_vectorizer'] = vectorizer
        issues_dict[product_key]['issues_svd'] = svd
        issues_dict[product_key]['issues_lsa'] = lsa

    for key in product_keys_to_remove:
        issues_dict.pop(key)

    serialize_object(issues_dict, 'issues_dict.pickle')


def cluster_issues():
    issues_dict = deserialize_object('issues_dict.pickle')
    for k in issues_dict:
        print(f'clustering issues with product key: {k}...')
        issue_vectors = np.array(issues_dict[k]['issue_vectors'])
        clustering_model = MiniBatchKMeans(n_clusters=int(len(issue_vectors) * ISSUES_CLUSTERS_FACTOR),
                                           batch_size=CLUSTERING_BATCH_SIZE,
                                           verbose=True)
        issue_labels = clustering_model.fit(issue_vectors)
        issues_dict[k]['clustering_model'] = clustering_model
        issues_dict[k]['issue_labels'] = issue_labels
    serialize_object(issues_dict, 'issues_dict.pickle')


def get_issues_in_cluster(cluster_number,
                          clustering_model,
                          issue_ids,
                          issue_labels,
                          issue_vectors,
                          jira_issues_df):
    centroids = clustering_model.cluster_centers_
    issue_indices = np.where(issue_labels == cluster_number)[0].tolist()
    centroid = centroids[cluster_number]
    issue_distance_pairs = [(jira_issues_df.loc[jira_issues_df['id'] == issue_ids[i]].iloc[0],
                             distance.euclidean(centroid, issue_vectors[i]))
                            for i in issue_indices]
    issue_distance_pairs.sort(key=lambda x: x[1])
    return issue_distance_pairs


def annotate_clusters():
    engine = db.create_engine(SUPPORT_DB_URL)
    connection = engine.connect()
    jira_issues_df = load_jira_issues(engine, connection)

    issues_dict = deserialize_object('issues_dict.pickle')

    for k in issues_dict:
        print(f'annotating cluster of issues with product key: {k}...')
        issue_ids = issues_dict[k]['issue_ids']
        issue_labels = issues_dict[k]['issue_labels'].labels_
        issue_vectors = issues_dict[k]['issue_vectors']

        cluster_dict = dict()
        progress_bar = tqdm(total=len(issue_labels))
        for i in range(len(issue_labels)):
            label = issue_labels[i]
            issue_vector = issue_vectors[i]
            issue = jira_issues_df.loc[jira_issues_df['id'] == issue_ids[i]].iloc[0]
            if label not in cluster_dict:
                cluster_dict[label] = {'issues': [(issue, issue_vector)]}
            else:
                cluster_dict[label]['issues'].append((issue, issue_vector))
            progress_bar.update()
        progress_bar.close()

        vectorizer = issues_dict[k]['issues_vectorizer']

        svd = issues_dict[k]['issues_svd']
        clustering_model = issues_dict[k]['clustering_model']

        centroids = clustering_model.cluster_centers_
        original_space_centroids = svd.inverse_transform(centroids)
        order_centroids = original_space_centroids.argsort()[:, ::-1]
        terms = vectorizer.get_feature_names()

        for i in range(len(order_centroids)):
            if i not in cluster_dict:
                continue
            cluster_keywords = []
            for ind in order_centroids[i, :MAX_NUMBER_OF_KEYWORDS]:
                cluster_keywords.append(terms[ind])
            cluster_dict[i]['keywords'] = cluster_keywords

        for label in cluster_dict:
            centroid = centroids[label]
            issues = cluster_dict[label]['issues']
            issues.sort(key=lambda x: distance.euclidean(centroid, x[1]))

        issues_dict[k]['cluster_dict'] = cluster_dict

    serialize_object(issues_dict, 'issues_dict.pickle')


def print_and_write_to_file(file_name, text):
    with open(file_name, 'a') as f:
        f.write(text)
        f.write('\n')
    print(text)


def create_new_file_name():
    now = datetime.now()
    return f'info-{now.year}-{now.month}-{now.day}--{now.hour}-{now.minute}-{now.second}.txt'


def get_current_status():
    if not os.path.isfile('clustering_status.dat'):
        return None
    else:
        return deserialize_object('clustering_status.dat')


def update_status(new_status_value):
    serialize_object(new_status_value, 'clustering_status.dat')


def view_clusters():
    print('loading information...')
    issues_dict = deserialize_object('issues_dict.pickle')

    print('The issues that have been clustered have one of the following product keys:')
    for k in issues_dict:
        print(f'{k} -> issues: {len(issues_dict[k]["issue_ids"])} -> clusters: {len(issues_dict[k]["cluster_dict"])}')

    while True:
        product_key = None
        while product_key not in issues_dict:
            if product_key is not None:
                print('The product key does not exist or the issues with that product key were not clustered.')
            product_key = input('Please enter a product key among the list above to continue: ')

        file_name = create_new_file_name()
        cluster_dict = issues_dict[product_key]['cluster_dict']
        keys = [*cluster_dict]
        keys.sort(key=lambda x: len(cluster_dict[x]['issues']))
        keys = reversed(keys)
        for key in keys:
            print_and_write_to_file(file_name,
                                    f'############################## BEGINNING OF CLUSTER {key} ##############################')
            print_and_write_to_file(file_name,
                                    f'cluster keywords: {cluster_dict[key]["keywords"]}')
            i = 0
            for issue, _ in cluster_dict[key]['issues'][:CLUSTERING_TOP_NUMBER_OF_ISSUES_TO_DISPLAY]:
                i += 1
                print_and_write_to_file(file_name,
                                        f'------------------------------------ ISSUE {i} --------------------------------------')
                print_and_write_to_file(file_name,
                                        f'==> description: {issue["description"]}\n==> summary: {issue["summary"]}')
                print_and_write_to_file(file_name,
                                        f'==> issue id: {issue["id"]}')
            print_and_write_to_file(file_name,
                                    '------------------------------------------------------------------------------------')
            print_and_write_to_file(file_name,
                                    f'################################# END OF CLUSTER #################################')


def view_cluster():
    print('loading information...')
    issues_dict = deserialize_object('issues_dict.pickle')

    print('The issues that have been clustered have one of the following product keys:')
    for k in issues_dict:
        print(f'{k} -> issues: {len(issues_dict[k]["issue_ids"])} -> clusters: {len(issues_dict[k]["cluster_dict"])}')

    while True:
        product_key = None
        while product_key not in issues_dict:
            if product_key is not None:
                print('The product key does not exist or the issues with that product key were not clustered.')
            product_key = input('Please enter a product key among the list above to continue: ')

        cluster_dict = issues_dict[product_key]['cluster_dict']

        cluster_key = None
        while cluster_key not in cluster_dict:
            if cluster_key is not None:
                print('The cluster key does not exist among the clusters of the product.')
            cluster_key = int(input('Please enter a cluster key: '))

        file_name = create_new_file_name()
        print_and_write_to_file(file_name,
                                f'cluster keywords: {cluster_dict[cluster_key]["keywords"]}')
        i = 0
        for issue, _ in cluster_dict[cluster_key]['issues']:
            i += 1
            print_and_write_to_file(file_name,
                                    f'------------------------------------ ISSUE {i} --------------------------------------')
            print_and_write_to_file(file_name,
                                    f'==> description: {issue["description"]}\n==> summary: {issue["summary"]}')
            print_and_write_to_file(file_name,
                                    f'==> issue id: {issue["id"]}')
        print_and_write_to_file(file_name,
                                '------------------------------------------------------------------------------------')


if __name__ == '__main__':
    if get_current_status() is None:
        generate_embeddings_with_tfidf()
        update_status(ClusteringStatus.EmbeddingsGenerated)
    if get_current_status() == ClusteringStatus.EmbeddingsGenerated:
        cluster_issues()
        update_status(ClusteringStatus.IssuesClustered)
    if get_current_status() == ClusteringStatus.IssuesClustered:
        annotate_clusters()
        update_status(ClusteringStatus.ClustersAnnotated)
    if get_current_status() == ClusteringStatus.ClustersAnnotated:
        view_clusters()
        view_cluster()
