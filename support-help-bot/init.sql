CREATE DATABASE supportdb20210805 WITH ENCODING 'UNICODE' LC_COLLATE 'C' LC_CTYPE 'C' TEMPLATE template0;
create user support with encrypted password 'supportP@ssw0rd' ;
grant all privileges on database supportdb20210805 to support;

