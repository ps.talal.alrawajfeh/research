import json
import os
import string
import warnings
from datetime import datetime

import pandas as pd
import sqlalchemy as db
from elasticsearch import Elasticsearch, helpers
from elasticsearch_dsl import Search, Q
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from tqdm import tqdm

with open('config.json', 'r') as config_file:
    config = json.load(config_file)
SUPPORT_DB_URL = config['support_db_url']
ELASTIC_SEARCH_HOST = config['elastic_search_host']
MAX_NUMBER_OF_ISSUES_TO_DISPLAY = config['search_max_number_of_issues_to_display']

BULK_SIZE = 100
warnings.simplefilter("ignore")


def pre_process(text):
    if text is None:
        return ''
    return text


def query_table_as_df(connection, table, sort_by_created_date=False):
    if sort_by_created_date:
        query = db.select(table).order_by(table.c.created.asc())
    else:
        query = db.select(table)
    result_proxy = connection.execute(query)
    result_set = result_proxy.fetchall()
    df = pd.DataFrame(result_set)
    df.columns = result_set[0].keys()
    return df


def combine_into_one_document(strings):
    return '\n'.join([pre_process(s) for s in strings])


def prepare_issues_action_bodies(jira_actions_df):
    issues_action_bodies = dict()
    for i in range(len(jira_actions_df)):
        action = jira_actions_df.iloc[i]
        if action['actiontype'] == 'comment' and action['actionbody'] is not None:
            issue_id = action['issueid']
            if issue_id in issues_action_bodies:
                issues_action_bodies[issue_id].append(str(action['actionbody']))
            else:
                issues_action_bodies[issue_id] = [str(action['actionbody'])]
    return issues_action_bodies


def prepare_issues_custom_field_values(custom_field_values_df):
    issues_custom_field_values = dict()
    for i in range(len(custom_field_values_df)):
        custom_field_value = custom_field_values_df.iloc[i]
        if custom_field_value['textvalue'] is not None:
            issue_id = int(custom_field_value['issue'])
            if issue_id in issues_custom_field_values:
                issues_custom_field_values[issue_id].append(str(custom_field_value['textvalue']))
            else:
                issues_custom_field_values[issue_id] = [str(custom_field_value['textvalue'])]
    return issues_custom_field_values


def document_from_issue(issue,
                        issues_action_bodies,
                        issues_custom_field_values,
                        projects_dict,
                        countries_dict,
                        products_dict,
                        customers_dict):
    issue_id = int(issue['id'])

    if issue_id in issues_action_bodies:
        associated_action_bodies = issues_action_bodies[issue_id]
    else:
        associated_action_bodies = []

    if issue_id in issues_custom_field_values:
        associated_custom_fields_text_values = issues_custom_field_values[issue_id]
    else:
        associated_custom_fields_text_values = []

    project_id = int(issue['project'])
    project = projects_dict[project_id]
    project_key = str(project['key']).strip().lower()

    if project_key[:2] in countries_dict:
        country_key = project_key[:2]
        country = countries_dict[country_key]
        product_key = project_key[2:4]
        if product_key in products_dict:
            product = products_dict[product_key]
            customer_key = project_key[4:]
            if customer_key in customers_dict:
                customer = customers_dict[customer_key]
            else:
                customer_key = ''
                customer = ''
        elif project_key[2:] in products_dict:
            product_key = project_key[2:]
            product = products_dict[product_key]
            customer_key = ''
            customer = ''
        else:
            product_key = ''
            product = ''
            customer_key = project_key[2:]
            if customer_key in customers_dict:
                customer = customers_dict[customer_key]
            else:
                customer_key = ''
                customer = ''
    elif project_key in customers_dict:
        customer_key = project_key
        customer = customers_dict[customer_key]
        country_key = ''
        country = ''
        product_key = ''
        product = ''
    elif project_key in products_dict:
        product_key = project_key
        product = products_dict[product_key]
        country_key = ''
        country = ''
        customer_key = ''
        customer = ''
    else:
        country_key = ''
        country = ''
        product_key = ''
        product = ''
        customer_key = ''
        customer = ''

    return {
        'text': combine_into_one_document([issue['summary'], issue['description']]),
        'actions': associated_action_bodies,
        'custom_fields': associated_custom_fields_text_values,
        'project_name': str(project['name']).strip().lower(),
        'project_key': project_key,
        'country_key': country_key,
        'country': country,
        'product_key': product_key,
        'product': product,
        'customer_key': customer_key,
        'customer': customer,
        'issue_id': issue_id
    }


def prepare_projects(projects_df):
    projects_dict = dict()
    for i in range(len(projects_df)):
        project = projects_df.iloc[i]
        projects_dict[int(project['id'])] = {
            'id': int(project['id']),
            'name': str(project['pname']).strip().lower(),
            'key': str(project['pkey']).strip().lower()
        }
    return projects_dict


def prepare_lookups(lookups_df):
    countries_dict = dict()
    products_dict = dict()
    customers_dict = dict()
    for i in range(len(lookups_df)):
        lookup = lookups_df.iloc[i]
        lookup_type = str(lookup['type']).strip().lower()
        lookup_dict = {'key': str(lookup['key']).strip().lower(),
                       'value': str(lookup['value']).strip().lower()}
        if lookup_type == 'countries':
            countries_dict[lookup_dict['key']] = lookup_dict['value']
        if lookup_type == 'products':
            products_dict[lookup_dict['key']] = lookup_dict['value']
        if lookup_type == 'customers':
            customers_dict[lookup_dict['key']] = lookup_dict['value']
    return countries_dict, products_dict, customers_dict


def index_issues(jira_issues_df,
                 issues_action_bodies,
                 issues_custom_field_values,
                 projects_dict,
                 countries_dict,
                 products_dict,
                 customers_dict):
    es = Elasticsearch([ELASTIC_SEARCH_HOST])
    n = len(jira_issues_df) // BULK_SIZE
    r = len(jira_issues_df) % BULK_SIZE
    if r != 0:
        progress_bar = tqdm(total=n + 1)
    else:
        progress_bar = tqdm(total=n)
    for i in range(n):
        documents = []
        for j in range(BULK_SIZE):
            row = jira_issues_df.iloc[i * BULK_SIZE + j]
            if row['summary'] is None and row['description'] is None:
                continue
            documents.append(document_from_issue(row,
                                                 issues_action_bodies,
                                                 issues_custom_field_values,
                                                 projects_dict,
                                                 countries_dict,
                                                 products_dict,
                                                 customers_dict))
        helpers.bulk(es, documents, index="jira-issues")
        progress_bar.update()
    documents = []
    for j in range(r):
        row = jira_issues_df.iloc[n * BULK_SIZE + j]
        if row['summary'] is None and row['description'] is None:
            continue
        documents.append(document_from_issue(row,
                                             issues_action_bodies,
                                             issues_custom_field_values,
                                             projects_dict,
                                             countries_dict,
                                             products_dict,
                                             customers_dict))
    helpers.bulk(es, documents, index="jira-issues")
    progress_bar.update()
    progress_bar.close()


def retrieve_data():
    engine = db.create_engine(SUPPORT_DB_URL)
    connection = engine.connect()

    jira_issues_metadata = db.MetaData()
    jira_issues = db.Table('jiraissue',
                           jira_issues_metadata,
                           autoload=True,
                           autoload_with=engine)

    jira_actions_metadata = db.MetaData()
    jira_actions = db.Table('jiraaction',
                            jira_actions_metadata,
                            autoload=True,
                            autoload_with=engine)

    custom_field_values_metadata = db.MetaData()
    custom_field_values = db.Table('customfieldvalue',
                                   custom_field_values_metadata,
                                   autoload=True,
                                   autoload_with=engine)

    projects_metadata = db.MetaData()
    projects = db.Table('project',
                        projects_metadata,
                        autoload=True,
                        autoload_with=engine)

    lookups_metadata = db.MetaData()
    lookups = db.Table('lookups',
                       lookups_metadata,
                       autoload=True,
                       autoload_with=engine)

    print('retrieving issues...')
    jira_issues_df = query_table_as_df(connection, jira_issues, True)

    print('retrieving associated actions...')
    jira_actions_df = query_table_as_df(connection, jira_actions, True)
    issues_action_bodies = prepare_issues_action_bodies(jira_actions_df)

    print('retrieving associated custom field values...')
    custom_field_values_df = query_table_as_df(connection, custom_field_values)
    issues_custom_field_values = prepare_issues_custom_field_values(custom_field_values_df)

    print('retrieving projects...')
    projects_df = query_table_as_df(connection, projects)
    projects_dict = prepare_projects(projects_df)

    print('retrieving lookups...')
    lookups_df = query_table_as_df(connection, lookups)
    countries_dict, products_dict, customers_dict = prepare_lookups(lookups_df)

    print('indexing issues...')
    index_issues(jira_issues_df,
                 issues_action_bodies,
                 issues_custom_field_values,
                 projects_dict,
                 countries_dict,
                 products_dict,
                 customers_dict)


def is_excluded_word(word):
    return word in stopwords.words('english') or word in string.punctuation


def normalize_text(text):
    tokens = word_tokenize(text.lower())
    words = []

    for word in tokens:
        if is_excluded_word(word):
            continue
        words.append(word)

    return words


def validated_input(input_prompt, error_message, valid_choices):
    result = None
    while result not in valid_choices:
        if result is not None:
            print(error_message)
        result = input(input_prompt).strip().lower()
    return result


def print_and_write_to_file(file_name, text):
    with open(file_name, 'a') as f:
        f.write(text)
        f.write('\n')
    print(text)


def create_new_file_name():
    now = datetime.now()
    return f'info-{now.year}-{now.month}-{now.day}--{now.hour}-{now.minute}-{now.second}.txt'


def search_issues():
    es = Elasticsearch([ELASTIC_SEARCH_HOST])
    search = Search(using=es)

    while True:
        text = input('Enter your question: ')
        normalized_text = ' '.join(normalize_text(text))

        specify_search = validated_input('Do you want to specify your search according a certain criteria? [y/N] ',
                                         'Invalid choice. Please try again.',
                                         ['y', 'n', ''])

        if specify_search == 'y':
            country_code = input('Enter the country code: [default is empty] ').strip().lower()
            product_code = input('Enter the product code: [default is empty] ').strip().lower()
            customer_code = input('Enter the customer code: [default is empty] ').strip().lower()

            queries = []

            if country_code != '':
                queries.append(Q('term', country_key=country_code))
            if product_code != '':
                queries.append(Q('term', product_key=product_code))
            if customer_code != '':
                queries.append(Q('term', customer_key=customer_code))

            search = search.query('bool', filter=queries)

        query1 = search.query('match', text=normalized_text)
        results1 = query1.execute()

        query2 = search.query('fuzzy', text=normalized_text)
        results2 = query2.execute()

        if len(results1) == 0 and len(results2) == 0:
            print('could not find any relevant issue matching your question.')
            return

        if len(results1) != 0:
            results = results1
            score = results[0].meta.score
        else:
            results = []
            score = 0.0

        if len(results2) != 0 and results2[0].meta.score > score:
            results = results2

        file_name = create_new_file_name()
        print_and_write_to_file(file_name, '==> relevant issues found:')
        i = 0
        for result in results[:min(MAX_NUMBER_OF_ISSUES_TO_DISPLAY, len(results))]:
            result_dict = result.to_dict()
            print_and_write_to_file(file_name,
                                    f'\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Issue #{i + 1} ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
            print_and_write_to_file(file_name, f'Issue Id: {result_dict["issue_id"]}')
            print_and_write_to_file(file_name, 'Summary & Description:')
            print_and_write_to_file(file_name, result_dict["text"])
            if len(result_dict['actions']) != 0:
                print_and_write_to_file(file_name, '\n')
                print_and_write_to_file(file_name, 'Steps/Actions Taken:')
                for j in range(len(result_dict['actions'])):
                    action_body = result_dict['actions'][j]
                    print_and_write_to_file(file_name, f'[{j + 1}] {action_body}')
            if len(result_dict['custom_fields']) != 0:
                print_and_write_to_file(file_name, '\n')
                print_and_write_to_file(file_name, 'Other Notes/Comments:')
                for j in range(len(result_dict['custom_fields'])):
                    custom_field = result_dict['custom_fields'][j]
                    print_and_write_to_file(file_name, f'[{j + 1}] {custom_field}')
            print_and_write_to_file(file_name,
                                    '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
            i += 1


if __name__ == '__main__':
    if not os.path.isfile('data_retrieval_status.dat'):
        retrieve_data()
        with open('data_retrieval_status.dat', 'w') as f:
            f.write('\n')
    search_issues()
