# Support Database Issues Full-Text Search & Clustering

## Setup

First, make sure you have python installed of version at least `3.6`. Next install the requirements using the command:

```bash
python3 -m pip install -r --upgrade requirements.txt
```

Now run the script `download_nltk_data.py` in this folder using the command:

```bash
python3 download_nltk_data.py
```

To use the Full-Text search script make sure you have an elastic search node running or just use the following commands:

```bash
docker pull elasticsearch:7.14.0
docker run -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" elasticsearch:7.14.0
```

To configure the support database url and the elastic search url, edit the `config.json` file inside this folder:

```
"support_db_url": "postgresql://sys:sys@localhost:5432/supportdb20210805",
"elastic_search_host": "http://localhost:9200"
```

To do a full-text search, run the script `issues_search.py` using the command:

```bash
python3 issues_search.py
```

To look at the clusters, run the script `issues_clustering.py` using the command:

```bash
python3 issues_clustering.py
```