import math
import os
import pickle
import random
import shutil
from enum import Enum

import numpy as np
import tensorflow as tf
from alt_model_checkpoint.keras import AltModelCheckpoint
from matplotlib import pyplot as plt
from scipy.interpolate import interp1d
from sklearn.metrics import accuracy_score, confusion_matrix, classification_report
from tensorflow.keras import Model
from tensorflow.keras.callbacks import LambdaCallback, ModelCheckpoint
from tensorflow.keras.layers import Input, BatchNormalization, Dense, ReLU, Concatenate, Lambda, \
    Activation, Dropout, GaussianNoise
from tensorflow.keras.losses import binary_crossentropy
from tensorflow.keras.models import load_model
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.utils import Sequence
from tqdm import tqdm

tf.random.set_seed(123456)
random.seed(123456)
np.random.seed(123456)

USE_THUNDER_SVM = True

if USE_THUNDER_SVM:
    from thundersvm import SVC
else:
    from sklearn.svm import SVC

MODEL_FILE_NAME = 'siamese'
SIAMESE_LOGITS_MODEL_FILE_NAME = 'siamese_logits'
EVALUATION_REPORT_FILE = 'report.txt'
FEATURE_VECTOR_CLASSES_FILE = 'feature_vector_classes.pickle'
SPECIAL_FORGERY_FEATURE_VECTOR_CLASSES_FILE = 'special_forgery_feature_vector_classes.pickle'
TRAIN_FEATURE_VECTOR_CLASSES_FILE = 'train_feature_vector_classes.pickle'
VALIDATION_FEATURE_VECTOR_CLASSES_FILE = 'validation_feature_vector_classes.pickle'
TEST_FEATURE_VECTOR_CLASSES_FILE = 'test_feature_vector_classes.pickle'
TRAIN_PAIRS_FILE = 'train_pairs.pickle'
VALIDATION_PAIRS_FILE = 'validation_pairs.pickle'
TEST_PAIRS_FILE = 'test_pairs.pickle'
SVC_MODEL_FILE_NAME = 'svc_model.pickle'

GENUINE_LABEL = [1, 0]
FORGERY_LABEL = [0, 1]

# average number of genuines per class = VARIATIONS_PER_IMAGE * 3 + 2
# lower bound of genunines per class = VARIATIONS_PER_IMAGE * 2 + 2
REFERENCES_PER_CLASS = 10
FORGERY_CLASSES_PER_REFERENCE = 14
FORGERIES_PER_FORGERY_CLASS = 4

TRAIN_CLASSES_PERCENTAGE = 0.7
VALIDATION_CLASSES_PERCENTAGE = 0.15
TEST_CLASSES_PERCENTAGE = 0.15

EMBEDDING_VECTOR_SIZE = 1920
LEAKY_RELU_ALPHA = 0.05
LABEL_SMOOTHING_ALPHA = 0.1
LEARNING_RATE = 1e-3
CONFIDENCE_PENALTY = 0.1
EPSILON = 1e-7

TRAINING_BATCH_SIZE = 128
EVALUATION_BATCH_SIZE = 1024
EPOCHS = 100

REPORT_GENUINE_THRESHOLD = 0.5
REPORT_HISTOGRAM_BIN_SIZE = 5

SPECIAL_FORGERIES_ENABLED = False
VARIATIONS_PER_IMAGE = 4

SPECIAL_FORGERY_VARIATIONS = 1
SPECIAL_FORGERY_CLASSES = FORGERY_CLASSES_PER_REFERENCE // 2
SPECIAL_FORGERY_PER_CLASS = 1


class TrainingMode(Enum):
    TrainWithSplittingClasses = 1
    TrainWithoutSplittingClasses = 2


TRAINING_MODE = TrainingMode.TrainWithSplittingClasses


def cache_object(obj, file):
    with open(file, 'wb') as f:
        pickle.dump(obj, f)


def load_cached(file):
    with open(file, 'rb') as f:
        return pickle.load(f)


def file_exists(file):
    return os.path.isfile(file)


class MinimumRecurrenceRateRandomSamplerWithReplacement:
    def __init__(self, array) -> None:
        self.array = array
        self.left_choices = array.copy()

    def next(self):
        if len(self.left_choices) == 0:
            self.left_choices = self.array.copy()
        i = random.randint(0, len(self.left_choices) - 1)
        return self.left_choices.pop(i)


def generate_pairs(feature_vector_classes,
                   special_forgery_feature_vector_classes,
                   make_number_of_genuines_and_forgeries_equal=False):
    keys = [*feature_vector_classes]
    number_of_classes = len(keys)

    references = []
    others = []
    labels = []

    for c in range(number_of_classes - FORGERY_CLASSES_PER_REFERENCE):
        c = random.randint(0, len(keys) - 1)
        key = keys.pop(c)

        reference_indices = random.sample(list(range(len(feature_vector_classes[key]))),
                                          min(REFERENCES_PER_CLASS, len(feature_vector_classes[key])))

        for r in reference_indices:
            reference = [key, r]
            if not make_number_of_genuines_and_forgeries_equal:
                genuines = [[key, i] for i in range(len(feature_vector_classes[key]))]
                references.extend([reference] * len(genuines))
                others.extend(genuines)
                labels.extend([GENUINE_LABEL] * len(genuines))

            forgery_keys = random.sample(keys, min(FORGERY_CLASSES_PER_REFERENCE, len(keys)))
            for forgery_key in forgery_keys:
                forgery_class = feature_vector_classes[forgery_key]
                forgery_indices = random.sample(list(range(len(forgery_class))),
                                                min(FORGERIES_PER_FORGERY_CLASS, len(forgery_class)))
                forgeries = [[forgery_key, f] for f in forgery_indices]

                references.extend([reference] * len(forgeries))
                others.extend(forgeries)
                labels.extend([FORGERY_LABEL] * len(forgeries))

            if make_number_of_genuines_and_forgeries_equal:
                sampler = MinimumRecurrenceRateRandomSamplerWithReplacement(
                    list(range(len(feature_vector_classes[key]))))
                references.extend([reference] * len(forgery_keys))
                others.extend([key, sampler.next()] for _ in range(len(forgery_keys)))
                labels.extend([GENUINE_LABEL] * len(forgery_keys))

            if special_forgery_feature_vector_classes is not None:
                forgery_key = 's' + key
                forgery_class = special_forgery_feature_vector_classes[forgery_key]
                forgeries = [[forgery_key, f] for f in list(range(len(forgery_class)))]

                references.extend([reference] * len(forgeries))
                others.extend(forgeries)
                labels.extend([FORGERY_LABEL] * len(forgeries))

                if make_number_of_genuines_and_forgeries_equal:
                    sampler = MinimumRecurrenceRateRandomSamplerWithReplacement(
                        list(range(len(feature_vector_classes[key]))))
                    references.extend([reference] * len(forgeries))
                    others.extend([key, sampler.next()] for _ in range(len(forgeries)))
                    labels.extend([GENUINE_LABEL] * len(forgeries))

    return references, others, labels


def class_train_validation_test_split(feature_vector_classes):
    keys = [*feature_vector_classes]

    keys_indices = list(range(len(keys)))
    random.shuffle(keys_indices)

    classes = (dict(), dict(), dict())
    percentages = [TRAIN_CLASSES_PERCENTAGE,
                   VALIDATION_CLASSES_PERCENTAGE,
                   TEST_CLASSES_PERCENTAGE]

    for i in range(3):
        chosen_key_indices = keys_indices[:int(len(feature_vector_classes) * percentages[i])]
        for j in range(len(chosen_key_indices)):
            key = keys[chosen_key_indices[j]]
            classes[i][key] = feature_vector_classes[key]
        keys_indices = keys_indices[len(chosen_key_indices):]

    return classes


def shuffle_pairs(pairs):
    references, others, true_outputs = pairs
    indices = list(range(len(references)))
    random.shuffle(indices)

    shuffled_references = [references[indices[i]] for i in range(len(references))]
    shuffled_others = [others[indices[i]] for i in range(len(references))]
    shuffled_true_outputs = [true_outputs[indices[i]] for i in range(len(references))]

    return shuffled_references, shuffled_others, shuffled_true_outputs


def to_float_array(np_array):
    return np.array(np_array, np.float)


class DataGenerator(Sequence):
    def __init__(self,
                 feature_vector_classes,
                 pairs,
                 special_forgery_feature_vector_classes,
                 batch_size,
                 count):
        self.batch_size = batch_size
        self.count = count
        self.feature_vector_classes = feature_vector_classes
        self.references, self.others, self.true_labels = shuffle_pairs(pairs)
        self.special_forgery_feature_vector_classes = special_forgery_feature_vector_classes

    def __len__(self):
        return math.ceil(self.count / self.batch_size)

    def __getitem__(self, index):
        references, others, true_labels = [], [], []
        for i in range(index * self.batch_size,
                       min((index + 1) * self.batch_size, len(self.references))):
            reference_pair = self.references[i]
            other_pair = self.others[i]
            reference_image = self.feature_vector_classes[reference_pair[0]][reference_pair[1]]
            key = other_pair[0]
            if key in self.feature_vector_classes:
                other_image = self.feature_vector_classes[key][other_pair[1]]
            else:
                other_image = self.special_forgery_feature_vector_classes[key][other_pair[1]]
            references.append(to_float_array(reference_image))
            others.append(to_float_array(other_image))
            true_labels.append(to_float_array(self.true_labels[i]))

        if len(references) == 0:
            return self.__getitem__(0)

        return [np.array(references), np.array(others)], np.array(true_labels)

    def on_epoch_end(self):
        self.references, self.others, self.true_labels = shuffle_pairs([self.references,
                                                                        self.others,
                                                                        self.true_labels])


def leaky_relu6(negative_slope=LEAKY_RELU_ALPHA):
    return ReLU(max_value=6, negative_slope=negative_slope)


def absolute_difference(tensors):
    x, y = tensors
    return tf.abs(tf.reduce_sum(tf.stack([x, -y], axis=1), axis=1))


#
# def siamese_net(input_shape=(EMBEDDING_VECTOR_SIZE,)):
#     input1 = Input(input_shape)
#     input2 = Input(input_shape)
#
#     input1_noise = GaussianNoise(stddev=1e-7)(input1)
#     input2_noise = GaussianNoise(stddev=1e-7)(input2)
#
#     embedding1 = Lambda(absolute_difference)([input1_noise, input2_noise])
#     embedding2 = Lambda(lambda x: x[0] * x[1])([input1_noise, input2_noise])
#     embedding = Concatenate()([embedding1, embedding2])
#     embedding = BatchNormalization()(embedding)
#     embedding = Dropout(0.5)(embedding)
#
#     fc1 = Dense(1920,
#                 use_bias=False,
#                 kernel_initializer='he_normal')(embedding)
#     # fc1 = leaky_relu6(0.05)(fc1)
#     fc1 = tf.keras.activations.swish(fc1)
#     concat_skip_connection = Concatenate()([embedding, fc1])
#     concat_skip_connection = BatchNormalization()(concat_skip_connection)
#     concat_skip_connection = Dropout(0.5)(concat_skip_connection)
#
#     fc2 = Dense(1920,
#                 use_bias=False,
#                 kernel_initializer='he_normal')(concat_skip_connection)
#     # fc2 = leaky_relu6(0.05)(fc2)
#     fc2 = tf.keras.activations.swish(fc2)
#     fc2 = BatchNormalization()(fc2)
#     fc2 = Dropout(0.5)(fc2)
#
#     fc3 = Dense(2,
#                 use_bias=False,
#                 kernel_initializer='glorot_normal')(fc2)
#     probabilities = Activation('sigmoid')(fc3)
#
#     siamese_model = Model(inputs=[input1, input2], outputs=[probabilities])
#     siamese_logits_model = Model(inputs=[input1, input2], outputs=[fc3])
#
#     return siamese_model, siamese_logits_model


def siamese_net(input_shape=(EMBEDDING_VECTOR_SIZE,)):
    input1 = Input(input_shape)
    input2 = Input(input_shape)

    embedding_input = Input(input_shape)
    embedding = BatchNormalization()(embedding_input)
    embedding = Dropout(0.5)(embedding)
    embedding = tf.keras.layers.GaussianNoise(stddev=1e-7)(embedding)

    activation_function = leaky_relu6()

    fc1 = Dense(1024,
                use_bias=False,
                kernel_initializer='he_normal')(embedding)
    fc1 = BatchNormalization()(fc1)
    fc1 = activation_function(fc1)
    fc1 = Dropout(0.5)(fc1)

    fc2 = Dense(1024,
                use_bias=False,
                kernel_initializer='he_normal')(fc1)
    fc2 = BatchNormalization()(fc2)
    fc2 = activation_function(fc2)

    embedding_model = Model(inputs=[embedding_input], outputs=[fc2])

    input_embedding1 = embedding_model(input1)
    input_embedding2 = embedding_model(input2)
    dichotomy = Lambda(absolute_difference)([input_embedding1, input_embedding2])
    dichotomy = BatchNormalization()(dichotomy)
    dichotomy = Dropout(0.5)(dichotomy)

    dichotomy_fc1 = Dense(1024,
                          use_bias=False,
                          kernel_initializer='glorot_normal')(dichotomy)
    dichotomy_fc1 = BatchNormalization()(dichotomy_fc1)
    dichotomy_fc1 = activation_function(dichotomy_fc1)
    dichotomy_fc1 = Dropout(0.5)(dichotomy_fc1)

    dichotomy_rbf = tf.norm(dichotomy_fc1, ord='euclidean', axis=-1, keepdims=True)
    dichotomy_rbf = Dense(1,
                          kernel_initializer=tf.constant_initializer(value=0.5),
                          kernel_constraint=tf.keras.constraints.NonNeg())(dichotomy_rbf)
    dichotomy_rbf = tf.exp(-dichotomy_rbf)

    dichotomy_rbf = Dense(1,
                          activation='sigmoid',
                          kernel_initializer='he_normal')(dichotomy_rbf)

    output = Concatenate()([dichotomy_rbf, 1.0 - dichotomy_rbf])

    siamese_model = Model(inputs=[input1, input2], outputs=[output])
    siamese_logits_model = Model(inputs=[input1, input2], outputs=[dichotomy_fc1])

    return siamese_model, siamese_logits_model


def binary_accuracy(y_true, y_pred, thresh=REPORT_GENUINE_THRESHOLD):
    return tf.reduce_mean(tf.cast(tf.equal(y_true > thresh, y_pred > thresh), tf.float32))


def entropy(prob_batch):
    log_prob_batch = tf.math.log(prob_batch + EPSILON)
    entropy_batch = tf.reduce_sum(prob_batch * log_prob_batch, axis=-1)
    return -1.0 * tf.reduce_mean(entropy_batch)


def confidence_regularized_binary_crossentropy(y_true,
                                               y_pred,
                                               label_smoothing_alpha=LABEL_SMOOTHING_ALPHA,
                                               confidence_penalty=CONFIDENCE_PENALTY):
    return binary_crossentropy(y_true,
                               y_pred,
                               label_smoothing=label_smoothing_alpha) - \
           confidence_penalty * entropy(y_pred)


def append_lines_to_report(lines):
    with open(EVALUATION_REPORT_FILE, 'a') as f:
        f.writelines(lines)


def epoch_metrics_log(epoch, logs):
    line = f'epoch: {epoch}'
    line += ' - '
    line += f'loss: {logs["loss"]}'
    line += ' - '
    line += f'binary accuracy: {logs["binary_accuracy"]}'
    line += ' - '
    line += f'validation loss: {logs["val_loss"]}'
    line += ' - '
    line += f'validation binary accuracy: {logs["val_binary_accuracy"]}'
    return line


class ModelLoadingMode(Enum):
    MinimumValue = 0
    MaximumValue = 1


def rename_best_model(mode=ModelLoadingMode.MaximumValue):
    all_models = list(filter(lambda x: x[-3:] == '.h5' and x[:len(MODEL_FILE_NAME)] == MODEL_FILE_NAME,
                             os.listdir(os.curdir)))
    values = []

    for model in all_models:
        value_start_index = model.rfind('_')
        value_end_index = model.rfind('.')

        if value_start_index == -1 or value_end_index == -1:
            values.append(None)
            continue

        value = model[value_start_index + 1: value_end_index]

        try:
            values.append(float(value))
        except ValueError:
            values.append(None)

    model_value_pairs = [[all_models[i], values[i]]
                         for i in range(len(all_models)) if values[i] is not None]

    model_value_pairs.sort(key=lambda x: x[1])
    if mode == ModelLoadingMode.MaximumValue:
        best_index = -1
    else:
        best_index = 0

    shutil.copy(model_value_pairs[best_index][0], MODEL_FILE_NAME + '.h5')


def hard_binary_cross_entropy(y_true, y_pred, acceptance_threshold=0.6, rejection_threshold=0.4):
    accepted_mask = tf.logical_and(y_pred[:, 0] >= acceptance_threshold, y_pred[:, 1] <= rejection_threshold)
    rejected_mask = tf.logical_and(y_pred[:, 0] <= rejection_threshold, y_pred[:, 1] >= acceptance_threshold)

    true_labels = tf.argmax(y_true, axis=-1)
    false_acceptance_mask = tf.logical_and(true_labels == 1, accepted_mask)
    false_rejection_mask = tf.logical_and(true_labels == 0, rejected_mask)

    false_acceptance_loss = binary_crossentropy(y_true[false_acceptance_mask],
                                                y_pred[false_acceptance_mask],
                                                label_smoothing=0.1)
    false_rejection_loss = binary_crossentropy(y_true[false_rejection_mask],
                                               y_pred[false_rejection_mask],
                                               label_smoothing=0.1)

    overall_loss = binary_crossentropy(y_true,
                                       y_pred,
                                       label_smoothing=0.1)
    return tf.reduce_sum(false_acceptance_loss) * 0.5 + tf.reduce_sum(
        false_rejection_loss) * 0.5 + tf.reduce_sum(overall_loss) - CONFIDENCE_PENALTY * entropy(
        y_pred)


def train(train_feature_vector_classes,
          train_pairs,
          validation_feature_vector_classes,
          validation_pairs,
          special_forgery_feature_vector_classes):
    siamese_last_checkpoint_file = SIAMESE_LOGITS_MODEL_FILE_NAME + '_last.h5'
    siamese_model, siamese_logits_model = siamese_net()

    siamese_model.summary()

    siamese_model.compile(loss='binary_crossentropy',
                          optimizer=Adam(LEARNING_RATE),
                          metrics=[binary_accuracy])

    train_data_generator = DataGenerator(train_feature_vector_classes,
                                         train_pairs,
                                         special_forgery_feature_vector_classes,
                                         TRAINING_BATCH_SIZE,
                                         len(train_pairs[0]))

    validation_data_generator = DataGenerator(validation_feature_vector_classes,
                                              validation_pairs,
                                              special_forgery_feature_vector_classes,
                                              EVALUATION_BATCH_SIZE,
                                              len(validation_pairs[0]))

    append_lines_to_report('[training log]\n\n')

    update_report_callback = LambdaCallback(
        on_epoch_end=lambda epoch, logs: append_lines_to_report(
            epoch_metrics_log(epoch, logs) + '\n'
        ))

    model_best_checkpoint_callback = ModelCheckpoint(
        filepath=MODEL_FILE_NAME + '_{epoch:02d}_{val_binary_accuracy:0.4f}.h5',
        monitor='val_binary_accuracy',
        mode='max',
        save_best_only=True)

    model_last_checkpoint_callback = ModelCheckpoint(
        filepath=MODEL_FILE_NAME + '_last.h5',
        save_best_only=False)

    siamese_logits_best_checkpoint_callback = AltModelCheckpoint(
        filepath=SIAMESE_LOGITS_MODEL_FILE_NAME + '_{epoch:02d}_{val_binary_accuracy:0.4f}.h5',
        alternate_model=siamese_logits_model,
        monitor='val_binary_accuracy',
        mode='max',
        save_best_only=True)

    siamese_logits_last_checkpoint_callback = AltModelCheckpoint(
        alternate_model=siamese_logits_model,
        filepath=siamese_last_checkpoint_file,
        save_best_only=False)

    siamese_model.fit_generator(generator=train_data_generator,
                                steps_per_epoch=math.ceil(len(train_pairs[0]) / TRAINING_BATCH_SIZE),
                                validation_data=validation_data_generator,
                                validation_steps=math.ceil(len(validation_pairs[0]) / EVALUATION_BATCH_SIZE),
                                epochs=EPOCHS,
                                callbacks=[update_report_callback,
                                           model_best_checkpoint_callback,
                                           model_last_checkpoint_callback,
                                           siamese_logits_best_checkpoint_callback,
                                           siamese_logits_last_checkpoint_callback])

    rename_best_model()


def write_classification_report(all_true_labels, all_predicted_labels):
    accuracy = accuracy_score(all_true_labels, all_predicted_labels)
    append_lines_to_report(f'\ntest accuracy: {round(accuracy * 100, 2)}%\n')

    append_lines_to_report('\nconfusion matrix:\n')
    matrix = confusion_matrix(all_true_labels, all_predicted_labels)
    append_lines_to_report(f'{matrix}\n')

    frr = round((matrix[0, 1] / np.sum(matrix[0])) * 100, 2)
    far = round((matrix[1, 0] / np.sum(matrix[1])) * 100, 2)
    append_lines_to_report(f'\nFRR: {frr}%\n')
    append_lines_to_report(f'FAR: {far}%\n')

    append_lines_to_report('\nclassification report:\n')
    append_lines_to_report(f'{classification_report(all_true_labels, all_predicted_labels)}\n')


def draw_distributions(all_true_labels,
                       all_predicted_outputs,
                       genuine_threshold=REPORT_GENUINE_THRESHOLD,
                       bin_size=REPORT_HISTOGRAM_BIN_SIZE):
    bins = [bin_size * i / 100 for i in range(1, 100 // bin_size + 1)]

    true_labels = 1 - np.array(all_true_labels)
    predicted_outputs = np.array(all_predicted_outputs)

    genuine_outputs = predicted_outputs[true_labels > genuine_threshold]
    forgery_outputs = predicted_outputs[true_labels <= genuine_threshold]

    plt.hist(genuine_outputs, bins, color='blue', label='genuine')
    plt.legend(loc='best')
    plt.savefig('genuine_distribution.png', dpi=200)
    plt.close('all')

    plt.hist(forgery_outputs, bins, color='red', label='forgery')
    plt.legend(loc='best')
    plt.savefig('forgery_distribution.png', dpi=200)
    plt.close('all')

    plt.hist(genuine_outputs, bins, color='blue', label='genuine')
    plt.hist(forgery_outputs, bins, color='red', label='forgery')
    plt.legend(loc='best')
    plt.savefig('distributions.png', dpi=200)
    plt.close('all')


def evaluate(model,
             test_feature_vector_classes,
             test_pairs,
             special_forgery_feature_vector_classes):
    model.summary()

    model.compile(loss=binary_crossentropy,
                  optimizer=Adam(LEARNING_RATE),
                  metrics=[binary_accuracy])

    test_data_generator = DataGenerator(test_feature_vector_classes,
                                        test_pairs,
                                        special_forgery_feature_vector_classes,
                                        EVALUATION_BATCH_SIZE,
                                        len(test_pairs[0]))

    append_lines_to_report('\n\n[evaluation log]\n')

    results = model.evaluate_generator(generator=test_data_generator,
                                       steps=math.ceil(len(test_pairs[0]) / EVALUATION_BATCH_SIZE))

    append_lines_to_report(f'test loss: {results[0]} - test accuracy: {results[1]}\n')

    all_true_labels, all_predicted_labels, all_predicted_outputs = [], [], []
    batches = len(test_data_generator)
    progress_bar = tqdm(total=batches)
    for i in range(batches):
        input_channels, true_labels = test_data_generator[i]
        predicted_outputs = model.predict(input_channels)
        all_true_labels.extend(list(np.argmax(true_labels, axis=1)))
        all_predicted_labels.extend(list(np.argmax(predicted_outputs, axis=1)))
        all_predicted_outputs.extend(list(predicted_outputs[:, 0]))
        progress_bar.update()
    progress_bar.close()

    write_classification_report(all_true_labels, all_predicted_labels)
    draw_distributions(all_true_labels, all_predicted_outputs)


def load_or_supply_and_cache(cache_file, supplier_lambda):
    if file_exists(cache_file):
        return load_cached(cache_file)
    result = supplier_lambda()
    cache_object(result, cache_file)
    return result


def train_model(train_feature_vector_classes,
                validation_feature_vector_classes,
                special_forgery_feature_vector_classes):
    train_pairs = load_or_supply_and_cache(TRAIN_PAIRS_FILE,
                                           lambda: generate_pairs(train_feature_vector_classes,
                                                                  special_forgery_feature_vector_classes))
    validation_pairs = load_or_supply_and_cache(VALIDATION_PAIRS_FILE,
                                                lambda: generate_pairs(validation_feature_vector_classes,
                                                                       special_forgery_feature_vector_classes))

    train(train_feature_vector_classes,
          train_pairs,
          validation_feature_vector_classes,
          validation_pairs,
          special_forgery_feature_vector_classes)


def evaluate_model(test_feature_vector_classes,
                   special_forgery_feature_vector_classes):
    test_pairs = load_or_supply_and_cache(TEST_PAIRS_FILE,
                                          lambda: generate_pairs(test_feature_vector_classes,
                                                                 special_forgery_feature_vector_classes))

    model = load_model(MODEL_FILE_NAME + '.h5', compile=False)

    evaluate(model,
             test_feature_vector_classes,
             test_pairs,
             special_forgery_feature_vector_classes)


def calculate_mean_images_per_class():
    train_feature_vector_classes = load_cached(TRAIN_FEATURE_VECTOR_CLASSES_FILE)
    validation_feature_vector_classes = load_cached(VALIDATION_FEATURE_VECTOR_CLASSES_FILE)
    test_feature_vector_classes = load_cached(TEST_FEATURE_VECTOR_CLASSES_FILE)

    total = 0
    for c in train_feature_vector_classes:
        total += len(train_feature_vector_classes[c])
    for c in validation_feature_vector_classes:
        total += len(validation_feature_vector_classes[c])
    for c in test_feature_vector_classes:
        total += len(test_feature_vector_classes[c])

    total /= (len(train_feature_vector_classes) + len(validation_feature_vector_classes) + len(
        test_feature_vector_classes))
    print(f"average: {total}")


def train_svm(train_feature_vector_classes,
              special_forgery_feature_vector_classes):
    train_pairs = load_or_supply_and_cache(TRAIN_PAIRS_FILE,
                                           lambda: generate_pairs(train_feature_vector_classes,
                                                                  special_forgery_feature_vector_classes))

    model = load_model(SIAMESE_LOGITS_MODEL_FILE_NAME + '.h5', compile=False)

    data_generator = DataGenerator(train_feature_vector_classes,
                                   train_pairs,
                                   special_forgery_feature_vector_classes,
                                   EVALUATION_BATCH_SIZE,
                                   len(train_pairs[0]))

    logits, labels = [], []
    batches = len(data_generator)
    progress_bar = tqdm(total=batches)
    for i in range(batches):
        input_channels, true_labels = data_generator[i]
        predicted_outputs = model.predict(input_channels)
        labels.extend(list(np.argmax(true_labels, axis=1)))
        logits.extend(list(predicted_outputs))
        progress_bar.update()
    progress_bar.close()

    svc_model = SVC(C=1.0, kernel='linear', verbose=True)
    svc_model.fit(np.array(logits, np.float32), np.array(labels))

    cache_object(svc_model, 'svc_model.pickle')


def calculate_accuracy_far_frr(labels, predicted_labels):
    accuracy = np.mean(np.array((predicted_labels - labels) < EPSILON, np.float32)) * 100
    accuracy = round(float(accuracy), 2)
    print(f'Accuracy: {accuracy}%')

    append_lines_to_report(f'\nAccuracy: {accuracy}%\n')
    forgery_labels_mask = labels == 0
    forgeries_count = np.count_nonzero(forgery_labels_mask)
    expected_forgeries = predicted_labels[forgery_labels_mask]
    false_acceptance_count = np.count_nonzero(expected_forgeries == 1)
    false_acceptance_rate = round(false_acceptance_count / forgeries_count * 100, 2)
    false_acceptance_rate_line = f'false acceptance: {false_acceptance_rate}%\n'
    print(false_acceptance_rate_line)
    append_lines_to_report(false_acceptance_rate_line)

    genuine_labels_mask = labels == 1
    genuines_count = np.count_nonzero(genuine_labels_mask)
    expected_genuines = predicted_labels[genuine_labels_mask]
    false_rejection_count = np.count_nonzero(expected_genuines == 0)
    false_rejection_rate = round(false_rejection_count / genuines_count * 100, 2)
    false_rejection_rate_line = f'false rejection: {false_rejection_rate}%\n'
    print(false_rejection_rate_line)
    append_lines_to_report(false_rejection_rate_line)

    print('')
    append_lines_to_report('\n')

    return forgery_labels_mask, forgeries_count, genuine_labels_mask, genuines_count


def calculate_false_acceptance_rate_per_threshold(forgery_labels_mask, forgeries_count, histogram_bins, probabilities):
    false_acceptances = probabilities[forgery_labels_mask]
    false_acceptances_histogram = np.histogram(false_acceptances, histogram_bins)[0]
    false_acceptance_per_threshold = np.cumsum(false_acceptances_histogram)
    false_acceptance_per_threshold = forgeries_count - false_acceptance_per_threshold
    false_acceptance_rate_per_threshold = false_acceptance_per_threshold / forgeries_count
    return false_acceptance_rate_per_threshold


def calculate_false_rejection_rate_per_threshold(genuine_labels_mask,
                                                 genuines_count,
                                                 histogram_bins,
                                                 probabilities):
    false_rejections = probabilities[genuine_labels_mask]
    false_rejections_histogram = np.histogram(false_rejections, histogram_bins)[0]
    false_rejection_per_threshold = np.cumsum(false_rejections_histogram)
    false_rejection_rate_per_threshold = false_rejection_per_threshold / genuines_count
    return false_rejection_rate_per_threshold


def generate_distributions_table(false_acceptance_rate_per_threshold,
                                 false_rejection_rate_per_threshold,
                                 histogram_bin_size):
    table_header = 'THRESH\t\tFAR\t\tFRR\n'
    print(table_header)
    append_lines_to_report(table_header)
    for i in range(0, 100 // histogram_bin_size):
        if i == 0:
            far = 100.0
            frr = 0.0
        else:
            far = round(false_acceptance_rate_per_threshold[i - 1] * 100, 2)
            frr = round(false_rejection_rate_per_threshold[i - 1] * 100, 2)
        table_thresh_rates_i = f'{i * histogram_bin_size}%\t\t{far}%\t\t{frr}%\n'
        print(table_thresh_rates_i)
        append_lines_to_report(table_thresh_rates_i)
    table_last_line = f'{100}%\t\t{0.0}%\t\t{100.0}%\n'
    print(table_last_line)
    append_lines_to_report(table_last_line)


def generate_distribution_figures(forgery_labels_mask, genuine_labels_mask, histogram_bins, probabilities):
    plt.hist(probabilities[genuine_labels_mask],
             histogram_bins,
             color='blue',
             label='genuine')
    plt.legend(loc='best')
    plt.savefig('genuine_distribution.png', dpi=200)
    plt.close('all')
    plt.hist(probabilities[forgery_labels_mask],
             histogram_bins,
             color='red',
             label='forgery')
    plt.legend(loc='best')
    plt.savefig('forgery_distribution.png', dpi=200)
    plt.close('all')
    plt.hist(probabilities[genuine_labels_mask],
             histogram_bins,
             color='blue',
             label='genuine')
    plt.hist(probabilities[forgery_labels_mask],
             histogram_bins,
             color='red',
             label='forgery')
    plt.legend(loc='best')
    plt.savefig('distribution.png', dpi=200)
    plt.close('all')


def evaluate_svm(test_feature_vector_classes, special_forgery_feature_vector_classes):
    test_pairs = load_or_supply_and_cache(TEST_PAIRS_FILE,
                                          lambda: generate_pairs(test_feature_vector_classes,
                                                                 special_forgery_feature_vector_classes))

    model = load_model(SIAMESE_LOGITS_MODEL_FILE_NAME + '.h5', compile=False)

    data_generator = DataGenerator(test_feature_vector_classes,
                                   test_pairs,
                                   special_forgery_feature_vector_classes,
                                   EVALUATION_BATCH_SIZE,
                                   len(test_pairs[0]))

    logits, labels = [], []
    batches = len(data_generator)
    progress_bar = tqdm(total=batches)
    for i in range(batches):
        input_channels, true_labels = data_generator[i]
        predicted_outputs = model.predict(input_channels)
        labels.extend(list(np.argmax(true_labels, axis=1)))
        logits.extend(list(predicted_outputs))
        progress_bar.update()
    progress_bar.close()

    logits = np.array(logits)
    labels = np.array(labels)
    svc_model = load_cached(SVC_MODEL_FILE_NAME)

    weights = svc_model.coef_
    if USE_THUNDER_SVM:
        intercept = -svc_model.intercept_
    else:
        intercept = svc_model.intercept_

    predictions = np.sum(logits * weights, axis=-1) + intercept[0]

    predicted_labels = np.sign(predictions)
    predicted_labels[predicted_labels == -1] = 0

    (forgery_labels_mask,
     forgeries_count,
     genuine_labels_mask,
     genuines_count) = calculate_accuracy_far_frr(labels,
                                                  predicted_labels)

    scores = np.abs(predictions) / np.linalg.norm(weights[0]) * np.sign(predictions)
    probability_transform = interp1d([np.min(scores), 0, np.max(scores)], [0.0, 50.0, 100.0])
    probabilities = probability_transform(scores)

    histogram_bin_size = 5
    histogram_bins = list(range(0, 101, histogram_bin_size))

    false_acceptance_rate_per_threshold = calculate_false_acceptance_rate_per_threshold(forgery_labels_mask,
                                                                                        forgeries_count,
                                                                                        histogram_bins,
                                                                                        probabilities)
    false_rejection_rate_per_threshold = calculate_false_rejection_rate_per_threshold(genuine_labels_mask,
                                                                                      genuines_count,
                                                                                      histogram_bins,
                                                                                      probabilities)
    generate_distributions_table(false_acceptance_rate_per_threshold,
                                 false_rejection_rate_per_threshold,
                                 histogram_bin_size)
    generate_distribution_figures(forgery_labels_mask,
                                  genuine_labels_mask,
                                  histogram_bins,
                                  probabilities)


def run_pipeline():
    feature_vector_classes = None
    train_feature_vector_classes = None
    validation_feature_vector_classes = None
    test_feature_vector_classes = None
    special_forgery_feature_vector_classes = None

    if not file_exists(TRAIN_FEATURE_VECTOR_CLASSES_FILE) or \
            not file_exists(VALIDATION_FEATURE_VECTOR_CLASSES_FILE) or \
            not file_exists(TEST_FEATURE_VECTOR_CLASSES_FILE):
        if feature_vector_classes is None:
            feature_vector_classes = load_cached(FEATURE_VECTOR_CLASSES_FILE)
        (train_feature_vector_classes,
         validation_feature_vector_classes,
         test_feature_vector_classes) = class_train_validation_test_split(feature_vector_classes)

        if TRAINING_MODE == TrainingMode.TrainWithoutSplittingClasses:
            train_feature_vector_classes = feature_vector_classes

        cache_object(train_feature_vector_classes, TRAIN_FEATURE_VECTOR_CLASSES_FILE)
        cache_object(validation_feature_vector_classes, VALIDATION_FEATURE_VECTOR_CLASSES_FILE)
        cache_object(test_feature_vector_classes, TEST_FEATURE_VECTOR_CLASSES_FILE)

    if not file_exists(MODEL_FILE_NAME + '.h5'):
        if train_feature_vector_classes is None or \
                validation_feature_vector_classes is None:
            train_feature_vector_classes = load_cached(TRAIN_FEATURE_VECTOR_CLASSES_FILE)
            validation_feature_vector_classes = load_cached(VALIDATION_FEATURE_VECTOR_CLASSES_FILE)
        if SPECIAL_FORGERIES_ENABLED and special_forgery_feature_vector_classes is None:
            special_forgery_feature_vector_classes = load_cached(SPECIAL_FORGERY_FEATURE_VECTOR_CLASSES_FILE)
        train_model(train_feature_vector_classes,
                    validation_feature_vector_classes,
                    special_forgery_feature_vector_classes)

    if file_exists(MODEL_FILE_NAME + '.h5'):
        if test_feature_vector_classes is None:
            test_feature_vector_classes = load_cached(TEST_FEATURE_VECTOR_CLASSES_FILE)
        if SPECIAL_FORGERIES_ENABLED and special_forgery_feature_vector_classes is None:
            special_forgery_feature_vector_classes = load_cached(SPECIAL_FORGERY_FEATURE_VECTOR_CLASSES_FILE)
        evaluate_model(test_feature_vector_classes, special_forgery_feature_vector_classes)

    # if file_exists(MODEL_FILE_NAME + '.h5') and not file_exists(SVC_MODEL_FILE_NAME):
    #     if train_feature_vector_classes is None:
    #         train_feature_vector_classes = load_cached(TRAIN_FEATURE_VECTOR_CLASSES_FILE)
    #     if SPECIAL_FORGERIES_ENABLED and special_forgery_feature_vector_classes is None:
    #         special_forgery_feature_vector_classes = load_cached(SPECIAL_FORGERY_FEATURE_VECTOR_CLASSES_FILE)
    #     train_svm(train_feature_vector_classes,
    #               special_forgery_feature_vector_classes)
    #
    # if file_exists(SVC_MODEL_FILE_NAME):
    #     if test_feature_vector_classes is None:
    #         test_feature_vector_classes = load_cached(TEST_FEATURE_VECTOR_CLASSES_FILE)
    #     if SPECIAL_FORGERIES_ENABLED and special_forgery_feature_vector_classes is None:
    #         special_forgery_feature_vector_classes = load_cached(SPECIAL_FORGERY_FEATURE_VECTOR_CLASSES_FILE)
    #     evaluate_svm(test_feature_vector_classes, special_forgery_feature_vector_classes)


if __name__ == '__main__':
    run_pipeline()
