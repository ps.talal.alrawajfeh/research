import math
import os.path
import pickle
import random

import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import interpolate
from thundersvm import SVC

# from sklearn.svm import SVC

REPORT_FILE = 'report.txt'

REFERENCES_PER_CLASS = 14
FORGERY_CLASSES_PER_REFERENCE = 14
FORGERIES_PER_FORGERY_CLASS = 4

GENUINE_LABEL = 1
FORGERY_LABEL = 0


def cache_object(obj, file):
    with open(file, 'wb') as f:
        pickle.dump(obj, f)


def load_cached(file):
    with open(file, 'rb') as f:
        return pickle.load(f)


def generate_pairs(feature_vector_classes):
    keys = [*feature_vector_classes]
    number_of_classes = len(keys)

    references = []
    others = []
    labels = []

    for c in range(number_of_classes - FORGERY_CLASSES_PER_REFERENCE):
        c = random.randint(0, len(keys) - 1)
        key = keys.pop(c)

        reference_indices = random.sample(list(range(len(feature_vector_classes[key]))),
                                          min(REFERENCES_PER_CLASS, len(feature_vector_classes[key])))

        for r in reference_indices:
            reference = [key, r]

            genuines = [[key, i] for i in range(len(feature_vector_classes[key]))]
            references.extend([reference] * len(genuines))
            others.extend(genuines)
            labels.extend([GENUINE_LABEL] * len(genuines))

            forgery_keys = random.sample(keys, min(FORGERY_CLASSES_PER_REFERENCE, len(keys)))
            for forgery_key in forgery_keys:
                forgery_class = feature_vector_classes[forgery_key]
                forgery_indices = random.sample(list(range(len(forgery_class))),
                                                min(FORGERIES_PER_FORGERY_CLASS, len(forgery_class)))
                forgeries = [[forgery_key, f] for f in forgery_indices]

                references.extend([reference] * len(forgeries))
                others.extend(forgeries)
                labels.extend([FORGERY_LABEL] * len(forgeries))

    return references, others, labels


def dichotomy_transform(pairs):
    references, others, labels = pairs

    return np.abs(references - others), labels


def load_feature_vector_classes():
    train_classes = load_cached('train_feature_vector_classes.pickle')
    validation_classes = load_cached('validation_feature_vector_classes.pickle')
    test_classes = load_cached('test_feature_vector_classes.pickle')
    return test_classes, train_classes, validation_classes


def generate_or_load_existing_pairs(file_name, vector_classes):
    pairs = None
    if not os.path.isfile(file_name):
        references, others, labels = generate_pairs(vector_classes)
        vectors1 = [vector_classes[c][i] for c, i in references]
        vectors2 = [vector_classes[c][i] for c, i in others]
        cache_object((np.array(vectors1), np.array(vectors2), np.array(labels, np.int32)), file_name)
    if pairs is None:
        pairs = load_cached(file_name)
    return pairs


def apply_rbf_kernel(vectors, gamma=math.pow(2, -5)):
    norms = np.linalg.norm(vectors, axis=-1)
    return np.exp(-gamma * norms ** 2)


def generate_or_load_existing_vectors_and_labels(pairs,
                                                 vectors_file,
                                                 labels_file):
    vectors, labels = (None, None)
    if not os.path.isfile(vectors_file) or \
            not os.path.isfile(labels_file):
        vectors, labels = dichotomy_transform(pairs)
        vectors = apply_rbf_kernel(vectors)
        cache_object(vectors, vectors_file)
        cache_object(labels, labels_file)
    if vectors is None or labels is None:
        vectors = load_cached(vectors_file)
        labels = load_cached(labels_file)
    return vectors, labels


def train_model_or_load_existing(train_vectors, train_labels):
    svc_model = None
    if not os.path.isfile('svc_model.pickle'):
        svc_model = SVC(kernel='linear', C=1.0, verbose=True)
        svc_model.fit(np.expand_dims(train_vectors, axis=-1), train_labels)
        cache_object(svc_model, 'svc_model.pickle')
    if svc_model is None:
        svc_model = load_cached('svc_model.pickle')
    return svc_model


def generate_distribution_figures(forgery_labels_mask, genuine_labels_mask, histogram_bins, probabilities):
    plt.hist(probabilities[genuine_labels_mask],
             histogram_bins,
             color='blue',
             label='genuine')
    plt.legend(loc='best')
    plt.savefig('genuine_distribution.png', dpi=200)
    plt.close('all')
    plt.hist(probabilities[forgery_labels_mask],
             histogram_bins,
             color='red',
             label='forgery')
    plt.legend(loc='best')
    plt.savefig('forgery_distribution.png', dpi=200)
    plt.close('all')
    plt.hist(probabilities[genuine_labels_mask],
             histogram_bins,
             color='blue',
             label='genuine')
    plt.hist(probabilities[forgery_labels_mask],
             histogram_bins,
             color='red',
             label='forgery')
    plt.legend(loc='best')
    plt.savefig('distribution.png', dpi=200)
    plt.close('all')


def write_line_to_report(line):
    with open(REPORT_FILE, 'a') as f:
        f.write(line + '\n')


def generate_distributions_table(false_acceptance_rate_per_threshold, false_rejection_rate_per_threshold,
                                 histogram_bin_size):
    table_header = 'THRESH\t\tFAR\t\tFRR'
    print(table_header)
    write_line_to_report(table_header)
    for i in range(0, 100 // histogram_bin_size):
        if i == 0:
            far = 100.0
            frr = 0.0
        else:
            far = round(false_acceptance_rate_per_threshold[i - 1] * 100, 2)
            frr = round(false_rejection_rate_per_threshold[i - 1] * 100, 2)
        table_thresh_rates_i = f'{i * histogram_bin_size}%\t\t{far}%\t\t{frr}%'
        print(table_thresh_rates_i)
        write_line_to_report(table_thresh_rates_i)
    table_last_line = f'{100}%\t\t{0.0}%\t\t{100.0}%'
    print(table_last_line)
    write_line_to_report(table_last_line)


def calculate_false_acceptance_rate_per_threshold(forgery_labels_mask, forgeries_count, histogram_bins, probabilities):
    false_acceptances = probabilities[forgery_labels_mask]
    false_acceptances_histogram = np.histogram(false_acceptances, histogram_bins)[0]
    false_acceptance_per_threshold = np.cumsum(false_acceptances_histogram)
    false_acceptance_per_threshold = forgeries_count - false_acceptance_per_threshold
    false_acceptance_rate_per_threshold = false_acceptance_per_threshold / forgeries_count
    return false_acceptance_rate_per_threshold


def calculate_false_rejection_rate_per_threshold(genuine_labels_mask,
                                                 genuines_count,
                                                 histogram_bins,
                                                 probabilities):
    false_rejections = probabilities[genuine_labels_mask]
    false_rejections_histogram = np.histogram(false_rejections, histogram_bins)[0]
    false_rejection_per_threshold = np.cumsum(false_rejections_histogram)
    false_rejection_rate_per_threshold = false_rejection_per_threshold / genuines_count
    return false_rejection_rate_per_threshold


def calculate_svm_margin(svm_weight, svm_intercept):
    return -svm_intercept / svm_weight


def get_probability_transform(vectors, margin):
    min_vectors = np.min(vectors)
    max_vectors = np.max(vectors)
    probability_transform = interpolate.interp1d([min_vectors, margin, max_vectors],
                                                 [0.0, 50.0, 100.0])
    return probability_transform


def generate_report(vectors, labels, predictions, svm_weight, svm_intercept):
    overall_accuracy = np.mean(np.array(np.abs(predictions - labels) < 1e-7, np.float32))

    forgery_labels_mask = labels == 0
    forgeries_count = np.count_nonzero(forgery_labels_mask)
    expected_forgeries = predictions[forgery_labels_mask]
    false_acceptance_count = np.count_nonzero(expected_forgeries == 1)
    false_acceptance = false_acceptance_count / forgeries_count

    genuine_labels_mask = labels == 1
    genuines_count = np.count_nonzero(genuine_labels_mask)
    expected_genuines = predictions[genuine_labels_mask]
    false_rejection_count = np.count_nonzero(expected_genuines == 0)
    false_rejection = false_rejection_count / genuines_count

    overall_accuracy_line = f'accuracy: {round(overall_accuracy * 100, 2)}%'
    print(overall_accuracy_line)
    write_line_to_report(overall_accuracy_line)
    false_acceptance_rate_line = f'false acceptance: {round(false_acceptance * 100, 2)}%'
    print(false_acceptance_rate_line)
    write_line_to_report(false_acceptance_rate_line)
    false_rejection_rate_line = f'false rejection: {round(false_rejection * 100, 2)}%'
    print(false_rejection_rate_line)
    write_line_to_report(false_rejection_rate_line)
    print('')
    write_line_to_report('')

    margin = calculate_svm_margin(svm_weight, svm_intercept)
    probability_transform = get_probability_transform(vectors, margin)
    probabilities = probability_transform(vectors)

    histogram_bin_size = 5
    histogram_bins = list(range(0, 101, histogram_bin_size))

    false_acceptance_rate_per_threshold = calculate_false_acceptance_rate_per_threshold(forgery_labels_mask,
                                                                                        forgeries_count, histogram_bins,
                                                                                        probabilities)
    false_rejection_rate_per_threshold = calculate_false_rejection_rate_per_threshold(genuine_labels_mask,
                                                                                      genuines_count, histogram_bins,
                                                                                      probabilities)
    generate_distributions_table(false_acceptance_rate_per_threshold,
                                 false_rejection_rate_per_threshold,
                                 histogram_bin_size)
    generate_distribution_figures(forgery_labels_mask,
                                  genuine_labels_mask,
                                  histogram_bins,
                                  probabilities)


def run():
    test_classes, train_classes, validation_classes = load_feature_vector_classes()

    # train_pairs = generate_or_load_existing_pairs('triplet_feature_vectors_train_pairs.pickle', train_classes)
    validation_pairs = generate_or_load_existing_pairs('triplet_feature_vectors_validation_pairs.pickle',
                                                       validation_classes)
    test_pairs = generate_or_load_existing_pairs('triplet_feature_vectors_test_pairs.pickle', test_classes)

    # train_vectors, train_labels = generate_or_load_existing_vectors_and_labels(train_pairs,
    #                                                                            'triplet_feature_vectors_train_vectors.pickle',
    #                                                                            'triplet_feature_vectors_train_labels.pickle')
    validation_vectors, validation_labels = generate_or_load_existing_vectors_and_labels(validation_pairs,
                                                                                         'triplet_feature_vectors_validation_vectors.pickle',
                                                                                         'triplet_feature_vectors_validation_labels.pickle')
    test_vectors, test_labels = generate_or_load_existing_vectors_and_labels(test_pairs,
                                                                             'triplet_feature_vectors_test_vectors.pickle',
                                                                             'triplet_feature_vectors_test_labels.pickle')

    svc_model = train_model_or_load_existing(validation_vectors, validation_labels)

    test_predictions = svc_model.predict(np.expand_dims(test_vectors, axis=-1))

    # note: In thundersvm, the intercept is negated; however in scikit-learn (libSVM) the intercept is not negated.
    intercept = -svc_model.intercept_[0]
    weight = svc_model.coef_[0, 0]
    generate_report(test_vectors,
                    test_labels,
                    test_predictions,
                    weight,
                    intercept)


if __name__ == '__main__':
    run()
