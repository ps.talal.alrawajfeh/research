import mimetypes
import os
import pickle

import cv2
import numpy as np
from tqdm import tqdm
import tensorflow as tf

DATA_BASE_PATH = '/home/user/Development/data/'
SIGNATURE_CLASSES_PATHS = [DATA_BASE_PATH + 'CABHBTF',
                           DATA_BASE_PATH + 'trainingSignatures']

CLASS_NUMBER_LENGTH = 9
NUMBER_OF_IMAGES_PER_CLASS_THRESHOLD = 2
NUMBER_OF_IMAGES_TO_AUGMENTATIONS_PER_IMAGE = 4
NUMBER_OF_SPECIAL_FORGERIES_AUGMENTED_IMAGES = 1

PRE_TRAINED_MODEL_INPUT_IMAGE_SIZE = (128, 128)
PRE_TRAINED_MODEL_INPUT_IMAGE_SHAPE = (128, 128, 1)


def threshold(image):
    return cv2.threshold(
        image,
        0,
        255,
        cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]


def remove_white_border(binary_image):
    mask = (255 - binary_image) > 0

    height, width = binary_image.shape[0:2]
    mask1, mask2 = mask.any(0), mask.any(1)
    x1, x2 = mask1.argmax(), width - mask1[::-1].argmax()
    y1, y2 = mask2.argmax(), height - mask2[::-1].argmax()

    return binary_image[y1:y2, x1:x2]


def pre_process_image(image):
    return remove_white_border(threshold(image))


def pre_process_for_model(image):
    if image.shape[1] == PRE_TRAINED_MODEL_INPUT_IMAGE_SIZE[0] and \
            image.shape[0] == PRE_TRAINED_MODEL_INPUT_IMAGE_SIZE[1]:
        resized = image
    else:
        resized = cv2.resize(image, PRE_TRAINED_MODEL_INPUT_IMAGE_SIZE, interpolation=cv2.INTER_CUBIC)
        resized = threshold(resized)

    inverted = 255 - resized
    normalized = inverted / 255.0
    return normalized


PRE_TRAINED_MODEL_PRE_PROCESSOR = lambda image: pre_process_for_model(pre_process_image(image))
PRE_TRAINED_MODEL = tf.keras.models.load_model('embedding_network.h5')


# -----------------------------------------------------------------------------

# Returns all the extensions for a given mime type.
def get_extensions_for_type(general_type):
    for ext in mimetypes.types_map:
        if mimetypes.types_map[ext].split('/')[0] == general_type:
            yield ext.lower()


# Function decorator for caching outputs.
def memoize(function):
    memo = {}

    def wrapper(*args):
        if args in memo:
            return memo[args]
        else:
            rv = function(*args)
            memo[args] = rv
            return rv

    return wrapper


# Returns all image extensions, e.g. '.jpg'.
@memoize
def get_image_extensions():
    return tuple(get_extensions_for_type('image'))


def file_extension(path):
    return os.path.splitext(os.path.basename(path))[1].lower()


def is_image(path):
    return file_extension(path) in get_image_extensions()


def to_int(number):
    return int(round(number))


# -----------------------------------------------------------------------------


def read_image_grayscale(image_path):
    return cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)


def resize_image_high_quality(image, new_size):
    if new_size[0] == image.shape[1] and new_size[1] == image.shape[0]:
        return np.array(image)
    return cv2.resize(image,
                      new_size,
                      interpolation=cv2.INTER_CUBIC)


def images_to_feature_vectors(image_batch,
                              model=PRE_TRAINED_MODEL,
                              model_specific_pre_processor=PRE_TRAINED_MODEL_PRE_PROCESSOR):
    return model.predict(
        np.array([model_specific_pre_processor(image) for image in image_batch], np.float32)
    )


def class_from_file_name(file_name):
    return file_name[0:CLASS_NUMBER_LENGTH]


def extract_classes(iterable,
                    class_lambda,
                    item_mapper_lambda=lambda x: x):
    classes = dict()
    for item in iterable:
        c = class_lambda(item)
        if c not in classes:
            classes[c] = []
        classes[c].append(item_mapper_lambda(item))
    return classes


def extract_classes_from_file_names(paths,
                                    file_name_class_lambda=class_from_file_name):
    image_classes = dict()
    for path in paths:
        image_classes.update(extract_classes(os.listdir(path),
                                             file_name_class_lambda,
                                             lambda f: os.path.join(path, f)))
    return image_classes


def image_classes_from_paths(image_paths):
    image_paths_classes = extract_classes_from_file_names(image_paths, class_from_file_name)

    image_classes = dict()
    for c in image_paths_classes:
        image_classes[c] = [read_image_grayscale(image_path)
                            for image_path in image_paths_classes[c]]
    return image_classes


def cache_object(obj, file):
    with open(file, 'wb') as f:
        pickle.dump(obj, f)


def load_cached(file):
    with open(file, 'rb') as f:
        return pickle.load(f)


def load_image_classes():
    image_classes = None
    if not os.path.isfile('image_classes.pickle'):
        image_classes = image_classes_from_paths(SIGNATURE_CLASSES_PATHS)
        cache_object(image_classes, 'image_classes.pickle')
    if image_classes is None:
        image_classes = load_cached('image_classes.pickle')
    return image_classes


class ImageBatchVectorizer:
    def __init__(self, batch_size=512):
        self.__images = []
        self.__classes = []
        self.__feature_vector_classes = dict()
        self.__batch_size = batch_size

    def __predict(self):
        if len(self.__images) == 0:
            return
        feature_vectors = images_to_feature_vectors(self.__images)
        for i in range(len(self.__classes)):
            c = self.__classes[i]
            if c not in self.__feature_vector_classes:
                self.__feature_vector_classes[c] = []
            self.__feature_vector_classes[c].append(feature_vectors[i])
        self.__images = []
        self.__classes = []

    def __predict_if_batch_size_reached(self):
        if len(self.__images) >= self.__batch_size:
            self.__predict()

    def feed_images(self, images, classes):
        for i in range(len(images)):
            self.__images.append(images[i])
            self.__classes.append(classes[i])
            self.__predict_if_batch_size_reached()

    def feed_image(self, image, image_class):
        self.__images.append(image)
        self.__classes.append(image_class)
        self.__predict_if_batch_size_reached()

    def finalize(self):
        self.__predict()

    def get_feature_vectors(self):
        return self.__feature_vector_classes


def generate_feature_vectors(image_classes):
    progress_bar = tqdm(total=len(image_classes))
    image_batch_vectorizer = ImageBatchVectorizer()
    for c in image_classes:
        batch = image_classes[c]
        image_batch_vectorizer.feed_images(batch, [c] * len(batch))
        progress_bar.update()
    image_batch_vectorizer.finalize()
    progress_bar.close()
    cache_object(image_batch_vectorizer.get_feature_vectors(), 'feature_vector_classes.pickle')


def run():
    print('loading image classes...')
    image_classes = load_image_classes()

    print('generating feature vectors...')
    generate_feature_vectors(image_classes)


if __name__ == '__main__':
    run()
