cd ~/Downloads
git clone https://github.com/Xtra-Computing/thundersvm.git
cd thundersvm/
git submodule update --init src/test/googletest
mkdir build
cd build
cmake .. -DBUILD_TESTS=ON
make -j runtest
sudo make install

cd ..
cd python/
python3 setup.py bdist_wheel --universal
cd dist/
python3 -m pip install --user ./thundersvm-0.3.4-cp38-cp38-linux_x86_64.whl
