import math
import os
import pickle
import random

import numpy as np
import tensorflow as tf
from alt_model_checkpoint.keras import AltModelCheckpoint
from tensorflow.keras import Model
from tensorflow.keras.callbacks import LambdaCallback, ModelCheckpoint
from tensorflow.keras.layers import Input, BatchNormalization, Dense, ReLU, Concatenate, Dropout
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.utils import Sequence
from tqdm import tqdm

USE_THUNDER_SVM = True

if USE_THUNDER_SVM:
    pass
else:
    pass

FEATURE_VECTOR_CLASSES_FILE = 'feature_vector_classes.pickle'
TRAIN_FEATURE_VECTOR_CLASSES_FILE = 'train_feature_vector_classes.pickle'
VALIDATION_FEATURE_VECTOR_CLASSES_FILE = 'validation_feature_vector_classes.pickle'
TEST_FEATURE_VECTOR_CLASSES_FILE = 'test_feature_vector_classes.pickle'
TRAIN_TRIPLETS_FILE = 'train_triplets.pickle'
VALIDATION_TRIPLETS_FILE = 'validation_triplets.pickle'
TEST_TRIPLETS_FILE = 'test_triplets.pickle'
EVALUATION_REPORT_FILE = 'report.txt'
EMBEDDING_NETWORK_FILE_NAME = 'embedding_network.h5'
COMBINED_MODEL_FILE_NAME = 'combined_model.h5'

MARGIN = 0.6
EMBEDDING_LAYER_SIZE = 128
EMBEDDING_VECTOR_SIZE = 1920
LEAKY_RELU_ALPHA = 0.05
USE_ALL_ANCHORS = False
NUMBER_OF_ANCHORS = 10
FORGERY_CLASSES_PER_REFERENCE = 40
FORGERIES_PER_CLASS = 10
TRAIN_CLASSES_PERCENTAGE = 0.7
VALIDATION_CLASSES_PERCENTAGE = 0.15
TEST_CLASSES_PERCENTAGE = 0.15

TRAINING_BATCH_SIZE = 128
VALIDATION_BATCH_SIZE = 1024
EPOCHS = 100


def squared_euclidean_distance(x, y):
    return tf.reduce_sum(tf.square(x - y), axis=-1)


def euclidean_distance(x, y):
    return tf.math.sqrt(tf.maximum(tf.reduce_sum(tf.square(x - y), axis=-1), 0.0))


def triplet_loss(_, y_predict, margin=MARGIN):
    anchor = y_predict[:, 0:EMBEDDING_LAYER_SIZE]
    positive = y_predict[:, EMBEDDING_LAYER_SIZE: EMBEDDING_LAYER_SIZE * 2]
    negative = y_predict[:, EMBEDDING_LAYER_SIZE * 2: EMBEDDING_LAYER_SIZE * 3]

    positive_dist = euclidean_distance(anchor, positive)
    negative_dist = euclidean_distance(anchor, negative)

    denominator = tf.math.exp(positive_dist) + tf.math.exp(negative_dist) + 1e-7
    d_positive = (tf.math.exp(positive_dist) + 1e-7) / denominator
    d_negative = (tf.math.exp(negative_dist) + 1e-7) / denominator

    # return tf.reduce_mean(tf.maximum(positive_dist - negative_dist + margin, 0.0))
    return tf.math.reduce_mean(-tf.math.log(d_negative))


def accuracy(_, y_predict, margin=MARGIN):
    anchor = y_predict[:, 0:EMBEDDING_LAYER_SIZE]
    positive = y_predict[:, EMBEDDING_LAYER_SIZE: EMBEDDING_LAYER_SIZE * 2]
    negative = y_predict[:, EMBEDDING_LAYER_SIZE * 2: EMBEDDING_LAYER_SIZE * 3]

    positive_dist = euclidean_distance(anchor, positive)
    negative_dist = euclidean_distance(anchor, negative)

    return tf.reduce_mean(tf.cast(positive_dist < negative_dist, tf.dtypes.float32))


def leaky_relu6(negative_slope=LEAKY_RELU_ALPHA):
    return ReLU(max_value=6, negative_slope=negative_slope)


def embedding_network(input_shape=(EMBEDDING_VECTOR_SIZE,)):
    embedding = Input(input_shape)
    embedding = BatchNormalization()(embedding)
    embedding = Dropout(0.25)(embedding)

    fc1 = Dense(1024,
                use_bias=False,
                kernel_initializer='he_normal')(embedding)
    fc1 = BatchNormalization()(fc1)
    fc1 = leaky_relu6()(fc1)

    concat_skip_connection = Concatenate()([embedding, fc1])
    concat_skip_connection = Dropout(0.25)(concat_skip_connection)

    fc2 = Dense(1024,
                use_bias=False,
                kernel_initializer='he_normal')(concat_skip_connection)
    fc2 = BatchNormalization()(fc2)
    fc2 = leaky_relu6()(fc2)

    output = Dense(EMBEDDING_LAYER_SIZE,
                   kernel_initializer='he_normal')(fc2)
    output = leaky_relu6()(output)

    return Model(inputs=[embedding], outputs=[output])


def training_wrapped_model(input_shape=(EMBEDDING_VECTOR_SIZE,),
                           embedding_net=embedding_network()):
    anchor = Input(input_shape)
    positive = Input(input_shape)
    negative = Input(input_shape)

    anchor_embedding = embedding_net(anchor)
    positive_embedding = embedding_net(positive)
    negative_embedding = embedding_net(negative)

    merged_vector = Concatenate(axis=-1)(
        [anchor_embedding,
         positive_embedding,
         negative_embedding])

    return Model(inputs=[anchor, positive, negative], outputs=merged_vector)


@tf.function
def absolute_difference(tensors):
    x, y = tensors
    return tf.abs(tf.reduce_sum(tf.stack([x, -y], axis=1), axis=1))


class MinimumRecurrenceRateRandomSamplerWithReplacement:
    def __init__(self, array) -> None:
        self.array = array
        self.left_choices = array.copy()

    def next(self):
        if len(self.left_choices) == 0:
            self.left_choices = self.array.copy()
        i = random.randint(0, len(self.left_choices) - 1)
        return self.left_choices.pop(i)


def generate_triplets(classes,
                      use_all_anchors=USE_ALL_ANCHORS,
                      number_of_anchors=NUMBER_OF_ANCHORS,
                      forgery_classes_per_reference=FORGERY_CLASSES_PER_REFERENCE,
                      forgeries_per_class=FORGERIES_PER_CLASS):
    keys = [*classes]
    number_of_classes = len(keys)

    anchors = []
    positives = []
    negatives = []

    for c in range(number_of_classes):
        key = keys[c]
        keys_except_key = [k for k in keys if k != key]

        if use_all_anchors:
            number_of_anchors = len(classes[key])
        else:
            number_of_anchors = min(number_of_anchors, len(classes[key]))

        if number_of_classes == 0:
            continue

        anchor_indices_sampler = MinimumRecurrenceRateRandomSamplerWithReplacement(list(range(len(classes[key]))))

        for r in range(number_of_anchors):
            anchor_index = anchor_indices_sampler.next()
            reference = [key, anchor_index]
            genuine_indices = list(range(len(classes[key])))
            genuine_indices.pop(anchor_index)
            if len(genuine_indices) == 0:
                continue
            genuine_indices_sampler = MinimumRecurrenceRateRandomSamplerWithReplacement(genuine_indices)
            forgery_keys = random.sample(keys_except_key, min(forgery_classes_per_reference, len(keys_except_key)))

            for forgery_key in forgery_keys:
                forgery_class = classes[forgery_key]
                forgery_indices_sampler = MinimumRecurrenceRateRandomSamplerWithReplacement(
                    list(range(len(forgery_class))))
                forgeries = [[forgery_key, forgery_indices_sampler.next()]
                             for _ in range(min(forgeries_per_class, len(forgery_class)))]

                anchors.extend([reference] * len(forgeries))
                positives.extend([[key, genuine_indices_sampler.next()]
                                  for _ in range(len(forgeries))])
                negatives.extend(forgeries)

    return anchors, positives, negatives


def shuffle_triplets(triplets):
    anchors, positives, negatives = triplets
    indices = list(range(len(anchors)))
    random.shuffle(indices)

    shuffled_anchors = [anchors[indices[i]] for i in range(len(anchors))]
    shuffled_positives = [positives[indices[i]] for i in range(len(anchors))]
    shuffled_true_negatives = [negatives[indices[i]] for i in range(len(anchors))]

    return shuffled_anchors, shuffled_positives, shuffled_true_negatives


class DataGenerator(Sequence):
    def __init__(self,
                 feature_vector_classes,
                 triplets,
                 batch_size,
                 count):
        self.batch_size = batch_size
        self.count = count
        self.feature_vector_classes = feature_vector_classes
        self.anchors, self.positives, self.negatives = shuffle_triplets(triplets)
        self.true_output = np.array([[0] for _ in range(batch_size)])

    def __len__(self):
        return math.ceil(self.count / self.batch_size)

    def __getitem__(self, index):
        anchors, positives, negatives = [], [], []
        for i in range(index * self.batch_size, min((index + 1) * self.batch_size, len(self.anchors))):
            anchor_pair = self.anchors[i]
            positive_pair = self.positives[i]
            negative_pair = self.negatives[i]
            anchors.append(self.feature_vector_classes[anchor_pair[0]][anchor_pair[1]])
            positives.append(self.feature_vector_classes[positive_pair[0]][positive_pair[1]])
            negatives.append(self.feature_vector_classes[negative_pair[0]][negative_pair[1]])

        if len(anchors) == 0:
            return self.__getitem__(0)

        true_output = self.true_output
        if len(anchors) != self.batch_size:
            true_output = np.array([[0] for _ in range(len(anchors))])

        return [np.array(anchors),
                np.array(positives),
                np.array(negatives)], true_output

    def on_epoch_end(self):
        self.anchors, self.positives, self.negatives = shuffle_triplets([self.anchors, self.positives, self.negatives])


def class_train_validation_test_split(feature_vector_classes):
    keys = [*feature_vector_classes]

    keys_indices = list(range(len(keys)))
    random.shuffle(keys_indices)

    classes = (dict(), dict(), dict())
    percentages = [TRAIN_CLASSES_PERCENTAGE,
                   VALIDATION_CLASSES_PERCENTAGE,
                   TEST_CLASSES_PERCENTAGE]

    for i in range(3):
        chosen_key_indices = keys_indices[:int(len(feature_vector_classes) * percentages[i])]
        for j in range(len(chosen_key_indices)):
            key = keys[chosen_key_indices[j]]
            classes[i][key] = feature_vector_classes[key]
        keys_indices = keys_indices[len(chosen_key_indices):]

    return classes


def append_lines_to_report(lines):
    with open(EVALUATION_REPORT_FILE, 'a') as f:
        f.writelines(lines)


def epoch_metrics_log(epoch, logs):
    line = f'epoch: {epoch}'
    line += ' - '
    line += f'loss: {logs["loss"]}'
    line += ' - '
    line += f'binary accuracy: {logs["accuracy"]}'
    line += ' - '
    line += f'validation loss: {logs["val_loss"]}'
    line += ' - '
    line += f'validation binary accuracy: {logs["val_accuracy"]}'
    return line


def train(train_feature_vector_classes,
          train_triplets,
          validation_feature_vector_classes,
          validation_triplets):
    embedding_net = embedding_network()
    model = training_wrapped_model(embedding_net=embedding_net)

    embedding_net.summary()
    model.summary()

    model.compile(loss=triplet_loss,
                  optimizer=Adam(clipvalue=6),
                  metrics=[accuracy])

    train_data_generator = DataGenerator(train_feature_vector_classes,
                                         train_triplets,
                                         TRAINING_BATCH_SIZE,
                                         len(train_triplets[0]))

    validation_data_generator = DataGenerator(validation_feature_vector_classes,
                                              validation_triplets,
                                              VALIDATION_BATCH_SIZE,
                                              len(validation_triplets[0]))

    append_lines_to_report('[training log]\n\n')

    update_report_callback = LambdaCallback(
        on_epoch_end=lambda epoch, logs: append_lines_to_report(
            epoch_metrics_log(epoch, logs) + '\n'
        ))

    last_checkpoint_callback = ModelCheckpoint(
        filepath=COMBINED_MODEL_FILE_NAME + '_checkpoint_last.h5',
        save_best_only=False)

    best_checkpoint_callback = ModelCheckpoint(
        filepath=COMBINED_MODEL_FILE_NAME + '_{epoch:02d}_{val_accuracy:0.4f}.h5',
        monitor='val_accuracy',
        mode='max',
        save_best_only=True)

    en_last_model_checkpoint = AltModelCheckpoint(EMBEDDING_NETWORK_FILE_NAME + '_checkpoint_last.h5',
                                                  embedding_net)
    en_best_model_checkpoint = AltModelCheckpoint(
        EMBEDDING_NETWORK_FILE_NAME + '_checkpoint_{epoch:02d}_{val_accuracy:0.4f}.h5',
        embedding_net,
        monitor='val_accuracy',
        save_best_only=True,
        mode='max')

    model.fit_generator(generator=train_data_generator,
                        steps_per_epoch=math.ceil(len(train_triplets[0]) / TRAINING_BATCH_SIZE),
                        validation_data=validation_data_generator,
                        validation_steps=math.ceil(len(validation_triplets[0]) / VALIDATION_BATCH_SIZE),
                        epochs=EPOCHS,
                        callbacks=[update_report_callback,
                                   last_checkpoint_callback,
                                   best_checkpoint_callback,
                                   en_last_model_checkpoint,
                                   en_best_model_checkpoint])


def file_exists(file):
    return os.path.isfile(file)


def serialize_object(obj, file):
    with open(file, 'wb') as f:
        pickle.dump(obj, f)


def deserialize_object(file):
    with open(file, 'rb') as f:
        return pickle.load(f)


def predict_batch_feature_vectors(batch,
                                  model):
    return model.predict(np.array(batch, np.float32))


class BatchVectorizer:
    def __init__(self, model, batch_size=512):
        self.__images = []
        self.__classes = []
        self.__feature_vector_classes = dict()
        self.__batch_size = batch_size
        self.__model = model

    def __predict(self):
        if len(self.__images) == 0:
            return
        feature_vectors = predict_batch_feature_vectors(self.__images, self.__model)
        for i in range(len(self.__classes)):
            c = self.__classes[i]
            if c not in self.__feature_vector_classes:
                self.__feature_vector_classes[c] = []
            self.__feature_vector_classes[c].append(feature_vectors[i])
        self.__images = []
        self.__classes = []

    def __predict_if_batch_size_reached(self):
        if len(self.__images) >= self.__batch_size:
            self.__predict()

    def feed_images(self, images, classes):
        for i in range(len(images)):
            self.__images.append(images[i])
            self.__classes.append(classes[i])
            self.__predict_if_batch_size_reached()

    def feed_image(self, image, image_class):
        self.__images.append(image)
        self.__classes.append(image_class)
        self.__predict_if_batch_size_reached()

    def finalize(self):
        self.__predict()

    def get_feature_vectors(self):
        return self.__feature_vector_classes


def generate_feature_vectors(model,
                             feature_vector_classes,
                             output_file_name):
    progress_bar = tqdm(total=len(feature_vector_classes))
    image_batch_vectorizer = BatchVectorizer(model)
    for c in feature_vector_classes:
        batch = feature_vector_classes[c]
        image_batch_vectorizer.feed_images(batch, [c] * len(batch))
        progress_bar.update()
    image_batch_vectorizer.finalize()
    progress_bar.close()
    serialize_object(image_batch_vectorizer.get_feature_vectors(), output_file_name)


def run_pipeline():
    feature_vector_classes = None

    train_feature_vector_classes = None
    validation_feature_vector_classes = None
    test_feature_vector_classes = None

    train_triplets = None
    validation_triplets = None
    test_triplets = None

    if not file_exists(TRAIN_FEATURE_VECTOR_CLASSES_FILE) or \
            not file_exists(VALIDATION_FEATURE_VECTOR_CLASSES_FILE) or \
            not file_exists(TEST_FEATURE_VECTOR_CLASSES_FILE):
        if feature_vector_classes is None:
            feature_vector_classes = deserialize_object(FEATURE_VECTOR_CLASSES_FILE)

        train_feature_vector_classes, \
        validation_feature_vector_classes, \
        test_feature_vector_classes = class_train_validation_test_split(feature_vector_classes)

        serialize_object(train_feature_vector_classes, TRAIN_FEATURE_VECTOR_CLASSES_FILE)
        serialize_object(validation_feature_vector_classes, VALIDATION_FEATURE_VECTOR_CLASSES_FILE)
        serialize_object(test_feature_vector_classes, TEST_FEATURE_VECTOR_CLASSES_FILE)

        del feature_vector_classes
        feature_vector_classes = None
        del test_feature_vector_classes
        test_feature_vector_classes = None

    if not file_exists(TRAIN_TRIPLETS_FILE):
        if train_feature_vector_classes is None:
            train_feature_vector_classes = deserialize_object(TRAIN_FEATURE_VECTOR_CLASSES_FILE)
        train_triplets = generate_triplets(train_feature_vector_classes)
        serialize_object(train_triplets, TRAIN_TRIPLETS_FILE)

    if not file_exists(VALIDATION_TRIPLETS_FILE):
        if validation_feature_vector_classes is None:
            validation_feature_vector_classes = deserialize_object(VALIDATION_FEATURE_VECTOR_CLASSES_FILE)
        validation_triplets = generate_triplets(validation_feature_vector_classes)
        serialize_object(validation_triplets, VALIDATION_TRIPLETS_FILE)

    if train_feature_vector_classes is None:
        train_feature_vector_classes = deserialize_object(TRAIN_FEATURE_VECTOR_CLASSES_FILE)
    if train_triplets is None:
        train_triplets = deserialize_object(TRAIN_TRIPLETS_FILE)

    if validation_feature_vector_classes is None:
        validation_feature_vector_classes = deserialize_object(VALIDATION_FEATURE_VECTOR_CLASSES_FILE)
    if validation_triplets is None:
        validation_triplets = deserialize_object(VALIDATION_TRIPLETS_FILE)

    if not file_exists(EMBEDDING_NETWORK_FILE_NAME):
        train(train_feature_vector_classes,
              train_triplets,
              validation_feature_vector_classes,
              validation_triplets)

    if not file_exists(TEST_TRIPLETS_FILE):
        if test_feature_vector_classes is None:
            test_feature_vector_classes = deserialize_object(TEST_FEATURE_VECTOR_CLASSES_FILE)
        test_triplets = generate_triplets(test_feature_vector_classes)
        serialize_object(test_triplets, TEST_TRIPLETS_FILE)

    model = tf.keras.models.load_model('embedding_network.h5')
    generate_feature_vectors(model,
                             train_feature_vector_classes,
                             'train_triplet_feature_vector_classes.pickle')
    generate_feature_vectors(model,
                             validation_feature_vector_classes,
                             'validation_triplet_feature_vector_classes.pickle')
    generate_feature_vectors(model,
                             test_feature_vector_classes,
                             'test_triplet_feature_vector_classes.pickle')

    del train_feature_vector_classes
    train_feature_vector_classes = None
    del train_triplets
    train_triplets = None

    del validation_feature_vector_classes
    validation_feature_vector_classes = None
    del validation_triplets
    validation_triplets = None


def main():
    run_pipeline()


if __name__ == '__main__':
    main()
