import math
import random

import matplotlib.pyplot as plt
import numpy as np


def main():
    polar_points1 = [(random.uniform(0, 0.5), random.uniform(-math.pi, math.pi)) for _ in range(20)]
    euclidean_points1 = np.array([(r * math.cos(theta), r * math.sin(theta)) for r, theta in polar_points1])
    euclidean_points1 += np.array([[2, 2]])

    polar_points2 = [(random.uniform(0, 0.5), random.uniform(-math.pi, math.pi)) for _ in range(20)]
    euclidean_points2 = np.array([(r * math.cos(theta), r * math.sin(theta)) for r, theta in polar_points2])
    euclidean_points2 += np.array([[3.25, 2]])

    polar_points3 = [(random.uniform(0, 0.5), random.uniform(-math.pi, math.pi)) for _ in range(20)]
    euclidean_points3 = np.array([(r * math.cos(theta), r * math.sin(theta)) for r, theta in polar_points3])
    euclidean_points3 += np.array([[2, 3.25]])

    plt.scatter(euclidean_points1[:, 0], euclidean_points1[:, 1], color='blue')
    plt.scatter(euclidean_points2[:, 0], euclidean_points2[:, 1], color='red')
    plt.scatter(euclidean_points3[:, 0], euclidean_points3[:, 1], color='green')
    plt.show()

    positive_dichotomies = []
    for i in range(19):
        for j in range(i + 1, 20):
            positive_dichotomies.append(np.abs(euclidean_points1[i] - euclidean_points1[j]))
    for i in range(19):
        for j in range(i + 1, 20):
            positive_dichotomies.append(np.abs(euclidean_points2[i] - euclidean_points2[j]))
    for i in range(19):
        for j in range(i + 1, 20):
            positive_dichotomies.append(np.abs(euclidean_points3[i] - euclidean_points3[j]))

    negative_dichotomies = []
    for i in range(20):
        for j in range(20):
            negative_dichotomies.append(np.abs(euclidean_points1[i] - euclidean_points2[j]))
    for i in range(20):
        for j in range(20):
            negative_dichotomies.append(np.abs(euclidean_points1[i] - euclidean_points3[j]))
    for i in range(20):
        for j in range(20):
            negative_dichotomies.append(np.abs(euclidean_points2[i] - euclidean_points3[j]))

    positive_dichotomies = np.array(positive_dichotomies)
    negative_dichotomies = np.array(negative_dichotomies)

    plt.scatter(positive_dichotomies[:, 0], positive_dichotomies[:, 1], color='purple')
    plt.scatter(negative_dichotomies[:, 0], negative_dichotomies[:, 1], color='yellow')
    plt.show()

    rbf1 = []
    rbf2 = []

    for x in random.sample(positive_dichotomies.tolist(), 20):
        norm = np.sqrt(np.sum(np.square(x)))
        y = math.exp(-norm)
        rbf1.append((0, y))

    for x in random.sample(negative_dichotomies.tolist(), 20):
        norm = np.sqrt(np.sum(np.square(x)))
        y = math.exp(-norm)
        rbf2.append((0, y))

    rbf1 = np.array(rbf1)
    rbf2 = np.array(rbf2)

    plt.scatter(rbf1[:, 0], rbf1[:, 1], color='purple')
    plt.scatter(rbf2[:, 0], rbf2[:, 1], color='yellow')
    plt.show()


if __name__ == '__main__':
    main()
