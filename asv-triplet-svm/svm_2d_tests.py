#!/usr/bin/python3

import numpy as np
from scipy.interpolate import interp1d
from sklearn.svm import SVC


def point_score(point, weights, intercept):
    prediction = np.sum(point * weights, axis=-1) + intercept
    return np.abs(prediction) / np.linalg.norm(weights) * np.sign(prediction)


def main():
    train_x = np.array([[1.0, 0.0], [0.9, 0.1], [0.8, 0.2], [0.3, 0.7], [0.22, 0.78], [0.05, 0.95]])
    train_y = np.array([[1], [1], [1], [0], [0], [0]])

    svc_model = SVC(C=1.0, kernel='linear', verbose=True)
    svc_model.fit(train_x, train_y)

    print(svc_model.coef_)
    print(svc_model.intercept_)

    predictions = np.sum(train_x * svc_model.coef_, axis=-1) + svc_model.intercept_
    predicted_labels = np.sign(predictions)
    predicted_labels[predicted_labels == -1] = 0

    print(train_x)
    print(train_y)
    print(predictions)
    print(predicted_labels)

    scores = []

    for point in train_x:
        scores.append(point_score(point, svc_model.coef_[0], svc_model.intercept_[0]))

    score_to_prob = interp1d([np.min(scores), 0, np.max(scores)], [0.0, 50.0, 100.0])

    for score in scores:
        print(score_to_prob(score))


if __name__ == '__main__':
    main()
