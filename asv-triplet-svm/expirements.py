import data_preparation
import vanilla_svm

if __name__ == '__main__':
    data_preparation.run()
    vanilla_svm.run()
