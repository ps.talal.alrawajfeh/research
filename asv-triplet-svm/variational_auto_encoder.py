import mimetypes
import os
import pickle
import random

import cv2
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from alt_model_checkpoint.tensorflow import AltModelCheckpoint
from tensorflow import keras
from tensorflow.keras import layers

DATA_BASE_PATH = '/home/user/Development/data/'
SIGNATURE_CLASSES_PATHS = [DATA_BASE_PATH + 'CABHBTF',
                           DATA_BASE_PATH + 'trainingSignatures']

IMAGE_SIZE = (128, 128)
LATENT_SPACE_DIM = 128

SPECKLES_FRACTION = 1e-3
MIN_ROTATION_ANGLE = 1
MAX_ROTATION_ANGLE = 5
MIN_QUALITY = 75
MAX_QUALITY = 95
QUALITY_RANDOMIZATION_STEP_SIZE = 5
MORPHOLOGY_VARIATION_MIN_SIZE = 250 * 250
MIN_RESIZE_PERCENTAGE = 90
VARIATIONS_PER_IMAGE = 4
AUGMENT_DATA = True


# Returns all the extensions for a given mime type.
def get_extensions_for_type(general_type):
    for ext in mimetypes.types_map:
        if mimetypes.types_map[ext].split('/')[0] == general_type:
            yield ext.lower()


# Function decorator for caching outputs.
def memoize(function):
    memo = {}

    def wrapper(*args):
        if args in memo:
            return memo[args]
        else:
            rv = function(*args)
            memo[args] = rv
            return rv

    return wrapper


# Returns all image extensions, e.g. '.jpg'.
@memoize
def get_image_extensions():
    return tuple(get_extensions_for_type('image'))


def file_extension(path):
    return os.path.splitext(os.path.basename(path))[1].lower()


def is_image(path):
    return file_extension(path) in get_image_extensions()


class Sampling(layers.Layer):
    """Uses (z_mean, z_log_var) to sample z, the vector encoding a digit."""

    def call(self, inputs):
        z_mean, z_log_var = inputs
        batch = tf.shape(z_mean)[0]
        dim = tf.shape(z_mean)[1]
        epsilon = tf.keras.backend.random_normal(shape=(batch, dim))
        return z_mean + tf.exp(0.5 * z_log_var) * epsilon


class VAE(keras.Model):
    def __init__(self, encoder, decoder, **kwargs):
        super(VAE, self).__init__(**kwargs)
        self.encoder = encoder
        self.decoder = decoder
        self.total_loss_tracker = keras.metrics.Mean(name="total_loss")
        self.reconstruction_loss_tracker = keras.metrics.Mean(
            name="reconstruction_loss"
        )
        self.kl_loss_tracker = keras.metrics.Mean(name="kl_loss")

    @property
    def metrics(self):
        return [
            self.total_loss_tracker,
            self.reconstruction_loss_tracker,
            self.kl_loss_tracker,
        ]

    def train_step(self, data):
        with tf.GradientTape() as tape:
            z_mean, z_log_var, z = self.encoder(data)
            reconstruction = self.decoder(z)
            reconstruction_loss = tf.reduce_mean(
                tf.reduce_sum(
                    keras.losses.binary_crossentropy(data, reconstruction), axis=(1, 2)
                )
            )
            kl_loss = -0.5 * (1 + z_log_var - tf.square(z_mean) - tf.exp(z_log_var))
            kl_loss = tf.reduce_mean(tf.reduce_sum(kl_loss, axis=1))
            total_loss = reconstruction_loss + kl_loss
        grads = tape.gradient(total_loss, self.trainable_weights)
        self.optimizer.apply_gradients(zip(grads, self.trainable_weights))
        self.total_loss_tracker.update_state(total_loss)
        self.reconstruction_loss_tracker.update_state(reconstruction_loss)
        self.kl_loss_tracker.update_state(kl_loss)
        return {
            "loss": self.total_loss_tracker.result(),
            "reconstruction_loss": self.reconstruction_loss_tracker.result(),
            "kl_loss": self.kl_loss_tracker.result(),
        }


def plot_latent_space(vae, n=30, figsize=15):
    # display a n*n 2D manifold of digits
    digit_size = IMAGE_SIZE[0]
    scale = 1.0
    figure = np.zeros((digit_size * n, digit_size * n))
    # linearly spaced coordinates corresponding to the 2D plot
    # of digit classes in the latent space
    grid_x = np.linspace(-scale, scale, n)
    grid_y = np.linspace(-scale, scale, n)[::-1]

    for i, yi in enumerate(grid_y):
        for j, xi in enumerate(grid_x):
            z_sample = np.random.normal(size=(1, LATENT_SPACE_DIM))
            x_decoded = vae.decoder.predict(z_sample)
            digit = x_decoded[0].reshape(digit_size, digit_size)
            figure[
            i * digit_size: (i + 1) * digit_size,
            j * digit_size: (j + 1) * digit_size,
            ] = digit

    plt.figure(figsize=(figsize, figsize))
    start_range = digit_size // 2
    end_range = n * digit_size + start_range
    pixel_range = np.arange(start_range, end_range, digit_size)
    sample_range_x = np.round(grid_x, 1)
    sample_range_y = np.round(grid_y, 1)
    plt.xticks(pixel_range, sample_range_x)
    plt.yticks(pixel_range, sample_range_y)
    plt.xlabel("z[0]")
    plt.ylabel("z[1]")
    plt.imshow(figure, cmap="Greys_r")
    plt.show()


def to_int(number):
    return int(round(number))


def read_image_grayscale(image_path):
    return cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)


def resize_image_high_quality(image, new_size):
    if new_size[0] == image.shape[1] and new_size[1] == image.shape[0]:
        return np.array(image)
    return cv2.resize(image,
                      new_size,
                      interpolation=cv2.INTER_CUBIC)


def threshold(image, value=128):
    image_copy = np.array(image)
    image_copy[image_copy >= value] = 255
    image_copy[image_copy < value] = 0
    return image_copy


def threshold_otsu(image):
    return cv2.threshold(image,
                         0,
                         255,
                         cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]


def remove_white_border(binary_image):
    mask = (255 - binary_image) > 0

    height, width = binary_image.shape[0:2]
    mask1, mask2 = mask.any(0), mask.any(1)
    x1, x2 = mask1.argmax(), width - mask1[::-1].argmax()
    y1, y2 = mask2.argmax(), height - mask2[::-1].argmax()

    return binary_image[y1:y2, x1:x2]


def rotate(image, angle):
    (h, w) = image.shape
    center_x, center_y = w / 2, h / 2

    rotation_matrix = cv2.getRotationMatrix2D((center_x, center_y),
                                              angle,
                                              1.0)
    cos = np.abs(rotation_matrix[0, 0])
    sin = np.abs(rotation_matrix[0, 1])

    new_w = h * sin + w * cos
    new_h = h * cos + w * sin

    rotation_matrix[0, 2] += new_w / 2 - center_x
    rotation_matrix[1, 2] += new_h / 2 - center_y

    return cv2.warpAffine(image,
                          rotation_matrix,
                          (to_int(new_w), to_int(new_h)),
                          borderValue=(255),
                          flags=cv2.INTER_CUBIC)


def change_image_quality(image, quality):
    encoded = cv2.imencode('.jpg', image, [int(cv2.IMWRITE_JPEG_QUALITY), quality])[1]
    return cv2.imdecode(encoded, cv2.IMREAD_GRAYSCALE)


def random_coordinate(image):
    return random.randint(0, image.shape[0] - 1), random.randint(0, image.shape[1] - 1)


def speckle(image, fraction, color=0):
    coordinates = [random_coordinate(image)
                   for _ in range(to_int(image.size * fraction))]

    image_copy = np.array(image)
    for coord in coordinates:
        image_copy[coord[0], coord[1]] = color

    return image_copy


def random_bool():
    return random.random() >= 0.5


def generate_variation(image):
    variation = image

    if random_bool():
        x_factor = random.randint(MIN_RESIZE_PERCENTAGE, 99) / 100
        y_factor = random.randint(MIN_RESIZE_PERCENTAGE, 99) / 100

        variation = cv2.resize(image, (0, 0), fx=x_factor, fy=y_factor, interpolation=cv2.INTER_CUBIC)

    if random_bool():
        variation = change_image_quality(variation, random.randrange(MIN_QUALITY,
                                                                     MAX_QUALITY + 1,
                                                                     QUALITY_RANDOMIZATION_STEP_SIZE))

    if random_bool():
        if random_bool():
            sign = 1
        else:
            sign = -1
        variation = rotate(variation, sign * random.randint(MIN_ROTATION_ANGLE,
                                                            MAX_ROTATION_ANGLE))

    if np.prod(image.shape) >= MORPHOLOGY_VARIATION_MIN_SIZE and random_bool():
        variation = random.choice([
            lambda img: cv2.dilate(img, np.ones((2, 2))),
            lambda img: cv2.erode(img, np.ones((2, 2)))
        ])(variation)

    if random_bool():
        color = 0
        if random_bool():
            color = 255

        variation = speckle(variation,
                            SPECKLES_FRACTION,
                            color)

    return variation


def pre_process_image(image):
    cropped_resized = resize_image_high_quality(remove_white_border(image),
                                                IMAGE_SIZE)
    return 255 - threshold_otsu(cropped_resized)


def cache_object(obj, file):
    with open(file, 'wb') as f:
        pickle.dump(obj, f)


def load_cached(file):
    with open(file, 'rb') as f:
        return pickle.load(f)


def main():
    latent_dim = LATENT_SPACE_DIM

    encoder_inputs = keras.Input(shape=(IMAGE_SIZE[0], IMAGE_SIZE[1], 1))

    x = layers.Conv2D(16, 3, strides=2, padding='same')(encoder_inputs)
    x = layers.BatchNormalization()(x)
    x = layers.PReLU()(x)

    x = layers.Conv2D(32, 3, strides=2, padding='same')(x)
    x = layers.BatchNormalization()(x)
    x = layers.PReLU()(x)

    x = layers.Conv2D(64, 3, strides=2, padding='same')(x)
    x = layers.BatchNormalization()(x)
    x = layers.PReLU()(x)

    x = layers.Conv2D(128, 3, strides=2, padding='same')(x)
    x = layers.BatchNormalization()(x)
    x = layers.PReLU()(x)

    x = layers.Flatten()(x)
    x = layers.Dense(LATENT_SPACE_DIM)(x)
    x = layers.BatchNormalization()(x)
    x = layers.PReLU()(x)

    z_mean = layers.Dense(latent_dim, name='z_mean')(x)
    z_log_var = layers.Dense(latent_dim, name='z_log_var')(x)
    z = Sampling()([z_mean, z_log_var])
    encoder = keras.Model(encoder_inputs, [z_mean, z_log_var, z], name='encoder')
    encoder.summary()

    latent_inputs = keras.Input(shape=(latent_dim,))
    x = layers.Dense(8 * 8 * 128)(latent_inputs)
    x = layers.Reshape((8, 8, 128))(x)
    x = layers.BatchNormalization()(x)
    x = layers.PReLU()(x)

    x = layers.Conv2DTranspose(128, 3, strides=2, padding='same')(x)
    x = layers.BatchNormalization()(x)
    x = layers.PReLU()(x)

    x = layers.Conv2DTranspose(64, 3, strides=2, padding='same')(x)
    x = layers.BatchNormalization()(x)
    x = layers.PReLU()(x)

    x = layers.Conv2DTranspose(32, 3, strides=2, padding='same')(x)
    x = layers.BatchNormalization()(x)
    x = layers.PReLU()(x)

    x = layers.Conv2DTranspose(16, 3, strides=2, padding='same')(x)
    x = layers.BatchNormalization()(x)
    x = layers.PReLU()(x)

    decoder_outputs = layers.Conv2DTranspose(1, 3, padding='same')(x)
    decoder_outputs = layers.BatchNormalization()(decoder_outputs)
    decoder_outputs = layers.Activation('sigmoid')(decoder_outputs)

    decoder = keras.Model(latent_inputs, decoder_outputs, name='decoder')
    decoder.summary()

    if not os.path.exists('vae_data.pickle'):
        images = []
        for path in SIGNATURE_CLASSES_PATHS:
            files = os.listdir(path)
            for f in files:
                image_path = os.path.join(path, f)
                if is_image(image_path):
                    images.append(cv2.imread(image_path, cv2.IMREAD_GRAYSCALE))

        variations = []
        for image in images:
            variations.extend([generate_variation(image) for _ in range(2)])

        images.extend(variations)
        images = [pre_process_image(image) for image in images]
        images = np.expand_dims(images, -1) / 255

        cache_object(images, 'vae_data.pickle')
    else:
        images = load_cached('vae_data.pickle')

    encoder_model_checkpoint_callback = AltModelCheckpoint('encoder_checkpoint_last.h5',
                                                           encoder)
    decoder_model_checkpoint_callback = AltModelCheckpoint('decoder_checkpoint_last.h5',
                                                           decoder)

    if os.path.isfile('encoder_checkpoint_last.h5') and os.path.isfile('decoder_checkpoint_last.h5'):
        encoder = tf.keras.models.load_model('encoder_checkpoint_last.h5',
                                             custom_objects={'Sampling': Sampling})
        decoder = tf.keras.models.load_model('decoder_checkpoint_last.h5')

    vae = VAE(encoder, decoder)
    vae.compile(optimizer=keras.optimizers.Adam())
    vae.fit(images, epochs=2000, batch_size=64, callbacks=[encoder_model_checkpoint_callback,
                                                           decoder_model_checkpoint_callback])

    for i in range(1, 1001):
        z_sample = np.random.uniform(low=-10, high=10, size=(1, LATENT_SPACE_DIM))
        # z_sample = np.random.normal(size=(1, LATENT_SPACE_DIM))
        x_decoded = decoder.predict(z_sample)
        signature = x_decoded[0].reshape(IMAGE_SIZE)
        signature = 255 - signature * 255
        signature[signature > 255] = 255
        signature = np.array(signature, np.uint8)

        cv2.imwrite(f'/home/user/Desktop/generated/{i}.bmp', signature)


if __name__ == '__main__':
    main()
