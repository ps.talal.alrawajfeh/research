import random

import numpy as np
from matplotlib import pyplot as plt
from sklearn.metrics import accuracy_score, confusion_matrix, classification_report
from terminaltables import AsciiTable
from tqdm import tqdm


def calculate_false_acceptance_rate_per_threshold(forgeries_count,
                                                  histogram_bins,
                                                  forgery_probabilities):
    false_acceptances = forgery_probabilities
    false_acceptances_histogram = np.histogram(false_acceptances, histogram_bins)[0]
    false_acceptance_per_threshold = np.cumsum(false_acceptances_histogram)
    false_acceptance_per_threshold = forgeries_count - false_acceptance_per_threshold
    false_acceptance_rate_per_threshold = false_acceptance_per_threshold / forgeries_count
    return false_acceptance_rate_per_threshold


def calculate_false_rejection_rate_per_threshold(genuines_count,
                                                 histogram_bins,
                                                 genuine_probabilities):
    false_rejections = genuine_probabilities
    false_rejections_histogram = np.histogram(false_rejections, histogram_bins)[0]
    false_rejection_per_threshold = np.cumsum(false_rejections_histogram)
    false_rejection_rate_per_threshold = false_rejection_per_threshold / genuines_count
    return false_rejection_rate_per_threshold


def append_lines_to_report(file_path, lines):
    with open(file_path, 'a') as f:
        if isinstance(lines, list):
            f.writelines([line + '\n' for line in lines])
        elif isinstance(lines, str):
            f.writelines(lines + '\n')
        else:
            f.writelines(lines)


def write_distributions_table(report_file_path,
                              false_acceptance_rate_per_threshold,
                              false_rejection_rate_per_threshold,
                              histogram_bin_size):
    table = [['Threshold', 'FA Rate', 'FR Rate']]
    for i in range(0, 100 // histogram_bin_size):
        if i == 0:
            far = 100.0
            frr = 0.0
        else:
            far = round(false_acceptance_rate_per_threshold[i - 1] * 100, 2)
            frr = round(false_rejection_rate_per_threshold[i - 1] * 100, 2)
        table.append([f'{i * histogram_bin_size}%', f'{far}%', f'{frr}%'])
    table.append([f'{100}%', f'{0.0}%', f'{100.0}%'])
    table = AsciiTable(table).table
    print(table)
    append_lines_to_report(report_file_path, table)


def write_expected_false_acceptances_table(report_file_path,
                                           false_acceptance_rate_per_threshold,
                                           histogram_bin_size):
    table = [['Threshold',
              'FA Count per 10K verification',
              'FA Count per 100K verification',
              'FA Count per 1M verification']]
    for i in range(0, 100 // histogram_bin_size):
        if i == 0:
            far = 1.0
        else:
            far = false_acceptance_rate_per_threshold[i - 1]
        table.append([f'{i * histogram_bin_size}%',
                      f'{int(far * 10000)}',
                      f'{int(far * 100000)}',
                      f'{int(far * 1000000)}'])
    table.append([f'{100}%', f'{0}', f'{0}', f'{0}'])
    table = AsciiTable(table).table
    print(table)
    append_lines_to_report(report_file_path, table)


def write_expected_false_rejections_table(report_file_path,
                                          false_rejection_rate_per_threshold,
                                          histogram_bin_size):
    table = [['Threshold',
              'FR Count per 10K verification',
              'FR Count per 100K verification',
              'FR Count per 1M verification']]
    for i in range(0, 100 // histogram_bin_size):
        if i == 0:
            frr = 0.0
        else:
            frr = false_rejection_rate_per_threshold[i - 1]
        table.append([f'{i * histogram_bin_size}%',
                      f'{int(frr * 10000)}',
                      f'{int(frr * 100000)}',
                      f'{int(frr * 1000000)}'])
    table.append([f'{100}%', f'{10000}', f'{100000}', f'{1000000}'])
    table = AsciiTable(table).table
    print(table)
    append_lines_to_report(report_file_path, table)


def plot_distributions(genuine_distribution_file_path,
                       forgery_distribution_file_path,
                       mixed_distributions_file_path,
                       genuine_probabilities,
                       forgery_probabilities,
                       histogram_bins):
    plt.hist(genuine_probabilities,
             histogram_bins,
             color='blue',
             label='genuine')
    plt.legend(loc='best')
    plt.savefig(genuine_distribution_file_path, dpi=200)
    plt.close('all')

    plt.hist(forgery_probabilities,
             histogram_bins,
             color='red',
             label='forgery')
    plt.legend(loc='best')
    plt.savefig(forgery_distribution_file_path, dpi=200)
    plt.close('all')

    plt.hist(genuine_probabilities,
             histogram_bins,
             color='blue',
             label='genuine')
    plt.legend(loc='best')
    plt.hist(forgery_probabilities,
             histogram_bins,
             color='red',
             label='forgery')
    plt.legend(loc='best')
    plt.savefig(mixed_distributions_file_path, dpi=200)
    plt.close('all')


def write_classification_report(report_file_path,
                                all_true_labels,
                                all_predicted_labels):
    accuracy = accuracy_score(1 - all_true_labels, 1 - all_predicted_labels)
    append_lines_to_report(report_file_path,
                           f'\ntest accuracy: {round(accuracy * 100, 2)}%\n')

    append_lines_to_report(report_file_path,
                           'confusion matrix:')
    matrix = confusion_matrix(1 - all_true_labels, 1 - all_predicted_labels)
    append_lines_to_report(report_file_path,
                           f'{matrix}')

    frr = round((matrix[0, 1] / np.sum(matrix[0])) * 100, 2)
    far = round((matrix[1, 0] / np.sum(matrix[1])) * 100, 2)
    append_lines_to_report(report_file_path,
                           f'FRR: {frr}%')
    append_lines_to_report(report_file_path,
                           f'FAR: {far}%')

    append_lines_to_report(report_file_path,
                           '\nclassification report:')
    append_lines_to_report(report_file_path,
                           f'{classification_report(1 - all_true_labels, 1 - all_predicted_labels)}')


def generate_report(predictions,
                    labels,
                    histogram_bin_size,
                    report_file_path,
                    genuine_distribution_file_path,
                    forgery_distribution_file_path,
                    mixed_distributions_file_path):
    labels_mask = labels.ravel() > 0.5
    genuine_probabilities = predictions[labels_mask] * 100
    forgery_probabilities = predictions[~labels_mask] * 100
    genuines_count = np.count_nonzero(labels_mask)
    forgeries_count = np.count_nonzero(~labels_mask)

    histogram_bins = list(range(0, 101, histogram_bin_size))

    false_acceptance_rate_per_threshold = calculate_false_acceptance_rate_per_threshold(forgeries_count,
                                                                                        histogram_bins,
                                                                                        forgery_probabilities)

    false_rejection_rate_per_threshold = calculate_false_rejection_rate_per_threshold(genuines_count,
                                                                                      histogram_bins,
                                                                                      genuine_probabilities)

    write_classification_report(report_file_path,
                                np.array(labels_mask, np.int32),
                                np.array(predictions.ravel() > 0.5, np.int32))

    write_distributions_table(report_file_path,
                              false_acceptance_rate_per_threshold,
                              false_rejection_rate_per_threshold,
                              histogram_bin_size)
    append_lines_to_report(report_file_path, '')

    write_expected_false_acceptances_table(report_file_path,
                                           false_acceptance_rate_per_threshold,
                                           histogram_bin_size)
    append_lines_to_report(report_file_path, '')

    write_expected_false_rejections_table(report_file_path,
                                          false_rejection_rate_per_threshold,
                                          histogram_bin_size)

    plot_distributions(genuine_distribution_file_path,
                       forgery_distribution_file_path,
                       mixed_distributions_file_path,
                       genuine_probabilities,
                       forgery_probabilities,
                       histogram_bins)


class BatchVerification:
    def __init__(self, verification_lambda, batch_size=512):
        self.batch_size = batch_size
        self.verification_lambda = verification_lambda

        self.input_batch1 = []
        self.input_batch2 = []
        self.count = 0
        self.predictions = []
        self.classes = []
        self.signature_ids = []

    def __predict(self):
        self.predictions.extend(self.verification_lambda(self.input_batch1, self.input_batch2))
        self.input_batch1 = []
        self.input_batch2 = []
        self.count = 0

    def feed(self, signature_id, other_class, item1, item2):
        self.input_batch1.append(item1)
        self.input_batch2.append(item2)
        self.signature_ids.append(signature_id)
        self.classes.append(other_class)
        self.count += 1

        if self.count == self.batch_size:
            self.__predict()

    def finalize(self):
        self.__predict()

    def get_predictions(self):
        predictions_dict = dict()
        for i in range(len(self.signature_ids)):
            signature_id = self.signature_ids[i]
            if signature_id not in predictions_dict:
                predictions_dict[signature_id] = []
            predictions_dict[signature_id].append((self.classes[i], self.predictions[i]))
        return predictions_dict


def simulate_verification(feature_vector_classes,
                          verification_lambda,
                          signatories=10,
                          max_signatures_per_signatory=5,
                          number_of_signatures_to_verify=1,
                          number_of_verifications=1000,
                          verification_threshold=0.7):
    fa_count = 0
    fr_count = 0
    coverage = 0

    for i in range(number_of_verifications):
        keys = [*feature_vector_classes]
        chosen_classes = random.sample(keys, signatories)

        chosen_signatures_to_verify_classes = random.sample(chosen_classes, number_of_signatures_to_verify)
        chosen_signatures = []
        for j in range(number_of_signatures_to_verify):
            chosen_signature_to_verify_class = chosen_signatures_to_verify_classes[j]
            n = len(feature_vector_classes[chosen_signature_to_verify_class])
            chosen_signatures.append([chosen_signature_to_verify_class,
                                      random.randint(0, n - 1)])

        batch_verification = BatchVerification(verification_lambda)
        for signature_class, signature_index in chosen_signatures:
            feature_vector = feature_vector_classes[signature_class][signature_index]
            for c in chosen_classes:
                n = len(feature_vector_classes[c])
                feature_vectors_indices = list(range(n))
                chosen_feature_vectors_indices = random.sample(feature_vectors_indices,
                                                               min(max_signatures_per_signatory + 1, n))
                if c == signature_class and signature_index in chosen_feature_vectors_indices:
                    chosen_feature_vectors_indices.remove(signature_index)
                if len(chosen_feature_vectors_indices) > max_signatures_per_signatory:
                    chosen_feature_vectors_indices.pop(0)

                feature_vectors = feature_vector_classes[c]
                for j in feature_vectors_indices:
                    batch_verification.feed((signature_class, signature_index),
                                            c,
                                            feature_vector,
                                            feature_vectors[j])
        batch_verification.finalize()

        correct_verifications = 0
        predictions_dict = batch_verification.get_predictions()
        for signature_id in predictions_dict:
            signature_verifications = predictions_dict[signature_id]
            highest_probability = 0.0
            highest_probability_class = None
            for c, probability in signature_verifications:
                if probability > highest_probability:
                    highest_probability = probability
                    highest_probability_class = c

            if highest_probability >= verification_threshold:
                if highest_probability_class != signature_id[0]:
                    fa_count += 1
                else:
                    correct_verifications += 1
            else:
                fr_count += 1

        if correct_verifications == number_of_signatures_to_verify:
            coverage += 1

    coverage /= number_of_verifications
    return coverage, fa_count, fr_count


def generate_verification_simulation_report(feature_vector_classes,
                                            verification_lambda,
                                            report_file_path):
    verification_threshold = 70
    number_of_verifications = 1000
    max_signatures_per_signatory = 5

    append_lines_to_report(report_file_path, '\n[verification simulation]')
    append_lines_to_report(report_file_path, f'threshold: {verification_threshold}%')
    append_lines_to_report(report_file_path, f'number of simulations per signature: {number_of_verifications}')
    append_lines_to_report(report_file_path,
                           'note that each cell in the following table contains a tuple (coverage %, false acceptance count, false rejection count)')

    append_lines_to_report(report_file_path, '')

    table = [['Number of Signatories',
              'One Signature per Verification',
              'Two Signatures per Verification',
              'Three Signatures per Verification']]

    signatories_per_verification_list = [10, 20, 30, 40, 50]
    signatures_per_verification_list = [1, 2, 3]
    progress_bar = tqdm(total=len(signatories_per_verification_list) * len(signatures_per_verification_list))
    for signatories_per_verification in signatories_per_verification_list:
        row = [f'{signatories_per_verification}']
        for signatures_per_verification in signatures_per_verification_list:
            coverage, fa_count, fr_count = simulate_verification(feature_vector_classes,
                                                                 verification_lambda,
                                                                 signatories_per_verification,
                                                                 max_signatures_per_signatory,
                                                                 signatures_per_verification,
                                                                 number_of_verifications,
                                                                 verification_threshold / 100)
            row.append(f'({round(coverage * 100, 2)}%, {fa_count}, {fr_count})')
            progress_bar.update()
        table.append(row)

    table = AsciiTable(table).table
    print(table)
    append_lines_to_report(report_file_path, table)
