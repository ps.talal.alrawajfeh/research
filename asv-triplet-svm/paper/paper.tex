\documentclass{article}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{hyperref}
\usepackage{url}
\usepackage{booktabs}
\usepackage{amsfonts}
\usepackage{nicefrac}
\usepackage{microtype}
\usepackage{graphicx}
\usepackage{doi}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{tikz-cd}
\usepackage{tikz}
\usepackage{caption}
\usepackage{float}

\graphicspath{ {./figures/} }

\begin{document}
    \title{A Two-Model Approach to Writer-Independent Offline Signature Verification}
    \author{Apollo Team\\ \footnotesize{ProgressSoft Corporation}\\ \footnotesize{\textit{apollo@progressoft.com}}}

    \maketitle


    \section{Proposed Method}\label{sec:proposed-method}

    \subsection{Description}\label{subsec:description}
    We propose a solution to the problem of writer-independent offline signature verification using two deep learning models such that each model is independent of the other and solves the problem from a different perspective.
    A combination of the two models results in higher accuracy than the accuracy of each model separately.
    One of the difficulties in the problem of signature verification is producing a model with a false acceptance rate (FAR) as low as possible while maintaining an acceptable rate of false rejection (FRR).
    This balance is affected by many factors including the dataset, the models, the training process, the combination of the two models, and the user-selected verification threshold.
    Note that the FAR is also known as False Positive Rate (FPR) and the FRR is also known as False Negative Rate (FNR).

    \subsection{Dataset}\label{subsec:dataset}
    We maintain a dataset of around 27,000 grayscale signature images with about 9300 signature classes (signatories) each of which has approximately 3 signatures on average with the majority of the classes (about 8300) containing only 2 signatures.
    This low number of signatures per class is a problem of its own and to be able to reach a reasonable performance level with the models, we have decided to use a pre-trained model, namely DenseNet201\cite{densenet}, and apply several regularization techniques (e.g.\ data augmentation) to reduce overfitting.
    However, the large number of possible choices of negative pairs of signatures in the dataset compared to the low number of possible choices of positive pairs of signatures causes the models to be biased towards rejection in the verification process.
    The possible number of choices of positive pairs, while allowing choosing the same signature twice, is approximately:
    \[
        9300 \times 2^3  = 74,400
    \]
    If choosing the same signature twice is not allowed, then the number of possible pairs is approximately:
    \[
        9300 \times {3 \choose 2} = 9300 \times 3 = 27,900
    \]
    On the other hand, the number of possible negative pairs is approximately:
    \begin{align*}
        \sum\limits_{i = 1}^{9300} 3 \times (9300 - i) \times 3 &= \sum\limits_{i = 1}^{9300} 9 \times 9300 - 9 \times i \\
        &=  9300 \times 9 \times 9300 - 9 \sum\limits_{i = 1}^{9300} i \\
        &= 778,410,000 - 9 \times \frac{9300 \times 9301}{2} \\
        &= 778,410,000 - 389,246,850 \\
        &= 389,163,150
    \end{align*}

    Therefore, the ratio of the number of possible positive pairs to the number of possible negative pairs is:
    \[
        \frac{74,400}{389,163,150} \approx 0.02\%
    \]
    which is extremely low.
    To remedy this imbalance, we apply several training techniques which we will discuss later.

    \subsection{Data Augmentation}\label{subsec:data-augmentation}
    We produce both positive and negative (special) augmentations that will help reduce the imbalance in the dataset and reduce overfitting in the training process.
    To produce positive augmentations, which are augmentations of the signature images in the same class to enrich it, we apply several transformations in the following order:
    \begin{enumerate}
        \item Crop the white border of the image (if it exists) to remove the white rows and columns surrounding the signature;
        however, since there might be noise in the image, we apply OTSU threshold to obtain a binary image but the cropping is done on the original image such that the binary image is only used to find the rectangle that contains the signature without the white border
        \item With a 50\% probability, the resolution of the image is reduced by downscaling the image to a random percentage of its original size using Bi-Cubic Interpolation.
        This step is either done randomly at the beginning with a 50\% chance if the image's height and width were both above a minimum value to avoid damaging the signature, or right before the last step (which adds speckles to the image)
        \item With a 50\% probability, the aspect ratio of the image is distorted by decreasing the width or the height of the image or both within a random percentage of the original width and height
        \item With a 50\% probability, the quality of the image is reduced by applying JPEG compression with a random quality value
        \item With a 50\% probability, the image is rotated clockwise or counter-clockwise by a random angle
        \item If the image is large enough, a dilation is performed with a 50\% chance
        \item Finally, with a 50\% probability, random speckles are added to the image which are either black speckles or white speckles or both
    \end{enumerate}

    A visual inspection of the augmented images was performed and the values/ranges were tuned to give the required variability but without damaging the signatures to a degree that they become unrecognizable or introduce error in the training process later on.
    For each signature image, we generate a number of random positive augmentations that will make each signature class contain approximately $n \times 3 + 3$ signatures on average where $n$ is the number of augmentations.

    A negative (special) class is produced for each signature class to make the models reject certain types of inputs in the verification process.
    The images produced in this class are:
    \begin{enumerate}
        \item A random sample of signatures from the signature class with each signature having a connected part(s) of it removed (either vertically or horizontally) such that a random percentage of black pixels are removed
        \item Randomly generated images of random sizes with speckles, rectangles, circles, and lines
        \item Random pairs of signatures from the same class concatenated next to each other with random spacing between them
        \item Random pairs of signatures with one signature within the class and one from another class concatenated next to each other with random spacing between them
    \end{enumerate}

    \subsection{Generating Embeddings}\label{subsec:generating-embeddings}
    As discussed, we use a pre-trained model to help reduce overfitting.
    DenseNet201\cite{densenet} trained on ImageNet with the last fully connected layer removed is used with the other two models.
    The last layer in the resulting model is the embedding layer which has an output dimension of 1920.
    Before feeding the images to the pre-trained model, we pre-process each image according to the following steps:
    \begin{enumerate}
        \item Crop the white border of the image (if it exists) to remove the white rows and columns surrounding the signature
        \item Resize the image using Bi-Cubic Interpolation to the required input size of the pre-trained model which is $224 \times 224$
        \item Apply OTSU threshold to convert the image to binary
        \item Invert the colors of the image so that the background is black (0) and the foreground is white (255)
        \item Since the image has only one channel (grayscale) and DenseNet201 has an input shape of $(224, 224, 3)$, we replicate the channel to obtain the desired shape
        \item Apply the DenseNet201 specific pre-processing function
    \end{enumerate}

    Finally, we generate and store the embeddings (feature vectors) of the signature images by feeding them through the pre-trained model.

    Although using a pre-trained model like DenseNet201 reduces overfitting, there are also some downsides, to mention a few:
    \begin{enumerate}
        \item Most pre-trained models are trained with certain data augmentation techniques such as randomly cropping the image, rotating it 180 degrees, and flipping it horizontally and vertically which in the case of signature verification are sometimes invalid
        \item Re-training or even fine-tuning pre-trained models such as DenseNet201 could be very costly since they are relatively large and require high hardware (GPU) specs
        \item Some important features relevant to signature verification may not have been learned in the convolutional filters, and it may be using many irrelevant features for recognition which makes it less robust for our task
        \item Pre-trained models on ImageNet are usually trained on an input with 3 channels, and it utilizes this color information for recognition while in our case we only work with grayscale images
    \end{enumerate}

    \subsection{Siamese Model Architecture}\label{subsec:siamese-model-architecture}
    The first verification model is a Siamese Model (see\ \cite{siamese2014}).
    Our siamese model takes two embeddings (a pair) as an input and the output is a probability distribution of two possible outcomes, i.e. whether the two embeddings are from the same signature class or from different signature classes.
    Most siamese models apply a connection function to the two embeddings ($x$ and $y$), to mention some:
    \begin{align}
        \label{eqn:absolute-diff}
        c_{abs}(x, y) &= \left(\lvert x_1 - y_1 \rvert, \lvert x_2 - y_2 \rvert, \dots, \lvert x_n - y_n \rvert \right) \\
        c_{euc}(x, y) &= \left((x_1 - y_1)^2, (x_2 - y_2)^2, \dots, (x_n - y_n)^2 \right) \\
        c_{cos}(x, y) &= \frac{\left(x_1 y_1, x_2 y_2, \dots, x_n y_n \right)}{\| x \| \| y \|}
    \end{align}
    where $x = (x_1, x_2, \dots, x_n)$ and $y = (y_1, y_2, \dots, y_n)$.
    After testing many different connection functions, we chose to go with~\ref{eqn:absolute-diff}.
    The connection function~\ref{eqn:absolute-diff} is also known as a dichotomy transform which converts a multi-class problem to a two class problem (see\ \cite{sabourin2020} and \ \cite{dichotomizer2000}).
    For example, consider the following figure containing three classes of embeddings:
    \begin{figure}[H]
        \begin{center}
            \includegraphics[scale=0.5]{figure1}
            \caption{Embeddings}
            \label{fig:embeddings}
        \end{center}
    \end{figure}

    After applying the dichotomy transform on every possible pair of embeddings, we obtain only two classes, one class representing all the pairs that were from the same class (the distance between the two points is relatively small) and another class representing all the pairs that contain points from different classes (the distance between the two points is relatively large) as depicted in figure~\ref{fig:transformed-classes} which is exactly what we need for writer-independent signature verification.
    \begin{figure}[H]
        \begin{center}
            \includegraphics[scale=0.5]{figure2}
            \caption{Result of applying the dichotomy transform}
            \label{fig:transformed-classes}
        \end{center}
    \end{figure}

    The architecture of the siamese starts with an embedding model that reduces the dimensionality of each input to 1024.
    The connection function~\ref{eqn:absolute-diff} is applied to the embeddings, followed by a fully connected layer with an output dimension of 1024.
    A function similar to a Gaussian RBF function is learned that transforms the 1024 output to a single output between 0 and 1:
    \begin{equation}
        \phi(x;\gamma) = e^{-\gamma \| x \|}\label{eq:rbf}
    \end{equation}
    where $\gamma$ is a non-negative parameter learned during training and $\| . \|$ is the $l_2$ norm which is also known as the euclidean norm.
    The value of $\phi(x)$ approaches 1 as the two embeddings get closer to each other in terms of distance, and approaches 0 as the two embeddings get further away from each other as depicted in figure~\ref{fig:transformed-classes-rbf}.
    \begin{figure}[H]
        \begin{center}
            \includegraphics[scale=0.5]{figure3}
            \caption{Result of applying $\phi(x)$ with $\gamma = 1$ to the transformed classes}
            \label{fig:transformed-classes-rbf}
        \end{center}
    \end{figure}

    The final output is of the form $(S(a\phi(x) + b), 1 - S(a\phi(x) + b))$ where $S(x)$ is the sigmoid function (and both $a$ and $b$ are learned parameters) so that we can apply a two-class binary cross entropy loss function to train the model.

    \begin{center}
        \begin{tabular}{ |l|l| }
            \hline
            \textbf{Layer}      & \textbf{Parameters}                    \\
            \hline
            Input               & size = 1920                            \\
            Batch Normalization & momentum = 0.99, epsilon = 1e-3        \\
            Gaussian Noise      & stddev = 1e-7                          \\
            \hline
            Fully Connected     & output\_size = 1024, bias = 0          \\
            Batch Normalization & momentum = 0.99, epsilon = 1e-3        \\
            ReLU                & negative\_slope = 0.05, max\_value = 6 \\
            Dropout             & rate = 0.5                             \\
            \hline
            Fully Connected     & output\_size = 1024, bias = 0          \\
            Batch Normalization & momentum = 0.99, epsilon = 1e-3        \\
            ReLU                & negative\_slope = 0.05, max\_value = 6 \\
            \hline
        \end{tabular}
        \captionof{table}{\textbf{\footnotesize{Siamese Embedding Network}}}\label{table:siamese-embedding-network}
    \end{center}

    Table~\ref{table:siamese-embedding-network} contains the layers of the embedding model.
    Dropout and Gaussian Noise are used to reduce overfitting and increase model robustness.
    Batch Normalization is applied to the input and before each activation function to provide further regularization and to speed up training.
    Finally, a variant of ReLU with a negative slope (also known as Leaky ReLU) and a maximum value is used to avoid both dying ReLU (gradient vanishing) and gradient explosion.

    \begin{center}
        \begin{tabular}{ |l|l| }
            \hline
            \textbf{Layer}      & \textbf{Parameters}                              \\
            \hline
            Input               & two vectors of size = 1024                       \\
            Lambda              & apply $c_{abs}(x, y)$ as the connection function \\
            Batch Normalization & momentum = 0.99, epsilon = 1e-3                  \\
            Dropout             & rate = 0.5                                       \\
            \hline
            $\phi(x)$           & $\gamma$ has a non-negative constraint           \\
            Fully Connected     & output\_size = 1                                 \\
            Sigmoid             & none                                             \\
            Lambda              & $(x, 1 - x)$                                     \\
            \hline
        \end{tabular}
        \captionof{table}{\textbf{\footnotesize{Siamese Network}}}\label{table:siamese-network}
    \end{center}

    Table~\ref{table:siamese-network} contains the layers that are responsible for the verification part of the network.
    The fully connected layer after $\phi(x)$ represents the learned function $ax + b$ where $b$ is the bias and $a$ is the only weight since the dimension of the output of $\phi(x)$ is 1 and the output dimension of the fully connected layer is also 1.
    Although the siamese network has two outputs, we only use the first output at inference time.
    The problem with the siamese network is that most of its outputs are saturated between 0 and 1;
    hence, we are mainly concerned with the values near 1, most of which are between 0.95 and 1.
    To make it easier to apply thresholds on predictions, the values are mapped using a piecewise linear transform, such that for $0 < p_1 < p_2 < 1$, the probabilities in the intervals $\left[ 0, p_1 \right]$, $\left( p_1, p_2 \right]$, $\left( p_2, 1 \right]$ are mapped to the intervals $\left[ 0, 50 \right]$, $\left( 50, 75 \right]$, $\left( 75, 100 \right]$ respectively.
    \begin{equation}
        \label{siamese-mapping}
        f(x; p_1, p_2) = \begin{cases}
                             \frac{50 - 0}{p_1 - 0}(x - 0) + 0 & x \in \left[ 0, p_1 \right] \\
                             \frac{75 - 50}{p_2 - p_1}(x - p_1) + 50 & x \in \left( p_1, p_2 \right] \\
                             \frac{100 - 75}{1 - p_2}(x - p_2) + 75 & x \in \left( p_2, 1 \right]
        \end{cases}
    \end{equation}

    \subsection{Triplet Model Architecture}\label{subsec:triplet-model-architecture}
    The second verification model is an embedding model trained using Triplet Loss (see\ \cite{triplet2015}).
    Our triplet model takes a single embedding from DenseNet201 as an input and the output is another embedding of dimension 512, and thus the dimensionality of the input is reduced by about 26.7\%.
    The architecture of the Triplet Network is very simple, look at the following table~\ref{table:triplet-network}.
    \begin{center}
        \begin{tabular}{ |l|l| }
            \hline
            \textbf{Layer}      & \textbf{Parameters}                                         \\
            \hline
            Input               & size = 1920                                                 \\
            Batch Normalization & momentum = 0.99, epsilon = 1e-3                             \\
            Gaussian Noise      & stddev = 1e-7                                               \\
            \hline
            Fully Connected     & output\_size = 1024, bias = 0                               \\
            Batch Normalization & momentum = 0.99, epsilon = 1e-3                             \\
            ReLU                & negative\_slope = 0.05, max\_value = 6                      \\
            \hline
            Skip Connection     & concatenates the input and the output of the previous layer \\
            Batch Normalization & momentum = 0.99, epsilon = 1e-3                             \\
            Dropout             & rate = 0.5                                                  \\
            \hline
            Fully Connected     & output\_size = 1024, bias = 0                               \\
            Batch Normalization & momentum = 0.99, epsilon = 1e-3                             \\
            ReLU                & negative\_slope = 0.05, max\_value = 6                      \\
            Dropout             & rate = 0.5                                                  \\
            \hline
            Fully Connected     & output\_size = 512, bias = 0                                \\
            Tanh                & none                                                        \\
            Lambda              & apply $l_2$ normalization                                   \\
            \hline
        \end{tabular}
        \captionof{table}{\textbf{\footnotesize{Triplet Network}}}\label{table:triplet-network}
    \end{center}

    The triplet model starts similarly as the siamese network.
    After the first fully connected layer, a skip connection is applied to tunnel information that may be lost due to the reduction of dimensionality (from 1920 to 1024) to the next layer.
    The last fully connected layer has a different activation function, i.e. $\tanh(x)$, which is used because it is a bounded function (its output is in the interval $\left( -1, 1 \right)$).
    Finally, the embedding is $l_2$-normalized by applying $\frac{x}{\| x \|}$.
    Since the output of the $\tanh(x)$ function is bounded, then the embedding space which is a unit hypersphere is learned easier and faster.

    To make a decision to reject or accept a verification using the triplet network, the embeddings of the signatures from DenseNet201 are fed to the triplet network, and then the euclidean distance between these embeddings are calculated,
    \[
        d(x, y) = \sqrt{\sum_{i = 1}^{n} (x_i - y_i)^2}
    \]
    and the decision is made by applying a threshold on the distance.
    If the two embeddings are close enough, i.e. the distance between them are below the threshold, then they are considered to be of the same signature class;
    otherwise, the verification is rejected.
    Since all the embeddings are $l_2$ normalized, and they exist on a unit hypersphere, the maximum distance between any two embeddings is 2.
    This fact, could be utilized to convert the distances into similarities which are easier to deal with from a user perspective.
    There are many transformations that convert distances into similarities, to mention a few:
    \begin{align}
        s_1(x, y) &= 2 - d(x, y) \\
        s_2(x, y) &= 1 - \frac{d(x, y)}{2} \\
        s_3(x, y) &= \frac{1}{1 + \alpha d(x, y)} \\
        s_4(x, y) &= e^{-\alpha d(x, y)}
    \end{align}
    where $\alpha > 0$ is a parameter.
    We chose a piecewise linear transformation $s(x, y; d_1, d_2, d_3)$ as a similarity transform, such that for $0 < d_1 < d_2 < d_3 < 2$, the distances in the intervals $\left[ 0, d_1 \right]$, $\left( d_1, d_2 \right]$, $\left( d_2, d_3 \right]$, $\left(d_3 , 2 \right]$ are mapped to the intervals $\left[ 90, 100 \right]$, $\left( 50, 90 \right]$, $\left( 10, 50 \right]$, $\left( 0, 10 \right]$ respectively.
    \begin{equation}
        \label{triplet-mapping}
        s(x, y; d_1, d_2, d_3) = \begin{cases}
                                     \frac{90 - 100}{d_1 - 0}(x - 0) + 100 & x \in \left[ 0, d_1 \right] \\
                                     \frac{50 - 90}{d_2 - d_1}(x - d_1) + 90 & x \in \left( d_1, d_2 \right] \\
                                     \frac{10 - 50}{d_3 - d_2}(x - d_2) + 50 & x \in \left( d_2, d_3 \right] \\
                                     \frac{0 - 10}{2 - d_3}(x - 0) + 10 & x \in \left( d_3, 2 \right]
        \end{cases}
    \end{equation}
    This gives us a better control on the mapped similarities.

    \subsection{Siamese Training}\label{subsec:siamese-training}
    The siamese network is trained twice.
    The first time, it is trained on 70\% of the signature classes, which is around 6500 classes, to measure its generalization ability on the remaining 30\%.
    Half of the remaining classes (15\%) are used during model validation (at the end of each epoch) and selection where after the training process is completed, the model with the highest validation accuracy or least validation loss is selected.
    The other half of the remaining classes are used for testing, i.e. to confirm the validation accuracy and loss.
    After the first time the model is trained, it is trained again but on all the signature classes.
    This is to make use of the entire dataset and to see if the model is able to learn the dataset.

    Since the siamese network takes two feature vectors as the input, it is trained on positive and negative pairs of feature vectors.
    For each signature class, $r$ reference signatures are chosen randomly and for each reference signature, all the possible positive pairs containing that reference are generated.
    Also, for each reference, negative pairs are chosen by choosing $n$ different signature classes than the signature class containing the reference, and then $k$ signatures are chosen randomly from each one of these classes.
    After obtaining positive and negative pairs for a signature class, it is excluded and not used again even when forming negative pairs for other classes.
    This is done to avoid having negative pairs repeated multiple times and helps control the ratio of distinct positive and negative pairs better.

    If each class had $x$ signatures on average, then the number of possible choices of positive pairs is $C \times r \times x$, where $C$ is the number of all the signature classes.
    On the other hand, the number of possible choices of negative pairs is $(C - n) \times r \times n \times k$.
    Thus, the ratio of positive pairs to negative pairs is given by:
    \[
        \frac{C \times r \times x}{(C - n) \times r \times n \times k} = \frac{C}{C - n} \times \frac{x}{n \times k}
    \]
    When $C$ is large, $\frac{C}{C - n}$ is approximately 1, so the most influential factor of this ratio is
    \[
        \frac{x}{n \times k}
    \]

    This ratio affects the bias of the network in the verification process to either accept or reject the verification.
    After experimenting with different ratios, we chose a ratio of $1:4$.

    Regarding the special (negative) augmentations, for each reference signature, we generate all the possible pairs containing the reference signature and a special augmentation.
    Positive pairs containing the same reference signature are repeated until the number of positive pairs is equal to the number of the special negative pairs containing the same reference signature.
    This affects the above ratio such that if each class has $y$ special augmentations, then the ratio becomes:
    \[
        \frac{x + y}{n \times k + y}
    \]
    We have a ratio of approximately $1:2.5$, and if we only count unique positive pairs it becomes approximately $1:5$.
    This makes the model biased towards rejection, and thus lowers the FAR while not having a very large imbalance between positive and negative pairs which doesn't make the FRR too high as we discussed in section~\ref{subsec:dataset}.

    Positive pairs are labeled with $(1, 0)$ and the negative pairs are labeled with $(0, 1)$.
    The model is trained using binary cross-entropy loss and Adam optimizer with a learning rate of 0.001.

    The following table~\ref{table:siamese-far-frr}, shows the FAR and FRR of the model trained on 70\% of the signature classes on each threshold.
    \begin{center}
        \begin{tabular}{ |l|l|l| }
            \hline
            \textbf{Threshold} & \textbf{FAR} & \textbf{FRR} \\
            \hline
            0\%                & 100.0\%      & 0.0\%        \\
            5\%                & 0.72\%       & 1.79\%       \\
            10\%               & 0.61\%       & 2.12\%       \\
            15\%               & 0.54\%       & 2.37\%       \\
            20\%               & 0.49\%       & 2.57\%       \\
            25\%               & 0.46\%       & 2.75\%       \\
            30\%               & 0.43\%       & 2.92\%       \\
            35\%               & 0.4\%        & 3.09\%       \\
            40\%               & 0.38\%       & 3.25\%       \\
            45\%               & 0.36\%       & 3.41\%       \\
            50\%               & 0.34\%       & 3.6\%        \\
            55\%               & 0.32\%       & 3.78\%       \\
            60\%               & 0.31\%       & 3.99\%       \\
            65\%               & 0.29\%       & 4.21\%       \\
            70\%               & 0.27\%       & 4.46\%       \\
            75\%               & 0.25\%       & 4.73\%       \\
            80\%               & 0.22\%       & 5.08\%       \\
            85\%               & 0.2\%        & 5.49\%       \\
            90\%               & 0.17\%       & 6.16\%       \\
            95\%               & 0.13\%       & 7.59\%       \\
            100\%              & 0.0\%        & 100.0\%      \\
            \hline
        \end{tabular}
        \captionof{table}{\textbf{\footnotesize{Siamese Model FAR and FRR}}}\label{table:siamese-far-frr}
    \end{center}

    \subsection{Triplet Training}\label{subsec:triplet-training}
    Similarly, the triplet network is trained two times as with the siamese model.
    However, the process of training the triplet network is very different.
    One way to train the triplet network is to generate triplets which are tuples containing three items.
    The first item in a triplet is called the anchor which is chosen from any class.
    The second item in a triplet is from the same class of the anchor which is called the positive of the triplet.
    The third item in a triplet is from a different class which is called the negative of the triplet.
    A triplet network is trained such that the distance between the anchor and positive embeddings is less than the distance between the anchor and negative embeddings by a margin $\alpha$ which is a hyper-parameter.
    If a triplet is denoted by $(a, p, n)$, then its triplet loss is calculated as:
    \[
        l(a, p, n) = \max(d(a, p) - d(a, n) + \alpha, 0)
    \]

    The problem with triplet loss is that most triplets are easy to learn, and after a period of time during training, the loss starts to stagnate because each mini-batch contains too few ``hard" triplets.
    A hard triplet is a triplet where $d(a, n) \leq d(a, p)$ and a semi-hard triplet is a triplet where $d(a, n) \leq d(a, p) + \alpha$ but $d(a, n) > d(a, p)$.

    Instead of choosing random triplets before training, we apply Online Batch Hard Triplet Mining (see\ \cite{triplet2017}) such that each mini-batch contains $b_c$ signature classes chosen randomly during training where $b_x$ signatures are chosen randomly from each one of these classes.
    Therefore, each mini-batch contains $b_c \times b_x$ signatures. The Batch Hard Triplet Loss is calculated by choosing for each possible anchor in the mini-batch, the ``hardest" positive and ``hardest" negative and then calculating their triplet loss.
    The ``hardest" positive is the positive with the largest distance from the anchor and the ``hardest" negative is the negative with the least distance from the anchor.
    The final loss is the average of all triplet losses calculated for each possible anchor with its hardest positive and its hardest negative in the mini-batch.

    The embedding model is trained using Adam Optimizer with a learning rate of 0.001 and a margin $\alpha = 0.5$.
    After training is done, random pairs are generated to calculate a histogram of distances to determine the distance $d_{50}$ where the number of positive distances is equal to the number of negative distances.
    This value is used to map the distances to similarities using a piecewise linear function
    \[
        s(x, y) = \begin{cases}
                      \frac{50 - 100}{d_{50} - 0}(x - 0) + 100 & x \in \left[ 0, d_{50} \right] \\
                      \frac{0 - 50}{2 - d_{50}}(x - d_{50}) + 50 & x \in \left[ d_{50}, 2 \right]
        \end{cases}
    \]

    The following table~\ref{table:triplet-far-frr}, shows the FAR and FRR of the model trained on 70\% of the signature classes on each threshold where the distances are mapped using the function defined above with $d_{50}=1.0$.
    \begin{center}
        \begin{tabular}{ |l|l|l| }
            \hline
            \textbf{Threshold} & \textbf{FAR} & \textbf{FRR} \\
            \hline
            0\%                & 100.0\%      & 0.0\%        \\
            5\%                & 100.0\%      & 0.0\%        \\
            10\%               & 99.97\%      & 0.0\%        \\
            15\%               & 98.9\%       & 0.0\%        \\
            20\%               & 90.48\%      & 0.0\%        \\
            25\%               & 68.72\%      & 0.0\%        \\
            30\%               & 41.38\%      & 0.0\%        \\
            35\%               & 20.36\%      & 0.03\%       \\
            40\%               & 8.59\%       & 0.1\%        \\
            45\%               & 3.21\%       & 0.35\%       \\
            50\%               & 1.07\%       & 1.24\%       \\
            55\%               & 0.35\%       & 3.41\%       \\
            60\%               & 0.11\%       & 8.44\%       \\
            65\%               & 0.03\%       & 17.87\%      \\
            70\%               & 0.01\%       & 32.28\%      \\
            75\%               & 0.0\%        & 49.64\%      \\
            80\%               & 0.0\%        & 65.68\%      \\
            85\%               & 0.0\%        & 79.56\%      \\
            90\%               & 0.0\%        & 89.29\%      \\
            95\%               & 0.0\%        & 92.88\%      \\
            100\%              & 0.0\%        & 100.0\%      \\
            \hline
        \end{tabular}
        \captionof{table}{\textbf{\footnotesize{Triplet Model FAR and FRR}}}\label{table:triplet-far-frr}
    \end{center}

    \subsection{Combination of Siamese and Triplet}\label{subsec:combination-siamese-triplet}
    The combination of the siamese and triplet model is done by averaging both the mapped siamese and mapped triplet output values.
    As we discussed in sections ~\ref{subsec:siamese-model-architecture} and ~\ref{subsec:triplet-model-architecture}, we use the functions ~\ref{siamese-mapping} and ~\ref{triplet-mapping} for mapping the siamese and triplet output values respectively.
    After expirementation and visual inspection, we chose $p_1 = 0.7$ and $p_2 = 0.95$ for function~\ref{siamese-mapping} and $d_1 = 0.6$, $d_2 = 0.9$, and $d_3 = 1.3$ for function~\ref{triplet-mapping}.
    If the mapped output of the siamese is $x$ and the mapped output of the triplet is $y$, then the final combination is calculated as:
    \begin{equation}
        \label{combination}
        \text{comb}(x, y) = \begin{cases}
                                0 & x < 35 \\
                                0 & x + y < 70 \\
                                \frac{x + y}{2} & \text{otherwise}
        \end{cases}
    \end{equation}

    The following table~\ref{table:combination-far-frr}, shows the FAR and FRR of the combination~\ref{combination} of the siamese and triplet models that are trained on 70\% of the signature classes on each threshold.
    \begin{center}
        \begin{tabular}{ |l|l|l| }
            \hline
            \textbf{Threshold} & \textbf{FAR} & \textbf{FRR} \\
            \hline
            0\%                & 100.0\%      & 0.0\%        \\
            5\%                & 0.27\%       & 2.91\%       \\
            10\%               & 0.27\%       & 2.91\%       \\
            15\%               & 0.27\%       & 2.91\%       \\
            20\%               & 0.27\%       & 2.91\%       \\
            25\%               & 0.27\%       & 2.91\%       \\
            30\%               & 0.27\%       & 2.91\%       \\
            35\%               & 0.27\%       & 2.91\%       \\
            40\%               & 0.23\%       & 3.26\%       \\
            45\%               & 0.21\%       & 3.57\%       \\
            50\%               & 0.18\%       & 3.94\%       \\
            55\%               & 0.16\%       & 4.35\%       \\
            60\%               & 0.12\%       & 4.98\%       \\
            65\%               & 0.09\%       & 5.86\%       \\
            70\%               & 0.07\%       & 7.05\%       \\
            75\%               & 0.05\%       & 8.81\%       \\
            80\%               & 0.04\%       & 11.41\%      \\
            85\%               & 0.02\%       & 15.89\%      \\
            90\%               & 0.01\%       & 22.99\%      \\
            95\%               & 0.0\%        & 36.57\%      \\
            100\%              & 0.0\%        & 100.0\%      \\
            \hline
        \end{tabular}
        \captionof{table}{\textbf{\footnotesize{Combination Model FAR and FRR}}}\label{table:combination-far-frr}
    \end{center}

    \begin{thebibliography}{1}
        \bibitem{sabourin2020}
        Souza V.L.F., Oliveira A.L.I., Cruz R.M.O., Sabourin R. (2020).
        \newblock \textit{A white-box analysis on the writer-independent dichotomy transformation applied to offline handwritten signature verification}
        \newblock Expert Systems with Applications, 113397.
        \newblock \url{doi:10.1016/j.eswa.2020.113397}

        \bibitem{triplet2017}
        Hermans A., Beyer L., and Leibe B. (2017).
        \newblock \textit{In Defense of the Triplet Loss for Person Re-Identification.}
        \newblock \url{arXiv:1703.07737v4}

        \bibitem{densenet}
        Huang G., Liu Z., Maaten L. V. D., and Weinberger, K. Q. (2017).
        \newblock \textit{Densely Connected Convolutional Networks.}
        \newblock 2017 IEEE Conference on Computer Vision and Pattern Recognition (CVPR), 2261–2269.
        \newblock IEEE Computer Society.
        \newblock \url{doi:10.1109/CVPR.2017.243}

        \bibitem{triplet2015}
        Hofer E. and Ailon, N. (2015)
        \newblock \textit{Deep Metric Learning Using Triplet Network.}
        \newblock Similarity-Based Pattern Recognition, 84--92.
        \newblock Springer International Publishing.
        \newblock \url{doi:10.1007/978-3-319-24261-3_7}

        \bibitem{siamese2014}
        Yi D., Lei Z., Liao S., and Li S.Z. (2014).
        \newblock \textit{Deep Metric Learning for Person Re-identification.}
        \newblock 2014 22nd International Conference on Pattern Recognition, 34--39.
        \newblock IEEE.
        \newblock \url{doi:10.1109/ICPR.2014.16}

        \bibitem{dichotomizer2000}
        Cha S.-H., and Srihari S.N. (2000).
        \newblock \textit{Writer Identification: Statistical Analysis and Dichotomizer.}
        \newblock Proceedings of the Joint IAPR International Workshops on Advances in Pattern Recognition, 123–132.
        \newblock Springer-Verlag.
        \newblock \url{doi:10.5555/645889.673379}
    \end{thebibliography}

\end{document}