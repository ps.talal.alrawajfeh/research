#!/usr/bin/python3

import math
import mimetypes
import os
import pickle
import random

import numpy as np
import tensorflow as tf
from alt_model_checkpoint.tensorflow import AltModelCheckpoint
from cv2 import cv2
from tensorflow.keras import Model
from tensorflow.keras.callbacks import LambdaCallback, ModelCheckpoint
from tensorflow.keras.layers import concatenate, Input, Conv2D, BatchNormalization, ReLU, \
    GlobalAveragePooling2D, Dense, Concatenate, AveragePooling2D, Dropout
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.utils import Sequence

# Settings
IMAGE_PATHS = ['/home/user/Development/data/trainingSignatures',
               '/home/user/Development/data/CABHBTF']
CLASS_CODE_LENGTH = 9
IMAGES_PER_CLASS_THRESHOLD = 2

IMAGE_CLASSES_FILE = 'image_classes.pickle'
TRAIN_IMAGE_CLASSES_FILE = 'train_image_classes.pickle'
VALIDATION_IMAGE_CLASSES_FILE = 'validation_image_classes.pickle'
TEST_IMAGE_CLASSES_FILE = 'test_image_classes.pickle'
TRAIN_TRIPLETS_FILE = 'train_triplets.pickle'
VALIDATION_TRIPLETS_FILE = 'validation_triplets.pickle'
TEST_TRIPLETS_FILE = 'test_triplets.pickle'
EVALUATION_REPORT_FILE = 'report.txt'
EMBEDDING_NETWORK_FILE_NAME = 'embedding_network.h5'
COMBINED_MODEL_FILE_NAME = 'combined_model.h5'

# Hyper-parameters
INPUT_SIZE = (128, 128)
INPUT_SHAPE = (INPUT_SIZE[1], INPUT_SIZE[0], 1)

MARGIN = 0.6
EMBEDDING_LAYER_SIZE = 128
HIDDEN_LAYER_SIZE = 128
INPUT_DROPOUT_PERCENTAGE = 0.05

USE_ALL_ANCHORS = False
NUMBER_OF_ANCHORS = 10
FORGERY_CLASSES_PER_REFERENCE = 14
FORGERIES_PER_CLASS = 6

TRAIN_CLASSES_PERCENTAGE = 0.7
VALIDATION_CLASSES_PERCENTAGE = 0.15
TEST_CLASSES_PERCENTAGE = 0.15

TRAINING_BATCH_SIZE = 32
VALIDATION_BATCH_SIZE = 256
EPOCHS = 100

# Augmentation Parameters
SPECKLES_FRACTION = 1e-3
MIN_ROTATION_ANGLE = 1
MAX_ROTATION_ANGLE = 5
MIN_QUALITY = 75
MAX_QUALITY = 95
QUALITY_RANDOMIZATION_STEP_SIZE = 5
MORPHOLOGY_VARIATION_MIN_SIZE = 250 * 250
MIN_RESIZE_PERCENTAGE = 90
VARIATIONS_PER_IMAGE = 4
AUGMENT_DATA = True


def get_extensions_for_type(general_type):
    for ext in mimetypes.types_map:
        if mimetypes.types_map[ext].split('/')[0] == general_type:
            yield ext.lower()


def memoize(function):
    memo = {}

    def wrapper(*args):
        if args in memo:
            return memo[args]
        else:
            return_value = function(*args)
            memo[args] = return_value
            return return_value

    return wrapper


def file_exists(file):
    return os.path.isfile(file)


@memoize
def get_image_extensions():
    return tuple(get_extensions_for_type('image'))


def file_extension(path):
    return os.path.splitext(os.path.basename(path))[1].lower()


def is_image(path):
    return file_extension(path) in get_image_extensions()


def class_from_file_name(file_name):
    return file_name[0:CLASS_CODE_LENGTH]


def serialize_object(obj, file):
    with open(file, 'wb') as f:
        pickle.dump(obj, f)


def deserialize_object(file):
    with open(file, 'rb') as f:
        return pickle.load(f)


def extract_classes(iterable,
                    class_lambda,
                    item_mapper_lambda=lambda x: x):
    classes = dict()
    for item in iterable:
        c = class_lambda(item)
        if c not in classes:
            classes[c] = []
        classes[c].append(item_mapper_lambda(item))
    return classes


def extract_classes_from_file_names(paths,
                                    file_name_class_lambda=class_from_file_name):
    image_classes = dict()
    for path in paths:
        image_classes.update(extract_classes(filter(is_image, os.listdir(path)),
                                             file_name_class_lambda,
                                             lambda f: os.path.join(path, f)))
    return image_classes


def read_image(image_path):
    return cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)


def to_byte_array(np_array):
    return np.array(np_array, bool)


def to_8_bit_array(np_array):
    return np.array(np_array, np.uint8)


def to_int(number):
    return int(math.ceil(number))


def random_bool():
    return random.random() >= 0.5


def threshold(image):
    return cv2.threshold(
        image,
        0,
        255,
        cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]


def remove_white_border(binary_image):
    mask = (255 - binary_image) > 0

    height, width = binary_image.shape[0:2]
    mask1, mask2 = mask.any(0), mask.any(1)
    x1, x2 = mask1.argmax(), width - mask1[::-1].argmax()
    y1, y2 = mask2.argmax(), height - mask2[::-1].argmax()

    return binary_image[y1:y2, x1:x2]


def pre_process_image(image):
    return remove_white_border(threshold(image))


def rotate(image, angle):
    (h, w) = image.shape
    center_x, center_y = w / 2, h / 2

    rotation_matrix = cv2.getRotationMatrix2D((center_x, center_y),
                                              angle,
                                              1.0)
    cos = np.abs(rotation_matrix[0, 0])
    sin = np.abs(rotation_matrix[0, 1])

    new_w = h * sin + w * cos
    new_h = h * cos + w * sin

    rotation_matrix[0, 2] += new_w / 2 - center_x
    rotation_matrix[1, 2] += new_h / 2 - center_y

    return cv2.warpAffine(image,
                          rotation_matrix,
                          (to_int(new_w), to_int(new_h)),
                          borderValue=(255),
                          flags=cv2.INTER_CUBIC)


def change_image_quality(image, quality):
    encoded = cv2.imencode('.jpg', image, [int(cv2.IMWRITE_JPEG_QUALITY), quality])[1]
    return cv2.imdecode(encoded, cv2.IMREAD_GRAYSCALE)


def random_coordinate(image):
    return random.randint(0, image.shape[0] - 1), random.randint(0, image.shape[1] - 1)


def speckle(image, fraction, color=0):
    coordinates = [random_coordinate(image)
                   for _ in range(to_int(image.size * fraction))]

    image_copy = np.array(image)
    for coord in coordinates:
        image_copy[coord[0], coord[1]] = color

    return image_copy


def generate_variation(image):
    variation = image

    if random_bool():
        x_factor = random.randint(MIN_RESIZE_PERCENTAGE, 99) / 100
        y_factor = random.randint(MIN_RESIZE_PERCENTAGE, 99) / 100

        variation = cv2.resize(image, (0, 0), fx=x_factor, fy=y_factor, interpolation=cv2.INTER_CUBIC)

    if random_bool():
        variation = change_image_quality(variation, random.randrange(MIN_QUALITY,
                                                                     MAX_QUALITY + 1,
                                                                     QUALITY_RANDOMIZATION_STEP_SIZE))

    if random_bool():
        if random_bool():
            sign = 1
        else:
            sign = -1
        variation = rotate(variation, sign * random.randint(MIN_ROTATION_ANGLE,
                                                            MAX_ROTATION_ANGLE))

    if np.prod(image.shape) >= MORPHOLOGY_VARIATION_MIN_SIZE and random_bool():
        variation = random.choice([
            lambda img: cv2.dilate(img, np.ones((2, 2))),
            lambda img: cv2.erode(img, np.ones((2, 2)))
        ])(variation)

    if random_bool():
        variation = speckle(variation,
                            SPECKLES_FRACTION)

    return variation


def pre_process_for_model(image):
    if image.shape[1] == INPUT_SIZE[0] and image.shape[0] == INPUT_SIZE[1]:
        resized = image
    else:
        resized = cv2.resize(image, INPUT_SIZE, interpolation=cv2.INTER_CUBIC)
        resized = threshold(resized)

    inverted = 255 - resized
    normalized = inverted / 255.0
    return to_byte_array(normalized.reshape(INPUT_SHAPE))


def image_classes_from_paths(image_paths,
                             augment_data=AUGMENT_DATA,
                             variations_per_image=VARIATIONS_PER_IMAGE):
    file_classes = extract_classes_from_file_names(image_paths)
    image_classes = dict()
    for c in file_classes:
        image_classes[c] = []
        for f in file_classes[c]:
            image = read_image(f)
            image_classes[c].append(pre_process_for_model(pre_process_image(image)))
            if augment_data:
                variations = [generate_variation(image) for _ in range(variations_per_image)]
                variations = [pre_process_for_model(pre_process_image(v)) for v in variations]
                image_classes[c].extend(variations)
    return image_classes


def test_signature_image_variations(output_path):
    files = []
    for path in IMAGE_PATHS:
        files.extend([os.path.join(path, f) for f in os.listdir(path)])

    files = list(filter(is_image, files))

    files_sample = random.sample(files, 1000)

    for f in files_sample:
        image = read_image(f)
        variation = remove_white_border(threshold(generate_variation(image)))
        file_name = os.path.splitext(os.path.basename(f))[0]
        cv2.imwrite(os.path.join(output_path, file_name + '_original.bmp'), image)
        cv2.imwrite(os.path.join(output_path, file_name + '_variation.bmp'), variation)


def squared_euclidean_distance(x, y):
    return tf.reduce_sum(tf.square(x - y), axis=-1)


def triplet_loss(_, y_predict, margin=MARGIN):
    anchor = y_predict[:, 0:EMBEDDING_LAYER_SIZE]
    positive = y_predict[:, EMBEDDING_LAYER_SIZE: EMBEDDING_LAYER_SIZE * 2]
    negative = y_predict[:, EMBEDDING_LAYER_SIZE * 2: EMBEDDING_LAYER_SIZE * 3]

    positive_dist = squared_euclidean_distance(anchor, positive)
    negative_dist = squared_euclidean_distance(anchor, negative)

    return tf.reduce_mean(tf.maximum(positive_dist - negative_dist + margin, 0.0))


def accuracy(_, y_predict, margin=MARGIN):
    anchor = y_predict[:, 0:EMBEDDING_LAYER_SIZE]
    positive = y_predict[:, EMBEDDING_LAYER_SIZE: EMBEDDING_LAYER_SIZE * 2]
    negative = y_predict[:, EMBEDDING_LAYER_SIZE * 2: EMBEDDING_LAYER_SIZE * 3]

    positive_dist = squared_euclidean_distance(anchor, positive)
    negative_dist = squared_euclidean_distance(anchor, negative)

    return tf.reduce_mean(tf.cast(positive_dist + margin < negative_dist, tf.dtypes.float32))


def conv_block(input_layer,
               filters,
               kernel_size):
    output_layer = BatchNormalization()(input_layer)
    output_layer = ReLU()(output_layer)
    return Conv2D(filters=filters,
                  kernel_size=kernel_size,
                  padding='same',
                  use_bias=False,
                  kernel_initializer='he_normal')(output_layer)


def dense_layer(input_layer,
                filters_multiplier,
                output_filters):
    output_layer = conv_block(input_layer,
                              filters_multiplier * output_filters,
                              (1, 1))
    output_layer = conv_block(output_layer,
                              output_filters,
                              (3, 3))
    return Concatenate()([input_layer, output_layer])


def dense_block(input_layer,
                filters_multiplier,
                output_filters,
                dense_layers):
    output_layer = input_layer
    for i in range(dense_layers):
        output_layer = dense_layer(output_layer,
                                   filters_multiplier,
                                   output_filters)
    return output_layer


def transition_block(input_layer,
                     reduction_factor):
    output_layer = conv_block(input_layer,
                              int(input_layer.shape[-1] * reduction_factor),
                              (1, 1))
    return AveragePooling2D(pool_size=(2, 2),
                            strides=2)(output_layer)


def embedding_network(input_shape=INPUT_SHAPE):
    input_layer = Input(shape=input_shape)

    output_layer = BatchNormalization()(input_layer)
    output_layer = Dropout(INPUT_DROPOUT_PERCENTAGE)(output_layer)

    output_layer = Conv2D(filters=32,
                          kernel_size=(3, 3),
                          padding='same',
                          use_bias=False,
                          kernel_initializer='he_normal')(output_layer)
    output_layer = BatchNormalization()(output_layer)
    output_layer = ReLU()(output_layer)

    output_layer = dense_block(output_layer, 2, 8, 3)
    output_layer = transition_block(output_layer, 0.5)
    output_layer = dense_block(output_layer, 2, 8, 3)
    output_layer = transition_block(output_layer, 0.5)
    output_layer = dense_block(output_layer, 2, 8, 3)
    output_layer = transition_block(output_layer, 0.5)

    output_layer = BatchNormalization()(output_layer)
    output_layer = ReLU()(output_layer)

    embedding = GlobalAveragePooling2D()(output_layer)

    embedding = Dense(HIDDEN_LAYER_SIZE,
                      kernel_initializer='he_normal',
                      use_bias=False)(embedding)
    embedding = BatchNormalization()(embedding)
    embedding = ReLU()(embedding)

    embedding = Dense(EMBEDDING_LAYER_SIZE,
                      kernel_initializer='he_normal',
                      use_bias=False)(embedding)
    embedding = ReLU()(embedding)

    return Model(inputs=input_layer, outputs=embedding)


def training_wrapped_model(input_shape=INPUT_SHAPE,
                           embedding_net=embedding_network()):
    anchor = Input(input_shape)
    positive = Input(input_shape)
    negative = Input(input_shape)

    anchor_embedding = embedding_net(anchor)
    positive_embedding = embedding_net(positive)
    negative_embedding = embedding_net(negative)

    merged_vector = concatenate(
        [anchor_embedding,
         positive_embedding,
         negative_embedding], axis=-1)

    return Model(inputs=[anchor, positive, negative], outputs=merged_vector)


class MinimumRecurrenceRateRandomSamplerWithReplacement:
    def __init__(self, array) -> None:
        self.array = array
        self.left_choices = array.copy()

    def next(self):
        if len(self.left_choices) == 0:
            self.left_choices = self.array.copy()
        i = random.randint(0, len(self.left_choices) - 1)
        return self.left_choices.pop(i)


def generate_triplets(classes,
                      use_all_anchors=USE_ALL_ANCHORS,
                      number_of_anchors=NUMBER_OF_ANCHORS,
                      forgery_classes_per_reference=FORGERY_CLASSES_PER_REFERENCE,
                      forgeries_per_class=FORGERIES_PER_CLASS):
    keys = [*classes]
    number_of_classes = len(keys)

    anchors = []
    positives = []
    negatives = []

    for c in range(number_of_classes):
        key = keys[c]
        keys_except_key = [k for k in keys if k != key]

        if use_all_anchors:
            number_of_anchors = len(classes[key])
        else:
            number_of_anchors = min(number_of_anchors, len(classes[key]))

        anchor_indices_sampler = MinimumRecurrenceRateRandomSamplerWithReplacement(list(range(len(classes[key]))))

        for r in range(number_of_anchors):
            anchor_index = anchor_indices_sampler.next()
            reference = [key, anchor_index]
            genuine_indices = list(range(len(classes[key])))
            genuine_indices.pop(anchor_index)
            genuine_indices_sampler = MinimumRecurrenceRateRandomSamplerWithReplacement(genuine_indices)
            forgery_keys = random.sample(keys_except_key, min(forgery_classes_per_reference, len(keys_except_key)))

            for forgery_key in forgery_keys:
                forgery_class = classes[forgery_key]
                forgery_indices_sampler = MinimumRecurrenceRateRandomSamplerWithReplacement(
                    list(range(len(forgery_class))))
                forgeries = [[forgery_key, forgery_indices_sampler.next()]
                             for _ in range(min(forgeries_per_class, len(forgery_class)))]

                anchors.extend([reference] * len(forgeries))
                positives.extend([[key, genuine_indices_sampler.next()]
                                  for _ in range(len(forgeries))])
                negatives.extend(forgeries)

    return anchors, positives, negatives


def shuffle_triplets(triplets):
    anchors, positives, negatives = triplets
    indices = list(range(len(anchors)))
    random.shuffle(indices)

    shuffled_anchors = [anchors[indices[i]] for i in range(len(anchors))]
    shuffled_positives = [positives[indices[i]] for i in range(len(anchors))]
    shuffled_true_negatives = [negatives[indices[i]] for i in range(len(anchors))]

    return shuffled_anchors, shuffled_positives, shuffled_true_negatives


class DataGenerator(Sequence):
    def __init__(self,
                 image_classes,
                 triplets,
                 batch_size,
                 count):
        self.batch_size = batch_size
        self.count = count
        self.image_classes = image_classes
        self.anchors, self.positives, self.negatives = shuffle_triplets(triplets)
        self.true_output = np.array([[0] for _ in range(batch_size)])

    def __len__(self):
        return math.ceil(self.count / self.batch_size)

    def __getitem__(self, index):
        anchors, positives, negatives = [], [], []
        for i in range(index * self.batch_size, min((index + 1) * self.batch_size, len(self.anchors))):
            anchor_pair = self.anchors[i]
            positive_pair = self.positives[i]
            negative_pair = self.negatives[i]
            anchors.append(self.image_classes[anchor_pair[0]][anchor_pair[1]])
            positives.append(self.image_classes[positive_pair[0]][positive_pair[1]])
            negatives.append(self.image_classes[negative_pair[0]][negative_pair[1]])

        if len(anchors) == 0:
            return self.__getitem__(0)

        true_output = self.true_output
        if len(anchors) != self.batch_size:
            true_output = np.array([[0] for _ in range(len(anchors))])

        return [np.array(anchors),
                np.array(positives),
                np.array(negatives)], true_output

    def on_epoch_end(self):
        self.anchors, self.positives, self.negatives = shuffle_triplets([self.anchors, self.positives, self.negatives])


def class_train_validation_test_split(image_classes):
    keys = [*image_classes]

    keys_indices = list(range(len(keys)))
    random.shuffle(keys_indices)

    classes = (dict(), dict(), dict())
    percentages = [TRAIN_CLASSES_PERCENTAGE,
                   VALIDATION_CLASSES_PERCENTAGE,
                   TEST_CLASSES_PERCENTAGE]

    for i in range(3):
        chosen_key_indices = keys_indices[:int(len(image_classes) * percentages[i])]
        for j in range(len(chosen_key_indices)):
            key = keys[chosen_key_indices[j]]
            classes[i][key] = image_classes[key]
        keys_indices = keys_indices[len(chosen_key_indices):]

    return classes


def append_lines_to_report(lines):
    with open(EVALUATION_REPORT_FILE, 'a') as f:
        f.writelines(lines)


def epoch_metrics_log(epoch, logs):
    line = f'epoch: {epoch}'
    line += ' - '
    line += f'loss: {logs["loss"]}'
    line += ' - '
    line += f'binary accuracy: {logs["accuracy"]}'
    line += ' - '
    line += f'validation loss: {logs["val_loss"]}'
    line += ' - '
    line += f'validation binary accuracy: {logs["val_accuracy"]}'
    return line


def train(train_image_classes,
          train_triplets,
          validation_image_classes,
          validation_triplets):
    embedding_net = embedding_network()
    model = training_wrapped_model(embedding_net=embedding_net)

    embedding_net.summary()
    model.summary()

    model.compile(loss=triplet_loss,
                  optimizer=Adam(),
                  metrics=[accuracy])

    train_data_generator = DataGenerator(train_image_classes,
                                         train_triplets,
                                         TRAINING_BATCH_SIZE,
                                         len(train_triplets[0]))

    validation_data_generator = DataGenerator(validation_image_classes,
                                              validation_triplets,
                                              VALIDATION_BATCH_SIZE,
                                              len(validation_triplets[0]))

    append_lines_to_report('[training log]\n\n')

    update_report_callback = LambdaCallback(
        on_epoch_end=lambda epoch, logs: append_lines_to_report(
            epoch_metrics_log(epoch, logs) + '\n'
        ))

    last_checkpoint_callback = ModelCheckpoint(
        filepath=COMBINED_MODEL_FILE_NAME + '_checkpoint_last.h5',
        save_best_only=False)

    best_checkpoint_callback = ModelCheckpoint(
        filepath=COMBINED_MODEL_FILE_NAME + '_{epoch:02d}_{val_accuracy:0.4f}.h5',
        monitor='val_accuracy',
        mode='max',
        save_best_only=True)

    en_last_model_checkpoint = AltModelCheckpoint(EMBEDDING_NETWORK_FILE_NAME + '_checkpoint_last.h5',
                                                  embedding_net)
    en_best_model_checkpoint = AltModelCheckpoint(
        EMBEDDING_NETWORK_FILE_NAME + '_checkpoint_{epoch:02d}_{val_accuracy:0.4f}.h5',
        embedding_net,
        monitor='val_accuracy',
        save_best_only=True,
        mode='max')

    model.fit_generator(generator=train_data_generator,
                        steps_per_epoch=math.ceil(len(train_triplets[0]) / TRAINING_BATCH_SIZE),
                        validation_data=validation_data_generator,
                        validation_steps=math.ceil(len(validation_triplets[0]) / VALIDATION_BATCH_SIZE),
                        epochs=EPOCHS,
                        callbacks=[update_report_callback,
                                   last_checkpoint_callback,
                                   best_checkpoint_callback,
                                   en_last_model_checkpoint,
                                   en_best_model_checkpoint])


def run_pipeline():
    image_classes = None

    train_image_classes = None
    validation_image_classes = None
    test_image_classes = None

    train_triplets = None
    validation_triplets = None
    test_triplets = None

    if not file_exists(IMAGE_CLASSES_FILE):
        image_classes = image_classes_from_paths(IMAGE_PATHS)
        serialize_object(image_classes, IMAGE_CLASSES_FILE)

    if not file_exists(TRAIN_IMAGE_CLASSES_FILE) or \
            not file_exists(VALIDATION_IMAGE_CLASSES_FILE) or \
            not file_exists(TEST_IMAGE_CLASSES_FILE):
        if image_classes is None:
            image_classes = deserialize_object(IMAGE_CLASSES_FILE)

        train_image_classes, \
        validation_image_classes, \
        test_image_classes = class_train_validation_test_split(image_classes)

        serialize_object(train_image_classes, TRAIN_IMAGE_CLASSES_FILE)
        serialize_object(validation_image_classes, VALIDATION_IMAGE_CLASSES_FILE)
        serialize_object(test_image_classes, TEST_IMAGE_CLASSES_FILE)

        del image_classes
        image_classes = None
        del test_image_classes
        test_image_classes = None

    if not file_exists(TRAIN_TRIPLETS_FILE):
        if train_image_classes is None:
            train_image_classes = deserialize_object(TRAIN_IMAGE_CLASSES_FILE)
        train_triplets = generate_triplets(train_image_classes)
        serialize_object(train_triplets, TRAIN_TRIPLETS_FILE)

    if not file_exists(VALIDATION_TRIPLETS_FILE):
        if validation_image_classes is None:
            validation_image_classes = deserialize_object(VALIDATION_IMAGE_CLASSES_FILE)
        validation_triplets = generate_triplets(validation_image_classes)
        serialize_object(validation_triplets, VALIDATION_TRIPLETS_FILE)

    if train_image_classes is None:
        train_image_classes = deserialize_object(TRAIN_IMAGE_CLASSES_FILE)
    if train_triplets is None:
        train_triplets = deserialize_object(TRAIN_TRIPLETS_FILE)

    if validation_image_classes is None:
        validation_image_classes = deserialize_object(VALIDATION_IMAGE_CLASSES_FILE)
    if validation_triplets is None:
        validation_triplets = deserialize_object(VALIDATION_TRIPLETS_FILE)

    train(train_image_classes,
          train_triplets,
          validation_image_classes,
          validation_triplets)

    del train_image_classes
    train_image_classes = None
    del train_triplets
    train_triplets = None

    del validation_image_classes
    validation_image_classes = None
    del validation_triplets
    validation_triplets = None


def main():
    run_pipeline()


if __name__ == '__main__':
    main()
