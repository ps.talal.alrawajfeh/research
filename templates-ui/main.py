import sys

import cv2
import numpy as np
from PyQt5.QtCore import QSize
from PyQt5.QtGui import QPixmap, QImage
from PyQt5.QtWidgets import QApplication, QMainWindow, QPushButton, QFileDialog, QHBoxLayout, QLabel


def convert_qt_image_to_ndarray(image: QImage) -> np.ndarray:
    image = image.convertToFormat(4)

    width = image.width()
    height = image.height()

    ptr = image.bits()
    ptr.setsize(image.byteCount())
    arr = np.array(ptr).reshape(height, width, 4)

    return arr


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        self.setWindowTitle("My App")
        self.setFixedSize(QSize(400, 300))

        button = QPushButton(self)
        button.setText('Choose File')
        button.setFixedSize(QSize(100, 25))
        button.clicked.connect(self.button_clicked)
        button.move(400 - 100 - 15, 300 - 25 - 15)

        label = QLabel(self)
        pixmap = QPixmap()
        label.move(15, 15)
        label.setFixedSize(QSize(400 - 15 - 15, 300 - 15 - 25 - 15))
        label.setPixmap(pixmap)

        self.label_bitmap = label

        hbox = QHBoxLayout()
        hbox.addStretch(1)
        hbox.addWidget(button)
        hbox.addWidget(label)

    def button_clicked(self):
        options = QFileDialog.Options()
        file_name, _ = QFileDialog.getOpenFileName(self,
                                                   "QFileDialog.getOpenFileName()",
                                                   "",
                                                   "All Files (*);;Python Files (*.py)",
                                                   options=options)

        q_pixmap = QPixmap(file_name)
        image = convert_qt_image_to_ndarray(q_pixmap.toImage())
        image = cv2.resize(image, (400 - 15 - 15, 300 - 15 - 25 - 15), interpolation=cv2.INTER_AREA)
        image = cv2.cvtColor(image, cv2.COLOR_RGBA2RGB)
        image = np.require(image, np.uint8, 'C')
        test_image = QImage(image.data, image.shape[1], image.shape[0], image.strides[0], QImage.Format_RGB888)
        self.label_bitmap.setPixmap(QPixmap.fromImage(test_image))


def main():
    app = QApplication(sys.argv)

    window = MainWindow()
    window.show()

    app.exec()


if __name__ == '__main__':
    main()
