import tensorflow as tf
import numpy as np
import time

import onnxruntime as ort

def main():
    # mb2: 0.0035183897018432617
    # mb3: 0.004649200201034546

    # mb3 = tf.keras.applications.MobileNetV3Large()
    # mb2 = tf.keras.applications.MobileNetV2()

    # mb3.save('/home/u764/Downloads/fonts-downloads/mbnet/mbnet3.h5')
    # mb2.save('/home/u764/Downloads/fonts-downloads/mbnet/mbnet2.h5')

    # test_input = np.random.uniform(0.0, 1.0, size=(1, 224, 224, 3)).astype(np.float32)

    # mb2 = ort.InferenceSession('/home/u764/Downloads/fonts-downloads/mbnet/mbnet2.onnx')
    
    # start_time = time.time()
    # for i in range(1000):
    #     # mb2(test_input)
    #     outputs = mb2.run(None, {'input_2': test_input})
    # end_time = time.time()
    # print('mb2:', (end_time - start_time) / 1000)

    # mb3 = ort.InferenceSession('/home/u764/Downloads/fonts-downloads/mbnet/mbnet3.onnx')
    
    # start_time = time.time()
    # for i in range(1000):
    #     outputs = mb3.run(None, {'input_1': test_input})
    # end_time = time.time()
    # print('mb3:', (end_time - start_time) / 1000)

    mb = ort.InferenceSession('/home/u764/Downloads/fonts-downloads/MobileFaceNet_9925_9680/mobilenet.onnx')
    test_input = np.random.uniform(0.0, 1.0, size=(1, 112, 112, 3)).astype(np.float32)

    start_time = time.time()
    for i in range(1000):
        outputs = mb.run(None, {'img_inputs:0': test_input})
    end_time = time.time()
    print('mb:', (end_time - start_time) / 1000)

    # print(tf.compat.v1.saved_model.load('/home/u764/Downloads/fonts-downloads/MobileFaceNet_9925_9680/MobileFaceNet_9925_9680.pb'))

if __name__ == '__main__':
    main()
