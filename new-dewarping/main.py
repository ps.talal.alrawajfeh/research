import cv2
import matplotlib.pyplot as plt
import numpy as np


# Do a binary search on the gradients to find a suitable threshold
# We are trying to find the maximum threshold that creates a contour that is closest to a 4-polygon after we perform edge hysteresis
# We might do a closing operation in the beginning to remove text so we are left with only the background
# Should we perform a blurring/smoothing operation?

def threshold_image(image, threshold):
    image_copy = np.copy(image)
    image_copy[image_copy >= threshold] = 255
    image_copy[image_copy < threshold] = 0
    return image_copy


def threshold_image_weak(image, high_threshold, low_threshold):
    image_copy = np.copy(image)
    image_copy[image_copy >= high_threshold] = 0
    image_copy[image_copy >= low_threshold] = 255
    image_copy[image_copy < low_threshold] = 0
    return image_copy


def perform_edge_hysteresis(strong_edges, weak_edges):
    mask = weak_edges | strong_edges
    result = strong_edges
    while True:
        new_strong_edges = cv2.dilate(result, np.ones((3, 3), np.uint8))
        new_strong_edges = (new_strong_edges & mask)
        if np.all(new_strong_edges == result):
            break
        result = new_strong_edges
    return result


def find_max_closed_contour_area(edges):
    contours, hierarchy = cv2.findContours(cv2.dilate(edges, np.ones((3, 3), np.uint8)),
                                           cv2.RETR_CCOMP,
                                           cv2.CHAIN_APPROX_SIMPLE)
    hierarchy = hierarchy[0]
    max_area = -1
    for i in range(len(contours)):
        if hierarchy[i][2] >= 0:
            contour = contours[i]
            contour_area = cv2.moments(contour)['m00']

            if contour_area > max_area:
                max_area = contour_area

    return max_area


def find_best_hysteresis(gradients, strong_edges, high_threshold):
    min_area = 0.2 * np.prod(gradients.shape)

    b = high_threshold - 1
    a = 1

    edges_b = perform_edge_hysteresis(strong_edges, threshold_image_weak(gradients,
                                                                         high_threshold,
                                                                         b))

    max_contour_area_b = find_max_closed_contour_area(edges_b)
    if max_contour_area_b >= min_area:
        return edges_b

    edges_a = perform_edge_hysteresis(strong_edges, threshold_image_weak(gradients,
                                                                         high_threshold,
                                                                         a))

    max_contour_area_a = find_max_closed_contour_area(edges_a)
    if max_contour_area_a < min_area:
        return None

    result = edges_a
    while (b - a) > 1:
        thresh = (a + b) // 2
        edges_thresh = perform_edge_hysteresis(strong_edges, threshold_image_weak(gradients,
                                                                                  high_threshold,
                                                                                  thresh))

        max_contour_area_thresh = find_max_closed_contour_area(edges_thresh)
        if max_contour_area_thresh >= min_area:
            a = thresh
            result = edges_thresh
        else:
            b = thresh
            result = edges_thresh

    return result


def find_best_edges(gradients):
    b = 254
    a = 2

    strong_edges = threshold_image(gradients, b)

    if not np.all(strong_edges == 0):
        edges_b = find_best_hysteresis(gradients, strong_edges, b)
    else:
        edges_b = None

    if edges_b is not None:
        return edges_b

    strong_edges = threshold_image(gradients, a)

    if not np.all(strong_edges == 0):
        edges_a = find_best_hysteresis(gradients, strong_edges, a)
    else:
        edges_a = None

    if edges_a is None:
        return None

    result = edges_a
    while (b - a) > 1:
        thresh = (a + b) // 2
        strong_edges = threshold_image(gradients, thresh)
        if not np.all(strong_edges == 0):
            edges_thresh = find_best_hysteresis(gradients, strong_edges, thresh)
        else:
            edges_thresh = None

        if edges_thresh is not None:
            a = thresh
            result = edges_thresh
        else:
            b = thresh
            result = edges_thresh

    return result


def edge_detection_colored(image_luv):
    sobel_x = cv2.Sobel(image_luv[:, :, 1:], cv2.CV_64F, 1, 0, ksize=7)
    sobel_y = cv2.Sobel(image_luv[:, :, 1:], cv2.CV_64F, 0, 1, ksize=7)
    gradients = np.max(np.hypot(sobel_x, sobel_y), axis=-1)
    # gradients = np.power(gradients, 2 / 3)
    max_gradient = np.max(gradients)
    min_gradient = np.min(gradients)
    gradients = (gradients - min_gradient) / (max_gradient - min_gradient) * 255
    return gradients


def edge_detection_gray(image_l):
    sobel_x = cv2.Sobel(image_l, cv2.CV_64F, 1, 0, ksize=7)
    sobel_y = cv2.Sobel(image_l, cv2.CV_64F, 0, 1, ksize=7)
    gradients = np.hypot(sobel_x, sobel_y)
    # gradients = np.power(gradients, 2 / 3)
    max_gradient = np.max(gradients)
    min_gradient = np.min(gradients)
    gradients = (gradients - min_gradient) / (max_gradient - min_gradient) * 255
    return gradients


def main():
    image = cv2.imread('/home/u764/Downloads/test/test-tesser-a (copy)/20230611_082222832_iOS.heic.png')
    # analyze_image(image)
    image = cv2.morphologyEx(image,
                             cv2.MORPH_CLOSE,
                             cv2.getStructuringElement(cv2.MORPH_RECT, (21, 21)))

    max_side_length = np.max(image.shape)
    resize_factor = 750 / max_side_length
    image = cv2.resize(image, None, fx=resize_factor, fy=resize_factor, interpolation=cv2.INTER_CUBIC)

    # image = cv2.rotate(image, cv2.ROTATE_90_CLOCKWISE)
    plt.imshow(image)
    plt.show()

    image_luv = cv2.cvtColor(image, cv2.COLOR_BGR2LUV)
    image_l = image_luv[:, :, 0]

    colored_gradients = edge_detection_colored(image_luv)
    gray_gradients = edge_detection_gray(image_l)

    gradients = (colored_gradients + gray_gradients) / 2
    gradients[gradients >= 255] = 255
    gradients[gradients < 0] = 0
    gradients = gradients.astype(np.uint8)

    plt.imshow(gradients)
    plt.show()
    result_edges = find_best_edges(gradients)
    if result_edges is None:
        print('no closed contour found')
        return

    plt.imshow(result_edges, plt.cm.gray)
    plt.show()

    # thresh = 20
    # gradients[gradients >= thresh] = 255
    # gradients[gradients < thresh] = 0
    # gradients = cv2.threshold(gradients, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]

    # plt.imshow(gradients, plt.cm.gray)
    # plt.show()
    # print(np.mean(gradients))
    # print(np.std(gradients))
    #
    # plt.hist(gradients.ravel(), bins=255)
    # plt.show()


def main2():
    image = cv2.imread('/home/u764/Downloads/untitled.drawio.png')
    image = 255 - cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    image = threshold_image(image, 127)
    image = cv2.dilate(image, np.ones((3, 3), np.uint8))

    contours, hierarchy = cv2.findContours(image,
                                           cv2.RETR_EXTERNAL,
                                           cv2.CHAIN_APPROX_NONE)
    max_area = -1
    largest_contour = None

    for i in range(len(contours)):
        contour = contours[i]
        contour_area = cv2.moments(contour)['m00']
        if contour_area > max_area:
            max_area = contour_area
            largest_contour = contour.reshape(-1, 2)

    if largest_contour is not None:
        largest_contour = np.flip(largest_contour, axis=0)

        skipping_length = 50
        slope_rescaling_factor = 100
        angle_difference_threshold = 5
        parameter_histogram_threshold = 10
        intercept_difference_threshold = 10

        next_points = largest_contour[skipping_length:, :]
        previous_points = largest_contour[:-skipping_length:, :]
        deltas = next_points - previous_points

        slopes = deltas[:, 1] / (deltas[:, 0] + 1e-7)
        max_slope = image.shape[0]
        slopes[np.abs(deltas[:, 0]) < 1e-7] = max_slope
        slopes[slopes > max_slope] = max_slope
        slopes[slopes < -max_slope] = -max_slope
        intercepts = previous_points[:, 1] - slopes * previous_points[:, 0]

        slopes = np.expand_dims(slopes, axis=-1)
        intercepts = np.expand_dims(intercepts, axis=-1)
        parameters = np.concatenate([slopes, intercepts], axis=-1)

        contour_image = np.zeros(image.shape, np.uint8)
        points_x, points_y = np.transpose(largest_contour)
        contour_image[points_y, points_x] = 255
        # contour_image = cv2.dilate(contour_image, np.ones((3, 3), np.uint8))

        parameter_histogram = dict()
        for a, b in parameters:
            angle = int(round(a * slope_rescaling_factor))
            intercept = int(round(b))
            key = (angle, intercept)
            if key not in parameter_histogram:
                parameter_histogram[key] = 0
            parameter_histogram[key] += 1

        filtered_parameters = []
        for a, b in parameter_histogram:
            if parameter_histogram[a, b] > parameter_histogram_threshold:
                filtered_parameters.append((a, b))

        contour_pixels = np.count_nonzero(contour_image)
        scores = []
        for a, b in filtered_parameters:
            a = a / slope_rescaling_factor
            x1 = 0
            x2 = image.shape[1]
            y1 = int(a * x1 + b)
            y2 = int(a * x2 + b)
            line_image = np.zeros(contour_image.shape, np.uint8)
            cv2.line(line_image, (x1, y1), (x2, y2), 255, 1)
            score = np.count_nonzero(line_image & contour_image) / contour_pixels
            scores.append(score)

        best_line_i = np.argmax(scores)
        best_a, best_b = filtered_parameters[best_line_i]

        chosen_parameters = [(best_a, best_b)]

        scored_parameters = list(zip(scores, filtered_parameters))
        scored_parameters.sort(key=lambda x: x[0], reverse=True)

        remaining = 3
        for i in range(1, len(scored_parameters)):
            score, (a, b) = scored_parameters[i]
            found_good_parameters = True
            for previous_a, previous_b in chosen_parameters:
                if abs(previous_a - a) <= angle_difference_threshold and \
                        abs(previous_b - b) <= intercept_difference_threshold:
                    found_good_parameters = False
                    break
            if found_good_parameters:
                chosen_parameters.append((a, b))
                remaining -= 1
                if remaining == 0:
                    break

        image = cv2.cvtColor(image, cv2.COLOR_GRAY2BGR)
        for a, b in chosen_parameters:
            a = a / slope_rescaling_factor
            x1 = 0
            x2 = image.shape[1]
            y1 = int(a * x1 + b)
            y2 = int(a * x2 + b)

            cv2.line(image, (x1, y1), (x2, y2), (255, 0, 0), 1)

        plt.imshow(image, plt.cm.gray)
        plt.show()


if __name__ == '__main__':
    main2()
