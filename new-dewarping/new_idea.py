import itertools
import math
import os

import cv2
import matplotlib
import numpy as np
import shapely
from matplotlib import pyplot as plt
from textdistance import DamerauLevenshtein
from tqdm import tqdm

EXTRACTION_MAX_SIDE_LENGTH = 750
MAX_MICR_LINE_PART_DISTANCE_FROM_CENTER_LINE = 5
MAX_MICR_LINE_DETECTION_ANGLE = 25
MICR_LINE_MIN_WIDTH_TO_IMAGE_WIDTH_RATIO = 0.45
MICR_LINE_MAX_WIDTH_TO_HEIGHT_RATIO = 0.06
MICR_CHARACTERS = 'ABCD0123456789'
EPSILON = 1e-6


def threshold_image(image, threshold):
    image_copy = np.copy(image)
    image_copy[image_copy >= threshold] = 255
    image_copy[image_copy < threshold] = 0
    return image_copy


def double_threshold_image(image, high_threshold, low_threshold):
    image_copy = np.copy(image)
    image_copy[image_copy >= high_threshold] = 0
    image_copy[image_copy >= low_threshold] = 255
    image_copy[image_copy < low_threshold] = 0
    return image_copy


def perform_edge_hysteresis(strong_edges, weak_edges):
    mask = weak_edges | strong_edges
    result = strong_edges
    while True:
        new_strong_edges = cv2.dilate(result, np.ones((3, 3), np.uint8))
        new_strong_edges = (new_strong_edges & mask)
        if np.all(new_strong_edges == result):
            break
        result = new_strong_edges
    return result


def points_to_parametric_line(point1, point2):
    (x1, y1) = point1
    (x2, y2) = point2
    return np.array([[x2 - x1, x1],
                     [y2 - y1, y1]], np.float32)


def is_parametric_line_vertical(parametric_line):
    a_x, _ = parametric_line[0]
    a_y, _ = parametric_line[1]

    if abs(a_x) <= EPSILON < abs(a_y):
        return True
    return False


def is_parametric_line_horizontal(parametric_line):
    a_x, _ = parametric_line[0]
    a_y, _ = parametric_line[1]

    if abs(a_y) <= EPSILON < abs(a_x):
        return True
    return False


def vertical_lines_intersect(parametric_line1, parametric_line2):
    _, b_x1 = parametric_line1[0]
    _, b_x2 = parametric_line2[0]

    if abs(b_x1 - b_x2) <= EPSILON:
        return True
    return False


def horizontal_lines_intersect(parametric_line1, parametric_line2):
    _, b_y1 = parametric_line1[1]
    _, b_y2 = parametric_line2[1]

    if abs(b_y1 - b_y2) <= EPSILON:
        return True
    return False


def calculate_intersection(parametric_line1,
                           parametric_line2):
    if parametric_line1 is None or parametric_line2 is None:
        return None

    a1_x, b1_x = parametric_line1[0]
    a1_y, b1_y = parametric_line1[1]

    a2_x, b2_x = parametric_line2[0]
    a2_y, b2_y = parametric_line2[1]

    mat1 = np.array([[a1_x, -a2_x],
                     [a1_y, -a2_y]], np.float32)
    if np.linalg.det(mat1) == 0:
        return None

    mat2 = np.array([[b2_x - b1_x],
                     [b2_y - b1_y]], np.float32)

    t = np.dot(np.linalg.inv(mat1), mat2).reshape(2)

    point1 = parametric_line1[:, 0] * t[0] + parametric_line1[:, 1]
    point2 = parametric_line2[:, 0] * t[1] + parametric_line2[:, 1]

    return (point1 + point2) / 2


def get_lines_intersection_point(line1_point1, line1_point2, line2_point1, line2_point2):
    parametric_line1 = points_to_parametric_line(line1_point1,
                                                 line1_point2)
    parametric_line2 = points_to_parametric_line(line2_point1,
                                                 line2_point2)
    if is_parametric_line_vertical(parametric_line1) and is_parametric_line_vertical(parametric_line2):
        if not vertical_lines_intersect(parametric_line1, parametric_line2):
            intersection_point = None
        else:
            y1 = max(min(line1_point1[1], line1_point2[1]),
                     min(line2_point1[1], line2_point2[1]))
            y2 = min(max(line1_point1[1], line1_point2[1]),
                     max(line2_point1[1], line2_point2[1]))
            x = np.mean([line2_point1[0], line1_point2[0], line2_point1[0], line2_point2[0]])
            intersection_point = (x, (y1 + y2) / 2)
    elif is_parametric_line_horizontal(parametric_line1) and is_parametric_line_horizontal(parametric_line2):
        if not horizontal_lines_intersect(parametric_line1, parametric_line2):
            intersection_point = None
        else:
            x1 = max(min(line1_point1[0], line1_point2[0]),
                     min(line2_point1[0], line2_point2[0]))
            x2 = min(max(line1_point1[0], line1_point2[0]),
                     max(line2_point1[0], line2_point2[0]))
            y = np.mean([line2_point1[1], line1_point2[1], line2_point1[1], line2_point2[1]])
            intersection_point = ((x1 + x2) / 2, y)
    else:
        intersection_point = calculate_intersection(parametric_line1,
                                                    parametric_line2)
    if intersection_point is None:
        return None
    return np.array(intersection_point, np.float32)


def consolidate_two_vertices_with_minimum_cost(vertices, image_width, image_height):
    best_polygon_vertices = vertices
    was_consolidated = False
    vertices_polygon = shapely.Polygon(vertices)
    vertices_polygon_area = vertices_polygon.area
    best_score = None

    for i in range(len(vertices)):
        line1_point1 = vertices[i - 1]
        line1_point2 = vertices[i]
        line2_point1 = vertices[(i + 1) % len(vertices)]
        line2_point2 = vertices[(i + 2) % len(vertices)]

        intersection_point = get_lines_intersection_point(line1_point1,
                                                          line1_point2,
                                                          line2_point1,
                                                          line2_point2)
        if intersection_point is None:
            continue

        x, y = intersection_point
        if x < 0 or y < 0 or x >= image_width or y >= image_height:
            continue

        new_polygon_vertices = [p for p in vertices]
        next_i = (i + 1) % len(vertices)
        if next_i > i:
            new_polygon_vertices.pop(next_i)
            new_polygon_vertices.pop(i)
        else:
            new_polygon_vertices.pop(i)
            new_polygon_vertices.pop(next_i)
        new_polygon_vertices.insert(i, intersection_point)
        new_polygon_vertices = np.array(new_polygon_vertices, np.float32)

        try:
            new_polygon = shapely.Polygon(new_polygon_vertices)
            intersection = new_polygon.intersection(vertices_polygon).area
            union = new_polygon.area + vertices_polygon_area - intersection
            iou = intersection / union

            if best_score is None or best_score < iou:
                best_score = iou
                was_consolidated = True
                best_polygon_vertices = new_polygon_vertices
        except:
            continue

    if was_consolidated is False:
        for i in range(len(vertices)):
            new_polygon_vertices = [p for p in vertices]
            new_polygon_vertices.pop(i)

            try:
                new_polygon = shapely.Polygon(new_polygon_vertices)
                intersection = new_polygon.intersection(vertices_polygon).area
                union = new_polygon.area + vertices_polygon_area - intersection
                iou = intersection / union

                if best_score is None or best_score < iou:
                    best_score = iou
                    was_consolidated = True
                    best_polygon_vertices = new_polygon_vertices
            except:
                continue

    return best_polygon_vertices, was_consolidated


def euclidean_distance(x, y):
    return np.sqrt(np.sum(np.square(np.array(x, np.float32) - np.array(y, np.float32))))


def sort_points_clockwise_polygonal(origin,
                                    points):
    point_angle_pairs = [[points[i], compute_angle(origin, points[i])] for i in range(len(points))]
    point_angle_pairs.sort(key=lambda x: x[1], reverse=True)

    i = 0
    n = len(point_angle_pairs)
    while i < n:
        if abs(point_angle_pairs[i][1] - point_angle_pairs[(i + 1) % n][1]) < EPSILON:
            j = 1
            while j < n - 1:
                if abs(point_angle_pairs[(i + j) % n][1] - point_angle_pairs[(i + j + 1) % n][1]) >= EPSILON:
                    j += 1
                    break
                j += 1
            section = [point_angle_pairs[(i + k) % n] for k in range(0, j)]
            section.sort(key=lambda x: euclidean_distance(point_angle_pairs[i - 1][0], x[0]))

            if i + j > n:
                point_angle_pairs = section[n - i:] + point_angle_pairs[(i + j) % n:i] + section[:n - i]
            else:
                point_angle_pairs = point_angle_pairs[:i] + section + point_angle_pairs[i + j:]
            i += j - 1
            if i > n:
                break
        i += 1
    return [point for point, _ in point_angle_pairs]


def try_consolidate_polygon_points(polygon_vertices,
                                   image_width,
                                   image_height,
                                   target_points):
    polygon_vertices = np.array(polygon_vertices, np.float32)
    polygon_vertices = sort_points_clockwise_polygonal(compute_center_of_gravity(polygon_vertices), polygon_vertices)
    best_polygon = polygon_vertices
    while len(best_polygon) > target_points:
        best_polygon, was_consolidated = consolidate_two_vertices_with_minimum_cost(best_polygon,
                                                                                    image_width,
                                                                                    image_height)
        if not was_consolidated:
            break
        best_polygon = sort_points_clockwise_polygonal(compute_center_of_gravity(best_polygon),
                                                       best_polygon)
        best_polygon = np.array(best_polygon, np.float32)

    return best_polygon


def approximate_contour_polygon(contour,
                                image_width,
                                image_height,
                                arc_length_percentage=0.001):
    if contour is None or len(contour) < 3:
        return np.array([])

    epsilon = arc_length_percentage * cv2.arcLength(contour, True)
    contour = cv2.approxPolyDP(contour, epsilon, True).reshape(-1, 2)

    convex_hull = cv2.convexHull(contour).reshape(-1, 2)
    contour = np.array(convex_hull, np.int32)
    contour = try_consolidate_polygon_points(contour, image_width, image_height, 4)

    return np.round(contour).astype(int).tolist(), convex_hull


def adjust_angle_by_quadrant(angle,
                             point):
    x, y = point
    if x < 0 and y > 0:
        return 180 - angle
    if x < 0 and y < 0:
        return 180 + angle
    if x > 0 and y < 0:
        return 360 - angle
    return angle


def compute_angle(origin,
                  point):
    x1 = point[0] - origin[0]
    y1 = origin[1] - point[1]
    if math.fabs(x1) > 1e-6:
        theta = math.fabs(math.atan(y1 / x1)) / math.pi * 180
    else:
        theta = 90
    return adjust_angle_by_quadrant(theta, (x1, y1))


def compute_center_of_gravity(points):
    points_array = np.array(points, np.float32)
    return np.array([np.mean(points_array[:, :1]), np.mean(points_array[:, 1:])])


def slope_to_angle(slope):
    return (math.degrees(math.atan(slope)) + 180) % 180


def find_largest_contour(image):
    contours, hierarchy = cv2.findContours(image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    max_area = -1
    largest_contour = None
    for contour in contours:
        contour_area = cv2.moments(contour)['m00']
        if contour_area > max_area:
            max_area = contour_area
            largest_contour = contour
    if largest_contour is None:
        return None
    return largest_contour.reshape(-1, 2), max_area


def find_best_closed_contour(edges,
                             area_threshold):
    inverted_edges = 255 - cv2.dilate(edges, np.ones((5, 5), np.uint8))

    labels_count, labeled_image, stats, centroids = cv2.connectedComponentsWithStats(inverted_edges, 8, cv2.CV_32S)

    if labels_count == 1:
        return None

    chosen_labels = stats[1:, -1] > area_threshold
    chosen_labels = np.where(chosen_labels)[0] + 1

    best_score = None
    best_contour = None
    best_points = None

    for i in range(len(chosen_labels)):
        label = chosen_labels[i]
        if labeled_image[0, 0] == label:
            continue

        connected_component_image = np.array(labeled_image == label, np.uint8) * 255

        if np.all(connected_component_image == 0):
            continue

        connected_component_image = cv2.dilate(connected_component_image,
                                               np.ones((7, 7), np.uint8))
        contour, contour_area = find_largest_contour(connected_component_image)

        if contour_area > area_threshold:
            points, contour_convex_hull = approximate_contour_polygon(contour, edges.shape[1], edges.shape[0])
            if points is None:
                continue

            try:
                polygon = np.array(points)
                points_polygon = shapely.Polygon(polygon)
                contour_polygon = shapely.Polygon(contour_convex_hull)

                intersection_area = points_polygon.intersection(contour_polygon).area
                union_area = contour_polygon.area + points_polygon.area - intersection_area
                score = intersection_area / union_area

                if best_score is None:
                    best_score = score
                    best_contour = contour
                    best_points = points
                elif best_score < score:
                    best_score = score
                    best_contour = contour
                    best_points = points
            except:
                continue

    if best_contour is None:
        return None

    return best_contour, best_points, best_score


def edge_detection_colored(image_luv):
    sobel_x = cv2.Sobel(image_luv, cv2.CV_64F, 1, 0, ksize=7)
    sobel_y = cv2.Sobel(image_luv, cv2.CV_64F, 0, 1, ksize=7)
    gradients = np.max(np.hypot(sobel_x, sobel_y), axis=-1)
    max_gradient = np.max(gradients)
    min_gradient = np.min(gradients)
    gradients = (gradients - min_gradient) / (max_gradient - min_gradient) * 255
    return gradients


def edge_detection_gray(image_gray):
    sobel_x = cv2.Sobel(image_gray, cv2.CV_64F, 1, 0, ksize=7)
    sobel_y = cv2.Sobel(image_gray, cv2.CV_64F, 0, 1, ksize=7)
    gradients = np.hypot(sobel_x, sobel_y)
    max_gradient = np.max(gradients)
    min_gradient = np.min(gradients)
    gradients = (gradients - min_gradient) / (max_gradient - min_gradient) * 255
    return gradients


def get_two_points_on_line(image_width, image_height, line_parameters):
    a, b = line_parameters
    if abs(a - 90.0) < 1e-7:
        point1 = (b, 0)
        point2 = (b, image_height)
    else:
        slope = math.tan(math.radians(a))
        point1 = (0, b)
        point2 = (image_width, image_width * slope + b)
    return point1, point2


def find_closest_point_from(reference_point, points):
    distances = np.array([reference_point], np.float32) - np.array(points, np.float32)
    distances = np.sum(np.square(distances), axis=-1)
    min_distance_index = np.argmin(distances)
    return distances[min_distance_index], min_distance_index


def merge_similar_line_parameters(parameter_histogram, angle_difference_threshold, intercept_difference_threshold):
    current_label = 0
    labels_count = 0
    parameters = [*parameter_histogram]
    parameters_labels = dict()

    while len(parameters) > 0:
        (angle, intercept) = parameters.pop(0)
        if (angle, intercept) in parameters_labels:
            continue
        parameters_labels[(angle, intercept)] = current_label
        queue = [(angle, intercept)]
        while len(queue) > 0:
            current_angle, current_intercept = queue.pop(0)
            for i in range(len(parameters)):
                angle, intercept = parameters[i]
                if (angle, intercept) not in parameters_labels and \
                        abs(angle - current_angle) < angle_difference_threshold and \
                        abs(intercept - current_intercept) < intercept_difference_threshold:
                    parameters_labels[(angle, intercept)] = current_label
                    queue.append((angle, intercept))
        current_label += 1
        labels_count += 1

    parameters = [*parameter_histogram]
    new_parameter_histogram = dict()
    while len(parameters) > 0:
        current_parameter = parameters.pop(0)
        current_label = parameters_labels[current_parameter]
        total_count = parameter_histogram[current_parameter]
        weighted_sum = np.array(current_parameter, np.float32) * total_count
        indices_to_remove = []
        for i in range(len(parameters)):
            parameter = parameters[i]
            if parameters_labels[parameter] == current_label:
                count = parameter_histogram[parameter]
                weighted_sum += np.array(parameter, np.float32) * count
                total_count += count
                indices_to_remove.append(i)
        weighted_sum /= total_count
        angle = round(float(weighted_sum[0]), 2)
        intercept = round(float(weighted_sum[1]), 2)
        new_parameter_histogram[(angle, intercept)] = total_count
        while len(indices_to_remove) > 0:
            parameters.pop(indices_to_remove.pop(-1))

    return new_parameter_histogram


def sort_points_anticlockwise(points):
    origin = compute_center_of_gravity(points)
    point_angle_pairs = [[points[i], compute_angle(origin, points[i])]
                         for i in range(len(points))]
    point_angle_pairs.sort(key=lambda x: x[1])
    return np.array([p[0] for p in point_angle_pairs])


def sort_points_clockwise(points):
    origin = compute_center_of_gravity(points)
    point_angle_pairs = [[points[i], compute_angle(origin, points[i])]
                         for i in range(len(points))]
    point_angle_pairs.sort(key=lambda x: x[1], reverse=True)
    return np.array([p[0] for p in point_angle_pairs])


def find_best_four_line_parameters(scored_parameters,
                                   contour,
                                   image_width,
                                   image_height):
    chosen_parameters = [scored_parameters[0][1]]
    best_score = None
    best_choices = None
    best_intersection_points = None

    contour_hull = cv2.convexHull(contour).reshape(-1, 2)
    contour_polygon = shapely.Polygon(contour_hull)
    contour_polygon_area = contour_polygon.area

    line_points = []
    for i in range(len(scored_parameters)):
        line_point1, line_point2 = get_two_points_on_line(image_width, image_height, scored_parameters[i][1])
        line_points.append((line_point1, line_point2))

    line1_point1, line1_point2 = line_points[0]
    intersection_cache = dict()
    distance_contour_index_cache = dict()

    for i, j, k in itertools.combinations(range(1, len(scored_parameters)), 3):
        line2_point1, line2_point2 = line_points[i]
        line3_point1, line3_point2 = line_points[j]
        line4_point1, line4_point2 = line_points[k]

        intersection_points = []
        for line_points1, line_points2 in itertools.combinations([(line1_point1, line1_point2, 0),
                                                                  (line2_point1, line2_point2, i),
                                                                  (line3_point1, line3_point2, j),
                                                                  (line4_point1, line4_point2, k)], 2):
            key = (line_points1[-1], line_points2[-1])
            if key in intersection_cache:
                intersection_point = intersection_cache[key]
            else:
                intersection_point = get_lines_intersection_point(line_points1[0],
                                                                  line_points1[1],
                                                                  line_points2[0],
                                                                  line_points2[1])
                intersection_cache[(line_points1[-1], line_points2[-1])] = intersection_point
                intersection_cache[(line_points2[-1], line_points1[-1])] = intersection_point

            if intersection_point is None:
                continue
            x, y = intersection_point
            if x < 0 or x >= image_width or y < 0 or y >= image_height:
                continue

            if key in distance_contour_index_cache:
                distance, contour_point_index = distance_contour_index_cache[key]
            else:
                distance, contour_point_index = find_closest_point_from(intersection_point, contour)
                distance_contour_index_cache[(line_points1[-1], line_points2[-1])] = (distance, contour_point_index)
                distance_contour_index_cache[(line_points2[-1], line_points1[-1])] = (contour_point_index, distance)
            intersection_points.append((intersection_point, distance, contour_point_index))

        if len(intersection_points) < 4:
            continue

        intersection_points.sort(key=lambda x: x[1])
        intersection_points = intersection_points[:4]
        intersection_points = [point for point, _, _ in intersection_points]

        intersection_points = sort_points_anticlockwise(intersection_points)
        intersection_points = np.round(intersection_points).astype(np.int32)
        score = scored_parameters[i][0] + scored_parameters[j][0] + scored_parameters[k][0]

        try:
            polygon = shapely.Polygon(intersection_points)
            intersection_area = polygon.intersection(contour_polygon).area
            union_area = contour_polygon_area + polygon.area - intersection_area
            iou = intersection_area / union_area
        except:
            iou = None

        if iou is None:
            continue

        score *= iou
        if best_score is None or best_score < score:
            best_score = score
            best_choices = (i, j, k)
            best_intersection_points = intersection_points

    if best_choices is None:
        return None
    i, j, k = best_choices
    chosen_parameters.extend([scored_parameters[i][1],
                              scored_parameters[j][1],
                              scored_parameters[k][1]])

    return chosen_parameters, best_intersection_points


def find_lines_by_hough_transform(image, contour):
    skipping_length = 50
    parameter_histogram_threshold = 10
    angle_difference_threshold = 1
    intercept_difference_threshold = 2

    if len(contour) < skipping_length * 2 + 1:
        return None

    contour = np.flip(contour, axis=0)

    next_points = contour[skipping_length:, :]
    previous_points = contour[:-skipping_length:, :]
    deltas = next_points - previous_points

    slopes = deltas[:, 1] / (deltas[:, 0] + 1e-7)
    intercepts = previous_points[:, 1] - slopes * previous_points[:, 0]

    slopes = np.expand_dims(slopes, axis=-1)
    intercepts = np.expand_dims(intercepts, axis=-1)
    parameters = np.concatenate([slopes, intercepts], axis=-1)

    parameter_histogram = dict()
    for i in range(len(parameters)):
        delta = abs(deltas[i, 0])
        if delta < 1e-7:
            angle = 90.0
            intercept = round(float((previous_points[i, 0] + next_points[i, 0]) / 2), 2)
        else:
            a, b = parameters[i]
            angle = round(float(slope_to_angle(a)), 2)
            intercept = round(float(b), 2)
        key = (angle, intercept)
        if key not in parameter_histogram:
            parameter_histogram[key] = 0
        parameter_histogram[key] += 1

    parameter_histogram = merge_similar_line_parameters(parameter_histogram,
                                                        angle_difference_threshold,
                                                        intercept_difference_threshold)

    filtered_parameters = []
    for a, b in parameter_histogram:
        if parameter_histogram[a, b] > parameter_histogram_threshold:
            filtered_parameters.append((a, b))

    if len(filtered_parameters) < 4:
        return None

    image_width = image.shape[1]
    image_height = image.shape[0]

    histogram_values = list(parameter_histogram.values())
    max_histogram_value = np.max(histogram_values)

    scores = []
    for a, b in filtered_parameters:
        histogram_score = parameter_histogram[(a, b)] / max_histogram_value
        scores.append(histogram_score)

    scored_parameters = list(zip(scores, filtered_parameters))
    scored_parameters.sort(key=lambda x: x[0], reverse=True)
    chosen_parameters, intersection_points = find_best_four_line_parameters(scored_parameters,
                                                                            contour,
                                                                            image_width,
                                                                            image_height)
    lines = []
    converted_parameters = []
    final_intersection_points = []
    for i in range(4):
        a, b = chosen_parameters[i]
        converted_parameters.append((a, b))
        if abs(a - 90.0) < 1e-7:
            x1 = int(round(b))
            x2 = int(round(b))
            y1 = 0
            y2 = image_height
        else:
            slope = math.tan(math.radians(a))
            x1 = 0
            x2 = image_width
            y1 = int(round(slope * x1 + b))
            y2 = int(round(slope * x2 + b))
        lines.append(((x1, y1), (x2, y2)))

        x, y = intersection_points[i]
        x = int(round(x))
        y = int(round(y))
        final_intersection_points.append((x, y))

    return lines, final_intersection_points, converted_parameters


def morphological_gradient(image_luv):
    image_l = image_luv[:, :, 0]
    image_u = image_luv[:, :, 1]
    image_v = image_luv[:, :, 2]

    kernel = np.ones((5, 5), np.uint8)
    edges_l = cv2.dilate(image_l, kernel).astype(np.float32) - cv2.erode(image_l, kernel).astype(np.float32)
    edges_u = cv2.dilate(image_u, kernel).astype(np.float32) - cv2.erode(image_u, kernel).astype(np.float32)
    edges_v = cv2.dilate(image_v, kernel).astype(np.float32) - cv2.erode(image_v, kernel).astype(np.float32)

    gradients = np.abs(edges_l) + np.abs(edges_u) + np.abs(edges_v)

    min_gradients = np.min(gradients)
    max_gradients = np.max(gradients)
    gradients = (gradients - min_gradients) / (max_gradients - min_gradients) * 255

    gradients[gradients >= 255] = 255
    gradients[gradients < 0] = 0
    return gradients.astype(np.uint8)


def sobel_gradient(image_luv):
    sobel_x = cv2.Sobel(image_luv, cv2.CV_64F, 1, 0, ksize=7)
    sobel_y = cv2.Sobel(image_luv, cv2.CV_64F, 0, 1, ksize=7)
    gradients = np.sum(np.hypot(sobel_x, sobel_y), axis=-1)

    min_gradients = np.min(gradients)
    max_gradients = np.max(gradients)
    gradients = (gradients - min_gradients) / (max_gradients - min_gradients) * 255

    gradients[gradients >= 255] = 255
    gradients[gradients < 0] = 0
    return gradients.astype(np.uint8)


def auto_correct_vertices_order(vertices):
    point1, point2, point3, point4 = vertices
    side1_length = euclidean_distance(point1, point2)
    corresponding_side1_length = euclidean_distance(point4, point3)
    side2_length = euclidean_distance(point2, point3)
    corresponding_side2_length = euclidean_distance(point1, point4)
    if max(side1_length, corresponding_side1_length) < min(side2_length, corresponding_side2_length):
        point1, point2, point3, point4 = point2, point3, point4, point1
    point1, point2, point3, point4 = point3, point4, point1, point2
    return np.array([point1,
                     point2,
                     point3,
                     point4])


def triangle_area(point1,
                  point2,
                  point3):
    x1, y1 = point1
    x2, y2 = point2
    x3, y3 = point3
    return math.fabs(0.5 * (x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)))


def calculate_4_polygon_area(point1,
                             point2,
                             point3,
                             point4):
    return triangle_area(point1, point2, point3) + \
        triangle_area(point1, point4, point3)


def estimate_rectangle_width_height_from_vertices(vertices,
                                                  id_card_width_height_ratio):
    area = calculate_4_polygon_area(*vertices)
    height = math.sqrt(area / id_card_width_height_ratio)
    width = id_card_width_height_ratio * height
    return int(math.floor(width)), int(math.floor(height))


def warp_perspective(image,
                     original_points,
                     target_points,
                     target_width,
                     target_height):
    original_points = np.float32(original_points)
    target_points = np.float32(target_points)
    perspective_transform_matrix = cv2.getPerspectiveTransform(original_points, target_points)
    return cv2.warpPerspective(image,
                               perspective_transform_matrix,
                               (target_width, target_height),
                               flags=cv2.INTER_CUBIC)


def correct_rectangle_perspective(image,
                                  warped_vertices,
                                  rectangle_width_height_ratio=None):
    if rectangle_width_height_ratio is not None:
        target_width, target_height = estimate_rectangle_width_height_from_vertices(warped_vertices,
                                                                                    rectangle_width_height_ratio)
    else:
        point1, point2, point3, point4 = sort_points_clockwise(warped_vertices)
        side1_length = euclidean_distance(point1, point2)
        corresponding_side1_length = euclidean_distance(point4, point3)
        side2_length = euclidean_distance(point2, point3)
        corresponding_side2_length = euclidean_distance(point1, point4)
        target_width = max(side1_length, corresponding_side1_length)
        target_height = max(side2_length, corresponding_side2_length)
        if target_width < target_height:
            target_width, target_height = target_height, target_width
        target_width = int(round(target_width))
        target_height = int(round(target_height))

    target_rectangle_vertices = [(0, 0),
                                 (target_width - 1, 0),
                                 (target_width - 1, target_height - 1),
                                 (0, target_height - 1)]
    return warp_perspective(image,
                            warped_vertices,
                            target_rectangle_vertices,
                            target_width,
                            target_height)


def find_closest_points_indices(target_points, source_points):
    x = np.expand_dims(target_points, axis=0)
    source_points_array = np.array(source_points, np.float32)
    y = np.expand_dims(np.array(source_points_array, np.float32), axis=1)
    squared_distances = np.sum(np.square(x - y), axis=-1)
    closest_indices = np.argmin(squared_distances, axis=-1)
    return closest_indices


def get_contour_segments(contour,
                         vertices_indices):
    segments = []
    for i in range(len(vertices_indices)):
        current_i = vertices_indices[i]
        next_i = vertices_indices[(i + 1) % len(vertices_indices)]
        if current_i < next_i:
            segment = np.concatenate([contour[next_i:], contour[:current_i]], axis=0)
        else:
            segment = contour[next_i:current_i]
        segments.append(segment)
    return segments


def linearity_error(segments,
                    vertices):
    total_error = 0
    n = 0
    for i in range(vertices.shape[0]):
        current_vertex = vertices[i]
        next_vertex = vertices[(i + 1) % vertices.shape[0]]
        segment = segments[i]

        denominator = euclidean_distance(current_vertex, next_vertex)
        if denominator < EPSILON:
            distances = np.sqrt(np.sum(np.square(segment - np.array([current_vertex])), axis=-1))
            total_error += np.sum(distances)
        else:
            x1, y1 = current_vertex
            x2, y2 = next_vertex
            x0 = segment[:, 0]
            y0 = segment[:, 1]
            distances = np.abs((x2 - x1) * (y1 - y0) - (x1 - x0) * (y2 - y1))
            total_error += np.sum(distances) / denominator
        n += segment.shape[0]
    return total_error / n


def calculate_contour_segments_linearity_error(contour, vertices):
    vertices = sort_points_anticlockwise(vertices)
    closest_contour_points_indices = find_closest_points_indices(contour,
                                                                 vertices)
    segments = get_contour_segments(contour,
                                    closest_contour_points_indices)
    return linearity_error(segments, vertices)


def dewarp_image(image,
                 rectangle_width_height_ratio=2.4):
    original_image = np.copy(image)
    image = cv2.morphologyEx(image,
                             cv2.MORPH_CLOSE,
                             cv2.getStructuringElement(cv2.MORPH_RECT, (21, 21)))

    max_side_length = np.max(image.shape)
    resize_factor = EXTRACTION_MAX_SIDE_LENGTH / max_side_length
    image = cv2.resize(image, None, fx=resize_factor, fy=resize_factor, interpolation=cv2.INTER_CUBIC)
    image = cv2.medianBlur(image, 7)

    image_luv = cv2.cvtColor(image, cv2.COLOR_BGR2LUV)
    image_l = image_luv[:, :, 0]

    colored_gradients = edge_detection_colored(image_luv[:, :, 1:])
    gray_gradients = edge_detection_gray(image_l)

    gradients = (colored_gradients + gray_gradients) / 2
    gradients[gradients >= 255] = 255
    gradients[gradients < 0] = 0
    gradients = gradients.astype(np.uint8)

    padding_size = 50
    new_image = np.zeros((gradients.shape[0] + padding_size * 2, gradients.shape[1] + padding_size * 2),
                         np.uint8)
    new_image[padding_size:-padding_size, padding_size:-padding_size] = gradients
    gradients = new_image

    otsu_threshold_value = cv2.threshold(gradients[gradients > 0],
                                         0,
                                         255,
                                         cv2.THRESH_OTSU | cv2.THRESH_BINARY)[0]
    otsu_threshold_value = int(otsu_threshold_value)
    high_threshold_max = np.mean(gradients[gradients >= otsu_threshold_value])
    high_threshold_max = int(math.ceil(high_threshold_max))
    high_threshold_min = max(50, otsu_threshold_value)
    high_threshold_max = max(high_threshold_max, high_threshold_min)

    min_area = 0.1 * np.prod(gradients.shape)

    best_score = None
    best_result = None

    is_done = False
    found_good_high_threshold = False
    high_thresholds = list(range(high_threshold_max, high_threshold_min - 1, -5))
    if high_threshold_min not in high_thresholds:
        high_thresholds.append(high_threshold_min)

    for high_threshold in high_thresholds:
        strong_edges = threshold_image(gradients, high_threshold)
        low_thresholds = list(range(high_threshold - 1, 4, -2))
        if 4 not in low_thresholds:
            low_thresholds.append(4)
        for low_threshold in low_thresholds:
            weak_edges = double_threshold_image(gradients, high_threshold, low_threshold)
            edges = perform_edge_hysteresis(strong_edges, weak_edges)
            result = find_best_closed_contour(edges,
                                              min_area)

            if result is not None:
                score = result[-1]
                if score > 0.6:
                    found_good_high_threshold = True

                if best_score is None:
                    best_score = score
                    best_result = result
                elif best_score < score:
                    best_score = score
                    best_result = result

                if score > 0.9:
                    is_done = True
                    break

        if is_done or found_good_high_threshold:
            break

    if best_result is None:
        return None

    best_contour, best_points, best_score = best_result
    contour_image = np.zeros(gradients.shape[:2], np.uint8)
    points_x, points_y = np.transpose(best_contour)
    contour_image[points_y, points_x] = 255
    result = find_lines_by_hough_transform(contour_image, best_contour)

    chosen_points = best_points
    if result is not None:
        try:
            lines, final_intersection_points, converted_parameters = result
            hough_linearity_error = calculate_contour_segments_linearity_error(best_contour,
                                                                               np.array(best_points,
                                                                                        np.float32))

            contour_approximation_linearity_error = calculate_contour_segments_linearity_error(best_contour,
                                                                                               np.array(
                                                                                                   final_intersection_points,
                                                                                                   np.float32))

            if abs(hough_linearity_error - contour_approximation_linearity_error) < 1e-7:
                contour_hull = cv2.convexHull(best_contour).reshape(-1, 2)
                contour_polygon = shapely.Polygon(contour_hull)
                contour_polygon_area = contour_polygon.area

                polygon = shapely.Polygon(final_intersection_points)
                intersection_area = polygon.intersection(contour_polygon).area
                union_area = contour_polygon_area + polygon.area - intersection_area
                hough_score = intersection_area / union_area

                polygon = shapely.Polygon(best_points)
                intersection_area = polygon.intersection(contour_polygon).area
                union_area = contour_polygon_area + polygon.area - intersection_area
                contour_approximation_score = intersection_area / union_area

                if hough_score > contour_approximation_score:
                    chosen_points = final_intersection_points
                else:
                    chosen_points = best_points
            else:
                if hough_linearity_error > contour_approximation_linearity_error:
                    chosen_points = final_intersection_points
                else:
                    chosen_points = best_points
        except:
            chosen_points = best_points

    adjusted_points = []
    for x, y in chosen_points:
        x = (x - padding_size) / resize_factor
        y = (y - padding_size) / resize_factor
        adjusted_points.append((x, y))
    adjusted_points = sort_points_clockwise(adjusted_points)
    adjusted_points = auto_correct_vertices_order(adjusted_points)

    return correct_rectangle_perspective(original_image,
                                         adjusted_points,
                                         rectangle_width_height_ratio)


def filter_connected_components(binary_image,
                                connected_components_stats_filter,
                                stats_with_background_label=False):
    labels_count, labeled_image, stats, centroids = cv2.connectedComponentsWithStats(binary_image, 8, cv2.CV_32S)
    chosen_labels = connected_components_stats_filter(stats)
    chosen_labels = np.where(chosen_labels)[0]
    if not stats_with_background_label:
        chosen_labels += 1
    result_image = np.zeros(binary_image.shape, np.uint8)
    for i in range(chosen_labels.shape[0]):
        label = chosen_labels[i]
        x, y, w, h = stats[label, :4]
        mask = labeled_image[y:y + h, x:x + w] == label
        result_image[y:y + h, x: x + w][mask] = 255
    return result_image


def otsu_threshold(image_gray):
    return cv2.threshold(image_gray,
                         0,
                         255,
                         cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]


def smear_micr_horizontally(gray_image):
    black_hat_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (7, 7))
    black_hat_morphology = cv2.morphologyEx(gray_image, cv2.MORPH_BLACKHAT, black_hat_kernel)

    gradients = cv2.Sobel(black_hat_morphology, cv2.CV_64F, 1, 0, ksize=7)
    gradients = np.abs(gradients)

    min_gradient = np.min(gradients)
    max_gradient = np.max(gradients)
    gradients = (gradients - min_gradient) / (max_gradient - min_gradient) * 255
    gradients[gradients > 255] = 255

    binary_image = otsu_threshold(gradients.astype(np.uint8))
    binary_image = cv2.morphologyEx(binary_image, cv2.MORPH_CLOSE, np.ones((5, 5), np.uint8))
    filter_connected_components(binary_image, lambda stats: stats[1:, -1] > 20)
    binary_image = cv2.morphologyEx(binary_image, cv2.MORPH_CLOSE, np.ones((1, 15), np.uint8))

    binary_image = cv2.erode(binary_image, np.ones((1, 15), np.uint8))
    filter_connected_components(binary_image, lambda stats: stats[1:, -1] > 200)
    binary_image = cv2.dilate(binary_image, np.ones((1, 15), np.uint8))

    return cv2.dilate(binary_image, np.ones((5, 5), np.uint8))


def get_rotated_rect_angle(rotated_area_rect):
    box = cv2.boxPoints(rotated_area_rect)
    box = sort_points_clockwise(box)
    distances = [(euclidean_distance(box[i], box[(i + 1) % 4]), i) for i in range(4)]
    distances.sort(key=lambda x: x[0], reverse=True)
    best_i = distances[0][1]
    point1 = (box[best_i - 1] + box[best_i]) / 2
    point2 = (box[best_i + 1] + box[(best_i + 2) % 4]) / 2
    if point1[0] > point2[0]:
        point1, point2 = point2, point1
    result = compute_angle(point1, point2)
    return result, (point1, point2)


import re

from PIL import Image
from tesserocr import PyTessBaseAPI, PSM, OEM


class TesseractOcrEngine:
    __configured_models = dict()

    def __init__(self):
        pass

    @staticmethod
    def read(image,
             lang='e13b',
             psm=PSM.RAW_LINE,
             oem=OEM.LSTM_ONLY,
             white_list=''):
        if image is None or lang is None or psm is None or oem is None:
            return None

        model_key = (lang, psm, oem)
        if model_key in TesseractOcrEngine.__configured_models:
            model = TesseractOcrEngine.__configured_models[model_key]
        else:
            model = PyTessBaseAPI(lang=lang, psm=psm, oem=oem)
            TesseractOcrEngine.__configured_models[model_key] = model

        model.SetVariable('tessedit_char_whitelist', white_list)
        model.SetImage(Image.fromarray(image))
        return model.GetUTF8Text(), model.MeanTextConf()

    @staticmethod
    def __parse_parameter(config, parameter_regex):
        regex = re.compile(parameter_regex)
        regex_search = regex.search(config)
        if regex_search is None:
            return ''
        return regex_search.group(1)


def extract_micr_field(image):
    max_side_length = np.max(image.shape)
    resize_factor = EXTRACTION_MAX_SIDE_LENGTH / max_side_length
    resized_image = cv2.resize(image,
                               None,
                               fx=resize_factor,
                               fy=resize_factor,
                               interpolation=cv2.INTER_CUBIC)
    offset = int(round(resized_image.shape[0] * 3 / 4))
    resized_image = resized_image[offset:, :]
    image_luv = cv2.cvtColor(resized_image, cv2.COLOR_BGR2LUV)
    image_l = image_luv[:, :, 0]
    image_gray = cv2.GaussianBlur(image_l, (3, 3), 0)
    smeared = smear_micr_horizontally(image_gray)

    contours, hierarchy = cv2.findContours(smeared, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    max_area = -1
    largest_contour = None
    for contour in contours:
        contour_area = cv2.moments(contour)['m00']
        if contour_area > max_area:
            min_area_rect = cv2.minAreaRect(contour)
            angle, (point1, point2) = get_rotated_rect_angle(min_area_rect)
            if angle < MAX_MICR_LINE_DETECTION_ANGLE or 180 - angle < MAX_MICR_LINE_DETECTION_ANGLE:
                max_area = contour_area
                largest_contour = contour, min_area_rect, angle, (point1, point2)

    if largest_contour is None:
        return None

    best_contour, best_min_area_rect, best_angle, (best_point1, best_point2) = largest_contour

    best_x1, best_y1 = best_point1
    best_x2, best_y2 = best_point2

    best_left = min(best_x1, best_x2)
    best_right = max(best_x1, best_x2)

    selected_contours = []
    contour_centers = []
    for contour in contours:
        min_area_rect = cv2.minAreaRect(contour)
        center_x, center_y = min_area_rect[0]
        angle, (point1, point2) = get_rotated_rect_angle(min_area_rect)

        x1, y1 = point1
        x2, y2 = point2
        left = min(x1, x2)
        right = max(x1, x2)

        if (angle < MAX_MICR_LINE_DETECTION_ANGLE or 180 - angle < MAX_MICR_LINE_DETECTION_ANGLE) and \
                (right < best_left or left > best_right):
            selected_contours.append((contour, min_area_rect))
            contour_centers.append((center_x, center_y))

    if len(selected_contours) > 0:
        contour_centers = np.array(contour_centers, np.float32)
        denominator = euclidean_distance(best_point1, best_point2)
        x1, y1 = best_point1
        x2, y2 = best_point2
        x0 = contour_centers[:, 0]
        y0 = contour_centers[:, 1]
        distances = np.abs((x2 - x1) * (y1 - y0) - (x1 - x0) * (y2 - y1)) / denominator
        closest_contour_indices = np.where(distances < MAX_MICR_LINE_PART_DISTANCE_FROM_CENTER_LINE)[0].ravel()
        micr_contours = []
        for i in closest_contour_indices:
            micr_contours.append(selected_contours[i][0])
        micr_contours.append(best_contour)
        micr_contours = np.concatenate(micr_contours, axis=0)
    else:
        micr_contours = best_contour

    total_min_area_react = cv2.minAreaRect(micr_contours)
    (center_x, center_y), (width, height), angle = total_min_area_react
    width += 5
    height += 5
    total_min_area_react = ((center_x, center_y), (width, height), angle)
    box_points = np.array(cv2.boxPoints(total_min_area_react), int)
    adjusted_points = sort_points_clockwise(box_points)
    adjusted_points = auto_correct_vertices_order(adjusted_points).astype(np.float32)
    adjusted_points[:, 1] += offset
    adjusted_points /= resize_factor
    corrected_field_image = correct_rectangle_perspective(image,
                                                          adjusted_points,
                                                          None)
    return cv2.cvtColor(corrected_field_image, cv2.COLOR_BGR2RGB)


def calculate_micr_score(micr_line_image):
    black_hat_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (21, 21))
    image_luv = cv2.cvtColor(micr_line_image, cv2.COLOR_BGR2LUV)
    image_l = image_luv[:, :, 0]
    black_hat_morphology = cv2.morphologyEx(image_l, cv2.MORPH_BLACKHAT, black_hat_kernel)
    binary_image = otsu_threshold(black_hat_morphology)
    binary_image = cv2.dilate(binary_image, np.ones((5, 5), np.uint8))
    labels_count, labeled_image, stats, centroids = cv2.connectedComponentsWithStats(binary_image, 8,
                                                                                     cv2.CV_32S)
    for i in range(1, len(stats)):
        x, y, w, h, _ = stats[i]
        if h > binary_image.shape[0] // 2:
            binary_image[0:binary_image.shape[0], x:x + w] = 255
    return np.count_nonzero(binary_image) / (binary_image.shape[0] * binary_image.shape[1])


def pre_process_text_field(extracted_field_image):
    resized = cv2.resize(extracted_field_image, None, fx=0.5, fy=0.5, interpolation=cv2.INTER_CUBIC)
    image_luv = cv2.cvtColor(resized, cv2.COLOR_BGR2LUV)
    image_l = image_luv[:, :, 0]
    black_hat_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (21, 21))
    black_hat_morphology = cv2.morphologyEx(image_l, cv2.MORPH_BLACKHAT, black_hat_kernel)
    binary_image = 255 - otsu_threshold(black_hat_morphology)
    return cv2.GaussianBlur(binary_image, (3, 3), 0)


def main():
    matplotlib.use('Agg')
    # base_dir = '/home/u764/Downloads/eKYc/docs/cheque-dewarping/input-cheques'
    # out_dir = '/home/u764/Downloads/out-cheques'
    # if not os.path.isdir(out_dir):
    #     os.makedirs(out_dir)
    # progress_bar = tqdm(total=len(os.listdir(base_dir)))
    # for file in os.listdir(base_dir):
    #     # if file != '20230611_083253930_iOS.heic.png':
    #     #     continue
    #     try:
    #         image = cv2.imread(os.path.join(base_dir, file))
    #         corrected_image = dewarp_image(image)
    #         if corrected_image is None:
    #             progress_bar.update()
    #             continue
    #         cv2.imwrite(os.path.join(out_dir, file) + '_dewarped.png', corrected_image)
    #         progress_bar.update()
    #     except FileNotFoundError:
    #         progress_bar.update()
    #         continue
    # progress_bar.close()

    # image = cv2.imread('/home/u764/Downloads/cheque-scanning-instructions/bad contrast.jpg')
    # corrected_image = None
    # import time
    # start_time = time.time()
    # for i in range(100):
    # corrected_image = dewarp_image(image)

    base_dir = '/home/u764/Downloads/cheques'
    out_dir = '/home/u764/Downloads/out-cheques'
    # labels_dir = '/home/u764/Downloads/labels'

    # base_dir = '/home/u764/Downloads/eKYc/docs/cheque-dewarping/input-cheques'
    # out_dir = '/home/u764/Downloads/out-cheques-all'

    labels = ['C009388C0A634001001AC0011660664001C',
              'C149195C0A634004001AC0560332100022619C',
              'C000017C24D01A0011010024841C01',
              'C000106C03D0390A0130101500414701C',
              'C110335C06D50A0001097049201C01',
              'A063107513A1010052447406C1249',
              'C710029C02D02A0100000048671C01',
              'C125645C31D1010A01901050187C',
              'C001658A013300000AC0000002354C',
              'C3A000140145A0120031492C23302591B0000000901B']

    levenshtein_distance = DamerauLevenshtein()

    if not os.path.isdir(out_dir):
        os.makedirs(out_dir)
    file_names = os.listdir(base_dir)
    error_rate = 0
    total_cheques = len(file_names)
    not_read_correctly = []
    progress_bar = tqdm(total=total_cheques)
    for f in file_names:
        path = os.path.join(base_dir, f)
        original_image = cv2.imread(path)
        corrected_image = dewarp_image(original_image)
        if corrected_image is None:
            error_rate += 1
            not_read_correctly.append(f)
            progress_bar.update()
            continue

        extracted_field_image1 = extract_micr_field(corrected_image)
        # print(width_height_ratio, width_ratio)
        if extracted_field_image1 is not None:
            width_height_ratio1 = extracted_field_image1.shape[0] / extracted_field_image1.shape[1]
            width_ratio1 = extracted_field_image1.shape[1] / corrected_image.shape[1]
            if width_height_ratio1 <= MICR_LINE_MAX_WIDTH_TO_HEIGHT_RATIO and \
                    width_ratio1 >= MICR_LINE_MIN_WIDTH_TO_IMAGE_WIDTH_RATIO:
                pre_processed = pre_process_text_field(extracted_field_image1)
                ocr_text1, ocr_confidence1 = TesseractOcrEngine.read(pre_processed)
                # ocr_text1, ocr_confidence1 = TesseractOcrEngine.read(extracted_field_image1)
                score1 = calculate_micr_score(extracted_field_image1)
                score1 *= ocr_confidence1 / 100
                extracted_field_image1 = pre_processed
            else:
                ocr_text1 = None
                score1 = None
                extracted_field_image1 = None
        else:
            ocr_text1 = None
            score1 = None
            extracted_field_image1 = None

        rotated_image = cv2.rotate(corrected_image, cv2.ROTATE_180)
        extracted_field_image2 = extract_micr_field(rotated_image)
        if extracted_field_image2 is not None:
            width_height_ratio2 = extracted_field_image2.shape[0] / extracted_field_image2.shape[1]
            width_ratio2 = extracted_field_image2.shape[1] / corrected_image.shape[1]
            if width_height_ratio2 <= MICR_LINE_MAX_WIDTH_TO_HEIGHT_RATIO and \
                    width_ratio2 >= MICR_LINE_MIN_WIDTH_TO_IMAGE_WIDTH_RATIO:
                pre_processed = pre_process_text_field(extracted_field_image2)
                ocr_text2, ocr_confidence2 = TesseractOcrEngine.read(pre_processed)
                # ocr_text2, ocr_confidence2 = TesseractOcrEngine.read(extracted_field_image2)
                score2 = calculate_micr_score(extracted_field_image2)
                score2 *= ocr_confidence2 / 100
                extracted_field_image2 = pre_processed
            else:
                ocr_text2 = None
                score2 = None
                extracted_field_image2 = None
        else:
            ocr_text2 = None
            score2 = None
            extracted_field_image2 = None

        if score1 is None and score2 is None:
            error_rate += 1
            not_read_correctly.append(f)
            progress_bar.update()
            continue
        else:
            if score1 is None:
                ocr_text = ocr_text2
                extracted_field_image = extracted_field_image2
                corrected_image = rotated_image
            elif score2 is None:
                ocr_text = ocr_text1
                extracted_field_image = extracted_field_image1
            elif score1 < score2:
                ocr_text = ocr_text2
                corrected_image = rotated_image
                extracted_field_image = extracted_field_image2
            else:
                ocr_text = ocr_text1
                extracted_field_image = extracted_field_image1

            ocr_text = ocr_text.strip().upper()
            ocr_text = ''.join([c for c in ocr_text if c in MICR_CHARACTERS])

            # with open(f'{os.path.join(labels_dir, f)}.txt', 'r') as file:
            #     label = file.read()
            # label = label.strip().upper()

            label = labels[0]
            min_distance = levenshtein_distance(label, ocr_text)
            for i in range(1, len(labels)):
                distance = levenshtein_distance(labels[i], ocr_text)
                if min_distance > distance:
                    min_distance = distance
                    label = labels[i]

            correctness = 'correct'
            if label != ocr_text:
                error_rate += 1
                not_read_correctly.append(f)
                correctness = 'not correct'

            fig, axs = plt.subplots(2, 1)
            fig.set_figheight(12)
            fig.set_figwidth(12)
            axs[0].set_title('de-warped', fontweight='bold', size=20)
            axs[0].imshow(cv2.cvtColor(corrected_image, cv2.COLOR_BGR2RGB))
            axs[1].set_title(f'extracted micr: {ocr_text}\n{correctness}', fontweight='bold', size=20)
            axs[1].imshow(extracted_field_image, plt.cm.gray)
            output_path = os.path.join(out_dir, f)
            plt.savefig(output_path, dpi=200)
            plt.clf()
            plt.close('all')
        progress_bar.update()
    progress_bar.close()
    print(f'error rate: {round(error_rate / total_cheques * 100, 2)}%')
    print(f'incorrectly read cheques: {error_rate}')
    for f in not_read_correctly:
        print(f)


def main2():
    matplotlib.use('TkAgg')
    image = cv2.imread('/home/u764/Downloads/midv500_data/midv500/16_deu_passport_new/images/HA/HA16_02.tif')

    extracted = dewarp_image(image, None)
    plt.imshow(cv2.cvtColor(extracted, cv2.COLOR_BGR2RGB))
    plt.show()


def main3():
    base_dir = '/home/u764/Downloads/eKYc/docs/test-images'
    out_dir = '/home/u764/Downloads/out-test'
    if not os.path.isdir(out_dir):
        os.makedirs(out_dir)
    progress_bar = tqdm(total=len(os.listdir(base_dir)))
    for file in os.listdir(base_dir):
        try:
            image = cv2.imread(os.path.join(base_dir, file))
            corrected_image = dewarp_image(image, None)
            if corrected_image is None:
                progress_bar.update()
                continue
            cv2.imwrite(os.path.join(out_dir, file), corrected_image)
            progress_bar.update()
        except:
            progress_bar.update()
            continue
    progress_bar.close()


if __name__ == '__main__':
    main2()
