import math

import cv2
import matplotlib.pyplot as plt
import numpy as np

# Do a binary search on the gradients to find a suitable threshold
# We are trying to find the maximum threshold that creates a contour that is closest to a 4-polygon after we perform edge hysteresis
# We might do a closing operation in the beginning to remove text so we are left with only the background
# Should we perform a blurring/smoothing operation?
EPSILON = 1e-6


def threshold_image(image, threshold):
    image_copy = np.copy(image)
    image_copy[image_copy >= threshold] = 255
    image_copy[image_copy < threshold] = 0
    return image_copy


def threshold_image_weak(image, high_threshold, low_threshold):
    image_copy = np.copy(image)
    image_copy[image_copy >= high_threshold] = 0
    image_copy[image_copy >= low_threshold] = 255
    image_copy[image_copy < low_threshold] = 0
    return image_copy


def perform_edge_hysteresis(strong_edges, weak_edges):
    mask = weak_edges | strong_edges
    result = strong_edges
    while True:
        new_strong_edges = cv2.dilate(result, np.ones((3, 3), np.uint8))
        new_strong_edges = (new_strong_edges & mask)
        if np.all(new_strong_edges == result):
            break
        result = new_strong_edges
    return result


def find_best_four_polygon_lines(image, contour):
    skipping_length = 50
    slope_rescaling_factor = 100
    angle_difference_threshold = 5
    parameter_histogram_threshold = 5
    intercept_difference_threshold = 100

    if len(contour) < skipping_length * 2 + 1:
        return None

    contour = np.flip(contour, axis=0)

    next_points = contour[skipping_length:, :]
    previous_points = contour[:-skipping_length:, :]
    deltas = next_points - previous_points

    slopes = deltas[:, 1] / (deltas[:, 0] + 1e-7)
    max_slope = image.shape[0]
    slopes[np.abs(deltas[:, 0]) < 1e-7] = max_slope
    slopes[slopes > max_slope] = max_slope
    slopes[slopes < -max_slope] = -max_slope
    intercepts = previous_points[:, 1] - slopes * previous_points[:, 0]

    slopes = np.expand_dims(slopes, axis=-1)
    intercepts = np.expand_dims(intercepts, axis=-1)
    parameters = np.concatenate([slopes, intercepts], axis=-1)

    contour_image = np.zeros(image.shape[:2], np.uint8)
    points_x, points_y = np.transpose(contour)
    contour_image[points_y, points_x] = 255
    contour_image = cv2.dilate(contour_image, np.ones((3, 3), np.uint8))

    parameter_histogram = dict()
    for a, b in parameters:
        angle = int(round(a * slope_rescaling_factor))
        intercept = int(round(b))
        key = (angle, intercept)
        if key not in parameter_histogram:
            parameter_histogram[key] = 0
        parameter_histogram[key] += 1

    filtered_parameters = []
    for a, b in parameter_histogram:
        if parameter_histogram[a, b] > parameter_histogram_threshold:
            filtered_parameters.append((a, b))

    if len(filtered_parameters) < 4:
        return None

    contour_pixels = np.count_nonzero(contour_image)
    scores = []
    for a, b in filtered_parameters:
        a = a / slope_rescaling_factor
        x1 = 0
        x2 = image.shape[1]
        y1 = int(a * x1 + b)
        y2 = int(a * x2 + b)
        line_image = np.zeros(contour_image.shape, np.uint8)
        cv2.line(line_image, (x1, y1), (x2, y2), 255, 1)
        score = np.count_nonzero(line_image & contour_image) / contour_pixels
        scores.append(score)

    best_line_i = np.argmax(scores)
    best_a, best_b = filtered_parameters[best_line_i]

    chosen_parameters = [(best_a, best_b)]

    scored_parameters = list(zip(scores, filtered_parameters))
    scored_parameters.sort(key=lambda x: x[0], reverse=True)

    remaining = 3
    for i in range(1, len(scored_parameters)):
        score, (a, b) = scored_parameters[i]
        found_good_parameters = True
        for previous_a, previous_b in chosen_parameters:
            if abs(previous_a - a) <= angle_difference_threshold and \
                    abs(previous_b - b) <= intercept_difference_threshold:
                found_good_parameters = False
                break
        if found_good_parameters:
            chosen_parameters.append((a, b))
            remaining -= 1
            if remaining == 0:
                break

    if len(chosen_parameters) < 4:
        return None

    lines = []
    converted_parameters = []
    # imagergb = np.zeros((image.shape[0], image.shape[1], 3), np.uint8)
    for a, b in chosen_parameters:
        a = a / slope_rescaling_factor
        converted_parameters.append((a, b))
        x1 = 0
        x2 = image.shape[1]
        y1 = int(a * x1 + b)
        y2 = int(a * x2 + b)
        lines.append(((x1, y1), (x2, y2)))

        # cv2.line(imagergb, (x1, y1), (x2, y2), (255, 0, 0), 1)
    # plt.imshow(imagergb, plt.cm.gray)
    # plt.show()

    return lines, converted_parameters


def points_to_parametric_line(point1, point2):
    (x1, y1) = point1
    (x2, y2) = point2
    return np.array([[x2 - x1, x1],
                     [y2 - y1, y1]], np.float32)


def is_parametric_line_vertical(parametric_line):
    a_x, _ = parametric_line[0]
    a_y, _ = parametric_line[1]

    if abs(a_x) <= EPSILON < abs(a_y):
        return True
    else:
        return False


def is_parametric_line_horizontal(parametric_line):
    a_x, _ = parametric_line[0]
    a_y, _ = parametric_line[1]

    if abs(a_y) <= EPSILON < abs(a_x):
        return True
    else:
        return False


def find_lines_intersection_point_from_parametric_line_equations(line1,
                                                                 line2):
    if line1 is None or line2 is None:
        return None

    a1_x, b1_x = line1[0]
    a1_y, b1_y = line1[1]

    a2_x, b2_x = line2[0]
    a2_y, b2_y = line2[1]

    mat1 = np.array([[a1_x, -a2_x],
                     [a1_y, -a2_y]], np.float32)
    if np.linalg.det(mat1) == 0:
        return None

    mat2 = np.array([[b2_x - b1_x],
                     [b2_y - b1_y]], np.float32)

    t = np.dot(np.linalg.inv(mat1), mat2).reshape(2)

    point1 = line1[:, 0] * t[0] + line1[:, 1]
    point2 = line2[:, 0] * t[1] + line2[:, 1]

    return (point1 + point2) / 2


def find_lines_intersection_point(line1, line2):
    line1_point1, line1_point2 = line1
    line2_point1, line2_point2 = line2

    parametric_line1 = points_to_parametric_line(line1_point1,
                                                 line1_point2)
    parametric_line2 = points_to_parametric_line(line2_point1,
                                                 line2_point2)

    if (is_parametric_line_vertical(parametric_line1) and is_parametric_line_vertical(parametric_line2)) or \
            (is_parametric_line_horizontal(parametric_line1) and is_parametric_line_horizontal(parametric_line2)):
        intersection_point = (np.array(line1_point1) + np.array(line2_point2)) / 2
    else:
        intersection_point = find_lines_intersection_point_from_parametric_line_equations(parametric_line1,
                                                                                          parametric_line2)

    if intersection_point is None:
        return None

    return tuple(np.round(intersection_point).astype(int))


def adjust_angle_by_quadrant(angle,
                             point):
    x, y = point
    if x < 0 and y > 0:
        return 180 - angle
    if x < 0 and y < 0:
        return 180 + angle
    if x > 0 and y < 0:
        return 360 - angle
    return angle


def compute_angle(origin,
                  point):
    x1 = point[0] - origin[0]
    y1 = origin[1] - point[1]
    if math.fabs(x1) > 1e-6:
        theta = math.fabs(math.atan(y1 / x1)) / math.pi * 180
    else:
        theta = 90
    return adjust_angle_by_quadrant(theta, (x1, y1))


def compute_center_of_gravity(points):
    points_array = np.array(points, np.float32)
    return np.array([np.mean(points_array[:, :1]), np.mean(points_array[:, 1:])])


def slope_to_angle(slope):
    return (math.degrees(math.atan(slope)) + 180) % 180


def get_four_polygon_points_from_lines(lines, line_parameters):
    if len(lines) != 4 or len(line_parameters) != 4:
        return None

    angles = [slope_to_angle(slope) for slope, intercept in line_parameters]
    angle1 = angles[0]
    closest_angle_index = int(np.argmin(np.abs(np.array(angles[1:]) - angle1))) + 1

    intersection_points = []
    for i in range(1, 4):
        if i == closest_angle_index:
            continue
        point = find_lines_intersection_point(lines[0], lines[i])
        if point is None:
            return None
        intersection_points.append(point)

    for i in range(1, 4):
        if i == closest_angle_index:
            continue
        point = find_lines_intersection_point(lines[closest_angle_index], lines[i])
        if point is None:
            return None
        intersection_points.append(point)

    origin = compute_center_of_gravity(intersection_points)
    sorted_points_anti_clockwise = [(intersection_points[i],
                                     compute_angle(origin, intersection_points[i]))
                                    for i in range(4)]
    sorted_points_anti_clockwise.sort(key=lambda p: p[1])
    sorted_points_anti_clockwise = [p[0] for p in sorted_points_anti_clockwise]

    return sorted_points_anti_clockwise


def find_best_closed_contour(edges,
                             area_threshold):
    contours, hierarchy = cv2.findContours(cv2.dilate(edges, np.ones((3, 3), np.uint8)),
                                           cv2.RETR_EXTERNAL,
                                           cv2.CHAIN_APPROX_NONE)
    best_score = None
    best_contour = None
    best_points = None
    for i in range(len(contours)):
        contour = contours[i]
        contour_area = cv2.moments(contour)['m00']

        if contour_area > area_threshold:
            contour = contour.reshape(-1, 2)

            result = find_best_four_polygon_lines(edges, contour)
            if result is None:
                continue
            lines, parameters = result
            points = get_four_polygon_points_from_lines(lines, parameters)
            if points is None:
                continue
            area = cv2.moments(np.array(points))['m00']
            area_score = min(area, contour_area) / max(area, contour_area)

            contour_image = np.zeros(edges.shape, np.uint8)
            points_x, points_y = np.transpose(contour)
            contour_image[points_y, points_x] = 255

            dilated_contour = cv2.dilate(contour_image, np.ones((3, 3), np.uint8))
            lines_image = np.zeros(contour_image.shape, np.uint8)
            contour_pixels = np.count_nonzero(dilated_contour)
            for (x1, y1), (x2, y2) in lines:
                cv2.line(lines_image, (x1, y1), (x2, y2), 255, 1)
            contour_score = np.count_nonzero(lines_image & dilated_contour) / contour_pixels

            score = contour_score * area_score
            print('score: ', contour_score, area_score, score)
            if best_score is None:
                best_score = score
                best_contour = contour
                best_points = points
            elif best_score < score:
                best_score = score
                best_contour = contour
                best_points = points

    if best_contour is None:
        return None

    return best_contour, best_points, best_score


def find_best_hysteresis(gradients, strong_edges, high_threshold):
    min_area = 0.2 * np.prod(gradients.shape)

    b = high_threshold - 1
    a = 1

    edges_b = perform_edge_hysteresis(strong_edges, threshold_image_weak(gradients,
                                                                         high_threshold,
                                                                         b))

    result_b = find_best_closed_contour(edges_b,
                                        min_area)
    if result_b is not None:
        return edges_b

    edges_a = perform_edge_hysteresis(strong_edges, threshold_image_weak(gradients,
                                                                         high_threshold,
                                                                         a))

    result_a = find_best_closed_contour(edges_a,
                                        min_area)
    if result_a is None:
        return None

    result = edges_a
    while (b - a) > 1:
        thresh = (a + b) // 2
        edges_thresh = perform_edge_hysteresis(strong_edges, threshold_image_weak(gradients,
                                                                                  high_threshold,
                                                                                  thresh))

        result_thresh = find_best_closed_contour(edges_thresh,
                                                 min_area)
        if result_thresh is not None:
            a = thresh
            result = edges_thresh
        else:
            b = thresh
            result = edges_thresh

    return result


def find_best_edges(gradients):
    b = 254
    a = 2

    strong_edges = threshold_image(gradients, b)
    if not np.all(strong_edges == 0):
        edges_b = find_best_hysteresis(gradients, strong_edges, b)
    else:
        edges_b = None

    if edges_b is not None:
        return edges_b

    strong_edges = threshold_image(gradients, a)
    if not np.all(strong_edges == 0):
        edges_a = find_best_hysteresis(gradients, strong_edges, a)
    else:
        edges_a = None

    if edges_a is None:
        return None

    result = edges_a
    while (b - a) > 1:
        thresh = (a + b) // 2
        strong_edges = threshold_image(gradients, thresh)
        if not np.all(strong_edges == 0):
            edges_thresh = find_best_hysteresis(gradients, strong_edges, thresh)
        else:
            edges_thresh = None

        if edges_thresh is not None:
            a = thresh
            result = edges_thresh
        else:
            b = thresh
            result = edges_thresh

    return result


def edge_detection_colored(image_luv):
    sobel_x = cv2.Sobel(image_luv[:, :, 1:], cv2.CV_64F, 1, 0, ksize=7)
    sobel_y = cv2.Sobel(image_luv[:, :, 1:], cv2.CV_64F, 0, 1, ksize=7)
    gradients = np.max(np.hypot(sobel_x, sobel_y), axis=-1)
    # gradients = np.power(gradients, 2 / 3)
    max_gradient = np.max(gradients)
    min_gradient = np.min(gradients)
    gradients = (gradients - min_gradient) / (max_gradient - min_gradient) * 255
    return gradients


def edge_detection_gray(image_l):
    sobel_x = cv2.Sobel(image_l, cv2.CV_64F, 1, 0, ksize=7)
    sobel_y = cv2.Sobel(image_l, cv2.CV_64F, 0, 1, ksize=7)
    gradients = np.hypot(sobel_x, sobel_y)
    # gradients = np.power(gradients, 2 / 3)
    max_gradient = np.max(gradients)
    min_gradient = np.min(gradients)
    gradients = (gradients - min_gradient) / (max_gradient - min_gradient) * 255
    return gradients


def main():
    image = cv2.imread('/home/u764/Downloads/eKYc/docs/1686729469831.jpg')
    # analyze_image(image)
    image = cv2.morphologyEx(image,
                             cv2.MORPH_CLOSE,
                             cv2.getStructuringElement(cv2.MORPH_RECT, (21, 21)))

    max_side_length = np.max(image.shape)
    resize_factor = 750 / max_side_length
    image = cv2.resize(image, None, fx=resize_factor, fy=resize_factor, interpolation=cv2.INTER_CUBIC)

    # image = cv2.rotate(image, cv2.ROTATE_90_CLOCKWISE)
    plt.imshow(image)
    plt.show()

    image_luv = cv2.cvtColor(image, cv2.COLOR_BGR2LUV)
    image_l = image_luv[:, :, 0]

    colored_gradients = edge_detection_colored(image_luv)
    gray_gradients = edge_detection_gray(image_l)

    gradients = (colored_gradients + gray_gradients) / 2
    gradients[gradients >= 255] = 255
    gradients[gradients < 0] = 0
    gradients = gradients.astype(np.uint8)

    plt.imshow(gradients, plt.cm.gray)
    plt.show()
    otsu_threshold_value = cv2.threshold(gradients, 0, 255, cv2.THRESH_OTSU | cv2.THRESH_BINARY)[0]
    otsu_threshold_value = int(otsu_threshold_value)
    high_threshold_max = np.mean(gradients[gradients >= otsu_threshold_value])
    high_threshold_min = np.mean(gradients[gradients < otsu_threshold_value])
    high_threshold_max = int(math.ceil(high_threshold_max))
    high_threshold_min = max(2, int(math.floor(high_threshold_min)))

    min_area = 0.2 * np.prod(gradients.shape)

    # strong_edges = threshold_image(gradients, otsu_threshold_value)
    # weak_edges = threshold_image_weak(gradients, otsu_threshold_value // 2, otsu_threshold_value)
    # edges = perform_edge_hysteresis(strong_edges, weak_edges)
    #
    # plt.imshow(edges, plt.cm.gray)
    # plt.title('edges')
    # plt.show()

    # result = find_best_closed_contour(edges,
    #                                   min_area)
    #
    # points = result[1]
    # edges = cv2.cvtColor(edges, cv2.COLOR_GRAY2BGR)
    # for point in points:
    #     cv2.circle(edges, point, 10, (255, 0, 0), -1)
    #
    # plt.imshow(edges)
    # plt.show()
    # print(result[1])

    best_score = None
    best_result = None

    for high_threshold in range(high_threshold_min, high_threshold_max + 1):
        strong_edges = threshold_image(gradients, high_threshold)
        for low_threshold in range(1, high_threshold):
            weak_edges = threshold_image_weak(gradients, high_threshold, low_threshold)
            edges = perform_edge_hysteresis(strong_edges, weak_edges)

            result = find_best_closed_contour(edges,
                                              min_area)

            if result is not None:
                score = result[-1]

                if best_score is None:
                    best_score = score
                    best_result = result
                elif best_score < score:
                    best_score = score
                    best_result = result

    best_contour, best_points, best_score = best_result
    contour_image = np.zeros(image.shape[:2], np.uint8)
    points_x, points_y = np.transpose(best_contour)
    contour_image[points_y, points_x] = 255
    contour_image = cv2.cvtColor(contour_image, cv2.COLOR_GRAY2BGR)

    for point in best_points:
        cv2.circle(contour_image, point, 10, (255, 0, 0), -1)

    print('best_score: ', best_score)
    plt.imshow(contour_image)
    plt.show()


if __name__ == '__main__':
    main()
