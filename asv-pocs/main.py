import os
import pickle
import random

import cv2
import matplotlib.pyplot as plt
import numpy as np
import onnx
import onnxruntime as ort
from scipy.interpolate import interp1d
from tensorflow.keras.applications.densenet import preprocess_input

CLASS_NUMBER_LENGTH = 9
EPSILON = 1e-6


def read_image_grayscale(image_path):
    return cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)


def class_from_file_name(file_name):
    return file_name[0:CLASS_NUMBER_LENGTH]


def extract_classes(iterable,
                    class_lambda,
                    item_mapper_lambda=lambda x: x):
    classes = dict()
    for item in iterable:
        c = class_lambda(item)
        if c not in classes:
            classes[c] = []
        classes[c].append(item_mapper_lambda(item))
    return classes


def extract_classes_from_file_names(paths,
                                    file_name_class_lambda=class_from_file_name):
    image_classes = dict()
    for path in paths:
        image_classes.update(extract_classes(os.listdir(path),
                                             file_name_class_lambda,
                                             lambda f: os.path.join(path, f)))
    return image_classes


def image_classes_from_paths(image_paths):
    image_paths_classes = extract_classes_from_file_names(image_paths, class_from_file_name)

    image_classes = dict()
    for c in image_paths_classes:
        image_classes[c] = [read_image_grayscale(image_path)
                            for image_path in image_paths_classes[c]]
    return image_classes


def cache_object(obj, file):
    with open(file, 'wb') as f:
        pickle.dump(obj, f)


def load_cached(file):
    with open(file, 'rb') as f:
        return pickle.load(f)


def euclidean_distance(x, y):
    squared_euclidean_distance = np.sum(np.square(np.subtract(x, y)), axis=-1)
    return np.sqrt(np.maximum(squared_euclidean_distance, 0.0))


def remove_white_border_non_binary(image):
    binary_image = otsu_threshold(image)

    mask = (255 - binary_image) > 0

    height, width = binary_image.shape[0:2]
    mask1, mask2 = mask.any(0), mask.any(1)
    x1, x2 = mask1.argmax(), width - mask1[::-1].argmax()
    y1, y2 = mask2.argmax(), height - mask2[::-1].argmax()

    return image[y1:y2, x1:x2]


def remove_white_border(binary_image):
    mask = (255 - binary_image) > 0

    height, width = binary_image.shape[0:2]
    mask1, mask2 = mask.any(0), mask.any(1)
    x1, x2 = mask1.argmax(), width - mask1[::-1].argmax()
    y1, y2 = mask2.argmax(), height - mask2[::-1].argmax()

    return binary_image[y1:y2, x1:x2]


def predict_autoencoder(autoencoder_session, input_batch):
    return autoencoder_session.run(None, {'input_1': input_batch})


def predict_dense(dense_session, input_batch):
    return dense_session.run(None, {'input_1:0': input_batch})


def predict_siamese(siamese_session, input_batch1, input_batch2):
    return siamese_session.run(None, {'input_1:0': input_batch1, 'input_2:0': input_batch2})


def predict_triplet(triplet_session, input_batch):
    return triplet_session.run(None, {'input_1:0': input_batch})


def predict_combined_verification_model(dense_session,
                                        triplet_session,
                                        siamese_session,
                                        input_batch1,
                                        input_batch2):
    distance_to_probability = interp1d([0.0, 0.6, 0.9, 1.3, 2.0],
                                       [1.0, 0.9, 0.5, 0.1, 0.0])
    siamese_output_mapper = interp1d([0.0, 0.7, 0.95, 1.0],
                                     [0.0, 0.5, 0.75, 1.0])

    embeddings1 = predict_dense(dense_session, input_batch1)
    embeddings2 = predict_dense(dense_session, input_batch2)

    siamese_output = predict_siamese(siamese_session, embeddings1, embeddings2)
    triplet_output1 = predict_triplet(triplet_session, embeddings1)
    triplet_output2 = predict_triplet(triplet_session, embeddings2)

    distances = euclidean_distance(triplet_output1, triplet_output2)
    triplet_probabilities = distance_to_probability(distances)
    siamese_mapped_output = siamese_output_mapper(siamese_output)

    triplet_probabilities[triplet_probabilities >= 1.0 - EPSILON] = 1.0
    siamese_mapped_output[siamese_mapped_output < 0.35] = 0.0

    combined_values = 0.5 * triplet_probabilities + 0.5 * siamese_mapped_output
    combined_values[combined_values < 0.35] = 0.0
    return combined_values


def pre_process_for_autoencoder(image):
    image_tensor = 255.0 - np.array(image, np.float32)
    return (image_tensor / 255.0).reshape(200, 748, 1)


def post_process_autoencoder_output(output):
    output = output.reshape(200, 748)
    output *= 255
    output = 255 - np.array(output, np.uint8)
    return cv2.threshold(output, 0, 255, cv2.THRESH_OTSU + cv2.THRESH_BINARY)[1]


def otsu_threshold(image):
    return cv2.threshold(image, 0, 255, cv2.THRESH_OTSU + cv2.THRESH_BINARY)[1]


def predict_cheque_cleaning_100dpi(autoencoder_session,
                                   image):
    original_height = 200 * 2
    original_width = 748 * 2

    if image.shape[0] >= original_height or image.shape[1] >= original_width:
        ratio1 = image.shape[0] / original_height
        ratio2 = image.shape[1] / original_width
        ratio = max(ratio1, ratio2)
        image = cv2.resize(image, (None, None), fx=1.0 / ratio, fy=1.0 / ratio, interpolation=cv2.INTER_CUBIC)

    cheque = np.ones((original_height, original_width), np.uint8) * 255
    x = random.randint(0, original_width - image.shape[1])
    y = random.randint(0, original_height - image.shape[0])
    cheque[y:y + image.shape[0], x: x + image.shape[1]] = image

    cheque = cv2.resize(cheque, (748, 200), interpolation=cv2.INTER_CUBIC)
    cheque = otsu_threshold(cheque)

    prediction = predict_autoencoder(autoencoder_session, np.array([pre_process_for_autoencoder(cheque)]))[0]
    output = post_process_autoencoder_output(prediction)
    return remove_white_border(output)


def predict_cheque_cleaning_200dpi(autoencoder_session,
                                   image):
    original_height = 200 * 2
    original_width = 748 * 2

    if image.shape[0] >= original_height or image.shape[1] >= original_width:
        ratio1 = image.shape[0] / original_height
        ratio2 = image.shape[1] / original_width
        ratio = max(ratio1, ratio2)
        image = cv2.resize(image, (None, None), fx=1.0 / ratio, fy=1.0 / ratio, interpolation=cv2.INTER_CUBIC)

    cheque200dpi = np.ones((original_height, original_width), np.uint8) * 255
    x = random.randint(0, original_width - image.shape[1])
    y = random.randint(0, original_height - image.shape[0])
    cheque200dpi[y:y + image.shape[0], x: x + image.shape[1]] = image
    cheque200dpi = otsu_threshold(cheque200dpi)

    cheque100dpi = cv2.resize(cheque200dpi, (748, 200), interpolation=cv2.INTER_CUBIC)
    cheque100dpi = otsu_threshold(cheque100dpi)

    prediction = predict_autoencoder(autoencoder_session, np.array([pre_process_for_autoencoder(cheque100dpi)]))[0]
    output = post_process_autoencoder_output(prediction)
    output = cv2.resize(output, (original_width, original_height), interpolation=cv2.INTER_CUBIC)
    output = otsu_threshold(output)
    output = cv2.erode(output, np.ones((3, 3)))
    bitwise_and = 255 - cv2.bitwise_and(255 - output, 255 - cheque200dpi)
    return remove_white_border(bitwise_and)


def replicate_channel(image, replicas=3):
    return np.concatenate([(np.expand_dims(image, axis=-1)) for _ in range(replicas)],
                          axis=-1)


def pre_process_image_for_dense_net(image):
    cropped_resized = cv2.resize(remove_white_border_non_binary(image), (224, 224), interpolation=cv2.INTER_CUBIC)
    image_threshold_inverted = 255 - otsu_threshold(cropped_resized)
    return preprocess_input(np.array([replicate_channel(image_threshold_inverted)]))


def predict_verification(dense_session,
                         triplet_session,
                         siamese_session,
                         image1,
                         image2):
    distance_to_probability = interp1d([0.0, 0.6, 0.9, 1.3, 2.0],
                                       [1.0, 0.9, 0.5, 0.1, 0.0])
    siamese_output_mapper = interp1d([0.0, 0.7, 0.95, 1.0],
                                     [0.0, 0.5, 0.75, 1.0])

    input1 = pre_process_image_for_dense_net(image1)
    input2 = pre_process_image_for_dense_net(image2)

    embedding1 = np.array(predict_dense(dense_session, input1)).reshape(1, 1920)
    embedding2 = np.array(predict_dense(dense_session, input2)).reshape(1, 1920)

    siamese_output = np.array(predict_siamese(siamese_session, embedding1, embedding2)).reshape(1, 2)[:, 0:1]
    triplet_output1 = np.array(predict_triplet(triplet_session, embedding1))
    triplet_output2 = np.array(predict_triplet(triplet_session, embedding2))

    distances = euclidean_distance(triplet_output1, triplet_output2)
    triplet_probabilities = distance_to_probability(distances)
    siamese_mapped_output = siamese_output_mapper(siamese_output)

    triplet_probabilities[triplet_probabilities >= 1.0 - EPSILON] = 1.0
    siamese_mapped_output[siamese_mapped_output < 0.35] = 0.0

    combined_values = 0.5 * triplet_probabilities + 0.5 * siamese_mapped_output
    combined_values[combined_values < 0.35] = 0.0
    return combined_values.ravel()[0]


def main():
    if not os.path.isfile('image_classes.pickle'):
        signatures_directory = '/home/u764/Development/data/signatures'
        image_classes = image_classes_from_paths([signatures_directory])
        classes = [*image_classes]
        for c in classes:
            if len(image_classes[c]) < 2:
                image_classes.pop(c)
        cache_object(image_classes, 'image_classes.pickle')
    else:
        image_classes = load_cached('image_classes.pickle')

    classes = [*image_classes]
    random.shuffle(classes)

    autoencoder_net = onnx.load('models/Autoencoder.onnx')
    onnx.checker.check_model(autoencoder_net)
    autoencoder_session = ort.InferenceSession('models/Autoencoder.onnx')

    dense_net = onnx.load('models/Dense.onnx')
    onnx.checker.check_model(dense_net)
    dense_session = ort.InferenceSession('models/Dense.onnx')

    siamese_net = onnx.load('models/Siamese.onnx')
    onnx.checker.check_model(siamese_net)
    siamese_session = ort.InferenceSession('models/Siamese.onnx')

    triplet_net = onnx.load('models/Triplet.onnx')
    onnx.checker.check_model(triplet_net)
    triplet_session = ort.InferenceSession('models/Triplet.onnx')

    # outputs = autoencoder_session.run(None, {'input_1': np.ones((1, 200, 748, 1), np.float32)})
    # outputs = dense_session.run(None, {'input_1:0': np.ones((1, 224, 224, 3), np.float32)})
    # outputs = siamese_session.run(None, {'input_1:0': np.ones((1, 1920), np.float32),
    #                                      'input_2:0': np.ones((1, 1920), np.float32)})
    # outputs = triplet_session.run(None, {'input_1:0': np.ones((1, 1920), np.float32)})

    threshold = 0.6

    false_acceptances_200dpi = 0
    true_acceptances_200dpi = 0
    false_rejections_200dpi = 0
    true_rejections_200dpi = 0

    false_acceptances_100dpi = 0
    true_acceptances_100dpi = 0
    false_rejections_100dpi = 0
    true_rejections_100dpi = 0

    number_of_forgery_classes = 5
    for i in range(len(classes) - number_of_forgery_classes):
        c = classes[i]
        genuine_images = image_classes[c]
        forgery_indices = random.sample(list(range(i + 1, len(classes))), number_of_forgery_classes)
        forgery_images = []
        for j in forgery_indices:
            forgery_class = image_classes[classes[j]]
            forgery_images.extend(random.sample(forgery_class, 2))

        for j in range(len(genuine_images)):
            reference = genuine_images[j]

            others = genuine_images[j + 1:]
            for image in others:
                image100dpi = predict_cheque_cleaning_100dpi(autoencoder_session, image)
                image200dpi = predict_cheque_cleaning_200dpi(autoencoder_session, image)

                result = predict_verification(dense_session,
                                              triplet_session,
                                              siamese_session,
                                              reference,
                                              image100dpi)
                if result >= threshold:
                    true_acceptances_100dpi += 1
                else:
                    false_rejections_100dpi += 1

                result = predict_verification(dense_session,
                                              triplet_session,
                                              siamese_session,
                                              reference,
                                              image200dpi)
                if result >= threshold:
                    true_acceptances_200dpi += 1
                else:
                    false_rejections_200dpi += 1

            for image in forgery_images:
                image100dpi = predict_cheque_cleaning_100dpi(autoencoder_session, image)
                image200dpi = predict_cheque_cleaning_200dpi(autoencoder_session, image)

                result = predict_verification(dense_session,
                                              triplet_session,
                                              siamese_session,
                                              reference,
                                              image100dpi)
                if result >= threshold:
                    false_acceptances_100dpi += 1
                else:
                    true_rejections_100dpi += 1

                result = predict_verification(dense_session,
                                              triplet_session,
                                              siamese_session,
                                              reference,
                                              image200dpi)
                if result >= threshold:
                    false_acceptances_200dpi += 1
                else:
                    true_rejections_200dpi += 1

        print('current 100 DPI statistics:')
        print(f'far: {false_acceptances_100dpi / (false_acceptances_100dpi + true_rejections_100dpi)}')
        print(f'frr: {false_rejections_100dpi / (true_acceptances_100dpi + false_rejections_100dpi)}')
        print('current 200 DPI statistics:')
        print(f'far: {false_acceptances_200dpi / (false_acceptances_200dpi + true_rejections_200dpi)}')
        print(f'frr: {false_rejections_200dpi / (true_acceptances_200dpi + false_rejections_200dpi)}')
        print('--------------------------------------------------------------')


if __name__ == '__main__':
    main()
