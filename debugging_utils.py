#!/usr/bin/python3

import math
import os
import shutil
from typing import Tuple

import cv2
import numpy as np
from matplotlib import pyplot as plt

from commons.imaging_utils import text_smearing
from configuration.config import get_document_pre_processor, get_document_fields
from document.document_reader import read_field

LABEL_LOCATION_TYPE = Tuple[Tuple[int, int], Tuple[int, int]]


def view_image(image, title=''):
    plt.imshow(image, cmap=plt.cm.gray)
    plt.title(title)
    plt.show()


def draw_contour_min_bounding_box(image,
                                  contour,
                                  color=(255, 0, 0),
                                  thickness=1):
    image_copy = np.array(image)
    image_copy = cv2.cvtColor(image_copy, cv2.COLOR_GRAY2RGB)
    cv2.drawContours(image_copy, [contour], -1, color, thickness)
    return image_copy


def draw_points(image,
                points,
                color=(0, 0, 255),
                thickness=5):
    image_copy = np.array(image)
    image_copy = cv2.cvtColor(image_copy, cv2.COLOR_GRAY2RGB)
    for pt in points:
        cv2.circle(image_copy, tuple(pt), thickness // 2, color, -1)
    return image_copy


def view_hue_saturation_distributions(hue_values,
                                      saturation_values,
                                      bin_size=5):
    fig, axs = plt.subplots(1, 2)
    plt.suptitle(f'{len(hue_values)}')

    axs[0].hist(hue_values,
                [bin_size * i for i in range(int(math.ceil(180 / bin_size)) + 1)])
    axs[0].set_title(f'hue')

    axs[1].hist(saturation_values,
                [bin_size * i for i in range(int(math.ceil(255 / bin_size)) + 1)])
    axs[1].set_title(f'saturation')
    plt.show()


def save_frames_to_video(frames,
                         detected_rectangles,
                         video_path):
    out = cv2.VideoWriter(video_path,
                          cv2.VideoWriter_fourcc(*'DIVX'),
                          30,
                          (750, 476))

    for frame in frames:
        for (x1, y1), (x2, y2) in detected_rectangles:
            cv2.rectangle(frame, (x1, y1), (x2, y2), (255, 0, 0), 3)
        out.write(frame)

    out.release()


def read_document_without_pre_processing(pre_processed_document_image: np.ndarray,
                                         country: str,
                                         document_type: str,
                                         extra_info: LABEL_LOCATION_TYPE,
                                         i: int) -> Tuple[dict, np.ndarray]:
    recognized_fields = dict()
    fields = get_document_fields(country, document_type)

    n = int(math.floor(np.sqrt(len(fields))))
    if n * n != len(fields):
        r = 1
    else:
        r = 0
    fig, axs = plt.subplots(n + r, n)

    y = 0
    x = 0
    for field in fields:
        extracted_field = field['extractor'](pre_processed_document_image, extra_info)
        pre_processed_field = field['pre_processor'](extracted_field)
        recognized_fields[field['id']] = field['post_processor'](read_field(pre_processed_field,
                                                                            field['tesseract_config']))
        if n == 1 and r == 0:
            axs.imshow(pre_processed_field)
            axs.set_title(recognized_fields[field['id']])
        elif n == 1 and r != 0:
            axs[x].imshow(pre_processed_field)
            axs[x].set_title(recognized_fields[field['id']])
        else:
            axs[y, x].imshow(pre_processed_field)
            axs[y, x].set_title(recognized_fields[field['id']])
        x += 1
        if x == n:
            x = 0
            y += 1
    plt.savefig(f'/home/u764/Downloads/test/image_{i}_readings.png', dpi=600)
    plt.close()
    return recognized_fields, pre_processed_document_image


def test_id_card_reading():
    images_with_info = [
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Adrian Mikko/WhatsApp Image 2020-06-10 at 9.04.00 AM (1).jpeg',
            'qat',
            'id_card.non_smart.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Alma Pita/Shared Throught whatsapp/WhatsApp Image 2020-06-10 at 9.41.09 AM.jpeg',
            'qat',
            'id_card.non_smart.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Anas Abu Shaweesh/20200610_090313.jpg',
            'qat',
            'id_card.non_smart.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Anas Abu Shaweesh/20200610_090303.jpg',
            'qat',
            'id_card.non_smart.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Anas Abu Shaweesh/20200610_090247.jpg',
            'qat',
            'id_card.non_smart.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Botros/20200610_102433.jpg',
            'qat',
            'id_card.non_smart.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Fayez Al Wahidi/20200610_091923.jpg',
            'qat',
            'id_card.non_smart.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Jehad/IMG_20200610_172502.jpg',
            'qat',
            'id_card.non_smart.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/SMART ID non Qatari/Alan/IMG_20200610_110951.jpg',
            'qat',
            'id_card.smart.resident.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/SMART ID non Qatari/Jack/smart id pics/20200610_151437.jpg',
            'qat',
            'id_card.smart.resident.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/drive-download-20200830T074731Z-001/IMG_20200830_104154.jpg',
            'jor',
            'id_card.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/test-data/id_front_14.jpg',
            'jor',
            'id_card.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/test-data/id_front_10.jpg',
            'jor',
            'id_card.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/test-data/id_front_9.jpg',
            'jor',
            'id_card.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/PSO ID/khaldoun B.jpg',
            'oma',
            'id_card.resident.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/drive-download-20210824T150016Z-001/IMG_20210824_175702_1.jpg',
            'jor',
            'id_card.front',
        ),
        (
            '/home/u764/Development/data/ekyc-data/drive-download-20210824T150016Z-001/IMG_20210824_175704.jpg',
            'jor',
            'id_card.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/drive-download-20210824T150016Z-001/IMG_20210824_175708.jpg',
            'jor',
            'id_card.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/drive-download-20210824T150016Z-001/IMG_20210824_175720.jpg',
            'jor',
            'id_card.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/drive-download-20210824T150016Z-001/IMG_20210824_175723.jpg',
            'jor',
            'id_card.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/drive-download-20210824T150016Z-001/IMG_20210824_175725.jpg',
            'jor',
            'id_card.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2021-08-25/QID Sample/Regular QID/WhatsApp Image 2021-08-24 at 9.40.26 AM.jpeg',
            'qat',
            'id_card.non_smart.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2021-08-25/QID Sample/Regular QID/WhatsApp Image 2021-08-24 at 9.40.23 AM.jpeg',
            'qat',
            'id_card.non_smart.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2021-08-25/QID Sample/Regular QID/WhatsApp Image 2021-08-24 at 9.40.28 AM.jpeg',
            'qat',
            'id_card.non_smart.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2021-08-25/QID Sample/Regular QID/WhatsApp Image 2021-08-24 at 9.40.22 AM.jpeg',
            'qat',
            'id_card.non_smart.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2021-08-25/QID Sample/Smart Resident QID/WhatsApp Image 2021-08-24 at 9.40.20 AM (1).jpeg',
            'qat',
            'id_card.smart.resident.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2021-08-25/QID Sample/Smart Resident QID/WhatsApp Image 2021-08-24 at 9.40.19 AM (1).jpeg',
            'qat',
            'id_card.smart.resident.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/PSO ID/Fatema F.jpg',
            'oma',
            'id_card.citizen.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/PSO ID/Jamal Al Shukri -F.jpg',
            'oma',
            'id_card.citizen.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/PSO ID/Sara -F.jpg',
            'oma',
            'id_card.citizen.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/drive-download-20200830T074731Z-001/IMG_20200830_104148.jpg',
            'jor',
            'id_card.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/drive-download-20200830T074731Z-001/IMG_20200830_104154.jpg',
            'jor',
            'id_card.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/drive-download-20200830T074731Z-001/IMG_20200830_104236.jpg',
            'jor',
            'id_card.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/drive-download-20200830T074731Z-001/IMG_20210725_124622.jpg',
            'jor',
            'id_card.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Adrian Mikko/WhatsApp Image 2020-06-10 at 9.03.55 AM.jpeg',
            'qat',
            'id_card.non_smart.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Adrian Mikko/WhatsApp Image 2020-06-10 at 9.03.56 AM.jpeg',
            'qat',
            'id_card.non_smart.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Jehad/IMG_20200610_172839.jpg',
            'qat',
            'id_card.non_smart.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Jehad/IMG_20200610_172423.jpg',
            'qat',
            'id_card.non_smart.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Jehad/IMG_20200610_172309.jpg',
            'qat',
            'id_card.non_smart.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Jehad/IMG_20200610_172252.jpg',
            'qat',
            'id_card.non_smart.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Jehad/IMG_20200610_172229.jpg',
            'qat',
            'id_card.non_smart.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/QATARTI ID/WhatsApp Image 2020-06-10 at 12.09.49 PM (1).jpeg',
            'qat',
            'id_card.smart.citizen.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/QATARTI ID/WhatsApp Image 2020-06-10 at 12.09.49 PM (2).jpeg',
            'qat',
            'id_card.smart.citizen.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/QATARTI ID/WhatsApp Image 2020-06-10 at 12.09.49 PM (3).jpeg',
            'qat',
            'id_card.smart.citizen.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/test-data/id_front_10.jpg',
            'jor',
            'id_card.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/test-data/id_front_13.jpg',
            'jor',
            'id_card.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/test-data/id_front_14.jpg',
            'jor',
            'id_card.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/test-data/300dpi full page/NON_SMART_1_CROPPED.jpg',
            'qat',
            'id_card.non_smart.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/test-data/300dpi full page/NON_SMART_2_CROPPED.jpg',
            'qat',
            'id_card.non_smart.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/test-data/300dpi full page/NON_SMART_3_CROPPED.jpg',
            'qat',
            'id_card.non_smart.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/test-data/300dpi full page/SMART_1_CROPPED.jpg',
            'qat',
            'id_card.smart.resident.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Adrian Mikko/WhatsApp Image 2020-06-10 at 9.04.00 AM.jpeg',
            'qat',
            'id_card.non_smart.back'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Botros/20200610_102440.jpg',
            'qat',
            'id_card.non_smart.back'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Fayez Al Wahidi/20200610_091910.jpg',
            'qat',
            'id_card.non_smart.back'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/QATARTI ID/WhatsApp Image 2020-06-10 at 12.09.49 PM (4).jpeg',
            'qat',
            'id_card.smart.citizen.back'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/SMART ID non Qatari/Alan/IMG_20200610_111001.jpg',
            'qat',
            'id_card.smart.resident.back'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/SMART ID non Qatari/Jack/smart id pics/20200610_151445.jpg',
            'qat',
            'id_card.smart.resident.back'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2021-08-25/QID Sample/Regular QID/WhatsApp Image 2021-08-24 at 9.40.23 AM (1).jpeg',
            'qat',
            'id_card.non_smart.back'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2021-08-25/QID Sample/Regular QID/WhatsApp Image 2021-08-24 at 9.40.26 AM (1).jpeg',
            'qat',
            'id_card.non_smart.back'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2021-08-25/QID Sample/Smart Citizen QID/wahab 2031 - back.jpg',
            'qat',
            'id_card.smart.citizen.back'
        )
    ]

    i = 0
    for image_path, country, document_type in images_with_info:
        image = cv2.imread(image_path)
        if country != 'qat':
            continue

        pre_processor_result = get_document_pre_processor(country, document_type)(image,
                                                                                  True)

        if pre_processor_result is None:
            continue

        # if i not in [6, 21, 22, 28]:
        #     i += 1
        #     continue

        pre_processed_image, extra_info = pre_processor_result
        fig, axs = plt.subplots(3, 1)
        axs[0].imshow(image)
        axs[1].imshow(pre_processed_image)
        axs[2].imshow(text_smearing(pre_processed_image))
        plt.savefig(f'/home/u764/Downloads/test/image_{i}.png', dpi=600)
        plt.close()
        read_document_without_pre_processing(pre_processed_image, country, document_type, extra_info, i)
        i += 1


def de_warp(image_path, country, document_type):
    view_image(get_document_pre_processor(country, document_type)(cv2.imread(image_path), True)[0])


if __name__ == '__main__':
    test_id_card_reading()
    # de_warp(
    #     '/home/u764/Development/data/ekyc-data/OneDrive_2021-08-25/QID Sample/Smart Resident QID/WhatsApp Image 2021-08-24 at 9.40.25 AM (1).jpeg',
    #     'qat',
    #     'id_card.smart.resident.back')
