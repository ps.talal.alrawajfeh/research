#!/usr/bin/python3.6
import os

import cv2
import numpy as np
from sklearn.cluster import KMeans
from tqdm import tqdm
from matplotlib import pyplot as plt
import math
from scipy.stats import norm


def squash(x, gamma=10):
    ex = math.exp(gamma * (x - 0.5))
    return ex / (1 + ex)


def clip(x, min_clip=0, max_clip=1):
    if x > max_clip:
        return max_clip
    if x < min_clip:
        return min_clip
    return x


def geometric_mean(*args):
    prod = 1
    for arg in args:
        prod *= arg
    return math.pow(prod, 1 / len(args))


def shifted_scaled_exponential(x):
    return math.exp(x + 1) / math.e


def adaptive_two_means_clustering(inputs, centroids=None):
    max_input = int(np.max(inputs))
    min_input = int(np.min(inputs))
    mu = np.mean(inputs)
    sigma = np.std(inputs)

    if centroids is None:
        centroids = [min_input, max_input]

    centroid1 = centroids[0]
    centroid2 = centroids[1]

    for i in range(len(inputs)):
        x = inputs[i]
        dist1 = abs(centroid1 - x)
        dist2 = abs(centroid2 - x)
        centroid_dist = abs(centroid1 - centroid2)
        inverted_percentage = (i + 1) / len(inputs)
        inverted_centroid_dist_ratio = 1 - centroid_dist / 255

        if dist2 <= dist1:
            alpha2 = squash(geometric_mean(shifted_scaled_exponential(inverted_centroid_dist_ratio),
                                           clip(sigma / abs(mu - centroid2)),
                                           shifted_scaled_exponential(inverted_percentage)))
            beta2 = 1 - alpha2
            centroid2 = alpha2 * centroid2 + beta2 * x
        else:
            alpha1 = squash(geometric_mean(shifted_scaled_exponential(inverted_centroid_dist_ratio),
                                           clip(sigma / abs(mu - centroid1)),
                                           shifted_scaled_exponential(inverted_percentage)))

            beta1 = 1 - alpha1
            centroid1 = alpha1 * centroid1 + beta1 * x

    return [centroid1, centroid2]


def k_means_binarization_attempt1(image_path):
    image = cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)
    # image = cv2.GaussianBlur(image, (3, 3), 0)

    pixels = np.copy(image).reshape((image.shape[0] * image.shape[1]))
    pixels = np.expand_dims(pixels, 1)

    k_means = KMeans(n_clusters=2, random_state=0)
    k_means = k_means.fit(pixels)

    # new_pixels = k_means.labels_
    # new_pixels = new_pixels.reshape((image.shape[0], image.shape[1])) * 255
    #
    # if len(new_pixels[new_pixels == 255]) / (image.shape[0] * image.shape[1]) > 0.5:
    #     new_pixels = 255 - new_pixels

    thresh = np.sum(k_means.cluster_centers_) / 2

    image[image < thresh] = 0
    image[image >= thresh] = 255

    return image


def adaptive_threshold(image):
    image = cv2.GaussianBlur(image, (3, 3), 0)

    pixels = np.copy(image).reshape((image.shape[0] * image.shape[1]))
    centroids = adaptive_two_means_clustering(pixels)

    return sum(centroids) / 2


def binarize(image_path, thresh=None):
    image = 255 - cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)
    if thresh is None:
        thresh = adaptive_threshold(image)
    image[image >= thresh] = 255

    return 255 - image


def main():
    # image_names = os.listdir('/home/u764/Development/progressoft/en-car-reader/data/test/mutaz/')
    #
    # progress = tqdm(total=len(image_names))
    # for f in image_names:
    #     cv2.imwrite('/home/u764/Development/progressoft/en-car-reader/data/test/bin/' + f,
    #                 k_means_binarization(cv2.imread('/home/u764/Development/progressoft/en-car-reader/data/test/mutaz/' + f, cv2.IMREAD_GRAYSCALE)))
    #     progress.update()
    # progress.close()

    # # path = '/home/u764/Development/progressoft/en-car-reader/data/test/mutaz/3,180_0.bmp'
    # path = '/home/u764/Development/progressoft/en-car-reader/data/decimals/decimal/25.jpg'
    #
    # plt.imshow(binarize(path),
    #            cmap=plt.cm.gray)
    # plt.show()

    path = '/home/u764/Development/progressoft/en-car-reader/data/commas/comma1/'
    image_names = os.listdir(path)
    progress = tqdm(total=len(image_names))
    for name in image_names:
        cv2.imwrite(path + name, binarize(path + name))
        progress.update()
    progress.close()


if __name__ == '__main__':
    main()
