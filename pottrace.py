#!/usr/bin/python3.6

import cv2
import os
import tempfile
from tqdm import tqdm


# potrace cli should be installed for this script to work
# type `potrace --help` in a terminal to ensure that it is installed


INPUT_DIRECTORY = '/home/u764/Downloads/signatures'
OUTPUT_DIRECTORY = '/home/u764/Downloads/result'


def main():
    files = [f for f in os.listdir(INPUT_DIRECTORY) if f[-4:] == '.jpg']

    if not os.path.isdir(OUTPUT_DIRECTORY):
        os.makedirs(OUTPUT_DIRECTORY)

    progress_bar = tqdm(total=len(files))

    for original_file in files:
        image = cv2.imread(os.path.join(INPUT_DIRECTORY, original_file))
        downscaled = cv2.resize(image,
                                (image.shape[1] // 2, image.shape[0] // 2),
                                interpolation=cv2.INTER_CUBIC)

        image[image >= 128] = 255
        image[image < 128] = 0

        downscaled[downscaled >= 128] = 255
        downscaled[downscaled < 128] = 0

        image_file = tempfile.NamedTemporaryFile('wb')
        downscaled_file = tempfile.NamedTemporaryFile('wb')

        _, image_data = cv2.imencode('.bmp', image)
        with image_file.file as f:
            f.write(image_data)

        _, downscaled_data = cv2.imencode('.bmp', downscaled)
        with downscaled_file.file as f:
            f.write(downscaled_data)

        file_name = os.path.basename(original_file)
        file_name = file_name[:file_name.rfind(os.path.extsep)]

        base_name = os.path.join(OUTPUT_DIRECTORY,
                                 f'{file_name}')
        image_output = base_name + '_1'
        downscaled_output = base_name + '_2'

        os.system(
            f'potrace --backend pgm --turnpolicy minority -t 2 -a 1 --opttolerance 0.2 --unit 10 --level2 --gamma 1.5 --blacklevel 0.5 -W {image.shape[1] / 90} -s {image_file.name} -o {image_output}')
        os.system(
            f'potrace --backend pgm --turnpolicy minority -t 2 -a 1 --opttolerance 0.2 --unit 10 --level2 --gamma 1.5 --blacklevel 0.5 -W {image.shape[1] / 90} -s {downscaled_file.name} -o {downscaled_output}')

        # -- clean up ---------------------------------------------------------
        image_file.close()
        downscaled_file.close()
        # ---------------------------------------------------------------------

        progress_bar.update()
    progress_bar.close()


if __name__ == "__main__":
    main()
