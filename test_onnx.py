import onnxruntime
import numpy as np
import time


def main():
    sess = onnxruntime.InferenceSession("test_model2.onnx")

    start_time = time.time()
    for i in range(100):
        output = sess.run(None, {"input_1": np.expand_dims(np.random.normal(size=(96, 144, 1)).astype(np.float32), axis=0)})[0]
    end_time = time.time()

    print(output)
    print((end_time - start_time) / 100)

if __name__ == "__main__":
    main()
