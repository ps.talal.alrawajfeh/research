#!/bin/bash

#cython --embed -o hello.c hello.pyx
#gcc -Os -I /usr/include/python3.6m -o hello hello.c -lpython3.6m -lpthread -lm -lutil -ldl

python3 build.py build_ext --inplace
# mv hello.c hello.h

# python3-config --cflags
# python3-config --ldflags

gcc -c example.c -o example.o $(python3-config --cflags)
# cp ./build/lib.linux-x86_64-3.6/hello.cpython-37m-x86_64-linux-gnu.so ./hello.cpython-37m-x86_64-linux-gnu.so
cp ./build/temp.linux-x86_64-3.6/hello.o ./hello.o
gcc example.o hello.o -o prog $(python3-config --ldflags) -fno-lto -lavdevice
