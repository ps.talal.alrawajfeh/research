#include <Python.h>
#include <stdio.h>
#include "hello.h"

int main(int argc, char *argv[])
{
    int status = PyImport_AppendInittab("hello", PyInit_hello);

    if (status == -1)
    {
        return -1;
    }

    Py_Initialize();

    PyObject *module = PyImport_ImportModule("hello");

    if (module == NULL)
    {
        Py_Finalize();
        return -1;
    }

    say_hello();

    // PyObject *mod = PyInit_hello();
    // if (mod == NULL) {
    //     return 0;
    // }

    Py_Finalize();
    return 0;
}
