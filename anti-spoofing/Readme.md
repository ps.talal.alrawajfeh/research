# Anti-Spoofing

## Datasets

### 1. CelebA-Spoof

The dataset could be found [here](https://github.com/ZhangYuanhan-AI/CelebA-Spoof).
Download all the zip files which are splits of the original zip file.
To merge the zip files run the following commands:

```bash
cat *.zip* > CelebA_Spoof.zip
unzip CelebA_Spoof.zip
chmod a+rw CelebA_Spoof
```

### 2. LLC-FASD
The dataset could be found [here](https://www.kaggle.com/datasets/faber24/lcc-fasd). 
After the archive has been downloaded, run the following command:

```bash
unzip archive.zip
```

## Data Preparation

Make sure that both the `CelebA_Spoof` and `LLC_FASD` datasets exist and contain the following:
* `CelebA_Spoof` should contain `Data` and `metas` directories and a `README` file.
* `LLC_FASD` should contain `LCC_FASD_development`, `LCC_FASD_evaluation`, and `LCC_FASD_training` directories.

To prepare the dataset run the following command:
```bash
python3 prepare_data.py
```