import tensorflow as tf
import sys

from train import NormalizedDotProduct


def main():
    model = tf.keras.models.load_model(sys.argv[1],
                                       compile=False,
                                       custom_objects={'NormalizedDotProduct': NormalizedDotProduct})
    tf.keras.models.save_model(model, sys.argv[2])


if __name__ == '__main__':
    main()
