import math
import os
import random
from enum import Enum

import numpy as np
import tensorflow as tf
from matplotlib import pyplot as plt
from sklearn.metrics import accuracy_score, confusion_matrix, classification_report, roc_curve, auc
from terminaltables import AsciiTable
from tqdm import tqdm

from prepare_data import read_image, deserialize_object, serialize_object, get_image_paths

keras = tf.keras
from keras.applications.mobilenet_v3 import MobileNetV3Large, hard_swish
from alt_model_checkpoint.tensorflow import AltModelCheckpoint

DATA_PATH = '/home/user/Downloads/data'
INPUT_SIZE = (224, 224)
INPUT_SHAPE = (224, 224, 3)
FEATURE_VECTOR_SIZE = 960
MAX_BATCH_SIZE = 1024
LIVE_LABEL = [0, 1]
SPOOF_LABEL = [1, 0]
WARM_UP_TRAIN_BATCH_SIZE = 128
WARM_UP_VALIDATION_BATCH_SIZE = 512
WARM_UP_TRAIN_EPOCHS = 2
TRAIN_BATCH_SIZE = 32
VALIDATION_BATCH_SIZE = 128
TRAIN_EPOCHS = 20
EVALUATE_MODEL = True
TRAIN_MODEL = True
TRAIN_FINAL_MODEL = False
FINAL_TRAIN_EPOCHS = 5
DEFAULT_LEARNING_RATE = 0.001
DEFAULT_SGD_MOMENTUM = 0.9
DEFAULT_WEIGHT_DECAY = None
GENERATE_FINAL_MODEL = True


def build_classifier_head(feature_vector_size=FEATURE_VECTOR_SIZE):
    input_layer = tf.keras.layers.Input(shape=(feature_vector_size,))
    dense = tf.keras.layers.Dense(128)(input_layer)
    dense = hard_swish(dense)
    dense = tf.keras.layers.Dropout(0.5)(dense)
    output = NormalizedDotProduct(2)(dense)
    return tf.keras.models.Model(inputs=input_layer, outputs=output)


def build_classifier_model(input_shape=INPUT_SHAPE, pre_trained_model_trainable=False):
    pre_trained_model = MobileNetV3Large(include_top=False,
                                         weights='imagenet',
                                         input_shape=INPUT_SHAPE,
                                         pooling='avg')
    for layer in pre_trained_model.layers:
        layer.trainable = pre_trained_model_trainable
    input_layer = tf.keras.layers.Input(shape=input_shape)
    feature_vector = pre_trained_model(input_layer)
    classifier_head = build_classifier_head()
    label = classifier_head(feature_vector)
    model = tf.keras.models.Model(inputs=input_layer, outputs=label)
    return model, pre_trained_model, classifier_head


def build_classifier_model_from_existing(pre_trained_model,
                                         classifier_head,
                                         input_shape=INPUT_SHAPE,
                                         pre_trained_model_trainable=False,
                                         with_softmax=False):
    for layer in pre_trained_model.layers:
        layer.trainable = pre_trained_model_trainable
    input_layer = tf.keras.layers.Input(shape=input_shape)
    feature_vector = pre_trained_model(input_layer)
    label = classifier_head(feature_vector)
    if with_softmax:
        label = tf.keras.layers.Activation(tf.nn.softmax)(label)
    return tf.keras.models.Model(inputs=input_layer, outputs=label)


def make_divisible_by(x, d):
    return int(math.ceil(x / d) * d)


def bootstrap_array_up_to_count(array, count):
    difference = count - len(array)
    if difference != 0:
        for _ in range(difference):
            array.append(random.choice(array))
        random.shuffle(array)


def fnr(y_true, y_pred):
    # fnr = false negative rate
    # false negative: true label should be positive, but the predicted label was negative
    # 1 is positive, live
    # 0 is negative, spoof
    true_labels = tf.argmax(y_true, axis=-1)
    pred_labels = tf.argmax(y_pred, axis=-1)
    true_positives_mask = tf.cast(tf.equal(true_labels, 1), tf.float32)
    pred_negatives_mask = tf.cast(tf.equal(pred_labels, 0), tf.float32)
    return tf.reduce_sum(true_positives_mask * pred_negatives_mask) / tf.reduce_sum(true_positives_mask)


def fpr(y_true, y_pred):
    # fpr = false positive rate
    # false positive: true label should be negative, but the predicted label was positive
    # 1 is positive, live
    # 0 is negative, spoof
    true_labels = tf.argmax(y_true, axis=-1)
    pred_labels = tf.argmax(y_pred, axis=-1)
    true_negatives_mask = tf.cast(tf.equal(true_labels, 0), tf.float32)
    pred_positives_mask = tf.cast(tf.equal(pred_labels, 1), tf.float32)
    return tf.reduce_sum(true_negatives_mask * pred_positives_mask) / tf.reduce_sum(true_negatives_mask)


def aer(y_true, y_pred):
    return (fnr(y_true, y_pred) + fpr(y_true, y_pred)) / 2


class NormalizedDotProduct(tf.keras.layers.Layer):
    def __init__(self, outputs, name=None, **kwargs):
        super(NormalizedDotProduct, self).__init__(name=name)
        self.outputs = outputs
        super(NormalizedDotProduct, self).__init__(**kwargs)

    def get_config(self):
        config = super(NormalizedDotProduct, self).get_config()
        config.update({"outputs": self.outputs})
        return config

    def build(self, input_shape):
        self.weight_matrix = self.add_weight(shape=(self.outputs, input_shape[-1]),
                                             initializer=tf.initializers.glorot_uniform(),
                                             name=f'normalized_dot_product_weights')

    def call(self, inputs):
        normalized_inputs = tf.linalg.l2_normalize(inputs, axis=-1)
        normalized_weights = tf.linalg.l2_normalize(self.weight_matrix, axis=-1)
        return tf.matmul(normalized_inputs, normalized_weights, transpose_b=True)


def additive_margin_softmax_with_focal_loss(y_true, y_pred, s=30, margin=0.4, alpha=0.25, gamma=2, epsilon=1e-12):
    """
    papers: https://arxiv.org/pdf/1801.05599.pdf, https://arxiv.org/pdf/1708.02002.pdf
    couldn't find any official implementation, so I implemented this and the NormalizedDotProduct layer from this paper
    """
    nominator = tf.reduce_sum(y_true * tf.exp(s * (y_pred - margin)), axis=-1)
    denominator = nominator + tf.reduce_sum((1.0 - y_true) * tf.exp(s * y_pred), axis=-1)
    probabilities = tf.maximum(nominator / denominator, 0.0) + epsilon
    losses = -tf.math.log(probabilities)
    losses *= alpha * tf.math.pow(1 - probabilities, gamma)
    return tf.reduce_mean(losses)


class DataGenerator(keras.utils.Sequence):
    def __init__(self,
                 live_paths,
                 spoof_paths,
                 batch_size):
        self.count = len(live_paths) + len(spoof_paths)
        self.live_paths = live_paths
        self.spoof_paths = spoof_paths
        self.batch_size = batch_size
        random.shuffle(self.live_paths)
        random.shuffle(self.spoof_paths)

    def __getitem__(self, index):
        live_paths_batch = self.live_paths[index * self.batch_size // 2: (index + 1) * self.batch_size // 2]
        spoof_paths_batch = self.spoof_paths[index * self.batch_size // 2: (index + 1) * self.batch_size // 2]

        live_images = [read_image(f) for f in live_paths_batch]
        spoof_images = [read_image(f) for f in spoof_paths_batch]

        live_labels = [LIVE_LABEL] * (self.batch_size // 2)
        spoof_labels = [SPOOF_LABEL] * (self.batch_size // 2)

        return np.array(live_images + spoof_images, np.float32), np.array(live_labels + spoof_labels, np.float32)

    def __len__(self):
        return int(math.ceil(self.count / self.batch_size))

    def on_epoch_end(self):
        random.shuffle(self.live_paths)
        random.shuffle(self.spoof_paths)


def epoch_metrics_log(epoch, logs, with_validation=True):
    line = f'epoch: {epoch + 1}'
    line += ' - '
    line += f'loss: {logs["loss"]}'
    line += ' - '
    line += f'fpr: {logs["fpr"]}'
    line += ' - '
    line += f'fnr: {logs["fnr"]}'
    line += ' - '
    line += f'aer: {logs["aer"]}'
    if not with_validation:
        return line + '\n'
    line += ' - '
    line += f'validation loss: {logs["val_loss"]}'
    line += ' - '
    line += f'validation fpr: {logs["val_fpr"]}'
    line += ' - '
    line += f'validation fnr: {logs["val_fnr"]}'
    line += ' - '
    line += f'validation aer: {logs["val_aer"]}'
    return line + '\n'


def default_optimizer():
    return tf.keras.optimizers.SGD(learning_rate=DEFAULT_LEARNING_RATE,
                                   momentum=DEFAULT_SGD_MOMENTUM,
                                   nesterov=True,
                                   weight_decay=DEFAULT_WEIGHT_DECAY)


def train_initial_model(train_live_paths,
                        train_spoof_paths,
                        validation_live_paths,
                        validation_spoof_paths):
    classifier_model, pre_trained_model, classifier_head = build_classifier_model()

    classifier_head.summary()
    classifier_model.summary()

    train_data_generator = DataGenerator(train_live_paths,
                                         train_spoof_paths,
                                         WARM_UP_TRAIN_BATCH_SIZE)
    validation_data_generator = DataGenerator(validation_live_paths,
                                              validation_spoof_paths,
                                              WARM_UP_VALIDATION_BATCH_SIZE)

    classifier_model.compile(loss=additive_margin_softmax_with_focal_loss,
                             optimizer=default_optimizer(),
                             metrics=[fpr, fnr, aer])

    append_lines_to_report('evaluation_report.txt', '\n[initial training log]\n')

    update_report_callback = tf.keras.callbacks.LambdaCallback(
        on_epoch_end=lambda epoch, logs: append_lines_to_report('evaluation_report.txt',
                                                                epoch_metrics_log(epoch, logs)))

    checkpoint_callback1 = AltModelCheckpoint(alternate_model=pre_trained_model,
                                              filepath='pre_trained_model_warmup.h5',
                                              save_best_only=False)
    checkpoint_callback2 = AltModelCheckpoint(alternate_model=classifier_head,
                                              filepath='classifier_head_warmup.h5',
                                              save_best_only=False)

    initial_model_history = classifier_model.fit(train_data_generator,
                                                 batch_size=WARM_UP_TRAIN_BATCH_SIZE,
                                                 validation_data=validation_data_generator,
                                                 validation_batch_size=WARM_UP_VALIDATION_BATCH_SIZE,
                                                 epochs=WARM_UP_TRAIN_EPOCHS,
                                                 callbacks=[update_report_callback,
                                                            checkpoint_callback1,
                                                            checkpoint_callback2])

    serialize_object('initial_model_history.pickle', initial_model_history)


class TrainingMode(Enum):
    FromLastWarmUpCheckpoint = 1
    FromLastCheckpoint = 2
    FromBestCheckpoint = 3


def model_files_from_training_mode(training_mode):
    if training_mode == TrainingMode.FromLastWarmUpCheckpoint:
        pre_trained_model_file = 'pre_trained_model_warmup.h5'
        classifier_head_file = 'classifier_head_warmup.h5'
    elif training_mode == TrainingMode.FromLastCheckpoint:
        pre_trained_model_file = 'pre_trained_model_last.h5'
        classifier_head_file = 'classifier_head_last.h5'
    else:
        pre_trained_model_file = 'pre_trained_model_best.h5'
        classifier_head_file = 'classifier_head_best.h5'
    return pre_trained_model_file, classifier_head_file


def train_model(train_live_paths,
                train_spoof_paths,
                validation_live_paths,
                validation_spoof_paths,
                learning_rate=DEFAULT_LEARNING_RATE,
                training_mode=TrainingMode.FromLastWarmUpCheckpoint,
                epochs=TRAIN_EPOCHS):
    pre_trained_model_file, classifier_head_file = model_files_from_training_mode(training_mode)

    pre_trained_model = tf.keras.models.load_model(pre_trained_model_file, compile=False)
    classifier_head = tf.keras.models.load_model(classifier_head_file,
                                                 compile=False,
                                                 custom_objects={
                                                     'NormalizedDotProduct': NormalizedDotProduct
                                                 })
    classifier_model = build_classifier_model_from_existing(pre_trained_model,
                                                            classifier_head,
                                                            pre_trained_model_trainable=True)

    classifier_head.summary()
    classifier_model.summary()

    train_data_generator = DataGenerator(train_live_paths,
                                         train_spoof_paths,
                                         TRAIN_BATCH_SIZE)
    validation_data_generator = DataGenerator(validation_live_paths, validation_spoof_paths, VALIDATION_BATCH_SIZE)

    classifier_model.compile(loss=additive_margin_softmax_with_focal_loss,
                             optimizer=tf.keras.optimizers.SGD(learning_rate=learning_rate,
                                                               momentum=DEFAULT_SGD_MOMENTUM,
                                                               nesterov=True,
                                                               weight_decay=DEFAULT_WEIGHT_DECAY),
                             metrics=[fpr, fnr, aer])

    append_lines_to_report('evaluation_report.txt', '\n[training log]\n')

    update_report_callback = tf.keras.callbacks.LambdaCallback(
        on_epoch_end=lambda epoch, logs: append_lines_to_report('evaluation_report.txt',
                                                                epoch_metrics_log(epoch, logs)))

    checkpoint_callback1 = AltModelCheckpoint(alternate_model=pre_trained_model,
                                              filepath='pre_trained_model_best.h5',
                                              monitor='val_aer',
                                              mode='min',
                                              save_best_only=True)
    checkpoint_callback2 = AltModelCheckpoint(alternate_model=classifier_head,
                                              filepath='classifier_head_best.h5',
                                              monitor='val_aer',
                                              mode='min',
                                              save_best_only=True)

    checkpoint_callback3 = AltModelCheckpoint(alternate_model=pre_trained_model,
                                              filepath='pre_trained_model_last.h5',
                                              save_best_only=False)
    checkpoint_callback4 = AltModelCheckpoint(alternate_model=classifier_head,
                                              filepath='classifier_head_last.h5',
                                              save_best_only=False)

    model_history = classifier_model.fit(train_data_generator,
                                         batch_size=TRAIN_BATCH_SIZE,
                                         validation_data=validation_data_generator,
                                         validation_batch_size=VALIDATION_BATCH_SIZE,
                                         epochs=epochs,
                                         callbacks=[update_report_callback,
                                                    checkpoint_callback1,
                                                    checkpoint_callback2,
                                                    checkpoint_callback3,
                                                    checkpoint_callback4])

    serialize_object(f'history_{training_mode}.pickle', model_history)


def append_lines_to_report(report_file, lines):
    if isinstance(lines, list):
        for line in lines:
            print(line.rstrip())
    else:
        print(lines.rstrip())

    with open(report_file, 'a') as f:
        f.writelines(lines)


def write_to_file(report_file, text):
    with open(report_file, 'a') as f:
        f.writelines(text)


def write_classification_report(report_file, all_true_labels, all_predicted_scores, threshold):
    all_predicted_labels = np.array(all_predicted_scores, np.float32) >= threshold

    accuracy = accuracy_score(all_true_labels, all_predicted_labels)
    append_lines_to_report(report_file, f'\nthreshold: {round(threshold * 100, 2)}%\n')
    append_lines_to_report(report_file, f'test accuracy: {round(accuracy * 100, 2)}%\n')

    append_lines_to_report(report_file, '\nconfusion matrix:\n')
    matrix = confusion_matrix(all_true_labels, all_predicted_labels)
    append_lines_to_report(report_file, f'{matrix}\n')

    tn, fp, fn, tp = matrix.ravel()

    false_positive_rate = round(fp / (fp + tn) * 100, 2)
    false_negative_rate = round(fn / (fn + tp) * 100, 2)

    append_lines_to_report(report_file, f'\nFPR: {false_positive_rate}%\n')
    append_lines_to_report(report_file, f'FNR: {false_negative_rate}%\n')

    false_positive_rates, true_positive_rates, thresholds = roc_curve(all_true_labels, all_predicted_scores)
    area_under_curve = auc(false_positive_rates, true_positive_rates)
    append_lines_to_report(report_file, f'AUC: {area_under_curve}%\n')

    append_lines_to_report(report_file, '\nclassification report:\n')
    append_lines_to_report(report_file, f'{classification_report(all_true_labels, all_predicted_labels)}\n')


def calculate_false_positive_rate_per_threshold(spoof_count,
                                                histogram_bins,
                                                spoof_probabilities):
    false_positives = spoof_probabilities
    false_positives_histogram = np.histogram(false_positives, histogram_bins)[0]
    false_positive_per_threshold = np.cumsum(false_positives_histogram)
    false_positive_per_threshold = spoof_count - false_positive_per_threshold
    false_positive_rate_per_threshold = false_positive_per_threshold / spoof_count
    return false_positive_rate_per_threshold


def calculate_false_negative_rate_per_threshold(live_count,
                                                histogram_bins,
                                                live_probabilities):
    false_negatives = live_probabilities
    false_negatives_histogram = np.histogram(false_negatives, histogram_bins)[0]
    false_negative_per_threshold = np.cumsum(false_negatives_histogram)
    false_negative_rate_per_threshold = false_negative_per_threshold / live_count
    return false_negative_rate_per_threshold


def write_distributions_table(report_file_path,
                              false_positive_rate_per_threshold,
                              false_negative_rate_per_threshold,
                              histogram_bin_size):
    table = [['Threshold', 'FPR', 'FNR']]
    for i in range(0, 100 // histogram_bin_size):
        if i == 0:
            false_positive_rate = 100.0
            false_negative_rate = 0.0
        else:
            false_positive_rate = round(false_positive_rate_per_threshold[i - 1] * 100, 2)
            false_negative_rate = round(false_negative_rate_per_threshold[i - 1] * 100, 2)
        table.append([f'{i * histogram_bin_size}%', f'{false_positive_rate}%', f'{false_negative_rate}%'])
    table.append([f'{100}%', f'{0.0}%', f'{100.0}%'])
    table = AsciiTable(table).table
    append_lines_to_report(report_file_path, table)
    write_to_file(report_file_path, '\n')


def plot_distributions(live_distribution_file_path,
                       spoof_distribution_file_path,
                       mixed_distributions_file_path,
                       live_probabilities,
                       spoof_probabilities,
                       histogram_bin_size):
    histogram_bins = list(range(0, 101, histogram_bin_size))
    plt.hist(live_probabilities,
             histogram_bins,
             color='blue',
             label='live')
    plt.legend(loc='best')
    plt.savefig(live_distribution_file_path, dpi=200)
    plt.close('all')

    plt.hist(spoof_probabilities,
             histogram_bins,
             color='red',
             label='spoof')
    plt.legend(loc='best')
    plt.savefig(spoof_distribution_file_path, dpi=200)
    plt.close('all')

    plt.hist(live_probabilities,
             histogram_bins,
             color='blue',
             label='live')
    plt.legend(loc='best')
    plt.hist(spoof_probabilities,
             histogram_bins,
             color='red',
             label='spoof')
    plt.legend(loc='best')
    plt.savefig(mixed_distributions_file_path, dpi=200)
    plt.close('all')


def get_fnr_and_fpr_per_threshold(histogram_bin_size,
                                  live_count,
                                  live_probabilities,
                                  spoof_count,
                                  spoof_probabilities):
    histogram_bins = list(range(0, 101, histogram_bin_size))
    false_positive_rate_per_threshold = calculate_false_positive_rate_per_threshold(spoof_count,
                                                                                    histogram_bins,
                                                                                    spoof_probabilities)
    false_negative_rate_per_threshold = calculate_false_negative_rate_per_threshold(live_count,
                                                                                    histogram_bins,
                                                                                    live_probabilities)
    return false_positive_rate_per_threshold, false_negative_rate_per_threshold


def generate_distributions_report(false_positive_rate_per_threshold,
                                  false_negative_rate_per_threshold,
                                  live_probabilities,
                                  spoof_probabilities,
                                  histogram_bin_size,
                                  report_file_path,
                                  live_distribution_file_path,
                                  spoof_distribution_file_path,
                                  mixed_distributions_file_path):
    write_distributions_table(report_file_path,
                              false_positive_rate_per_threshold,
                              false_negative_rate_per_threshold,
                              histogram_bin_size)
    append_lines_to_report(report_file_path, '')
    plot_distributions(live_distribution_file_path,
                       spoof_distribution_file_path,
                       mixed_distributions_file_path,
                       live_probabilities,
                       spoof_probabilities,
                       histogram_bin_size)


def generate_evaluation_report(test_name,
                               test_data_generator,
                               evaluate_final_model,
                               histogram_bin_size=1):
    keras.backend.clear_session()

    classifier_head_file, pre_trained_model_file = get_evaluation_models_file_names(evaluate_final_model)
    pre_trained_model = tf.keras.models.load_model(pre_trained_model_file, compile=False)
    classifier_head = tf.keras.models.load_model(classifier_head_file,
                                                 compile=False,
                                                 custom_objects={
                                                     'NormalizedDotProduct': NormalizedDotProduct
                                                 })
    classifier_model = build_classifier_model_from_existing(pre_trained_model,
                                                            classifier_head,
                                                            with_softmax=True)
    classifier_model.compile(loss=tf.keras.losses.binary_crossentropy,
                             optimizer=default_optimizer(),
                             metrics=[fpr, fnr, aer])

    all_true_labels, all_predicted_scores = [], []
    batches = len(test_data_generator)
    progress_bar = tqdm(total=batches)
    for i in range(batches):
        input_batch, true_labels = test_data_generator[i]
        predicted_labels = classifier_model(input_batch, training=False)
        all_true_labels.extend(list(np.argmax(true_labels, axis=-1)))
        all_predicted_scores.extend(list(predicted_labels[:, 1]))
        progress_bar.update()
    progress_bar.close()

    all_predicted_scores = np.array(all_predicted_scores, np.float32)
    all_true_labels = np.array(all_true_labels, bool)

    live_probabilities = all_predicted_scores[all_true_labels] * 100
    spoof_probabilities = all_predicted_scores[~all_true_labels] * 100
    live_count = np.count_nonzero(all_true_labels)
    spoof_count = np.count_nonzero(~all_true_labels)

    false_positive_rate_per_threshold, false_negative_rate_per_threshold = get_fnr_and_fpr_per_threshold(
        histogram_bin_size,
        live_count,
        live_probabilities,
        spoof_count,
        spoof_probabilities)

    best_threshold = (np.argmin(
        np.abs(false_positive_rate_per_threshold - false_negative_rate_per_threshold)) + 1) / 100

    prefix = test_name.replace(' ', '_')
    serialize_object(f'{prefix}_best_threshold.pickle', best_threshold)
    print(f'found best threshold at: {best_threshold * 100}')

    write_classification_report('evaluation_report.txt',
                                all_true_labels.copy(),
                                all_predicted_scores.copy(),
                                best_threshold)

    generate_distributions_report(false_positive_rate_per_threshold,
                                  false_negative_rate_per_threshold,
                                  live_probabilities,
                                  spoof_probabilities,
                                  histogram_bin_size,
                                  'evaluation_report.txt',
                                  f'{prefix}_live_distribution.png',
                                  f'{prefix}_spoof_distribution.png',
                                  f'{prefix}_mixed_distributions.png')


def get_evaluation_models_file_names(evaluate_final_model):
    if evaluate_final_model:
        pre_trained_model_file = 'pre_trained_model.h5'
        classifier_head_file = 'classifier_head.h5'
    else:
        pre_trained_model_file = 'pre_trained_model_best.h5'
        classifier_head_file = 'classifier_head_best.h5'
    return classifier_head_file, pre_trained_model_file


def evaluate_model(test_name,
                   test_live_paths,
                   test_spoof_paths,
                   evaluate_final_model=False):
    keras.backend.clear_session()
    classifier_head_file, pre_trained_model_file = get_evaluation_models_file_names(evaluate_final_model)
    pre_trained_model = tf.keras.models.load_model(pre_trained_model_file, compile=False)
    classifier_head = tf.keras.models.load_model(classifier_head_file,
                                                 compile=False,
                                                 custom_objects={
                                                     'NormalizedDotProduct': NormalizedDotProduct
                                                 })
    classifier_model = build_classifier_model_from_existing(pre_trained_model,
                                                            classifier_head)
    classifier_model.compile(loss=additive_margin_softmax_with_focal_loss,
                             optimizer=default_optimizer(),
                             metrics=[fpr, fnr, aer])

    test_data_generator = DataGenerator(test_live_paths, test_spoof_paths, VALIDATION_BATCH_SIZE)

    results = classifier_model.evaluate(test_data_generator,
                                        batch_size=VALIDATION_BATCH_SIZE)

    append_lines_to_report('evaluation_report.txt',
                           f'test loss: {results[0]}, test fpr: {results[1]}, test fnr: {results[2]}, test aer: {results[3]}\n')

    generate_evaluation_report(test_name,
                               test_data_generator,
                               evaluate_final_model)


class FinalTrainingMode(Enum):
    FromLastCheckpoint = 1
    FromBestCheckpoint = 2


def model_files_from_final_training_mode(training_mode):
    if training_mode == FinalTrainingMode.FromLastCheckpoint:
        pre_trained_model_file = 'pre_trained_model.h5'
        classifier_head_file = 'classifier_head.h5'
    else:
        pre_trained_model_file = 'pre_trained_model_best.h5'
        classifier_head_file = 'classifier_head_best.h5'
    return pre_trained_model_file, classifier_head_file


def train_final_model(train_live_paths,
                      train_spoof_paths,
                      learning_rate=DEFAULT_LEARNING_RATE,
                      training_mode=FinalTrainingMode.FromBestCheckpoint):
    pre_trained_model_file, classifier_head_file = model_files_from_final_training_mode(training_mode)
    pre_trained_model = tf.keras.models.load_model(pre_trained_model_file, compile=False)
    classifier_head = tf.keras.models.load_model(classifier_head_file, compile=False,
                                                 custom_objects={
                                                     'NormalizedDotProduct': NormalizedDotProduct
                                                 })
    classifier_model = build_classifier_model_from_existing(pre_trained_model,
                                                            classifier_head,
                                                            pre_trained_model_trainable=True)

    classifier_head.summary()
    classifier_model.summary()

    train_data_generator = DataGenerator(train_live_paths,
                                         train_spoof_paths,
                                         TRAIN_BATCH_SIZE)

    classifier_model.compile(loss=tf.keras.losses.binary_crossentropy,
                             optimizer=tf.keras.optimizers.SGD(learning_rate=learning_rate,
                                                               momentum=DEFAULT_SGD_MOMENTUM,
                                                               nesterov=True,
                                                               weight_decay=DEFAULT_WEIGHT_DECAY),
                             metrics=[fpr, fnr, aer])

    append_lines_to_report('evaluation_report.txt', '\n[final training log]\n')
    update_report_callback = tf.keras.callbacks.LambdaCallback(
        on_epoch_end=lambda epoch, logs: append_lines_to_report('evaluation_report.txt',
                                                                epoch_metrics_log(epoch, logs, with_validation=False)))

    checkpoint_callback1 = AltModelCheckpoint(alternate_model=pre_trained_model,
                                              filepath='pre_trained_model.h5')
    checkpoint_callback2 = AltModelCheckpoint(alternate_model=classifier_head,
                                              filepath='classifier_head.h5')

    model_history = classifier_model.fit(train_data_generator,
                                         batch_size=TRAIN_BATCH_SIZE,
                                         epochs=FINAL_TRAIN_EPOCHS,
                                         callbacks=[update_report_callback,
                                                    checkpoint_callback1,
                                                    checkpoint_callback2])

    serialize_object('final_history.pickle', model_history)


def main():
    if not os.path.isfile('train_live_paths.pickle') or \
            not os.path.isfile('train_spoof_paths.pickle') or \
            not os.path.isfile('validation_live_paths.pickle') or \
            not os.path.isfile('validation_spoof_paths.pickle') or \
            not os.path.isfile('test_live_paths.pickle') or \
            not os.path.isfile('test_spoof_paths.pickle'):
        train_live_paths = get_image_paths(os.path.join(DATA_PATH, 'train', 'live'))
        train_spoof_paths = get_image_paths(os.path.join(DATA_PATH, 'train', 'spoof'))
        validation_live_paths = get_image_paths(os.path.join(DATA_PATH, 'validation', 'live'))
        validation_spoof_paths = get_image_paths(os.path.join(DATA_PATH, 'validation', 'spoof'))
        test_live_paths = get_image_paths(os.path.join(DATA_PATH, 'test', 'live'))
        test_spoof_paths = get_image_paths(os.path.join(DATA_PATH, 'test', 'spoof'))

        random.shuffle(train_live_paths)
        random.shuffle(train_spoof_paths)
        random.shuffle(validation_live_paths)
        random.shuffle(validation_spoof_paths)
        random.shuffle(test_live_paths)
        random.shuffle(test_spoof_paths)

        append_lines_to_report('evaluation_report.txt', f'number of live training samples: {len(train_live_paths)}\n')
        append_lines_to_report('evaluation_report.txt', f'number of spoof training samples: {len(train_spoof_paths)}\n')
        append_lines_to_report('evaluation_report.txt',
                               f'number of live validation samples: {len(validation_live_paths)}\n')
        append_lines_to_report('evaluation_report.txt',
                               f'number of spoof validation samples: {len(validation_spoof_paths)}\n')
        append_lines_to_report('evaluation_report.txt', f'number of live test samples: {len(test_live_paths)}\n')
        append_lines_to_report('evaluation_report.txt', f'number of spoof test samples: {len(test_spoof_paths)}\n')

        serialize_object('train_live_paths.pickle', train_live_paths)
        serialize_object('train_spoof_paths.pickle', train_spoof_paths)
        serialize_object('validation_live_paths.pickle', validation_live_paths)
        serialize_object('validation_spoof_paths.pickle', validation_spoof_paths)
        serialize_object('test_live_paths.pickle', test_live_paths)
        serialize_object('test_spoof_paths.pickle', test_spoof_paths)
    else:
        train_live_paths = deserialize_object('train_live_paths.pickle')
        train_spoof_paths = deserialize_object('train_spoof_paths.pickle')
        validation_live_paths = deserialize_object('validation_live_paths.pickle')
        validation_spoof_paths = deserialize_object('validation_spoof_paths.pickle')
        test_live_paths = deserialize_object('test_live_paths.pickle')
        test_spoof_paths = deserialize_object('test_spoof_paths.pickle')

    if not os.path.isfile('balanced_train_live_paths.pickle') or \
            not os.path.isfile('balanced_train_spoof_paths.pickle') or \
            not os.path.isfile('balanced_validation_live_paths.pickle') or \
            not os.path.isfile('balanced_validation_spoof_paths.pickle'):
        for live_paths, spoof_paths, live_paths_file_name, spoof_paths_file_name in ((train_live_paths,
                                                                                      train_spoof_paths,
                                                                                      'balanced_train_live_paths.pickle',
                                                                                      'balanced_train_spoof_paths.pickle'),
                                                                                     (validation_live_paths,
                                                                                      validation_spoof_paths,
                                                                                      'balanced_validation_live_paths.pickle',
                                                                                      'balanced_validation_spoof_paths.pickle')):
            if len(live_paths) != len(spoof_paths) or \
                    len(live_paths) % MAX_BATCH_SIZE != 0 or \
                    len(spoof_paths) % MAX_BATCH_SIZE != 0:
                paths_count = max(len(live_paths), len(spoof_paths))
                paths_count = make_divisible_by(paths_count, MAX_BATCH_SIZE)
                bootstrap_array_up_to_count(live_paths, paths_count)
                bootstrap_array_up_to_count(spoof_paths, paths_count)
                serialize_object(live_paths_file_name, live_paths)
                serialize_object(spoof_paths_file_name, spoof_paths)
            else:
                serialize_object(live_paths_file_name, live_paths)
                serialize_object(spoof_paths_file_name, spoof_paths)

            append_lines_to_report('evaluation_report.txt',
                                   f'\nbootstrapping to make live and spoof samples equal...\n')
            append_lines_to_report('evaluation_report.txt',
                                   f'number of live training samples: {len(train_live_paths)}\n')
            append_lines_to_report('evaluation_report.txt',
                                   f'number of spoof training samples: {len(train_spoof_paths)}\n')
            append_lines_to_report('evaluation_report.txt',
                                   f'number of live validation samples: {len(validation_live_paths)}\n')
            append_lines_to_report('evaluation_report.txt',
                                   f'number of spoof validation samples: {len(validation_spoof_paths)}\n')
    else:
        train_live_paths = deserialize_object('balanced_train_live_paths.pickle')
        train_spoof_paths = deserialize_object('balanced_train_spoof_paths.pickle')
        validation_live_paths = deserialize_object('balanced_validation_live_paths.pickle')
        validation_spoof_paths = deserialize_object('balanced_validation_spoof_paths.pickle')

    if not os.path.isfile('pre_trained_model_warmup.h5') or \
            not os.path.isfile('classifier_head_warmup.h5'):
        train_initial_model(train_live_paths, train_spoof_paths, validation_live_paths, validation_spoof_paths)

    if TRAIN_MODEL:
        # 1
        train_model(train_live_paths,
                    train_spoof_paths,
                    validation_live_paths,
                    validation_spoof_paths)
        # 2
        train_model(train_live_paths,
                    train_spoof_paths,
                    validation_live_paths,
                    validation_spoof_paths,
                    learning_rate=DEFAULT_LEARNING_RATE * 0.1,
                    training_mode=TrainingMode.FromBestCheckpoint)

    if EVALUATE_MODEL:
        for test_name, spoof_paths in (('global spoof',
                                        list(filter(lambda x: x.find('_local_') == -1, test_spoof_paths))),
                                       ('local spoof',
                                        list(filter(lambda x: x.find('_local_') != -1, test_spoof_paths))),
                                       ('global and local spoof',
                                        test_spoof_paths.copy())):
            live_paths = test_live_paths.copy()
            if len(live_paths) != len(spoof_paths) or \
                    len(live_paths) % MAX_BATCH_SIZE != 0 or \
                    len(spoof_paths) % MAX_BATCH_SIZE != 0:
                paths_count = max(len(live_paths), len(spoof_paths))
                paths_count = make_divisible_by(paths_count, MAX_BATCH_SIZE)
                bootstrap_array_up_to_count(live_paths, paths_count)
                bootstrap_array_up_to_count(spoof_paths, paths_count)
            append_lines_to_report('evaluation_report.txt', f'\n\n[evaluation report - {test_name}]\n')
            evaluate_model(test_name, live_paths, spoof_paths)

    if GENERATE_FINAL_MODEL:
        pre_trained_model = tf.keras.models.load_model('pre_trained_model_best.h5', compile=False)
        classifier_head = tf.keras.models.load_model('classifier_head_best.h5', compile=False,
                                                     custom_objects={
                                                         'NormalizedDotProduct': NormalizedDotProduct
                                                     })
        for layer in classifier_head.layers:
            layer.trainable = False
        final_model = build_classifier_model_from_existing(pre_trained_model,
                                                           classifier_head,
                                                           with_softmax=True)
        final_model.save('final_model.h5')


if __name__ == '__main__':
    main()
