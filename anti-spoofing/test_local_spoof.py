import math
import os
import random

import cv2
import numpy as np
from matplotlib import pyplot as plt

from prepare_data import get_image_paths, read_image
from train import build_classifier_model_from_existing, NormalizedDotProduct
import tensorflow as tf

DATA_PATH = '/home/user/Downloads/data'

MIN_AREA_RATIO = 0.05
MAX_AREA_RATIO = 0.6
MIN_SIDE_RATIO = 0.1
MAX_SIDE_RATIO = 0.6
EPSILON = 1e-6


def to_int(x):
    return int(round(x))


def draw_rotated_rectangle(image, rectangle, angle):
    theta = math.radians(angle)
    rx, ry = rectangle[0]
    rw, rh = rectangle[1]

    sin = math.sin(theta)
    if abs(sin) < EPSILON:
        w = rw
        h = rh
        sin = 0.0
        cos = 1.0
    else:
        sin = abs(sin)
        cos = abs(math.cos(theta))
        w = (rw * cos - rh * sin) / (2 * cos ** 2 - 1)
        h = (rw - w * cos) / sin
    if sin * cos > 0:
        point1 = (rx, ry + w * sin)
        point2 = (rx + w * cos, ry)
        point3 = (rx + rw, ry + h * cos)
        point4 = (rx + h * sin, ry + rh)
    else:
        point1 = (rx + h * sin, ry)
        point2 = (rx + rw, ry + w * sin)
        point3 = (rx + w * cos, ry + rh)
        point4 = (rx, ry + h * cos)

    point1 = (int(round(point1[0])), int(round(point1[1])))
    point2 = (int(round(point2[0])), int(round(point2[1])))
    point3 = (int(round(point3[0])), int(round(point3[1])))
    point4 = (int(round(point4[0])), int(round(point4[1])))

    cv2.line(image, point1, point2, 255, 1)
    cv2.line(image, point2, point3, 255, 1)
    cv2.line(image, point3, point4, 255, 1)
    cv2.line(image, point4, point1, 255, 1)

    points = np.transpose(np.where(np.transpose(image == 255)))

    cv2.fillPoly(image, pts=[points], color=255)


def get_random_shape_mask(x, y, width, height):
    mask = np.zeros((224, 224), np.uint8)
    if random.choice([1, 2]) == 1:
        cv2.ellipse(mask, ((x + width) // 2, ((y + height) // 2)), (width // 2, height // 2),
                    angle=random.randint(-20, 20),
                    startAngle=random.randint(-20, 20),
                    endAngle=360,
                    color=255,
                    thickness=-1)
    else:
        draw_rotated_rectangle(mask, ((x, y), (width, height)), random.randint(-20, 20))
    return mask == 255


def get_random_rectangle():
    area = random.uniform(MIN_AREA_RATIO, MAX_AREA_RATIO) * 224 ** 2
    width = random.uniform(MIN_SIDE_RATIO, MAX_SIDE_RATIO) * 224
    height = min(area / width, 224)
    width = to_int(width)
    height = to_int(height)
    x = random.randint(0, 224 - width)
    y = random.randint(0, 224 - height)
    return x, y, width, height


def generate_local_spoof(live_image, spoof_image):
    if random.choice([1, 2]) == 1:
        x, y, width, height = get_random_rectangle()
        mask = get_random_shape_mask(x, y, width, height)
    else:
        x1, y1, width1, height1 = get_random_rectangle()
        x2, y2, width2, height2 = get_random_rectangle()
        mask1 = get_random_shape_mask(x1, y1, width1, height1)
        mask2 = get_random_shape_mask(x2, y2, width2, height2)
        mask = mask1 | mask2

    live_image_copy = np.array(live_image)
    for i in range(3):
        live_image_copy[:, :, i][mask] = spoof_image[:, :, i][mask]
    return live_image_copy


def main():
    test_live_paths = get_image_paths(os.path.join(DATA_PATH, 'test', 'live'))
    test_spoof_paths = get_image_paths(os.path.join(DATA_PATH, 'test', 'spoof'))
    spoof_image = read_image(random.choice(test_spoof_paths))
    live_image = read_image(random.choice(test_live_paths))
    generate_local_spoof(live_image, spoof_image)

    pre_trained_model = tf.keras.models.load_model('/home/u764/Downloads/pre_trained_model_best.h5', compile=False)
    classifier_head = tf.keras.models.load_model('/home/u764/Downloads/classifier_head_best.h5', compile=False,
                                                 custom_objects={'NormalizedDotProduct': NormalizedDotProduct})
    for layer in classifier_head.layers:
        layer.trainable = False
    final_model = build_classifier_model_from_existing(pre_trained_model,
                                                       classifier_head,
                                                       with_softmax=True)

    image = read_image('/home/u764/Downloads/test.png')
    plt.imshow(image)
    plt.show()
    print(final_model.predict(np.array([image])))


if __name__ == '__main__':
    main()
