import math
import sys
import time

import cv2
import numpy as np
from matplotlib import pyplot as plt

EPSILON = 1e-6
ID_CARD_FINAL_WIDTH = 750


def resize_image_by_factor(image, factor, interpolation):
    if abs(factor - 1.0) < EPSILON:
        return image
    image = cv2.resize(image,
                       (0, 0),
                       fx=factor,
                       fy=factor,
                       interpolation=interpolation)
    return image


def otsu_threshold(image_gray):
    return cv2.threshold(image_gray,
                         0,
                         255,
                         cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]


def edge_detection_gray(gray_image,
                        edge_kernel_size=5,
                        smoothing_kernel_size=5):
    if smoothing_kernel_size > 0:
        blurred = cv2.GaussianBlur(gray_image, (smoothing_kernel_size, smoothing_kernel_size), 0.0)
    else:
        blurred = gray_image
    sobel_x = cv2.Sobel(blurred, cv2.CV_32F, 1, 0, ksize=edge_kernel_size)
    sobel_y = cv2.Sobel(blurred, cv2.CV_32F, 0, 1, ksize=edge_kernel_size)
    gradients = np.hypot(sobel_x, sobel_y)
    min_gradients = np.min(gradients)
    max_gradients = np.max(gradients)
    gradients = (gradients - min_gradients) / (max_gradients - min_gradients) * 255
    return gradients.astype(np.uint8)


def text_detection(image):
    gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    gray_image = cv2.GaussianBlur(gray_image, (5, 5), 0)
    black_hat_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (21, 21))
    gray_image = cv2.morphologyEx(gray_image, cv2.MORPH_BLACKHAT, black_hat_kernel)
    edges = edge_detection_gray(gray_image, smoothing_kernel_size=0)
    return otsu_threshold(edges)


def otsu_threshold_value(image_gray):
    return int(cv2.threshold(image_gray,
                             0,
                             255,
                             cv2.THRESH_BINARY + cv2.THRESH_OTSU)[0])


def remove_noise(binary_image,
                 stats_filter):
    labels_count, labeled_image, stats, centroids = cv2.connectedComponentsWithStats(binary_image, 8, cv2.CV_32S)
    chosen_labels = stats_filter(stats)
    chosen_labels = np.where(chosen_labels)[0] + 1
    result_image = np.zeros(binary_image.shape, np.uint8)
    for i in range(chosen_labels.shape[0]):
        label = chosen_labels[i]
        x, y, w, h = stats[label, :4]
        mask = labeled_image[y:y + h, x:x + w] == label
        result_image[y:y + h, x: x + w][mask] = 255
    return result_image


def noise_removal_for_text_extraction(image_size,
                                      text_detection_min_area,
                                      text_detection_max_width_ratio,
                                      text_detection_max_height_ratio,
                                      text_detection_max_area_to_image_area_ratio):
    def curry(stats):
        return (stats[1:, -1] >= text_detection_min_area) & \
            (stats[1:, 2] <= image_size[1] * text_detection_max_width_ratio) & \
            (stats[1:, 3] <= image_size[0] * text_detection_max_height_ratio) & \
            (stats[1:, -1] <= np.prod(image_size) * text_detection_max_area_to_image_area_ratio)

    return curry


def rectangular_morphology(image,
                           morphological_operation,
                           kernel_width,
                           kernel_height,
                           iterations=1):
    return cv2.morphologyEx(image,
                            morphological_operation,
                            cv2.getStructuringElement(cv2.MORPH_RECT,
                                                      (kernel_width, kernel_height)),
                            iterations=iterations)


def get_text_mask(image):
    text_detection_min_area = 20
    text_detection_max_width_ratio = 0.25
    text_detection_max_height_ratio = 0.25
    text_detection_max_area_to_image_area_ratio = 0.005
    text_detection_closing_kernel_size = 5
    text_detection_closing_kernel_iterations = 3
    text_detection_closed_min_area_to_image_area_ratio = 0.025
    text_detection_opening_kernel_size = 5
    text_detection_opening_kernel_iterations = 1
    text_detection_opened_min_height_ratio = 0.05
    text_detection_opened_min_width_ratio = 0.05
    text_detection_final_closing_kernel_size = 7
    text_detection_final_closing_kernel_iterations = 1

    detected_text = text_detection(image)
    #----------------- NEW ---------------------
    inverted = 255 - detected_text
    inverted = pad_image(inverted, 50)

    inverted = rectangular_morphology(inverted,
                                      cv2.MORPH_ERODE,
                                      7,
                                      7,
                                      1)
    labels_count, labeled_image, stats, centroids = cv2.connectedComponentsWithStats(inverted, 8, cv2.CV_32S)
    label = np.argmax(stats[1:, -1]) + 1
    largest_connected_component = np.array(labeled_image == label, np.uint8) * 255
    inverted = rectangular_morphology(largest_connected_component,
                                      cv2.MORPH_DILATE,
                                      7,
                                      7,
                                      1)

    inverted = rectangular_morphology(inverted,
                                      cv2.MORPH_CLOSE,
                                      21,
                                      21,
                                      3)
    inverted = inverted[50:-50, 50:-50]
    detected_text = detected_text & inverted
    # ----------------- NEW ---------------------

    image_size = (image.shape[1], image.shape[0])
    detected_text = remove_noise(detected_text,
                                 noise_removal_for_text_extraction(image_size,
                                                                   text_detection_min_area,
                                                                   text_detection_max_width_ratio,
                                                                   text_detection_max_height_ratio,
                                                                   text_detection_max_area_to_image_area_ratio))

    detected_text = cv2.morphologyEx(detected_text,
                                     cv2.MORPH_CLOSE,
                                     np.ones((text_detection_closing_kernel_size,
                                              text_detection_closing_kernel_size), np.uint8),
                                     iterations=text_detection_closing_kernel_iterations)

    detected_text = remove_noise(detected_text,
                                 lambda stats: stats[1:, -1] <= np.prod(
                                     image_size) * text_detection_closed_min_area_to_image_area_ratio)

    detected_text = cv2.morphologyEx(detected_text,
                                     cv2.MORPH_OPEN,
                                     np.ones((text_detection_opening_kernel_size,
                                              text_detection_opening_kernel_size), np.uint8),
                                     iterations=text_detection_opening_kernel_iterations)

    detected_text = remove_noise(detected_text,
                                 lambda s: (s[1:, 2] >= image.shape[1] * text_detection_opened_min_width_ratio) |
                                           (s[1:, 3] >= image.shape[0] * text_detection_opened_min_height_ratio))

    return cv2.morphologyEx(detected_text,
                            cv2.MORPH_CLOSE,
                            np.ones((text_detection_final_closing_kernel_size,
                                     text_detection_final_closing_kernel_size), np.uint8),
                            iterations=text_detection_final_closing_kernel_iterations)


def max_reduce(image):
    result = image[:, :, 0]
    for i in range(1, image.shape[-1]):
        result = np.maximum(result, image[:, :, i])
    return result


def gradients_from_sobel_filters(sobel_x, sobel_y):
    sobel_x = max_reduce(np.abs(sobel_x))
    sobel_y = max_reduce(np.abs(sobel_y))
    gradients = np.hypot(sobel_x, sobel_y)
    min_gradients = np.min(gradients)
    max_gradients = np.max(gradients)
    gradients = (gradients - min_gradients) / (max_gradients - min_gradients) * 255
    return gradients.astype(np.uint8)


def edge_detection(image,
                   edge_kernel_size=5,
                   gaussian_blur_kernel=5,
                   median_blur_kernel=0):
    if median_blur_kernel > 0:
        image = cv2.medianBlur(image, median_blur_kernel)
    if gaussian_blur_kernel > 0:
        image = cv2.GaussianBlur(image, (gaussian_blur_kernel, gaussian_blur_kernel), 1.2)
    sobel_x = cv2.Sobel(image, cv2.CV_32F, 1, 0, ksize=edge_kernel_size)
    sobel_y = cv2.Sobel(image, cv2.CV_32F, 0, 1, ksize=edge_kernel_size)
    return gradients_from_sobel_filters(sobel_x, sobel_y)


def close_edges(edges, kernel_size, iterations=1):
    result = cv2.morphologyEx(edges,
                              cv2.MORPH_CLOSE,
                              np.ones((1, kernel_size), np.uint8),
                              iterations=iterations)
    result = cv2.morphologyEx(result,
                              cv2.MORPH_CLOSE,
                              np.ones((kernel_size, 1), np.uint8),
                              iterations=iterations)

    return result


def edge_hysteresis(edges, high_threshold, low_threshold):
    strong_edges = np.array(edges)
    strong_edges[strong_edges < high_threshold] = 0
    strong_edges[strong_edges >= high_threshold] = 255

    weak_edges = np.array(edges)
    weak_edges[strong_edges == 255] = 0
    weak_edges[weak_edges <= low_threshold] = 0

    mask = np.array(weak_edges > 0, np.uint8) * 255 | strong_edges

    while True:
        new_strong_edges = cv2.dilate(strong_edges,
                                      cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3)))
        new_strong_edges = (new_strong_edges & mask)
        if np.all(new_strong_edges == strong_edges):
            break
        strong_edges = new_strong_edges

    return strong_edges


def calculate_hysteresis_score(gradients,
                               threshold,
                               foreground_mask,
                               foreground_pixels_count,
                               high_threshold,
                               contour_area_cutoff_area_ratio=0.05,
                               edge_closing_kernel_size=3):
    edges = edge_hysteresis(gradients,
                            high_threshold,
                            threshold)
    closed_edges = edges
    if edge_closing_kernel_size > 0:
        closed_edges = close_edges(edges,
                                   edge_closing_kernel_size,
                                   1)

    contours, hierarchy = cv2.findContours(closed_edges, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    max_score = 0.0
    for contour in contours:
        contour_area = cv2.moments(contour)['m00']
        if contour_area < contour_area_cutoff_area_ratio * np.prod(edges.shape):
            continue

        contour_area_mask = np.zeros(closed_edges.shape[0:2], np.uint8)
        cv2.fillPoly(contour_area_mask, pts=[contour], color=255)
        contour_area_mask = cv2.dilate(contour_area_mask, np.ones((3, 3), np.uint8))
        masked_foreground_pixels_count = np.count_nonzero(foreground_mask[contour_area_mask == 255])

        if masked_foreground_pixels_count == foreground_pixels_count:
            return 1.0

        foreground_ratio = masked_foreground_pixels_count / foreground_pixels_count
        if foreground_ratio > max_score:
            max_score = foreground_ratio

    return max_score


def low_hysteresis_threshold_binary_search(gradients,
                                           foreground_mask,
                                           high_threshold,
                                           min_low_threshold=4,
                                           contour_area_cutoff_area_ratio=0.05,
                                           edge_closing_kernel_size=3,
                                           epsilon=1e-7):
    max_low_threshold = high_threshold
    foreground_pixels_count = np.count_nonzero(foreground_mask == 255)

    score1 = calculate_hysteresis_score(gradients,
                                        min_low_threshold,
                                        foreground_mask,
                                        foreground_pixels_count,
                                        high_threshold,
                                        contour_area_cutoff_area_ratio,
                                        edge_closing_kernel_size)
    score2 = calculate_hysteresis_score(gradients,
                                        max_low_threshold,
                                        foreground_mask,
                                        foreground_pixels_count,
                                        high_threshold,
                                        contour_area_cutoff_area_ratio,
                                        edge_closing_kernel_size)
    if abs(score1 - score2) < epsilon:
        return max_low_threshold

    thresh1 = min_low_threshold
    thresh2 = max_low_threshold

    while (thresh2 - thresh1) > 1:
        thresh = (thresh1 + thresh2) // 2
        score = calculate_hysteresis_score(gradients,
                                           thresh,
                                           foreground_mask,
                                           foreground_pixels_count,
                                           high_threshold,
                                           contour_area_cutoff_area_ratio,
                                           edge_closing_kernel_size)

        if abs(score - score1) < epsilon:
            thresh1 = thresh
            score1 = score
        else:
            thresh2 = thresh
            score2 = score

    if abs(score1 - score2) < epsilon:
        return thresh2

    return thresh1


def pad_image(image, padding_width):
    if len(image.shape) < 3:
        return np.pad(image,
                      ((padding_width, padding_width), (padding_width, padding_width)),
                      'constant',
                      constant_values=((0, 0), (0, 0)))

    channels = []
    for i in range(image.shape[-1]):
        image_i = image[:, :, i]
        image_i = np.pad(image_i,
                         ((padding_width, padding_width), (padding_width, padding_width)),
                         'constant',
                         constant_values=((0, 0), (0, 0)))
        channels.append(image_i)

    return np.dstack(tup=tuple(channels))


def find_best_region_mask_with_stats(candidate_areas_mask,
                                     candidate_best_area_cutoff_area_ratio,
                                     labels_count,
                                     labeled_connected_components,
                                     components_stats,
                                     foreground_mask,
                                     image_luv,
                                     foreground_pixels_count):
    mask_area = candidate_areas_mask.shape[0] * candidate_areas_mask.shape[1]
    bins = np.arange(0, components_stats.shape[0] + 1)
    foreground_label_counts = np.histogram(labeled_connected_components[foreground_mask == 255], bins=bins)[0]
    candidate_areas_label_counts = np.histogram(labeled_connected_components[candidate_areas_mask == 255], bins=bins)[0]
    candidate_areas_labels_mask = np.array(candidate_areas_label_counts > 0, np.float32)
    region_areas_mask = np.array(components_stats[:, -1] >= mask_area * candidate_best_area_cutoff_area_ratio,
                                 np.float32)
    foreground_labels_ratios = foreground_label_counts / foreground_pixels_count
    region_area_ratios = components_stats[:, -1] / foreground_pixels_count
    scores = (foreground_labels_ratios + region_area_ratios * 0.1) * candidate_areas_labels_mask * region_areas_mask
    best_label = np.argmax(scores)

    best_region_mask = labeled_connected_components == best_label

    best_region_mask_raveled = best_region_mask.ravel()
    image_l = image_luv[:, :, 0].ravel()[best_region_mask_raveled]
    image_u = image_luv[:, :, 1].ravel()[best_region_mask_raveled]
    image_v = image_luv[:, :, 2].ravel()[best_region_mask_raveled]

    mu = np.array([np.mean(image_l),
                   np.mean(image_u),
                   np.mean(image_v)])

    sigma = np.array([np.std(image_l),
                      np.std(image_u),
                      np.std(image_v)])

    n = np.count_nonzero(best_region_mask)

    return best_region_mask, n, mu, sigma


def find_similar_colored_regions_labels(candidate_areas_mask,
                                        image_luv,
                                        labels_count,
                                        labeled_connected_components,
                                        components_stats,
                                        min_candidate_area_pixels,
                                        region_mu,
                                        region_sigma,
                                        region_n,
                                        region_colors_distance_threshold):
    labels = []

    for label in range(0, labels_count):
        if components_stats[label, -1] < min_candidate_area_pixels:
            continue

        region_mask = labeled_connected_components == label
        n = np.count_nonzero(candidate_areas_mask.ravel()[region_mask.ravel()])
        if n == 0:
            continue

        region_mask_raveled = region_mask.ravel()
        image_l = image_luv[:, :, 0].ravel()[region_mask_raveled]
        image_u = image_luv[:, :, 1].ravel()[region_mask_raveled]
        image_v = image_luv[:, :, 2].ravel()[region_mask_raveled]

        if image_l.size == 0 or image_u.size == 0 or image_v.size == 0:
            continue

        mu = np.array([np.mean(image_l),
                       np.mean(image_u),
                       np.mean(image_v)])

        sigma = np.array([np.std(image_l),
                          np.std(image_u),
                          np.std(image_v)])

        sigma_mask = (region_sigma < EPSILON) & (sigma < EPSILON)
        if np.any(sigma_mask):
            mu_mask = (np.abs(region_mu - mu) > EPSILON) & sigma_mask
            if np.any(mu_mask):
                fisher_distance = 1000.0
            else:
                fisher_distance = 0.0
        else:
            nominator = np.sqrt(region_n + n) * np.abs(region_mu - mu)
            denominator = np.sqrt(region_n * region_sigma ** 2 + n * sigma ** 2)
            fisher_distance = nominator / denominator
            fisher_distance = np.max(fisher_distance)

        if fisher_distance < region_colors_distance_threshold:
            labels.append((label, region_mask))

    return labels


def filter_regions_and_get_mask_with_convex_hull(region_labels,
                                                 foreground_mask):
    regions_mask = np.zeros(foreground_mask.shape, np.uint8)
    all_vertices = []
    filtered_labels = []

    for i in range(len(region_labels)):
        label, region_mask = region_labels[i]

        if np.count_nonzero(foreground_mask[region_mask]) == 0:
            continue

        try:
            convex_hull = cv2.convexHull(np.transpose(np.where(np.transpose(region_mask)))).reshape(-1, 2)
        except:
            continue

        regions_mask[region_mask] = 255
        convex_hull_vertices = convex_hull.tolist()

        all_vertices.extend(convex_hull_vertices)
        filtered_labels.append(label)

    if len(filtered_labels) == 0:
        return None

    final_convex_hull = cv2.convexHull(np.array(all_vertices)).reshape(-1, 2)
    convex_hull_vertices = final_convex_hull.tolist()

    return regions_mask, filtered_labels, convex_hull_vertices


def add_intersecting_regions_of_similar_color(regions_mask,
                                              regions_labels,
                                              regions_convex_hull_vertices,
                                              similar_colored_region_labels,
                                              min_candidate_area_intersection_ratio):
    final_convex_region_mask = np.zeros(regions_mask.shape, np.uint8)
    cv2.fillPoly(final_convex_region_mask, [np.array(regions_convex_hull_vertices, np.int32)], 255)
    final_convex_region_mask = final_convex_region_mask == 255

    for label, region_mask in similar_colored_region_labels:
        if label in regions_labels:
            continue

        intersecting_pixels_count = np.count_nonzero(np.logical_and(final_convex_region_mask, region_mask))
        intersection_ratio = intersecting_pixels_count / np.count_nonzero(region_mask)
        if intersection_ratio < min_candidate_area_intersection_ratio:
            continue

        regions_mask[region_mask] = 255


def closing_morphology(image,
                       kernel_dimension_size,
                       power):
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT,
                                       (kernel_dimension_size,
                                        kernel_dimension_size))
    image = cv2.dilate(image, kernel, iterations=power)
    return cv2.erode(image, kernel, iterations=power)


def dilate(image,
           kernel_dimension_size):
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT,
                                       (kernel_dimension_size,
                                        kernel_dimension_size))
    return cv2.dilate(image, kernel)


def clean_binary_image(binary_image,
                       max_noise_size,
                       closing_kernel_size,
                       dilation_kernel_size):
    new_image = remove_noise(binary_image, lambda stats: stats[1:, -1] > max_noise_size)
    binary_image = closing_morphology(new_image, closing_kernel_size, 1)
    return dilate(binary_image, dilation_kernel_size)


def find_largest_contour(image):
    contours, hierarchy = cv2.findContours(image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    max_area = -1
    largest_contour = None
    for contour in contours:
        contour_area = cv2.moments(contour)['m00']
        if contour_area > max_area:
            max_area = contour_area
            largest_contour = contour
    if largest_contour is None:
        return None
    return largest_contour.reshape(-1, 2)


def points_to_parametric_line(point1, point2):
    (x1, y1) = point1
    (x2, y2) = point2
    return np.array([[x2 - x1, x1],
                     [y2 - y1, y1]], np.float32)


def is_parametric_line_vertical(parametric_line):
    a_x, _ = parametric_line[0]
    a_y, _ = parametric_line[1]

    if abs(a_x) <= EPSILON < abs(a_y):
        return True
    else:
        return False


def is_parametric_line_horizontal(parametric_line):
    a_x, _ = parametric_line[0]
    a_y, _ = parametric_line[1]

    if abs(a_y) <= EPSILON < abs(a_x):
        return True
    else:
        return False


def find_lines_intersection_point(line1,
                                  line2):
    if line1 is None or line2 is None:
        return None

    a1_x, b1_x = line1[0]
    a1_y, b1_y = line1[1]

    a2_x, b2_x = line2[0]
    a2_y, b2_y = line2[1]

    mat1 = np.array([[a1_x, -a2_x],
                     [a1_y, -a2_y]], np.float32)
    if np.linalg.det(mat1) == 0:
        return None

    mat2 = np.array([[b2_x - b1_x],
                     [b2_y - b1_y]], np.float32)

    t = np.dot(np.linalg.inv(mat1), mat2).reshape(2)

    point1 = line1[:, 0] * t[0] + line1[:, 1]
    point2 = line2[:, 0] * t[1] + line2[:, 1]

    return (point1 + point2) / 2


class Polygon:
    def __init__(self, vertices):
        vertices_array = np.array(vertices, np.float32)
        self.vertices = vertices
        self.area = cv2.moments(vertices_array)['m00']

        rect = cv2.minAreaRect(vertices_array)
        box = cv2.boxPoints(rect)
        box = np.int0(box)
        self.minimum_rotated_rectangle_area = cv2.moments(box)['m00']


def consolidate_two_vertices_with_minimum_cost(vertices):
    min_positive_area_diff = sys.maxsize
    best_polygon_vertices = vertices
    was_consolidated = False
    vertices_polygon = Polygon(vertices)
    vertices_polygon_area = vertices_polygon.area
    vertices_min_polygon_area = vertices_polygon.minimum_rotated_rectangle_area

    for i in range(len(vertices)):
        parametric_line1 = points_to_parametric_line(vertices[i - 1],
                                                     vertices[i])
        parametric_line2 = points_to_parametric_line(vertices[(i + 1) % len(vertices)],
                                                     vertices[(i + 2) % len(vertices)])

        if (is_parametric_line_vertical(parametric_line1) and is_parametric_line_vertical(parametric_line2)) or \
                (is_parametric_line_horizontal(parametric_line1) and is_parametric_line_horizontal(parametric_line2)):
            intersection_point = (np.array(vertices[i - 1]) + np.array(vertices[(i + 2) % len(vertices)])) / 2
        else:
            intersection_point = find_lines_intersection_point(parametric_line1,
                                                               parametric_line2)

        if intersection_point is None:
            continue

        new_polygon_vertices = [p for p in vertices]
        next_i = (i + 1) % len(vertices)
        if next_i > i:
            new_polygon_vertices.pop(next_i)
            new_polygon_vertices.pop(i)
        else:
            new_polygon_vertices.pop(i)
            new_polygon_vertices.pop(next_i)
        new_polygon_vertices.insert(i, intersection_point)

        new_polygon = Polygon(new_polygon_vertices)
        area_diff1 = new_polygon.area - vertices_polygon_area
        area_diff2 = new_polygon.minimum_rotated_rectangle_area - vertices_min_polygon_area
        area_diff = area_diff1 + area_diff2
        if area_diff < 0:
            continue

        if area_diff < min_positive_area_diff:
            was_consolidated = True
            min_positive_area_diff = area_diff
            best_polygon_vertices = new_polygon_vertices

    if was_consolidated is False:
        for i in range(len(vertices)):
            new_polygon_vertices = [p for p in vertices]
            new_polygon_vertices.pop(i)

            new_polygon = Polygon(new_polygon_vertices)
            area_diff = new_polygon.area - vertices_polygon_area
            if area_diff < 0:
                continue

            if area_diff < min_positive_area_diff:
                was_consolidated = True
                min_positive_area_diff = area_diff
                best_polygon_vertices = new_polygon_vertices

    return best_polygon_vertices, was_consolidated


def adjust_angle_by_quadrant(angle,
                             point):
    x, y = point
    if x < 0 and y > 0:
        return 180 - angle
    if x < 0 and y < 0:
        return 180 + angle
    if x > 0 and y < 0:
        return 360 - angle
    return angle


def compute_angle(origin,
                  point):
    x1 = point[0] - origin[0]
    y1 = origin[1] - point[1]
    if math.fabs(x1) > 1e-6:
        theta = math.fabs(math.atan(y1 / x1)) / math.pi * 180
    else:
        theta = 90
    return adjust_angle_by_quadrant(theta, (x1, y1))


def euclidean_distance(x, y):
    return np.sqrt(np.sum(np.square(np.array(x, np.float32) - np.array(y, np.float32))))


def sort_points_clockwise_polygonal(origin,
                                    points):
    point_angle_pairs = [[points[i], compute_angle(origin, points[i])] for i in range(len(points))]
    point_angle_pairs.sort(key=lambda x: x[1], reverse=True)

    i = 0
    n = len(point_angle_pairs)
    while i < n:
        if abs(point_angle_pairs[i][1] - point_angle_pairs[(i + 1) % n][1]) < EPSILON:
            j = 1
            while j < n - 1:
                if abs(point_angle_pairs[(i + j) % n][1] - point_angle_pairs[(i + j + 1) % n][1]) >= EPSILON:
                    j += 1
                    break
                j += 1
            section = [point_angle_pairs[(i + k) % n] for k in range(0, j)]
            section.sort(key=lambda x: euclidean_distance(point_angle_pairs[i - 1][0], x[0]))

            if i + j > n:
                point_angle_pairs = section[n - i:] + point_angle_pairs[(i + j) % n:i] + section[:n - i]
            else:
                point_angle_pairs = point_angle_pairs[:i] + section + point_angle_pairs[i + j:]
            i += j - 1
            if i > n:
                break
        i += 1
    return [point for point, _ in point_angle_pairs]


def compute_center_of_gravity(points):
    return np.array([np.mean(points[:, :1]), np.mean(points[:, 1:])])


def try_consolidate_polygon_points(polygon_vertices, target_points):
    polygon_vertices = np.array(polygon_vertices)
    polygon_vertices = sort_points_clockwise_polygonal(compute_center_of_gravity(polygon_vertices), polygon_vertices)
    best_polygon = polygon_vertices
    while len(best_polygon) > target_points:
        best_polygon, was_consolidated = consolidate_two_vertices_with_minimum_cost(best_polygon)
        if not was_consolidated:
            break

    return best_polygon


def approximate_contour_polygon(contour, arc_length_percentage):
    if contour is None or len(contour) < 3:
        return np.array([])
    epsilon = arc_length_percentage * cv2.arcLength(contour, True)
    contour = cv2.approxPolyDP(contour, epsilon, True).reshape(-1, 2)
    convex_hull = cv2.convexHull(contour).reshape(-1,
                                                  2)  # list(set([v for v in Polygon(contour.tolist()).exterior.coords]))
    contour = np.array(convex_hull, np.int32)
    contour = try_consolidate_polygon_points(contour, 4)
    return np.array(contour, np.int32)


def distance(point1,
             point2):
    return np.sqrt(np.sum(np.square(np.subtract(point1, point2))))


def auto_correct_vertices_order(vertices):
    point1, point2, point3, point4 = vertices
    side1_length = distance(point1, point2)
    corresponding_side1_length = distance(point4, point3)
    side2_length = distance(point2, point3)
    corresponding_side2_length = distance(point1, point4)
    if max(side1_length, corresponding_side1_length) < min(side2_length, corresponding_side2_length):
        point1, point2, point3, point4 = point2, point3, point4, point1
    point1, point2, point3, point4 = point3, point4, point1, point2
    return np.array([point1,
                     point2,
                     point3,
                     point4])


def find_furthest_point_from(point,
                             points):
    max_distance = -1.0
    furthest_point = None
    furthest_point_index = -1
    for i in range(len(points)):
        p = points[i]
        point_distance = distance(point, p)
        if point_distance > max_distance:
            max_distance = point_distance
            furthest_point = p
            furthest_point_index = i
    return furthest_point, furthest_point_index


def find_point_by_maximality_criteria(points,
                                      maximality_criteria):
    max_value = -1.0
    point_of_max_value = None
    point_index = -1
    for i in range(len(points)):
        p = points[i]
        value = maximality_criteria(p)
        if value > max_value:
            max_value = value
            point_of_max_value = p
            point_index = i
    return point_of_max_value, point_index


def distance_between_point_and_line(line_initial_point,
                                    line_terminal_point,
                                    point):
    x1, y1 = line_initial_point
    x2, y2 = line_terminal_point
    x0, y0 = point
    denominator = distance(line_initial_point, line_terminal_point)
    if denominator < EPSILON:
        return distance(line_initial_point, point)
    return abs((x2 - x1) * (y1 - y0) - (x1 - x0) * (y2 - y1)) / denominator


def sort_points_clockwise_with_indices(origin,
                                       points,
                                       points_indices):
    point_angle_pairs = [[points[i], points_indices[i], compute_angle(origin, points[i])] for i in range(len(points))]
    point_angle_pairs.sort(key=lambda x: x[2], reverse=True)
    return [point for point, _, _ in point_angle_pairs], [point_i for _, point_i, _ in point_angle_pairs]


def calculate_triangle_area(point1,
                            point2,
                            point3):
    return np.fabs(0.5 * (point1[0] * (point2[1] - point3[1])
                          + point2[0] * (point3[1] - point1[1])
                          + point3[0] * (point1[1] - point2[1])))


def calculate_polygon_area(point1,
                           point2,
                           point3,
                           base_point):
    area1 = calculate_triangle_area(base_point, point1, point2)
    area2 = calculate_triangle_area(base_point, point2, point3)
    return area1 + area2


def find_convex_4_polygon_vertices(points):
    center = compute_center_of_gravity(points)
    vertex1, vertex1_index = find_furthest_point_from(center, points)
    vertex2, vertex2_index = find_furthest_point_from(vertex1, points)
    vertex3, vertex3_index = find_point_by_maximality_criteria(points,
                                                               lambda p: distance_between_point_and_line(vertex1,
                                                                                                         vertex2,
                                                                                                         p))
    vertices = [vertex1, vertex2, vertex3]
    vertices_indices = [vertex1_index, vertex2_index, vertex3_index]
    vertices, vertices_indices = sort_points_clockwise_with_indices(center, vertices, vertices_indices)
    vertex4, vertex4_index = find_point_by_maximality_criteria(points,
                                                               lambda p: calculate_polygon_area(vertices[0],
                                                                                                vertices[1],
                                                                                                vertices[2],
                                                                                                p))
    return vertices + [vertex4], vertices_indices + [vertex4_index]


def get_contour_segments(contour,
                         vertices_indices):
    contour_list = contour.tolist()
    segments = []
    for i in range(len(vertices_indices)):
        current_i = vertices_indices[i]
        next_i = vertices_indices[(i + 1) % len(vertices_indices)]
        if current_i < next_i:
            segment = np.array(contour_list[next_i:] + contour_list[:current_i], np.float32)
        else:
            segment = np.array(contour_list[next_i:current_i], np.float32)
        segments.append(segment)
    return segments


def estimate_line_parameters(n, points, axis=0):
    coefficients = np.array([i + 1 for i in range(n)])

    sum1 = np.sum(points[:, axis] * coefficients)
    sum2 = np.sum(points[:, axis])

    mat1 = np.array([[n * (n + 1) * (2 * n + 1) / 6, n * (n + 1) / 2],
                     [n * (n + 1) / 2, n]], np.float32)

    mat2 = np.array([[sum1],
                     [sum2]], np.float32)

    return np.dot(np.linalg.inv(mat1), mat2).reshape(2)


def parametric_linear_regression(points):
    n = len(points)

    if n <= 1:
        return None

    parameters1 = estimate_line_parameters(n, points)
    parameters2 = estimate_line_parameters(n, points, 1)

    return np.concatenate((parameters1.reshape(1, 2), parameters2.reshape(1, 2)), axis=0)


def enhance_vertices(contour,
                     vertices_indices):
    segments = get_contour_segments(contour, vertices_indices)

    lines = []
    for segment in segments:
        line = parametric_linear_regression(segment)
        if line is None:
            return None
        lines.append(line)

    intersections = []
    for i in range(len(segments)):
        intersection = find_lines_intersection_point(lines[i - 1], lines[i])
        if intersection is None:
            return None
        intersections.append(intersection)

    return np.array(intersections, np.int32)


def estimate_width_to_height_ratio(four_polygon):
    distance1 = euclidean_distance(four_polygon[0], four_polygon[1])
    distance2 = euclidean_distance(four_polygon[1], four_polygon[2])
    distance3 = euclidean_distance(four_polygon[2], four_polygon[3])
    distance4 = euclidean_distance(four_polygon[3], four_polygon[0])
    estimated1 = max(distance1, distance3)
    estimated2 = max(distance2, distance4)
    return max(estimated1, estimated2) / min(estimated1, estimated2)


def get_polygon_pixels_count(vertices, image_shape):
    polygon_area_image = np.zeros(image_shape, np.uint8)
    cv2.fillPoly(polygon_area_image, [vertices], 255)
    return np.count_nonzero(polygon_area_image == 255)


def find_closest_points(target_points, source_points):
    result = []
    for point in source_points:
        min_distance = sys.float_info.max
        best_target_point = target_points[0]
        best_target_point_i = 0
        for i in range(len(target_points)):
            target_point = target_points[i]
            distance_between_points = euclidean_distance(point, target_point)
            if distance_between_points < min_distance:
                min_distance = distance_between_points
                best_target_point = target_point
                best_target_point_i = i
        result.append((best_target_point, best_target_point_i))
    return result


def linearity_error(segments,
                    vertices):
    total_error = 0
    n = 0
    for i in range(vertices.shape[0]):
        current_vertex = vertices[i]
        next_vertex = vertices[(i + 1) % vertices.shape[0]]
        segment = segments[i]
        for j in range(1, len(segment) - 1):
            total_error += distance_between_point_and_line(current_vertex, next_vertex, segment[j])
            n += 1
    return total_error / n


def calculate_contour_segments_linearity_error(contour, vertices):
    closest_contour_points = find_closest_points(contour,
                                                 vertices)
    segments = get_contour_segments(contour,
                                    [p[1] for p in closest_contour_points])
    return linearity_error(segments, vertices)


def calculate_document_vertices_scores(document_vertices,
                                       document_mask,
                                       document_mask_contour_area,
                                       document_mask_contour_vertices,
                                       expected_document_width_height_ratio):
    if document_vertices is None:
        return 0.0, 0.0, 0.0

    if len(document_vertices) >= 4:
        ratio = estimate_width_to_height_ratio(document_vertices)
        polygon_area = get_polygon_pixels_count(document_vertices, document_mask.shape)
        error = calculate_contour_segments_linearity_error(document_mask_contour_vertices,
                                                           document_vertices)

        area_score = min(polygon_area, document_mask_contour_area) / max(polygon_area, document_mask_contour_area)

        width_height_score = 1.0 - min(1.0,
                                       abs(ratio - expected_document_width_height_ratio) /
                                       expected_document_width_height_ratio)

        edges_linearity_score = 1.0 - min(1.0,
                                          error /
                                          (np.sqrt(document_mask.shape[0] ** 2 + document_mask.shape[1] ** 2) / 8))

        scores = (area_score, width_height_score, edges_linearity_score)
    else:
        scores = (0.0, 0.0, 0.0)

    return scores


def adjust_vertices_after_downscaling_and_padding(vertices,
                                                  downscaling_factor,
                                                  padding_width):
    return [((x - padding_width) / downscaling_factor, (y - padding_width) / downscaling_factor)
            for x, y in vertices]


def get_and_adjust_vertices_with_best_score(vertices1,
                                            vertices2,
                                            document_mask,
                                            document_mask_contour_area,
                                            document_mask_contour_vertices,
                                            document_downscaling_factor,
                                            document_padding_width,
                                            document_expected_width_height_ratio,
                                            width_height_ratio_correctness_threshold,
                                            linearity_score_correctness_threshold):
    scores1 = calculate_document_vertices_scores(vertices1,
                                                 document_mask,
                                                 document_mask_contour_area,
                                                 document_mask_contour_vertices,
                                                 document_expected_width_height_ratio)
    if len(vertices1) >= 4:
        final_vertices1 = adjust_vertices_after_downscaling_and_padding(vertices1,
                                                                        document_downscaling_factor,
                                                                        document_padding_width)
    else:
        final_vertices1 = None

    scores2 = calculate_document_vertices_scores(vertices2,
                                                 document_mask,
                                                 document_mask_contour_area,
                                                 document_mask_contour_vertices,
                                                 document_expected_width_height_ratio)
    if len(vertices2) >= 4:
        final_vertices2 = adjust_vertices_after_downscaling_and_padding(vertices2,
                                                                        document_downscaling_factor,
                                                                        document_padding_width)
    else:
        final_vertices2 = None

    if final_vertices1 is None and final_vertices2 is None:
        return None

    if (scores1[2] >= linearity_score_correctness_threshold or
        scores2[2] >= linearity_score_correctness_threshold) and \
            abs(scores1[2] - scores2[2]) >= EPSILON:
        if scores1[2] > scores2[2]:
            return final_vertices1, float(np.prod(scores1))
        return final_vertices2, float(np.prod(scores2))

    if (scores1[1] >= width_height_ratio_correctness_threshold or
        scores2[1] >= width_height_ratio_correctness_threshold) and \
            abs(scores1[1] - scores2[1]) >= EPSILON:
        if scores1[1] > scores2[1]:
            return final_vertices1, float(np.prod(scores1))
        return final_vertices2, float(np.prod(scores2))

    if scores1[0] > scores2[0]:
        return final_vertices1, float(np.prod(scores1))

    return final_vertices2, float(np.prod(scores2))


def triangle_area(point1,
                  point2,
                  point3):
    x1, y1 = point1
    x2, y2 = point2
    x3, y3 = point3
    return math.fabs(0.5 * (x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)))


def calculate_4_polygon_area(point1,
                             point2,
                             point3,
                             point4):
    return triangle_area(point1, point2, point3) + \
        triangle_area(point1, point4, point3)


def estimate_rectangle_width_height_from_vertices(vertices,
                                                  id_card_width_height_ratio):
    area = calculate_4_polygon_area(*vertices)
    height = math.sqrt(area / id_card_width_height_ratio)
    width = id_card_width_height_ratio * height
    return int(math.floor(width)), int(math.floor(height))


def warp_perspective(image,
                     original_points,
                     target_points,
                     target_width,
                     target_height):
    original_points = np.float32(original_points)
    target_points = np.float32(target_points)
    perspective_transform_matrix = cv2.getPerspectiveTransform(original_points, target_points)
    return cv2.warpPerspective(image,
                               perspective_transform_matrix,
                               (target_width, target_height),
                               flags=cv2.INTER_CUBIC)


def correct_rectangle_perspective(image,
                                  warped_vertices,
                                  rectangle_width_height_ratio):
    target_width, target_height = estimate_rectangle_width_height_from_vertices(warped_vertices,
                                                                                rectangle_width_height_ratio)
    target_rectangle_vertices = [(0, 0),
                                 (target_width - 1, 0),
                                 (target_width - 1, target_height - 1),
                                 (0, target_height - 1)]
    return warp_perspective(image,
                            warped_vertices,
                            target_rectangle_vertices,
                            target_width,
                            target_height)


def resize_id_card_to_final_dimensions(id_card_image, id_card_width_height_ratio):
    id_card_final_height = int(math.floor(ID_CARD_FINAL_WIDTH / id_card_width_height_ratio))
    return cv2.resize(id_card_image, (ID_CARD_FINAL_WIDTH, id_card_final_height), cv2.INTER_CUBIC)


def edge_based_id_card_extraction(image,
                                  id_card_width_height_ratio=1.575):
    # parameters
    target_max_side_length = 1500
    edge_detection_kernel_size = 5
    edge_detection_smoothing_kernel_size = 7
    edge_closing_kernel_size = 43
    edge_closing_iterations = 1
    contour_area_cutoff_area_ratio = 0.05
    extraction_resize_factor = 0.5
    padding_width = 50
    min_low_threshold = 4
    min_high_threshold = 50
    candidate_best_area_cutoff_area_ratio = 0.025
    min_candidate_area_pixels = 20
    min_candidate_area_intersection_ratio = 0.75
    max_noise_size = 200
    final_mask_closing_kernel_size = 11
    final_mask_closing_iterations = 3
    final_mask_dilation_kernel_size = 5
    contour_approximation_arc_length_ratio = 0.001
    linearity_score_correctness_threshold = 0.985
    width_height_ratio_correctness_threshold = 0.99
    region_colors_distance_threshold = 1.75

    max_side_length = np.max(image.shape)
    resize_factor = target_max_side_length / max_side_length
    resized_image = resize_image_by_factor(image, resize_factor, cv2.INTER_CUBIC)
    detected_text = get_text_mask(resized_image)
    foreground_removed = cv2.morphologyEx(resized_image,
                                          cv2.MORPH_CLOSE,
                                          cv2.getStructuringElement(cv2.MORPH_RECT, (21, 21)))
    image_luv = cv2.cvtColor(foreground_removed, cv2.COLOR_BGR2LUV)
    gradients = edge_detection(foreground_removed,
                               edge_detection_kernel_size,
                               edge_detection_smoothing_kernel_size)

    high_threshold = otsu_threshold_value(gradients[(gradients > 0) & (detected_text == 0)])
    high_threshold = max(high_threshold, min_high_threshold)
    resized_detected_text = resize_image_by_factor(detected_text,
                                                   extraction_resize_factor,
                                                   cv2.INTER_NEAREST)
    low_threshold = low_hysteresis_threshold_binary_search(resize_image_by_factor(gradients,
                                                                                  extraction_resize_factor,
                                                                                  cv2.INTER_AREA),
                                                           resized_detected_text,
                                                           int(math.ceil(high_threshold / 2)),
                                                           min_low_threshold,
                                                           contour_area_cutoff_area_ratio)

    gradients[detected_text == 255] = 0
    edges = edge_hysteresis(gradients,
                            high_threshold,
                            low_threshold)

    closed_edges = close_edges(edges,
                               edge_closing_kernel_size,
                               edge_closing_iterations)

    edges = resize_image_by_factor(edges, extraction_resize_factor, cv2.INTER_NEAREST)
    edges = pad_image(edges, padding_width)
    closed_edges = resize_image_by_factor(closed_edges, extraction_resize_factor, cv2.INTER_NEAREST)
    closed_edges = pad_image(closed_edges, padding_width)
    image_luv = resize_image_by_factor(image_luv, extraction_resize_factor, cv2.INTER_AREA)
    image_luv = pad_image(image_luv, padding_width)
    detected_text = resized_detected_text
    detected_text = pad_image(detected_text, padding_width)

    vertices_score_pairs = []
    text_pixels_count = np.count_nonzero(detected_text)
    contours, hierarchy = cv2.findContours(closed_edges, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

    for contour in contours:
        contour_area = cv2.moments(contour)['m00']
        if contour_area < contour_area_cutoff_area_ratio * np.prod(edges.shape):
            continue

        contour_area_mask = np.zeros(closed_edges.shape, np.uint8)
        cv2.fillPoly(contour_area_mask, pts=[contour], color=255)
        candidate_areas_mask = (contour_area_mask == 255) & ~(edges == 255)

        (labels_count,
         labeled_connected_components,
         components_stats,
         centroids) = cv2.connectedComponentsWithStats(candidate_areas_mask.astype(np.uint8),
                                                       8,
                                                       cv2.CV_32S)

        best_region_mask, n1, mu1, sigma1 = find_best_region_mask_with_stats(candidate_areas_mask,
                                                                             candidate_best_area_cutoff_area_ratio,
                                                                             labels_count,
                                                                             labeled_connected_components,
                                                                             components_stats,
                                                                             detected_text,
                                                                             image_luv,
                                                                             text_pixels_count)

        chosen_labels = find_similar_colored_regions_labels(candidate_areas_mask,
                                                            image_luv,
                                                            labels_count,
                                                            labeled_connected_components,
                                                            components_stats,
                                                            min_candidate_area_pixels,
                                                            mu1,
                                                            sigma1,
                                                            n1,
                                                            region_colors_distance_threshold)

        result = filter_regions_and_get_mask_with_convex_hull(chosen_labels,
                                                              detected_text)
        if result is None:
            continue
        colors_mask, filtered_labels, final_convex_hull_vertices = result

        add_intersecting_regions_of_similar_color(colors_mask,
                                                  filtered_labels,
                                                  final_convex_hull_vertices,
                                                  chosen_labels,
                                                  min_candidate_area_intersection_ratio)

        final_mask = clean_binary_image(colors_mask, max_noise_size, 3, 1)
        final_mask = cv2.morphologyEx(final_mask,
                                      cv2.MORPH_CLOSE,
                                      np.ones((final_mask_closing_kernel_size,
                                               final_mask_closing_kernel_size), np.uint8),
                                      iterations=final_mask_closing_iterations)
        final_mask = cv2.dilate(final_mask,
                                np.ones((final_mask_dilation_kernel_size,
                                         final_mask_dilation_kernel_size), np.uint8))

        contour_vertices = find_largest_contour(final_mask)
        if contour_vertices is None or len(contour_vertices) < 3:
            continue

        vertices1 = approximate_contour_polygon(find_largest_contour(final_mask),
                                                contour_approximation_arc_length_ratio)
        vertices1 = auto_correct_vertices_order(vertices1)

        center_point = compute_center_of_gravity(contour_vertices)
        rectangle_vertices, rectangle_vertices_i = find_convex_4_polygon_vertices(contour_vertices)
        rectangle_vertices, rectangle_vertices_i = sort_points_clockwise_with_indices(center_point,
                                                                                      rectangle_vertices,
                                                                                      rectangle_vertices_i)
        vertices2 = enhance_vertices(contour_vertices, rectangle_vertices_i)
        if vertices2 is not None:
            vertices2 = auto_correct_vertices_order(vertices2)
        else:
            vertices2 = np.array([])

        downscaling_factor = extraction_resize_factor * resize_factor
        _, (width, height), _ = cv2.minAreaRect(contour_vertices)
        contour_area = int(width * height)

        result = get_and_adjust_vertices_with_best_score(vertices1,
                                                         vertices2,
                                                         final_mask,
                                                         contour_area,
                                                         contour_vertices,
                                                         downscaling_factor,
                                                         padding_width,
                                                         id_card_width_height_ratio,
                                                         width_height_ratio_correctness_threshold,
                                                         linearity_score_correctness_threshold)
        if result is None:
            continue
        best_vertices, best_scores = result

        vertices_score_pairs.append((best_vertices, np.prod(best_scores)))

    vertices_score_pairs.sort(key=lambda x: x[1], reverse=True)
    if len(vertices_score_pairs) == 0:
        return None

    best_vertices_score_pair = vertices_score_pairs[0]
    corrected_perspective = correct_rectangle_perspective(image, best_vertices_score_pair[0],
                                                          id_card_width_height_ratio)

    return resize_id_card_to_final_dimensions(corrected_perspective, id_card_width_height_ratio)


def main():
    id_card = cv2.imread('/home/u764/Downloads/1679391077949.jpg', cv2.IMREAD_COLOR)

    result = edge_based_id_card_extraction(id_card)

    start_time = time.time()
    for _ in range(100):
        result = edge_based_id_card_extraction(id_card)

    end_time = time.time()
    print((end_time - start_time) / 100)

    plt.imshow(result)
    plt.show()


if __name__ == '__main__':
    main()
