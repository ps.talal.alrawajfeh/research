import math
import os
import random
import sys

import cv2
import matplotlib.pyplot as plt
import numpy as np

EPSILON = 1e-6
DEBUG = False

DEFAULT_ID_CARD_EXTRACTION_CONFIG = {
    'pre_processing': {
        'target_max_side_length': 1500
    },
    'text_areas_mask_detection': {
        'text_mask_detection': {
            'edge_detection_kernel_size': 5,
            'smoothing_kernel_size': 5,
            'blackhat_morphology_kernel_size': 21,
            'smoothing_sigma_x': 0
        },
        'min_area': 20,
        'max_width_ratio': 0.25,
        'max_height_ratio': 0.25,
        'max_area_to_image_area_ratio': 0.005,
        'closing_morphology_kernel_size': 5,
        'closing_morphology_iterations': 3,
        'closed_min_area_to_image_area_ratio': 0.025,
        'opening_morphology_kernel_size': 5,
        'opening_morphology_iterations': 1,
        'opened_min_width_ratio': 0.05,
        'opened_min_height_ratio': 0.065,
        'final_closing_morphology_kernel_size': 7,
        'final_closing_morphology_iterations': 1
    },
    'foreground_removal': {
        'closing_morphology_kernel_width': 21,
        'closing_morphology_kernel_height': 21
    },
    'edge_detection': {
        'kernel_size': 5,
        'smoothing_kernel_size': 7,
        'smoothing_sigma_x': 1.2,
    },
    'edge_hysteresis': {
        'min_high_threshold': 50,
        'min_low_threshold': 4,
        'edge_closing_kernel_size': 3,
        'edge_closing_iterations': 1,
        'contour_area_mask_dilation_kernel_size': 3
    },
    'similar_colored_areas_search': {
        'similar_area_colors_distance_threshold': 1.75,
        'min_candidate_area_pixels': 20,
    },
    'final_mask': {
        'cleaning_max_noise_size': 200,
        'cleaning_closing_kernel_size': 3,
        'closing_kernel_size': 11,
        'closing_iterations': 3,
        'dilation_kernel_size': 5,
    },
    'edge_closing_kernel_size': 43,
    'edge_closing_iterations': 1,
    'extraction_resize_factor': 0.5,
    'extraction_padding_width': 50,
    'contour_area_cutoff_area_ratio': 0.05,
    'candidate_best_area_cutoff_area_ratio': 0.025,
    'min_candidate_area_intersection_ratio': 0.75,
    'contour_approximation_arc_length_ratio': 0.001,
    'width_height_ratio_correctness_threshold': 0.99,
    'linearity_score_correctness_threshold': 0.985,
    'id_card_width_height_ratio': 1.575,
    'id_card_final_width': 750
}


def resize_image_by_factor(image,
                           factor,
                           interpolation=cv2.INTER_CUBIC,
                           epsilon=EPSILON):
    if abs(factor - 1.0) < epsilon:
        return image
    return cv2.resize(image,
                      (0, 0),
                      fx=factor,
                      fy=factor,
                      interpolation=interpolation)


def dilate(image,
           kernel_size):
    return cv2.dilate(image, cv2.getStructuringElement(cv2.MORPH_RECT,
                                                       (kernel_size,
                                                        kernel_size)))


def otsu_threshold(gray_image):
    return cv2.threshold(gray_image,
                         0,
                         255,
                         cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]


def otsu_threshold_value(gray_image):
    return int(cv2.threshold(gray_image,
                             0,
                             255,
                             cv2.THRESH_BINARY + cv2.THRESH_OTSU)[0])


def resize_image_by_max_side_length(image, target_max_side_length):
    resize_factor = target_max_side_length / np.max(image.shape)
    return resize_image_by_factor(image, resize_factor), resize_factor


def detect_edges_from_gray_image(gray_image,
                                 kernel_size,
                                 smoothing_kernel_size=0,
                                 smoothing_sigma_x=0):
    if smoothing_kernel_size > 0:
        blurred = cv2.GaussianBlur(gray_image, (smoothing_kernel_size, smoothing_kernel_size), smoothing_sigma_x)
    else:
        blurred = gray_image
    sobel_x = cv2.Sobel(blurred, cv2.CV_32F, 1, 0, ksize=kernel_size)
    sobel_y = cv2.Sobel(blurred, cv2.CV_32F, 0, 1, ksize=kernel_size)
    gradients = np.hypot(sobel_x, sobel_y)
    min_gradients = np.min(gradients)
    max_gradients = np.max(gradients)
    gradients = (gradients - min_gradients) / (max_gradients - min_gradients) * 255
    return gradients.astype(np.uint8)


def rectangular_morphology(image,
                           morphological_operation,
                           kernel_width,
                           kernel_height,
                           iterations=1):
    return cv2.morphologyEx(image,
                            morphological_operation,
                            cv2.getStructuringElement(cv2.MORPH_RECT,
                                                      (kernel_width, kernel_height)),
                            iterations=iterations)


def detect_text_mask(id_card_image,
                     edge_detection_kernel_size,
                     smoothing_kernel_size,
                     smoothing_sigma_x,
                     blackhat_morphology_kernel_size):
    gray_image = cv2.cvtColor(id_card_image, cv2.COLOR_BGR2GRAY)
    gray_image = cv2.GaussianBlur(gray_image, (smoothing_kernel_size, smoothing_kernel_size), smoothing_sigma_x)
    gray_image = pad_image(gray_image, 50)
    gray_image = rectangular_morphology(gray_image,
                                        cv2.MORPH_BLACKHAT,
                                        blackhat_morphology_kernel_size,
                                        blackhat_morphology_kernel_size)

    if DEBUG:
        plt.imshow(gray_image, plt.cm.gray)
        plt.title('blackhat_morphology')
        plt.show()
    edges = detect_edges_from_gray_image(gray_image,
                                         edge_detection_kernel_size)

    return otsu_threshold(edges)[50:-50, 50:-50]


def filter_connected_components(binary_image,
                                connected_components_stats_filter,
                                stats_with_background_label=False):
    labels_count, labeled_image, stats, centroids = cv2.connectedComponentsWithStats(binary_image, 8, cv2.CV_32S)
    chosen_labels = connected_components_stats_filter(stats)
    chosen_labels = np.where(chosen_labels)[0]
    if not stats_with_background_label:
        chosen_labels += 1
    result_image = np.zeros(binary_image.shape, np.uint8)
    for i in range(chosen_labels.shape[0]):
        label = chosen_labels[i]
        x, y, w, h = stats[label, :4]
        mask = labeled_image[y:y + h, x:x + w] == label
        result_image[y:y + h, x: x + w][mask] = 255
    return result_image


def detect_text_areas_mask(id_card_image,
                           text_areas_mask_detection_config):
    text_mask_detection_config = text_areas_mask_detection_config['text_mask_detection']
    text_mask = detect_text_mask(id_card_image,
                                 text_mask_detection_config['edge_detection_kernel_size'],
                                 text_mask_detection_config['smoothing_kernel_size'],
                                 text_mask_detection_config['smoothing_sigma_x'],
                                 text_mask_detection_config['blackhat_morphology_kernel_size'])

    if DEBUG:
        plt.imshow(text_mask, plt.cm.gray)
        plt.title('text mask')
        plt.show()

    image_size = id_card_image.shape[:2]
    image_area = np.prod(image_size)

    components_filter = lambda stats: (stats[1:, -1] >= text_areas_mask_detection_config['min_area']) & \
                                      (stats[1:, 2] <= image_size[1] *
                                       text_areas_mask_detection_config['max_width_ratio']) & \
                                      (stats[1:, 3] <= image_size[0] *
                                       text_areas_mask_detection_config['max_height_ratio']) & \
                                      (stats[1:, -1] <= image_area *
                                       text_areas_mask_detection_config['max_area_to_image_area_ratio'])

    text_areas_mask = filter_connected_components(text_mask,
                                                  components_filter)

    inverted = 255 - text_areas_mask
    inverted = pad_image(inverted, 50)

    inverted = rectangular_morphology(inverted,
                                      cv2.MORPH_ERODE,
                                      7,
                                      7,
                                      1)
    labels_count, labeled_image, stats, centroids = cv2.connectedComponentsWithStats(inverted, 8, cv2.CV_32S)
    label = np.argmax(stats[1:, -1]) + 1
    largest_connected_component = np.array(labeled_image == label, np.uint8) * 255
    inverted = rectangular_morphology(largest_connected_component,
                                      cv2.MORPH_DILATE,
                                      7,
                                      7,
                                      1)

    inverted = rectangular_morphology(inverted,
                                      cv2.MORPH_CLOSE,
                                      21,
                                      21,
                                      3)
    inverted = inverted[50:-50, 50:-50]

    if DEBUG:
        plt.imshow(inverted, plt.cm.gray)
        plt.title('inverted')
        plt.show()

    text_areas_mask = text_areas_mask & inverted
    if DEBUG:
        plt.imshow(text_areas_mask, plt.cm.gray)
        plt.title('text_areas_mask')
        plt.show()

    text_areas_mask = rectangular_morphology(text_areas_mask,
                                             cv2.MORPH_CLOSE,
                                             text_areas_mask_detection_config['closing_morphology_kernel_size'],
                                             text_areas_mask_detection_config['closing_morphology_kernel_size'],
                                             text_areas_mask_detection_config['closing_morphology_iterations'])

    components_filter = lambda stats: stats[1:, -1] <= image_area * \
                                      text_areas_mask_detection_config['closed_min_area_to_image_area_ratio']

    text_areas_mask = filter_connected_components(text_areas_mask,
                                                  components_filter)

    text_areas_mask = rectangular_morphology(text_areas_mask,
                                             cv2.MORPH_OPEN,
                                             text_areas_mask_detection_config['opening_morphology_kernel_size'],
                                             text_areas_mask_detection_config['opening_morphology_kernel_size'],
                                             text_areas_mask_detection_config['opening_morphology_iterations'])

    components_filter = lambda s: (s[1:, 2] >= image_size[1] *
                                   text_areas_mask_detection_config['opened_min_width_ratio']) | \
                                  (s[1:, 3] >= image_size[0] *
                                   text_areas_mask_detection_config['opened_min_height_ratio'])

    text_areas_mask = filter_connected_components(text_areas_mask,
                                                  components_filter)

    return rectangular_morphology(text_areas_mask,
                                  cv2.MORPH_CLOSE,
                                  text_areas_mask_detection_config['final_closing_morphology_kernel_size'],
                                  text_areas_mask_detection_config['final_closing_morphology_kernel_size'],
                                  text_areas_mask_detection_config['final_closing_morphology_iterations'])


def remove_foreground(image, foreground_removal_config):
    return rectangular_morphology(image,
                                  cv2.MORPH_CLOSE,
                                  foreground_removal_config['closing_morphology_kernel_width'],
                                  foreground_removal_config['closing_morphology_kernel_height'])


def max_reduce(image):
    result = image[:, :, 0]
    for i in range(1, image.shape[-1]):
        result = np.maximum(result, image[:, :, i])
    return result


def gradients_from_sobel_filters(sobel_x, sobel_y):
    sobel_x = max_reduce(np.abs(sobel_x))
    sobel_y = max_reduce(np.abs(sobel_y))
    gradients = np.hypot(sobel_x, sobel_y)
    min_gradients = np.min(gradients)
    max_gradients = np.max(gradients)
    gradients = (gradients - min_gradients) / (max_gradients - min_gradients) * 255
    return gradients.astype(np.uint8)


def detect_edges_from_colored_image(image,
                                    kernel_size,
                                    smoothing_kernel_size=0,
                                    smoothing_sigma_x=0):
    if smoothing_kernel_size > 0:
        blurred = cv2.GaussianBlur(image, (smoothing_kernel_size, smoothing_kernel_size), smoothing_sigma_x)
    else:
        blurred = image
    sobel_x = cv2.Sobel(blurred, cv2.CV_32F, 1, 0, ksize=kernel_size)
    sobel_y = cv2.Sobel(blurred, cv2.CV_32F, 0, 1, ksize=kernel_size)
    return gradients_from_sobel_filters(sobel_x, sobel_y)


def edge_hysteresis(edges, high_threshold, low_threshold):
    strong_edges = np.array(edges)
    strong_edges[strong_edges < high_threshold] = 0
    strong_edges[strong_edges >= high_threshold] = 255

    weak_edges = np.array(edges)
    weak_edges[strong_edges == 255] = 0
    weak_edges[weak_edges <= low_threshold] = 0
    weak_edges[weak_edges > low_threshold] = 255

    mask = weak_edges | strong_edges

    while True:
        new_strong_edges = dilate(strong_edges, 3)
        new_strong_edges = (new_strong_edges & mask)
        if np.all(new_strong_edges == strong_edges):
            break
        strong_edges = new_strong_edges

    return strong_edges


def close_edges(edges, kernel_size, iterations=1):
    result = cv2.morphologyEx(edges,
                              cv2.MORPH_CLOSE,
                              np.ones((1, kernel_size), np.uint8),
                              iterations=iterations)
    result = cv2.morphologyEx(result,
                              cv2.MORPH_CLOSE,
                              np.ones((kernel_size, 1), np.uint8),
                              iterations=iterations)
    return result


def calculate_hysteresis_score(gradients,
                               threshold,
                               foreground_mask,
                               foreground_pixels_count,
                               high_threshold,
                               edge_closing_kernel_size,
                               edge_closing_iterations,
                               contour_area_cutoff_area_ratio,
                               contour_area_mask_dilation_kernel_size):
    edges = edge_hysteresis(gradients,
                            high_threshold,
                            threshold)
    closed_edges = edges
    if edge_closing_kernel_size > 0:
        closed_edges = close_edges(edges,
                                   edge_closing_kernel_size,
                                   edge_closing_iterations)

    contours, hierarchy = cv2.findContours(closed_edges,
                                           cv2.RETR_EXTERNAL,
                                           cv2.CHAIN_APPROX_NONE)
    max_score = 0.0
    for contour in contours:
        contour_area = cv2.moments(contour)['m00']
        if contour_area < contour_area_cutoff_area_ratio * np.prod(edges.shape):
            continue

        contour_area_mask = np.zeros(closed_edges.shape[0:2], np.uint8)
        cv2.fillPoly(contour_area_mask, pts=[contour], color=255)
        if contour_area_mask_dilation_kernel_size > 0:
            contour_area_mask = dilate(contour_area_mask,
                                       contour_area_mask_dilation_kernel_size)
        masked_foreground_pixels_count = np.count_nonzero(foreground_mask[contour_area_mask == 255])

        if masked_foreground_pixels_count == foreground_pixels_count:
            return 1.0

        foreground_ratio = masked_foreground_pixels_count / foreground_pixels_count
        if foreground_ratio > max_score:
            max_score = foreground_ratio

    return max_score


def low_hysteresis_threshold_search(gradients,
                                    foreground_mask,
                                    high_threshold,
                                    min_low_threshold,
                                    edge_closing_kernel_size,
                                    edge_closing_iterations,
                                    contour_area_cutoff_area_ratio,
                                    contour_area_mask_dilation_kernel_size,
                                    epsilon=EPSILON):
    max_low_threshold = high_threshold
    foreground_pixels_count = np.count_nonzero(foreground_mask == 255)

    score1 = calculate_hysteresis_score(gradients,
                                        min_low_threshold,
                                        foreground_mask,
                                        foreground_pixels_count,
                                        high_threshold,
                                        edge_closing_kernel_size,
                                        edge_closing_iterations,
                                        contour_area_cutoff_area_ratio,
                                        contour_area_mask_dilation_kernel_size)
    score2 = calculate_hysteresis_score(gradients,
                                        max_low_threshold,
                                        foreground_mask,
                                        foreground_pixels_count,
                                        high_threshold,
                                        edge_closing_kernel_size,
                                        edge_closing_iterations,
                                        contour_area_cutoff_area_ratio,
                                        contour_area_mask_dilation_kernel_size)
    if abs(score1 - score2) < epsilon:
        return max_low_threshold

    thresh1 = min_low_threshold
    thresh2 = max_low_threshold

    while (thresh2 - thresh1) > 1:
        thresh = (thresh1 + thresh2) // 2
        score = calculate_hysteresis_score(gradients,
                                           thresh,
                                           foreground_mask,
                                           foreground_pixels_count,
                                           high_threshold,
                                           edge_closing_kernel_size,
                                           edge_closing_iterations,
                                           contour_area_cutoff_area_ratio,
                                           contour_area_mask_dilation_kernel_size)

        if abs(score - score1) < epsilon:
            thresh1 = thresh
            score1 = score
        else:
            thresh2 = thresh
            score2 = score

    if abs(score1 - score2) < epsilon:
        return thresh2

    return thresh1


def pad_image(image, padding_width):
    if len(image.shape) < 3:
        return np.pad(image,
                      ((padding_width, padding_width), (padding_width, padding_width)),
                      'constant',
                      constant_values=((0, 0), (0, 0)))

    channels = []
    for i in range(image.shape[-1]):
        image_i = image[:, :, i]
        image_i = np.pad(image_i,
                         ((padding_width, padding_width), (padding_width, padding_width)),
                         'constant',
                         constant_values=((0, 0), (0, 0)))
        channels.append(image_i)

    return np.dstack(tup=tuple(channels))


def find_best_area_mask_with_statistics(candidate_areas_mask,
                                        candidate_best_area_cutoff_area_ratio,
                                        labeled_connected_components,
                                        components_stats,
                                        foreground_mask,
                                        image_luv,
                                        foreground_pixels_count):
    mask_area = candidate_areas_mask.shape[0] * candidate_areas_mask.shape[1]
    bins = np.arange(0, components_stats.shape[0] + 1)

    candidate_areas_label_counts = np.histogram(labeled_connected_components[candidate_areas_mask == 255], bins=bins)[0]
    candidate_areas_labels_mask = np.array(candidate_areas_label_counts > 0, np.float32)

    components_areas_mask = np.array(components_stats[:, -1] >= mask_area * candidate_best_area_cutoff_area_ratio,
                                     np.float32)

    foreground_label_counts = np.histogram(labeled_connected_components[foreground_mask == 255], bins=bins)[0]
    foreground_labels_ratios = foreground_label_counts / foreground_pixels_count

    components_area_ratios = components_stats[:, -1] / foreground_pixels_count

    scores = (foreground_labels_ratios + components_area_ratios * 0.001) * \
             candidate_areas_labels_mask * \
             components_areas_mask
    best_label = np.argmax(scores[1:]) + 1

    best_area_mask = (labeled_connected_components == best_label)

    best_area_mask_raveled = best_area_mask.ravel()
    image_l = image_luv[:, :, 0].ravel()[best_area_mask_raveled]
    image_u = image_luv[:, :, 1].ravel()[best_area_mask_raveled]
    image_v = image_luv[:, :, 2].ravel()[best_area_mask_raveled]

    mu = np.array([np.mean(image_l),
                   np.mean(image_u),
                   np.mean(image_v)])

    sigma = np.array([np.std(image_l),
                      np.std(image_u),
                      np.std(image_v)])

    n = np.count_nonzero(best_area_mask)

    return best_area_mask, n, mu, sigma


def find_similar_colored_areas_labels(candidate_areas_mask,
                                      image_luv,
                                      labels_count,
                                      labeled_connected_components,
                                      components_stats,
                                      min_candidate_area_pixels,
                                      best_candidate_area_mu,
                                      best_candidate_area_sigma,
                                      best_candidate_area_n,
                                      similar_area_colors_distance_threshold,
                                      epsilon=EPSILON):
    labels = []

    for label in range(0, labels_count):
        if components_stats[label, -1] < min_candidate_area_pixels:
            continue

        area_mask = labeled_connected_components == label
        n = np.count_nonzero(candidate_areas_mask.ravel()[area_mask.ravel()])
        if n == 0:
            continue

        area_mask_raveled = area_mask.ravel()
        image_l = image_luv[:, :, 0].ravel()[area_mask_raveled]
        image_u = image_luv[:, :, 1].ravel()[area_mask_raveled]
        image_v = image_luv[:, :, 2].ravel()[area_mask_raveled]

        if image_l.size == 0 or image_u.size == 0 or image_v.size == 0:
            continue

        mu = np.array([np.mean(image_l),
                       np.mean(image_u),
                       np.mean(image_v)])

        sigma = np.array([np.std(image_l),
                          np.std(image_u),
                          np.std(image_v)])

        sigma_mask = (best_candidate_area_sigma < epsilon) & (sigma < epsilon)
        if np.any(sigma_mask):
            mu_mask = (np.abs(best_candidate_area_mu - mu) > epsilon) & sigma_mask
            if np.any(mu_mask):
                fisher_distance = 1000.0
            else:
                fisher_distance = 0.0
        else:
            nominator = np.sqrt(best_candidate_area_n + n) * np.abs(best_candidate_area_mu - mu)
            denominator = np.sqrt(best_candidate_area_n * best_candidate_area_sigma ** 2 + n * sigma ** 2)
            fisher_distance = nominator / denominator
            fisher_distance = np.max(fisher_distance)

        if fisher_distance < similar_area_colors_distance_threshold:
            labels.append((label, area_mask))

    return labels


def filter_areas_and_get_areas_mask_and_convex_hull_of_areas(components_labels,
                                                             foreground_mask):
    areas_mask = np.zeros(foreground_mask.shape, np.uint8)
    all_vertices = []
    filtered_labels = []

    for i in range(len(components_labels)):
        label, area_mask = components_labels[i]

        if np.count_nonzero(foreground_mask[area_mask]) == 0:
            continue

        try:
            convex_hull = cv2.convexHull(np.transpose(np.where(np.transpose(area_mask)))).reshape(-1, 2)
        except:
            continue

        areas_mask[area_mask] = 255
        convex_hull_vertices = convex_hull.tolist()

        all_vertices.extend(convex_hull_vertices)
        filtered_labels.append(label)

    if len(filtered_labels) == 0:
        return None

    final_convex_hull = cv2.convexHull(np.array(all_vertices)).reshape(-1, 2)
    convex_hull_vertices = final_convex_hull.tolist()

    return areas_mask, filtered_labels, convex_hull_vertices


def add_intersecting_areas_of_similar_color(filtered_areas_mask,
                                            filtered_labels,
                                            filtered_areas_convex_hull_vertices,
                                            similar_colored_areas_labels,
                                            min_candidate_area_intersection_ratio):
    final_convex_region_mask = np.zeros(filtered_areas_mask.shape, np.uint8)
    cv2.fillPoly(final_convex_region_mask, [np.array(filtered_areas_convex_hull_vertices, np.int32)], 255)
    final_convex_region_mask = final_convex_region_mask == 255

    for label, area_mask in similar_colored_areas_labels:
        if label in filtered_labels:
            continue

        intersecting_pixels_count = np.count_nonzero(np.logical_and(final_convex_region_mask, area_mask))
        intersection_ratio = intersecting_pixels_count / np.count_nonzero(area_mask)
        if intersection_ratio < min_candidate_area_intersection_ratio:
            continue

        filtered_areas_mask[area_mask] = 255


def clean_binary_image(binary_image,
                       max_noise_size,
                       closing_kernel_size):
    new_image = filter_connected_components(binary_image,
                                            lambda stats: stats[1:, -1] > max_noise_size)
    return rectangular_morphology(new_image,
                                  cv2.MORPH_CLOSE,
                                  closing_kernel_size,
                                  closing_kernel_size,
                                  1)


def find_largest_contour(binary_image):
    contours, hierarchy = cv2.findContours(binary_image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    max_area = -1
    largest_contour = None
    for contour in contours:
        contour_area = cv2.moments(contour)['m00']
        if contour_area > max_area:
            max_area = contour_area
            largest_contour = contour
    if largest_contour is None:
        return None
    return largest_contour.reshape(-1, 2)


class Polygon:
    def __init__(self, vertices):
        vertices_array = np.array(vertices, np.float32)
        self.vertices = vertices
        self.area = cv2.moments(vertices_array)['m00']

        rect = cv2.minAreaRect(vertices_array)
        box = cv2.boxPoints(rect)
        box = np.int0(box)
        self.minimum_rotated_rectangle_area = cv2.moments(box)['m00']


def points_to_parametric_line(point1, point2):
    (x1, y1) = point1
    (x2, y2) = point2
    return np.array([[x2 - x1, x1],
                     [y2 - y1, y1]], np.float32)


def is_parametric_line_vertical(parametric_line,
                                epsilon=EPSILON):
    a_x, _ = parametric_line[0]
    a_y, _ = parametric_line[1]

    if abs(a_x) <= epsilon < abs(a_y):
        return True
    else:
        return False


def is_parametric_line_horizontal(parametric_line,
                                  epsilon=EPSILON):
    a_x, _ = parametric_line[0]
    a_y, _ = parametric_line[1]

    if abs(a_y) <= epsilon < abs(a_x):
        return True
    else:
        return False


def find_lines_intersection_point(line1,
                                  line2):
    if line1 is None or line2 is None:
        return None

    a1_x, b1_x = line1[0]
    a1_y, b1_y = line1[1]

    a2_x, b2_x = line2[0]
    a2_y, b2_y = line2[1]

    mat1 = np.array([[a1_x, -a2_x],
                     [a1_y, -a2_y]], np.float32)
    if np.linalg.det(mat1) == 0:
        return None

    mat2 = np.array([[b2_x - b1_x],
                     [b2_y - b1_y]], np.float32)

    t = np.dot(np.linalg.inv(mat1), mat2).reshape(2)

    point1 = line1[:, 0] * t[0] + line1[:, 1]
    point2 = line2[:, 0] * t[1] + line2[:, 1]

    return (point1 + point2) / 2


def consolidate_two_vertices_with_minimum_cost(vertices):
    min_positive_area_diff = sys.maxsize
    best_polygon_vertices = vertices
    was_consolidated = False
    vertices_polygon = Polygon(vertices)
    vertices_polygon_area = vertices_polygon.area
    vertices_min_polygon_area = vertices_polygon.minimum_rotated_rectangle_area

    for i in range(len(vertices)):
        parametric_line1 = points_to_parametric_line(vertices[i - 1],
                                                     vertices[i])
        parametric_line2 = points_to_parametric_line(vertices[(i + 1) % len(vertices)],
                                                     vertices[(i + 2) % len(vertices)])

        if (is_parametric_line_vertical(parametric_line1) and
            is_parametric_line_vertical(parametric_line2)) or \
                (is_parametric_line_horizontal(parametric_line1) and
                 is_parametric_line_horizontal(parametric_line2)):
            intersection_point = (np.array(vertices[i - 1]) + np.array(vertices[(i + 2) % len(vertices)])) / 2
        else:
            intersection_point = find_lines_intersection_point(parametric_line1,
                                                               parametric_line2)

        if intersection_point is None:
            continue

        new_polygon_vertices = [p for p in vertices]
        next_i = (i + 1) % len(vertices)
        if next_i > i:
            new_polygon_vertices.pop(next_i)
            new_polygon_vertices.pop(i)
        else:
            new_polygon_vertices.pop(i)
            new_polygon_vertices.pop(next_i)
        new_polygon_vertices.insert(i, intersection_point)

        new_polygon = Polygon(new_polygon_vertices)
        area_diff1 = new_polygon.area - vertices_polygon_area
        area_diff2 = new_polygon.minimum_rotated_rectangle_area - vertices_min_polygon_area
        area_diff = area_diff1 + area_diff2
        if area_diff < 0:
            continue

        if area_diff < min_positive_area_diff:
            was_consolidated = True
            min_positive_area_diff = area_diff
            best_polygon_vertices = new_polygon_vertices

    if was_consolidated is False:
        for i in range(len(vertices)):
            new_polygon_vertices = [p for p in vertices]
            new_polygon_vertices.pop(i)

            new_polygon = Polygon(new_polygon_vertices)
            area_diff = new_polygon.area - vertices_polygon_area
            if area_diff < 0:
                continue

            if area_diff < min_positive_area_diff:
                was_consolidated = True
                min_positive_area_diff = area_diff
                best_polygon_vertices = new_polygon_vertices

    return best_polygon_vertices, was_consolidated


def adjust_angle_by_quadrant(angle,
                             point):
    x, y = point
    if x < 0 and y > 0:
        return 180 - angle
    if x < 0 and y < 0:
        return 180 + angle
    if x > 0 and y < 0:
        return 360 - angle
    return angle


def compute_angle(origin,
                  point):
    x1 = point[0] - origin[0]
    y1 = origin[1] - point[1]
    if math.fabs(x1) > 1e-6:
        theta = math.fabs(math.atan(y1 / x1)) / math.pi * 180
    else:
        theta = 90
    return adjust_angle_by_quadrant(theta, (x1, y1))


def euclidean_distance(x, y):
    return np.sqrt(np.sum(np.square(np.array(x, np.float32) - np.array(y, np.float32))))


def sort_points_clockwise_polygonal(origin,
                                    points,
                                    epsilon=EPSILON):
    point_angle_pairs = [[points[i], compute_angle(origin, points[i])] for i in range(len(points))]
    point_angle_pairs.sort(key=lambda x: x[1], reverse=True)

    i = 0
    n = len(point_angle_pairs)
    while i < n:
        if abs(point_angle_pairs[i][1] - point_angle_pairs[(i + 1) % n][1]) < epsilon:
            j = 1
            while j < n - 1:
                if abs(point_angle_pairs[(i + j) % n][1] - point_angle_pairs[(i + j + 1) % n][1]) >= epsilon:
                    j += 1
                    break
                j += 1
            section = [point_angle_pairs[(i + k) % n] for k in range(0, j)]
            section.sort(key=lambda x: euclidean_distance(point_angle_pairs[i - 1][0], x[0]))

            if i + j > n:
                point_angle_pairs = section[n - i:] + point_angle_pairs[(i + j) % n:i] + section[:n - i]
            else:
                point_angle_pairs = point_angle_pairs[:i] + section + point_angle_pairs[i + j:]
            i += j - 1
            if i > n:
                break
        i += 1
    return [point for point, _ in point_angle_pairs]


def compute_center_of_gravity(points):
    return np.array([np.mean(points[:, :1]), np.mean(points[:, 1:])])


def try_consolidate_polygon_points(polygon_vertices, target_points):
    polygon_vertices = np.array(polygon_vertices)
    polygon_vertices = sort_points_clockwise_polygonal(compute_center_of_gravity(polygon_vertices), polygon_vertices)
    best_polygon = polygon_vertices
    while len(best_polygon) > target_points:
        best_polygon, was_consolidated = consolidate_two_vertices_with_minimum_cost(best_polygon)
        if not was_consolidated:
            break

    return best_polygon


def approximate_contour_polygon(contour, arc_length_percentage):
    if contour is None or len(contour) < 3:
        return np.array([])
    epsilon = arc_length_percentage * cv2.arcLength(contour, True)
    contour = cv2.approxPolyDP(contour, epsilon, True).reshape(-1, 2)
    convex_hull = cv2.convexHull(contour).reshape(-1, 2)
    contour = np.array(convex_hull, np.int32)
    contour = try_consolidate_polygon_points(contour, 4)
    return np.array(contour, np.int32)


def auto_correct_vertices_order(vertices):
    point1, point2, point3, point4 = vertices
    side1_length = euclidean_distance(point1, point2)
    corresponding_side1_length = euclidean_distance(point4, point3)
    side2_length = euclidean_distance(point2, point3)
    corresponding_side2_length = euclidean_distance(point1, point4)
    if max(side1_length, corresponding_side1_length) < min(side2_length, corresponding_side2_length):
        point1, point2, point3, point4 = point2, point3, point4, point1
    point1, point2, point3, point4 = point3, point4, point1, point2
    return np.array([point1,
                     point2,
                     point3,
                     point4])


def find_furthest_point_from(point,
                             points):
    max_distance = -1.0
    furthest_point = None
    furthest_point_index = -1
    for i in range(len(points)):
        p = points[i]
        point_distance = euclidean_distance(point, p)
        if point_distance > max_distance:
            max_distance = point_distance
            furthest_point = p
            furthest_point_index = i
    return furthest_point, furthest_point_index


def find_point_by_maximality_criteria(points,
                                      maximality_criteria):
    max_value = -1.0
    point_of_max_value = None
    point_index = -1
    for i in range(len(points)):
        p = points[i]
        value = maximality_criteria(p)
        if value > max_value:
            max_value = value
            point_of_max_value = p
            point_index = i
    return point_of_max_value, point_index


def distance_between_point_and_line(line_initial_point,
                                    line_terminal_point,
                                    point,
                                    epsilon=EPSILON):
    x1, y1 = line_initial_point
    x2, y2 = line_terminal_point
    x0, y0 = point
    denominator = euclidean_distance(line_initial_point, line_terminal_point)
    if denominator < epsilon:
        return euclidean_distance(line_initial_point, point)
    return abs((x2 - x1) * (y1 - y0) - (x1 - x0) * (y2 - y1)) / denominator


def sort_points_clockwise_with_indices(origin,
                                       points,
                                       points_indices):
    point_angle_pairs = [[points[i], points_indices[i], compute_angle(origin, points[i])] for i in range(len(points))]
    point_angle_pairs.sort(key=lambda x: x[2], reverse=True)
    return [point for point, _, _ in point_angle_pairs], [point_i for _, point_i, _ in point_angle_pairs]


def calculate_triangle_area(point1,
                            point2,
                            point3):
    return np.fabs(0.5 * (point1[0] * (point2[1] - point3[1])
                          + point2[0] * (point3[1] - point1[1])
                          + point3[0] * (point1[1] - point2[1])))


def calculate_polygon_area(point1,
                           point2,
                           point3,
                           base_point):
    area1 = calculate_triangle_area(base_point, point1, point2)
    area2 = calculate_triangle_area(base_point, point2, point3)
    return area1 + area2


def find_convex_4_polygon_vertices(points):
    center = compute_center_of_gravity(points)
    vertex1, vertex1_index = find_furthest_point_from(center, points)
    vertex2, vertex2_index = find_furthest_point_from(vertex1, points)
    vertex3, vertex3_index = find_point_by_maximality_criteria(points,
                                                               lambda p: distance_between_point_and_line(vertex1,
                                                                                                         vertex2,
                                                                                                         p))
    vertices = [vertex1, vertex2, vertex3]
    vertices_indices = [vertex1_index, vertex2_index, vertex3_index]
    vertices, vertices_indices = sort_points_clockwise_with_indices(center, vertices, vertices_indices)
    vertex4, vertex4_index = find_point_by_maximality_criteria(points,
                                                               lambda p: calculate_polygon_area(vertices[0],
                                                                                                vertices[1],
                                                                                                vertices[2],
                                                                                                p))
    return vertices + [vertex4], vertices_indices + [vertex4_index]


def get_contour_segments(contour,
                         vertices_indices):
    contour_list = contour.tolist()
    segments = []
    for i in range(len(vertices_indices)):
        current_i = vertices_indices[i]
        next_i = vertices_indices[(i + 1) % len(vertices_indices)]
        if current_i < next_i:
            segment = np.array(contour_list[next_i:] + contour_list[:current_i], np.float32)
        else:
            segment = np.array(contour_list[next_i:current_i], np.float32)
        segments.append(segment)
    return segments


def estimate_line_parameters(n, points, axis=0):
    coefficients = np.array([i + 1 for i in range(n)])

    sum1 = np.sum(points[:, axis] * coefficients)
    sum2 = np.sum(points[:, axis])

    mat1 = np.array([[n * (n + 1) * (2 * n + 1) / 6, n * (n + 1) / 2],
                     [n * (n + 1) / 2, n]], np.float32)

    mat2 = np.array([[sum1],
                     [sum2]], np.float32)

    return np.dot(np.linalg.inv(mat1), mat2).reshape(2)


def parametric_linear_regression(points):
    n = len(points)

    if n <= 1:
        return None

    parameters1 = estimate_line_parameters(n, points)
    parameters2 = estimate_line_parameters(n, points, 1)

    return np.concatenate((parameters1.reshape(1, 2), parameters2.reshape(1, 2)), axis=0)


def enhance_vertices(contour,
                     vertices_indices):
    segments = get_contour_segments(contour, vertices_indices)

    lines = []
    for segment in segments:
        line = parametric_linear_regression(segment)
        if line is None:
            return None
        lines.append(line)

    intersections = []
    for i in range(len(segments)):
        intersection = find_lines_intersection_point(lines[i - 1], lines[i])
        if intersection is None:
            return None
        intersections.append(intersection)

    return np.array(intersections, np.int32)


def estimate_width_to_height_ratio(four_polygon):
    distance1 = euclidean_distance(four_polygon[0], four_polygon[1])
    distance2 = euclidean_distance(four_polygon[1], four_polygon[2])
    distance3 = euclidean_distance(four_polygon[2], four_polygon[3])
    distance4 = euclidean_distance(four_polygon[3], four_polygon[0])
    estimated1 = max(distance1, distance3)
    estimated2 = max(distance2, distance4)
    return max(estimated1, estimated2) / min(estimated1, estimated2)


def get_polygon_pixels_count(vertices, image_shape):
    polygon_area_image = np.zeros(image_shape, np.uint8)
    cv2.fillPoly(polygon_area_image, [vertices], 255)
    return np.count_nonzero(polygon_area_image == 255)


def find_closest_points(target_points, source_points):
    result = []
    for point in source_points:
        min_distance = sys.float_info.max
        best_target_point = target_points[0]
        best_target_point_i = 0
        for i in range(len(target_points)):
            target_point = target_points[i]
            distance_between_points = euclidean_distance(point, target_point)
            if distance_between_points < min_distance:
                min_distance = distance_between_points
                best_target_point = target_point
                best_target_point_i = i
        result.append((best_target_point, best_target_point_i))
    return result


def linearity_error(segments,
                    vertices):
    total_error = 0
    n = 0
    for i in range(vertices.shape[0]):
        current_vertex = vertices[i]
        next_vertex = vertices[(i + 1) % vertices.shape[0]]
        segment = segments[i]
        for j in range(1, len(segment) - 1):
            total_error += distance_between_point_and_line(current_vertex, next_vertex, segment[j])
            n += 1
    return total_error / n


def calculate_contour_segments_linearity_error(contour, vertices):
    closest_contour_points = find_closest_points(contour,
                                                 vertices)
    segments = get_contour_segments(contour,
                                    [p[1] for p in closest_contour_points])
    return linearity_error(segments, vertices)


def calculate_document_vertices_scores(document_vertices,
                                       document_mask,
                                       document_mask_contour_area,
                                       document_mask_contour_vertices,
                                       expected_document_width_height_ratio):
    if document_vertices is None:
        return 0.0, 0.0, 0.0

    if len(document_vertices) >= 4:
        ratio = estimate_width_to_height_ratio(document_vertices)
        polygon_area = get_polygon_pixels_count(document_vertices, document_mask.shape)
        error = calculate_contour_segments_linearity_error(document_mask_contour_vertices,
                                                           document_vertices)

        area_score = min(polygon_area, document_mask_contour_area) / max(polygon_area, document_mask_contour_area)

        width_height_score = 1.0 - min(1.0,
                                       abs(ratio - expected_document_width_height_ratio) /
                                       expected_document_width_height_ratio)

        edges_linearity_score = 1.0 - min(1.0,
                                          error /
                                          (np.sqrt(document_mask.shape[0] ** 2 + document_mask.shape[1] ** 2) / 8))

        scores = (area_score, width_height_score, edges_linearity_score)
    else:
        scores = (0.0, 0.0, 0.0)

    return scores


def adjust_vertices_after_downscaling_and_padding(vertices,
                                                  downscaling_factor,
                                                  padding_width):
    return [((x - padding_width) / downscaling_factor, (y - padding_width) / downscaling_factor)
            for x, y in vertices]


def get_and_adjust_vertices_with_best_score(vertices1,
                                            vertices2,
                                            document_mask,
                                            document_mask_contour_area,
                                            document_mask_contour_vertices,
                                            document_downscaling_factor,
                                            document_padding_width,
                                            document_expected_width_height_ratio,
                                            width_height_ratio_correctness_threshold,
                                            linearity_score_correctness_threshold,
                                            epsilon=EPSILON):
    scores1 = calculate_document_vertices_scores(vertices1,
                                                 document_mask,
                                                 document_mask_contour_area,
                                                 document_mask_contour_vertices,
                                                 document_expected_width_height_ratio)
    if len(vertices1) >= 4:
        final_vertices1 = adjust_vertices_after_downscaling_and_padding(vertices1,
                                                                        document_downscaling_factor,
                                                                        document_padding_width)
    else:
        final_vertices1 = None

    scores2 = calculate_document_vertices_scores(vertices2,
                                                 document_mask,
                                                 document_mask_contour_area,
                                                 document_mask_contour_vertices,
                                                 document_expected_width_height_ratio)
    if len(vertices2) >= 4:
        final_vertices2 = adjust_vertices_after_downscaling_and_padding(vertices2,
                                                                        document_downscaling_factor,
                                                                        document_padding_width)
    else:
        final_vertices2 = None

    if final_vertices1 is None and final_vertices2 is None:
        return None

    if (scores1[2] >= linearity_score_correctness_threshold or
        scores2[2] >= linearity_score_correctness_threshold) and \
            abs(scores1[2] - scores2[2]) >= epsilon:
        if scores1[2] > scores2[2]:
            return final_vertices1, float(np.prod(scores1))
        return final_vertices2, float(np.prod(scores2))

    if (scores1[1] >= width_height_ratio_correctness_threshold or
        scores2[1] >= width_height_ratio_correctness_threshold) and \
            abs(scores1[1] - scores2[1]) >= epsilon:
        if scores1[1] > scores2[1]:
            return final_vertices1, float(np.prod(scores1))
        return final_vertices2, float(np.prod(scores2))

    if scores1[0] > scores2[0]:
        return final_vertices1, float(np.prod(scores1))

    return final_vertices2, float(np.prod(scores2))


def triangle_area(point1,
                  point2,
                  point3):
    x1, y1 = point1
    x2, y2 = point2
    x3, y3 = point3
    return math.fabs(0.5 * (x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)))


def calculate_4_polygon_area(point1,
                             point2,
                             point3,
                             point4):
    return triangle_area(point1, point2, point3) + \
        triangle_area(point1, point4, point3)


def estimate_rectangle_width_height_from_vertices(vertices,
                                                  id_card_width_height_ratio):
    area = calculate_4_polygon_area(*vertices)
    height = math.sqrt(area / id_card_width_height_ratio)
    width = id_card_width_height_ratio * height
    return int(math.floor(width)), int(math.floor(height))


def warp_perspective(image,
                     original_points,
                     target_points,
                     target_width,
                     target_height):
    original_points = np.float32(original_points)
    target_points = np.float32(target_points)
    perspective_transform_matrix = cv2.getPerspectiveTransform(original_points, target_points)
    return cv2.warpPerspective(image,
                               perspective_transform_matrix,
                               (target_width, target_height),
                               flags=cv2.INTER_CUBIC)


def correct_rectangle_perspective(image,
                                  warped_vertices,
                                  rectangle_width_height_ratio):
    target_width, target_height = estimate_rectangle_width_height_from_vertices(warped_vertices,
                                                                                rectangle_width_height_ratio)
    target_rectangle_vertices = [(0, 0),
                                 (target_width - 1, 0),
                                 (target_width - 1, target_height - 1),
                                 (0, target_height - 1)]
    return warp_perspective(image,
                            warped_vertices,
                            target_rectangle_vertices,
                            target_width,
                            target_height)


def resize_id_card_to_final_dimensions(id_card_image,
                                       id_card_width_height_ratio,
                                       id_card_final_width):
    id_card_final_height = int(math.floor(id_card_final_width / id_card_width_height_ratio))
    return cv2.resize(id_card_image, (id_card_final_width, id_card_final_height), cv2.INTER_CUBIC)


def extract_id_card(id_card_image,
                    id_card_extraction_config=None):
    if id_card_extraction_config is None:
        id_card_extraction_config = DEFAULT_ID_CARD_EXTRACTION_CONFIG

    pre_processing_config = id_card_extraction_config['pre_processing']
    resized_id_card_image, resize_factor = resize_image_by_max_side_length(id_card_image,
                                                                           pre_processing_config[
                                                                               'target_max_side_length'])

    text_areas_mask = detect_text_areas_mask(resized_id_card_image,
                                             id_card_extraction_config['text_areas_mask_detection'])

    foreground_removed = resized_id_card_image  # remove_foreground(resized_id_card_image, id_card_extraction_config['foreground_removal'])
    foreground_removed_luv = cv2.cvtColor(foreground_removed, cv2.COLOR_BGR2LUV)

    edge_detection_config = id_card_extraction_config['edge_detection']
    gradients = detect_edges_from_colored_image(foreground_removed,
                                                edge_detection_config['kernel_size'],
                                                edge_detection_config['smoothing_kernel_size'],
                                                edge_detection_config['smoothing_sigma_x'])

    resized_text_areas_mask = resize_image_by_factor(text_areas_mask,
                                                     id_card_extraction_config['extraction_resize_factor'],
                                                     cv2.INTER_NEAREST)
    resized_gradients = resize_image_by_factor(gradients,
                                               id_card_extraction_config['extraction_resize_factor'],
                                               cv2.INTER_AREA)

    edge_hysteresis_config = id_card_extraction_config['edge_hysteresis']
    high_threshold = otsu_threshold_value(gradients[(gradients > 0) & (text_areas_mask == 0)])
    high_threshold = max(high_threshold,
                         edge_hysteresis_config['min_high_threshold'])
    contour_area_cutoff_area_ratio = id_card_extraction_config['contour_area_cutoff_area_ratio']
    low_threshold = low_hysteresis_threshold_search(resized_gradients,
                                                    resized_text_areas_mask,
                                                    int(math.ceil(high_threshold / 2)),
                                                    edge_hysteresis_config['min_low_threshold'],
                                                    edge_hysteresis_config['edge_closing_kernel_size'],
                                                    edge_hysteresis_config['edge_closing_iterations'],
                                                    contour_area_cutoff_area_ratio,
                                                    edge_hysteresis_config['contour_area_mask_dilation_kernel_size'])

    print(low_threshold)
    print(high_threshold)
    print('asdfasdfa')
    gradients[text_areas_mask == 255] = 0
    edges_mask = edge_hysteresis(gradients,
                                 high_threshold,
                                 low_threshold)
    # edges_mask = cv2.dilate(edges_mask, np.ones((3, 3), np.uint8))
    closed_edges_mask = close_edges(edges_mask,
                                    id_card_extraction_config['edge_closing_kernel_size'],
                                    id_card_extraction_config['edge_closing_iterations'])

    edges_mask = resize_image_by_factor(edges_mask,
                                        id_card_extraction_config['extraction_resize_factor'],
                                        cv2.INTER_NEAREST)
    edges_mask = pad_image(edges_mask,
                           id_card_extraction_config['extraction_padding_width'])
    closed_edges_mask = resize_image_by_factor(closed_edges_mask,
                                               id_card_extraction_config['extraction_resize_factor'],
                                               cv2.INTER_NEAREST)
    closed_edges_mask = pad_image(closed_edges_mask,
                                  id_card_extraction_config['extraction_padding_width'])
    foreground_removed_luv = resize_image_by_factor(foreground_removed_luv,
                                                    id_card_extraction_config['extraction_resize_factor'],
                                                    cv2.INTER_AREA)
    foreground_removed_luv = pad_image(foreground_removed_luv,
                                       id_card_extraction_config['extraction_padding_width'])
    text_areas_mask = resized_text_areas_mask
    text_areas_mask = pad_image(text_areas_mask,
                                id_card_extraction_config['extraction_padding_width'])

    if DEBUG:
        plt.imshow(id_card_image)
        plt.title('id_card_image')
        plt.show()

        plt.imshow(text_areas_mask, plt.cm.gray)
        plt.title('text_areas_mask')
        plt.show()

        plt.imshow(edges_mask, plt.cm.gray)
        plt.title('edges_mask')
        plt.show()

        plt.imshow(closed_edges_mask, plt.cm.gray)
        plt.title('closed_edges_mask')
        plt.show()

    text_areas_mask_pixels_count = np.count_nonzero(text_areas_mask)
    contours, hierarchy = cv2.findContours(closed_edges_mask,
                                           cv2.RETR_EXTERNAL,
                                           cv2.CHAIN_APPROX_NONE)

    similar_colored_areas_search_config = id_card_extraction_config['similar_colored_areas_search']
    final_mask_config = id_card_extraction_config['final_mask']
    vertices_score_pairs = []

    for contour in contours:
        contour_area = cv2.moments(contour)['m00']
        if contour_area < contour_area_cutoff_area_ratio * np.prod(edges_mask.shape):
            continue

        contour_area_mask = np.zeros(closed_edges_mask.shape, np.uint8)
        cv2.fillPoly(contour_area_mask, pts=[contour], color=255)
        candidate_areas_mask = (contour_area_mask == 255) & ~(edges_mask == 255)

        if DEBUG:
            plt.imshow(candidate_areas_mask, plt.cm.gray)
            plt.title('candidate areas')
            plt.show()

        candidate_areas_mask = candidate_areas_mask.astype(np.uint8) * 255
        (labels_count,
         labeled_connected_components,
         components_stats,
         centroids) = cv2.connectedComponentsWithStats(candidate_areas_mask,
                                                       8,
                                                       cv2.CV_32S)
        if DEBUG:
            plt.imshow(labeled_connected_components, plt.cm.gray)
            plt.title('labels')
            plt.show()
        best_area_mask, n1, mu1, sigma1 = find_best_area_mask_with_statistics(candidate_areas_mask,
                                                                              id_card_extraction_config[
                                                                                  'candidate_best_area_cutoff_area_ratio'],
                                                                              labeled_connected_components,
                                                                              components_stats,
                                                                              text_areas_mask,
                                                                              foreground_removed_luv,
                                                                              text_areas_mask_pixels_count)

        if DEBUG:
            plt.imshow(best_area_mask, plt.cm.gray)
            plt.title('best candidate area')
            plt.show()

        similar_colored_areas_labels = find_similar_colored_areas_labels(candidate_areas_mask,
                                                                         foreground_removed_luv,
                                                                         labels_count,
                                                                         labeled_connected_components,
                                                                         components_stats,
                                                                         similar_colored_areas_search_config[
                                                                             'min_candidate_area_pixels'],
                                                                         mu1,
                                                                         sigma1,
                                                                         n1,
                                                                         similar_colored_areas_search_config[
                                                                             'similar_area_colors_distance_threshold'])

        result = filter_areas_and_get_areas_mask_and_convex_hull_of_areas(similar_colored_areas_labels,
                                                                          text_areas_mask)
        if result is None:
            continue
        filtered_areas_mask, filtered_labels, filtered_areas_convex_hull_vertices = result

        add_intersecting_areas_of_similar_color(filtered_areas_mask,
                                                filtered_labels,
                                                filtered_areas_convex_hull_vertices,
                                                similar_colored_areas_labels,
                                                id_card_extraction_config['min_candidate_area_intersection_ratio'])

        final_mask = clean_binary_image(filtered_areas_mask,
                                        final_mask_config['cleaning_max_noise_size'],
                                        final_mask_config['cleaning_closing_kernel_size'])
        final_mask = rectangular_morphology(final_mask,
                                            cv2.MORPH_CLOSE,
                                            final_mask_config['closing_kernel_size'],
                                            final_mask_config['closing_kernel_size'],
                                            final_mask_config['closing_iterations'])
        final_mask = dilate(final_mask,
                            final_mask_config['dilation_kernel_size'])

        contour_vertices = find_largest_contour(final_mask)
        if contour_vertices is None or len(contour_vertices) < 3:
            continue

        vertices1 = approximate_contour_polygon(find_largest_contour(final_mask),
                                                id_card_extraction_config['contour_approximation_arc_length_ratio'])
        vertices1 = auto_correct_vertices_order(vertices1)

        center_point = compute_center_of_gravity(contour_vertices)
        rectangle_vertices, rectangle_vertices_i = find_convex_4_polygon_vertices(contour_vertices)
        rectangle_vertices, rectangle_vertices_i = sort_points_clockwise_with_indices(center_point,
                                                                                      rectangle_vertices,
                                                                                      rectangle_vertices_i)

        vertices2 = enhance_vertices(contour_vertices, rectangle_vertices_i)
        if vertices2 is not None:
            vertices2 = auto_correct_vertices_order(vertices2)
        else:
            vertices2 = np.array([])

        downscaling_factor = id_card_extraction_config['extraction_resize_factor'] * resize_factor
        _, (width, height), _ = cv2.minAreaRect(contour_vertices)
        contour_area = int(width * height)

        # TODO: this part can be enhanced
        result = get_and_adjust_vertices_with_best_score(vertices1,
                                                         vertices2,
                                                         final_mask,
                                                         contour_area,
                                                         contour_vertices,
                                                         downscaling_factor,
                                                         id_card_extraction_config['extraction_padding_width'],
                                                         id_card_extraction_config['id_card_width_height_ratio'],
                                                         id_card_extraction_config[
                                                             'width_height_ratio_correctness_threshold'],
                                                         id_card_extraction_config[
                                                             'linearity_score_correctness_threshold'])
        if result is None:
            continue
        best_vertices, best_scores = result
        vertices_score_pairs.append((best_vertices, np.prod(best_scores)))

    vertices_score_pairs.sort(key=lambda x: x[1], reverse=True)
    if len(vertices_score_pairs) == 0:
        return None

    best_vertices_score_pair = vertices_score_pairs[0]
    corrected_perspective = correct_rectangle_perspective(id_card_image,
                                                          best_vertices_score_pair[0],
                                                          id_card_extraction_config['id_card_width_height_ratio'])

    return resize_id_card_to_final_dimensions(corrected_perspective,
                                              id_card_extraction_config['id_card_width_height_ratio'],
                                              id_card_extraction_config['id_card_final_width'])


def main():
    # id_card_image = cv2.imread('/home/u764/Downloads/1679391077949.jpg', cv2.IMREAD_COLOR)
    #
    # result = extract_id_card(id_card_image)
    #
    # plt.imshow(result, plt.cm.gray)
    # plt.show()

    base_dir = '/home/u764/Development/data/ekyc-data/PSO ID'
    output_dir = '/home/u764/Downloads/results_new/Oman'
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
    images = [(os.path.join(base_dir, file), file) for file in os.listdir(base_dir)]
    for image_path, file_name in images:
        # if file_name != '20200610_090145.jpg':
        #     continue
        image = cv2.imread(image_path)
        try:
            result = extract_id_card(image)

            if DEBUG:
                plt.imshow(result)
                plt.show()
        except:
            result = None
            print(f'error in: {file_name}')

        if result is not None:
            cv2.imwrite(os.path.join(output_dir, file_name), result)


if __name__ == '__main__':
    main()


def test_point_in_4_polygin():
    x1, y1 = 0.0, 0.0
    x2, y2 = 1.0, 0.0
    x3, y3 = 1.0, 1.0
    x4, y4 = 0.0, 1.0
    a, b = 0.5, 0.5
    c1, c2, c3, c4 = calculate_coefficients(a, b, x1, x2, x3, x4, y1, y2, y3, y4)
    print(c1, c2, c3, c4)
    print(c1 * x1 + c2 * x2 + c3 * x3 + c4 * x4)
    print(c1 * y1 + c2 * y2 + c3 * y3 + c4 * y4)


def calculate_coefficients(a, b, x1, x2, x3, x4, y1, y2, y3, y4):
    u1 = x1 - a
    u2 = x2 - a
    u3 = x3 - a
    u4 = x4 - a

    v1 = y1 - a
    v2 = y2 - a
    v3 = y3 - a
    v4 = y4 - a

    alpha = 0.5
    l4 = random.uniform(1, 2)
    l3 = random.uniform(1, 2)

    print(alpha, l3, l4)
    f = a * alpha ** 2
    g = b * alpha ** 2
    q3 = l3 ** 2
    q4 = l4 ** 2

    l2_squared = (g - f * v1 / u1 - q3 * (v3 - v1 * u3 / u1) - q4 * (v4 - v1 * u4 / u1)) / (v2 - v1 * u2 / u1)
    print(l2_squared)
    l2 = math.sqrt(l2_squared)
    q2 = l2 ** 2

    l1_squared = (f - q2 * u2 - q3 * u3 - q4 * u4) / u1
    print(l1_squared)
    l1 = math.sqrt(l1_squared)

    d = (l1 ** 2 + l2 ** 2 + l3 ** 2 + l4 ** 2 + alpha ** 2)
    c1 = l1 ** 2 / d
    c2 = l2 ** 2 / d
    c3 = l3 ** 2 / d
    c4 = l4 ** 2 / d

    return c1, c2, c3, c4
