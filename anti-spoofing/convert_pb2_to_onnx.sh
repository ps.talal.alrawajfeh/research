#!/usr/bin/bash

INPUT_FILE_NAME=final_model
OUTPUT_FILE_NAME=final_model

python3 -m pip install --user --upgrade pip tf2onnx
python3 convert_h5_to_pb2.py $INPUT_FILE_NAME.h5 ./$OUTPUT_FILE_NAME
python3 -m tf2onnx.convert --saved-model $OUTPUT_FILE_NAME --output $OUTPUT_FILE_NAME.onnx
