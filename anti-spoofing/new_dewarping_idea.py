import cv2
import matplotlib.pyplot as plt
import numpy as np


# Do a binary search on the gradients to find a suitable threshold
# We are trying to find the maximum threshold that creates a contour that is closest to a 4-polygon after we perform edge hysteresis
# We might do a closing operation in the beginning to remove text so we are left with only the background
# Should we perform a blurring/smoothing operation?

def threshold_image(image, threshold):
    image_copy = np.copy(image)
    image_copy[image_copy >= threshold] = 255
    image_copy[image_copy < threshold] = 0
    return image_copy


def threshold_image_weak(image, high_threshold, low_threshold):
    image_copy = np.copy(image)
    image_copy[image_copy >= high_threshold] = 0
    image_copy[image_copy >= low_threshold] = 255
    image_copy[image_copy < low_threshold] = 0
    return image_copy


def perform_edge_hysteresis(strong_edges, weak_edges):
    mask = weak_edges | strong_edges
    result = strong_edges
    while True:
        new_strong_edges = cv2.dilate(result, np.ones((3, 3), np.uint8))
        new_strong_edges = (new_strong_edges & mask)
        if np.all(new_strong_edges == result):
            break
        result = new_strong_edges
    return result


def find_max_closed_contour_area(edges):
    contours, hierarchy = cv2.findContours(cv2.dilate(edges, np.ones((3, 3), np.uint8)),
                                           cv2.RETR_CCOMP,
                                           cv2.CHAIN_APPROX_SIMPLE)
    hierarchy = hierarchy[0]
    max_area = -1
    for i in range(len(contours)):
        if hierarchy[i][2] >= 0:
            contour = contours[i]
            contour_area = cv2.moments(contour)['m00']

            contour = np.reshape(contour, (-1, 2))
            points_next = contour[2:]
            points_previous = contour[:-2]
            derivatives = (points_next - points_previous) / 2
            print(np.mean(derivatives))
            print(np.std(derivatives))
            print('------------')

            if contour_area > max_area:
                max_area = contour_area

    return max_area


def find_best_hysteresis(gradients, strong_edges, high_threshold):
    min_area = 0.2 * np.prod(gradients.shape)

    b = high_threshold - 1
    a = 1

    edges_b = perform_edge_hysteresis(strong_edges, threshold_image_weak(gradients,
                                                                         high_threshold,
                                                                         b))

    max_contour_area_b = find_max_closed_contour_area(edges_b)
    if max_contour_area_b >= min_area:
        return edges_b

    edges_a = perform_edge_hysteresis(strong_edges, threshold_image_weak(gradients,
                                                                         high_threshold,
                                                                         a))

    max_contour_area_a = find_max_closed_contour_area(edges_a)
    if max_contour_area_a < min_area:
        return None

    result = edges_a
    while (b - a) > 1:
        thresh = (a + b) // 2
        edges_thresh = perform_edge_hysteresis(strong_edges, threshold_image_weak(gradients,
                                                                                  high_threshold,
                                                                                  thresh))

        max_contour_area_thresh = find_max_closed_contour_area(edges_thresh)
        if max_contour_area_thresh >= min_area:
            a = thresh
            result = edges_thresh
        else:
            b = thresh
            result = edges_thresh

    return result


def find_best_edges(gradients):
    b = 254
    a = 2

    strong_edges = threshold_image(gradients, b)
    if not np.all(strong_edges == 0):
        edges_b = find_best_hysteresis(gradients, strong_edges, b)
    else:
        edges_b = None

    if edges_b is not None:
        return edges_b

    strong_edges = threshold_image(gradients, a)
    if not np.all(strong_edges == 0):
        edges_a = find_best_hysteresis(gradients, strong_edges, a)
    else:
        edges_a = None

    if edges_a is None:
        return None

    result = edges_a
    while (b - a) > 1:
        thresh = (a + b) // 2

        strong_edges = threshold_image(gradients, thresh)
        if not np.all(strong_edges == 0):
            edges_thresh = find_best_hysteresis(gradients, strong_edges, thresh)
        else:
            edges_thresh = None

        if edges_thresh is not None:
            a = thresh
            result = edges_thresh
        else:
            b = thresh
            result = edges_thresh

    return result


def main():
    image = cv2.imread('/home/u764/Downloads/test-images/1684847875107.jpg')

    image = cv2.morphologyEx(image,
                             cv2.MORPH_CLOSE,
                             cv2.getStructuringElement(cv2.MORPH_RECT, (21, 21)))

    max_side_length = np.max(image.shape)
    resize_factor = 750 / max_side_length
    image = cv2.resize(image, None, fx=resize_factor, fy=resize_factor, interpolation=cv2.INTER_CUBIC)

    # image = cv2.rotate(image, cv2.ROTATE_90_CLOCKWISE)
    plt.imshow(image)
    plt.show()

    image_luv = cv2.cvtColor(image, cv2.COLOR_BGR2LUV)
    # image_l = image_luv[:, :, 0]

    sobel_x = cv2.Sobel(image_luv, cv2.CV_64F, 1, 0, ksize=7)
    sobel_y = cv2.Sobel(image_luv, cv2.CV_64F, 0, 1, ksize=7)

    gradients = np.max(np.hypot(sobel_x, sobel_y), axis=-1)
    # gradients = np.power(gradients, 2 / 3)
    max_gradient = np.max(gradients)
    min_gradient = np.min(gradients)
    gradients = (gradients - min_gradient) / (max_gradient - min_gradient) * 255
    gradients = gradients.astype(np.uint8)

    result_edges = find_best_edges(gradients)
    if result_edges is None:
        print('no closed contour found')
        return

    plt.imshow(result_edges, plt.cm.gray)
    plt.show()

    # thresh = 20
    # gradients[gradients >= thresh] = 255
    # gradients[gradients < thresh] = 0
    # gradients = cv2.threshold(gradients, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]

    # plt.imshow(gradients, plt.cm.gray)
    # plt.show()
    # print(np.mean(gradients))
    # print(np.std(gradients))
    #
    # plt.hist(gradients.ravel(), bins=255)
    # plt.show()


if __name__ == '__main__':
    main()
