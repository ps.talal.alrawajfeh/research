import math

import cv2
import dlib
import matplotlib
import numpy as np
from matplotlib import pyplot as plt

matplotlib.use('TkAgg')
EPSILON = 1e-6


def to_int(number):
    return int(round(number))


def rotate(image, angle):
    (h, w) = image.shape[:2]
    center_x, center_y = w / 2, h / 2

    rotation_matrix = cv2.getRotationMatrix2D((center_x, center_y),
                                              angle,
                                              1.0)
    cos = np.abs(rotation_matrix[0, 0])
    sin = np.abs(rotation_matrix[0, 1])

    new_w = h * sin + w * cos
    new_h = h * cos + w * sin

    rotation_matrix[0, 2] += new_w / 2 - center_x
    rotation_matrix[1, 2] += new_h / 2 - center_y

    return cv2.warpAffine(image,
                          rotation_matrix,
                          (to_int(new_w), to_int(new_h)),
                          borderValue=(255, 255, 255),
                          flags=cv2.INTER_CUBIC)


def get_face_landmark_points(image,
                             face_detector,
                             face_alignment_detector,
                             face_landmark_detector):
    image_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    face_locations = face_detector(image_rgb, 1)
    faces = dlib.full_object_detections()
    faces.append(face_alignment_detector(image_rgb, face_locations[0]))
    alignment_angle = dlib.get_face_chip_details(faces[0]).angle * 180 / math.pi
    aligned_face = rotate(image_rgb, alignment_angle)
    face_locations = face_detector(aligned_face, 1)
    face_landmarks = face_landmark_detector(aligned_face, face_locations[0])
    points = []
    for i in range(0, 68):
        x = face_landmarks.part(i).x
        y = face_landmarks.part(i).y
        x = int(round(x))
        y = int(round(y))
        points.append((x, y))
    return points


def point_differences_to_magnitudes(pairwise_differences):
    return np.sqrt(np.sum(np.square(pairwise_differences), axis=-1))


def point_differences_to_angles(pairwise_differences, epsilon=EPSILON):
    mask = np.fabs(pairwise_differences[:, :, 0]) > epsilon
    angles = np.zeros((int(pairwise_differences.shape[0]),
                       int(pairwise_differences.shape[1])), np.float32)

    mask_90 = pairwise_differences[:, :, 1] > epsilon
    mask_270 = pairwise_differences[:, :, 1] < -epsilon
    mask_0 = (pairwise_differences[:, :, 1] <= epsilon) & (pairwise_differences[:, :, 1] >= -epsilon)
    angles[~mask & mask_90] = 90.0
    angles[~mask & mask_270] = 270.0
    angles[~mask & mask_0] = 0.0
    xs = pairwise_differences[:, :, 0]
    ys = pairwise_differences[:, :, 1]
    angles[mask] = np.fabs(np.arctan(ys[mask] / xs[mask])) / math.pi * 180
    angle_mask1 = (xs < 0) & (ys > 0)
    angle_mask2 = (xs < 0) & (ys < 0)
    angle_mask3 = (xs > 0) & (ys < 0)
    angles[mask & angle_mask1] = 180 - angles[mask & angle_mask1]
    angles[mask & angle_mask2] = 180 + angles[mask & angle_mask2]
    angles[mask & angle_mask3] = 360 - angles[mask & angle_mask3]
    return angles


def angular_difference(angles1, angles2):
    larger = np.maximum(angles1, angles2)
    smaller = np.minimum(angles1, angles2)
    return np.minimum(larger - smaller, smaller + 360 - larger)


def normalized_angular_difference(angles1, angles2):
    return angular_difference(angles1, angles2) / 180.0


def distance_difference(distances1, distances2):
    return np.square(distances1 - distances2)


def normalized_distance_difference(distances1, distances2):
    difference = distance_difference(distances1, distances2)
    return difference / np.max(difference)


def get_landmark_features(points):
    pairwise_differences = np.expand_dims(points, 0) - np.expand_dims(points, 1)
    distances = point_differences_to_magnitudes(pairwise_differences)
    angles = point_differences_to_angles(pairwise_differences)
    return angles, distances


def main():
    face_detector = dlib.get_frontal_face_detector()
    face_landmark_detector = dlib.shape_predictor('/home/u764/Downloads/shape_predictor_68_face_landmarks.dat')
    face_alignment_detector = dlib.shape_predictor('/home/u764/Downloads/shape_predictor_5_face_landmarks.dat')

    image1 = cv2.imread('/home/u764/Downloads/Me/IMG_20190614_191017_1.jpg')
    image2 = cv2.imread('/home/u764/Downloads/Me/IMG_20190627_190622.jpg')

    points1 = get_face_landmark_points(image1,
                                       face_detector,
                                       face_alignment_detector,
                                       face_landmark_detector)

    points2 = get_face_landmark_points(image2,
                                       face_detector,
                                       face_alignment_detector,
                                       face_landmark_detector)

    angles1, distances1 = get_landmark_features(points1)
    angles2, distances2 = get_landmark_features(points2)

    angular_differences = normalized_angular_difference(angles1, angles2)
    distance_differences = normalized_distance_difference(distances1, distances2)

    distance_differences_mean = np.mean(distance_differences)
    angular_differences_mean = np.mean(angular_differences)
    score = (distance_differences_mean + angular_differences_mean) / 2
    print(score)

    # plt.imshow(aligned)
    # plt.show()

    # for img in images:
    #     plt.imshow(img)
    #     plt.show()

    # video_capture = cv2.VideoCapture(0)
    #
    # while True:
    #     ret, image = video_capture.read()
    #     gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    #     rectangles = face_detector(gray, 1)
    #     for rectangle in rectangles:
    #         shape = face_landmark_detector(gray, rectangle)
    #         for i in range(0, 68):
    #             x = int(round(shape.part(i).x))
    #             y = int(round(shape.part(i).y))
    #             cv2.circle(image, (x, y), 2, (0, 0, 255), -1)
    #
    #     cv2.imshow('Face Landmarks', image)
    #     if cv2.waitKey(10) == 27:
    #         break
    # video_capture.release()


if __name__ == '__main__':
    main()
