import sys

import cv2
import dlib
import numpy as np
import onnxruntime

FACE_DETECTION_MAX_DIMENSION = 800
INPUT_SIZE = (224, 224)
FACE_DETECTOR = dlib.get_frontal_face_detector()


def locate_faces(face_image):
    face_locations, confidences, _ = FACE_DETECTOR.run(face_image, 1, 0.0)
    locations = []
    for i in range(len(face_locations)):
        face_location = face_locations[i]
        x = face_location.left()
        y = face_location.top()
        w = face_location.width()
        h = face_location.height()
        locations.append({
            'rectangle': [x, y, w, h],
            'score': confidences[i]
        })
    return locations


def get_face_rectangle(image):
    locations = locate_faces(image)
    if len(locations) == 0:
        return None
    max_score = 0.0
    best_rectangle = None
    for location in locations:
        if location['score'] > max_score:
            max_score = location['score']
            best_rectangle = location['rectangle']
    if best_rectangle is None:
        return None
    return best_rectangle


def get_model_input(image, face_detection_max_dimension=FACE_DETECTION_MAX_DIMENSION):
    image_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

    max_dimension = max(image.shape[0], image.shape[1])
    if max_dimension > face_detection_max_dimension:
        factor = face_detection_max_dimension / max_dimension
        downscaled_image = cv2.resize(image_rgb, None, fx=factor, fy=factor, interpolation=cv2.INTER_CUBIC)
    else:
        factor = 1.0
        downscaled_image = image_rgb.copy()

    x, y, w, h = get_face_rectangle(downscaled_image)

    x /= factor
    y /= factor
    w /= factor
    h /= factor

    x = int(round(x))
    y = int(round(y))
    w = int(round(w))
    h = int(round(h))

    face = image_rgb[y:y + h, x:x + w]
    return cv2.resize(face, INPUT_SIZE, interpolation=cv2.INTER_CUBIC)


def feed_forward_onnx_model(onnx_session, pre_processed_image):
    output = onnx_session.run(None,
                              {'input_2': np.expand_dims(pre_processed_image, axis=0)})[0]
    return output


def linear_interpolation(x1, x2, y1, y2, x):
    slope = (y2 - y1) / (x2 - x1)
    return slope * (x - x1) + y1


def predict(image, onnx_session):
    resized_cropped_face = get_model_input(image)
    prediction = feed_forward_onnx_model(onnx_session, resized_cropped_face.astype(np.float32))[0, 1]
    if prediction < 0.15:
        prediction = 0.0
    elif 0.15 <= prediction < 0.79:
        prediction = linear_interpolation(0.15, 0.79, 0.01, 0.5, prediction)
    elif 0.79 <= prediction <= 0.85:
        prediction = linear_interpolation(0.79, 0.85, 0.9, 0.99, prediction)
    else:
        prediction = 1.0
    return prediction


def print_prediction(prediction):
    print(f'prediction: {round(float(prediction) * 100, 2)}%')
    if prediction >= 0.5:
        print(f'result: live')
    else:
        print(f'result: spoof')


def main():
    sess = onnxruntime.InferenceSession('final_model.onnx')
    prediction = predict(cv2.imread(sys.argv[1],
                                    cv2.IMREAD_COLOR),
                         sess)
    print_prediction(prediction)


if __name__ == '__main__':
    main()
