import gzip
import math
import mimetypes
import os
import pickle
import random

import cv2
import dlib
import numpy as np
from tqdm import tqdm

OUTPUT_DATA_PATH = '/home/user/Downloads/data'
CELEB_A_SPOOF_DATA_PATH = '/home/user/Downloads/CelebA_Spoof'
LCC_FASD_DATA_PATH = '/home/user/Downloads/LCC_FASD'
ENABLE_COMPRESSION = True
PRE_PROCESS_IMAGE = True
INPUT_SIZE = (224, 224)
FACE_DETECTOR = dlib.get_frontal_face_detector()

AUGMENTATION_PER_IMAGE = 1
AUGMENTATION_PER_LIVE_IMAGE = 2
AUGMENTATION_PER_SPOOF_IMAGE = 1
REMOVE_PREVIOUS_AUGMENTATIONS = True
AUGMENT_AND_BALANCE_LIVE_AND_SPOOF = False
AUGMENT_TRAIN_DATA = True
AUGMENT_VALIDATION_DATA = True
AUGMENT_TEST_DATA = True

MIN_AREA_RATIO = 0.05
MAX_AREA_RATIO = 0.6
MIN_SIDE_RATIO = 0.1
MAX_SIDE_RATIO = 0.6
EPSILON = 1e-6
LOCAL_SPOOF_PER_LIVE_IMAGE = 1
GENERATE_LOCAL_SPOOFS_FOR_TRAIN_DATA = True
GENERATE_LOCAL_SPOOFS_FOR_VALIDATION_DATA = True
GENERATE_LOCAL_SPOOFS_FOR_TEST_DATA = True


def get_extensions_for_type(general_type):
    for ext in mimetypes.types_map:
        if mimetypes.types_map[ext].split('/')[0] == general_type:
            yield ext.lower()


def memoize(function):
    memo = {}

    def wrapper(*args):
        if args in memo:
            return memo[args]
        else:
            rv = function(*args)
            memo[args] = rv
            return rv

    return wrapper


@memoize
def get_image_extensions():
    return tuple(get_extensions_for_type('image'))


def file_extension(path):
    return os.path.splitext(os.path.basename(path))[1].lower()


def is_image(path):
    return file_extension(path) in get_image_extensions()


def ensure_directory_exists(directory_path):
    if not os.path.isdir(directory_path):
        os.makedirs(directory_path)


def translate_bounding_box_file(bounding_box_file_text,
                                real_w,
                                real_h):
    """
    This function was written according to the description in the README file in the CelebA-Spoof dataset
    """
    x1, y1, w, h, _ = bounding_box_file_text.strip().split()

    x1 = float(x1)
    y1 = float(y1)
    w = float(w)
    h = float(h)

    x1 = int(x1 * real_w / 224)
    y1 = int(y1 * real_h / 224)
    w = int(w * real_w / 224)
    h = int(h * real_h / 224)

    return x1, y1, w, h,


def get_output_directories():
    output_train_data_path = os.path.join(OUTPUT_DATA_PATH, 'train')
    output_validation_data_path = os.path.join(OUTPUT_DATA_PATH, 'validation')
    output_test_data_path = os.path.join(OUTPUT_DATA_PATH, 'test')

    output_train_live_data_path = os.path.join(output_train_data_path, 'live')
    output_train_spoof_data_path = os.path.join(output_train_data_path, 'spoof')
    output_validation_live_data_path = os.path.join(output_validation_data_path, 'live')
    output_validation_spoof_data_path = os.path.join(output_validation_data_path, 'spoof')
    output_test_live_data_path = os.path.join(output_test_data_path, 'live')
    output_test_spoof_data_path = os.path.join(output_test_data_path, 'spoof')

    ensure_directory_exists(OUTPUT_DATA_PATH)

    ensure_directory_exists(output_train_data_path)
    ensure_directory_exists(output_validation_data_path)
    ensure_directory_exists(output_test_data_path)

    ensure_directory_exists(output_train_live_data_path)
    ensure_directory_exists(output_train_spoof_data_path)
    ensure_directory_exists(output_validation_live_data_path)
    ensure_directory_exists(output_validation_spoof_data_path)
    ensure_directory_exists(output_test_live_data_path)
    ensure_directory_exists(output_test_spoof_data_path)

    return (output_train_live_data_path,
            output_train_spoof_data_path,
            output_validation_live_data_path,
            output_validation_spoof_data_path,
            output_test_live_data_path,
            output_test_spoof_data_path)


def save_image_compressed(path, image):
    _, buffer = cv2.imencode('.png', image)
    with gzip.open(path, 'wb') as f:
        f.write(buffer)


def locate_faces(face_image):
    face_locations, confidences, _ = FACE_DETECTOR.run(face_image, 1, 0.0)

    locations = []
    for i in range(len(face_locations)):
        face_location = face_locations[i]
        x = face_location.left()
        y = face_location.top()
        w = face_location.width()
        h = face_location.height()
        locations.append({
            'rectangle': [x, y, w, h],
            'score': confidences[i]
        })

    return locations


def crop_face(image):
    locations = locate_faces(image)
    if len(locations) == 0:
        return None
    max_score = 0.0
    best_rectangle = None
    for location in locations:
        if location['score'] > max_score:
            max_score = location['score']
            best_rectangle = location['rectangle']
    if best_rectangle is None:
        return None
    x, y, w, h = best_rectangle
    return image[y:y + h, x:x + w]


def get_image_paths(path):
    files = os.listdir(path)
    images = filter(is_image, files)
    return [os.path.join(path, file) for file in images]


def pre_process_image(image):
    image_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    return cv2.resize(image_rgb, INPUT_SIZE, interpolation=cv2.INTER_CUBIC)


def serialize_object_compressed(path, obj):
    with gzip.open(path, 'wb') as f:
        pickle.dump(obj, f)


def deserialize_object_compressed(file_path):
    with gzip.open(file_path, 'rb') as f:
        return pickle.load(f)


def serialize_object(path, obj):
    with open(path, 'wb') as f:
        pickle.dump(obj, f)


def deserialize_object(image_path):
    with open(image_path, 'rb') as f:
        return pickle.load(f)


def save_image(output_image_path, image):
    if ENABLE_COMPRESSION:
        serialize_object_compressed(output_image_path, image)
    else:
        serialize_object(output_image_path, image)


def read_image(image_path):
    if ENABLE_COMPRESSION:
        return deserialize_object_compressed(image_path)
    return deserialize_object(image_path)


def safe_crop_face(image):
    try:
        face = crop_face(image)
    except:
        face = image
    if face is None or face.shape[0] == 0 or face.shape[1] == 0:
        face = image
    return face


def safe_crop_face_from_bounding_box_file_text(bounding_box_file_text, image):
    x1, y1, w, h = translate_bounding_box_file(bounding_box_file_text,
                                               image.shape[1],
                                               image.shape[0])
    try:
        face = image[y1:y1 + h, x1:x1 + w]
    except:
        face = image
    if face is None or face.shape[0] == 0 or face.shape[1] == 0:
        face = image
    return face


def try_crop_face_from_bounding_box_file(bounding_box_file, image):
    try:
        with open(bounding_box_file, 'r') as f:
            bounding_box_file_text = f.read()
    except:
        bounding_box_file_text = None
    if bounding_box_file_text is None or len(bounding_box_file_text.strip().split()) < 5:
        face = image
    else:
        face = safe_crop_face_from_bounding_box_file_text(bounding_box_file_text, image)
    return face


def iso_noise(image, color_shift=0.05, intensity=0.5):
    one_over_255 = float(1.0 / 255.0)
    image = np.multiply(image, one_over_255, dtype=np.float32)
    hls = cv2.cvtColor(image, cv2.COLOR_RGB2HLS)
    _, stddev = cv2.meanStdDev(hls)

    luminance_noise = np.random.poisson(stddev[1] * intensity * 255, size=hls.shape[:2])
    color_noise = np.random.normal(0, color_shift * 360 * intensity, size=hls.shape[:2])

    hue = hls[..., 0]
    hue += color_noise
    hue[hue < 0] += 360
    hue[hue > 360] -= 360

    luminance = hls[..., 1]
    luminance += (luminance_noise / 255) * (1.0 - luminance)

    image = cv2.cvtColor(hls, cv2.COLOR_HLS2RGB) * 255
    return image.astype(np.uint8)


def horizontal_flip(image):
    return cv2.flip(image, 1)


def vertical_flip(image):
    return cv2.flip(image, -1)


def random_alpha_beta_from_limits(brightness_limit=0.2, contrast_limit=0.2):
    alpha = 1.0 + random.uniform(-contrast_limit, contrast_limit)
    beta = random.uniform(-brightness_limit, brightness_limit)
    return alpha, beta


def brightness_contrast_adjust(img, alpha=1.0, beta=0.0):
    max_value = 255
    lut = np.arange(0, max_value + 1).astype(np.float32)
    if alpha != 1:
        lut *= alpha
    if beta != 0:
        lut += beta * max_value
    lut = np.clip(lut, 0, max_value).astype(np.uint8)
    return cv2.LUT(img, lut)


def get_motion_blur_kernel(blur_limit):
    kernel_size = random.choice(np.arange(3, blur_limit + 1, 2))
    kernel = np.zeros((kernel_size, kernel_size), dtype=np.uint8)

    x1, x2 = random.randint(0, kernel_size - 1), random.randint(0, kernel_size - 1)
    if x1 == x2:
        y1, y2 = random.sample(range(kernel_size), 2)
    else:
        y1, y2 = random.randint(0, kernel_size - 1), random.randint(0, kernel_size - 1)

    def make_odd_val(v1, v2):
        len_v = abs(v1 - v2) + 1
        if len_v % 2 != 1:
            if v2 > v1:
                v2 -= 1
            else:
                v1 -= 1
        return v1, v2

    x1, x2 = make_odd_val(x1, x2)
    y1, y2 = make_odd_val(y1, y2)

    xc = (x1 + x2) / 2
    yc = (y1 + y2) / 2

    center = kernel_size / 2 - 0.5
    dx = xc - center
    dy = yc - center
    x1, x2 = [int(i - dx) for i in [x1, x2]]
    y1, y2 = [int(i - dy) for i in [y1, y2]]

    cv2.line(kernel, (x1, y1), (x2, y2), 1, thickness=1)

    return kernel.astype(np.float32) / np.sum(kernel)


def motion_blur(image, blur_limit):
    kernel = get_motion_blur_kernel(blur_limit)
    return cv2.filter2D(image, ddepth=-1, kernel=kernel)


def change_image_quality(image, quality):
    encoded = cv2.imencode('.jpg', image, [int(cv2.IMWRITE_JPEG_QUALITY), quality])[1]
    return cv2.imdecode(encoded, cv2.IMREAD_COLOR)


def change_image_dpi(image, dpi):
    factor = dpi / 100
    resized = cv2.resize(image, None, fx=factor, fy=factor, interpolation=cv2.INTER_CUBIC)
    return cv2.resize(resized, INPUT_SIZE, interpolation=cv2.INTER_CUBIC)


def to_int(x):
    return int(round(x))


def draw_rotated_rectangle(image, rectangle, angle):
    theta = math.radians(angle)
    rx, ry = rectangle[0]
    rw, rh = rectangle[1]

    sin = math.sin(theta)
    if abs(sin) < EPSILON:
        w = rw
        h = rh
        sin = 0.0
        cos = 1.0
    else:
        sin = abs(sin)
        cos = abs(math.cos(theta))
        w = (rw * cos - rh * sin) / (2 * cos ** 2 - 1)
        h = (rw - w * cos) / sin
    if sin * cos > 0:
        point1 = (rx, ry + w * sin)
        point2 = (rx + w * cos, ry)
        point3 = (rx + rw, ry + h * cos)
        point4 = (rx + h * sin, ry + rh)
    else:
        point1 = (rx + h * sin, ry)
        point2 = (rx + rw, ry + w * sin)
        point3 = (rx + w * cos, ry + rh)
        point4 = (rx, ry + h * cos)

    point1 = (to_int(point1[0]), to_int(point1[1]))
    point2 = (to_int(point2[0]), to_int(point2[1]))
    point3 = (to_int(point3[0]), to_int(point3[1]))
    point4 = (to_int(point4[0]), to_int(point4[1]))

    cv2.line(image, point1, point2, 255, 1)
    cv2.line(image, point2, point3, 255, 1)
    cv2.line(image, point3, point4, 255, 1)
    cv2.line(image, point4, point1, 255, 1)

    points = np.transpose(np.where(np.transpose(image == 255)))

    cv2.fillPoly(image, pts=[points], color=255)


def get_random_shape_mask(x, y, width, height):
    mask = np.zeros((224, 224), np.uint8)
    if random.choice([1, 2]) == 1:
        cv2.ellipse(mask, ((x + width) // 2, ((y + height) // 2)), (width // 2, height // 2),
                    angle=random.randint(-20, 20),
                    startAngle=random.randint(-20, 20),
                    endAngle=360,
                    color=255,
                    thickness=-1)
    else:
        draw_rotated_rectangle(mask, ((x, y), (width, height)), random.randint(-20, 20))
    return mask == 255


def get_random_rectangle():
    area = random.uniform(MIN_AREA_RATIO, MAX_AREA_RATIO) * 224 ** 2
    width = random.uniform(MIN_SIDE_RATIO, MAX_SIDE_RATIO) * 224
    height = min(area / width, 224)
    width = to_int(width)
    height = to_int(height)
    x = random.randint(0, 224 - width)
    y = random.randint(0, 224 - height)
    return x, y, width, height


def generate_local_spoof(live_image, spoof_image):
    if random.choice([1, 2]) == 1:
        x, y, width, height = get_random_rectangle()
        mask = get_random_shape_mask(x, y, width, height)
    else:
        x1, y1, width1, height1 = get_random_rectangle()
        x2, y2, width2, height2 = get_random_rectangle()
        mask1 = get_random_shape_mask(x1, y1, width1, height1)
        mask2 = get_random_shape_mask(x2, y2, width2, height2)
        mask = mask1 | mask2

    live_image_copy = np.array(live_image)
    for i in range(3):
        live_image_copy[:, :, i][mask] = spoof_image[:, :, i][mask]
    return live_image_copy


def generate_augmentation(image):
    augmented = np.array(image)

    if random.uniform(0.0, 1.0) <= 0.5:
        augmented = horizontal_flip(augmented)

    if random.uniform(0.0, 1.0) <= 0.5:
        augmented = vertical_flip(augmented)

    if random.uniform(0.0, 1.0) <= 0.3:
        alpha, beta = random_alpha_beta_from_limits(0.2, 0.2)
        augmented = brightness_contrast_adjust(augmented,
                                               alpha,
                                               beta)

    # if random.uniform(0.0, 1.0) <= 0.2:
    #     augmented = motion_blur(augmented, 5)
    #
    # if random.uniform(0.0, 1.0) <= 0.2:
    #     augmented = iso_noise(augmented,
    #                           random.uniform(0.15, 0.35),
    #                           random.uniform(0.2, 0.5))
    #
    # if random.uniform(0.0, 1.0) <= 0.2:
    #     augmented = change_image_dpi(augmented,
    #                                  random.choice(range(50, 91, 10)))
    #
    # if random.uniform(0.0, 1.0) <= 0.2:
    #     augmented = change_image_quality(augmented,
    #                                      random.randint(75, 99))

    return augmented


def prepare_celeba_spoof_data():
    train_data_path = os.path.join(CELEB_A_SPOOF_DATA_PATH, 'Data', 'train')
    test_data_path = os.path.join(CELEB_A_SPOOF_DATA_PATH, 'Data', 'test')
    train_directories = os.listdir(train_data_path)
    test_directories = os.listdir(test_data_path)
    validation_directories = []

    for _ in range(len(test_directories)):
        i = random.randint(0, len(train_directories) - 1)
        validation_directories.append(train_directories.pop(i))

    (output_train_live_data_path,
     output_train_spoof_data_path,
     output_validation_live_data_path,
     output_validation_spoof_data_path,
     output_test_live_data_path,
     output_test_spoof_data_path) = get_output_directories()

    progress_bar = tqdm(total=len(train_directories) + len(validation_directories) + len(test_directories))
    for identity_directories, directories_path, output_live_path, output_spoof_path in ((train_directories,
                                                                                         train_data_path,
                                                                                         output_train_live_data_path,
                                                                                         output_train_spoof_data_path),
                                                                                        (validation_directories,
                                                                                         train_data_path,
                                                                                         output_validation_live_data_path,
                                                                                         output_validation_spoof_data_path),
                                                                                        (test_directories,
                                                                                         test_data_path,
                                                                                         output_test_live_data_path,
                                                                                         output_test_spoof_data_path)):
        for identity in identity_directories:
            identity_path = os.path.join(directories_path, identity)
            identity_live_path = os.path.join(identity_path, 'live')
            identity_spoof_path = os.path.join(identity_path, 'spoof')

            for input_path, output_path in ((identity_live_path, output_live_path),
                                            (identity_spoof_path, output_spoof_path)):
                if os.path.isdir(input_path):
                    files = os.listdir(input_path)
                    images = filter(is_image, files)
                    images_paths = [os.path.join(input_path, file) for file in images]

                    for image_path in images_paths:
                        file_name = os.path.splitext(os.path.basename(image_path))[0]
                        bounding_box_file = os.path.join(input_path, f'{file_name}_BB.txt')

                        image = cv2.imread(image_path, cv2.IMREAD_COLOR)
                        if image is None or image.shape[0] == 0 or image.shape[1] == 0:
                            continue

                        face = try_crop_face_from_bounding_box_file(bounding_box_file, image)
                        output_image_path = os.path.join(output_path, f'celeba_spoof_{identity}_{file_name}.png')

                        if PRE_PROCESS_IMAGE:
                            save_image(output_image_path, pre_process_image(face))
                        else:
                            save_image(output_image_path, face)

            progress_bar.update()
    progress_bar.close()


def prepare_lcc_fasd_data():
    train_data_path = os.path.join(LCC_FASD_DATA_PATH, 'LCC_FASD_training')
    validation_data_path = os.path.join(LCC_FASD_DATA_PATH, 'LCC_FASD_development')
    test_data_path = os.path.join(LCC_FASD_DATA_PATH, 'LCC_FASD_evaluation')

    (output_train_live_data_path,
     output_train_spoof_data_path,
     output_validation_live_data_path,
     output_validation_spoof_data_path,
     output_test_live_data_path,
     output_test_spoof_data_path) = get_output_directories()

    train_data_live_path = os.path.join(train_data_path, 'real')
    train_data_spoof_path = os.path.join(train_data_path, 'spoof')
    validation_data_live_path = os.path.join(validation_data_path, 'real')
    validation_data_spoof_path = os.path.join(validation_data_path, 'spoof')
    test_data_live_path = os.path.join(test_data_path, 'real')
    test_data_spoof_path = os.path.join(test_data_path, 'spoof')

    train_live_images_paths = get_image_paths(train_data_live_path)
    train_spoof_images_paths = get_image_paths(train_data_spoof_path)
    validation_live_images_paths = get_image_paths(validation_data_live_path)
    validation_spoof_images_paths = get_image_paths(validation_data_spoof_path)
    test_live_images_paths = get_image_paths(test_data_live_path)
    test_spoof_images_paths = get_image_paths(test_data_spoof_path)

    progress_bar = tqdm(total=len(train_live_images_paths) +
                              len(train_spoof_images_paths) +
                              len(validation_live_images_paths) +
                              len(validation_spoof_images_paths) +
                              len(test_live_images_paths) +
                              len(test_spoof_images_paths))
    for input_images_paths, output_path in ((train_live_images_paths, output_train_live_data_path),
                                            (train_spoof_images_paths, output_train_spoof_data_path),
                                            (validation_live_images_paths, output_validation_live_data_path),
                                            (validation_spoof_images_paths, output_validation_spoof_data_path),
                                            (test_live_images_paths, output_test_live_data_path),
                                            (test_spoof_images_paths, output_test_spoof_data_path)):
        for input_image_path in input_images_paths:
            image = cv2.imread(input_image_path, cv2.IMREAD_COLOR)
            if image is None or image.shape[0] == 0 or image.shape[1] == 0:
                continue

            face = safe_crop_face(image)
            file_name = os.path.splitext(os.path.basename(input_image_path))[0]
            output_image_path = os.path.join(output_path, f'lcc_fasd_{file_name}.png')

            if PRE_PROCESS_IMAGE:
                save_image(output_image_path, pre_process_image(face))
            else:
                save_image(output_image_path, face)

            progress_bar.update()
    progress_bar.close()


def augment_images():
    (output_train_live_data_path,
     output_train_spoof_data_path,
     output_validation_live_data_path,
     output_validation_spoof_data_path,
     output_test_live_data_path,
     output_test_spoof_data_path) = get_output_directories()

    augmentation_locations = []
    if AUGMENT_TRAIN_DATA:
        augmentation_locations.append((output_train_live_data_path, output_train_spoof_data_path))
    if AUGMENT_VALIDATION_DATA:
        augmentation_locations.append((output_validation_live_data_path, output_validation_spoof_data_path))
    if AUGMENT_TEST_DATA:
        augmentation_locations.append((output_test_live_data_path, output_test_spoof_data_path))

    for live_data_path, spoof_data_path in augmentation_locations:
        live_image_paths = get_image_paths(live_data_path)
        spoof_image_paths = get_image_paths(spoof_data_path)

        if REMOVE_PREVIOUS_AUGMENTATIONS:
            for image_path in live_image_paths + spoof_image_paths:
                if os.path.basename(image_path).find('_aug_') != -1:
                    os.remove(image_path)
            live_image_paths = get_image_paths(live_data_path)
            spoof_image_paths = get_image_paths(spoof_data_path)

        if AUGMENT_AND_BALANCE_LIVE_AND_SPOOF:
            max_count = max(len(live_image_paths), len(spoof_image_paths))
            max_count_after_augmentation = max_count * (1 + AUGMENTATION_PER_IMAGE)

            live_augmentation_per_image = to_int(max_count_after_augmentation / len(live_image_paths))
            spoof_augmentation_per_image = to_int(max_count_after_augmentation / len(spoof_image_paths))
        else:
            live_augmentation_per_image = AUGMENTATION_PER_LIVE_IMAGE
            spoof_augmentation_per_image = AUGMENTATION_PER_SPOOF_IMAGE

        progress_bar = tqdm(total=len(live_image_paths) + len(spoof_image_paths))
        for (image_paths, augmentation_per_image) in ((live_image_paths, live_augmentation_per_image),
                                                      (spoof_image_paths, spoof_augmentation_per_image)):
            for image_path in image_paths:
                image = read_image(image_path)
                image_base_dir = os.path.split(image_path)[0]
                file_name = os.path.splitext(os.path.basename(image_path))[0]
                for i in range(augmentation_per_image):
                    save_image(os.path.join(image_base_dir, f'{file_name}_aug_{i}.png'), generate_augmentation(image))
                progress_bar.update()
        progress_bar.close()


def generate_local_spoofs():
    (output_train_live_data_path,
     output_train_spoof_data_path,
     output_validation_live_data_path,
     output_validation_spoof_data_path,
     output_test_live_data_path,
     output_test_spoof_data_path) = get_output_directories()

    augmentation_locations = []
    if GENERATE_LOCAL_SPOOFS_FOR_TRAIN_DATA:
        augmentation_locations.append((output_train_live_data_path, output_train_spoof_data_path))
    if GENERATE_LOCAL_SPOOFS_FOR_VALIDATION_DATA:
        augmentation_locations.append((output_validation_live_data_path, output_validation_spoof_data_path))
    if GENERATE_LOCAL_SPOOFS_FOR_TEST_DATA:
        augmentation_locations.append((output_test_live_data_path, output_test_spoof_data_path))

    for live_data_path, spoof_data_path in augmentation_locations:
        live_image_paths = get_image_paths(live_data_path)
        spoof_image_paths = get_image_paths(spoof_data_path)

        progress_bar = tqdm(total=len(live_image_paths))
        for image_path in live_image_paths:
            live_image = read_image(image_path)
            spoof_image = read_image(random.choice(spoof_image_paths))
            local_spoof = generate_local_spoof(live_image, spoof_image)
            file_name = os.path.splitext(os.path.basename(image_path))[0]
            for i in range(LOCAL_SPOOF_PER_LIVE_IMAGE):
                save_image(os.path.join(spoof_data_path, f'{file_name}_local_{i}.png'), local_spoof)
            progress_bar.update()
        progress_bar.close()


def main():
    prepare_celeba_spoof_data()
    prepare_lcc_fasd_data()
    augment_images()
    generate_local_spoofs()


if __name__ == '__main__':
    main()
