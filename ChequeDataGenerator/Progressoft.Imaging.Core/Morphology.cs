using Progressoft.Imaging.Core.Common;
using System;
using System.Drawing;
using System.Drawing.Imaging;

namespace Progressoft.Imaging.Core
{
    /// <summary>
    /// Applies Basic Morphological Operations To 8-Bit Bitmaps
    /// </summary>
    public static unsafe class Morphology
    {
        /// <summary>
        /// Applies Dilation 3X3 To An Image Of 8 Bit
        /// </summary>
        /// <param name="inputImage">The Bitmap To Be Dilated</param>
        /// <param name="outputImage">Reference To The Resulted Dilated Bitmap</param>
        /// <returns>Error Code:
        /// 0: Success.
        /// 1: Invalid Pixel Format.
        /// 2: Generic Error</returns>
        public static Bitmap Dilation3x3(Bitmap inputImage)
        {
            Bitmap outputImage = null;
            Bitmap bmTemp = null;
            BitmapData bmData = null;// for b
            BitmapData bmData1 = null; //for bmTemp

            try
            {
                if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed) return null;
                outputImage = (Bitmap)inputImage.Clone();
                bmTemp = (Bitmap)outputImage.Clone();

                bmData = outputImage.LockBits(new Rectangle(0, 0, outputImage.Width, outputImage.Height), ImageLockMode.ReadWrite, outputImage.PixelFormat);
                bmData1 = bmTemp.LockBits(new Rectangle(0, 0, outputImage.Width, outputImage.Height), ImageLockMode.ReadWrite, outputImage.PixelFormat);

                int stride = bmData.Stride;
                System.IntPtr Scan0 = bmData.Scan0;
                System.IntPtr Scan01 = bmData1.Scan0;

                int h = outputImage.Height;
                int w = outputImage.Width;
                byte* p = (byte*)(void*)Scan0;
                byte* p1 = (byte*)(void*)Scan01;
                int nOffset = stride - outputImage.Width;

                int nPixel = 0;

                p += stride;
                p1 += stride;

                for (int y = 3; y < h - 3; ++y)
                {
                    p += 3;
                    p1 += 3;
                    for (int x1 = 3; x1 < w - 3; ++x1)
                    {
                        nPixel = Math.Min((p1 - (stride) - 1)[0], (p1 - (stride))[0]);
                        nPixel = Math.Min(nPixel, (p1 - stride + 1)[0]);
                        nPixel = Math.Min(nPixel, (p1 - 1)[0]);
                        nPixel = Math.Min(nPixel, (p1 + 1)[0]);
                        nPixel = Math.Min(nPixel, (p1 + stride - 1)[0]);
                        nPixel = Math.Min(nPixel, (p1 + stride)[0]);
                        nPixel = Math.Min(nPixel, (p1 + stride + 1)[0]);
                        if (nPixel < 50) nPixel = 0;
                        p[0] = (byte)nPixel;

                        p++;
                        p1++;
                    }

                    p += 3 + nOffset;
                    p1 += 3 + nOffset;
                }
                outputImage.UnlockBits(bmData);
                bmTemp.UnlockBits(bmData1);
                bmTemp.Dispose();
                return outputImage;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Applies Erosion 3X3 For An 8 bit Bitmaps
        /// </summary>
        /// <param name="inputImage">The Bitmap To Be Erosed.</param>
        /// <param name="outputImage">Reference To The Resulted Erosed Bitmap</param>
        /// <returns>Error Code:
        /// 0: Success.
        /// 1: Invalid Pixel Format.
        /// 2: Generic Error.</returns>
        public static Bitmap Erosion3x3(Bitmap inputImage)
        {
            Bitmap outputImage = null;
            Bitmap bmTemp = null;
            BitmapData bmData = null; //for b
            BitmapData bmData1 = null; //for bmTemp

            try
            {
                if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed) return null;

                outputImage = (Bitmap)inputImage.Clone();
                bmTemp = (Bitmap)outputImage.Clone();
                bmData = outputImage.LockBits(new Rectangle(0, 0, outputImage.Width, outputImage.Height), ImageLockMode.ReadWrite, outputImage.PixelFormat);
                bmData1 = bmTemp.LockBits(new Rectangle(0, 0, outputImage.Width, outputImage.Height), ImageLockMode.ReadWrite, outputImage.PixelFormat);

                int stride = bmData.Stride;
                System.IntPtr Scan0 = bmData.Scan0;
                System.IntPtr Scan01 = bmData1.Scan0;
                int w = outputImage.Width;
                int h = outputImage.Height;
                byte* p = (byte*)(void*)Scan0;
                byte* p1 = (byte*)(void*)Scan01;
                int nOffset = stride - w;

                int nPixel = 0;

                p += stride;
                p1 += stride;

                for (int y = 2; y < h - 3; ++y)
                {
                    p++;
                    p1++;
                    for (int x1 = 3; x1 < w - 3; ++x1)
                    {
                        nPixel = Math.Max((p1 - (stride) - 1)[0], (p1 - (stride))[0]);
                        nPixel = Math.Max(nPixel, (p1 - stride + 1)[0]);
                        nPixel = Math.Max(nPixel, (p1 - 1)[0]);
                        nPixel = Math.Max(nPixel, (p1 + 1)[0]);
                        nPixel = Math.Max(nPixel, (p1 + stride - 1)[0]);
                        nPixel = Math.Max(nPixel, (p1 + stride)[0]);
                        nPixel = Math.Max(nPixel, (p1 + stride + 1)[0]);

                        p[0] = (byte)nPixel;

                        p++;
                        p1++;
                    }

                    p += nOffset;
                    p1 += nOffset;
                }

                bmTemp.UnlockBits(bmData1);
                bmTemp.Dispose();
                outputImage = (Bitmap)outputImage.Clone();
                return outputImage;
            } //try
            catch
            {
                return null;
            }
        } //Erosion3X3

        /// <summary>
        /// Applies Dilation 2X2 To 8 Bit Bitmaps
        /// </summary>
        /// <param name="inputImage">The Bitmap To Be Dilated</param>
        /// <param name="outputImage">Reference To The Resulted Dilated Image.</param>
        /// <returns>Error Code:
        /// 0: Success.
        /// 1: Invalid Pixel Format.
        /// 2: Generic Error.</returns>
        public static Bitmap Dilation2x2(Bitmap inputImage)
        {
            Bitmap b = null;
            BitmapData bmData = null; //for  b
            BitmapData bmDataS = null; //for bResult
            Bitmap outputImage = null;

            try
            {
                if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed)
                    return null;
                b = (Bitmap)inputImage.Clone();

                int w = b.Width;
                int h = b.Height;
                outputImage = BitmapFactory.CreateGrayImage(w, h);

                bmData = b.LockBits(new Rectangle(0, 0, b.Width, b.Height), ImageLockMode.ReadWrite, b.PixelFormat);
                bmDataS = outputImage.LockBits(new Rectangle(0, 0, outputImage.Width, outputImage.Height), ImageLockMode.ReadWrite, outputImage.PixelFormat);
                int stride = bmData.Stride;
                System.IntPtr Scan0 = bmData.Scan0;
                System.IntPtr Scan0S = bmDataS.Scan0;

                byte* p = (byte*)(void*)Scan0;
                byte* pS = (byte*)(void*)Scan0S;
                int nPixel = 0;
                for (int y = 0; y < h; y++)
                {
                    for (int x1 = 0; x1 < w; x1++)
                    {
                        if (x1 == w - 1)
                        {
                            pS[(y * stride) + x1] = 255;
                            continue;
                        }
                        if (y == h - 1)
                        {
                            pS[(y * stride) + x1] = 255;
                            continue;
                        }
                        if (p[(y * stride) + x1] == 255)
                        {
                            nPixel = Math.Min(p[(y * stride) + x1], p[(y * stride) + x1 + 1]);
                            nPixel = Math.Min(nPixel, p[((y + 1) * (stride)) + x1]);
                            nPixel = Math.Min(nPixel, p[((y + 1) * stride) + (x1 + 1)]);
                            pS[(y * stride) + x1] = (byte)nPixel;
                        }
                    }
                } //for y

                b.UnlockBits(bmData);
                b.Dispose();

                outputImage.UnlockBits(bmDataS);
                return outputImage;
            } //try
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Applies Erosion 2X2 To 8 Bit Bitmaps
        /// </summary>
        /// <param name="inputImage">The Bitmap To Be Erosed</param>
        /// <param name="outputImage">Reference To The Resulted Erosed Image.</param>
        /// <returns>Error Code:
        /// 0: Success.
        /// 1: Invalid Pixel Format.
        /// 2: Generic Error.</returns>
        public static Bitmap Erosion2X2(Bitmap inputImage)
        {
            Bitmap b = null;
            BitmapData bmData = null; //for b
            BitmapData bmDataS = null; // for bResult
            Bitmap outputImage = null;

            try
            {
                if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed)
                    return null;
                b = (Bitmap)inputImage.Clone();

                int w = b.Width;
                int h = b.Height;
                outputImage = BitmapFactory.CreateGrayImage(w, h);

                bmData = b.LockBits(new Rectangle(0, 0, b.Width, b.Height), ImageLockMode.ReadWrite, b.PixelFormat);
                bmDataS = outputImage.LockBits(new Rectangle(0, 0, outputImage.Width, outputImage.Height), ImageLockMode.ReadWrite, outputImage.PixelFormat);
                int stride = bmData.Stride;
                System.IntPtr Scan0 = bmData.Scan0;
                System.IntPtr Scan0S = bmDataS.Scan0;

                byte* p = (byte*)(void*)Scan0;
                byte* pS = (byte*)(void*)Scan0S;
                int nPixel = 0;
                for (int y = 0; y < h; y++)
                {
                    for (int x1 = 0; x1 < w; x1++)
                    {
                        if (x1 + 2 == w || y + 2 == h)
                        {
                            pS[(y * stride) + x1] = 255;
                            continue;
                        }
                        if (p[(y * stride) + x1] != 255)
                        {
                            nPixel = Math.Max(p[(y * stride) + x1], p[(y * stride) + x1 + 1]);
                            nPixel = Math.Max(nPixel, p[((y + 1) * (stride)) + x1]);
                            nPixel = Math.Max(nPixel, p[((y + 1) * stride) + (x1 + 1)]);
                            pS[(y * stride) + x1] = (byte)nPixel;
                        }
                        else
                            pS[(y * stride) + x1] = 255;
                    }
                } //for y

                b.UnlockBits(bmData);
                b.Dispose();

                outputImage.UnlockBits(bmDataS);
                return outputImage;
            } //try
            catch
            {
                return null;
            }
        } //Erosion 2X2

        public static Bitmap Erosion5x5(Bitmap inputImage)
        {
            BitmapData bmData = null; //for b
            BitmapData bmData1 = null; //for bmTemp
            Bitmap outputImage = null;
            try
            {
                if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed)
                    return null;

                int w = inputImage.Width;
                int h = inputImage.Height;

                outputImage = inputImage.Clone() as Bitmap;
                Rectangle rectLock = new Rectangle(0, 0, w, h);
                bmData = inputImage.LockBits(rectLock, ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                bmData1 = outputImage.LockBits(rectLock, ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);

                int stride = bmData.Stride;
                System.IntPtr Scan0 = bmData.Scan0;
                System.IntPtr Scan01 = bmData1.Scan0;

                byte* p = (byte*)(void*)Scan0;
                byte* p1 = (byte*)(void*)Scan01;
                int nOffset = stride - w + 4;

                int s2 = 2 * stride;

                p += s2 + 2;
                p1 += s2 + 2;

                for (int y = 2; y < h - 2; ++y)
                {
                    for (int x1 = 2; x1 < w - 2; ++x1)
                    {
                        if (p1[0] < 120)
                        {
                            byte* p_M_s2 = p - s2;
                            byte* p_M_s = p - stride;
                            byte* p_P_s = p + stride;
                            byte* p_P_s2 = p + s2;

                            p1[0] =
                            Math.Max((p_M_s2 - 2)[0],
                            Math.Max((p_M_s2 - 1)[0],
                            Math.Max((p_M_s2)[0],
                            Math.Max((p_M_s2 + 1)[0],
                            Math.Max((p_M_s2 + 2)[0],
                            Math.Max((p_M_s - 2)[0],
                            Math.Max((p_M_s - 1)[0],
                            Math.Max((p_M_s)[0],
                            Math.Max((p_M_s + 1)[0],
                            Math.Max((p_M_s + 2)[0],
                            Math.Max((p - 2)[0],
                            Math.Max((p - 1)[0],
                            Math.Max(p[0],
                            Math.Max((p + 1)[0],
                            Math.Max((p + 2)[0],
                            Math.Max((p_P_s - 2)[0],
                            Math.Max((p_P_s - 1)[0],
                            Math.Max((p_P_s)[0],
                            Math.Max((p_P_s + 1)[0],
                            Math.Max((p_P_s + 2)[0],
                            Math.Max((p_P_s2 - 2)[0],
                            Math.Max((p_P_s2 - 1)[0],
                            Math.Max((p_P_s2)[0],
                            Math.Max((p_P_s2 + 1)[0], (p_P_s2 + 2)[0]))))))))))))))))))))))));
                        }

                        p++;
                        p1++;
                    }

                    p += nOffset;
                    p1 += nOffset;
                }

                inputImage.UnlockBits(bmData);
                outputImage.UnlockBits(bmData1);

                return outputImage;
            } //try
            catch
            {
                return null;
            }
        }

        public static Bitmap Dialation5x5(Bitmap inputImage)
        {
            BitmapData bmData = null; //for b
            BitmapData bmData1 = null; //for bmTemp
            Bitmap outputImage = null;
            try
            {
                if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed)
                    return null;

                int w = inputImage.Width;
                int h = inputImage.Height;

                outputImage = inputImage.Clone() as Bitmap;
                Rectangle rectLock = new Rectangle(0, 0, w, h);
                bmData = inputImage.LockBits(rectLock, ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                bmData1 = outputImage.LockBits(rectLock, ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);

                int stride = bmData.Stride;
                System.IntPtr Scan0 = bmData.Scan0;
                System.IntPtr Scan01 = bmData1.Scan0;

                unsafe
                {
                    byte* p = (byte*)(void*)Scan0;
                    byte* p1 = (byte*)(void*)Scan01;
                    int nOffset = stride - w + 4;

                    int s2 = 2 * stride;

                    p += s2 + 2;
                    p1 += s2 + 2;

                    for (int y = 2; y < h - 2; ++y)
                    {
                        for (int x1 = 2; x1 < w - 2; ++x1)
                        {
                            byte* p_M_s2 = p - s2;
                            byte* p_M_s = p - stride;
                            byte* p_P_s = p + stride;
                            byte* p_P_s2 = p + s2;

                            p1[0] =
                            Math.Min((p_M_s2 - 2)[0],
                            Math.Min((p_M_s2 - 1)[0],
                            Math.Min((p_M_s2)[0],
                            Math.Min((p_M_s2 + 1)[0],
                            Math.Min((p_M_s2 + 2)[0],
                            Math.Min((p_M_s - 2)[0],
                            Math.Min((p_M_s - 1)[0],
                            Math.Min((p_M_s)[0],
                            Math.Min((p_M_s + 1)[0],
                            Math.Min((p_M_s + 2)[0],
                            Math.Min((p - 2)[0],
                            Math.Min((p - 1)[0],
                            Math.Min(p[0],
                            Math.Min((p + 1)[0],
                            Math.Min((p + 2)[0],
                            Math.Min((p_P_s - 2)[0],
                            Math.Min((p_P_s - 1)[0],
                            Math.Min((p_P_s)[0],
                            Math.Min((p_P_s + 1)[0],
                            Math.Min((p_P_s + 2)[0],
                            Math.Min((p_P_s2 - 2)[0],
                            Math.Min((p_P_s2 - 1)[0],
                            Math.Min((p_P_s2)[0],
                            Math.Min((p_P_s2 + 1)[0], (p_P_s2 + 2)[0]))))))))))))))))))))))));

                            p++;
                            p1++;
                        }

                        p += nOffset;
                        p1 += nOffset;
                    }
                }

                inputImage.UnlockBits(bmData);
                outputImage.UnlockBits(bmData1);
                return outputImage;
            } //try
            catch
            {
                return null;
            }
        }

        public static Bitmap Erosion7x7(Bitmap inputImage)
        {
            BitmapData bmData = null; //for b
            BitmapData bmData1 = null; //for bmTemp
            Bitmap outputImage = null;
            try
            {
                if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed)
                    return null;

                int w = inputImage.Width;
                int h = inputImage.Height;

                outputImage = inputImage.Clone() as Bitmap;
                Rectangle rectLock = new Rectangle(0, 0, w, h);
                bmData = inputImage.LockBits(rectLock, ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                bmData1 = outputImage.LockBits(rectLock, ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);

                int stride = bmData.Stride;
                System.IntPtr Scan0 = bmData.Scan0;
                System.IntPtr Scan01 = bmData1.Scan0;

                byte* p = (byte*)(void*)Scan0;
                byte* p1 = (byte*)(void*)Scan01;
                int nOffset = stride - w + 2 * 3;

                int s2 = 2 * stride;
                int s3 = 3 * stride;

                p += s3 + 3;
                p1 += s3 + 3;

                for (int y = 3; y < h - 3; ++y)
                {
                    for (int x1 = 3; x1 < w - 3; ++x1)
                    {
                        byte* p_M_s3 = p - s3;
                        byte* p_M_s2 = p - s2;
                        byte* p_M_s = p - stride;
                        byte* p_P_s = p + stride;
                        byte* p_P_s2 = p + s2;
                        byte* p_P_s3 = p + s3;

                        p1[0] =
                        Math.Max((p_M_s3 - 2)[0],
                        Math.Max((p_M_s3 - 1)[0],
                        Math.Max((p_M_s3)[0],
                        Math.Max((p_M_s3 + 1)[0],
                        Math.Max((p_M_s3 + 2)[0],

                        Math.Max((p_M_s2 - 2)[0],
                        Math.Max((p_M_s2 - 1)[0],
                        Math.Max((p_M_s2)[0],
                        Math.Max((p_M_s2 + 1)[0],
                        Math.Max((p_M_s2 + 2)[0],

                        Math.Max((p_M_s2 - 2)[0],
                        Math.Max((p_M_s2 - 1)[0],
                        Math.Max((p_M_s2)[0],
                        Math.Max((p_M_s2 + 1)[0],
                        Math.Max((p_M_s2 + 2)[0],

                        Math.Max((p_M_s - 2)[0],
                        Math.Max((p_M_s - 1)[0],
                        Math.Max((p_M_s)[0],
                        Math.Max((p_M_s + 1)[0],
                        Math.Max((p_M_s + 2)[0],

                        Math.Max((p - 2)[0],
                        Math.Max((p - 1)[0],
                        Math.Max(p[0],
                        Math.Max((p + 1)[0],
                        Math.Max((p + 2)[0],

                        Math.Max((p_P_s - 2)[0],
                        Math.Max((p_P_s - 1)[0],
                        Math.Max((p_P_s)[0],
                        Math.Max((p_P_s + 1)[0],
                        Math.Max((p_P_s + 2)[0],

                        Math.Max((p_P_s2 - 2)[0],
                        Math.Max((p_P_s2 - 1)[0],
                        Math.Max((p_P_s2)[0],
                        Math.Max((p_P_s2 + 1)[0],
                        Math.Max((p_P_s2 + 2)[0],

                        Math.Max((p_P_s3 - 2)[0],
                        Math.Max((p_P_s3 - 1)[0],
                        Math.Max((p_P_s3)[0],
                        Math.Max((p_P_s3 + 1)[0], (p_P_s3 + 2)[0]

                        )))))))))))))))))))))))))))))))))))))));

                        p++;
                        p1++;
                    }

                    p += nOffset;
                    p1 += nOffset;
                }

                inputImage.UnlockBits(bmData);
                outputImage.UnlockBits(bmData1);

                return outputImage;
            } //try
            catch
            {
                return null;
            }
        }

        public static Bitmap Erosion9x9(Bitmap inputImage)
        {
            BitmapData bmData = null; //for b
            BitmapData bmData1 = null; //for bmTemp
            Bitmap outputImage = null;
            try
            {
                if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed)
                    return null;

                int w = inputImage.Width;
                int h = inputImage.Height;

                outputImage = inputImage.Clone() as Bitmap;
                Rectangle rectLock = new Rectangle(0, 0, w, h);
                bmData = inputImage.LockBits(rectLock, ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                bmData1 = outputImage.LockBits(rectLock, ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);

                int stride = bmData.Stride;
                System.IntPtr Scan0 = bmData.Scan0;
                System.IntPtr Scan01 = bmData1.Scan0;

                byte* p = (byte*)(void*)Scan0;
                byte* p1 = (byte*)(void*)Scan01;
                int nOffset = stride - w + 2 * 4;

                int s2 = 2 * stride;
                int s3 = 3 * stride;
                int s4 = 4 * stride;

                p += s4 + 4;
                p1 += s4 + 4;

                for (int y = 4; y < h - 4; ++y)
                {
                    for (int x1 = 4; x1 < w - 4; ++x1)
                    {
                        byte* p_M_s4 = p - s4;
                        byte* p_M_s3 = p - s3;
                        byte* p_M_s2 = p - s2;
                        byte* p_M_s = p - stride;
                        byte* p_P_s = p + stride;
                        byte* p_P_s2 = p + s2;
                        byte* p_P_s3 = p + s3;
                        byte* p_P_s4 = p + s4;

                        p1[0] =
                        Math.Max((p_M_s4 - 2)[0],
                        Math.Max((p_M_s4 - 1)[0],
                        Math.Max((p_M_s4)[0],
                        Math.Max((p_M_s4 + 1)[0],
                        Math.Max((p_M_s4 + 2)[0],

                        Math.Max((p_M_s3 - 2)[0],
                        Math.Max((p_M_s3 - 1)[0],
                        Math.Max((p_M_s3)[0],
                        Math.Max((p_M_s3 + 1)[0],
                        Math.Max((p_M_s3 + 2)[0],

                        Math.Max((p_M_s2 - 2)[0],
                        Math.Max((p_M_s2 - 1)[0],
                        Math.Max((p_M_s2)[0],
                        Math.Max((p_M_s2 + 1)[0],
                        Math.Max((p_M_s2 + 2)[0],

                        Math.Max((p_M_s2 - 2)[0],
                        Math.Max((p_M_s2 - 1)[0],
                        Math.Max((p_M_s2)[0],
                        Math.Max((p_M_s2 + 1)[0],
                        Math.Max((p_M_s2 + 2)[0],

                        Math.Max((p_M_s - 2)[0],
                        Math.Max((p_M_s - 1)[0],
                        Math.Max((p_M_s)[0],
                        Math.Max((p_M_s + 1)[0],
                        Math.Max((p_M_s + 2)[0],

                        Math.Max((p - 2)[0],
                        Math.Max((p - 1)[0],
                        Math.Max(p[0],
                        Math.Max((p + 1)[0],
                        Math.Max((p + 2)[0],

                        Math.Max((p_P_s - 2)[0],
                        Math.Max((p_P_s - 1)[0],
                        Math.Max((p_P_s)[0],
                        Math.Max((p_P_s + 1)[0],
                        Math.Max((p_P_s + 2)[0],

                        Math.Max((p_P_s2 - 2)[0],
                        Math.Max((p_P_s2 - 1)[0],
                        Math.Max((p_P_s2)[0],
                        Math.Max((p_P_s2 + 1)[0],
                        Math.Max((p_P_s2 + 2)[0],

                        Math.Max((p_P_s3 - 2)[0],
                        Math.Max((p_P_s3 - 1)[0],
                        Math.Max((p_P_s3)[0],
                        Math.Max((p_P_s3 + 1)[0],
                        Math.Max((p_P_s3 + 2)[0],

                        Math.Max((p_P_s4 - 2)[0],
                        Math.Max((p_P_s4 - 1)[0],
                        Math.Max((p_P_s4)[0],
                        Math.Max((p_P_s4 + 1)[0], (p_P_s4 + 2)[0]

                        )))))))))))))))))))))))))))))))))))))))))))))))));

                        p++;
                        p1++;
                    }

                    p += nOffset;
                    p1 += nOffset;
                }

                inputImage.UnlockBits(bmData);
                outputImage.UnlockBits(bmData1);

                return outputImage;
            } //try
            catch
            {
                return null;
            }
        }

        public static Bitmap Dialation9x9(Bitmap inputImage)
        {
            BitmapData bmData = null; //for b
            BitmapData bmData1 = null; //for bmTemp
            Bitmap outputImage = null;
            try
            {
                if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed)
                    return null;

                int w = inputImage.Width;
                int h = inputImage.Height;

                outputImage = inputImage.Clone() as Bitmap;
                Rectangle rectLock = new Rectangle(0, 0, w, h);
                bmData = inputImage.LockBits(rectLock, ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                bmData1 = outputImage.LockBits(rectLock, ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);

                int stride = bmData.Stride;
                System.IntPtr Scan0 = bmData.Scan0;
                System.IntPtr Scan01 = bmData1.Scan0;

                byte* p = (byte*)(void*)Scan0;
                byte* p1 = (byte*)(void*)Scan01;
                int nOffset = stride - w + 2 * 4;

                int s2 = 2 * stride;
                int s3 = 3 * stride;
                int s4 = 4 * stride;

                p += s4 + 4;
                p1 += s4 + 4;

                for (int y = 4; y < h - 4; ++y)
                {
                    for (int x1 = 4; x1 < w - 4; ++x1)
                    {
                        byte* p_M_s4 = p - s4;
                        byte* p_M_s3 = p - s3;
                        byte* p_M_s2 = p - s2;
                        byte* p_M_s = p - stride;
                        byte* p_P_s = p + stride;
                        byte* p_P_s2 = p + s2;
                        byte* p_P_s3 = p + s3;
                        byte* p_P_s4 = p + s4;

                        p1[0] =
                        Math.Min((p_M_s4 - 2)[0],
                        Math.Min((p_M_s4 - 1)[0],
                        Math.Min((p_M_s4)[0],
                        Math.Min((p_M_s4 + 1)[0],
                        Math.Min((p_M_s4 + 2)[0],

                        Math.Min((p_M_s3 - 2)[0],
                        Math.Min((p_M_s3 - 1)[0],
                        Math.Min((p_M_s3)[0],
                        Math.Min((p_M_s3 + 1)[0],
                        Math.Min((p_M_s3 + 2)[0],

                        Math.Min((p_M_s2 - 2)[0],
                        Math.Min((p_M_s2 - 1)[0],
                        Math.Min((p_M_s2)[0],
                        Math.Min((p_M_s2 + 1)[0],
                        Math.Min((p_M_s2 + 2)[0],

                        Math.Min((p_M_s2 - 2)[0],
                        Math.Min((p_M_s2 - 1)[0],
                        Math.Min((p_M_s2)[0],
                        Math.Min((p_M_s2 + 1)[0],
                        Math.Min((p_M_s2 + 2)[0],

                        Math.Min((p_M_s - 2)[0],
                        Math.Min((p_M_s - 1)[0],
                        Math.Min((p_M_s)[0],
                        Math.Min((p_M_s + 1)[0],
                        Math.Min((p_M_s + 2)[0],

                        Math.Min((p - 2)[0],
                        Math.Min((p - 1)[0],
                        Math.Min(p[0],
                        Math.Min((p + 1)[0],
                        Math.Min((p + 2)[0],

                        Math.Min((p_P_s - 2)[0],
                        Math.Min((p_P_s - 1)[0],
                        Math.Min((p_P_s)[0],
                        Math.Min((p_P_s + 1)[0],
                        Math.Min((p_P_s + 2)[0],

                        Math.Min((p_P_s2 - 2)[0],
                        Math.Min((p_P_s2 - 1)[0],
                        Math.Min((p_P_s2)[0],
                        Math.Min((p_P_s2 + 1)[0],
                        Math.Min((p_P_s2 + 2)[0],

                        Math.Min((p_P_s3 - 2)[0],
                        Math.Min((p_P_s3 - 1)[0],
                        Math.Min((p_P_s3)[0],
                        Math.Min((p_P_s3 + 1)[0],
                        Math.Min((p_P_s3 + 2)[0],

                        Math.Min((p_P_s4 - 2)[0],
                        Math.Min((p_P_s4 - 1)[0],
                        Math.Min((p_P_s4)[0],
                        Math.Min((p_P_s4 + 1)[0], (p_P_s4 + 2)[0]

                        )))))))))))))))))))))))))))))))))))))))))))))))));

                        p++;
                        p1++;
                    }

                    p += nOffset;
                    p1 += nOffset;
                }

                inputImage.UnlockBits(bmData);
                outputImage.UnlockBits(bmData1);

                return outputImage;
            } //try
            catch
            {
                return null;
            }
        }

        public static Bitmap Erosion15x15(Bitmap inputImage)
        {
            BitmapData bmData = null; //for b
            BitmapData bmData1 = null; //for bmTemp
            Bitmap outputImage = null;
            try
            {
                if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed)
                    return null;

                int w = inputImage.Width;
                int h = inputImage.Height;

                outputImage = inputImage.Clone() as Bitmap;
                Rectangle rectLock = new Rectangle(0, 0, w, h);
                bmData = inputImage.LockBits(rectLock, ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                bmData1 = outputImage.LockBits(rectLock, ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);

                int stride = bmData.Stride;
                System.IntPtr Scan0 = bmData.Scan0;
                System.IntPtr Scan01 = bmData1.Scan0;

                byte* p = (byte*)(void*)Scan0;
                byte* p1 = (byte*)(void*)Scan01;
                int nOffset = stride - w + 2 * 7;

                int s2 = 2 * stride;
                int s3 = 3 * stride;
                int s4 = 4 * stride;
                int s5 = 5 * stride;
                int s6 = 6 * stride;
                int s7 = 7 * stride;

                p += s7 + 7;
                p1 += s7 + 7;

                for (int y = 7; y < h - 7; ++y)
                {
                    for (int x1 = 7; x1 < w - 7; ++x1)
                    {
                        byte* p_M_s7 = p - s7;
                        byte* p_M_s6 = p - s6;
                        byte* p_M_s5 = p - s5;
                        byte* p_M_s4 = p - s4;
                        byte* p_M_s3 = p - s3;
                        byte* p_M_s2 = p - s2;
                        byte* p_M_s = p - stride;
                        byte* p_P_s = p + stride;
                        byte* p_P_s2 = p + s2;
                        byte* p_P_s3 = p + s3;
                        byte* p_P_s4 = p + s4;
                        byte* p_P_s5 = p + s5;
                        byte* p_P_s6 = p + s6;
                        byte* p_P_s7 = p + s7;

                        p1[0] =

                        Math.Max((p_M_s7 - 2)[0],
                        Math.Max((p_M_s7 - 1)[0],
                        Math.Max((p_M_s7)[0],
                        Math.Max((p_M_s7 + 1)[0],
                        Math.Max((p_M_s7 + 2)[0],

                        Math.Max((p_M_s6 - 2)[0],
                        Math.Max((p_M_s6 - 1)[0],
                        Math.Max((p_M_s6)[0],
                        Math.Max((p_M_s6 + 1)[0],
                        Math.Max((p_M_s6 + 2)[0],

                        Math.Max((p_M_s5 - 2)[0],
                        Math.Max((p_M_s5 - 1)[0],
                        Math.Max((p_M_s5)[0],
                        Math.Max((p_M_s5 + 1)[0],
                        Math.Max((p_M_s5 + 2)[0],

                        Math.Max((p_M_s4 - 2)[0],
                        Math.Max((p_M_s4 - 1)[0],
                        Math.Max((p_M_s4)[0],
                        Math.Max((p_M_s4 + 1)[0],
                        Math.Max((p_M_s4 + 2)[0],

                        Math.Max((p_M_s3 - 2)[0],
                        Math.Max((p_M_s3 - 1)[0],
                        Math.Max((p_M_s3)[0],
                        Math.Max((p_M_s3 + 1)[0],
                        Math.Max((p_M_s3 + 2)[0],

                        Math.Max((p_M_s2 - 2)[0],
                        Math.Max((p_M_s2 - 1)[0],
                        Math.Max((p_M_s2)[0],
                        Math.Max((p_M_s2 + 1)[0],
                        Math.Max((p_M_s2 + 2)[0],

                        Math.Max((p_M_s2 - 2)[0],
                        Math.Max((p_M_s2 - 1)[0],
                        Math.Max((p_M_s2)[0],
                        Math.Max((p_M_s2 + 1)[0],
                        Math.Max((p_M_s2 + 2)[0],

                        Math.Max((p_M_s - 2)[0],
                        Math.Max((p_M_s - 1)[0],
                        Math.Max((p_M_s)[0],
                        Math.Max((p_M_s + 1)[0],
                        Math.Max((p_M_s + 2)[0],

                        Math.Max((p - 2)[0],
                        Math.Max((p - 1)[0],
                        Math.Max(p[0],
                        Math.Max((p + 1)[0],
                        Math.Max((p + 2)[0],

                        Math.Max((p_P_s - 2)[0],
                        Math.Max((p_P_s - 1)[0],
                        Math.Max((p_P_s)[0],
                        Math.Max((p_P_s + 1)[0],
                        Math.Max((p_P_s + 2)[0],

                        Math.Max((p_P_s2 - 2)[0],
                        Math.Max((p_P_s2 - 1)[0],
                        Math.Max((p_P_s2)[0],
                        Math.Max((p_P_s2 + 1)[0],
                        Math.Max((p_P_s2 + 2)[0],

                        Math.Max((p_P_s3 - 2)[0],
                        Math.Max((p_P_s3 - 1)[0],
                        Math.Max((p_P_s3)[0],
                        Math.Max((p_P_s3 + 1)[0],
                        Math.Max((p_P_s3 + 2)[0],

                        Math.Max((p_P_s4 - 2)[0],
                        Math.Max((p_P_s4 - 1)[0],
                        Math.Max((p_P_s4)[0],
                        Math.Max((p_P_s4 + 1)[0],
                        Math.Max((p_P_s4 + 2)[0],

                        Math.Max((p_P_s5 - 2)[0],
                        Math.Max((p_P_s5 - 1)[0],
                        Math.Max((p_P_s5)[0],
                        Math.Max((p_P_s5 + 1)[0],
                        Math.Max((p_P_s5 + 2)[0],

                        Math.Max((p_P_s6 - 2)[0],
                        Math.Max((p_P_s6 - 1)[0],
                        Math.Max((p_P_s6)[0],
                        Math.Max((p_P_s6 + 1)[0],
                        Math.Max((p_P_s6 + 2)[0],

                        Math.Max((p_P_s7 - 2)[0],
                        Math.Max((p_P_s7 - 1)[0],
                        Math.Max((p_P_s7)[0],
                        Math.Max((p_P_s7 + 1)[0], (p_P_s7 + 2)[0]

                        )))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));

                        p++;
                        p1++;
                    }

                    p += nOffset;
                    p1 += nOffset;
                }

                inputImage.UnlockBits(bmData);
                outputImage.UnlockBits(bmData1);
                return outputImage;
            } //try
            catch
            {
                return null;
            }
        }

        public static Bitmap Dialation15x15(Bitmap inputImage)
        {
            BitmapData bmData = null; //for b
            BitmapData bmData1 = null; //for bmTemp
            Bitmap outputImage = null;

            try
            {
                if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed)
                    return null;

                int w = inputImage.Width;
                int h = inputImage.Height;

                outputImage = inputImage.Clone() as Bitmap;
                Rectangle rectLock = new Rectangle(0, 0, w, h);
                bmData = inputImage.LockBits(rectLock, ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                bmData1 = outputImage.LockBits(rectLock, ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);

                int stride = bmData.Stride;
                System.IntPtr Scan0 = bmData.Scan0;
                System.IntPtr Scan01 = bmData1.Scan0;

                byte* p = (byte*)(void*)Scan0;
                byte* p1 = (byte*)(void*)Scan01;
                int nOffset = stride - w + 2 * 7;

                int s2 = 2 * stride;
                int s3 = 3 * stride;
                int s4 = 4 * stride;
                int s5 = 5 * stride;
                int s6 = 6 * stride;
                int s7 = 7 * stride;

                p += s7 + 7;
                p1 += s7 + 7;

                for (int y = 7; y < h - 7; ++y)
                {
                    for (int x1 = 7; x1 < w - 7; ++x1)
                    {
                        byte* p_M_s7 = p - s7;
                        byte* p_M_s6 = p - s6;
                        byte* p_M_s5 = p - s5;
                        byte* p_M_s4 = p - s4;
                        byte* p_M_s3 = p - s3;
                        byte* p_M_s2 = p - s2;
                        byte* p_M_s = p - stride;
                        byte* p_P_s = p + stride;
                        byte* p_P_s2 = p + s2;
                        byte* p_P_s3 = p + s3;
                        byte* p_P_s4 = p + s4;
                        byte* p_P_s5 = p + s5;
                        byte* p_P_s6 = p + s6;
                        byte* p_P_s7 = p + s7;

                        p1[0] =

                        Math.Min((p_M_s7 - 2)[0],
                        Math.Min((p_M_s7 - 1)[0],
                        Math.Min((p_M_s7)[0],
                        Math.Min((p_M_s7 + 1)[0],
                        Math.Min((p_M_s7 + 2)[0],

                        Math.Min((p_M_s6 - 2)[0],
                        Math.Min((p_M_s6 - 1)[0],
                        Math.Min((p_M_s6)[0],
                        Math.Min((p_M_s6 + 1)[0],
                        Math.Min((p_M_s6 + 2)[0],

                        Math.Min((p_M_s5 - 2)[0],
                        Math.Min((p_M_s5 - 1)[0],
                        Math.Min((p_M_s5)[0],
                        Math.Min((p_M_s5 + 1)[0],
                        Math.Min((p_M_s5 + 2)[0],

                        Math.Min((p_M_s4 - 2)[0],
                        Math.Min((p_M_s4 - 1)[0],
                        Math.Min((p_M_s4)[0],
                        Math.Min((p_M_s4 + 1)[0],
                        Math.Min((p_M_s4 + 2)[0],

                        Math.Min((p_M_s3 - 2)[0],
                        Math.Min((p_M_s3 - 1)[0],
                        Math.Min((p_M_s3)[0],
                        Math.Min((p_M_s3 + 1)[0],
                        Math.Min((p_M_s3 + 2)[0],

                        Math.Min((p_M_s2 - 2)[0],
                        Math.Min((p_M_s2 - 1)[0],
                        Math.Min((p_M_s2)[0],
                        Math.Min((p_M_s2 + 1)[0],
                        Math.Min((p_M_s2 + 2)[0],

                        Math.Min((p_M_s2 - 2)[0],
                        Math.Min((p_M_s2 - 1)[0],
                        Math.Min((p_M_s2)[0],
                        Math.Min((p_M_s2 + 1)[0],
                        Math.Min((p_M_s2 + 2)[0],

                        Math.Min((p_M_s - 2)[0],
                        Math.Min((p_M_s - 1)[0],
                        Math.Min((p_M_s)[0],
                        Math.Min((p_M_s + 1)[0],
                        Math.Min((p_M_s + 2)[0],

                        Math.Min((p - 2)[0],
                        Math.Min((p - 1)[0],
                        Math.Min(p[0],
                        Math.Min((p + 1)[0],
                        Math.Min((p + 2)[0],

                        Math.Min((p_P_s - 2)[0],
                        Math.Min((p_P_s - 1)[0],
                        Math.Min((p_P_s)[0],
                        Math.Min((p_P_s + 1)[0],
                        Math.Min((p_P_s + 2)[0],

                        Math.Min((p_P_s2 - 2)[0],
                        Math.Min((p_P_s2 - 1)[0],
                        Math.Min((p_P_s2)[0],
                        Math.Min((p_P_s2 + 1)[0],
                        Math.Min((p_P_s2 + 2)[0],

                        Math.Min((p_P_s3 - 2)[0],
                        Math.Min((p_P_s3 - 1)[0],
                        Math.Min((p_P_s3)[0],
                        Math.Min((p_P_s3 + 1)[0],
                        Math.Min((p_P_s3 + 2)[0],

                        Math.Min((p_P_s4 - 2)[0],
                        Math.Min((p_P_s4 - 1)[0],
                        Math.Min((p_P_s4)[0],
                        Math.Min((p_P_s4 + 1)[0],
                        Math.Min((p_P_s4 + 2)[0],

                        Math.Min((p_P_s5 - 2)[0],
                        Math.Min((p_P_s5 - 1)[0],
                        Math.Min((p_P_s5)[0],
                        Math.Min((p_P_s5 + 1)[0],
                        Math.Min((p_P_s5 + 2)[0],

                        Math.Min((p_P_s6 - 2)[0],
                        Math.Min((p_P_s6 - 1)[0],
                        Math.Min((p_P_s6)[0],
                        Math.Min((p_P_s6 + 1)[0],
                        Math.Min((p_P_s6 + 2)[0],

                        Math.Min((p_P_s7 - 2)[0],
                        Math.Min((p_P_s7 - 1)[0],
                        Math.Min((p_P_s7)[0],
                        Math.Min((p_P_s7 + 1)[0], (p_P_s7 + 2)[0]

                        )))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));

                        p++;
                        p1++;
                    }

                    p += nOffset;
                    p1 += nOffset;
                }
                inputImage.UnlockBits(bmData);
                outputImage.UnlockBits(bmData1);
                return outputImage;
            } //try
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Applies Sphere Dilation To 8 Bit Bitmaps
        /// </summary>
        /// <param name="inputImage">The Bitmap To Be Dilated</param>
        /// <param name="outputImage">The Resulted Dilated Bitmap</param>
        /// <returns>Error Code:
        /// 0: Success.
        /// 1: Invalid Pixel Format.
        /// 2: Generic Error.</returns>
        public static Bitmap SphereDilation(Bitmap inputImage)
        {
            Bitmap b = null;
            Bitmap bmTemp = null;
            BitmapData bmData = null; //for b
            BitmapData bmData2 = null; // for bmTemp
            Bitmap outputImage = null;
            try
            {
                if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed) return null;

                b = (Bitmap)inputImage.Clone();
                bmTemp = (Bitmap)b.Clone();
                bmData = b.LockBits(new Rectangle(0, 0, b.Width, b.Height), ImageLockMode.ReadWrite, b.PixelFormat);
                bmData2 = bmTemp.LockBits(new Rectangle(0, 0, b.Width, b.Height), ImageLockMode.ReadWrite, b.PixelFormat);
                int stride = bmData.Stride;
                System.IntPtr Scan0 = bmData.Scan0;
                System.IntPtr Scan02 = bmData2.Scan0;
                int w = b.Width;
                int h = b.Height;

                byte* p = (byte*)(void*)Scan0;
                byte* p1 = (byte*)(void*)Scan02;
                int nOffset = stride - w;
                int nPixel = 0;
                p += stride * 3;
                p1 += stride * 3;

                for (int y = 3; y < h - 3; y++)
                {
                    p += 4;
                    p1 += 4;
                    for (int x1 = 4; x1 < w - 4; x1++)
                    {
                        nPixel = Math.Min((p1 - (stride * 2) - 2)[0], (p1 - (stride * 2) - 1)[0]);
                        nPixel = Math.Min(nPixel, (p1 - (stride * 2))[0]);
                        nPixel = Math.Min(nPixel, (p1 - (stride * 2) + 1)[0]);
                        nPixel = Math.Min(nPixel, (p1 - (stride * 2) + 2)[0]);
                        nPixel = Math.Min(nPixel, (p1 - stride - 3)[0]);
                        nPixel = Math.Min(nPixel, (p1 - stride - 2)[0]);
                        nPixel = Math.Min(nPixel, (p1 - stride - 1)[0]);
                        nPixel = Math.Min(nPixel, (p1 - stride)[0]);
                        nPixel = Math.Min(nPixel, (p1 - stride + 1)[0]);
                        nPixel = Math.Min(nPixel, (p1 - stride + 2)[0]);
                        nPixel = Math.Min(nPixel, (p1 - stride + 3)[0]);
                        nPixel = Math.Min(nPixel, (p1 - 4)[0]);
                        nPixel = Math.Min(nPixel, (p1 - 3)[0]);
                        nPixel = Math.Min(nPixel, (p1 - 2)[0]);
                        nPixel = Math.Min(nPixel, (p1 - 1)[0]);
                        nPixel = Math.Min(nPixel, (p1)[0]);
                        nPixel = Math.Min(nPixel, (p1 + 1)[0]);
                        nPixel = Math.Min(nPixel, (p1 + 2)[0]);
                        nPixel = Math.Min(nPixel, (p1 + 3)[0]);
                        nPixel = Math.Min(nPixel, (p1 + 4)[0]);
                        nPixel = Math.Min(nPixel, (p1 + stride - 3)[0]);
                        nPixel = Math.Min(nPixel, (p1 + stride - 2)[0]);
                        nPixel = Math.Min(nPixel, (p1 + stride - 1)[0]);
                        nPixel = Math.Min(nPixel, (p1 + stride)[0]);
                        nPixel = Math.Min(nPixel, (p1 + stride + 1)[0]);
                        nPixel = Math.Min(nPixel, (p1 + stride + 2)[0]);
                        nPixel = Math.Min(nPixel, (p1 + stride + 3)[0]);
                        nPixel = Math.Min(nPixel, (p1 + (stride * 2) - 2)[0]);
                        nPixel = Math.Min(nPixel, (p1 + (stride * 2) - 1)[0]);
                        nPixel = Math.Min(nPixel, (p1 + (stride * 2))[0]);
                        nPixel = Math.Min(nPixel, (p1 + (stride * 2) + 1)[0]);
                        nPixel = Math.Min(nPixel, (p1 + (stride * 2) + 2)[0]);
                        p[0] = (byte)nPixel;

                        ++p;
                        ++p1;
                    }
                    p += 4 + nOffset;
                    p1 += 4 + nOffset;
                }

                outputImage = (Bitmap)b.Clone();
                bmTemp.UnlockBits(bmData2);
                bmTemp.Dispose();
                b.UnlockBits(bmData);
                b.Dispose();

                return outputImage;
            } //try
            catch
            {
                return null;
            }
        } //sphere dilation

        /// <summary>
        /// Applies Sphere Erosion To 8 Bit Bitmaps
        /// </summary>
        /// <param name="inputImage">The Bitmap To Be Erosed</param>
        /// <param name="outputImage">The Resulted Erosed Bitmap</param>
        /// <returns>Error Code:
        /// 0: Success.
        /// 1: Invalid Pixel Format.
        /// 2: Generic Error.</returns>
        public static Bitmap SphereErosion(Bitmap inputImage)
        {
            Bitmap b = null;
            Bitmap bmTemp = null;
            BitmapData bmData = null; //for b
            BitmapData bmData2 = null; // for bmTemp
            Bitmap outputImage = null;

            try
            {
                if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed) return null;

                b = (Bitmap)inputImage.Clone();
                bmTemp = (Bitmap)b.Clone();
                bmData = b.LockBits(new Rectangle(0, 0, b.Width, b.Height), ImageLockMode.ReadWrite, b.PixelFormat);
                bmData2 = bmTemp.LockBits(new Rectangle(0, 0, b.Width, b.Height), ImageLockMode.ReadWrite, b.PixelFormat);
                int stride = bmData.Stride;
                System.IntPtr Scan0 = bmData.Scan0;
                System.IntPtr Scan02 = bmData2.Scan0;
                int w = b.Width;
                int h = b.Height;

                byte* p = (byte*)(void*)Scan0;
                byte* p1 = (byte*)(void*)Scan02;
                int nOffset = stride - w;
                int nPixel = 0;
                p += stride * 3;
                p1 += stride * 3;

                for (int y = 3; y < h - 3; y++)
                {
                    p += 4;
                    p1 += 4;
                    for (int x1 = 4; x1 < w - 4; x1++)
                    {
                        nPixel = Math.Max((p1 - (stride * 2) - 2)[0], (p1 - (stride * 2) - 1)[0]);
                        nPixel = Math.Max(nPixel, (p1 - (stride * 2))[0]);
                        nPixel = Math.Max(nPixel, (p1 - (stride * 2) + 1)[0]);
                        nPixel = Math.Max(nPixel, (p1 - (stride * 2) + 2)[0]);
                        nPixel = Math.Max(nPixel, (p1 - stride - 3)[0]);
                        nPixel = Math.Max(nPixel, (p1 - stride - 2)[0]);
                        nPixel = Math.Max(nPixel, (p1 - stride - 1)[0]);
                        nPixel = Math.Max(nPixel, (p1 - stride)[0]);
                        nPixel = Math.Max(nPixel, (p1 - stride + 1)[0]);
                        nPixel = Math.Max(nPixel, (p1 - stride + 2)[0]);
                        nPixel = Math.Max(nPixel, (p1 - stride + 3)[0]);
                        nPixel = Math.Max(nPixel, (p1 - 4)[0]);
                        nPixel = Math.Max(nPixel, (p1 - 3)[0]);
                        nPixel = Math.Max(nPixel, (p1 - 2)[0]);
                        nPixel = Math.Max(nPixel, (p1 - 1)[0]);
                        nPixel = Math.Max(nPixel, (p1)[0]);
                        nPixel = Math.Max(nPixel, (p1 + 1)[0]);
                        nPixel = Math.Max(nPixel, (p1 + 2)[0]);
                        nPixel = Math.Max(nPixel, (p1 + 3)[0]);
                        nPixel = Math.Max(nPixel, (p1 + 4)[0]);
                        nPixel = Math.Max(nPixel, (p1 + stride - 3)[0]);
                        nPixel = Math.Max(nPixel, (p1 + stride - 2)[0]);
                        nPixel = Math.Max(nPixel, (p1 + stride - 1)[0]);
                        nPixel = Math.Max(nPixel, (p1 + stride)[0]);
                        nPixel = Math.Max(nPixel, (p1 + stride + 1)[0]);
                        nPixel = Math.Max(nPixel, (p1 + stride + 2)[0]);
                        nPixel = Math.Max(nPixel, (p1 + stride + 3)[0]);
                        nPixel = Math.Max(nPixel, (p1 + (stride * 2) - 2)[0]);
                        nPixel = Math.Max(nPixel, (p1 + (stride * 2) - 1)[0]);
                        nPixel = Math.Max(nPixel, (p1 + (stride * 2))[0]);
                        nPixel = Math.Max(nPixel, (p1 + (stride * 2) + 1)[0]);
                        nPixel = Math.Max(nPixel, (p1 + (stride * 2) + 2)[0]);
                        p[0] = (byte)nPixel;

                        ++p;
                        ++p1;
                    }
                    p += 4 + nOffset;
                    p1 += 4 + nOffset;
                }
                b.UnlockBits(bmData);
                bmTemp.UnlockBits(bmData2);
                outputImage = (Bitmap)b.Clone();
                b.Dispose();
                bmTemp.Dispose();
                return outputImage;
            } //try
            catch
            {
                return null;
            }
        } //sphere dilation

        /// <summary>
        /// Applies Median Filtering To A Given 8 Bit Bitmap.
        /// Note: Works On Gray-Scaled Bitmaps (Not Binarized)
        /// </summary>
        /// <param name="inputImage"> A bitmap image to be filtered.</param>
        /// <param name="outputImage">Holds the output image.</param>
        /// <param name="filterSize">The Size Of The Filter That Sshould Be Used(A Positive Odd Value).</param>
        /// <returns>
        /// 0: Success.
        /// 1: Invalid pixel format or the filter size is not positive odd number.
        /// 2: Generic error.
        /// </returns>
        public static Bitmap MedianFilter(Bitmap inputImage, int filterSize)
        {
            BitmapData bmpData = null; // for bmpImg
            BitmapData newBmpData = null; // for outputBmp
            Bitmap outputImage = null;

            try
            {
                // check for the validity of the given parameters:
                if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed || filterSize < 1 || filterSize % 2 == 0)
                    return null;
                // get width and height:
                int width = inputImage.Width;
                int height = inputImage.Height;

                // create a bitmap object to hold the result:
                outputImage = new Bitmap(width, height, PixelFormat.Format8bppIndexed)
                {
                    Palette = inputImage.Palette
                };

                // lock image to be processed:
                bmpData = inputImage.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                System.IntPtr scan0 = bmpData.Scan0;
                int stride = bmpData.Stride;

                newBmpData = outputImage.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                System.IntPtr newScan0 = newBmpData.Scan0;
                int newStride = newBmpData.Stride;

                //how many pixels should we skip:
                int skip = (int)Math.Floor((double)filterSize / 2);
                // the median position:
                int position = (int)Math.Ceiling((filterSize * filterSize) / 2f);

                // a window to hold the neighboors:
                int[] window = new int[filterSize * filterSize];
                // an index for the window:
                int index = 0;

                // begain processing
                byte* p = (byte*)(void*)scan0;// original.
                byte* newP = (byte*)(void*)newScan0; // output.

                for (int y = skip; y < height - skip; y++)
                {
                    for (int x = skip; x < width - skip; x++)
                    {   // get the neighboors:
                        for (int wx = x - skip; wx < x + skip + 1; wx++)
                            for (int wy = y - skip; wy < y + skip + 1; wy++)
                                window[index++] = p[wy * stride + wx];

                        // sort them:
                        Array.Sort(window);

                        // get the median element:
                        newP[y * newStride + x] = (byte)(window[position] + 1);

                        index = 0;
                    }
                }

                inputImage.UnlockBits(bmpData);
                outputImage.UnlockBits(newBmpData);

                return outputImage;
            }
            catch// Generic error was encountered.
            {
                return null;
            }
        }

        public static Bitmap BinaryHorizontalErosion(Bitmap inputImage, int size)
        {
            BitmapData bmpData = null, bmpDataOut = null;
            Bitmap outputImage = null;

            try
            {
                if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed)
                    return null;

                int width = inputImage.Width;
                int height = inputImage.Height;

                outputImage = (Bitmap)inputImage.Clone();

                bmpData = inputImage.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                System.IntPtr scan0 = bmpData.Scan0;
                int stride = bmpData.Stride;
                int offset = stride - width;

                bmpDataOut = outputImage.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                System.IntPtr scan0Out = bmpDataOut.Scan0;

                byte* pIn = (byte*)(void*)scan0;
                byte* pOut = (byte*)(void*)scan0Out;

                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        int x_M_size = x - size;
                        int last = 0;
                        if (x_M_size < 0)
                            last = x;
                        else
                            last = size;

                        for (int i = 0; i < last; i++)
                        {
                            if ((pIn - i)[0] == 255)
                            {
                                pOut[0] = 255;
                                break;
                            }
                        }
                        pIn++;
                        pOut++;
                    } // for x ...

                    pIn += offset;
                    pOut += offset;
                } // for y ...
                inputImage.UnlockBits(bmpData);
                outputImage.UnlockBits(bmpDataOut);
                return outputImage;
            }
            catch
            {
                return null;
            }
        }

        public static Bitmap BinaryHorizontalDilation(Bitmap inputImage, int size)
        {
            BitmapData bmpData = null, bmpDataOut = null;
            Bitmap outputImage = null;
            try
            {
                if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed)
                    return null;

                int width = inputImage.Width;
                int height = inputImage.Height;

                outputImage = (Bitmap)inputImage.Clone();

                bmpData = inputImage.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                System.IntPtr scan0 = bmpData.Scan0;
                int stride = bmpData.Stride;
                int offset = stride - width;

                bmpDataOut = outputImage.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                System.IntPtr scan0Out = bmpDataOut.Scan0;

                byte* pIn = (byte*)(void*)scan0;
                byte* pOut = (byte*)(void*)scan0Out;

                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        int x_P_size = x + size;
                        int last = 0;
                        if (x_P_size >= width)
                            last = width - x;
                        else
                            last = size;

                        for (int i = 0; i < last; i++)
                        {
                            if ((pIn + i)[0] == 0)
                            {
                                pOut[0] = 0;
                                break;
                            }
                        }
                        pIn++;
                        pOut++;
                    } // for x ...

                    pIn += offset;
                    pOut += offset;
                } // for y ...
                inputImage.UnlockBits(bmpData);
                outputImage.UnlockBits(bmpDataOut);
                return outputImage;
            }
            catch
            {
                return null;
            }
        }

        public static Bitmap BinaryVerticalErosion(Bitmap bmpImg, int size)
        {
            BitmapData bmpData = null, bmpDataOut = null;
            Bitmap outputImage = null;

            try
            {
                if (bmpImg.PixelFormat != PixelFormat.Format8bppIndexed)
                    return null;

                int width = bmpImg.Width;
                int height = bmpImg.Height;

                outputImage = (Bitmap)bmpImg.Clone();

                bmpData = bmpImg.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                System.IntPtr scan0 = bmpData.Scan0;
                int stride = bmpData.Stride;
                int offset = stride - width;

                bmpDataOut = outputImage.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                System.IntPtr scan0Out = bmpDataOut.Scan0;

                byte* pIn = (byte*)(void*)scan0;
                byte* pOut = (byte*)(void*)scan0Out;

                for (int y = 0; y < height; y++)
                {
                    int y_M_size = y - size;
                    int last = 0;
                    if (y_M_size < 0)
                        last = y;
                    else
                        last = size;

                    for (int x = 0; x < width; x++)
                    {
                        for (int j = 0; j < last; j++)
                        {
                            if ((pIn - (j * stride))[0] == 255)
                            {
                                pOut[0] = 255;
                                break;
                            }
                        }
                        pIn++;
                        pOut++;
                    } // for x ...

                    pIn += offset;
                    pOut += offset;
                } // for y ...
                bmpImg.UnlockBits(bmpData);
                outputImage.UnlockBits(bmpDataOut);
                return outputImage;
            }
            catch
            {
                return null;
            }
        }

        public static Bitmap BinaryVerticalDilation(Bitmap inputImage, int size)
        {
            BitmapData bmpData = null, bmpDataOut = null;
            Bitmap outputImage = null;
            try
            {
                if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed)
                    return null;

                int width = inputImage.Width;
                int height = inputImage.Height;

                outputImage = (Bitmap)inputImage.Clone();

                bmpData = inputImage.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                System.IntPtr scan0 = bmpData.Scan0;
                int stride = bmpData.Stride;
                int offset = stride - width;

                bmpDataOut = outputImage.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                System.IntPtr scan0Out = bmpDataOut.Scan0;

                byte* pIn = (byte*)(void*)scan0;
                byte* pOut = (byte*)(void*)scan0Out;

                for (int y = 0; y < height; y++)
                {
                    int y_P_size = y + size;
                    int last = 0;
                    if (y_P_size >= height)
                        last = height - y;
                    else
                        last = size;

                    for (int x = 0; x < width; x++)
                    {
                        for (int j = 0; j < last; j++)
                        {
                            if ((pIn + (j * stride))[0] == 0)
                            {
                                pOut[0] = 0;
                                break;
                            }
                        }
                        pIn++;
                        pOut++;
                    } // for x ...

                    pIn += offset;
                    pOut += offset;
                } // for y ...
                inputImage.UnlockBits(bmpData);
                outputImage.UnlockBits(bmpDataOut);
                return outputImage;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Applies 5x5 dilation on a byte-binary image. byte-binary image is an
        /// 8-bit image contains only two values 0 and 255.
        /// </summary>
        /// <param name="inputImage">
        /// An image for which 5x5 dilation will be applied.
        /// </param>
        /// <param name="outputImage">
        /// holds the output.
        /// </param>
        /// <returns>
        /// 0: Success.
        /// 1: Invalid pixel format.
        /// 2: Generic error.
        /// </returns>
        public static Bitmap BinaryFullFastDilation5X5(Bitmap inputImage)
        {
            //Defined global to unlock image data in "FINALLY" block:
            BitmapData bmData = null,
                outbmData = null;
            Bitmap outputImage = null;
            try
            {
                // Check validity of the given parameters:
                if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed)
                    return null; // Invalid pixel format.

                // get a copy of the origenal image:
                outputImage = (Bitmap)inputImage.Clone();

                int width = inputImage.Width,
                    height = inputImage.Height;

                bmData = inputImage.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                outbmData = outputImage.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);

                int stride = bmData.Stride;

                System.IntPtr scan0 = bmData.Scan0,
                    outScan0 = outbmData.Scan0;

                byte* pScan0 = (byte*)(void*)scan0,
                    pOutScan0 = (byte*)(void*)outScan0;

                byte* p1 = pScan0,
                    p = pOutScan0;

                int nOffset = stride - width + 4;
                int h_M_1 = height - 2,
                    w_M_1 = width - 2;

                int s2 = 2 * stride,
                    s_M_2 = -s2,
                    s_M_1 = -stride;

                // 1: the first raw:
                p += 2;
                p1 += 2;

                for (int x1 = 2; x1 < w_M_1; ++x1)
                {
                    if (
                        (p1 - 2)[0] == 0 ||
                        (p1 - 1)[0] == 0 ||
                        (p1 + 1)[0] == 0 ||
                        (p1 + 2)[0] == 0 ||

                        ((p1 - 2) + stride)[0] == 0 ||
                        ((p1 - 1) + stride)[0] == 0 ||
                        ((p1) + stride)[0] == 0 ||
                        ((p1 + 1) + stride)[0] == 0 ||
                        ((p1 + 2) + stride)[0] == 0 ||

                        ((p1 - 2) + s2)[0] == 0 ||
                        ((p1 - 1) + s2)[0] == 0 ||
                        ((p1) + s2)[0] == 0 ||
                        ((p1 + 1) + s2)[0] == 0 ||
                        ((p1 + 2) + s2)[0] == 0
                        )
                    {
                        p[0] = 0;
                    }

                    ++p;
                    ++p1;
                }

                // 2: the second raw:
                p1 = pScan0;
                p = pOutScan0;

                p += stride + 2;
                p1 += stride + 2;

                for (int x1 = 2; x1 < w_M_1; ++x1)
                {
                    if (
                        ((p1 - 2) + s_M_1)[0] == 0 ||
                        ((p1 - 1) + s_M_1)[0] == 0 ||
                        ((p1) + s_M_1)[0] == 0 ||
                        ((p1 + 1) + s_M_1)[0] == 0 ||
                        ((p1 + 2) + s_M_1)[0] == 0 ||

                        (p1 - 2)[0] == 0 ||
                        (p1 - 1)[0] == 0 ||
                        (p1 + 1)[0] == 0 ||
                        (p1 + 2)[0] == 0 ||

                        ((p1 - 2) + stride)[0] == 0 ||
                        ((p1 - 1) + stride)[0] == 0 ||
                        ((p1) + stride)[0] == 0 ||
                        ((p1 + 1) + stride)[0] == 0 ||
                        ((p1 + 2) + stride)[0] == 0 ||

                        ((p1 - 2) + s2)[0] == 0 ||
                        ((p1 - 1) + s2)[0] == 0 ||
                        ((p1) + s2)[0] == 0 ||
                        ((p1 + 1) + s2)[0] == 0 ||
                        ((p1 + 2) + s2)[0] == 0

                        )
                    {
                        p[0] = 0;
                    }
                    p++;
                    p1++;
                }

                // 3: the last raw:
                p1 = pScan0;
                p = pOutScan0;

                p += (height - 1) * stride + 2;
                p1 += (height - 1) * stride + 2;

                for (int x1 = 2; x1 < w_M_1; ++x1)
                {
                    if (
                        ((p1 - 2) + s_M_2)[0] == 0 ||
                        ((p1 - 1) + s_M_2)[0] == 0 ||
                        ((p1) + s_M_2)[0] == 0 ||
                        ((p1 + 1) + s_M_2)[0] == 0 ||
                        ((p1 + 2) + s_M_2)[0] == 0 ||

                        ((p1 - 2) + s_M_1)[0] == 0 ||
                        ((p1 - 1) + s_M_1)[0] == 0 ||
                        ((p1) + s_M_1)[0] == 0 ||
                        ((p1 + 1) + s_M_1)[0] == 0 ||
                        ((p1 + 2) + s_M_1)[0] == 0 ||

                        (p1 - 2)[0] == 0 ||
                        (p1 - 1)[0] == 0 ||
                        (p1 + 1)[0] == 0 ||
                        (p1 + 2)[0] == 0
                        )
                    {
                        p[0] = 0;
                    }

                    ++p;
                    ++p1;
                }

                //4: the raw before the last:
                p1 = pScan0;
                p = pOutScan0;

                p += (height - 2) * stride + 2;
                p1 += (height - 2) * stride + 2;

                for (int x1 = 2; x1 < w_M_1; ++x1)
                {
                    if (
                        ((p1 - 2) + s_M_2)[0] == 0 ||
                        ((p1 - 1) + s_M_2)[0] == 0 ||
                        ((p1) + s_M_2)[0] == 0 ||
                        ((p1 + 1) + s_M_2)[0] == 0 ||
                        ((p1 + 2) + s_M_2)[0] == 0 ||

                        ((p1 - 2) + s_M_1)[0] == 0 ||
                        ((p1 - 1) + s_M_1)[0] == 0 ||
                        ((p1) + s_M_1)[0] == 0 ||
                        ((p1 + 1) + s_M_1)[0] == 0 ||
                        ((p1 + 2) + s_M_1)[0] == 0 ||

                        (p1 - 2)[0] == 0 ||
                        (p1 - 1)[0] == 0 ||
                        (p1 + 1)[0] == 0 ||
                        (p1 + 2)[0] == 0 ||

                        ((p1 - 2) + stride)[0] == 0 ||
                        ((p1 - 1) + stride)[0] == 0 ||
                        ((p1) + stride)[0] == 0 ||
                        ((p1 + 1) + stride)[0] == 0 ||
                        ((p1 + 2) + stride)[0] == 0
                        )
                    {
                        p[0] = 0;
                    }

                    ++p;
                    ++p1;
                }

                //5: the first column:
                p1 = pScan0;
                p = pOutScan0;

                p += s2;
                p1 += s2;

                for (int y = 2; y < h_M_1; ++y)
                {
                    if (
                        ((p1) + s_M_2)[0] == 0 ||
                        ((p1 + 1) + s_M_2)[0] == 0 ||
                        ((p1 + 2) + s_M_2)[0] == 0 ||

                        ((p1) + s_M_1)[0] == 0 ||
                        ((p1 + 1) + s_M_1)[0] == 0 ||
                        ((p1 + 2) + s_M_1)[0] == 0 ||

                        (p1 + 1)[0] == 0 ||
                        (p1 + 2)[0] == 0 ||

                        ((p1) + stride)[0] == 0 ||
                        ((p1 + 1) + stride)[0] == 0 ||
                        ((p1 + 2) + stride)[0] == 0 ||

                        ((p1) + s2)[0] == 0 ||
                        ((p1 + 1) + s2)[0] == 0 ||
                        ((p1 + 2) + s2)[0] == 0
                        )
                    {
                        p[0] = 0;
                    }

                    p += stride;
                    p1 += stride;
                }

                //6: the second column:
                p1 = pScan0;
                p = pOutScan0;

                p += s2 + 1;
                p1 += s2 + 1;

                for (int y = 2; y < h_M_1; ++y)
                {
                    if (
                        ((p1 - 1) + s_M_2)[0] == 0 ||
                        ((p1) + s_M_2)[0] == 0 ||
                        ((p1 + 1) + s_M_2)[0] == 0 ||
                        ((p1 + 2) + s_M_2)[0] == 0 ||

                        ((p1 - 1) + s_M_1)[0] == 0 ||
                        ((p1) + s_M_1)[0] == 0 ||
                        ((p1 + 1) + s_M_1)[0] == 0 ||
                        ((p1 + 2) + s_M_1)[0] == 0 ||

                        (p1 - 1)[0] == 0 ||
                        (p1 + 1)[0] == 0 ||
                        (p1 + 2)[0] == 0 ||

                        ((p1 - 1) + stride)[0] == 0 ||
                        ((p1) + stride)[0] == 0 ||
                        ((p1 + 1) + stride)[0] == 0 ||
                        ((p1 + 2) + stride)[0] == 0 ||

                        ((p1 - 1) + s2)[0] == 0 ||
                        ((p1) + s2)[0] == 0 ||
                        ((p1 + 1) + s2)[0] == 0 ||
                        ((p1 + 2) + s2)[0] == 0
                        )
                    {
                        p[0] = 0;
                    }

                    p += stride;
                    p1 += stride;
                }

                //7: the last column:
                p1 = pScan0;
                p = pOutScan0;

                p += s2 + (width - 1);
                p1 += s2 + (width - 1);

                for (int y = 2; y < h_M_1; ++y)
                {
                    if (
                        ((p1 - 2) + s_M_2)[0] == 0 ||
                        ((p1 - 1) + s_M_2)[0] == 0 ||
                        ((p1) + s_M_2)[0] == 0 ||

                        ((p1 - 2) + s_M_1)[0] == 0 ||
                        ((p1 - 1) + s_M_1)[0] == 0 ||
                        ((p1) + s_M_1)[0] == 0 ||

                        (p1 - 2)[0] == 0 ||
                        (p1 - 1)[0] == 0 ||

                        ((p1 - 2) + stride)[0] == 0 ||
                        ((p1 - 1) + stride)[0] == 0 ||
                        ((p1) + stride)[0] == 0 ||

                        ((p1 - 2) + s2)[0] == 0 ||
                        ((p1 - 1) + s2)[0] == 0 ||
                        ((p1) + s2)[0] == 0
                        )
                    {
                        p[0] = 0;
                    }

                    p += stride;
                    p1 += stride;
                }

                //8: the raw before the last:
                p1 = pScan0;
                p = pOutScan0;

                p += s2 + (width - 2);
                p1 += s2 + (width - 2);

                for (int y = 2; y < h_M_1; ++y)
                {
                    if (
                        ((p1 - 2) + s_M_2)[0] == 0 ||
                        ((p1 - 1) + s_M_2)[0] == 0 ||
                        ((p1) + s_M_2)[0] == 0 ||
                        ((p1 + 1) + s_M_2)[0] == 0 ||

                        ((p1 - 2) + s_M_1)[0] == 0 ||
                        ((p1 - 1) + s_M_1)[0] == 0 ||
                        ((p1) + s_M_1)[0] == 0 ||
                        ((p1 + 1) + s_M_1)[0] == 0 ||

                        (p1 - 2)[0] == 0 ||
                        (p1 - 1)[0] == 0 ||
                        (p1 + 1)[0] == 0 ||

                        ((p1 - 2) + stride)[0] == 0 ||
                        ((p1 - 1) + stride)[0] == 0 ||
                        ((p1) + stride)[0] == 0 ||
                        ((p1 + 1) + stride)[0] == 0 ||

                        ((p1 - 2) + s2)[0] == 0 ||
                        ((p1 - 1) + s2)[0] == 0 ||
                        ((p1) + s2)[0] == 0 ||
                        ((p1 + 1) + s2)[0] == 0
                        )
                    {
                        p[0] = 0;
                    }

                    p += stride;
                    p1 += stride;
                }

                // 9: others:

                p1 = pScan0;
                p = pOutScan0;

                p += s2 + 2;
                p1 += s2 + 2;

                for (int y = 2; y < h_M_1; ++y)
                {
                    for (int x1 = 2; x1 < w_M_1; ++x1)
                    {
                        if (
                            ((p1 - 2) + s_M_2)[0] == 0 ||
                            ((p1 - 1) + s_M_2)[0] == 0 ||
                            ((p1) + s_M_2)[0] == 0 ||
                            ((p1 + 1) + s_M_2)[0] == 0 ||
                            ((p1 + 2) + s_M_2)[0] == 0 ||

                            ((p1 - 2) + s_M_1)[0] == 0 ||
                            ((p1 - 1) + s_M_1)[0] == 0 ||
                            ((p1) + s_M_1)[0] == 0 ||
                            ((p1 + 1) + s_M_1)[0] == 0 ||
                            ((p1 + 2) + s_M_1)[0] == 0 ||

                            (p1 - 2)[0] == 0 ||
                            (p1 - 1)[0] == 0 ||
                            (p1 + 1)[0] == 0 ||
                            (p1 + 2)[0] == 0 ||

                            ((p1 - 2) + stride)[0] == 0 ||
                            ((p1 - 1) + stride)[0] == 0 ||
                            ((p1) + stride)[0] == 0 ||
                            ((p1 + 1) + stride)[0] == 0 ||
                            ((p1 + 2) + stride)[0] == 0 ||

                            ((p1 - 2) + s2)[0] == 0 ||
                            ((p1 - 1) + s2)[0] == 0 ||
                            ((p1) + s2)[0] == 0 ||
                            ((p1 + 1) + s2)[0] == 0 ||
                            ((p1 + 2) + s2)[0] == 0
                            )
                        {
                            p[0] = 0;
                        }

                        ++p;
                        ++p1;
                    }
                    p += nOffset;
                    p1 += nOffset;
                }
                inputImage.UnlockBits(bmData);
                outputImage.UnlockBits(outbmData);
                return outputImage;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Applies 5x5 erosion on a byte-binary image. byte-binary image is an
        /// 8-bit image contains only two values 0 and 255.
        /// </summary>
        /// <param name="inputImage">
        /// An image for which 5x5 erosion will be applied.
        /// </param>
        /// <param name="outputImage">
        /// holds the output.
        /// </param>
        /// <returns>
        /// 0: Success.
        /// 1: Invalid pixel format.
        /// 2: Generic error.
        /// </returns>
        public static Bitmap BinaryFullFastErosion5X5(Bitmap inputImage)
        {
            //Defined global to unlock image data in "FINALLY" block:
            BitmapData bmData = null,
                outbmData = null;
            Bitmap outputImage = null;
            try
            {
                // Check validity of the given parameters:
                if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed)
                    return null; // Invalid pixel format.

                // get a copy of the origenal image:
                outputImage = (Bitmap)inputImage.Clone();

                int width = inputImage.Width,
                    height = inputImage.Height;

                bmData = inputImage.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                outbmData = outputImage.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);

                int stride = bmData.Stride;

                System.IntPtr scan0 = bmData.Scan0,
                    outScan0 = outbmData.Scan0;

                byte* pScan0 = (byte*)(void*)scan0,
                    pOutScan0 = (byte*)(void*)outScan0;

                byte* p1 = pScan0,
                    p = pOutScan0;

                int nOffset = stride - width + 4;
                int h_M_1 = height - 2,
                    w_M_1 = width - 2;

                int s2 = 2 * stride,
                    s_M_2 = -s2,
                    s_M_1 = -stride;

                // 1: the first raw:
                p += 2;
                p1 += 2;

                for (int x1 = 2; x1 < w_M_1; ++x1)
                {
                    if (
                        (p1 - 2)[0] == 255 ||
                        (p1 - 1)[0] == 255 ||
                        (p1 + 1)[0] == 255 ||
                        (p1 + 2)[0] == 255 ||

                        ((p1 - 2) + stride)[0] == 255 ||
                        ((p1 - 1) + stride)[0] == 255 ||
                        ((p1) + stride)[0] == 255 ||
                        ((p1 + 1) + stride)[0] == 255 ||
                        ((p1 + 2) + stride)[0] == 255 ||

                        ((p1 - 2) + s2)[0] == 255 ||
                        ((p1 - 1) + s2)[0] == 255 ||
                        ((p1) + s2)[0] == 255 ||
                        ((p1 + 1) + s2)[0] == 255 ||
                        ((p1 + 2) + s2)[0] == 255
                        )
                    {
                        p[0] = 255;
                    }

                    ++p;
                    ++p1;
                }

                // 2: the second raw:
                p1 = pScan0;
                p = pOutScan0;

                p += stride + 2;
                p1 += stride + 2;

                for (int x1 = 2; x1 < w_M_1; ++x1)
                {
                    if (
                        ((p1 - 2) + s_M_1)[0] == 255 ||
                        ((p1 - 1) + s_M_1)[0] == 255 ||
                        ((p1) + s_M_1)[0] == 255 ||
                        ((p1 + 1) + s_M_1)[0] == 255 ||
                        ((p1 + 2) + s_M_1)[0] == 255 ||

                        (p1 - 2)[0] == 255 ||
                        (p1 - 1)[0] == 255 ||
                        (p1 + 1)[0] == 255 ||
                        (p1 + 2)[0] == 255 ||

                        ((p1 - 2) + stride)[0] == 255 ||
                        ((p1 - 1) + stride)[0] == 255 ||
                        ((p1) + stride)[0] == 255 ||
                        ((p1 + 1) + stride)[0] == 255 ||
                        ((p1 + 2) + stride)[0] == 255 ||

                        ((p1 - 2) + s2)[0] == 255 ||
                        ((p1 - 1) + s2)[0] == 255 ||
                        ((p1) + s2)[0] == 255 ||
                        ((p1 + 1) + s2)[0] == 255 ||
                        ((p1 + 2) + s2)[0] == 255

                        )
                    {
                        p[0] = 255;
                    }
                    p++;
                    p1++;
                }

                // 3: the last raw:
                p1 = pScan0;
                p = pOutScan0;

                p += (height - 1) * stride + 2;
                p1 += (height - 1) * stride + 2;

                for (int x1 = 2; x1 < w_M_1; ++x1)
                {
                    if (
                        ((p1 - 2) + s_M_2)[0] == 255 ||
                        ((p1 - 1) + s_M_2)[0] == 255 ||
                        ((p1) + s_M_2)[0] == 255 ||
                        ((p1 + 1) + s_M_2)[0] == 255 ||
                        ((p1 + 2) + s_M_2)[0] == 255 ||

                        ((p1 - 2) + s_M_1)[0] == 255 ||
                        ((p1 - 1) + s_M_1)[0] == 255 ||
                        ((p1) + s_M_1)[0] == 255 ||
                        ((p1 + 1) + s_M_1)[0] == 255 ||
                        ((p1 + 2) + s_M_1)[0] == 255 ||

                        (p1 - 2)[0] == 255 ||
                        (p1 - 1)[0] == 255 ||
                        (p1 + 1)[0] == 255 ||
                        (p1 + 2)[0] == 255
                        )
                    {
                        p[0] = 255;
                    }

                    ++p;
                    ++p1;
                }

                //4: the raw before the last:
                p1 = pScan0;
                p = pOutScan0;

                p += (height - 2) * stride + 2;
                p1 += (height - 2) * stride + 2;

                for (int x1 = 2; x1 < w_M_1; ++x1)
                {
                    if (
                        ((p1 - 2) + s_M_2)[0] == 255 ||
                        ((p1 - 1) + s_M_2)[0] == 255 ||
                        ((p1) + s_M_2)[0] == 255 ||
                        ((p1 + 1) + s_M_2)[0] == 255 ||
                        ((p1 + 2) + s_M_2)[0] == 255 ||

                        ((p1 - 2) + s_M_1)[0] == 255 ||
                        ((p1 - 1) + s_M_1)[0] == 255 ||
                        ((p1) + s_M_1)[0] == 255 ||
                        ((p1 + 1) + s_M_1)[0] == 255 ||
                        ((p1 + 2) + s_M_1)[0] == 255 ||

                        (p1 - 2)[0] == 255 ||
                        (p1 - 1)[0] == 255 ||
                        (p1 + 1)[0] == 255 ||
                        (p1 + 2)[0] == 255 ||

                        ((p1 - 2) + stride)[0] == 255 ||
                        ((p1 - 1) + stride)[0] == 255 ||
                        ((p1) + stride)[0] == 255 ||
                        ((p1 + 1) + stride)[0] == 255 ||
                        ((p1 + 2) + stride)[0] == 255
                        )
                    {
                        p[0] = 255;
                    }

                    ++p;
                    ++p1;
                }

                //5: the first column:
                p1 = pScan0;
                p = pOutScan0;

                p += s2;
                p1 += s2;

                for (int y = 2; y < h_M_1; ++y)
                {
                    if (
                        ((p1) + s_M_2)[0] == 255 ||
                        ((p1 + 1) + s_M_2)[0] == 255 ||
                        ((p1 + 2) + s_M_2)[0] == 255 ||

                        ((p1) + s_M_1)[0] == 255 ||
                        ((p1 + 1) + s_M_1)[0] == 255 ||
                        ((p1 + 2) + s_M_1)[0] == 255 ||

                        (p1 + 1)[0] == 255 ||
                        (p1 + 2)[0] == 255 ||

                        ((p1) + stride)[0] == 255 ||
                        ((p1 + 1) + stride)[0] == 255 ||
                        ((p1 + 2) + stride)[0] == 255 ||

                        ((p1) + s2)[0] == 255 ||
                        ((p1 + 1) + s2)[0] == 255 ||
                        ((p1 + 2) + s2)[0] == 255
                        )
                    {
                        p[0] = 255;
                    }

                    p += stride;
                    p1 += stride;
                }

                //6: the second column:
                p1 = pScan0;
                p = pOutScan0;

                p += s2 + 1;
                p1 += s2 + 1;

                for (int y = 2; y < h_M_1; ++y)
                {
                    if (
                        ((p1 - 1) + s_M_2)[0] == 255 ||
                        ((p1) + s_M_2)[0] == 255 ||
                        ((p1 + 1) + s_M_2)[0] == 255 ||
                        ((p1 + 2) + s_M_2)[0] == 255 ||

                        ((p1 - 1) + s_M_1)[0] == 255 ||
                        ((p1) + s_M_1)[0] == 255 ||
                        ((p1 + 1) + s_M_1)[0] == 255 ||
                        ((p1 + 2) + s_M_1)[0] == 255 ||

                        (p1 - 1)[0] == 255 ||
                        (p1 + 1)[0] == 255 ||
                        (p1 + 2)[0] == 255 ||

                        ((p1 - 1) + stride)[0] == 255 ||
                        ((p1) + stride)[0] == 255 ||
                        ((p1 + 1) + stride)[0] == 255 ||
                        ((p1 + 2) + stride)[0] == 255 ||

                        ((p1 - 1) + s2)[0] == 255 ||
                        ((p1) + s2)[0] == 255 ||
                        ((p1 + 1) + s2)[0] == 255 ||
                        ((p1 + 2) + s2)[0] == 255
                        )
                    {
                        p[0] = 255;
                    }

                    p += stride;
                    p1 += stride;
                }

                //7: the last column:
                p1 = pScan0;
                p = pOutScan0;

                p += s2 + (width - 1);
                p1 += s2 + (width - 1);

                for (int y = 2; y < h_M_1; ++y)
                {
                    if (
                        ((p1 - 2) + s_M_2)[0] == 255 ||
                        ((p1 - 1) + s_M_2)[0] == 255 ||
                        ((p1) + s_M_2)[0] == 255 ||

                        ((p1 - 2) + s_M_1)[0] == 255 ||
                        ((p1 - 1) + s_M_1)[0] == 255 ||
                        ((p1) + s_M_1)[0] == 255 ||

                        (p1 - 2)[0] == 255 ||
                        (p1 - 1)[0] == 255 ||

                        ((p1 - 2) + stride)[0] == 255 ||
                        ((p1 - 1) + stride)[0] == 255 ||
                        ((p1) + stride)[0] == 255 ||

                        ((p1 - 2) + s2)[0] == 255 ||
                        ((p1 - 1) + s2)[0] == 255 ||
                        ((p1) + s2)[0] == 255
                        )
                    {
                        p[0] = 255;
                    }

                    p += stride;
                    p1 += stride;
                }

                //8: the raw before the last:
                p1 = pScan0;
                p = pOutScan0;

                p += s2 + (width - 2);
                p1 += s2 + (width - 2);

                for (int y = 2; y < h_M_1; ++y)
                {
                    if (
                        ((p1 - 2) + s_M_2)[0] == 255 ||
                        ((p1 - 1) + s_M_2)[0] == 255 ||
                        ((p1) + s_M_2)[0] == 255 ||
                        ((p1 + 1) + s_M_2)[0] == 255 ||

                        ((p1 - 2) + s_M_1)[0] == 255 ||
                        ((p1 - 1) + s_M_1)[0] == 255 ||
                        ((p1) + s_M_1)[0] == 255 ||
                        ((p1 + 1) + s_M_1)[0] == 255 ||

                        (p1 - 2)[0] == 255 ||
                        (p1 - 1)[0] == 255 ||
                        (p1 + 1)[0] == 255 ||

                        ((p1 - 2) + stride)[0] == 255 ||
                        ((p1 - 1) + stride)[0] == 255 ||
                        ((p1) + stride)[0] == 255 ||
                        ((p1 + 1) + stride)[0] == 255 ||

                        ((p1 - 2) + s2)[0] == 255 ||
                        ((p1 - 1) + s2)[0] == 255 ||
                        ((p1) + s2)[0] == 255 ||
                        ((p1 + 1) + s2)[0] == 255
                        )
                    {
                        p[0] = 255;
                    }

                    p += stride;
                    p1 += stride;
                }

                // 9: others:

                p1 = pScan0;
                p = pOutScan0;

                p += s2 + 2;
                p1 += s2 + 2;

                for (int y = 2; y < h_M_1; ++y)
                {
                    for (int x1 = 2; x1 < w_M_1; ++x1)
                    {
                        if (
                            ((p1 - 2) + s_M_2)[0] == 255 ||
                            ((p1 - 1) + s_M_2)[0] == 255 ||
                            ((p1) + s_M_2)[0] == 255 ||
                            ((p1 + 1) + s_M_2)[0] == 255 ||
                            ((p1 + 2) + s_M_2)[0] == 255 ||

                            ((p1 - 2) + s_M_1)[0] == 255 ||
                            ((p1 - 1) + s_M_1)[0] == 255 ||
                            ((p1) + s_M_1)[0] == 255 ||
                            ((p1 + 1) + s_M_1)[0] == 255 ||
                            ((p1 + 2) + s_M_1)[0] == 255 ||

                            (p1 - 2)[0] == 255 ||
                            (p1 - 1)[0] == 255 ||
                            (p1 + 1)[0] == 255 ||
                            (p1 + 2)[0] == 255 ||

                            ((p1 - 2) + stride)[0] == 255 ||
                            ((p1 - 1) + stride)[0] == 255 ||
                            ((p1) + stride)[0] == 255 ||
                            ((p1 + 1) + stride)[0] == 255 ||
                            ((p1 + 2) + stride)[0] == 255 ||

                            ((p1 - 2) + s2)[0] == 255 ||
                            ((p1 - 1) + s2)[0] == 255 ||
                            ((p1) + s2)[0] == 255 ||
                            ((p1 + 1) + s2)[0] == 255 ||
                            ((p1 + 2) + s2)[0] == 255
                            )
                        {
                            p[0] = 255;
                        }

                        ++p;
                        ++p1;
                    }
                    p += nOffset;
                    p1 += nOffset;
                }

                inputImage.UnlockBits(bmData);
                outputImage.UnlockBits(outbmData);
                return outputImage;
            }
            catch (Exception) // Generic error
            {
                return null;
            }
        }

        public static Bitmap RectangularDilation(
            Bitmap bitmap,
            (int, int) kernelShape,
            byte backgroundColor = 0,
            byte foregroundColor = 255)
        {
            BitmapData bitmapData = null;
            BitmapData dilatedData = null;
            Bitmap dilated = bitmap.Clone() as Bitmap;

            try
            {
                if (bitmap.PixelFormat != PixelFormat.Format8bppIndexed) return null;

                var height = bitmap.Height;
                var width = bitmap.Width;

                if (kernelShape.Item1 % 2 == 0
                    || kernelShape.Item2 % 2 == 0
                    || kernelShape.Item1 < 0
                    || kernelShape.Item2 < 0)
                {
                    return null;
                }

                var originY = kernelShape.Item1 / 2;
                var originX = kernelShape.Item2 / 2;

                bitmapData = bitmap.LockBits(
                    new Rectangle(0, 0, width, height),
                    ImageLockMode.ReadWrite,
                    bitmap.PixelFormat);

                dilatedData = dilated.LockBits(
                    new Rectangle(0, 0, width, height),
                    ImageLockMode.ReadWrite,
                    bitmap.PixelFormat);

                var stride = bitmapData.Stride;

                var bitmapPtr = (byte*)bitmapData.Scan0.ToPointer();
                var dilatedPtr = (byte*)dilatedData.Scan0.ToPointer();
                var offset = stride - width;

                for (var y = 0; y < height; y++)
                {
                    for (var x = 0; x < width; x++)
                    {
                        var imageX1 = Math.Max(x - originX, 0);
                        var imageY1 = Math.Max(y - originY, 0);
                        var imageX2 = Math.Min(x + originX + 1, width);
                        var imageY2 = Math.Min(y + originY + 1, height);

                        var dilationRegionPtr = bitmapPtr - (y - imageY1) * stride - (x - imageX1);
                        var dilationWidth = imageX2 - imageX1;

                        var pixel = backgroundColor;
                        for (var yk = imageY1; yk < imageY2; yk++)
                        {
                            if (pixel == foregroundColor)
                            {
                                break;
                            }

                            for (var xk = imageX1; xk < imageX2; xk++)
                            {
                                if (dilationRegionPtr[0] == foregroundColor)
                                {
                                    pixel = foregroundColor;
                                    break;
                                }

                                dilationRegionPtr++;
                            }

                            dilationRegionPtr = dilationRegionPtr - dilationWidth + stride;
                        }

                        dilatedPtr[0] = pixel;

                        bitmapPtr++;
                        dilatedPtr++;
                    }

                    bitmapPtr += offset;
                    dilatedPtr += offset;
                }

                return dilated;
            }
            catch
            {
                return null;
            }
            finally
            {
                if (bitmapData != null)
                {
                    bitmap.UnlockBits(bitmapData);
                }

                if (dilatedData != null)
                {
                    dilated.UnlockBits(dilatedData);
                }
            }
        }
    }
}