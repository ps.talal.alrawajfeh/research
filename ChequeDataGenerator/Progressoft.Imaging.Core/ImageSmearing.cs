﻿using System;
using System.Drawing;
using System.Drawing.Imaging;

namespace Progressoft.Imaging.Core
{
    public static unsafe class ImageSmearing
    {
        /// <summary>
        /// Applies Smearing To Binarized 8 Bit Bitmaps, Given The Distance To Look Up.
        /// </summary>
        /// <param name="inputImage">The Bitmap To Be Smeared.</param>
        /// <param name="outputImage">Reference To The Resulted Bitmap</param>
        /// <param name="nThreshold">Distance To Look Up For Components. Preferred 30 For 200 dpi</param>
        /// <returns>Error Code:
        /// 0: Success.
        /// 1: Invalid Pixel Format.
        /// 2: Invalid Threshold Value.
        /// 3: Generic Error.</returns>
        public static Bitmap Smearing(Bitmap inputImage, int nThreshold)
        {
            BitmapData bmData = null;
            Bitmap outputImage = null;
            try
            {
                if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed)
                    return null;
                if (nThreshold < 1 && nThreshold > inputImage.Width - 1)
                    return null;

                outputImage = (Bitmap)inputImage.Clone();
                int w = outputImage.Width;
                int h = outputImage.Height;

                bmData = outputImage.LockBits(new Rectangle(0, 0, outputImage.Width, outputImage.Height),
                    ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);

                int stride = bmData.Stride;

                System.IntPtr Scan0 = bmData.Scan0;
                byte* p = (byte*)(void*)Scan0;
                int WhiteCtr = 0;

                for (int y = 0; y < h; y++)
                {
                    for (int x = 0; x < w - 1; x++)
                    {
                        if (p[(stride * y) + x] != 255 && p[(stride * y) + (x + 1)] == 255)
                        {
                            WhiteCtr = 0;
                            //Black Start Point Found
                            for (int x1 = x + 1; x1 < w; x1++)
                            {
                                if (p[(stride * y) + x1] == 255)
                                    WhiteCtr++;
                                else
                                {
                                    //check if the end found
                                    if (x1 == w - 1)
                                    {
                                        x = x1;
                                        break;
                                    }
                                    if (WhiteCtr > nThreshold)
                                    {
                                        x = x1;
                                        break;
                                    }
                                    for (int x2 = x + 1; x2 < x1; x2++)
                                        p[(stride * y) + x2] = 0;
                                    WhiteCtr = 0;
                                    x = x1;
                                } //else
                            } //for x1
                        } //if
                    } //for x
                } //for y
                outputImage.UnlockBits(bmData);
                return outputImage;
            } //try
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Applies Directional (Both Horizontal and Vertical) Smearing To Binarized 8 Bit Bitmaps,
        /// Given The Distance To Look Up.
        /// </summary>
        /// <param name="inputImage">The Bitmap To Be Smeared.</param>
        /// <param name="outputImage">Reference To The Resulted Bitmap</param>
        /// <param name="nThreshold">Distance To Look Up For Components. Preferred 30 For 200 dpi</param>
        /// <returns>Error Code:
        /// 0: Success.
        /// 1: Invalid Pixel Format.
        /// 2: Invalid Threshold Value.
        /// 3: Generic Error.</returns>
        public static Bitmap DirectionalSmearing(Bitmap inputImage, int nHorThreshold, int nVerThreshold)
        {
            BitmapData bmData = null;
            Bitmap outputImage = null;
            try
            {
                if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed) return null;
                if (nHorThreshold < 1 && nHorThreshold > inputImage.Width - 1) return null;
                if (nVerThreshold < 1 && nVerThreshold > inputImage.Height - 1) return null;

                outputImage = (Bitmap)inputImage.Clone();
                int w = outputImage.Width;
                int h = outputImage.Height;

                bmData = outputImage.LockBits(new Rectangle(0, 0, outputImage.Width, outputImage.Height),
                    ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);

                int stride = bmData.Stride;

                System.IntPtr Scan0 = bmData.Scan0;
                byte* p = (byte*)(void*)Scan0;

                int WhiteCtr = 0;

                #region Horizontal

                for (int y = 0; y < h; y++)
                {
                    for (int x = 0; x < w - 1; x++)
                    {
                        if (p[(stride * y) + x] != 255 && p[(stride * y) + (x + 1)] == 255)
                        {
                            WhiteCtr = 0;
                            //Black Start Point Found
                            for (int x1 = x + 1; x1 < w; x1++)
                            {
                                if (p[(stride * y) + x1] == 255) WhiteCtr++;
                                else
                                {
                                    if (x1 == w - 1)
                                    {
                                        x = x1;
                                        break;
                                    }
                                    if (WhiteCtr > nHorThreshold)
                                    {
                                        x = x1;
                                        break;
                                    }
                                    for (int x2 = x + 1; x2 < x1; x2++)
                                        p[(stride * y) + x2] = 0;
                                    WhiteCtr = 0;
                                    x = x1;
                                } //else
                            } //for x1
                        } //if
                    } //for x
                } //for y

                #endregion Horizontal

                #region Vertical Region

                p = (byte*)(void*)Scan0;
                for (int x = 0; x < w; x++)
                {
                    for (int y = 0; y < h - 1; y++)
                    {
                        if (p[(stride * y) + x] != 255 && p[(stride * (y + 1)) + (x)] == 255)
                        {
                            WhiteCtr = 0;
                            //Black Start Point Found
                            for (int x1 = y + 1; x1 < h; x1++)
                            {
                                if (p[(stride * x1) + x] == 255) WhiteCtr++;
                                else
                                {
                                    //check if the end found
                                    if (x1 == h - 1)
                                    {
                                        y = x1;
                                        break;
                                    }
                                    if (WhiteCtr > nVerThreshold)
                                    {
                                        y = x1;
                                        break;
                                    }
                                    for (int x2 = y + 1; x2 < x1; x2++)
                                        p[(stride * x2) + x] = 0;
                                    WhiteCtr = 0;
                                    y = x1;
                                } //else
                            } //for x1
                        } //if
                    } //for x
                } //for y

                #endregion Vertical Region

                outputImage.UnlockBits(bmData);
                return outputImage;
            } //try
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Applies smearing in all directions ... vertical, horizontal,
        /// left-diagonal, and right-diagonal.
        /// </summary>
        /// <param name="inputImage"></param>
        /// <param name="nHorThreshold"></param>
        /// <param name="nVerThreshold"></param>
        /// <param name="nRightDiagThreshold"></param>
        /// <param name="nLeftDiagThreshold"></param>
        /// <param name="bEnableHorizontal"></param>
        /// <param name="bEnableVertical"></param>
        /// <param name="bEnableLeftDigonal"></param>
        /// <param name="bEnableRightDigonal"></param>
        /// <param name="outputImage"></param>
        /// <returns></returns>
        public static Bitmap FullDirectionalSmearing(
            Bitmap inputImage,
            int nHorThreshold,
            int nVerThreshold,
            int nRightDiagThreshold,
            int nLeftDiagThreshold,
            bool bEnableHorizontal,
            bool bEnableVertical,
            bool bEnableLeftDigonal,
            bool bEnableRightDigonal)
        {
            BitmapData bmData = null;
            BitmapData bmDataB2 = null;
            Bitmap outputImage = null;
            try
            {
                if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed) return null;
                if (nHorThreshold < 1 || nHorThreshold > inputImage.Width - 1) return null;
                if (nVerThreshold < 1 || nVerThreshold > inputImage.Height - 1) return null;
                if (nRightDiagThreshold < 1 || nRightDiagThreshold > (int)Math.Sqrt(Math.Pow(inputImage.Width, 2) + Math.Pow(inputImage.Height, 2))) return null;
                if (nLeftDiagThreshold < 1 || nLeftDiagThreshold > (int)Math.Sqrt(Math.Pow(inputImage.Width, 2) + Math.Pow(inputImage.Height, 2))) return null;

                outputImage = (Bitmap)inputImage.Clone();
                int w = outputImage.Width;
                int h = outputImage.Height;

                bmData = outputImage.LockBits(new Rectangle(0, 0, w, h),
                    ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);

                bmDataB2 = inputImage.LockBits(new Rectangle(0, 0, w, h),
                    ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);

                int stride = bmData.Stride;

                System.IntPtr Scan0 = bmData.Scan0;
                System.IntPtr Scan0B2 = bmDataB2.Scan0;

                byte* p = (byte*)(void*)Scan0;
                byte* pB2 = (byte*)(void*)Scan0B2;

                int WhiteCtr = 0;

                if (bEnableHorizontal)
                {
                    #region Horizontal

                    for (int y = 0; y < h; y++)
                    {
                        for (int x = 0; x < w - 1; x++)
                        {
                            if (pB2[(stride * y) + x] != 255 && pB2[(stride * y) + (x + 1)] == 255)
                            {
                                WhiteCtr = 0;
                                //Black Start Point Found
                                for (int x1 = x + 1; x1 < w; x1++)
                                {
                                    if (pB2[(stride * y) + x1] == 255) WhiteCtr++;
                                    else
                                    {
                                        //check if the end found
                                        if (x1 == w - 1)
                                        {
                                            x = x1;
                                            break;
                                        }
                                        if (WhiteCtr > nHorThreshold)
                                        {
                                            x = x1;
                                            break;
                                        }

                                        for (int x2 = x + 1; x2 < x1; x2++)
                                            p[(stride * y) + x2] = 0;
                                        WhiteCtr = 0;
                                        x = x1;
                                    } //else
                                } //for x1
                            } //if
                        } //for x
                    } //for y

                    #endregion Horizontal
                }

                if (bEnableVertical)
                {
                    #region Vertical Region

                    p = (byte*)(void*)Scan0;
                    for (int x = 0; x < w; x++)
                    {
                        for (int y = 0; y < h - 1; y++)
                        {
                            if (pB2[(stride * y) + x] != 255 && pB2[(stride * (y + 1)) + (x)] == 255)
                            {
                                WhiteCtr = 0;
                                //Black Start Point Found
                                for (int x1 = y + 1; x1 < h; x1++)
                                {
                                    if (pB2[(stride * x1) + x] == 255) WhiteCtr++;
                                    else
                                    {
                                        //check if the end found
                                        if (x1 == h - 1)
                                        {
                                            y = x1;
                                            break;
                                        }
                                        if (WhiteCtr > nVerThreshold)
                                        {
                                            y = x1;
                                            break;
                                        }
                                        for (int x2 = y + 1; x2 < x1; x2++)
                                            p[(stride * x2) + x] = 0;
                                        WhiteCtr = 0;
                                        y = x1;
                                    } //else
                                } //for x1
                            } //if
                        } //for x
                    } //for y

                    #endregion Vertical Region
                }

                if (bEnableRightDigonal)
                {
                    #region Diagonal toward right

                    for (int y = 0; y < h - 1; y++)
                    {
                        for (int x = 0; x < w - 1; x++)
                        {
                            if (pB2[(stride * y) + x] != 255 && pB2[(stride * (y + 1)) + (x + 1)] == 255)
                            {
                                WhiteCtr = 0;
                                int y1 = y + 1;
                                int x1 = x + 1;
                                //Black Start Point Found
                                while (x1 < w && y1 < h)
                                {
                                    if (pB2[(stride * y1) + x1] == 255)
                                        WhiteCtr++;
                                    else
                                    {
                                        //check if the end found
                                        if (x1 == w - 1 || y1 == h - 1)
                                            break;
                                        if (WhiteCtr > nRightDiagThreshold)
                                            break;

                                        int y2 = y + 1;
                                        int x2 = x + 1;
                                        while (y2 < y1 && x2 < x1)
                                        {
                                            p[(stride * y2) + x2] = 0;

                                            y2++;
                                            x2++;
                                        }

                                        WhiteCtr = 0;
                                    } //else

                                    y1++;
                                    x1++;
                                }
                            } //if
                        } //for x
                    } //for y

                    #endregion Diagonal toward right
                }

                if (bEnableLeftDigonal)
                {
                    #region Diagonal toward left

                    for (int y = 0; y < h - 1; y++)
                    {
                        for (int x = 0; x < w - 1; x++)
                        {
                            if (pB2[(stride * y) + x] != 255 && pB2[(stride * (y + 1)) + (x + 1)] == 255)
                            {
                                WhiteCtr = 0;
                                int y1 = y + 1;
                                int x1 = x - 1;
                                //Black Start Point Found
                                while (x1 > 0 && y1 < h)
                                {
                                    if (pB2[(stride * y1) + x1] == 255)
                                        WhiteCtr++;
                                    else
                                    {
                                        //check if the end found
                                        if (x1 == 0 || y1 == h - 1)
                                            break;
                                        if (WhiteCtr > nLeftDiagThreshold)
                                            break;

                                        int y2 = y + 1;
                                        int x2 = x - 1;
                                        while (y2 < y1 && x2 > x1)
                                        {
                                            p[(stride * y2) + x2] = 0;

                                            y2++;
                                            x2--;
                                        }

                                        WhiteCtr = 0;
                                    } //else

                                    y1++;
                                    x1--;
                                }
                            } //if
                        } //for x
                    } //for y

                    #endregion Diagonal toward left
                }

                outputImage.UnlockBits(bmData);
                return outputImage;
            } //try
            catch
            {
                return null;
            }
        }
    }
}