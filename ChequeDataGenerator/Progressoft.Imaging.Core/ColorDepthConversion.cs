﻿using Progressoft.Imaging.Core.Common;
using System.Drawing;
using System.Drawing.Imaging;

namespace Progressoft.Imaging.Core
{
    public static unsafe class ColorDepthConversion
    {
        public static Bitmap To8Bits(Bitmap inputImage, bool useWeights = false)
        {
            Bitmap outputImage;
            if (inputImage.PixelFormat == PixelFormat.Format8bppIndexed)
            {
                outputImage = inputImage.Clone() as Bitmap;
            }
            else if (inputImage.PixelFormat == PixelFormat.Format24bppRgb)
            {
                outputImage = Convert24To8(inputImage, useWeights);
            }
            else
            {
                Bitmap bmpTmp24 = To24Bits(inputImage);
                outputImage = Convert24To8(bmpTmp24, useWeights);
                bmpTmp24.Dispose();
            }

            return outputImage;
        }

        public static Bitmap To1Bit(Bitmap inputImage, byte threshold = 128, bool useWeights = false)
        {
            Bitmap outputImage;
            if (inputImage.PixelFormat == PixelFormat.Format1bppIndexed)
            {
                outputImage = inputImage.Clone() as Bitmap;
            }
            else if (inputImage.PixelFormat == PixelFormat.Format8bppIndexed)
            {
                outputImage = Convert8To1Bit(inputImage, threshold);
            }
            else if (inputImage.PixelFormat == PixelFormat.Format24bppRgb)
            {
                outputImage = Convert24To1Bit(inputImage, threshold, useWeights);
            }
            else
            {
                Bitmap bmpTmp24 = To24Bits(inputImage);
                outputImage = Convert24To1Bit(bmpTmp24, threshold, useWeights);
                bmpTmp24.Dispose();
            }

            return outputImage;
        }

        public static Bitmap To24Bits(Bitmap inputImage)
        {
            Bitmap outputImage;
            if (inputImage.PixelFormat == PixelFormat.Format24bppRgb)
            {
                outputImage = inputImage.Clone() as Bitmap;
            }
            else
            {
                outputImage = BitmapFactory.Create24ColoredImage(inputImage.Width, inputImage.Height);
                Graphics g = Graphics.FromImage(outputImage);
                g.DrawImage(inputImage, 0, 0, inputImage.Width, inputImage.Height);
                g.Dispose();
            }

            return outputImage;
        }

        public static Bitmap To3Bits(Bitmap inputImage)
        {
            Bitmap outputImage;
            if (inputImage.PixelFormat == PixelFormat.Format32bppArgb)
            {
                outputImage = inputImage.Clone() as Bitmap;
            }
            else
            {
                outputImage = BitmapFactory.Create32ColoredImage(inputImage.Width, inputImage.Height);
                Graphics g = Graphics.FromImage(outputImage);
                g.DrawImage(inputImage, 0, 0, inputImage.Width, inputImage.Height);
                g.Dispose();
            }

            return outputImage;
        }

        private static Bitmap Convert24To8(Bitmap bmpImg24, bool useWeights)
        {
            int width = bmpImg24.Width;
            int height = bmpImg24.Height;

            BitmapData bmpData24 = bmpImg24.LockBits(new System.Drawing.Rectangle(0, 0, width, height), ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
            int offset24 = bmpData24.Stride - (3 * width);

            Bitmap eightbitImg = BitmapFactory.CreateGrayImage(width, height);

            BitmapData bmpData8 = eightbitImg.LockBits(new System.Drawing.Rectangle(0, 0, width, height), ImageLockMode.WriteOnly, PixelFormat.Format8bppIndexed);
            int offset8 = bmpData8.Stride - width;

            byte* p24 = (byte*)bmpData24.Scan0.ToPointer();
            byte* p8 = (byte*)bmpData8.Scan0.ToPointer();

            for (int y = 0; y < height; y++, p8 += offset8, p24 += offset24)
                for (int x = 0; x < width; x++, p8++, p24 += 3)
                {
                    p8[0] = useWeights ? (byte)((.299 * p24[2]) + (.587 * p24[1]) + (.114 * p24[0])) :
                             p24[0];
                }

            bmpImg24.UnlockBits(bmpData24);
            eightbitImg.UnlockBits(bmpData8);

            return eightbitImg;
        }

        private static Bitmap Convert8To1Bit(Bitmap inputImage, byte threshold)
        {
            BitmapData bmpData = null; //bmpImg
            BitmapData outBmpData = null; //outputBmp
            Bitmap outputBmp = null;
            try
            {
                if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed)
                    return null;

                int width = inputImage.Width;
                int height = inputImage.Height;

                outputBmp = BitmapFactory.CreateBinaryImage(width, height);

                bmpData = inputImage.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                System.IntPtr scan0 = bmpData.Scan0;
                int offset = bmpData.Stride - width;

                outBmpData = outputBmp.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format1bppIndexed);
                System.IntPtr outScan0 = outBmpData.Scan0;
                int outStride = outBmpData.Stride;

                byte* p = (byte*)(void*)scan0;
                byte* outP = (byte*)(void*)outScan0;

                // temp variables:
                byte bit;
                int y_T_outStride, // y * outStride.
                    x_D_8; // x / 8.

                for (int y = 0; y < height; y++)
                {
                    y_T_outStride = y * outStride;

                    for (int x = 0; x < width; x++)
                    {
                        // make use of threshold to convert the image:
                        if (p[0] > threshold)
                            bit = 1;
                        else
                            bit = 0;
                        // position of the byte block that contains the bit bumber
                        // x in a raw:
                        x_D_8 = x / 8;

                        // prepare the bit to be set in its correct position inside
                        // the byte block, in which it should be placed:
                        bit = (byte)(bit << (7 - (x - 8 * x_D_8)));

                        // y_T_outStride + x_D_8: is the byte block in which the
                        // the pixel number x inside a raw is found:
                        outP[y_T_outStride + x_D_8] = (byte)(outP[y * outStride + x_D_8] | bit);

                        p++;
                    }
                    p += offset;
                }

                inputImage.UnlockBits(bmpData);
                outputBmp.UnlockBits(outBmpData);
                return outputBmp;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
		/// Converts a 24-bit image to 1-bit image:
		/// </summary>
		/// <param name="inputImage"> A 24-bit image. </param>
		/// <param name="outputBmp">Holds the resulted 1-bit image.</param>
		/// <param name="threshold">A threshold in the range [0,255]to map pixels
		///  values to 1 or 0.Pixels have intensity values grater than this threshold
		///  will be set to 1, otherwise they will be set to 0. </param>
		/// <returns>
		///  0: Success.
		///  1: invalid pixel format or threshold value is illegal.
		///  2: Generic error.
		/// </returns>
		private static Bitmap Convert24To1Bit(Bitmap inputImage, byte threshold, bool useWeights)
        {
            BitmapData bmpData = null; //for bmpImg
            BitmapData outBmpData = null; //for outputbmp
            try
            {
                if (inputImage.PixelFormat != PixelFormat.Format24bppRgb)
                    return null;

                int width = inputImage.Width;
                int height = inputImage.Height;

                // create a 1 bit image to hold the resulted image:
                Bitmap outputImage = BitmapFactory.CreateBinaryImage(width, height);

                Rectangle rectLock = new Rectangle(0, 0, width, height);
                // lock images to be processed:
                bmpData = inputImage.LockBits(rectLock, ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
                System.IntPtr scan0 = bmpData.Scan0;
                int offset = bmpData.Stride - (3 * width);

                outBmpData = outputImage.LockBits(rectLock, ImageLockMode.ReadWrite, PixelFormat.Format1bppIndexed);
                System.IntPtr outScan0 = outBmpData.Scan0;
                int outStride = outBmpData.Stride;

                byte* p = (byte*)(void*)scan0;
                byte* outP = (byte*)(void*)outScan0;

                // temp variables:
                byte bit;
                int y_T_outStride, // y * outStride.
                    x_D_8; // x / 8.

                for (int y = 0; y < height; y++)
                {
                    y_T_outStride = y * outStride;

                    for (int x = 0; x < width; x++)
                    {
                        // make use of threshold to convert the image:
                        byte gray = useWeights ? (byte)((.299 * p[2]) + (.587 * p[1]) + (.114 * p[0])) :
                             p[0];

                        if (gray > threshold)
                            bit = 1;
                        else
                            bit = 0;
                        // position of the byte block that contains the bit bumber
                        // x in a raw:
                        x_D_8 = x / 8;

                        // prepare the bit to be set in its correct position inside
                        // the byte block, in which it should be placed:
                        bit = (byte)(bit << (7 - (x - 8 * x_D_8)));

                        // y_T_outStride + x_D_8: is the byte block in which the
                        // the pixel number x inside a raw is found:
                        outP[y_T_outStride + x_D_8] = (byte)(outP[y * outStride + x_D_8] | bit);

                        p += 3;
                    }
                    p += offset;
                }
                inputImage.UnlockBits(bmpData);
                outputImage.UnlockBits(outBmpData);
                return outputImage;
            }
            catch
            {
                return null;
            }
        }
    }
}