using Progressoft.Imaging.Core.Common;
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace Progressoft.Imaging.Core
{
    /// <summary>
    /// Applies Various Geometric Transforms.
    /// </summary>
    public static class GeometricTransforms
    {
        /// <summary>
        /// Rotates 1-Bit, 8-Bit, or 24 Image By A Specific Angle Without Cutting Any Corners Off.
        /// Note: The Background Color Works On 24 Bit Only, You Can Only Set Black Or White In Case Of 8 Bit
        /// </summary>
        /// <param name="bmpInput">An 8-bit bitmap image to be rotated.</param>
        /// <param name="outputBmp">Holds the rotated image.</param>
        /// <param name="angle"> Measured in degrees.</param>
        /// <param name="BkGrndColor"> The Background Color After Rotation.</param>
        /// <param name="return24Bit">Determines Whether The Returned Bitmap Is 8 Or 24.</param>
        /// <returns>
        /// 0 : Success.
        /// 1 : Invalid pixel format.
        /// 2 : Generic error.
        /// 3: Cannot Convert To 8 Bit</returns>
        public static Bitmap Rotate(Bitmap bmpInput, float angle, Color BkGrndColor)
        {
            try
            {
                if (Image.GetPixelFormatSize(bmpInput.PixelFormat) > 24)
                    return null; // invalid pixel format.

                const double pi2 = Math.PI / 2.0;

                double oldWidth = bmpInput.Width;
                double oldHeight = bmpInput.Height;

                // Convert degrees to radians
                double theta = angle * Math.PI / 180.0;
                double locked_theta = theta;

                // Ensure theta is now [0, 2pi)
                while (locked_theta < 0.0) locked_theta += 2 * Math.PI;

                int nWidth, nHeight; // The newWidth/newHeight expressed as ints

                double adjacentTop, oppositeTop, adjacentBottom, oppositeBottom;

                // We need to calculate the sides of the triangles based
                // on how much rotation is being done to the bitmap.
                //   Refer to the first paragraph in the explaination above for
                //   reasons why.
                if ((locked_theta >= 0.0 && locked_theta < pi2) ||
                    (locked_theta >= Math.PI && locked_theta < (Math.PI + pi2)))
                {
                    adjacentTop = Math.Abs(Math.Cos(locked_theta)) * oldWidth;
                    oppositeTop = Math.Abs(Math.Sin(locked_theta)) * oldWidth;

                    adjacentBottom = Math.Abs(Math.Cos(locked_theta)) * oldHeight;
                    oppositeBottom = Math.Abs(Math.Sin(locked_theta)) * oldHeight;
                }
                else
                {
                    adjacentTop = Math.Abs(Math.Sin(locked_theta)) * oldHeight;
                    oppositeTop = Math.Abs(Math.Cos(locked_theta)) * oldHeight;

                    adjacentBottom = Math.Abs(Math.Sin(locked_theta)) * oldWidth;
                    oppositeBottom = Math.Abs(Math.Cos(locked_theta)) * oldWidth;
                }

                nWidth = (int) Math.Ceiling(adjacentTop + oppositeBottom);
                nHeight = (int) Math.Ceiling(adjacentBottom + oppositeTop);

                // create 24 bitmap to draw on:
                Bitmap tmp24 = BitmapFactory.Create24ColoredImage(nWidth, nHeight);
                Bitmap outputBmp = null;

                using (Graphics g = Graphics.FromImage(tmp24))
                {
                    g.Clear(BkGrndColor);
                    // This array will be used to pass in the three points that
                    // make up the rotated image
                    Point[] points;

                    /*
					 * The values of opposite/adjacentTop/Bottom are referring to
					 * fixed locations instead of in relation to the
					 * rotating image so I need to change which values are used
					 * based on the how much the image is rotating.
					 *
					 * For each point, one of the coordinates will always be 0,
					 * nWidth, or nHeight.  This because the Bitmap we are drawing on
					 * is the bounding box for the rotated bitmap.  If both of the
					 * corrdinates for any of the given points wasn't in the set above
					 * then the bitmap we are drawing on WOULDN'T be the bounding box
					 * as required.
					 */
                    if (locked_theta >= 0.0 && locked_theta < pi2)
                    {
                        points = new Point[]
                        {
                            new Point((int) oppositeBottom, 0),
                            new Point(nWidth, (int) oppositeTop),
                            new Point(0, (int) adjacentBottom)
                        };
                    }
                    else if (locked_theta >= pi2 && locked_theta < Math.PI)
                    {
                        points = new Point[]
                        {
                            new Point(nWidth, (int) oppositeTop),
                            new Point((int) adjacentTop, nHeight),
                            new Point((int) oppositeBottom, 0)
                        };
                    }
                    else if (locked_theta >= Math.PI && locked_theta < (Math.PI + pi2))
                    {
                        points = new Point[]
                        {
                            new Point((int) adjacentTop, nHeight),
                            new Point(0, (int) adjacentBottom),
                            new Point(nWidth, (int) oppositeTop)
                        };
                    }
                    else
                    {
                        points = new Point[]
                        {
                            new Point(0, (int) adjacentBottom),
                            new Point((int) oppositeBottom, 0),
                            new Point((int) adjacentTop, nHeight)
                        };
                    }

                    g.DrawImage(bmpInput, points);

                    outputBmp = Image.GetPixelFormatSize(bmpInput.PixelFormat) < 24
                        ? ColorDepthConversion.To8Bits(tmp24)
                        : tmp24.Clone() as Bitmap;
                } //using

                tmp24.Dispose();
                return outputBmp;
            } //try
            catch // an error.
            {
                return null;
            }
        }

        public static Bitmap ScaleByBilinear(Bitmap bmpInput, int newWidth, int newHeight,
            bool forceToReturn24Bit = false)
        {
            return Scale(bmpInput, newWidth, newHeight,
                InterpolationMode.Bilinear,
                CompositingQuality.HighQuality,
                SmoothingMode.HighQuality,
                forceToReturn24Bit);
        }

        public static Bitmap ScaleByBilinear(Bitmap bmpInput, float xFactor, float yFactor,
            bool forceToReturn24Bit = false)
        {
            return ScaleByBilinear(bmpInput, (int) Math.Round(bmpInput.Width * xFactor),
                (int) Math.Round(bmpInput.Height * yFactor),
                forceToReturn24Bit);
        }

        public static Bitmap ScaleByBilinear(Bitmap bmpInput, float factor, bool forceToReturn24Bit = false)
        {
            return ScaleByBilinear(bmpInput, factor, factor, forceToReturn24Bit);
        }

        public static Bitmap ScaleByBicubic(Bitmap bmpInput, int newWidth, int newHeight,
            bool forceToReturn24Bit = false)
        {
            return Scale(bmpInput, newWidth, newHeight,
                InterpolationMode.Bicubic,
                CompositingQuality.HighQuality,
                SmoothingMode.HighQuality,
                forceToReturn24Bit);
        }

        public static Bitmap ScaleByBicubic(Bitmap bmpInput, float xFactor, float yFactor,
            bool forceToReturn24Bit = false)
        {
            return ScaleByBicubic(bmpInput, (int) Math.Round(bmpInput.Width * xFactor),
                (int) Math.Round(bmpInput.Height * yFactor),
                forceToReturn24Bit);
        }

        public static Bitmap ScaleByBicubic(Bitmap bmpInput, float factor, bool forceToReturn24Bit = false)
        {
            return ScaleByBicubic(bmpInput, factor, factor, forceToReturn24Bit);
        }

        public static Bitmap ScaleByNearestNeighbor(Bitmap bmpInput, int newWidth, int newHeight,
            bool forceToReturn24Bit = false)
        {
            return Scale(bmpInput, newWidth, newHeight,
                InterpolationMode.NearestNeighbor,
                CompositingQuality.HighQuality,
                SmoothingMode.HighQuality,
                forceToReturn24Bit);
        }

        public static Bitmap ScaleByNearestNeighbor(Bitmap bmpInput, float xFactor, float yFactor,
            bool forceToReturn24Bit = false)
        {
            return ScaleByNearestNeighbor(bmpInput, (int) Math.Round(bmpInput.Width * xFactor),
                (int) Math.Round(bmpInput.Height * yFactor),
                forceToReturn24Bit);
        }

        public static Bitmap ScaleByNearestNeighbor(Bitmap bmpInput, float factor, bool forceToReturn24Bit = false)
        {
            return ScaleByNearestNeighbor(bmpInput, factor, factor, forceToReturn24Bit);
        }

        public static Bitmap ScaleByHighQualityBilinear(Bitmap bmpInput, int newWidth, int newHeight,
            bool forceToReturn24Bit = false)
        {
            return Scale(bmpInput, newWidth, newHeight,
                InterpolationMode.HighQualityBilinear,
                CompositingQuality.HighQuality,
                SmoothingMode.HighQuality,
                forceToReturn24Bit);
        }

        public static Bitmap ScaleByHighQualityBilinear(Bitmap bmpInput, float xFactor, float yFactor,
            bool forceToReturn24Bit = false)
        {
            return ScaleByHighQualityBilinear(bmpInput, (int) Math.Round(bmpInput.Width * xFactor),
                (int) Math.Round(bmpInput.Height * yFactor),
                forceToReturn24Bit);
        }

        public static Bitmap ScaleByHighQualityBilinear(Bitmap bmpInput, float factor, bool forceToReturn24Bit = false)
        {
            return ScaleByHighQualityBilinear(bmpInput, factor, factor, forceToReturn24Bit);
        }

        public static Bitmap ScaleByHighQualityBicubic(Bitmap bmpInput, int newWidth, int newHeight,
            bool forceToReturn24Bit = false)
        {
            return Scale(bmpInput, newWidth, newHeight,
                InterpolationMode.HighQualityBicubic,
                CompositingQuality.HighQuality,
                SmoothingMode.HighQuality,
                forceToReturn24Bit);
        }

        public static Bitmap ScaleByHighQualityBicubic(Bitmap bmpInput, float xFactor, float yFactor,
            bool forceToReturn24Bit = false)
        {
            return ScaleByHighQualityBicubic(bmpInput, (int) Math.Round(bmpInput.Width * xFactor),
                (int) Math.Round(bmpInput.Height * yFactor),
                forceToReturn24Bit);
        }

        public static Bitmap ScaleByHighQualityBicubic(Bitmap bmpInput, float factor, bool forceToReturn24Bit = false)
        {
            return ScaleByHighQualityBicubic(bmpInput, factor, factor, forceToReturn24Bit);
        }

        public static Bitmap ScaleImageToBestFit(Bitmap inputImage, int iWidth, int iHeight)
        {
            int iImgWidth = inputImage.Width;
            int iImgHeight = inputImage.Height;
            if (iImgWidth <= iWidth && iImgHeight <= iHeight)
            {
                return inputImage.Clone() as Bitmap;
            }
            else
            {
                Size sz = CalculateResizeToFit(new Size(iImgWidth, iImgHeight), new Size(iWidth, iHeight));
                return ScaleByBilinear(inputImage, sz.Width, sz.Height);
            }
        }

        private static Size CalculateResizeToFit(Size imageSize, Size boxSize)
        {
            var widthScale = boxSize.Width / (double) imageSize.Width;
            var heightScale = boxSize.Height / (double) imageSize.Height;
            var scale = Math.Min(widthScale, heightScale);
            return new Size(
                (int) Math.Round((imageSize.Width * scale)),
                (int) Math.Round((imageSize.Height * scale))
            );
        }

        private static Bitmap Scale(
            Bitmap bmpInput,
            int newWidth,
            int newHeight,
            InterpolationMode interpolation,
            CompositingQuality compositing,
            SmoothingMode smoothingMode,
            bool forceToReturn24Bit)
        {
            Bitmap tmp = BitmapFactory.Create24ColoredImage(newWidth, newHeight);
            Graphics g = Graphics.FromImage(tmp);
            g.InterpolationMode = interpolation;
            g.CompositingQuality = compositing;
            g.SmoothingMode = smoothingMode;
            g.Clear(Color.White);
            g.DrawImage(bmpInput, 0, 0, newWidth, newHeight);

            Bitmap resizedBmp;
            if (forceToReturn24Bit || Image.GetPixelFormatSize(bmpInput.PixelFormat) >= 24)
                resizedBmp = tmp.Clone() as Bitmap;
            else
                resizedBmp = ColorDepthConversion.To8Bits(tmp);

            g.Dispose();
            tmp.Dispose();
            return resizedBmp;
        }

        private static int Floor(double value)
        {
            var floor = (int) value;
            return floor > value ? floor - 1 : floor;
        }

        private static double InterpolateCubic(double x)
        {
            const double a = -0.75;

            var fx = Math.Abs(x);

            return fx >= 0 && fx <= 1
                ? ((a + 2) * fx - (a + 3)) * fx * fx + 1
                : fx > 1 && fx <= 2
                    ? ((a * fx - 5 * a) * fx + 8 * a) * fx - 4 * a
                    : 0.0;
        }

        public static Bitmap ScaleByStandardBiCubic(Bitmap image, double xFactor, double yFactor)
        {
            int dstWidth = Convert.ToInt32(Math.Round(image.Width * xFactor));
            int dstHeight = Convert.ToInt32(Math.Round(image.Height * yFactor));

            return NativeImagingWrapper.ResizeBiCubicInterpolation(image, dstWidth, dstHeight);
        }

        public static Bitmap ScaleByStandardBiCubic(Bitmap image, int dstWidth, int dstHeight)
        {
            return NativeImagingWrapper.ResizeBiCubicInterpolation(image, dstWidth, dstHeight);
        }

        public static Bitmap ResizeBiCubicInterpolation(Bitmap image, double xFactor, double yFactor)
        {
            int dstWidth = Convert.ToInt32(Math.Round(image.Width * xFactor));
            int dstHeight = Convert.ToInt32(Math.Round(image.Height * yFactor));

            return ResizeBiCubicInterpolation(image, dstWidth, dstHeight);
        }

        public static unsafe Bitmap ResizeBiCubicInterpolation(Bitmap image, int dstWidth, int dstHeight)
        {
            Bitmap result = null;
            BitmapData resultData = null;
            BitmapData imageData = null;

            try
            {
                int srcWidth = image.Width;
                int srcHeight = image.Height;

                result = BitmapFactory.CreateImage(dstWidth, dstHeight, image.PixelFormat);

                imageData = image.LockBits(new Rectangle(0, 0, srcWidth, srcHeight),
                    ImageLockMode.ReadWrite,
                    image.PixelFormat);
                resultData = result.LockBits(new Rectangle(0, 0, dstWidth, dstHeight),
                    ImageLockMode.ReadWrite,
                    result.PixelFormat);

                int channels = Math.Max(1, Image.GetPixelFormatSize(imageData.PixelFormat) / 8);

                double scaleX = (double) dstWidth / (double) srcWidth;
                double scaleY = (double) dstHeight / (double) srcHeight;

                byte* imagePtr = (byte*) (void*) imageData.Scan0;
                byte* resultPtr = (byte*) (void*) resultData.Scan0;

                double invScaleX = 1.0 / scaleX;
                double invScaleY = 1.0 / scaleY;

                int imageStride = imageData.Stride;
                int resultStride = resultData.Stride;
                int resultOffset = resultStride - dstWidth * channels;

                double[] hKernelCoefficients = new double[dstWidth * 4];
                int[] hKernelX = new int[dstWidth];

                double[] vKernelCoefficients = new double[dstHeight * 4];
                int[] vKernelY = new int[dstHeight];

                int dY, dX, c;
                double sX, sY;
                double dX1, dX2, dX3, dX4, dY1, dY2, dY3, dY4;
                double kX1, kX2, kX3, kX4, kY1, kY2, kY3, kY4;
                int iX1, iX2, iX3, iX4, iY1, iY2, iY3, iY4;
                int x1, x2, x3, x4, y1, y2, y3, y4;

                double k11,
                    k12,
                    k13,
                    k14,
                    k21,
                    k22,
                    k23,
                    k24,
                    k31,
                    k32,
                    k33,
                    k34,
                    k41,
                    k42,
                    k43,
                    k44;

                byte* ptr11,
                    ptr12,
                    ptr13,
                    ptr14,
                    ptr21,
                    ptr22,
                    ptr23,
                    ptr24,
                    ptr31,
                    ptr32,
                    ptr33,
                    ptr34,
                    ptr41,
                    ptr42,
                    ptr43,
                    ptr44;

                double v, diffV;
                int intV;
                int sXStart, sYStart;

                fixed (double* pVKernelCoefficients = &vKernelCoefficients[0])
                {
                    fixed (int* pVKernelY = &vKernelY[0])
                    {
                        fixed (double* pHKernelCoefficients = &hKernelCoefficients[0])
                        {
                            fixed (int* pHKernelX = &hKernelX[0])
                            {
                                double* pTmpVKernelCoefficients = pVKernelCoefficients;
                                int* pTmpVKernelY = pVKernelY;

                                double* pTmpHKernelCoefficients = pHKernelCoefficients;
                                int* pTmpHKernelX = pHKernelX;

                                for (int x = 0; x < dstWidth; x++)
                                {
                                    sX = (x + 0.5) * invScaleX - 0.5;
                                    var sXFloor = Floor(sX);

                                    dX1 = 1 + sX - sXFloor;
                                    dX2 = sX - sXFloor;
                                    dX3 = sXFloor + 1 - sX;
                                    dX4 = sXFloor + 2 - sX;

                                    *pTmpHKernelCoefficients = InterpolateCubic(dX1);
                                    pTmpHKernelCoefficients++;
                                    *pTmpHKernelCoefficients = InterpolateCubic(dX2);
                                    pTmpHKernelCoefficients++;
                                    *pTmpHKernelCoefficients = InterpolateCubic(dX3);
                                    pTmpHKernelCoefficients++;
                                    *pTmpHKernelCoefficients = InterpolateCubic(dX4);
                                    pTmpHKernelCoefficients++;

                                    *pTmpHKernelX = sXFloor - 1;
                                    pTmpHKernelX++;
                                }

                                for (int y = 0; y < dstHeight; y++)
                                {
                                    sY = (y + 0.5) * invScaleY - 0.5;
                                    var sYFloor = Floor(sY);

                                    dY1 = 1 + sY - sYFloor;
                                    dY2 = sY - sYFloor;
                                    dY3 = sYFloor + 1 - sY;
                                    dY4 = sYFloor + 2 - sY;

                                    *pTmpVKernelCoefficients = InterpolateCubic(dY1);
                                    pTmpVKernelCoefficients++;
                                    *pTmpVKernelCoefficients = InterpolateCubic(dY2);
                                    pTmpVKernelCoefficients++;
                                    *pTmpVKernelCoefficients = InterpolateCubic(dY3);
                                    pTmpVKernelCoefficients++;
                                    *pTmpVKernelCoefficients = InterpolateCubic(dY4);
                                    pTmpVKernelCoefficients++;

                                    *pTmpVKernelY = sYFloor - 1;
                                    pTmpVKernelY++;
                                }

                                pTmpVKernelCoefficients = pVKernelCoefficients;
                                pTmpVKernelY = pVKernelY;

                                for (dY = 0; dY < dstHeight; dY++)
                                {
                                    kY1 = *pTmpVKernelCoefficients;
                                    pTmpVKernelCoefficients++;
                                    kY2 = *pTmpVKernelCoefficients;
                                    pTmpVKernelCoefficients++;
                                    kY3 = *pTmpVKernelCoefficients;
                                    pTmpVKernelCoefficients++;
                                    kY4 = *pTmpVKernelCoefficients;
                                    pTmpVKernelCoefficients++;

                                    sYStart = *pTmpVKernelY;
                                    pTmpVKernelY++;

                                    y1 = sYStart;
                                    y2 = sYStart + 1;
                                    y3 = sYStart + 2;
                                    y4 = sYStart + 3;

                                    y1 = Math.Max(Math.Min(y1, srcHeight - 1), 0);
                                    y2 = Math.Max(Math.Min(y2, srcHeight - 1), 0);
                                    y3 = Math.Max(Math.Min(y3, srcHeight - 1), 0);
                                    y4 = Math.Max(Math.Min(y4, srcHeight - 1), 0);

                                    iY1 = y1 * imageStride;
                                    iY2 = y2 * imageStride;
                                    iY3 = y3 * imageStride;
                                    iY4 = y4 * imageStride;

                                    pTmpHKernelCoefficients = pHKernelCoefficients;
                                    pTmpHKernelX = pHKernelX;

                                    for (dX = 0; dX < dstWidth; dX++)
                                    {
                                        kX1 = *pTmpHKernelCoefficients;
                                        pTmpHKernelCoefficients++;
                                        kX2 = *pTmpHKernelCoefficients;
                                        pTmpHKernelCoefficients++;
                                        kX3 = *pTmpHKernelCoefficients;
                                        pTmpHKernelCoefficients++;
                                        kX4 = *pTmpHKernelCoefficients;
                                        pTmpHKernelCoefficients++;

                                        sXStart = *pTmpHKernelX;
                                        pTmpHKernelX++;

                                        x1 = sXStart;
                                        x2 = sXStart + 1;
                                        x3 = sXStart + 2;
                                        x4 = sXStart + 3;

                                        x1 = Math.Max(Math.Min(x1, srcWidth - 1), 0);
                                        x2 = Math.Max(Math.Min(x2, srcWidth - 1), 0);
                                        x3 = Math.Max(Math.Min(x3, srcWidth - 1), 0);
                                        x4 = Math.Max(Math.Min(x4, srcWidth - 1), 0);

                                        iX1 = x1 * channels;
                                        iX2 = x2 * channels;
                                        iX3 = x3 * channels;
                                        iX4 = x4 * channels;

                                        k11 = kY1 * kX1;
                                        k21 = kY2 * kX1;
                                        k31 = kY3 * kX1;
                                        k41 = kY4 * kX1;
                                        k12 = kY1 * kX2;
                                        k22 = kY2 * kX2;
                                        k32 = kY3 * kX2;
                                        k42 = kY4 * kX2;
                                        k13 = kY1 * kX3;
                                        k23 = kY2 * kX3;
                                        k33 = kY3 * kX3;
                                        k43 = kY4 * kX3;
                                        k14 = kY1 * kX4;
                                        k24 = kY2 * kX4;
                                        k34 = kY3 * kX4;
                                        k44 = kY4 * kX4;

                                        ptr11 = imagePtr + iY1 + iX1;
                                        ptr21 = imagePtr + iY2 + iX1;
                                        ptr31 = imagePtr + iY3 + iX1;
                                        ptr41 = imagePtr + iY4 + iX1;
                                        ptr12 = imagePtr + iY1 + iX2;
                                        ptr22 = imagePtr + iY2 + iX2;
                                        ptr32 = imagePtr + iY3 + iX2;
                                        ptr42 = imagePtr + iY4 + iX2;
                                        ptr13 = imagePtr + iY1 + iX3;
                                        ptr23 = imagePtr + iY2 + iX3;
                                        ptr33 = imagePtr + iY3 + iX3;
                                        ptr43 = imagePtr + iY4 + iX3;
                                        ptr14 = imagePtr + iY1 + iX4;
                                        ptr24 = imagePtr + iY2 + iX4;
                                        ptr34 = imagePtr + iY3 + iX4;
                                        ptr44 = imagePtr + iY4 + iX4;

                                        for (c = 0; c < channels; c++)
                                        {
                                            v = k11 * (ptr11 + c)[0] +
                                                k21 * (ptr21 + c)[0] +
                                                k31 * (ptr31 + c)[0] +
                                                k41 * (ptr41 + c)[0] +
                                                k12 * (ptr12 + c)[0] +
                                                k22 * (ptr22 + c)[0] +
                                                k32 * (ptr32 + c)[0] +
                                                k42 * (ptr42 + c)[0] +
                                                k13 * (ptr13 + c)[0] +
                                                k23 * (ptr23 + c)[0] +
                                                k33 * (ptr33 + c)[0] +
                                                k43 * (ptr43 + c)[0] +
                                                k14 * (ptr14 + c)[0] +
                                                k24 * (ptr24 + c)[0] +
                                                k34 * (ptr34 + c)[0] +
                                                k44 * (ptr44 + c)[0];

                                            v = Math.Min(255.0, Math.Max(0.0, v));
                                            intV = (int) v;
                                            diffV = v - intV;

                                            resultPtr[0] =
                                                (byte) (uint) (diffV > 0.5 || diffV == 0.5 && (intV & 1) != 0
                                                    ? intV + 1
                                                    : intV);

                                            resultPtr++;
                                        }
                                    }

                                    resultPtr += resultOffset;
                                }
                            }
                        }
                    }
                }

                return result;
            }
            catch
            {
                return result;
            }
            finally
            {
                if (imageData != null)
                {
                    image.UnlockBits(imageData);
                }

                if (result != null && resultData != null)
                {
                    result.UnlockBits(resultData);
                }
            }
        }
    } //Class
} //Namespace