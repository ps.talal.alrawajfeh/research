﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Major Code Smell", "S3358:Ternary operators should not be nested", Justification = "<Pending>", Scope = "member", Target = "~M:Progressoft.Imaging.Core.Binarization.Threshold(System.Drawing.Bitmap,System.Byte,System.Boolean)~System.Drawing.Bitmap")]
[assembly: SuppressMessage("Major Code Smell", "S3358:Ternary operators should not be nested", Justification = "<Pending>", Scope = "member", Target = "~M:Progressoft.Imaging.Core.Binarization.Otsu(System.Drawing.Bitmap,System.Boolean,System.Byte@)~System.Drawing.Bitmap")]
[assembly: SuppressMessage("Minor Code Smell", "S4136:Method overloads should be grouped together", Justification = "<Pending>", Scope = "member", Target = "~M:Progressoft.Imaging.Core.ImageConvolution.ConvoluteOdd(System.Drawing.Bitmap,Progressoft.Imaging.Core.ConvolutionOptions)~System.Drawing.Bitmap")]
[assembly: SuppressMessage("Minor Code Smell", "S4136:Method overloads should be grouped together", Justification = "<Pending>", Scope = "member", Target = "~M:Progressoft.Imaging.Core.ImageConvolution.ConvoluteEven(System.Drawing.Bitmap,Progressoft.Imaging.Core.ConvolutionOptions)~System.Drawing.Bitmap")]
[assembly: SuppressMessage("Style", "IDE0066:Convert switch statement to expression", Justification = "<Pending>", Scope = "member", Target = "~M:Progressoft.Imaging.Core.GenericProcessing.PasteImageOver(System.Drawing.Bitmap,System.Drawing.Bitmap,System.Int32,System.Int32,System.Int32,System.Int32)~System.Drawing.Bitmap")]
[assembly: SuppressMessage("Minor Code Smell", "S4136:Method overloads should be grouped together", Justification = "<Pending>", Scope = "member", Target = "~M:Progressoft.Imaging.Core.Fourier.FourierUtils.LinearFFT(System.Single[],System.Int32,System.Int32,System.Int32,Progressoft.Imaging.Core.Fourier.FourierDirection)")]
[assembly: SuppressMessage("Minor Code Smell", "S1450:Private fields only used as local variables in methods should become local variables", Justification = "<Pending>", Scope = "member", Target = "~F:Progressoft.Imaging.Core.Fourier.FourierUtils._reverseBits")]
[assembly: SuppressMessage("Minor Code Smell", "S1450:Private fields only used as local variables in methods should become local variables", Justification = "<Pending>", Scope = "member", Target = "~F:Progressoft.Imaging.Core.Fourier.FourierUtils._uRLookup")]
[assembly: SuppressMessage("Minor Code Smell", "S1450:Private fields only used as local variables in methods should become local variables", Justification = "<Pending>", Scope = "member", Target = "~F:Progressoft.Imaging.Core.Fourier.FourierUtils._uILookup")]
[assembly: SuppressMessage("Major Code Smell", "S1168:Empty arrays and collections should be returned instead of null", Justification = "<Pending>", Scope = "member", Target = "~M:Progressoft.Imaging.Core.Common.BitmapUtils.ToIntArray2D(System.Drawing.Bitmap)~System.Int32[,]")]