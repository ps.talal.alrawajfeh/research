﻿using System;
using System.Drawing;
using System.Drawing.Imaging;

namespace Progressoft.Imaging.Core
{
    /// <summary>
    /// Retreives statistical information from an image.
    /// </summary>
    public static unsafe class Statistics
    {
        /// <summary>
        /// Computes the mean of the given image.
        /// </summary>
        /// <param name="bmpImg"> An 8 or 24 bit image.</param>
        /// <returns>
        /// The mean: if succeed.
        /// -1: invalid pixel format.
        /// -2: Generic error.
        /// </returns>
        public static float GetMean(Bitmap bmpImg)
        {
            float mean = 0f;

            try
            {
                int bits = Image.GetPixelFormatSize(bmpImg.PixelFormat);

                if (bits < 8 || bits > 24)
                    return float.MinValue;

                int factor = 1;
                if (bmpImg.PixelFormat == PixelFormat.Format24bppRgb)
                    factor = 3;

                int width = bmpImg.Width;
                int height = bmpImg.Height;

                BitmapData bmpData = bmpImg.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, bmpImg.PixelFormat);
                System.IntPtr scan0 = bmpData.Scan0;
                int stride = bmpData.Stride;

                byte* p = (byte*)(void*)scan0;
                int offset = stride - (factor * width);

                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        mean += p[0];
                        p += factor;
                    }
                    p += offset;
                }

                mean /= (width * height);

                bmpImg.UnlockBits(bmpData);
            }
            catch
            {
                return float.MinValue;
            }

            return mean;
        }

        /// <summary>
        /// Computes the means of the intensity values greater than the
        /// specified threshold.
        /// </summary>
        /// <param name="bmpImg">An 8 or 24 bit image.</param>
        /// <returns>
        /// The mean of the intensity values greater than the specified threshold:
        /// if succeed.
        /// -1: invalid pixel format.
        /// -2: Generic error.
        /// </returns>
        public static float GetMeanAboveThreshold(Bitmap bmpImg, byte threshold)
        {
            float mean = 0f;

            try
            {
                int bits = Image.GetPixelFormatSize(bmpImg.PixelFormat);

                if (bits < 8 || bits > 24)
                    return float.MinValue;

                int factor = 1;
                if (bmpImg.PixelFormat == PixelFormat.Format24bppRgb)
                    factor = 3;

                int totalTestedPixels = 0;

                int width = bmpImg.Width;
                int height = bmpImg.Height;

                BitmapData bmpData = bmpImg.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, bmpImg.PixelFormat);
                System.IntPtr scan0 = bmpData.Scan0;
                int stride = bmpData.Stride;
                double dVal = 0.0;
                byte* p = (byte*)(void*)scan0;
                int offset = stride - (factor * width);

                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        if (p[0] >= threshold)
                        {
                            dVal += p[0];
                            totalTestedPixels++;
                        }
                        p += factor;
                    }
                    p += offset;
                }
                double dMean = dVal / totalTestedPixels;
                mean = (float)dMean;
                bmpImg.UnlockBits(bmpData);
            }
            catch
            {
                return float.MinValue;
            }

            return mean;
        }

        /// <summary>
        /// Computes the standard deviation of the given image.
        /// </summary>
        /// <param name="bmpImg">An 8 or 24 bit image.</param>
        /// <param name="mean"> the mean of the given image in the range [0, 255].</param>
        /// <returns>
        /// The standard deviation: if succeed.
        /// -1: invalid pixel format or the mean value is illegal.
        /// -2: Generic error.
        /// </returns>
        public static float GetStandardDeviation(Bitmap bmpImg, float mean)
        {
            float std_dev = 0f;

            try
            {
                int bits = Image.GetPixelFormatSize(bmpImg.PixelFormat);

                if (bits < 8 || bits > 24 || mean < 0 || mean > 255)
                    return float.MinValue;

                int factor = 1;
                if (bmpImg.PixelFormat == PixelFormat.Format24bppRgb)
                    factor = 3;

                int width = bmpImg.Width;
                int height = bmpImg.Height;

                BitmapData bmpData = bmpImg.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, bmpImg.PixelFormat);
                System.IntPtr scan0 = bmpData.Scan0;
                int stride = bmpData.Stride;

                byte* p = (byte*)(void*)scan0;
                int offset = stride - (factor * width);

                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        std_dev += (float)Math.Pow(p[0] - mean, 2d);
                        p += factor;
                    }
                    p += offset;
                }
                std_dev = (float)Math.Sqrt(std_dev / ((width * height) - 1));

                bmpImg.UnlockBits(bmpData);
            }
            catch
            {
                return float.MinValue;
            }

            return std_dev;
        }

        /// <summary>
        /// Computes variance of the given image.
        /// </summary>
        /// <param name="bmpImg">An 8 or 24 bit image.</param>
        /// <param name="mean"> the mean of the given image in the range [0, 255].</param>
        /// <returns>
        /// The variance: if succeed.
        /// -1: invalid pixel format or the mean value is illegal.
        /// -2: Generic error.
        /// </returns>
        public static float GetVariance(Bitmap bmpImg, float mean)
        {
            float std_dev = GetStandardDeviation(bmpImg, mean);
            return (std_dev >= 0) ? std_dev * std_dev : std_dev;
        }

        /// <summary>
        /// Computes the average absolute standard deviation of the given image.
        /// </summary>
        /// <param name="bmpImg">An 8 or 24 bit image.</param>
        /// <param name="mean"> the mean of the given image.</param>
        /// <returns>
        /// The average absolute standard deviation: if succeed..
        /// -1: invalid pixel format or the mean value is illegal.
        /// -2: Generic error.
        /// </returns>
        public static float GetAverageAsoluteStandardDeviation(Bitmap bmpImg, float mean)
        {
            float avg_std_dev = 0f;

            try
            {
                int bits = Image.GetPixelFormatSize(bmpImg.PixelFormat);

                if (bits < 8 || bits > 24 || mean < 0 || mean > 255)
                    return float.MinValue;

                int factor = 1;
                if (bmpImg.PixelFormat == PixelFormat.Format24bppRgb)
                    factor = 3;

                int width = bmpImg.Width;
                int height = bmpImg.Height;

                BitmapData bmpData = bmpImg.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, bmpImg.PixelFormat);
                System.IntPtr scan0 = bmpData.Scan0;
                int stride = bmpData.Stride;

                byte* p = (byte*)(void*)scan0;
                int offset = stride - (factor * width);

                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        avg_std_dev += Math.Abs(p[0] - mean);
                        p += factor;
                    }
                    p += offset;
                }
                avg_std_dev /= (width * height);

                bmpImg.UnlockBits(bmpData);
            }
            catch
            {
                return float.MinValue;
            }

            return avg_std_dev;
        }

        /// <summary>
        /// Computes intensity histogram of the given image.
        /// </summary>
        /// <param name="bmpImg"> An 8 or 24 bit image. </param>
        /// <param name="histogram">An array to hold the computed histogram.</param>
        /// <returns>
        ///  0: if succeed.
        /// -1: invalid pixel format.
        /// -2: Generic error.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Major Code Smell", "S1168:Empty arrays and collections should be returned instead of null", Justification = "<Pending>")]
        public static int[] GetImageHistogram(Bitmap bmpImg)
        {
            try
            {
                int bits = Image.GetPixelFormatSize(bmpImg.PixelFormat);

                if (bits < 8 || bits > 24)
                    return null;

                int factor = 1;
                if (bmpImg.PixelFormat == PixelFormat.Format24bppRgb)
                    factor = 3;

                int width = bmpImg.Width;
                int height = bmpImg.Height;

                BitmapData bmpData = bmpImg.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, bmpImg.PixelFormat);
                System.IntPtr scan0 = bmpData.Scan0;
                int stride = bmpData.Stride;

                int[] histogram = new int[256];

                byte* p = (byte*)(void*)scan0;
                int offset = stride - (factor * width);

                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        histogram[p[0]]++;

                        p += factor;
                    }
                    p += offset;
                }
                bmpImg.UnlockBits(bmpData);
                return histogram;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Computes midian intensity value of the given image.
        /// </summary>
        /// <param name="bmpImg"> An 8 or 24 bit image. </param>
        /// <returns>
        /// The midian value: if succeed.
        /// -1: invalid pixel format.
        /// -2: Generic error.
        /// </returns>
        public static int GetMidian(Bitmap bmpImg)
        {
            int mid = 0;

            try
            {
                int bits = Image.GetPixelFormatSize(bmpImg.PixelFormat);

                if (bits < 8 || bits > 24)
                    return int.MinValue;

                int factor = 1;
                if (bmpImg.PixelFormat == PixelFormat.Format24bppRgb)
                    factor = 3;

                int width = bmpImg.Width;
                int height = bmpImg.Height;

                BitmapData bmpData = bmpImg.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, bmpImg.PixelFormat);
                System.IntPtr scan0 = bmpData.Scan0;
                int stride = bmpData.Stride;
                int bmpOffset = stride - (factor * width);

                byte* p = (byte*)(void*)scan0;

                int[] grayValues = new int[256];

                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        grayValues[p[0]] = 1;
                        p += factor;
                    }
                    p += bmpOffset;
                }

                int remaining = 0;
                for (int i = 0; i < 256; i++)
                    if (grayValues[i] != 0)
                        remaining++;

                int index = 0;
                int[] remainings = new int[remaining];

                for (int i = 0; i < 256; i++)
                {
                    if (grayValues[i] != 0)
                    {
                        remainings[index] = i;
                        ++index;
                    }
                }
                Array.Sort(remainings);

                mid = remainings[(int)(remaining / 2f)];
                bmpImg.UnlockBits(bmpData);
            }
            catch
            {
                return int.MinValue;
            }

            return mid;
        }

        /// <summary>
        /// Computes the minimum and maximum values and hold.
        /// </summary>
        /// <param name="bmpImg">An 8 or 24 bit image.</param>
        /// <param name="minmax">A MinMax structure to hold the minimum and maximum
        /// values.</param>
        /// <returns>
        ///	 0: if succeed.
        /// -1: invalid pixel format.
        /// -2: Generic error.
        /// </returns>
        public static ValueRange<byte> GetGrayRange(Bitmap bmpImg)
        {
            try
            {
                int bits = Image.GetPixelFormatSize(bmpImg.PixelFormat);

                if (bits < 8 || bits > 24)
                    return null;

                int factor = 1;
                if (bmpImg.PixelFormat == PixelFormat.Format24bppRgb)
                    factor = 3;

                int width = bmpImg.Width;
                int height = bmpImg.Height;

                BitmapData bmpData = bmpImg.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, bmpImg.PixelFormat);
                System.IntPtr scan0 = bmpData.Scan0;
                int stride = bmpData.Stride;

                byte min = byte.MaxValue;
                byte max = byte.MinValue;

                byte* p = (byte*)(void*)scan0;
                int offset = stride - (factor * width);

                for (int y = 0; y < height; y++, p += offset)
                {
                    for (int x = 0; x < width; x++, p += factor)
                    {
                        if (p[0] < min)
                            min = p[0];
                        if (p[0] > max)
                            max = p[0];
                    }
                }
                bmpImg.UnlockBits(bmpData);
                return new ValueRange<byte>(min, max);
            }
            catch
            {
                return null;
            }
        }

        public static bool GetVerticalAndHorizontalHistogram(Bitmap bmpImg, byte threshold, out int[] verHistogram, out int[] horHistogram)
        {
            verHistogram = null;
            horHistogram = null;

            try
            {
                int bits = Image.GetPixelFormatSize(bmpImg.PixelFormat);

                if (bits < 8 || bits > 24)
                    return false;

                int factor = 1;
                if (bmpImg.PixelFormat == PixelFormat.Format24bppRgb)
                    factor = 3;

                // get the width and height:
                int bmpWidth = bmpImg.Width;
                int bmpHeight = bmpImg.Height;

                // vertical and horizontal projections:
                verHistogram = new int[bmpWidth];
                horHistogram = new int[bmpHeight];

                // lock the bitmap to be processed:
                BitmapData bmpData = bmpImg.LockBits(new Rectangle(0, 0, bmpWidth, bmpHeight), ImageLockMode.ReadWrite, bmpImg.PixelFormat);
                System.IntPtr scan0 = bmpData.Scan0;
                int stride = bmpData.Stride;

                byte* p = (byte*)(void*)scan0;
                int offset = stride - (factor * bmpWidth);

                // compute the vertical and horizontal projections:

                for (int y = 0; y < bmpHeight; y++)
                {
                    for (int x = 0; x < bmpWidth; x++)
                    {
                        if (p[0] < threshold)
                        {
                            horHistogram[y]++;
                            verHistogram[x]++;
                        }
                        p += factor;
                    }
                    p += offset;
                }
                bmpImg.UnlockBits(bmpData);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Computes the vertical histogram (projection) of the given image.
        /// </summary>
        /// <param name= "bmpImg">An 8 or 24 bit image.</param>
        /// <param name="verHistogram"> An array to hold the vertical histogram.</param>
        /// <param name="threshold">All intensity values less than the given this
        /// value will be considered as forground pexils.</param>
        /// <returns>
        ///  0: if succeed.
        /// -1: invalid pixel format.
        /// -2: Generic error.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Major Code Smell", "S1168:Empty arrays and collections should be returned instead of null", Justification = "<Pending>")]
        public static int[] GetVerticalHistogram(Bitmap bitmap, byte threshold, bool invertedColor = false)
        {
            BitmapData bitmapData = null;
            try
            {
                if (bitmap.PixelFormat != PixelFormat.Format8bppIndexed) return null;

                var width = bitmap.Width;
                var height = bitmap.Height;

                var histogram = new int[width];

                bitmapData = bitmap.LockBits(
                    new Rectangle(0, 0, width, height),
                    ImageLockMode.ReadWrite,
                    bitmap.PixelFormat);

                var stride = bitmapData.Stride;

                var ptr = (byte*)bitmapData.Scan0.ToPointer();
                var offset = stride - width;

                for (var y = 0; y < height; y++)
                {
                    for (var x = 0; x < width; x++)
                    {
                        if (!invertedColor && ptr[0] < threshold
                            || invertedColor && ptr[0] > threshold)
                            histogram[x]++;

                        ptr++;
                    }

                    ptr += offset;
                }

                return histogram;
            }
            catch
            {
                return null;
            }
            finally
            {
                if (bitmapData != null)
                {
                    bitmap.UnlockBits(bitmapData);
                }
            }
        }

        /// <summary>
        /// Computes the horizontal histogram (projection) of the given image.
        /// </summary>
        /// <param name="bmpImg">An 8 or 24 bit image.</param>
        /// <param name="horHistogram">An array to hold the horizontal histogram.</param>
        /// <param name="threshold">All intensity values less than the given this
        /// value will be considered as forground pexils.</param>
        /// <returns>
        ///  0: if succeed.
        /// -1: invalid pixel format.
        /// -2: Generic error.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Major Code Smell", "S1168:Empty arrays and collections should be returned instead of null", Justification = "<Pending>")]
        public static int[] GetHorizontalHistogram(Bitmap bmpImg, byte threshold)
        {
            try
            {
                int bits = Image.GetPixelFormatSize(bmpImg.PixelFormat);

                if (bits < 8 || bits > 24)
                    return null;

                int factor = 1;
                if (bmpImg.PixelFormat == PixelFormat.Format24bppRgb)
                    factor = 3;

                // get the width and height:
                int bmpWidth = bmpImg.Width;
                int bmpHeight = bmpImg.Height;

                //horizontal projection:
                int[] horHistogram = new int[bmpHeight];

                // lock the bitmap to be processed:
                BitmapData bmpData = bmpImg.LockBits(new Rectangle(0, 0, bmpWidth, bmpHeight), ImageLockMode.ReadWrite, bmpImg.PixelFormat);
                System.IntPtr scan0 = bmpData.Scan0;
                int stride = bmpData.Stride;

                byte* p = (byte*)(void*)scan0;
                int offset = stride - (factor * bmpWidth);

                for (int y = 0; y < bmpHeight; y++)
                {
                    for (int x = 0; x < bmpWidth; x++)
                    {
                        if (p[0] < threshold)
                            horHistogram[y]++;

                        p += factor;
                    }
                    p += offset;
                }

                bmpImg.UnlockBits(bmpData);
                return horHistogram;
            }
            catch
            {
                return null;
            }
        }

        public static int VerticalSum(Bitmap bitmap, int x, int y1, int y2)
        {
            BitmapData bitmapData = null;
            try
            {
                if (bitmap.PixelFormat != PixelFormat.Format8bppIndexed) return 0;

                if (y2 - y1 <= 1)
                {
                    return 0;
                }

                var height = bitmap.Height;
                var width = bitmap.Width;

                bitmapData = bitmap.LockBits(
                    new Rectangle(0, 0, width, height),
                    ImageLockMode.ReadWrite,
                    bitmap.PixelFormat);

                var stride = bitmapData.Stride;

                var ptr = (byte*)bitmapData.Scan0.ToPointer();

                ptr += y1 * stride + x;

                var sum = 0;
                for (var y = y1; y < y2; y++)
                {
                    sum += ptr[0];
                    ptr += stride;
                }

                return sum;
            }
            catch
            {
                return 0;
            }
            finally
            {
                if (bitmapData != null)
                {
                    bitmap.UnlockBits(bitmapData);
                }
            }
        }

        public static int HorizontalSum(Bitmap bitmap, int y, int x1, int x2)
        {
            BitmapData bitmapData = null;
            try
            {
                if (bitmap.PixelFormat != PixelFormat.Format8bppIndexed) return 0;

                if (x2 - x1 <= 1)
                {
                    return 0;
                }

                var height = bitmap.Height;
                var width = bitmap.Width;

                bitmapData = bitmap.LockBits(
                    new Rectangle(0, 0, width, height),
                    ImageLockMode.ReadWrite,
                    bitmap.PixelFormat);

                var stride = bitmapData.Stride;

                var ptr = (byte*)bitmapData.Scan0.ToPointer();

                ptr += stride * y;

                var sum = 0;
                for (var x = x1 + 1; x < x2; x++)
                {
                    sum += ptr[0];
                    ptr++;
                }

                return sum;
            }
            catch
            {
                return 0;
            }
            finally
            {
                if (bitmapData != null)
                {
                    bitmap.UnlockBits(bitmapData);
                }
            }
        }
    }
}