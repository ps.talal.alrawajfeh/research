using System.Drawing;
using System.Drawing.Imaging;

namespace Progressoft.Imaging.Core
{
    /// <summary>
    /// Applies Various Skeletonization Algorithms To 8 Bit Bitmaps.
    /// </summary>
    public unsafe class Skeletonization
    {
        public Bitmap Skeletonization_ZHang_Suen_V1(Bitmap inputImage, ref Bitmap outputImage)
        {
            try
            {
                if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed)
                    return null;

                int width = inputImage.Width;
                int height = inputImage.Height;
                Rectangle rectLock = new Rectangle(0, 0, inputImage.Width, inputImage.Height);

                Bitmap tmpImage = (Bitmap)inputImage.Clone();
                outputImage = (Bitmap)inputImage.Clone();
                BitmapData bmDataTmp = tmpImage.LockBits(rectLock, ImageLockMode.ReadWrite, tmpImage.PixelFormat);
                BitmapData bmDataOut = outputImage.LockBits(rectLock, ImageLockMode.ReadWrite, outputImage.PixelFormat);

                int stride = bmDataTmp.Stride;
                byte* pTmp = (byte*)bmDataTmp.Scan0.ToPointer();
                byte* pOut = (byte*)bmDataOut.Scan0.ToPointer();

                bool SkeletonStable = false;
                while (!SkeletonStable)
                {
                    SkeletonStable = true;
                    for (int y = 1; y < height - 1; y++)
                    {
                        for (int x = 1; x < width - 1; x++)
                        {
                            if (pTmp[(y * stride) + x] != 255
                                && ConnectivityNumber(x, y, pTmp, stride) == 1
                                && NeighbourCount(x, y, pTmp, stride) >= 2
                                && NeighbourCount(x, y, pTmp, stride) <= 6
                                && (pTmp[(y * stride) + (x - 1)] == 255
                                || pTmp[((y + 1) * stride) + x] == 255
                                || pTmp[(y * stride) + (x + 1)] == 255)
                                && (pTmp[((y + 1) * stride) + x] == 255
                                || pTmp[(y * stride) + (x + 1)] == 255
                                || pTmp[((y - 1) * stride) + x] == 255))
                            {
                                SkeletonStable = false;
                                pOut[(stride * y) + x] = 255;
                            }
                        }
                    }

                    for (int x = 0; x < width; x++)
                        for (int y = 0, y_t_s = y * stride; y < height; y++)
                            pTmp[y_t_s + x] = pOut[y_t_s + x];
                } //while !skeletonchange

                for (int y = 0; y < height - 1; y++)
                {
                    for (int x = 0; x < width - 1; x++)
                    {
                        //check the 4 cases
                        if (pOut[(y * stride) + x] != 255 && pOut[((y + 1) * stride) + x] != 255 &&
                            pOut[((y + 1) * stride) + (x + 1)] != 255 && pOut[((y) * stride) + (x + 1)] == 255)
                            pOut[((y + 1) * stride) + x] = 255;
                        else if (pOut[((y) * stride) + x] == 255 && pOut[((y + 1) * stride) + x] != 255 && pOut[((y + 1) * stride) + (x + 1)] != 255
                            && pOut[((y) * stride) + (x + 1)] != 255)
                            pOut[((y + 1) * stride) + (x + 1)] = 255;
                        else if (pOut[((y) * stride) + x] != 255 && pOut[((y + 1) * stride) + x] == 255 && pOut[((y + 1) * stride) + (x + 1)] != 255
                            && pOut[((y) * stride) + (x + 1)] != 255)

                            pOut[((y) * stride) + (x + 1)] = 255;
                        else if (pOut[((y) * stride) + x] != 255 && pOut[((y + 1) * stride) + x] != 255 && pOut[((y + 1) * stride) + (x + 1)] == 255
                            && pOut[((y) * stride) + (x + 1)] != 255)

                            pOut[((y) * stride) + (x)] = 255;
                    } //for x
                } //for y

                tmpImage.UnlockBits(bmDataTmp);
                tmpImage.Dispose();
                outputImage.UnlockBits(bmDataOut);
                return null;
            } //try
            catch
            {
                return null;
            }
        }

        private int ConnectivityNumber(int x, int y, byte* p, int stride)
        {
            int connectivity = 0;

            if (p[(stride * (y - 1)) + (x + 1)] != 255 && p[(stride * (y - 1)) + x] == 255)
            {
                connectivity++;
            }

            if (p[(stride * (y - 1)) + (x)] != 255 && p[(stride * (y - 1)) + (x - 1)] == 255)
            {
                connectivity++;
            }

            if (p[(stride * (y - 1)) + (x - 1)] != 255 && p[(stride * (y)) + (x - 1)] == 255)
            {
                connectivity++;
            }

            if (p[(stride * (y)) + (x - 1)] != 255 && p[(stride * (y + 1)) + (x - 1)] == 255)
            {
                connectivity++;
            }

            if (p[(stride * (y + 1)) + (x - 1)] != 255 && p[(stride * (y + 1)) + x] == 255)
            {
                connectivity++;
            }

            if (p[(stride * (y + 1)) + (x)] != 255 && p[(stride * (y + 1)) + (x + 1)] == 255)
            {
                connectivity++;
            }

            if (p[(stride * (y + 1)) + (x + 1)] != 255 && p[(stride * (y)) + (x + 1)] == 255)
            {
                connectivity++;
            }

            if (p[(stride * (y)) + (x + 1)] != 255 && p[(stride * (y - 1)) + (x + 1)] == 255)
            {
                connectivity++;
            }
            return connectivity;
        } //connectivityNumber

        private int NeighbourCount(int x, int y, byte* p, int stride)
        {
            int count = 0;

            if (p[(y - 1) * stride + (x + 1)] != 255) count++;
            if (p[(y - 1) * stride + (x)] != 255) count++;
            if (p[(y - 1) * stride + (x - 1)] != 255) count++;
            if (p[(y) * stride + (x - 1)] != 255) count++;
            if (p[(y) * stride + (x + 1)] != 255) count++;
            if (p[(y + 1) * stride + (x - 1)] != 255) count++;
            if (p[(y + 1) * stride + (x)] != 255) count++;
            if (p[(y + 1) * stride + (x + 1)] != 255) count++;
            return count;
        } //NeighbourCount
    }
}