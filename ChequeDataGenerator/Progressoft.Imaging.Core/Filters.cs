﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;

namespace Progressoft.Imaging.Core
{
    public static unsafe class Filters
    {
        public static Bitmap GaussianBlur3x3(Bitmap inputImage)
        {
            float[,] kernel = new float[3, 3]
                                         { { 1, 2, 1, },
                                         { 2,  4,2, },
                                         { 1, 2, 1, }, };
            ConvolutionOptions options = new ConvolutionOptions(kernel, false, 0, true, 1.0f / 16, 0);
            return ImageConvolution.Convolute(inputImage, options);
        }

        public static Bitmap BoxBlur3x3(Bitmap inputImage)
        {
            float[,] kernel = new float[3, 3]
                                         { { 1, 1, 1, },
                                         { 1,  1,1, },
                                         { 1, 1, 1, }, };
            ConvolutionOptions options = new ConvolutionOptions(kernel, false, 0, true, 1.0f / 9, 0);
            return ImageConvolution.Convolute(inputImage, options);
        }

        public static Bitmap GaussianBlur5x5(Bitmap inputImage)
        {
            float[,] kernel = new float[5, 5]
                                         { { 1, 4, 6,4,1 },
                                        { 4, 16, 24,16,4 },
                                         { 6, 24, 36,24,6 },
                                         { 4, 16, 24,16,4  },
                                         { 1, 4, 6,4,1 }};
            ConvolutionOptions options = new ConvolutionOptions(kernel, false, 0, true, 1.0f / 256, 0);
            return ImageConvolution.Convolute(inputImage, options);
        }

        public static Bitmap Sharpen(Bitmap inputImage)
        {
            float[,] kernel = new float[3, 3]
                                         { { 0, -1, 0 },
                                        { -1, 5, -1},
                                         { 0, -1, 0 }};
            ConvolutionOptions options = new ConvolutionOptions(kernel, false, 0, true, 1.0f / 256, 0);
            return ImageConvolution.Convolute(inputImage, options);
        }

        public static Bitmap MeanRemovalHightBoost(Bitmap inputImage)
        {
            float[,] kernel = new float[3, 3]
                                             { {-1 , -1 , -1 },
                                        { -1, 9, -1},
                                         { -1 , -1 , -1 }};
            ConvolutionOptions options = new ConvolutionOptions(kernel, false, 0, true, 1.0f / 256, 0);
            return ImageConvolution.Convolute(inputImage, options);
        }

        public static Bitmap UnshapGaussian5x5(Bitmap inputImage)
        {
            float[,] kernel = new float[5, 5]
                                         { { 1, 4, 6,4,1 },
                                        { 4, 16, 24,16,4 },
                                         { 6, 24, -476,24,6 },
                                         { 4, 16, 24,16,4  },
                                         { 1, 4, 6,4,1 }};
            ConvolutionOptions options = new ConvolutionOptions(kernel, false, 0, true, -1.0f / 256, 0);
            return ImageConvolution.Convolute(inputImage, options);
        }

        public static Bitmap ApplyPointOperation(Bitmap inputImage, Func<byte, float> operation)
        {
            BitmapData outBmpData = null;
            try
            {
                // invalid pixel format:
                if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed)
                    return null;

                int width = inputImage.Width;
                int height = inputImage.Height;

                // create the corrected image:
                Bitmap outputImage = (Bitmap)inputImage.Clone();

                outBmpData = outputImage.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                System.IntPtr outScan0 = outBmpData.Scan0;
                int outOffset = outBmpData.Stride - width;

                byte* outP = (byte*)(void*)outScan0;

                for (int y = 0; y < height; y++, outP += outOffset)
                    for (int x = 0; x < width; x++, outP++)
                        outP[0] = (byte)Math.Round(Math.Max(0, Math.Min(255, operation(outP[0]))));
                outputImage.UnlockBits(outBmpData);
                return outputImage;
            }
            catch
            {
                return null;
            }
        }

        public static Bitmap GammaCorrection(Bitmap inputImage, float gamma, int scale, int offsetValue)
        {
            return ApplyPointOperation(inputImage, b => ((float)Math.Pow(b, gamma) * scale) + offsetValue);
        }

        public static Bitmap Contrast(Bitmap inputImage, float contrast)
        {
            if (contrast < -100f || contrast > 100f)
                return null;

            contrast = (100f + contrast) / 100f;
            contrast *= contrast;
            return ApplyPointOperation(inputImage, b => ((((b / 255f) - 0.5f) * contrast) + 0.5f) * 255f);
        }

        public static Bitmap AutoContrast(Bitmap inputImage)
        {
            float mean = Statistics.GetMean(inputImage);
            float stdev = Statistics.GetStandardDeviation(inputImage, mean);
            byte max = (byte)Math.Min(255, Math.Round(mean + stdev));
            byte min = (byte)Math.Min(0, Math.Round(mean - stdev));
            return ApplyPointOperation(inputImage, b => (byte)(255 * ((b - min) / (float)(max - min))));
        }

        public static Bitmap LogarethmicTransform(Bitmap inputImage)
        {
            float[] grayLogs = Enumerable.Range(0, 256).Select(i => (float)Math.Log(1 + i)).ToArray();
            ValueRange<byte> grayRange = Statistics.GetGrayRange(inputImage);
            ValueRange<float> logRange = new ValueRange<float>(grayLogs[grayRange.Minimum],
                                                               grayLogs[grayRange.Maximum]);
            float min_minus_max = logRange.Maximum - logRange.Minimum;
            return ApplyPointOperation(inputImage, b => (byte)(255 * ((grayLogs[b] - logRange.Minimum) / min_minus_max)));
        }

        public static Bitmap Normalization(Bitmap inputImage)
        {
            ValueRange<byte> grayRange = Statistics.GetGrayRange(inputImage);
            float min_minus_max = grayRange.Maximum - grayRange.Minimum;
            return ApplyPointOperation(inputImage, b => (byte)(255 * ((b - grayRange.Minimum) / min_minus_max)));
        }

        public static Bitmap HistogramEqualization(Bitmap inputImage)
        {
            int[] histogram = Statistics.GetImageHistogram(inputImage);
            int imageArea = inputImage.Width * inputImage.Height;
            float[] sumOfProb = new float[256];// holds probabilty of each gray level:
            sumOfProb[0] = 255 * (histogram[0] / imageArea);
            for (int i = 1; i < 256; i++)
                sumOfProb[i] = sumOfProb[i - 1] + (255 * (histogram[i] / imageArea));
            return ApplyPointOperation(inputImage, b => (byte)(sumOfProb[b]));
        }

        public static Bitmap Brightness(Bitmap inputImage, float nBrightness)
        {
            if (nBrightness < -100f || nBrightness > 100f)
                return null;
            return ApplyPointOperation(inputImage, b => (byte)(b * (1 + nBrightness)));
        }

        public static Bitmap SquareTransformFunction(Bitmap inputImage)
        {
            return ApplyPointOperation(inputImage, b => (byte)(b * b / 255));
        }

        public static Bitmap SquareRootTransformFunction(Bitmap inputImage)
        {
            return ApplyPointOperation(inputImage, b => (byte)Math.Sqrt(b * 255));
        }
    }
}