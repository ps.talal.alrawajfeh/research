﻿using Progressoft.Imaging.Core.Common;
using System;
using System.Drawing;
using System.Drawing.Imaging;

namespace Progressoft.Imaging.Core
{
    public static unsafe class ImagePadding
    {
        public static Bitmap Padding(Bitmap inputImage, int left, int right, int top, int bottom, KnownColor color = KnownColor.White)
        {
            if (inputImage == null || left < 0 || right < 0 || top < 0 || bottom < 0)
                return null;

            PixelFormat pixelFormat = inputImage.PixelFormat;
            int width = inputImage.Width;
            int height = inputImage.Height;
            int newWidth = width + left + right;
            int newHeight = height + top + bottom;
            Bitmap outputImage;

            if (Image.GetPixelFormatSize(pixelFormat) > 8)
            {
                outputImage = BitmapFactory.CreateImage(newWidth, newHeight, PixelFormat.Format24bppRgb, color);
                Graphics g = Graphics.FromImage(outputImage);
                g.DrawImage(inputImage, left, top, width, height);
                g.Dispose();
            }
            else
            {
                outputImage = BitmapFactory.CreateImage(newWidth, newHeight, PixelFormat.Format8bppIndexed, color);
                BitmapData bmpDataIn = inputImage.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                BitmapData bmpDataOut = outputImage.LockBits(new Rectangle(0, 0, newWidth, newHeight), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);

                int strideIn = bmpDataIn.Stride;
                int strideOut = bmpDataOut.Stride;

                byte* pIn = (byte*)bmpDataIn.Scan0.ToPointer();
                byte* pOut = (byte*)bmpDataOut.Scan0.ToPointer() + (top * strideOut) + left;

                for (int y = 0; y < height; y++, pIn += strideIn, pOut += strideOut)
                    Buffer.MemoryCopy(pIn, pOut, strideOut, strideIn);
                inputImage.UnlockBits(bmpDataIn);
                outputImage.UnlockBits(bmpDataOut);
            }

            return outputImage;
        }

        public static Bitmap Padding(Bitmap inputImage, int paddingSize, KnownColor color = KnownColor.White)
        {
            return Padding(inputImage, paddingSize, paddingSize, paddingSize, paddingSize, color);
        }

        public static Bitmap LeftPadding(Bitmap inputImage, int left, KnownColor color = KnownColor.White)
        {
            return Padding(inputImage, left, 0, 0, 0, color);
        }

        public static Bitmap RightPadding(Bitmap inputImage, int right, KnownColor color = KnownColor.White)
        {
            return Padding(inputImage, 0, right, 0, 0, color);
        }

        public static Bitmap TopPadding(Bitmap inputImage, int top, KnownColor color = KnownColor.White)
        {
            return Padding(inputImage, 0, 0, top, 0, color);
        }

        public static Bitmap BottomPadding(Bitmap inputImage, int bottom, KnownColor color = KnownColor.White)
        {
            return Padding(inputImage, 0, 0, 0, bottom, color);
        }

        public static Bitmap LeftAndRightPadding(Bitmap inputImage, int left, int right, KnownColor color = KnownColor.White)
        {
            return Padding(inputImage, left, right, 0, 0, color);
        }

        public static Bitmap LeftAndRightPadding(Bitmap inputImage, int paddingSize, KnownColor color = KnownColor.White)
        {
            return Padding(inputImage, paddingSize, paddingSize, 0, 0, color);
        }

        public static Bitmap TopAndBottomPadding(Bitmap inputImage, int top, int bottom, KnownColor color = KnownColor.White)
        {
            return Padding(inputImage, 0, 0, top, bottom, color);
        }

        public static Bitmap TopAndBottomPadding(Bitmap inputImage, int paddingSize, KnownColor color = KnownColor.White)
        {
            return Padding(inputImage, 0, 0, paddingSize, paddingSize, color);
        }
    }
}