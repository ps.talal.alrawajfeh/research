﻿using Progressoft.Imaging.Core.Common;
using System;
using System.Drawing;
using System.Drawing.Imaging;

namespace Progressoft.Imaging.Core
{
    public static unsafe class ImageConvolution
    {
        public static Bitmap Convolute(Bitmap inputImage, ConvolutionOptions options)
        {
            return options.IsOddKernel ? ConvoluteOdd(inputImage, options) : ConvoluteEven(inputImage, options);
        }

        public static Bitmap Convolute(Bitmap inputImage, GradientConvolutionOptions options)
        {
            return options.IsOddKernel ? ConvoluteOdd(inputImage, options) : ConvoluteEven(inputImage, options);
        }

        private static Bitmap ConvoluteOdd(Bitmap inputImage, ConvolutionOptions options)
        {
            try
            {
                if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed)
                    return null;

                int width = inputImage.Width;
                int height = inputImage.Height;
                Rectangle rectLock = new Rectangle(0, 0, width, height);
                Bitmap outputImage = BitmapFactory.CreateGrayImage(width, height);
                BitmapData bmDataOut =
                    outputImage.LockBits(rectLock, ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                BitmapData bmDataIn =
                    inputImage.LockBits(rectLock, ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                int strideIn = bmDataIn.Stride;
                int strideOut = bmDataOut.Stride;
                byte* pOut = (byte*) bmDataOut.Scan0.ToPointer();
                byte* pIn = (byte*) bmDataIn.Scan0.ToPointer();

                int rightEdgeMargine = width - options.LeftMargine;
                int bottomEdgeMargine = height - options.LeftMargine;

                for (int r = 0; r < width; r++)
                {
                    for (int c = 0; c < height; c++)
                    {
                        float sumOfGrayValue = 0;

                        /* image boundaries */
                        if (r < options.LeftMargine || r >= rightEdgeMargine ||
                            c < options.LeftMargine || c >= bottomEdgeMargine)
                        {
                            sumOfGrayValue = options.PreservceEdgesColor
                                ? pIn[(strideIn * c) + r]
                                : options.EdgeGrayValue;
                        }
                        else
                        {
                            /* Convolution starts here */
                            for (int I = -options.LeftMargine; I <= options.LeftMargine; I++)
                            for (int J = -options.LeftMargine; J <= options.LeftMargine; J++)
                                sumOfGrayValue += pIn[(strideIn * (c + I)) + (r + J)] *
                                                  options.Kernel[J + options.LeftMargine, I + options.LeftMargine];
                            sumOfGrayValue = Math.Max(0,
                                Math.Min(255, options.Bias + (options.Weight * sumOfGrayValue)));
                        }

                        pOut[(strideOut * c) + r] =
                            (byte) (options.InvertColor ? (255 - sumOfGrayValue) : sumOfGrayValue);
                    }
                }

                inputImage.UnlockBits(bmDataIn);
                outputImage.UnlockBits(bmDataOut);
                return outputImage;
            }
            catch
            {
                return null;
            }
        }

        private static Bitmap ConvoluteEven(Bitmap inputImage, ConvolutionOptions options)
        {
            try
            {
                if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed)
                    return null;

                int width = inputImage.Width;
                int height = inputImage.Height;
                Rectangle rectLock = new Rectangle(0, 0, width, height);
                Bitmap outputImage = inputImage.Clone(rectLock, inputImage.PixelFormat);
                BitmapData bmDataOut =
                    outputImage.LockBits(rectLock, ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                BitmapData bmDataIn =
                    inputImage.LockBits(rectLock, ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                int stride = bmDataIn.Stride;
                byte* pOut = (byte*) bmDataOut.Scan0.ToPointer();
                byte* pIn = (byte*) bmDataIn.Scan0.ToPointer();

                int rightEdgeMargine = width - (width % options.KernelSize);
                int bottomEdgeMargine = height - (height % options.KernelSize);

                for (int r = 0; r < width; r++)
                {
                    for (int c = 0; c < height; c++)
                    {
                        float sumOfGrayValue = 0;
                        /* image boundaries */
                        if (r >= rightEdgeMargine || c >= bottomEdgeMargine)
                        {
                            sumOfGrayValue = options.PreservceEdgesColor
                                ? pIn[(stride * c) + r]
                                : options.EdgeGrayValue;
                        }
                        else
                        {
                            /* Convolution starts here */
                            for (int I = 0; I < options.KernelSize; I++)
                            for (int J = 0; J < options.KernelSize; J++)
                                sumOfGrayValue += pIn[(stride * (c + I)) + (r + J)] *
                                                  options.Kernel[J, I];
                            sumOfGrayValue = Math.Max(0,
                                Math.Min(255, options.Bias + (options.Weight * sumOfGrayValue)));
                        }

                        pOut[(stride * c) + r] = (byte) (options.InvertColor ? (255 - sumOfGrayValue) : sumOfGrayValue);
                    }
                }

                inputImage.UnlockBits(bmDataIn);
                outputImage.UnlockBits(bmDataOut);
                return outputImage;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private static Bitmap ConvoluteOdd(Bitmap inputImage, GradientConvolutionOptions options)
        {
            try
            {
                if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed)
                    return null;

                int width = inputImage.Width;
                int height = inputImage.Height;
                Rectangle rectLock = new Rectangle(0, 0, width, height);
                Bitmap outputImage = BitmapFactory.CreateGrayImage(width, height);
                BitmapData bmDataOut =
                    outputImage.LockBits(rectLock, ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                BitmapData bmDataIn =
                    inputImage.LockBits(rectLock, ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                int stride = bmDataIn.Stride;
                byte* pOut = (byte*) bmDataOut.Scan0.ToPointer();
                byte* pIn = (byte*) bmDataIn.Scan0.ToPointer();

                int rightEdgeMargine = width - options.LeftMargine;
                int bottomEdgeMargine = height - options.LeftMargine;

                for (int r = 0; r < width; r++)
                {
                    for (int c = 0; c < height; c++)
                    {
                        float sumOfGrayValue = 0;

                        /* image boundaries */
                        if (r < options.LeftMargine || r >= rightEdgeMargine ||
                            c < options.LeftMargine || c >= bottomEdgeMargine)
                        {
                            sumOfGrayValue = options.PreservceEdgesColor
                                ? pIn[(stride * c) + r]
                                : options.EdgeGrayValue;
                        }
                        else
                        {
                            /* Convolution starts here */
                            float sumOfGrayValueGx = 0;
                            float sumOfGrayValueGy = 0;

                            for (int I = -options.LeftMargine; I <= options.LeftMargine; I++)
                            for (int J = -options.LeftMargine; J <= options.LeftMargine; J++)
                            {
                                sumOfGrayValueGx += pIn[(stride * (c + I)) + (r + J)] *
                                                    options.KernelX[J + options.LeftMargine, I + options.LeftMargine];

                                sumOfGrayValueGy += pIn[(stride * (c + I)) + (r + J)] *
                                                    options.KernelY[J + options.LeftMargine, I + options.LeftMargine];
                            }

                            sumOfGrayValue = Math.Abs(sumOfGrayValueGx) + Math.Abs(sumOfGrayValueGy);
                            sumOfGrayValue = Math.Max(0,
                                Math.Min(255, options.Bias + (options.Weight * sumOfGrayValue)));
                        }

                        pOut[(stride * c) + r] = (byte) (options.InvertColor ? (255 - sumOfGrayValue) : sumOfGrayValue);
                    }
                }

                inputImage.UnlockBits(bmDataIn);
                outputImage.UnlockBits(bmDataOut);
                return outputImage;
            }
            catch
            {
                return null;
            }
        }

        private static Bitmap ConvoluteEven(Bitmap inputImage, GradientConvolutionOptions options)
        {
            try
            {
                if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed)
                    return null;

                int width = inputImage.Width;
                int height = inputImage.Height;
                Rectangle rectLock = new Rectangle(0, 0, width, height);
                Bitmap outputImage = inputImage.Clone(rectLock, inputImage.PixelFormat);
                BitmapData bmDataOut =
                    outputImage.LockBits(rectLock, ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                BitmapData bmDataIn =
                    inputImage.LockBits(rectLock, ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                int stride = bmDataIn.Stride;
                byte* pOut = (byte*) bmDataOut.Scan0.ToPointer();
                byte* pIn = (byte*) bmDataIn.Scan0.ToPointer();

                int rightEdgeMargine = width - (width % options.KernelSize);
                int bottomEdgeMargine = height - (height % options.KernelSize);

                for (int r = 0; r < width; r++)
                {
                    for (int c = 0; c < height; c++)
                    {
                        float sumOfGrayValue = 0;
                        /* image boundaries */
                        if (r >= rightEdgeMargine || c >= bottomEdgeMargine)
                        {
                            sumOfGrayValue = options.PreservceEdgesColor
                                ? pIn[(stride * c) + r]
                                : options.EdgeGrayValue;
                        }
                        else
                        {
                            /* Convolution starts here */
                            float sumOfGrayValueGx = 0;
                            float sumOfGrayValueGy = 0;

                            for (int I = 0; I < options.KernelSize; I++)
                            for (int J = 0; J < options.KernelSize; J++)
                            {
                                sumOfGrayValueGx += pIn[(stride * (c + I)) + (r + J)] *
                                                    options.KernelX[J, I];

                                sumOfGrayValueGy += pIn[(stride * (c + I)) + (r + J)] *
                                                    options.KernelY[J, I];
                            }

                            sumOfGrayValue = Math.Abs(sumOfGrayValueGx) + Math.Abs(sumOfGrayValueGy);
                            sumOfGrayValue = Math.Max(0,
                                Math.Min(255, options.Bias + (options.Weight * sumOfGrayValue)));
                        }

                        pOut[(stride * c) + r] = (byte) (options.InvertColor ? (255 - sumOfGrayValue) : sumOfGrayValue);
                    }
                }

                inputImage.UnlockBits(bmDataIn);
                outputImage.UnlockBits(bmDataOut);
                return outputImage;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}