﻿using Progressoft.Imaging.Core.Common;
using System;
using System.Collections;
using System.Drawing;
using System.Drawing.Imaging;

namespace Progressoft.Imaging.Core
{
    internal abstract class Run
    {
        /// <summary>
        /// column
        /// </summary>
        internal int col;		//column

        /// <summary>
        /// First and last rows
        /// </summary>
        internal int r1, r2;		//First and last rows

        /// <summary>
        /// The col height
        /// </summary>
        internal int height;		//The col height

        ///public VRun *LRun;
        /// LEft and right run
        /// </summary>
        //public VRun *RRun;	//LEft and right run
        /// <summary>
        /// row
        /// </summary>
        internal int row;	//row

        /// <summary>
        /// first and last column
        /// </summary>
        internal int c1, c2;	//first and last column

        /// <summary>
        /// it's length
        /// </summary>
        internal int len;	//it's length
    }

    internal class VRun : Run
    {
        public VRun()
        {
            col = 0;
            r1 = 0;
            r2 = 0;
            height = 0;
        }

        public VRun(int c, int rr1, int rr2, int h)
        {
            col = c;
            r1 = rr1;
            r2 = rr2;
            height = h;
        }
    }

    internal class HRun : Run
    {
        public HRun()
        {
            row = 0;
            c1 = 0;
            c2 = 0;
            len = 0;
        }

        public HRun(int r, int cc1, int cc2, int l)
        {
            row = r;
            c1 = cc1;
            c2 = cc2;
            len = l;
        }
    }

    public class Line
    {
        /// <summary>
        /// Start Point of the Line (X)
        /// </summary>
        public int startX;

        /// <summary>
        /// Start Point of the Line (Y)
        /// </summary>
        public int startY;

        /// <summary>
        /// The width of the Line
        /// </summary>
        public int width;

        /// <summary>
        /// The Height of the Line
        /// </summary>
        public int height;

        /// <summary>
        /// Initialize a line with zero values
        /// </summary>
        public Line()
        {
            startX = 0;
            startY = 0;
            width = 0;
            height = 0;
        }

        /// <summary>
        /// Define a new Line(As a rectangle of width and height)
        /// (Combination of identical number of pixels)
        /// </summary>
        /// <param name="sX">Start Point of the Line (X)</param>
        /// <param name="sY">Start Point of the Line (Y)</param>
        /// <param name="W">The width of the Line</param>
        /// <param name="H">The Height of the Line</param>
        public Line(int sX, int sY, int W, int H)
        {
            startX = sX;
            startY = sY;
            width = W;
            height = H;
        }
    }

    public class Pixel
    {
        public int x;
        public int y;
    }

    public enum SortDirection
    {
        Asc,
        Desc
    }

    public class LineHeightComparer : IComparer
    {
        private readonly SortDirection m_direction = SortDirection.Asc;

        public LineHeightComparer() : base()
        {
        }

        public LineHeightComparer(SortDirection direction)
        {
            this.m_direction = direction;
        }

        int IComparer.Compare(object x, object y)
        {
            Line categoryX = (Line)x;
            Line categoryY = (Line)y;

            if (categoryX == null && categoryY == null)
            {
                return 0;
            }
            else if (categoryX == null && categoryY != null)
            {
                return (this.m_direction == SortDirection.Asc) ? -1 : 1;
            }
            else if (categoryX != null && categoryY == null)
            {
                return (this.m_direction == SortDirection.Asc) ? 1 : -1;
            }
            else
            {
                return (this.m_direction == SortDirection.Asc) ?
                    categoryX.height.CompareTo(categoryY.height) :
                    categoryY.height.CompareTo(categoryX.height);
            }
        }
    }

    public class LineStartXComparer : IComparer
    {
        private readonly SortDirection m_direction = SortDirection.Asc;

        public LineStartXComparer() : base()
        {
        }

        public LineStartXComparer(SortDirection direction)
        {
            this.m_direction = direction;
        }

        int IComparer.Compare(object x, object y)
        {
            Line categoryX = (Line)x;
            Line categoryY = (Line)y;

            if (categoryX == null && categoryY == null)
            {
                return 0;
            }
            else if (categoryX == null && categoryY != null)
            {
                return (this.m_direction == SortDirection.Asc) ? -1 : 1;
            }
            else if (categoryX != null && categoryY == null)
            {
                return (this.m_direction == SortDirection.Asc) ? 1 : -1;
            }
            else
            {
                return (this.m_direction == SortDirection.Asc) ?
                    categoryX.startX.CompareTo(categoryY.startX) :
                    categoryY.startX.CompareTo(categoryX.startX);
            }
        }
    }

    public class LineStartYComparer : IComparer
    {
        private readonly SortDirection m_direction = SortDirection.Asc;

        public LineStartYComparer() : base()
        {
        }

        public LineStartYComparer(SortDirection direction)
        {
            this.m_direction = direction;
        }

        int IComparer.Compare(object x, object y)
        {
            Line categoryX = (Line)x;
            Line categoryY = (Line)y;

            if (categoryX == null && categoryY == null)
            {
                return 0;
            }
            else if (categoryX == null && categoryY != null)
            {
                return (this.m_direction == SortDirection.Asc) ? -1 : 1;
            }
            else if (categoryX != null && categoryY == null)
            {
                return (this.m_direction == SortDirection.Asc) ? 1 : -1;
            }
            else
            {
                return (this.m_direction == SortDirection.Asc) ?
                    categoryX.startY.CompareTo(categoryY.startY) :
                    categoryY.startY.CompareTo(categoryX.startY);
            }
        }
    }

    public static unsafe class RunLength
    {
        internal static void FindHRun(BitmapData bmData, ref ArrayList HorizontalRuns, int RLNeglected, bool isSevere, int startX, int endX, int startY, int endY)
        {
            int stride = bmData.Stride;
            System.IntPtr Scan0 = bmData.Scan0;

            byte* p = (byte*)(void*)Scan0;

            int nOffset = stride - bmData.Width;
            int nWidth = bmData.Width;
            int height = bmData.Height;
            bool bIsVisited = false;
            int indRL = 0;
            int realendX = nWidth - endX;
            int realendY = height - endY;

            for (int y = startY; y < realendY; y++)
            {
                for (int x = startX; x < realendX; x++)
                {
                    indRL = 0;

                    //compare also if pixel not in another run length (No need)
                    //fill also the Left and Right run lengths (find all run Lengths first)
                    //remove visited pixel from Run Lengths
                    if (p[(y * stride) + x] == 0 && !bIsVisited)
                    {
                        try
                        {
                            while (indRL + x < nWidth && p[(y * stride) + (x + indRL)] == 0)
                                ++indRL;
                        }
                        catch
                        {
                        }

                        HRun HorizontalRun = new HRun()
                        {
                            c1 = x,
                            row = y,
                            len = indRL,
                            c2 = (x + indRL - 1)
                        };
                        x = x + indRL - 1;

                        //check for next run length
                        if (isSevere == true)
                        {
                            int rowSeq = 0;
                            bool bSeq = IsOtherRunL(ref rowSeq, stride, 10, 20, x, y, p, nWidth, height);
                            if (bSeq == true)
                            {
                                HorizontalRun.c2 = rowSeq;
                                HorizontalRun.len = HorizontalRun.c2 - HorizontalRun.c1;
                            }
                        }
                        //Neglect small run lengths
                        if (indRL > RLNeglected)
                        {
                            HorizontalRuns.Add(HorizontalRun);
                        }
                    }
                }
            }
        }

        private static bool IsOtherRunL(ref int rowSeq, int stride, int T1, int T2, int x, int y, byte* p, int width, int height)
        {
            //bool sequentBlack=true;
            int j = 0;
            //int rowSeq=0;
            try
            {
                for (int i = 1; i < T1; i++)
                {
                    j = 0;
                    while (((x + i + j) < width) && ((p[(y * stride) + (x + i + j)] == 0))) //for(int j=0;j<T2;j++)
                    {
                        j++;
                    }
                    if (j > 20)
                    {
                        rowSeq = (x + i + j);
                        return true;
                    }
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        internal static void FindVRun(BitmapData bmData, ref ArrayList VerticalRuns, int RLNeglected, int startThreshX, int endThreshX, int startThreshY, int endThreshY)
        {
            //#region "Vertical Runs"

            int stride = bmData.Stride;
            System.IntPtr Scan0 = bmData.Scan0;
            byte* p = (byte*)(void*)Scan0;
            int nOffset = stride - bmData.Width;
            int nWidth = bmData.Width;
            int height = bmData.Height;
            //bool bIsVisited=false;
            int indRL = 0;
            int endX = nWidth - endThreshX;
            int endY = height - endThreshY;

            for (int x = startThreshX; x < endX; ++x)
            {
                for (int y = startThreshY; y < endY; ++y)
                {
                    indRL = 0;
                    //if black pixel
                    if (p[(y * stride) + x] == 0)
                    {
                        try
                        {
                            while (indRL < height - y && p[((y + indRL) * stride) + x] == 0)
                            {
                                ++indRL;
                            }
                        }
                        catch (Exception)
                        {
                        }

                        VRun VerticalRun = new VRun
                        {
                            col = x,
                            r1 = y,
                            height = indRL,
                            r2 = (y + indRL - 1)
                        };
                        y = y + indRL - 1;

                        if (indRL > RLNeglected)
                            VerticalRuns.Add(VerticalRun);
                    }
                }
            }
        }

        internal static void MakeLineFromHRun(ArrayList HorizontalRuns, ref ArrayList HLines, int NewLThresh, int AddedLThresh)
        {
            try
            {
                int HorizontalRunsC = HorizontalRuns.Count;
                //int HLinesC=HLines.Count ;
                bool RunInNewLines = true;

                for (int iHR = 0; iHR < HorizontalRunsC; iHR++)
                {
                    for (int iHL = 0; iHL < HLines.Count; iHL++)
                    {
                        int HRx1 = ((HRun)HorizontalRuns[iHR]).c1;
                        int HRx2 = ((HRun)HorizontalRuns[iHR]).c2;

                        int HLx1 = ((Line)(HLines[iHL])).startX;
                        int HLx2 = HLx1 + ((Line)(HLines[iHL])).height;

                        if (Math.Max(HRx1, HLx1) - Math.Min(HRx2, HLx2) <= 2)
                        {
                            int HRy = ((HRun)HorizontalRuns[iHR]).row;
                            int HLy1 = ((Line)(HLines[iHL])).startY;
                            int HLy2 = HLy1 + ((Line)(HLines[iHL])).width;
                            //modified jan 25,2007
                            if ((HRy >= HLy1 + 1 && HRy <= HLy2 + 1) && ((HRun)HorizontalRuns[iHR]).len > AddedLThresh && ((Line)HLines[iHL]).width < 60)
                            {
                                RunInNewLines = false;
                                ((Line)(HLines[iHL])).startX = Math.Min(HLx1, HRx1);
                                ((Line)(HLines[iHL])).startY = Math.Min(HLy1, HRy);
                                //modified 6 jan,2007
                                //if (HRy == HLy1 + 1 || HRy == HLy2 + 1)
                                {
                                    ((Line)(HLines[iHL])).width++;
                                }
                                if (((Line)(HLines[iHL])).height < Math.Max(HLx2 - HRx1, HRx2 - HLx1))
                                {
                                    ((Line)(HLines[iHL])).height = Math.Max(HLx2 - HRx1, HRx2 - HLx1);
                                }
                                else if (HLx1 < HRx1 && HLx2 > HRx2)
                                {
                                    ((Line)(HLines[iHL])).height = HLx2 - HLx1;
                                }
                                else if (HRx1 < HLx1 && HRx2 > HLx2)
                                {
                                    ((Line)(HLines[iHL])).height = HRx2 - HRx1;
                                }
                                else
                                {
                                    ((Line)(HLines[iHL])).height = Math.Max(Math.Max(HLx2 - HRx1, HRx2 - HLx1), ((HRun)HorizontalRuns[iHR]).len);
                                }
                                break;
                            }
                        }
                    }
                    if (RunInNewLines == true && ((HRun)(HorizontalRuns[iHR])).len > NewLThresh)
                    {
                        Line ln = new Line
                        {
                            startX = ((HRun)(HorizontalRuns[iHR])).c1,
                            startY = ((HRun)(HorizontalRuns[iHR])).row,
                            height = ((HRun)(HorizontalRuns[iHR])).len,
                            width = 1
                        };
                        HLines.Add(ln);
                    }
                    RunInNewLines = true;
                }
            }
            catch
            {
            }
        }

        internal static void MakeLineFromVRun2(ArrayList VerticalRuns, ref ArrayList VLines, int NewLThresh, int AddedLThresh)
        {
            try
            {
                int VerticalRunsC = VerticalRuns.Count;
                //int HLinesC=VLines.Count ;
                bool RunInNewLines = true;

                for (int iHR = 0; iHR < VerticalRunsC; iHR++)
                {
                    for (int iHL = 0; iHL < VLines.Count; iHL++)
                    {
                        int HRx1 = ((VRun)VerticalRuns[iHR]).r1;
                        int HRx2 = ((VRun)VerticalRuns[iHR]).r2;

                        int HLx1 = ((Line)(VLines[iHL])).startX;
                        int HLx2 = HLx1 + ((Line)(VLines[iHL])).height;

                        if (Math.Max(HRx1, HLx1) - Math.Min(HRx2, HLx2) <= 2)
                        {
                            int HRy = ((VRun)VerticalRuns[iHR]).col;
                            int HLy1 = ((Line)(VLines[iHL])).startY;
                            int HLy2 = HLy1 + ((Line)(VLines[iHL])).width;

                            if ((HRy >= HLy1 + 1 && HRy <= HLy2 + 1) && ((VRun)VerticalRuns[iHR]).height > AddedLThresh && ((Line)(VLines[iHL])).width < 60)
                            {
                                RunInNewLines = false;
                                ((Line)(VLines[iHL])).startX = Math.Min(HLx1, HRx1);
                                ((Line)(VLines[iHL])).startY = Math.Min(HLy1, HRy);
                                if (HRy == HLy1 + 1 || HRy == HLy2 + 1)
                                {
                                    ((Line)(VLines[iHL])).width++;
                                }
                                if (((Line)(VLines[iHL])).height < Math.Max(HLx2 - HRx1, HRx2 - HLx1))
                                {
                                    ((Line)(VLines[iHL])).height = Math.Max(HLx2 - HRx1, HRx2 - HLx1);
                                }
                                else if (HLx1 < HRx1 && HLx2 > HRx2)
                                {
                                    ((Line)(VLines[iHL])).height = HLx2 - HLx1;
                                }
                                else if (HRx1 < HLx1 && HRx2 > HLx2)
                                {
                                    ((Line)(VLines[iHL])).height = HRx2 - HRx1;
                                }
                                else
                                {
                                    ((Line)(VLines[iHL])).height = Math.Max(Math.Max(HLx2 - HRx1, HRx2 - HLx1), ((VRun)VerticalRuns[iHR]).height);
                                }
                                break;
                            }
                        }
                    }
                    if (RunInNewLines == true && ((VRun)(VerticalRuns[iHR])).height > NewLThresh)
                    {
                        Line ln = new Line
                        {
                            startX = ((VRun)(VerticalRuns[iHR])).r1,
                            startY = ((VRun)(VerticalRuns[iHR])).col,
                            height = ((VRun)(VerticalRuns[iHR])).height,
                            width = 1
                        };
                        VLines.Add(ln);
                    }
                    RunInNewLines = true;
                }
            }
            catch (Exception exe)
            {
                Console.WriteLine(exe.Message);
            }
        }

        internal static void MakeLineFromVRun(ArrayList HorizontalRuns, ref ArrayList HLines, int NewLThresh, int AddedLThresh)
        {
            int HorizontalRunsC = HorizontalRuns.Count;
            bool RunInNewLines = true;

            for (int iHR = 0; iHR < HorizontalRunsC; iHR++)
            {
                for (int iHL = 0; iHL < HLines.Count; iHL++)
                {
                    int HRx1 = ((VRun)HorizontalRuns[iHR]).r1;
                    int HRx2 = ((VRun)HorizontalRuns[iHR]).r2;

                    int HLx1 = ((Line)(HLines[iHL])).startY;
                    int HLx2 = HLx1 + ((Line)(HLines[iHL])).height;

                    if (Math.Max(HRx1, HLx1) - Math.Min(HRx2, HLx2) <= 2)
                    {
                        int HRy = ((VRun)HorizontalRuns[iHR]).col;
                        int HLy1 = ((Line)(HLines[iHL])).startX;
                        int HLy2 = HLy1 + ((Line)(HLines[iHL])).width;

                        if ((HRy >= HLy1 + 1 && HRy <= HLy2 + 1) && ((VRun)HorizontalRuns[iHR]).len > AddedLThresh)
                        {
                            RunInNewLines = false;
                            ((Line)(HLines[iHL])).startX = Math.Min(HLx1, HRx1);
                            ((Line)(HLines[iHL])).startY = Math.Min(HLy1, HRy);
                            if (HRy == HLy1 + 1 || HRy == HLy2 + 1)
                            {
                                ((Line)(HLines[iHL])).width++;
                            }
                            if (((Line)(HLines[iHL])).height < Math.Max(HLx2 - HRx1, HRx2 - HLx1))
                            {
                                ((Line)(HLines[iHL])).height = Math.Max(HLx2 - HRx1, HRx2 - HLx1);
                            }
                            else
                            {
                                ((Line)(HLines[iHL])).height = Math.Max(Math.Max(HLx2 - HRx1, HRx2 - HLx1), ((VRun)HorizontalRuns[iHR]).height);
                            }
                            break;
                        }
                    }
                }
                if (RunInNewLines == true && ((VRun)(HorizontalRuns[iHR])).height > NewLThresh)
                {
                    Line ln = new Line
                    {
                        startX = ((VRun)(HorizontalRuns[iHR])).r1,
                        startY = ((VRun)(HorizontalRuns[iHR])).col,
                        height = ((VRun)(HorizontalRuns[iHR])).height
                    };
                    HLines.Add(ln);
                }
                RunInNewLines = true;
            }
        }

        internal static bool ConnectHRunLengths(ref HRun hr1, HRun hr2, int VThresh, int HThresh, int LRequired)
        {
            try
            {
                //length is suitable
                if ((hr2.len < LRequired) || (hr1.len < LRequired))
                { return false; }
                //Linearity Check (on same line)
                if (Math.Abs(hr1.row - hr2.row) > VThresh)
                { return false; }
                //connection Check
                if (Math.Max(hr1.c2, hr2.c2) - Math.Min(hr1.c1, hr2.c1) > HThresh)
                {
                    return false;
                }

                //connect the two Run Lengths
                if (hr1.c1 > hr2.c2)
                {
                    hr1.c1 -= (hr1.c1 - hr2.c2);
                    hr1.len = hr1.c2 - hr1.c1;
                }
                else
                {
                    hr1.c2 += (hr2.c1 - hr1.c2);
                    hr1.len = hr1.c2 - hr1.c1;
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Fill certain run length in an image
        /// </summary>
        /// <param name="hr">The run length to be filled in the image</param>
        /// <param name="bmData">The image to have the run length</param>
        internal static void FillHRunInImage(HRun hr, ref BitmapData bmData)
        {
            try
            {
                int stride = bmData.Stride;
                System.IntPtr Scan0 = bmData.Scan0;
                byte* p = (byte*)(void*)Scan0;

                //Run Length values
                int c1 = hr.c1;
                int c2 = hr.c2;
                int row = hr.row;

                for (int i = c1; i <= c2; i++)
                {
                    p[(row * stride) + i] = 0;
                }
            }
            catch
            {
            }
        }

        /// <summary>
        /// Fill lines in an image
        /// </summary>
        /// <param name="Lines">Array of lines to be filled</param>
        /// <param name="bmData">the image</param>
        /// <param name="Thresh">color to be filled</param>
        /// <param name="LineType">Line Type
        /// H	:Horizontal Line
        /// V	:Vertical Line</param>
        internal static int FillLinesInImage(ArrayList Lines, ref BitmapData bmData, byte Thresh, string LineType)
        {
            try
            {
                if (LineType.ToUpper() == "H")
                {
                    int stride = bmData.Stride;
                    System.IntPtr Scan0 = bmData.Scan0;
                    byte* p = (byte*)(void*)Scan0;

                    int LinesC = Lines.Count;
                    for (int iHL = 0; iHL < LinesC; iHL++)
                    {
                        int HLx1 = ((Line)(Lines[iHL])).startX;
                        int HLx2 = HLx1 + ((Line)(Lines[iHL])).height;
                        int HLy1 = ((Line)(Lines[iHL])).startY;
                        int HLy2 = HLy1 + ((Line)(Lines[iHL])).width;

                        for (int row = HLx1; row < HLx2; row++)
                        {
                            for (int col = HLy1; col < HLy2; col++)
                            {
                                p[(col * stride) + row] = Thresh;
                            }
                        }
                    }
                }
                else
                {
                    int stride = bmData.Stride;
                    System.IntPtr Scan0 = bmData.Scan0;
                    byte* p = (byte*)(void*)Scan0;

                    int LinesC = Lines.Count;
                    for (int iHL = 0; iHL < LinesC; iHL++)
                    {
                        int HLx1 = ((Line)(Lines[iHL])).startY;
                        int HLx2 = HLx1 + ((Line)(Lines[iHL])).height;
                        int HLy1 = ((Line)(Lines[iHL])).startX;
                        int HLy2 = HLy1 + ((Line)(Lines[iHL])).width;

                        for (int row = HLx1; row < HLx2; row++)
                        {
                            for (int col = HLy1; col <= HLy2; col++)
                            {
                                p[(row * stride) + col] = Thresh;
                            }
                        }
                    }
                }
                return 0;
            }
            catch
            {
                return 2;
            }
        }

        internal static unsafe void FillHLineInImage(Line pLine, ref BitmapData bmData, byte colValue)
        {
            try
            {
                int stride = bmData.Stride;
                System.IntPtr Scan0 = bmData.Scan0;
                byte* p = (byte*)(void*)Scan0;

                int HLx1 = pLine.startX;
                int HLx2 = HLx1 + pLine.height;
                int HLy1 = pLine.startY;
                int HLy2 = HLy1 + pLine.width;

                for (int row = HLx1; row <= HLx2; row++)
                    for (int col = HLy1; col <= HLy2; col++)
                        p[(col * stride) + row] = colValue;
            }
            catch
            {
            }
        }

        internal static void FillVLinesInImage(ArrayList Lines, ref BitmapData bmData, byte thresh)
        {
            try
            {
                int stride = bmData.Stride;
                System.IntPtr Scan0 = bmData.Scan0;
                byte* p = (byte*)(void*)Scan0;

                int LinesC = Lines.Count;
                for (int iHL = 0; iHL < LinesC; iHL++)
                {
                    int HLx1 = ((Line)(Lines[iHL])).startX;
                    int HLx2 = HLx1 + ((Line)(Lines[iHL])).height;
                    int HLy1 = ((Line)(Lines[iHL])).startY;
                    int HLy2 = HLy1 + ((Line)(Lines[iHL])).width;

                    for (int row = HLx1; row <= HLx2; row++)
                        for (int col = HLy1; col <= HLy2; col++)
                            p[(row * stride) + col] = thresh;
                }
            }
            catch
            {
            }
        }

        internal static void RemoveHRunFromImage(HRun hr, ref BitmapData bmData, int TextThresh)
        {
            try
            {
                int stride = bmData.Stride;
                System.IntPtr Scan0 = bmData.Scan0;
                byte* p = (byte*)(void*)Scan0;

                //Run Length values
                int c1 = hr.c1;
                int c2 = hr.c2;
                int row = hr.row;
                int j = 0;
                //int height=bmData.Height ;
                for (int i = c1; i <= c2; i++)
                {
                    j = 0;
                    while ((p[((row - j) * stride) + i] == 0) && (row - j) > 0)
                    {
                        j++;
                    }
                    if (j < TextThresh)
                    { p[(row * stride) + i] = 255; }
                }
            }
            catch
            {
            }
        }

        internal static void RemoveVRunFromImage(VRun hr, ref BitmapData bmData, int TextThresh)
        {
            int stride = bmData.Stride;
            System.IntPtr Scan0 = bmData.Scan0;
            byte* p = (byte*)(void*)Scan0;

            //Run Length values
            int r1 = hr.r1;
            int r2 = hr.r2;
            int col = hr.col;
            int j = 0;

            try
            {
                //int height=bmData.Height ;
                for (int i = r1; i <= r2; i++)
                {
                    j = 0;
                    while ((col - j) > 0 && (p[(i * stride) + (col - j)] == 0))
                        j++;
                    if (j < TextThresh)
                    { p[(i * stride) + col] = 255; }
                }
            }
            catch (Exception)
            {
            }
        }

        //Based on certain criteria to remove (Or not) the run length
        /// <summary>
        /// Run lengths to be removed from the line
        /// </summary>
        /// <param name="HorizontalRuns">HorizontalRuns</param>
        /// <param name="hr">Checked Horizontal Run</param>
        /// <param name="LThresh">Min allowed Pixel length(less than this will be deleted)</param>
        /// <param name="HThresh">Distance Between two run lengths(Default to 5)</param>
        /// <param name="Lseed">seed of the line</param>
        /// <param name="VThresh">Vertical Distance Between two run lengths(Default to 1)</param>
        /// <returns></returns>
        internal static bool RemoveRunLength(ArrayList Runs, Run hr, int LThresh, int HThresh, int Lseed, int VThresh)
        {
            if (HThresh < 1)
            { HThresh = 4; }

            int RunsC = Runs.Count;

            try
            {
                if (hr.GetType() == typeof(HRun))
                {
                    if (hr.len < LThresh) //normally 2 pixels
                    { return true; }
                    if (hr.len > Lseed) return false;

                    for (int i = 0; i < RunsC; i++)
                    {
                        if ((Math.Abs(((HRun)Runs[i]).row - hr.row) <= VThresh) && ((HRun)Runs[i]).len > Lseed)
                        {
                            if (Math.Max(((HRun)Runs[i]).c1, hr.c1) - Math.Min(((HRun)Runs[i]).c2, hr.c2) < HThresh)
                            {
                                return false;
                            }
                        }
                    }
                }
            }
            catch
            {
            }
            try
            {
                if (hr.GetType() == typeof(VRun))
                {
                    if (hr.height < LThresh) //normally 2 pixels
                    { return true; }
                    if (hr.height > Lseed) return false;

                    for (int i = 0; i < RunsC; i++)
                    {
                        if ((Math.Abs(((VRun)Runs[i]).col - hr.col) <= VThresh) && ((VRun)Runs[i]).height > Lseed)
                        {
                            if (Math.Max(((VRun)Runs[i]).r1, hr.r1) - Math.Min(((VRun)Runs[i]).r2, hr.r2) < HThresh)
                            {
                                return false;
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
            }
            return true;
        }

        internal static int FindLines(Bitmap src, ref Bitmap bresult)
        {
            BitmapData bmresData = null;
            BitmapData bmData = null;

            try
            {
                ArrayList VerticalRuns = new ArrayList();
                ArrayList HorizontalRuns = new ArrayList();
                ArrayList HLines = new ArrayList();
                ArrayList VLines = new ArrayList();

                //assume binary image
                if (src.PixelFormat != PixelFormat.Format8bppIndexed)
                    return 1;
                //Bitmap hLines=null;
                bresult = (Bitmap)src.Clone();
                bmresData = bresult.LockBits(new Rectangle(0, 0, src.Width, src.Height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);

                bmData = src.LockBits(new Rectangle(0, 0, src.Width, src.Height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);

                //Make the new Bitmap White
                int stride = bmresData.Stride;
                System.IntPtr Scan0 = bmresData.Scan0;

                byte* p = (byte*)(void*)Scan0;

                int nOffset = stride - bmresData.Width;
                int nWidth = bmresData.Width;
                int height = bmresData.Height;

                for (int y = 0; y < height; ++y)
                {
                    for (int x = 0; x < nWidth; ++x)
                    {
                        p[(y * stride) + x] = 255;
                    }
                }
                //1. Horizontal Lines
                RunLength.FindHRun(bmData, ref HorizontalRuns, 15, true, 0, 0, 0, 0);
                RunLength.FindVRun(bmData, ref VerticalRuns, 15, 0, 0, 0, 0);

                //Testing:
                //Filling the Horizontal runs into an image
                int HorizontalRunsC = HorizontalRuns.Count;
                int VerticalRunsC = VerticalRuns.Count;

                //Horizontal Runs
                for (int i = 0; i < HorizontalRunsC; i++)
                {
                    if (RunLength.RemoveRunLength(HorizontalRuns, (HRun)(HorizontalRuns[i]), 5, 10, 150, 2) == true)
                    {
                        //Remove Run Length from the list
                        HorizontalRuns.RemoveAt(i);
                        i--;
                        HorizontalRunsC--;
                    }
                }

                //Vertical Runs

                for (int j = 0; j < VerticalRunsC; j++)
                {
                    if (RemoveRunLength(VerticalRuns, (VRun)(VerticalRuns[j]), 15, 10, 60, 2) == true)
                        VerticalRuns.RemoveAt(j);
                    j--;
                    VerticalRunsC--;
                }

                RunLength.MakeLineFromHRun(HorizontalRuns, ref HLines, 70, 50);
                RunLength.MakeLineFromVRun(VerticalRuns, ref VLines, 40, 30);

                int resl = RunLength.FillLinesInImage(HLines, ref bmresData, (byte)(0), "H");

                //DateTime st=DateTime.Now;
                resl = RunLength.FillLinesInImage(VLines, ref bmresData, (byte)(0), "v");

                return 0;
            }
            catch (Exception)
            {
                return 2;
            }
            finally
            {
                if (src != null)
                    src.UnlockBits(bmData);
                if (bresult != null)
                    bresult.UnlockBits(bmresData);
            }
        }

        /// <summary>
        /// return an image with lines
        /// </summary>
        /// <param name="src">the source image</param>
        /// <param name="bresult">the resulted image(could be same as source)</param>
        /// <returns>success: 0</returns>
        /// <returns>Invalid Pixel Format: 1</returns>
        /// <returns>fail: 2</returns>
        public static int FindLinesChecks(Bitmap src, ref Bitmap bresult)
        {
            //DateTime st=DateTime.Now ;
            //Definitions
            BitmapData bmresData = null;
            BitmapData bmData = null;
            //TimeSpan  sp=2;

            try
            {
                //ColorPalette ColorPal;
                ArrayList VerticalRuns = new ArrayList();
                ArrayList HorizontalRuns = new ArrayList();
                ArrayList HLines = new ArrayList();
                ArrayList VLines = new ArrayList();

                //assume binary image
                if (src.PixelFormat != PixelFormat.Format8bppIndexed)
                    return 1;

                bresult = (Bitmap)src.Clone();
                bmresData = bresult.LockBits(new Rectangle(0, 0, src.Width, src.Height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                bmData = src.LockBits(new Rectangle(0, 0, src.Width, src.Height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);

                //Make the new Bitmap White
                int stride = bmresData.Stride;
                System.IntPtr Scan0 = bmresData.Scan0;
                int nWidth;
                int height;
                byte* p = (byte*)(void*)Scan0;

                int nOffset = stride - bmresData.Width;
                nWidth = bmresData.Width;
                height = bmresData.Height;

                for (int y = 0; y < height; ++y)
                {
                    for (int x = 0; x < nWidth; ++x)
                    {
                        p[(y * stride) + x] = 255;
                    }
                }

                //Find Horizontal and vertical Runs
                RunLength.FindHRun(bmData, ref HorizontalRuns, 15, true, 10, 10, 10, 10);
                RunLength.FindVRun(bmData, ref VerticalRuns, 7, Convert.ToUInt16(nWidth / 2), 10, Convert.ToUInt16(height / 8), Convert.ToUInt16(height / 8));

                //Counters
                int HorizontalRunsC = HorizontalRuns.Count;
                int VerticalRunsC = VerticalRuns.Count;

                //Horizontal Runs
                for (int i = 0; i < HorizontalRunsC; i++)
                {
                    if (RemoveRunLength(HorizontalRuns, (HRun)(HorizontalRuns[i]), 5, 10, 150, 2) == true)
                    {
                        //Remove Run Length from the list
                        HorizontalRuns.RemoveAt(i);
                        i--;
                        HorizontalRunsC--;
                    }
                }

                //Vertical Runs
                for (int j = 0; j < VerticalRunsC; j++)
                {
                    if (RunLength.RemoveRunLength(VerticalRuns, (VRun)(VerticalRuns[j]), 7, 10, 40, 2) == true)
                    {
                        //Remove Run Length from the list
                        VerticalRuns.RemoveAt(j);
                        j--;
                        VerticalRunsC--;
                    }
                }

                // Make Lines
                RunLength.MakeLineFromHRun(HorizontalRuns, ref HLines, 70, 50);
                RunLength.MakeLineFromVRun(VerticalRuns, ref VLines, 50, 30);
                int LengthC = 0;
                for (int i = 0; i < HLines.Count; i++)
                {
                    if (((Line)HLines[i]).width > 0)
                    {
                        LengthC += ((Line)(HLines[i])).height;
                    }
                }
                Console.WriteLine("Lines: " + LengthC);
                //Fill Lines
                int resh = RunLength.FillLinesInImage(HLines, ref bmresData, (byte)(0), "H");
                int resv = RunLength.FillLinesInImage(VLines, ref bmresData, (byte)(0), "V");

                //Free variables
                VerticalRuns.Clear();
                VerticalRuns = null;
                HorizontalRuns.Clear();
                HorizontalRuns = null;
                HLines.Clear();
                HorizontalRuns = null;

                return 0;
            }
            catch (Exception)
            {
                return 2;
            }
            finally
            {
                if (src != null)
                    src.UnlockBits(bmData);
                if (bresult != null)
                    bresult.UnlockBits(bmresData);
            }
        }

        public static int FindLinesChecks(Bitmap src, ref ArrayList HLines, ref ArrayList VLines, string LineType)
        {
            BitmapData bmData = null;

            try
            {
                //ColorPalette ColorPal;
                ArrayList VerticalRuns = new ArrayList();
                ArrayList HorizontalRuns = new ArrayList();

                //assume binary image
                if (src.PixelFormat != PixelFormat.Format8bppIndexed)
                    return 1;

                bmData = src.LockBits(new Rectangle(0, 0, src.Width, src.Height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);

                //				//Make the new Bitmap White
                //				int stride = bmresData.Stride;
                //				System.IntPtr Scan0 = bmresData.Scan0;
                int nWidth = bmData.Width;
                int height = bmData.Height;

                if (LineType.ToUpper() == "B")
                {
                    //Find Horizontal and vertical Runs
                    RunLength.FindHRun(bmData, ref HorizontalRuns, 15, true, 10, 10, 10, 10);
                    RunLength.FindVRun(bmData, ref VerticalRuns, 15, (int)(nWidth / 2), 10, (int)(height / 8), (int)(height / 8));//7,8,8

                    //Counters
                    int HorizontalRunsC = HorizontalRuns.Count;
                    int VerticalRunsC = VerticalRuns.Count;
                    //Horizontal Runs
                    for (int i = 0; i < HorizontalRunsC; i++)
                    {
                        if (RemoveRunLength(HorizontalRuns, (HRun)(HorizontalRuns[i]), 5, 10, 150, 2) == true)
                        {
                            //Remove Run Length from the list
                            HorizontalRuns.RemoveAt(i);
                            i--;
                            HorizontalRunsC--;
                        }
                    }
                    //Vertical Runs
                    for (int j = 0; j < VerticalRunsC; j++)
                    {
                        if (RunLength.RemoveRunLength(VerticalRuns, (VRun)(VerticalRuns[j]), 7, 10, 40, 2) == true)
                        {
                            //Remove Run Length from the list
                            VerticalRuns.RemoveAt(j);
                            j--;
                            VerticalRunsC--;
                        }
                    }
                    // Make Lines
                    RunLength.MakeLineFromHRun(HorizontalRuns, ref HLines, 70, 50);
                    RunLength.MakeLineFromVRun(VerticalRuns, ref VLines, 50, 30);
                }
                else if (LineType.ToUpper() == "H")
                {
                    //Find Horizontal and vertical Runs
                    RunLength.FindHRun(bmData, ref HorizontalRuns, 15, true, 10, 10, 10, 10);

                    //Counters
                    int HorizontalRunsC = HorizontalRuns.Count;

                    //Horizontal Runs
                    for (int i = 0; i < HorizontalRunsC; i++)
                    {
                        if (RemoveRunLength(HorizontalRuns, (HRun)(HorizontalRuns[i]), 5, 10, 150, 2) == true)
                        {
                            //Remove Run Length from the list
                            HorizontalRuns.RemoveAt(i);
                            i--;
                            HorizontalRunsC--;
                        }
                    }

                    // Make Lines
                    RunLength.MakeLineFromHRun(HorizontalRuns, ref HLines, 70, 50);

                    #region MergeHLines

                    int HLinesC = HLines.Count;
                    for (int i = 0; i < HLinesC; i++)
                    {
                        for (int j = i + 1; j < HLinesC; j++)
                        {
                            if (((Line)HLines[i]).startY == ((Line)HLines[j]).startY)
                            {
                                if (((Line)HLines[i]).startX + ((Line)HLines[i]).height > ((Line)HLines[j]).startX)
                                {
                                    bool resConnect = false;
                                    int sX = 0;
                                    int h = 0;
                                    resConnect = ConnectHLines((Line)HLines[i], (Line)HLines[j], ref sX, ref h);
                                    if (resConnect == true)
                                    {
                                        ((Line)HLines[i]).startX = sX;
                                        ((Line)HLines[i]).height = h;
                                        ((Line)HLines[j]).height = 0;
                                    }
                                }
                            }
                        }
                    }

                    #endregion MergeHLines
                }
                else if (LineType.ToUpper() == "V")
                {
                    //Find Horizontal and vertical Runs
                    RunLength.FindVRun(bmData, ref VerticalRuns, 7, Convert.ToUInt16(nWidth / 2), 10, Convert.ToUInt16(height / 8), Convert.ToUInt16(height / 8));

                    //Counters
                    int VerticalRunsC = VerticalRuns.Count;

                    //Vertical Runs
                    for (int j = 0; j < VerticalRunsC; j++)
                    {
                        if (RunLength.RemoveRunLength(VerticalRuns, (VRun)(VerticalRuns[j]), 7, 10, 40, 2) == true)
                        {
                            //Remove Run Length from the list
                            VerticalRuns.RemoveAt(j);
                            j--;
                            VerticalRunsC--;
                        }
                    }
                    // Make Lines
                    RunLength.MakeLineFromVRun(VerticalRuns, ref VLines, 50, 30);
                }

                //Free variables
                VerticalRuns.Clear();
                VerticalRuns = null;
                HorizontalRuns.Clear();
                HorizontalRuns = null;
                //HLines.Clear();
                HorizontalRuns = null;

                return 0;
            }
            catch (Exception)
            {
                //Return false
                //MessageBox.Show(exe.ToString ());
                return 2;
            }
            finally
            {
                if (src != null)
                    src.UnlockBits(bmData);
                //if (bresult !=null)
                //bresult.UnlockBits(bmresData);
                //bmData=null;
                //bmresData=null;
            }
        }

        internal static int FindAllHLines(Bitmap src, ref ArrayList HLines)
        {
            //Definitions
            //BitmapData bmresData=null;
            BitmapData bmData = null;

            try
            {
                //ColorPalette ColorPal;
                //ArrayList VerticalRuns = new ArrayList();
                ArrayList HorizontalRuns = new ArrayList();
                //HLines=new ArrayList ();
                //VLines=new ArrayList ();

                //assume binary image
                if (src.PixelFormat != PixelFormat.Format8bppIndexed)
                    return 1;

                bmData = src.LockBits(new Rectangle(0, 0, src.Width, src.Height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);

                //				//Make the new Bitmap White
                //				int stride = bmresData.Stride;
                //				System.IntPtr Scan0 = bmresData.Scan0;
                int nWidth = bmData.Width;
                int height = bmData.Height;

                //Find Horizontal and vertical Runs
                RunLength.FindHRun(bmData, ref HorizontalRuns, 15, true, 0, 0, 0, 0);

                //Counters
                int HorizontalRunsC = HorizontalRuns.Count;

                //Horizontal Runs
                for (int i = 0; i < HorizontalRunsC; i++)
                {
                    if (RemoveRunLength(HorizontalRuns, (HRun)(HorizontalRuns[i]), 5, 10, 150, 2) == true)
                    {
                        //Remove Run Length from the list
                        HorizontalRuns.RemoveAt(i);
                        i--;
                        HorizontalRunsC--;
                    }
                }
                HLines.Clear();
                // Make Lines
                RunLength.MakeLineFromHRun(HorizontalRuns, ref HLines, 70, 50);

                #region MergeHLines

                int HLinesC = HLines.Count;
                for (int i = 0; i < HLinesC; i++)
                {
                    for (int j = i + 1; j < HLinesC; j++)
                    {
                        if (((Line)HLines[i]).startY == ((Line)HLines[j]).startY)
                        {
                            if (((Line)HLines[i]).startX + ((Line)HLines[i]).height > ((Line)HLines[j]).startX)
                            {
                                bool resConnect = false;
                                int sX = 0;
                                int h = 0;
                                resConnect = ConnectHLines((Line)HLines[i], (Line)HLines[j], ref sX, ref h);
                                if (resConnect == true)
                                {
                                    ((Line)HLines[i]).startX = sX;
                                    ((Line)HLines[i]).height = h;
                                    ((Line)HLines[j]).height = 0;
                                }
                            }
                        }
                    }
                }

                #endregion MergeHLines

                HorizontalRuns.Clear();
                HorizontalRuns = null;
                //HLines.Clear();
                HorizontalRuns = null;

                return 0;
            }
            catch (Exception)
            {
                //Return false
                //MessageBox.Show(exe.ToString ());
                return 2;
            }
            finally
            {
                if (src != null)
                    src.UnlockBits(bmData);
                //if (bresult !=null)
                //bresult.UnlockBits(bmresData);
                //bmData=null;
                //bmresData=null;
            }
        }

        internal static int FindDashedLines(Bitmap src, ref Rectangle lineDashed)
        {
            BitmapData bmData;
            ArrayList HLines = new ArrayList();
            try
            {
                //ColorPalette ColorPal;
                ArrayList VerticalRuns = new ArrayList();
                ArrayList HorizontalRuns = new ArrayList();

                //assume binary image
                if (src.PixelFormat != PixelFormat.Format8bppIndexed)
                    return 1;

                bmData = src.LockBits(new Rectangle(0, 0, src.Width, src.Height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);

                int nWidth = bmData.Width;
                int height = bmData.Height;

                //Find Horizontal and vertical Runs
                RunLength.FindHRun(bmData, ref HorizontalRuns, 1, false, 0, 0, 0, 0);
                //get the longest dashed line:

                //Counters
                int HorizontalRunsC = HorizontalRuns.Count;
                int j = 0;
                //Horizontal Runs
                for (int i = 0; i < HorizontalRunsC; i++)
                {
                    if (IsWithinSameHorizon(HLines, (HRun)HorizontalRuns[i], ref j))//in row list j
                    {
                        //add to the previous runs
                        (HLines).Add(HorizontalRuns[i]);
                    }
                    else
                    {//make new line for it
                        ArrayList run = new ArrayList
                        {
                            HorizontalRuns[i]
                        };
                        HLines.Add(run);
                    }
                }
                //get the longest runs
                int count = 0;
                int index = -1;
                for (int i = 0; i < HLines.Count; i++)
                {
                    if (count < ((ArrayList)(HLines[i])).Count)
                    {
                        count = ((ArrayList)(HLines[i])).Count;
                        index = i;
                    }
                }
                //get the begining and end of the line.
                int stX = ((HRun)((ArrayList)(HLines[index]))[index]).c1;
                int endX = ((HRun)((ArrayList)(HLines[index]))[index]).c2;
                int y = ((HRun)((ArrayList)(HLines[index]))[index]).row;
                lineDashed.X = stX;
                lineDashed.Y = y;
                lineDashed.Width = endX - stX;
                lineDashed.Height = 1;

                return 0;
            }
            catch (Exception exe)
            {
                Console.WriteLine(exe.Message);
                return 2;
            }
        }

        private static bool IsWithinSameHorizon(ArrayList HRuns, HRun run, ref int ind)
        {
            int count = HRuns.Count;
            for (int i = 0; i < count; i++)
            {
                if (run.row == ((HRun)(HRuns[i])).row)
                {
                    ind = i;
                    return true;
                }
            }
            return false;
        }

        internal static int FindTemplateLines(Bitmap src, ref ArrayList HLinesFinal, int HLinesNumber, ref ArrayList VLinesFinal, int VLinesNumber)
        {
            BitmapData bmData = null;
            ArrayList VerticalRuns = new ArrayList();
            ArrayList HorizontalRuns = new ArrayList();
            ArrayList HLines = new ArrayList();
            ArrayList VLines = new ArrayList();
            //assume binary image
            if (src.PixelFormat != PixelFormat.Format8bppIndexed)
                return 1;

            try
            {
                bmData = src.LockBits(new Rectangle(0, 0, src.Width, src.Height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                int nWidth = bmData.Width;
                int height = bmData.Height;

                //Find Horizontal and vertical Runs
                RunLength.FindHRun(bmData, ref HorizontalRuns, 15, true, 0, 0, 0, 0);
                RunLength.FindVRun(bmData, ref VerticalRuns, 15, 0, 0, 0, 0);

                //Counters
                int HorizontalRunsC = HorizontalRuns.Count;
                int VerticalRunsC = VerticalRuns.Count;
                //Horizontal Runs
                for (int i = 0; i < HorizontalRunsC; i++)
                {
                    if (RemoveRunLength(HorizontalRuns, (HRun)(HorizontalRuns[i]), 5, 10, 150, 2) == true)
                    {
                        //Remove Run Length from the list
                        HorizontalRuns.RemoveAt(i);
                        i--;
                        HorizontalRunsC--;
                    }
                }
                //Vertical Runs
                for (int j = 0; j < VerticalRunsC; j++)
                {
                    if (RunLength.RemoveRunLength(VerticalRuns, (VRun)(VerticalRuns[j]), 7, 10, 150, 2) == true)
                    {
                        //Remove Run Length from the list
                        VerticalRuns.RemoveAt(j);
                        j--;
                        VerticalRunsC--;
                    }
                }
                // Make Lines
                RunLength.MakeLineFromHRun(HorizontalRuns, ref HLines, 70, 50);
                RunLength.MakeLineFromVRun2(VerticalRuns, ref VLines, 70, 50);

                int res = ComposeMajorLines_Template(ref HLinesFinal, HLines, HLinesNumber);
                int res1 = ComposeMajorLines_Template(ref VLinesFinal, VLines, VLinesNumber);
                if (res != 0 || res1 != 0)
                {
                    return 2;
                }
                return 0;
            }
            catch
            {
                return 2;
            }
            finally
            {
                if (src != null)
                    src.UnlockBits(bmData);
            }
        }

        /// <summary>
        /// composes the lines into (maxLinesLeft) number of lines
        /// </summary>
        /// <param name="LinesFinal">the returned main categories</param>
        /// <param name="Lines">the passed lines</param>
        /// <param name="maxLinesLeft">the required number of categories</param>
        /// <returns></returns>
        private static int ComposeMajorLines_Template(ref ArrayList LinesFinal2, ArrayList Lines, int maxLinesLeft)
        {
            ArrayList LinesFinal;
            try
            {
                //now get the major lines:
                int count = Lines.Count;
                LinesFinal = new ArrayList();
                bool bNewGroup = true;
                for (int i = 0; i < count; i++)
                {
                    //if applies to existing one append
                    for (int j = 0; j < LinesFinal.Count; j++)
                    {
                        if (Math.Abs(((Line)Lines[i]).startY - ((Line)LinesFinal[j]).startY) < 15)
                        {
                            //modify the existing one
                            ((Line)LinesFinal[j]).startX = Math.Min(((Line)Lines[i]).startX, ((Line)LinesFinal[j]).startX);
                            ((Line)LinesFinal[j]).startY = Math.Min(((Line)Lines[i]).startY, ((Line)LinesFinal[j]).startY);
                            ((Line)LinesFinal[j]).width = Math.Abs(((Line)Lines[i]).width - ((Line)LinesFinal[j]).width);
                            ((Line)LinesFinal[j]).height = Math.Max(((Line)Lines[i]).height, ((Line)LinesFinal[j]).height);
                            bNewGroup = false;
                        }
                        else
                        {
                            bNewGroup = true;
                        }
                    }
                    if (bNewGroup == true)
                    {
                        LinesFinal.Add((Line)(Lines[i]));
                    }
                }
                //temp
                for (int i = 0; i < LinesFinal.Count; i++)
                {
                    if (((Line)LinesFinal[i]).height < 200)
                    {
                        LinesFinal.RemoveAt(i);
                    }
                }
                //keep the longest maxLinesLeft ones:
                LinesFinal.Sort(new LineHeightComparer(SortDirection.Desc));
                for (int i = 0; i < maxLinesLeft; i++)
                {
                    LinesFinal2.Add((Line)LinesFinal[i]);
                }
                return 0;
            }
            catch
            {
                return 2;
            }
            finally
            {
            }
        }

        public static int FindLinesAndGaps(Bitmap src, ref ArrayList YLines, ref ArrayList GLines, ref ArrayList XLines, string LineType)
        {
            try
            {
                int bmpH = src.Height;
                int bmpW = src.Width;
                ArrayList HLines = new ArrayList();
                ArrayList VLines = new ArrayList();
                int res = RunLength.FindLines2(src, ref HLines, ref VLines, "H");
                if (res == 1)
                {
                    return 1;
                }
                else if (res == 2)
                {
                    return 2;
                }
                int HLinesC = HLines.Count;
                for (int j = 0; j < HLinesC; j++)
                {
                    if (((Line)HLines[j]).height != 0 && ((Line)HLines[j]).width != 0
                        )
                    {
                        if (j > 0 &&
                            Math.Abs(((Line)HLines[j]).startY - ((Line)HLines[j - 1]).startY) > 5)
                        {
                            YLines.Add((double)((Line)HLines[j]).startY / (double)bmpH);
                            XLines.Add(((double)((Line)HLines[j]).startX + (double)((Line)HLines[j]).height) / (double)bmpW);
                        }
                        else if (j < 1)
                        {
                            YLines.Add((double)((Line)HLines[j]).startY / (double)bmpH);
                            XLines.Add(((double)((Line)HLines[j]).startX + (double)((Line)HLines[j]).height) / (double)bmpW);
                        }
                    }
                }
                //fill the gaps
                int YLinesC = YLines.Count;
                for (int i = 1; i < YLinesC; i++)
                {
                    GLines.Add((double)YLines[i] - (double)YLines[i - 1]);
                }
                return 0;
            }
            catch
            {
                return 2;
            }
        }

        internal static int FindLines2(Bitmap src, ref ArrayList HLines, ref ArrayList VLines, string LineType)
        {
            BitmapData bmData = null;

            try
            {
                //ColorPalette ColorPal;
                ArrayList VerticalRuns = new ArrayList();
                ArrayList HorizontalRuns = new ArrayList();

                //assume binary image
                if (src.PixelFormat != PixelFormat.Format8bppIndexed)
                    return 1;

                bmData = src.LockBits(new Rectangle(0, 0, src.Width, src.Height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);

                //				//Make the new Bitmap White
                //				int stride = bmresData.Stride;
                //				System.IntPtr Scan0 = bmresData.Scan0;
                int nWidth = bmData.Width;
                int height = bmData.Height;

                if (LineType.ToUpper() == "B")
                {
                    //Find Horizontal and vertical Runs
                    RunLength.FindHRun(bmData, ref HorizontalRuns, 15, true, 50, 50, 150, 200);
                    RunLength.FindVRun(bmData, ref VerticalRuns, 15, 50, 50, 150, 200);

                    //Counters
                    int HorizontalRunsC = HorizontalRuns.Count;
                    int VerticalRunsC = VerticalRuns.Count;
                    //Horizontal Runs
                    for (int i = 0; i < HorizontalRunsC; i++)
                    {
                        if (RemoveRunLength(HorizontalRuns, (HRun)(HorizontalRuns[i]), 5, 10, 150, 2) == true)
                        {
                            //Remove Run Length from the list
                            HorizontalRuns.RemoveAt(i);
                            i--;
                            HorizontalRunsC--;
                        }
                    }
                    //Vertical Runs
                    for (int j = 0; j < VerticalRunsC; j++)
                    {
                        if (RunLength.RemoveRunLength(VerticalRuns, (VRun)(VerticalRuns[j]), 7, 10, 150, 2) == true)
                        {
                            //Remove Run Length from the list
                            VerticalRuns.RemoveAt(j);
                            j--;
                            VerticalRunsC--;
                        }
                    }
                    // Make Lines
                    RunLength.MakeLineFromHRun(HorizontalRuns, ref HLines, 70, 50);
                    RunLength.MakeLineFromVRun2(VerticalRuns, ref VLines, 70, 50);

                    //exchange the x and y
                    int VLinesC = VLines.Count;
                }
                else if (LineType.ToUpper() == "H")
                {
                    //Find Horizontal and vertical Runs
                    RunLength.FindHRun(bmData, ref HorizontalRuns, 15, true, 10, 10, 10, 10);

                    //Counters
                    int HorizontalRunsC = HorizontalRuns.Count;

                    //Horizontal Runs
                    for (int i = 0; i < HorizontalRunsC; i++)
                    {
                        if (RemoveRunLength(HorizontalRuns, (HRun)(HorizontalRuns[i]), 5, 10, 150, 2) == true)
                        {
                            //Remove Run Length from the list
                            HorizontalRuns.RemoveAt(i);
                            i--;
                            HorizontalRunsC--;
                        }
                    }

                    // Make Lines
                    RunLength.MakeLineFromHRun(HorizontalRuns, ref HLines, 70, 50);
                }
                else if (LineType.ToUpper() == "V")
                {
                    //Find Horizontal and vertical Runs
                    RunLength.FindVRun(bmData, ref VerticalRuns, 7, Convert.ToUInt16(nWidth / 2), 10, Convert.ToUInt16(height / 8), Convert.ToUInt16(height / 8));

                    //Counters
                    int VerticalRunsC = VerticalRuns.Count;

                    //Vertical Runs
                    for (int j = 0; j < VerticalRunsC; j++)
                    {
                        if (RunLength.RemoveRunLength(VerticalRuns, (VRun)(VerticalRuns[j]), 7, 10, 40, 2) == true)
                        {
                            //Remove Run Length from the list
                            VerticalRuns.RemoveAt(j);
                            j--;
                            VerticalRunsC--;
                        }
                    }
                    // Make Lines
                    RunLength.MakeLineFromVRun(VerticalRuns, ref VLines, 50, 30);
                }

                //Free variables
                VerticalRuns.Clear();
                VerticalRuns = null;
                HorizontalRuns.Clear();
                HorizontalRuns = null;
                //HLines.Clear();
                HorizontalRuns = null;

                return 0;
            }
            catch (Exception)
            {
                return 2;
            }
            finally
            {
                if (src != null)
                    src.UnlockBits(bmData);
            }
        }

        internal static int FindFormLines(Bitmap src, ref ArrayList HLines, ref ArrayList VLines, string LineType)
        {
            //Definitions
            //BitmapData bmresData=null;
            BitmapData bmData = null;

            try
            {
                //ColorPalette ColorPal;
                ArrayList VerticalRuns = new ArrayList();
                ArrayList HorizontalRuns = new ArrayList();
                //HLines=new ArrayList ();
                //VLines=new ArrayList ();

                //assume binary image
                if (src.PixelFormat != PixelFormat.Format8bppIndexed)
                    return 1;

                bmData = src.LockBits(new Rectangle(0, 0, src.Width, src.Height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);

                //				//Make the new Bitmap White
                //				int stride = bmresData.Stride;
                //				System.IntPtr Scan0 = bmresData.Scan0;
                int nWidth = bmData.Width;
                int height = bmData.Height;

                if (LineType.ToUpper() == "B")
                {
                    //Find Horizontal and vertical Runs
                    RunLength.FindHRun(bmData, ref HorizontalRuns, 15, true, 50, 50, 50, 50);
                    RunLength.FindVRun(bmData, ref VerticalRuns, 15, 50, 50, 50, 50);

                    //Counters
                    int HorizontalRunsC = HorizontalRuns.Count;
                    int VerticalRunsC = VerticalRuns.Count;
                    //Horizontal Runs
                    for (int i = 0; i < HorizontalRunsC; i++)
                    {
                        if (RemoveRunLength(HorizontalRuns, (HRun)(HorizontalRuns[i]), 5, 10, 150, 2) == true)
                        {
                            //Remove Run Length from the list
                            HorizontalRuns.RemoveAt(i);
                            i--;
                            HorizontalRunsC--;
                        }
                    }
                    //Vertical Runs
                    for (int j = 0; j < VerticalRunsC; j++)
                    {
                        if (RunLength.RemoveRunLength(VerticalRuns, (VRun)(VerticalRuns[j]), 7, 10, 40, 2) == true)
                        {
                            //Remove Run Length from the list
                            VerticalRuns.RemoveAt(j);
                            j--;
                            VerticalRunsC--;
                        }
                    }
                    // Make Lines
                    RunLength.MakeLineFromHRun(HorizontalRuns, ref HLines, 70, 50);
                    RunLength.MakeLineFromVRun2(VerticalRuns, ref VLines, 70, 50);
                }
                else if (LineType.ToUpper() == "H")
                {
                    //Find Horizontal and vertical Runs
                    RunLength.FindHRun(bmData, ref HorizontalRuns, 15, true, 10, 10, 10, 10);

                    //Counters
                    int HorizontalRunsC = HorizontalRuns.Count;

                    //Horizontal Runs
                    for (int i = 0; i < HorizontalRunsC; i++)
                    {
                        if (RemoveRunLength(HorizontalRuns, (HRun)(HorizontalRuns[i]), 5, 10, 150, 2) == true)
                        {
                            //Remove Run Length from the list
                            HorizontalRuns.RemoveAt(i);
                            i--;
                            HorizontalRunsC--;
                        }
                    }

                    // Make Lines
                    RunLength.MakeLineFromHRun(HorizontalRuns, ref HLines, 70, 50);
                }
                else if (LineType.ToUpper() == "V")
                {
                    //Find Horizontal and vertical Runs
                    RunLength.FindVRun(bmData, ref VerticalRuns, 7, Convert.ToUInt16(nWidth / 2), 10, Convert.ToUInt16(height / 8), Convert.ToUInt16(height / 8));

                    //Counters
                    int VerticalRunsC = VerticalRuns.Count;

                    //Vertical Runs
                    for (int j = 0; j < VerticalRunsC; j++)
                    {
                        if (RunLength.RemoveRunLength(VerticalRuns, (VRun)(VerticalRuns[j]), 7, 10, 40, 2) == true)
                        {
                            //Remove Run Length from the list
                            VerticalRuns.RemoveAt(j);
                            j--;
                            VerticalRunsC--;
                        }
                    }
                    // Make Lines
                    RunLength.MakeLineFromVRun(VerticalRuns, ref VLines, 50, 30);
                }

                //Free variables
                VerticalRuns.Clear();
                VerticalRuns = null;
                HorizontalRuns.Clear();
                HorizontalRuns = null;
                //HLines.Clear();
                HorizontalRuns = null;

                return 0;
            }
            catch (Exception)
            {
                //Return false
                //MessageBox.Show(exe.ToString ());
                return 2;
            }
            finally
            {
                if (src != null)
                    src.UnlockBits(bmData);
                //if (bresult !=null)
                //bresult.UnlockBits(bmresData);
                //bmData=null;
                //bmresData=null;
            }
        }

        public static int FindHLinesPoints(Bitmap src, ref ArrayList HLinesPoints,
            int startX, int endX, int startY, int endY, int HSeedLength,
            int HThreshold, int VThreshold, int LAddedThreshold, int LNeglectedRunLength)
        {
            //Definitions
            BitmapData bmData = null;
            ArrayList HLines = new ArrayList();
            try
            {
                //definitions:
                ArrayList HorizontalRuns = new ArrayList();

                //assume binary image
                if (src.PixelFormat != PixelFormat.Format8bppIndexed)
                    return 1;

                //lock data:
                bmData = src.LockBits(new Rectangle(0, 0, src.Width, src.Height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);

                //width and height:
                int nWidth = bmData.Width;
                int height = bmData.Height;

                //Find Horizontal Runs
                RunLength.FindHRun(bmData, ref HorizontalRuns, LNeglectedRunLength, true, startX, endX, startY, endY);

                //Counters
                int HorizontalRunsC = HorizontalRuns.Count;

                //Horizontal Runs
                for (int i = 0; i < HorizontalRunsC; i++)
                {
                    if (RemoveRunLength(HorizontalRuns, (HRun)(HorizontalRuns[i]), 5, HThreshold, HSeedLength, VThreshold) == true)
                    {
                        //Remove Run Length from the list
                        HorizontalRuns.RemoveAt(i);
                        i--;
                        HorizontalRunsC--;
                    }
                }
                // Make Lines
                RunLength.MakeLineFromHRun(HorizontalRuns, ref HLines, 70, LAddedThreshold);

                #region Tolerate HLines

                //refinement step:
                //1. connect HLines

                #region MergeHLines

                int HLinesC = HLines.Count;
                for (int i = 0; i < HLinesC; i++)
                {
                    for (int j = i + 1; j < HLinesC; j++)
                    {
                        if ((Math.Abs(((Line)HLines[i]).startY - ((Line)HLines[j]).startY) < 2) // ||
                            && ((Line)HLines[i]).height != 0 && i != j)
                        {
                            if (Math.Abs(((Line)HLines[i]).startX + ((Line)HLines[i]).height - ((Line)HLines[j]).startX) < HThreshold ||
                                    Math.Abs(((Line)HLines[j]).startX + ((Line)HLines[j]).height - ((Line)HLines[i]).startX) < HThreshold ||

                                    (((Line)HLines[i]).startX < ((Line)HLines[j]).startX && ((Line)HLines[j]).startX < ((Line)HLines[i]).startX + ((Line)HLines[i]).height) ||
                                    (((Line)HLines[j]).startX < ((Line)HLines[i]).startX && ((Line)HLines[i]).startX < ((Line)HLines[j]).startX + ((Line)HLines[j]).height)
                                    )
                            {
                                bool resConnect = false;
                                int sX = 0;
                                int h = 0;
                                resConnect = ConnectHLines((Line)HLines[i], (Line)HLines[j], ref sX, ref h);
                                if (resConnect == true)
                                {
                                    ((Line)HLines[i]).startX = sX;
                                    ((Line)HLines[i]).height = h;
                                    ((Line)HLines[j]).height = 0;   //to be removed
                                }
                            }
                        }
                    }
                }

                #endregion MergeHLines

                int thresh = nWidth / 5;
                //2.Remove small lines
                for (int i = 0; i < HLines.Count; i++)
                {
                    if (((Line)HLines[i]).height <= 15 || (((Line)HLines[i]).width <= 1 && ((Line)HLines[i]).height < thresh))
                    {
                        HLines.RemoveAt(i);
                        i--;
                    }
                }

                //3.Fit the Hlines
                HLinesC = HLines.Count;
                Line pLine;
                for (int i = 0; i < HLinesC; i++)
                {
                    pLine = (Line)HLines[i];
                    //Fit the line vertically:
                    int resLine = FitLine(bmData, ref pLine);
                    //Extend the line horizontally:
                    int resExtend = ExtendLine(bmData, ref pLine);
                    //int res = CorrectLineTerminals(bmData, ref pLine);

                    HLines[i] = (Line)pLine;
                }
                HLinesC = HLines.Count;
                for (int i = 0; i < HLinesC; i++)
                {
                    for (int j = i + 1; j < HLinesC; j++)
                    {
                        if ((Math.Abs(((Line)HLines[i]).startY - ((Line)HLines[j]).startY) < 2) // ||
                            && ((Line)HLines[i]).height != 0 && i != j)
                        {
                            if (Math.Abs(((Line)HLines[i]).startX + ((Line)HLines[i]).height - ((Line)HLines[j]).startX) < HThreshold ||
                                    Math.Abs(((Line)HLines[j]).startX + ((Line)HLines[j]).height - ((Line)HLines[i]).startX) < HThreshold ||

                                    (((Line)HLines[i]).startX < ((Line)HLines[j]).startX && ((Line)HLines[j]).startX < ((Line)HLines[i]).startX + ((Line)HLines[i]).height) ||
                                    (((Line)HLines[j]).startX < ((Line)HLines[i]).startX && ((Line)HLines[i]).startX < ((Line)HLines[j]).startX + ((Line)HLines[j]).height)
                                    )
                            {
                                bool resConnect = false;
                                int sX = 0;
                                int h = 0;
                                resConnect = ConnectHLines((Line)HLines[i], (Line)HLines[j], ref sX, ref h);
                                if (resConnect == true)
                                {
                                    ((Line)HLines[i]).startX = sX;
                                    ((Line)HLines[i]).height = h;
                                    ((Line)HLines[j]).height = 0;   //to be removed
                                }
                            }
                        }
                    }
                }
                for (int i = 0; i < HLines.Count; i++)
                {
                    if (((Line)HLines[i]).height <= 15 || (((Line)HLines[i]).width <= 1 && ((Line)HLines[i]).height < thresh))
                    {
                        HLines.RemoveAt(i);
                        i--;
                    }
                }

                #endregion Tolerate HLines

                //Finding Points:
                DateTime d1 = DateTime.Now;
                HLinesC = HLines.Count;

                for (int i = 0; i < HLinesC; i++)
                {
                    ArrayList pointsFinal = new ArrayList();

                    //1.Core points
                    //int result = GetCorePoints(bmData, ref pointsFinal, ((Line)HLines[i]));

                    ArrayList HLineRuns = new ArrayList();
                    ArrayList HRunsFinal = new ArrayList();

                    //get the boundaries:
                    int x1 = (((Line)HLines[i]).startX);
                    int y1 = (((Line)HLines[i]).startY - 15);
                    int x2 = (((Line)HLines[i]).startX + ((Line)HLines[i]).height);
                    int y2 = (((Line)HLines[i]).startY + ((Line)HLines[i]).width + 15);
                    int LineLength = ((Line)HLines[i]).height;

                    if (x1 < 0)
                    {
                        x1 = 0;
                    }
                    if (y1 < 0)
                    {
                        y1 = 0;
                    }
                    if (x2 > bmData.Width)
                    {
                        x2 = bmData.Width;
                    }
                    if (y2 > bmData.Height)
                    {
                        y2 = bmData.Height;
                    }

                    RunLength.FindHRun(bmData, ref HLineRuns, LNeglectedRunLength, true, x1, bmData.Width - x2, y1, bmData.Height - y2);
                    //go through these lines
                    //start from the seed line and go up and down:
                    int seedY = (((Line)HLines[i]).startY + 1) < bmData.Height ? (((Line)HLines[i]).startY + 1) : bmData.Height;
                    int HLineRunsC = HLineRuns.Count;

                    //accummulate RLs
                    for (int k = 0; k < HLineRunsC; k++)
                    {
                        if (Math.Abs(((HRun)HLineRuns[k]).row - seedY) <= 5 && Math.Abs(((HRun)HLineRuns[k]).row - seedY) > 2
                            && ((HRun)HLineRuns[k]).len > (LineLength / 5))
                        {
                            //near the seed line.
                            HRunsFinal.Add(((HRun)HLineRuns[k]));
                        }
                        else if (Math.Abs(((HRun)HLineRuns[k]).row - seedY) < 8 && Math.Abs(((HRun)HLineRuns[k]).row - seedY) > 5
                            && ((HRun)HLineRuns[k]).len > (LineLength / 3))
                        {
                            //near the seed line.
                            HRunsFinal.Add(((HRun)HLineRuns[k]));
                        }
                        else if (Math.Abs(((HRun)HLineRuns[k]).row - seedY) < 15 && Math.Abs(((HRun)HLineRuns[k]).row - seedY) > 2
                            && HLinePixelsDensity(bmData, ((HRun)HLineRuns[k]).c1, ((Line)HLines[i])) < 20)
                        {
                            //near the seed line.
                            HRunsFinal.Add(((HRun)HLineRuns[k]));
                        }
                    }

                    //get the points <=2
                    int resul = GetCorePointsH(bmData, ref pointsFinal, ((Line)HLines[i]));
                    //get the points
                    int HRunsFinalC = HRunsFinal.Count;
                    for (int k = 0; k < HRunsFinalC; k++)
                    {
                        int c1 = ((HRun)HRunsFinal[k]).c1;
                        int c2 = ((HRun)HRunsFinal[k]).c2;

                        for (int j = c1; j < c2; j++)
                        {
                            Point pt = new Point(j, ((HRun)HRunsFinal[k]).row);
                            pointsFinal.Add(pt);
                        }
                    }

                    HLinesPoints.Add((ArrayList)pointsFinal);
                    //pointsFinal.Clear();
                }

                DateTime d2 = DateTime.Now;
                TimeSpan sp = d2 - d1;
                //Free variables
                HorizontalRuns.Clear();
                HorizontalRuns = null;
                HorizontalRuns = null;

                return 0;
            }
            catch
            {
                return 2;
            }
            finally
            {
                if (src != null)
                    src.UnlockBits(bmData);
            }
        }

        public static int HLinePixelsDensity(BitmapData bmData, int xHRun, Line RectLine)
        {
            try
            {
                int stride = bmData.Stride;
                System.IntPtr Scan0 = bmData.Scan0;
                byte* p = (byte*)(void*)Scan0;
                int nOffset = stride - bmData.Width;
                int density = 0;

                int stx = xHRun;
                int endX = stx + 15 < bmData.Width ? stx + 15 : bmData.Width;
                int stY = RectLine.startY;
                int endY = RectLine.startY + 3 < bmData.Height ? RectLine.startY + 3 : bmData.Height;

                for (int x = stx; x < endX; x++)
                {
                    for (int y = stY; y < endY; y++)
                    {
                        // if black pixel ==> take it.
                        if (p[(y * stride) + x] == 0)
                        {
                            density++;
                        }
                    }
                }
                density *= 2; //100%

                return density;
            }
            catch
            {
                return 0;
            }
        }

        public static int VLinePixelsDensity(BitmapData bmData, int yVRun, Line RectLine)
        {
            try
            {
                int stride = bmData.Stride;
                System.IntPtr Scan0 = bmData.Scan0;
                byte* p = (byte*)(void*)Scan0;
                int nOffset = stride - bmData.Width;
                int density = 0;

                int stx = RectLine.startX;
                int endX = stx + RectLine.width < bmData.Width ? stx + RectLine.width : bmData.Width;
                int stY = yVRun;
                int endY = stY + 15 < bmData.Height ? stY + 15 : bmData.Height;

                for (int x = stx; x < endX; x++)
                {
                    for (int y = stY; y < endY; y++)
                    {
                        // if black pixel ==> take it.
                        if (p[(y * stride) + x] == 0)
                        {
                            density++;
                        }
                    }
                }
                density /= 2; //100%

                return density;
            }
            catch
            {
                return 0;
            }
        }

        public static int GetCorePointsH(BitmapData bmData, ref ArrayList pointsFinal, Line RefLine)
        {
            try
            {
                int stride = bmData.Stride;
                System.IntPtr Scan0 = bmData.Scan0;
                byte* p = (byte*)(void*)Scan0;
                int nOffset = stride - bmData.Width;

                int stx = RefLine.startX - 1 > 0 ? RefLine.startX - 1 : RefLine.startX;
                int endX = RefLine.startX + RefLine.height + 1 < bmData.Width ? (RefLine.startX + RefLine.height + 1) : bmData.Width;
                int stY = RefLine.startY - 1 > 0 ? RefLine.startY - 1 : RefLine.startY;
                int endY = RefLine.startY + RefLine.width < bmData.Height ? RefLine.startY + RefLine.width : bmData.Height;

                for (int x = stx; x <= endX; x++)
                {
                    for (int y = stY; y <= endY; y++)
                    {
                        // if black pixel ==> take it.
                        if (p[(y * stride) + x] == 0)
                        {
                            Point p1 = new Point(x, y);
                            pointsFinal.Add(p1);
                        }
                    }
                }
                return 0;
            }
            catch
            {
                return 2;
            }
        }

        public static int GetCorePointsV(BitmapData bmData, ref ArrayList pointsFinal, Line RefLine)
        {
            try
            {
                int stride = bmData.Stride;
                System.IntPtr Scan0 = bmData.Scan0;
                byte* p = (byte*)(void*)Scan0;
                int nOffset = stride - bmData.Width;

                int stx = RefLine.startX - 1 > 0 ? RefLine.startX - 1 : RefLine.startX;
                int endX = (RefLine.startX + RefLine.width + 1) < bmData.Width ? (RefLine.startX + RefLine.width + 1) : bmData.Width;
                int stY = RefLine.startY - 1 > 0 ? RefLine.startY - 1 : RefLine.startY;
                int endY = (RefLine.startY + RefLine.height + 1) < bmData.Height ? (RefLine.startY + RefLine.height + 1) : bmData.Height;

                for (int x = stx; x <= endX; x++)
                {
                    for (int y = stY; y <= endY; y++)
                    {
                        if (p[(y * stride) + x] == 0)
                        {
                            Point p1 = new Point(x, y);
                            pointsFinal.Add(p1);
                        }
                    }
                }
                return 0;
            }
            catch
            {
                return 2;
            }
        }

        /// <summary>
        /// Finds Horizontal and Vertical lines inside a binary image.
        /// </summary>
        /// <param name="src">The source 8 bit B/W image</param>
        /// <param name="HLines">The returned array of Horizontal lines</param>
        /// <param name="VLines">The returned array of Vertical lines</param>
        /// <param name="startX">x value to start detection from,recommended value 10</param>
        /// <param name="endX">x value to end detection at(Threshold from end),recommended value 20</param>
        /// <param name="startY">y value to start detection from,recommended value 10</param>
        /// <param name="endY">y value to end detection at(Threshold from end),recommended value 20</param>
        /// <param name="HSeedLength">seed of horizontal line,recommended value 150</param>
        /// <param name="VSeedLength">seed of vertical line,recommended value 40</param>
        /// <param name="HThreshold">maximum horizontal length between Run Lengths,recommended value 10</param>
        /// <param name="VThreshold">maximum vertical length between Run Lengths,recommended value 2</param>
        /// <param name="LAddedThreshold">Accepted length for RL to be addded,recommended value 50</param>
        /// <param name="LNeglectedRunLength">Threshold for RL's that are neglected,recommended value 15</param>
        /// <returns>
        /// 0	:success
        /// 1	:invalid Pixel Format
        /// 2	:Fail</returns>
        public static int FindHVLines(Bitmap src, ref ArrayList HLines, ref ArrayList VLines,// string LineType,
            int startX, int endX, int startY, int endY, int HSeedLength, int VSeedLength,
            int HThreshold, int VThreshold, int LAddedThreshold, int LNeglectedRunLength)
        {
            //Definitions
            BitmapData bmData = null;
            try
            {
                //definitions:
                ArrayList VerticalRuns = new ArrayList();
                ArrayList HorizontalRuns = new ArrayList();

                //assume binary image
                if (src.PixelFormat != PixelFormat.Format8bppIndexed)
                    return 1;

                //lock data:
                bmData = src.LockBits(new Rectangle(0, 0, src.Width, src.Height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);

                //width and height:
                int nWidth = bmData.Width;
                int height = bmData.Height;

                //Both H and V lines:
                //if (LineType.ToUpper() == "B")
                //{
                //Find Horizontal and vertical Runs
                RunLength.FindHRun(bmData, ref HorizontalRuns, LNeglectedRunLength, true, startX, endX, startY, endY);
                RunLength.FindVRun(bmData, ref VerticalRuns, LNeglectedRunLength, startX, endX, startY, endY);

                //Counters
                int HorizontalRunsC = HorizontalRuns.Count;
                int VerticalRunsC = VerticalRuns.Count;

                //Horizontal Runs
                for (int i = 0; i < HorizontalRunsC; i++)
                {
                    if (RemoveRunLength(HorizontalRuns, (HRun)(HorizontalRuns[i]), 5, HThreshold, HSeedLength, VThreshold) == true)
                    {
                        //Remove Run Length from the list
                        HorizontalRuns.RemoveAt(i);
                        i--;
                        HorizontalRunsC--;
                    }
                }

                //Vertical Runs
                for (int j = 0; j < VerticalRunsC; j++)
                {
                    if (RunLength.RemoveRunLength(VerticalRuns, (VRun)(VerticalRuns[j]), 7, HThreshold, VSeedLength, VThreshold) == true)
                    {
                        //Remove Run Length from the list
                        VerticalRuns.RemoveAt(j);
                        j--;
                        VerticalRunsC--;
                    }
                }

                // Make Lines
                RunLength.MakeLineFromHRun(HorizontalRuns, ref HLines, 70, LAddedThreshold);
                RunLength.MakeLineFromVRun2(VerticalRuns, ref VLines, 50, LAddedThreshold);

                #region Tolerate HLines

                //refinement step:
                //1. connect HLines

                #region MergeHLines

                int HLinesC = HLines.Count;
                for (int i = 0; i < HLinesC; i++)
                {
                    for (int j = i + 1; j < HLinesC; j++)
                    {
                        if ((Math.Abs(((Line)HLines[i]).startY - ((Line)HLines[j]).startY) < 2) // ||
                                                                                                //(((Line)HLines[j]).startY > ((Line)HLines[i]).startY && ((Line)HLines[j]).startY < ((Line)HLines[i]).startY + ((Line)HLines[i]).width)
                            && ((Line)HLines[i]).height != 0 && i != j)
                        //if (((Line)HLines[i]).startY == ((Line)HLines[j]).startY && ((Line)HLines[i]).height != 0)
                        {
                            if (Math.Abs(((Line)HLines[i]).startX + ((Line)HLines[i]).height - ((Line)HLines[j]).startX) < HThreshold ||
                                    Math.Abs(((Line)HLines[j]).startX + ((Line)HLines[j]).height - ((Line)HLines[i]).startX) < HThreshold ||

                                    (((Line)HLines[i]).startX < ((Line)HLines[j]).startX && ((Line)HLines[j]).startX < ((Line)HLines[i]).startX + ((Line)HLines[i]).height) ||
                                    (((Line)HLines[j]).startX < ((Line)HLines[i]).startX && ((Line)HLines[i]).startX < ((Line)HLines[j]).startX + ((Line)HLines[j]).height)
                                    )
                            {
                                bool resConnect = false;
                                int sX = 0;
                                int h = 0;
                                resConnect = ConnectHLines((Line)HLines[i], (Line)HLines[j], ref sX, ref h);
                                if (resConnect == true)
                                {
                                    ((Line)HLines[i]).startX = sX;
                                    ((Line)HLines[i]).height = h;
                                    ((Line)HLines[j]).height = 0;   //to be removed
                                }
                            }
                        }
                    }
                }

                #endregion MergeHLines

                int thresh = nWidth / 5;
                //2.Remove small lines
                for (int i = 0; i < HLines.Count; i++)
                {
                    if (((Line)HLines[i]).height <= 15 || (((Line)HLines[i]).width <= 1 && ((Line)HLines[i]).height < thresh))
                    {
                        HLines.RemoveAt(i);
                        i--;
                    }
                }

                /////////////////////////modified 6 jan,2007
                //3.Fit the Hlines
                HLinesC = HLines.Count;
                Line pLine;
                for (int i = 0; i < HLinesC; i++)
                {
                    pLine = (Line)HLines[i];
                    //Fit the line vertically:
                    int resLine = FitLine(bmData, ref pLine);
                    //Extend the line horizontally:
                    int resExtend = ExtendLine(bmData, ref pLine);
                    //int res = CorrectLineTerminals(bmData, ref pLine);

                    HLines[i] = (Line)pLine;
                }
                HLinesC = HLines.Count;
                for (int i = 0; i < HLinesC; i++)
                {
                    for (int j = i + 1; j < HLinesC; j++)
                    {
                        if ((Math.Abs(((Line)HLines[i]).startY - ((Line)HLines[j]).startY) < 2) // ||
                                                                                                //(((Line)HLines[j]).startY > ((Line)HLines[i]).startY && ((Line)HLines[j]).startY < ((Line)HLines[i]).startY + ((Line)HLines[i]).width)
                            && ((Line)HLines[i]).height != 0 && i != j)
                        //if (((Line)HLines[i]).startY == ((Line)HLines[j]).startY && ((Line)HLines[i]).height != 0)
                        {
                            if (Math.Abs(((Line)HLines[i]).startX + ((Line)HLines[i]).height - ((Line)HLines[j]).startX) < HThreshold ||
                                    Math.Abs(((Line)HLines[j]).startX + ((Line)HLines[j]).height - ((Line)HLines[i]).startX) < HThreshold ||

                                    (((Line)HLines[i]).startX < ((Line)HLines[j]).startX && ((Line)HLines[j]).startX < ((Line)HLines[i]).startX + ((Line)HLines[i]).height) ||
                                    (((Line)HLines[j]).startX < ((Line)HLines[i]).startX && ((Line)HLines[i]).startX < ((Line)HLines[j]).startX + ((Line)HLines[j]).height)
                                    )
                            {
                                bool resConnect = false;
                                int sX = 0;
                                int h = 0;
                                resConnect = ConnectHLines((Line)HLines[i], (Line)HLines[j], ref sX, ref h);
                                if (resConnect == true)
                                {
                                    ((Line)HLines[i]).startX = sX;
                                    ((Line)HLines[i]).height = h;
                                    ((Line)HLines[j]).height = 0;   //to be removed
                                }
                            }
                        }
                    }
                }
                for (int i = 0; i < HLines.Count; i++)
                {
                    if (((Line)HLines[i]).height <= 15 || (((Line)HLines[i]).width <= 1 && ((Line)HLines[i]).height < thresh))
                    //if (((Line)HLines[i]).height <= 15 || ((Line)HLines[i]).width <= 1)
                    {
                        HLines.RemoveAt(i);
                        i--;
                    }
                }

                #endregion Tolerate HLines

                ///////////////////////
                //modified 8 jan,2007

                #region Tolerate VLines

                // A. Vertical Lines:
                int temp = 0;
                int VLinesCount = VLines.Count;
                for (int i = 0; i < VLinesCount; i++)
                {
                    //swap:
                    temp = ((Line)VLines[i]).startY;
                    ((Line)VLines[i]).startY = ((Line)VLines[i]).startX;
                    ((Line)VLines[i]).startX = temp;
                    if (((Line)VLines[i]).height <= 15 || ((Line)VLines[i]).width <= 1)//|| ((Line)VLines[i]).width > 100)
                    {
                        VLines.RemoveAt(i);
                        i--;
                        VLinesCount--;
                    }
                }

                #region Merge Vertical Lines

                int VLinesC = VLines.Count;
                for (int i = 0; i < VLinesC; i++)
                {
                    for (int j = i + 1; j < VLinesC; j++)
                    {
                        if ((Math.Abs(((Line)VLines[i]).startX - ((Line)VLines[j]).startX) <= 3) // ||
                                                                                                 //(((Line)HLines[j]).startY > ((Line)HLines[i]).startY && ((Line)HLines[j]).startY < ((Line)HLines[i]).startY + ((Line)HLines[i]).width)
                            && ((Line)VLines[i]).height != 0 && i != j)
                        //if (((Line)HLines[i]).startY == ((Line)HLines[j]).startY && ((Line)HLines[i]).height != 0)
                        {
                            if (Math.Abs(((Line)VLines[i]).startY + ((Line)VLines[i]).height - ((Line)VLines[j]).startY) <= VThreshold ||
                                    Math.Abs(((Line)VLines[j]).startY + ((Line)VLines[j]).height - ((Line)VLines[i]).startY) <= VThreshold ||

                                    (((Line)VLines[i]).startY < ((Line)VLines[j]).startY && ((Line)VLines[j]).startY < ((Line)VLines[i]).startY + ((Line)VLines[i]).height) ||
                                    (((Line)VLines[j]).startY < ((Line)VLines[i]).startY && ((Line)VLines[i]).startY < ((Line)VLines[j]).startY + ((Line)VLines[j]).height)
                                    )
                            {
                                bool resConnect = false;
                                int sY = 0;
                                int h = 0;
                                resConnect = ConnectVLines((Line)VLines[i], (Line)VLines[j], ref sY, ref h);
                                if (resConnect == true)
                                {
                                    ((Line)VLines[i]).startY = sY;
                                    ((Line)VLines[i]).height = h;
                                    ((Line)VLines[j]).height = 0;   //to be removed
                                }
                            }
                        }
                    }
                }
                for (int i = 0; i < VLines.Count; i++)
                {
                    if (((Line)VLines[i]).height <= 15 || ((Line)VLines[i]).width <= 1)
                    {
                        VLines.RemoveAt(i);
                        i--;
                    }
                }

                #endregion Merge Vertical Lines

                #endregion Tolerate VLines

                //Free variables
                VerticalRuns.Clear();
                VerticalRuns = null;
                HorizontalRuns.Clear();
                HorizontalRuns = null;
                //HLines.Clear();
                HorizontalRuns = null;

                return 0;
            }
            catch
            {
                return 2;
            }
            finally
            {
                if (src != null)
                    src.UnlockBits(bmData);
            }
        }

        /// <summary>
        /// Finds Horizontal lines inside a binary image.
        /// </summary>
        /// <param name="src">The source 8 bit B/W image</param>
        /// <param name="HLines">The returned array of Horizontal lines</param>
        /// <param name="startX">x value to start detection from,recommended value 10</param>
        /// <param name="endX">x value to end detection at(Threshold from end),recommended value 20</param>
        /// <param name="startY">y value to start detection from,recommended value 10</param>
        /// <param name="endY">y value to end detection at(Threshold from end),recommended value 20</param>
        /// <param name="HSeedLength">seed of horizontal line,recommended value 150</param>
        /// <param name="HThreshold">maximum horizontal length between Run Lengths,recommended value 10</param>
        /// <param name="VThreshold">maximum vertical length between Run Lengths,recommended value 2</param>
        /// <param name="LAddedThreshold">Accepted length for RL to be addded,recommended value 50</param>
        /// <param name="LNeglectedRunLength">Threshold for RL's that are neglected,recommended value 15</param>
        /// <returns>
        /// 0	:success
        /// 1	:invalid Pixel Format
        /// 2	:Fail</returns>
        public static int FindHLines(Bitmap src, ref ArrayList HLines,
            int startX, int endX, int startY, int endY, int HSeedLength,
            int HThreshold, int VThreshold, int LAddedThreshold, int LNeglectedRunLength)
        {
            //Definitions
            BitmapData bmData = null;
            try
            {
                //definitions:
                ArrayList HorizontalRuns = new ArrayList();

                //assume binary image
                if (src.PixelFormat != PixelFormat.Format8bppIndexed)
                    return 1;

                //lock data:
                bmData = src.LockBits(new Rectangle(0, 0, src.Width, src.Height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);

                //width and height:
                int nWidth = bmData.Width;
                int height = bmData.Height;

                //Find Horizontal Runs
                RunLength.FindHRun(bmData, ref HorizontalRuns, LNeglectedRunLength, true, startX, endX, startY, endY);

                //Counters
                int HorizontalRunsC = HorizontalRuns.Count;

                //Horizontal Runs
                for (int i = 0; i < HorizontalRunsC; i++)
                {
                    if (RemoveRunLength(HorizontalRuns, (HRun)(HorizontalRuns[i]), 5, HThreshold, HSeedLength, VThreshold) == true)
                    {
                        //Remove Run Length from the list
                        HorizontalRuns.RemoveAt(i);
                        i--;
                        HorizontalRunsC--;
                    }
                }

                // Make Lines
                RunLength.MakeLineFromHRun(HorizontalRuns, ref HLines, 70, LAddedThreshold);

                #region Tolerate HLines

                //refinement step:
                //1. connect HLines

                #region MergeHLines

                int HLinesC = HLines.Count;
                for (int i = 0; i < HLinesC; i++)
                {
                    for (int j = i + 1; j < HLinesC; j++)
                    {
                        if ((Math.Abs(((Line)HLines[i]).startY - ((Line)HLines[j]).startY) < 2) // ||
                                                                                                //(((Line)HLines[j]).startY > ((Line)HLines[i]).startY && ((Line)HLines[j]).startY < ((Line)HLines[i]).startY + ((Line)HLines[i]).width)
                            && ((Line)HLines[i]).height != 0 && i != j)
                        //if (((Line)HLines[i]).startY == ((Line)HLines[j]).startY && ((Line)HLines[i]).height != 0)
                        {
                            if (Math.Abs(((Line)HLines[i]).startX + ((Line)HLines[i]).height - ((Line)HLines[j]).startX) < HThreshold ||
                                    Math.Abs(((Line)HLines[j]).startX + ((Line)HLines[j]).height - ((Line)HLines[i]).startX) < HThreshold ||

                                    (((Line)HLines[i]).startX < ((Line)HLines[j]).startX && ((Line)HLines[j]).startX < ((Line)HLines[i]).startX + ((Line)HLines[i]).height) ||
                                    (((Line)HLines[j]).startX < ((Line)HLines[i]).startX && ((Line)HLines[i]).startX < ((Line)HLines[j]).startX + ((Line)HLines[j]).height)
                                    )
                            {
                                bool resConnect = false;
                                int sX = 0;
                                int h = 0;
                                resConnect = ConnectHLines((Line)HLines[i], (Line)HLines[j], ref sX, ref h);
                                if (resConnect == true)
                                {
                                    ((Line)HLines[i]).startX = sX;
                                    ((Line)HLines[i]).height = h;
                                    ((Line)HLines[j]).height = 0;   //to be removed
                                }
                            }
                        }
                    }
                }

                #endregion MergeHLines

                int thresh = nWidth / 5;
                //2.Remove small lines
                for (int i = 0; i < HLines.Count; i++)
                {
                    if (((Line)HLines[i]).height <= 15 || (((Line)HLines[i]).width <= 1 && ((Line)HLines[i]).height < thresh))
                    {
                        HLines.RemoveAt(i);
                        i--;
                    }
                }

                /////////////////////////modified 6 jan,2007
                //3.Fit the Hlines
                HLinesC = HLines.Count;
                Line pLine;
                for (int i = 0; i < HLinesC; i++)
                {
                    pLine = (Line)HLines[i];
                    //Fit the line vertically:
                    int resLine = FitLine(bmData, ref pLine);
                    //Extend the line horizontally:
                    int resExtend = ExtendLine(bmData, ref pLine);
                    //int res = CorrectLineTerminals(bmData, ref pLine);

                    HLines[i] = (Line)pLine;
                }
                HLinesC = HLines.Count;
                for (int i = 0; i < HLinesC; i++)
                {
                    for (int j = i + 1; j < HLinesC; j++)
                    {
                        if ((Math.Abs(((Line)HLines[i]).startY - ((Line)HLines[j]).startY) < 2) // ||
                                                                                                //(((Line)HLines[j]).startY > ((Line)HLines[i]).startY && ((Line)HLines[j]).startY < ((Line)HLines[i]).startY + ((Line)HLines[i]).width)
                            && ((Line)HLines[i]).height != 0 && i != j)
                        //if (((Line)HLines[i]).startY == ((Line)HLines[j]).startY && ((Line)HLines[i]).height != 0)
                        {
                            if (Math.Abs(((Line)HLines[i]).startX + ((Line)HLines[i]).height - ((Line)HLines[j]).startX) < HThreshold ||
                                    Math.Abs(((Line)HLines[j]).startX + ((Line)HLines[j]).height - ((Line)HLines[i]).startX) < HThreshold ||

                                    (((Line)HLines[i]).startX < ((Line)HLines[j]).startX && ((Line)HLines[j]).startX < ((Line)HLines[i]).startX + ((Line)HLines[i]).height) ||
                                    (((Line)HLines[j]).startX < ((Line)HLines[i]).startX && ((Line)HLines[i]).startX < ((Line)HLines[j]).startX + ((Line)HLines[j]).height)
                                    )
                            {
                                bool resConnect = false;
                                int sX = 0;
                                int h = 0;
                                resConnect = ConnectHLines((Line)HLines[i], (Line)HLines[j], ref sX, ref h);
                                if (resConnect == true)
                                {
                                    ((Line)HLines[i]).startX = sX;
                                    ((Line)HLines[i]).height = h;
                                    ((Line)HLines[j]).height = 0;   //to be removed
                                }
                            }
                        }
                    }
                }
                for (int i = 0; i < HLines.Count; i++)
                {
                    if (((Line)HLines[i]).height <= 15 || (((Line)HLines[i]).width <= 1 && ((Line)HLines[i]).height < thresh))
                    //if (((Line)HLines[i]).height <= 15 || ((Line)HLines[i]).width <= 1)
                    {
                        HLines.RemoveAt(i);
                        i--;
                    }
                }

                #endregion Tolerate HLines

                //Free variables
                HorizontalRuns.Clear();
                HorizontalRuns = null;
                HorizontalRuns = null;

                return 0;
            }
            catch
            {
                return 2;
            }
            finally
            {
                if (src != null)
                    src.UnlockBits(bmData);
            }
        }

        /// <summary>
        /// Finds Vertical lines inside a binary image.
        /// </summary>
        /// <param name="src">The source 8 bit B/W image</param>
        /// <param name="VLines">The returned array of Vertical lines</param>
        /// <param name="startX">x value to start detection from,recommended value 10</param>
        /// <param name="endX">x value to end detection at(Threshold from end),recommended value 20</param>
        /// <param name="startY">y value to start detection from,recommended value 10</param>
        /// <param name="endY">y value to end detection at(Threshold from end),recommended value 20</param>
        /// <param name="VSeedLength">seed of vertical line,recommended value 40</param>
        /// <param name="HThreshold">maximum horizontal length between Run Lengths,recommended value 10</param>
        /// <param name="VThreshold">maximum vertical length between Run Lengths,recommended value 2</param>
        /// <param name="LAddedThreshold">Accepted length for RL to be addded,recommended value 50</param>
        /// <param name="LNeglectedRunLength">Threshold for RL's that are neglected,recommended value 15</param>
        /// <returns>
        /// 0	:success
        /// 1	:invalid Pixel Format
        /// 2	:Fail</returns>
        public static int FindVLines(Bitmap src, ref ArrayList VLines,
            int startX, int endX, int startY, int endY, int VSeedLength,
            int HThreshold, int VThreshold, int LAddedThreshold, int LNeglectedRunLength)
        {
            //Definitions
            BitmapData bmData = null;
            try
            {
                //definitions:
                ArrayList VerticalRuns = new ArrayList();

                //assume binary image
                if (src.PixelFormat != PixelFormat.Format8bppIndexed)
                    return 1;

                //lock data:
                bmData = src.LockBits(new Rectangle(0, 0, src.Width, src.Height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);

                //width and height:
                int nWidth = bmData.Width;
                int height = bmData.Height;

                //V lines:
                //Find vertical Runs
                RunLength.FindVRun(bmData, ref VerticalRuns, LNeglectedRunLength, startX, endX, startY, endY);

                //Counters
                int VerticalRunsC = VerticalRuns.Count;

                //Vertical Runs
                for (int j = 0; j < VerticalRunsC; j++)
                {
                    if (RunLength.RemoveRunLength(VerticalRuns, (VRun)(VerticalRuns[j]), 7, HThreshold, VSeedLength, VThreshold) == true)
                    {
                        //Remove Run Length from the list
                        VerticalRuns.RemoveAt(j);
                        j--;
                        VerticalRunsC--;
                    }
                }

                // Make Lines
                RunLength.MakeLineFromVRun2(VerticalRuns, ref VLines, 50, LAddedThreshold);

                ///////////////////////
                //modified 8 jan,2007
                // A. Vertical Lines:
                int temp = 0;
                int VLinesCount = VLines.Count;
                for (int i = 0; i < VLinesCount; i++)
                {
                    //swap:
                    temp = ((Line)VLines[i]).startY;
                    ((Line)VLines[i]).startY = ((Line)VLines[i]).startX;
                    ((Line)VLines[i]).startX = temp;
                    if (((Line)VLines[i]).height <= 15 || ((Line)VLines[i]).width <= 1)//|| ((Line)VLines[i]).width > 100)
                    {
                        VLines.RemoveAt(i);
                        i--;
                        VLinesCount--;
                    }
                }

                #region Merge Vertical Lines

                int VLinesC = VLines.Count;
                for (int i = 0; i < VLinesC; i++)
                {
                    for (int j = i + 1; j < VLinesC; j++)
                    {
                        if ((Math.Abs(((Line)VLines[i]).startX - ((Line)VLines[j]).startX) <= 3) // ||
                                                                                                 //(((Line)HLines[j]).startY > ((Line)HLines[i]).startY && ((Line)HLines[j]).startY < ((Line)HLines[i]).startY + ((Line)HLines[i]).width)
                            && ((Line)VLines[i]).height != 0 && i != j)
                        //if (((Line)HLines[i]).startY == ((Line)HLines[j]).startY && ((Line)HLines[i]).height != 0)
                        {
                            if (Math.Abs(((Line)VLines[i]).startY + ((Line)VLines[i]).height - ((Line)VLines[j]).startY) <= VThreshold ||
                                    Math.Abs(((Line)VLines[j]).startY + ((Line)VLines[j]).height - ((Line)VLines[i]).startY) <= VThreshold ||

                                    (((Line)VLines[i]).startY < ((Line)VLines[j]).startY && ((Line)VLines[j]).startY < ((Line)VLines[i]).startY + ((Line)VLines[i]).height) ||
                                    (((Line)VLines[j]).startY < ((Line)VLines[i]).startY && ((Line)VLines[i]).startY < ((Line)VLines[j]).startY + ((Line)VLines[j]).height)
                                    )
                            {
                                bool resConnect = false;
                                int sY = 0;
                                int h = 0;
                                resConnect = ConnectVLines((Line)VLines[i], (Line)VLines[j], ref sY, ref h);
                                if (resConnect == true)
                                {
                                    ((Line)VLines[i]).startY = sY;
                                    ((Line)VLines[i]).height = h;
                                    ((Line)VLines[j]).height = 0;   //to be removed
                                }
                            }
                        }
                    }
                }
                for (int i = 0; i < VLines.Count; i++)
                {
                    if (((Line)VLines[i]).height <= 15 || ((Line)VLines[i]).width <= 1)
                    {
                        VLines.RemoveAt(i);
                        i--;
                    }
                }

                #endregion Merge Vertical Lines

                //Free variables
                VerticalRuns.Clear();
                VerticalRuns = null;

                return 0;
            }
            catch
            {
                return 2;
            }
            finally
            {
                if (src != null)
                    src.UnlockBits(bmData);
            }
        }

        public static int FindVLinesPoints(Bitmap src, ref ArrayList VLinesPoints,
            int startX, int endX, int startY, int endY, int VSeedLength,
            int HThreshold, int VThreshold, int LAddedThreshold, int LNeglectedRunLength)
        {
            //Definitions
            BitmapData bmData = null;
            ArrayList VLines = new ArrayList();
            try
            {
                //definitions:
                ArrayList VerticalRuns = new ArrayList();

                //assume binary image
                if (src.PixelFormat != PixelFormat.Format8bppIndexed)
                    return 1;

                //lock data:
                bmData = src.LockBits(new Rectangle(0, 0, src.Width, src.Height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);

                //width and height:
                int nWidth = bmData.Width;
                int height = bmData.Height;

                //V lines:
                //Find vertical Runs
                RunLength.FindVRun(bmData, ref VerticalRuns, LNeglectedRunLength, startX, endX, startY, endY);

                //Counters
                int VerticalRunsC = VerticalRuns.Count;

                //Vertical Runs
                for (int j = 0; j < VerticalRunsC; j++)
                {
                    if (RunLength.RemoveRunLength(VerticalRuns, (VRun)(VerticalRuns[j]), 7, HThreshold, VSeedLength, VThreshold) == true)
                    {
                        //Remove Run Length from the list
                        VerticalRuns.RemoveAt(j);
                        j--;
                        VerticalRunsC--;
                    }
                }

                // Make Lines
                RunLength.MakeLineFromVRun2(VerticalRuns, ref VLines, 50, LAddedThreshold);

                ///////////////////////
                //modified 8 jan,2007
                // A. Vertical Lines:
                int temp = 0;
                int VLinesCount = VLines.Count;
                for (int i = 0; i < VLinesCount; i++)
                {
                    //swap:
                    temp = ((Line)VLines[i]).startY;
                    ((Line)VLines[i]).startY = ((Line)VLines[i]).startX;
                    ((Line)VLines[i]).startX = temp;
                    if (((Line)VLines[i]).height <= 15 || ((Line)VLines[i]).width <= 1)//|| ((Line)VLines[i]).width > 100)
                    {
                        VLines.RemoveAt(i);
                        i--;
                        VLinesCount--;
                    }
                }

                #region Merge Vertical Lines

                int VLinesC = VLines.Count;
                for (int i = 0; i < VLinesC; i++)
                {
                    for (int j = i + 1; j < VLinesC; j++)
                    {
                        if ((Math.Abs(((Line)VLines[i]).startX - ((Line)VLines[j]).startX) <= 3) // ||
                                                                                                 //(((Line)HLines[j]).startY > ((Line)HLines[i]).startY && ((Line)HLines[j]).startY < ((Line)HLines[i]).startY + ((Line)HLines[i]).width)
                            && ((Line)VLines[i]).height != 0 && i != j)
                        //if (((Line)HLines[i]).startY == ((Line)HLines[j]).startY && ((Line)HLines[i]).height != 0)
                        {
                            if (Math.Abs(((Line)VLines[i]).startY + ((Line)VLines[i]).height - ((Line)VLines[j]).startY) <= VThreshold ||
                                    Math.Abs(((Line)VLines[j]).startY + ((Line)VLines[j]).height - ((Line)VLines[i]).startY) <= VThreshold ||

                                    (((Line)VLines[i]).startY < ((Line)VLines[j]).startY && ((Line)VLines[j]).startY < ((Line)VLines[i]).startY + ((Line)VLines[i]).height) ||
                                    (((Line)VLines[j]).startY < ((Line)VLines[i]).startY && ((Line)VLines[i]).startY < ((Line)VLines[j]).startY + ((Line)VLines[j]).height)
                                    )
                            {
                                bool resConnect = false;
                                int sY = 0;
                                int h = 0;
                                resConnect = ConnectVLines((Line)VLines[i], (Line)VLines[j], ref sY, ref h);
                                if (resConnect == true)
                                {
                                    ((Line)VLines[i]).startY = sY;
                                    ((Line)VLines[i]).height = h;
                                    ((Line)VLines[j]).height = 0;   //to be removed
                                }
                            }
                        }
                    }
                }
                for (int i = 0; i < VLines.Count; i++)
                {
                    if (((Line)VLines[i]).height <= 15 || ((Line)VLines[i]).width <= 1)
                    {
                        VLines.RemoveAt(i);
                        i--;
                    }
                }

                #endregion Merge Vertical Lines

                //Find Points:
                VLinesC = VLines.Count;

                for (int i = 0; i < VLinesC; i++)
                {
                    ArrayList pointsFinal = new ArrayList();

                    //1.Core points
                    //int result = GetCorePoints(bmData, ref pointsFinal, ((Line)HLines[i]));

                    ArrayList VLineRuns = new ArrayList();
                    ArrayList VRunsFinal = new ArrayList();

                    //get the boundaries:
                    int x1 = (((Line)VLines[i]).startX - 10);
                    int y1 = (((Line)VLines[i]).startY);
                    int x2 = (((Line)VLines[i]).startX + ((Line)VLines[i]).width + 10);
                    int y2 = (((Line)VLines[i]).startY + ((Line)VLines[i]).height);
                    int LineLength = ((Line)VLines[i]).height;

                    if (x1 < 0)
                    {
                        x1 = 0;
                    }
                    if (y1 < 0)
                    {
                        y1 = 0;
                    }
                    if (x2 > bmData.Width)
                    {
                        x2 = bmData.Width;
                    }
                    if (y2 > bmData.Height)
                    {
                        y2 = bmData.Height;
                    }

                    RunLength.FindVRun(bmData, ref VLineRuns, LNeglectedRunLength, x1, bmData.Width - x2, y1, bmData.Height - y2);
                    //go through these lines
                    //start from the seed line and go up and down:
                    int seedX = (((Line)VLines[i]).startX + (((Line)VLines[i]).width / 2)) < bmData.Width ? (((Line)VLines[i]).startX + (((Line)VLines[i]).width / 2)) : bmData.Width;
                    int VLineRunsC = VLineRuns.Count;
                    int val1 = (((Line)VLines[i]).width / 2) + 2;
                    int val2 = (((Line)VLines[i]).width / 2) + 5;
                    int val3 = (((Line)VLines[i]).width / 2) + 8;
                    int val4 = (((Line)VLines[i]).width / 2) + 10;

                    //accummulate RLs
                    for (int k = 0; k < VLineRunsC; k++)
                    {
                        if (Math.Abs(((VRun)VLineRuns[k]).col - seedX) <= val2 && Math.Abs(((VRun)VLineRuns[k]).col - seedX) > val1
                            && ((VRun)VLineRuns[k]).height > (LineLength / 5))
                        {
                            //near the seed line.
                            VRunsFinal.Add(((VRun)VLineRuns[k]));
                        }
                        else if (Math.Abs(((VRun)VLineRuns[k]).col - seedX) <= val3 && Math.Abs(((VRun)VLineRuns[k]).col - seedX) > val2
                            && ((VRun)VLineRuns[k]).height > (LineLength / 3))
                        {
                            //near the seed line.
                            VRunsFinal.Add(((VRun)VLineRuns[k]));
                        }
                        else if (Math.Abs(((VRun)VLineRuns[k]).col - seedX) < val4 && Math.Abs(((VRun)VLineRuns[k]).col - seedX) > val1
                            && VLinePixelsDensity(bmData, ((VRun)VLineRuns[k]).r1, ((Line)VLines[i])) < 30)
                        {
                            //near the seed line.
                            VRunsFinal.Add(((VRun)VLineRuns[k]));
                        }
                    }

                    //get the points <=2
                    int resul = GetCorePointsV(bmData, ref pointsFinal, ((Line)VLines[i]));
                    //get the points
                    int VRunsFinalC = VRunsFinal.Count;
                    for (int k = 0; k < VRunsFinalC; k++)
                    {
                        int r1 = ((VRun)VRunsFinal[k]).r1;
                        int r2 = ((VRun)VRunsFinal[k]).r2;

                        for (int j = r1; j < r2; j++)
                        {
                            Point pt = new Point(((VRun)VRunsFinal[k]).col, j);
                            pointsFinal.Add(pt);
                        }
                    }

                    VLinesPoints.Add((ArrayList)pointsFinal);
                    //pointsFinal.Clear();
                }
                //Free variables
                VerticalRuns.Clear();
                VerticalRuns = null;

                return 0;
            }
            catch
            {
                return 2;
            }
            finally
            {
                if (src != null)
                    src.UnlockBits(bmData);
            }
        }

        internal static ArrayList GetFormVLines(ArrayList VLines, int h, int w)
        {
            ArrayList VLines3 = new ArrayList();
            int xVal = w;
            int ind = -1;
            int indSuspected = -1;
            int VLinesC = VLines.Count;
            //get 1st vertical line
            for (int i = 0; i < VLinesC; i++)
            {
                //if finds one ok
                if (((Line)VLines[i]).height > (0.2 * h) &&
                    ((Line)VLines[i]).startX > (int)(w / 17) &&
                    ((Line)VLines[i]).startX < (int)(0.5 * w) &&
                    ((Line)VLines[i]).startY > (int)(h / 5) &&
                    ((Line)VLines[i]).startY < (int)(h / 2))
                {
                    if (((Line)VLines[i]).startX < xVal)
                    {
                        ind = i;
                        xVal = ((Line)VLines[i]).startX;
                    }
                    else
                    {
                        indSuspected = i;
                    }
                    //one of the three lines
                    //VLines3.Add(((Line)VLines[i]));
                }
            }
            if (ind != -1)
            {
                VLines3.Add(((Line)VLines[ind]));
            }
            else if (indSuspected != -1)
            {
                VLines3.Add(((Line)VLines[indSuspected]));
            }
            else
            {
                Line l2 = new Line
                {
                    startX = (int)(w / 6),
                    startY = (int)(h / 1.6),
                    height = (int)(h / 2.7),
                    width = 6
                };

                VLines3.Add(l2);
            }
            //2nd VLine
            ind = -1;
            indSuspected = -1;
            xVal = w;

            for (int i = 0; i < VLinesC; i++)
            {
                //if finds one ok
                if (((Line)VLines[i]).height > (0.2 * h) &&
                    ((Line)VLines[i]).startX > (int)(0.3 * w) &&
                    ((Line)VLines[i]).startX < (int)(0.7 * w) &&
                    ((Line)VLines[i]).startY > (int)(h / 5) &&
                    ((Line)VLines[i]).startY < (int)(h / 2))
                {
                    if (((Line)VLines[i]).startX > ((Line)(VLines3[0])).startX)
                    {
                        ind = i;
                    }
                    //one of the three lines
                    //VLines3.Add(((Line)VLines[i]));
                }
            }
            if (ind != -1)
            {
                VLines3.Add(((Line)VLines[ind]));
            }
            else
            {
                Line l2 = new Line
                {
                    startX = (int)(w / 2),
                    startY = ((Line)(VLines3[0])).startY,
                    height = ((Line)(VLines3[0])).height,
                    width = ((Line)(VLines3[0])).width
                };

                VLines3.Add(l2);
            }

            //3rd VLine
            ind = -1;
            indSuspected = -1;
            xVal = 0;
            for (int i = 0; i < VLinesC; i++)
            {
                //if finds one ok
                if (((Line)VLines[i]).height > (0.25 * h) &&
                    ((Line)VLines[i]).startX > (int)(0.6 * w) &&
                    ((Line)VLines[i]).startX < w - (int)(w / 17) &&
                    ((Line)VLines[i]).startY > (int)(h / 5) &&
                    ((Line)VLines[i]).startY < (int)(h / 2))
                {
                    if (((Line)VLines[i]).startX > xVal)
                    {
                        ind = i;
                        xVal = ((Line)VLines[i]).startX;
                    }
                    else
                    {
                        indSuspected = i;
                    }
                    //one of the three lines
                    //VLines3.Add(((Line)VLines[i]));
                }
            }
            if (ind != -1)
            {
                VLines3.Add(((Line)VLines[ind]));
            }
            else if (indSuspected != -1)
            {
                VLines3.Add(((Line)VLines[indSuspected]));
            }
            else
            {
                Line l2 = new Line
                {
                    startX = w - (int)(w / 2),
                    startY = ((Line)(VLines3[0])).startY,
                    height = ((Line)(VLines3[0])).height,
                    width = ((Line)(VLines3[0])).width
                };

                VLines3.Add(l2);
            }
            return VLines3;
        }

        internal static int GetRectsFromImage(Bitmap src, ref Rectangle r1, ref Rectangle r2
            , ref Rectangle r3, ref Rectangle r4, ArrayList HLines, ArrayList VLines, int Thresh, int recThresh)
        {
            //get the major 3 lines
            int HLinesC = HLines.Count;
            int VLinesC = VLines.Count;
            int nWidth = src.Width;
            int height = src.Height;
            int prevY = 0;
            ArrayList HLines3 = new ArrayList();
            ArrayList VLines3 = new ArrayList();

            try
            {
                for (int i = 0; i < HLinesC; i++)
                {
                    if (((Line)HLines[i]).startY > Thresh &&
                        ((Line)HLines[i]).startY < height - Thresh &&
                        ((Line)HLines[i]).height > (0.6 * nWidth))
                    {
                        if (Math.Abs(((Line)HLines[i]).startY - prevY) > 10)
                        {
                            //one of the three liens
                            HLines3.Add(((Line)HLines[i]));
                            prevY = ((Line)HLines[i]).startY;
                            //get x Value of previous one
                        }
                    }
                }
                //if only 3 lines ==>take them
                if (VLines.Count == 3)
                {
                    VLines3 = VLines;
                }
                else
                {
                    //get the three lines
                    VLines3 = GetFormVLines(VLines, height, nWidth);
                }

                r1.Y = ((Line)HLines3[0]).startY + ((Line)HLines3[0]).width;
                r3.Y = ((Line)HLines3[1]).startY + ((Line)HLines3[1]).width;
                r1.Height = ((Line)HLines3[1]).startY - r1.Y;
                r3.Height = ((Line)HLines3[2]).startY - r3.Y;
                r1.X = ((Line)VLines3[0]).startX + ((Line)VLines3[0]).width;
                r3.X = r1.X;
                r1.Width = ((Line)VLines3[1]).startX - r1.X;
                r3.Width = r1.Width;

                r2.Y = r1.Y;//((Line)HLines3[0]).startY+((Line)HLines3[0]).width;
                r4.Y = r3.Y;//((Line)HLines3[1]).startY+((Line)HLines3[1]).width;
                r2.Height = ((Line)HLines3[1]).startY - r2.Y;
                r4.Height = ((Line)HLines3[2]).startY - r4.Y;
                r2.X = ((Line)VLines3[1]).startX + ((Line)VLines3[1]).width;
                r4.X = r2.X;
                r2.Width = ((Line)VLines3[2]).startX - r2.X;
                r4.Width = r2.Width;

                r1.X += recThresh;
                r1.Y += recThresh;
                r2.X += recThresh;
                r2.Y += recThresh;
                r3.X += recThresh;
                r3.Y += recThresh;
                r4.X += recThresh;
                r4.Y += recThresh;

                r1.Width -= 2 * recThresh;
                r1.Height -= 2 * recThresh;
                r2.Width -= 2 * recThresh;
                r2.Height -= 2 * recThresh;
                r3.Width -= 2 * recThresh;
                r3.Height -= 2 * recThresh;
                r4.Width -= 2 * recThresh;
                r4.Height -= 2 * recThresh;

                return 0;
            }
            catch (Exception exe)
            {
                Console.WriteLine(exe.ToString());
                return 2;
            }
        }

        //modified 8 jan,2007
        /// <summary>
        /// FitLine(fitSigLine)
        /// </summary>
        /// <param name="bmData"></param>
        /// <param name="pLine">Line to modify</param>
        /// <returns></returns>
        internal static int FitLine(BitmapData bmData, ref Line pLine)
        {
            if (pLine.width <= 2)
            {
                return 1;
            }
            try
            {
                int LWidth = pLine.width;
                int LHeight = pLine.height;
                int LX1 = pLine.startX;
                int LY1 = pLine.startY;
                int LX2 = LX1 + LHeight;
                int LY2 = LY1 + LWidth;
                int counter = 0;
                //int cAbove = 0;

                int nWidth = bmData.Width;
                int nHeight = bmData.Height;

                if (LX2 > nWidth)
                {
                    LX2 = nWidth;
                }
                if (LY2 > nHeight)
                {
                    LY2 = nHeight;
                }
                ArrayList yVal = new ArrayList();
                ArrayList pixelW = new ArrayList();

                int stride = bmData.Stride;
                System.IntPtr Scan0 = bmData.Scan0;
                //unsafe

                byte* p = (byte*)(void*)Scan0;

                int nOffset = stride - bmData.Width;
                //int nWidth = bmData.Width;
                //int height=bmData.Height ;
                for (int y = LY1; y < LY2; ++y)
                {
                    counter = 0;
                    //cAbove=0;
                    for (int x = LX1; x < LX2; ++x)
                    {
                        if (p[(y * stride) + x] == 255)
                        {
                            counter++;
                        }
                    }
                    //store all X value of lines much black
                    if ((counter < (int)(0.2 * LHeight) && LWidth > 3) ||
                        ((counter < (int)(0.4 * LHeight) && LWidth <= 3)))
                    {
                        yVal.Add(y);
                        pixelW.Add(counter);
                    }
                }

                //another shot
                if (yVal.Count < 1)
                {
                    for (int y = LY1; y < LY2; ++y)
                    {
                        counter = 0;
                        //cAbove=0;
                        for (int x = LX1; x < LX2; ++x)
                        {
                            if (p[(y * stride) + x] == 255)
                            {
                                counter++;
                            }
                        }
                        //store all X value of lines much black
                        if ((counter < (int)(0.3 * LHeight) && LWidth > 3) ||
                            ((counter < (int)(0.5 * LHeight) && LWidth <= 3)))
                        {
                            yVal.Add(y);
                            pixelW.Add(counter);
                        }
                    }
                }
                //modified 7 jan 2007
                //if still counts to 0
                if (yVal.Count < 1)
                {
                    for (int y = LY1; y < LY2; ++y)
                    {
                        counter = 0;
                        //cAbove=0;
                        for (int x = LX1; x < LX2; ++x)
                        {
                            if (p[(y * stride) + x] == 255)
                            {
                                counter++;
                            }
                        }
                        //store all X value of lines much black
                        if ((counter < (int)(0.4 * LHeight) && LWidth > 3) ||
                            ((counter < (int)(0.6 * LHeight) && LWidth <= 3)))
                        {
                            yVal.Add(y);
                            pixelW.Add(counter);
                        }
                    }
                }
                if (yVal.Count < 1)
                {
                    int initCounter = 20000;
                    int inity = -1;
                    for (int y = LY1; y < LY2; ++y)
                    {
                        counter = 0;
                        //cAbove=0;
                        for (int x = LX1; x < LX2; ++x)
                        {
                            if (p[(y * stride) + x] == 255)
                            {
                                counter++;
                            }
                        }
                        if (counter < initCounter)
                        {
                            initCounter = counter;
                            inity = y;
                        }
                    }
                    //fill and exit
                    if (inity != -1)
                    {
                        //yVal.Add(inity);
                        //pixelW.Add(counter);

                        pLine.width = 3;
                        pLine.startY = inity - 1;
                        return 0;
                    }
                }

                #region "Modified 9/3/2006"

                int PW0 = 2000;
                int PW1 = 2000;
                int PW2 = 2000;

                int y0 = -1;
                int y1 = -1;
                int y2 = -1;
                //Modified 13 March
                int ind0 = -1;
                int ind1 = -1;

                if (yVal.Count > 0)
                {
                    //get the largest 3 values
                    int yCount = yVal.Count;
                    for (int y = 0; y < yCount; y++)
                    {
                        if ((int)pixelW[y] < PW0)
                        {//larger than first element
                            PW0 = (int)pixelW[y];
                            y0 = (int)yVal[y];
                            ind0 = y;
                        }
                    }
                    for (int y = 0; y < yCount; y++)
                    {
                        if ((int)pixelW[y] < PW1 && y != ind0)
                        {
                            PW1 = (int)pixelW[y];
                            y1 = (int)yVal[y];
                            ind1 = y;
                        }
                    }
                    for (int y = 0; y < yCount; y++)
                    {
                        if ((int)pixelW[y] < PW2 && y != ind0 && ind1 != y)
                        {
                            PW2 = (int)pixelW[y];
                            y2 = (int)yVal[y];
                        }
                    }

                    pixelW.Clear();
                    if (PW0 < 2000 && PW1 < 2000 && PW2 < 2000)
                    {
                        //get the least y
                        int minY = Math.Min(y0, Math.Min(y1, y2));

                        //if two is more suitable:
                        int maxY = Math.Max(y0, Math.Max(y1, y2));
                        int DiffY = maxY - minY;
                        if (DiffY >= 5)
                        {
                            //get the nearest two lines:
                            int diff1 = Math.Abs(y0 - y1);
                            int diff2 = Math.Abs(y0 - y2);
                            int diff3 = Math.Abs(y1 - y2);

                            if (diff1 < 5)
                            {
                                //y0,y1
                                minY = Math.Min(y0, y1);
                            }
                            else if (diff2 < 5)
                            {
                                //y0,y2
                                minY = Math.Min(y0, y2);
                            }
                            else if (diff3 < 5)
                            {
                                //y1,y2
                                minY = Math.Min(y1, y2);
                            }
                            minY -= 1;
                            //if (minY != -1)
                            //{
                            //    pLine.width = 2;
                            //    pLine.startY = minY;
                            //    return 0;
                            //}
                        }

                        //change only Y and the Width.
                        if (minY != -1)
                        {
                            pLine.width = 3;
                            pLine.startY = minY;
                        }
                    }
                    ///////////////////modified 7 jan 2007
                    else if (PW0 + PW1 + PW2 < 4000)
                    {
                        //
                        int val1 = 0;
                        int val2 = 0;
                        //get the two values:
                        if (y0 < 0)
                        {
                            val1 = y1;
                            val2 = y2;
                        }
                        else if (y1 < 0)
                        {
                            val1 = y0;
                            val2 = y2;
                        }
                        else if (y2 < 0)
                        {
                            val1 = y0;
                            val2 = y1;
                        }
                        //get the least y
                        int minY = Math.Min(val1, val2);

                        //if two is more suitable:
                        int maxY = Math.Max(val1, val2);
                        int DiffY = maxY - minY;
                        if (DiffY >= 5)
                        {
                            minY = Math.Abs(val1 - val2);
                            minY -= 1;
                        }
                        //change only Y and the Width.
                        if (minY != -1)
                        {
                            pLine.width = 3;
                            pLine.startY = minY;
                        }
                    }
                    else if (PW0 + PW1 + PW2 < 6000)
                    {
                        //one of them is available
                        int val = 0;
                        //get the two values:
                        if (y0 > 0)
                        {
                            val = y0;
                        }
                        else if (y1 > 0)
                        {
                            val = y1;
                        }
                        else if (y2 > 0)
                        {
                            val = y2;
                        }

                        if (val != -1)
                        {
                            pLine.width = 3;
                            pLine.startY = val - 1;
                        }
                    }
                    /////////////////////////
                    else if (yVal.Count == 3)
                    {
                        int minY = Math.Min((int)yVal[0], Math.Min((int)yVal[1], (int)yVal[2]));
                        pLine.width = 3;
                        pLine.startY = minY;
                    }
                    else
                    {
                        int minY = Math.Min(y0, Math.Min(y1, y2));

                        //change only Y and the Width.
                        if (minY != -1)
                        {
                            pLine.width = 3;
                            pLine.startY = minY;
                        }
                    }
                }

                #endregion "Modified 9/3/2006"

                if (pLine.width > 3)
                {
                    pLine.startY += pLine.width - 3;
                    pLine.width = 3;
                }
                return 0;
            }
            catch
            {
                return 2;
            }
            finally
            {
                //if (src != null)
                //src.UnlockBits(bmData);
                //src.Dispose();
                //src=null;
            }
        }

        //Modified 30 March,2006
        internal static unsafe int CorrectLineTerminals(Bitmap src, ref Line pLine)
        {
            BitmapData bmData = null;
            try
            {
                bmData = src.LockBits(new Rectangle(0, 0, src.Width, src.Height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);

                int stX = pLine.startX;
                int y = pLine.startY;
                int endX = pLine.startX + pLine.height;
                int endY = pLine.startY + pLine.width;

                int beforeCounter = 0;
                int afterCounter = 0;
                int counter = 0;
                int stride = bmData.Stride;
                System.IntPtr Scan0 = bmData.Scan0;

                byte* p = (byte*)(void*)Scan0;

                int nOffset = stride - bmData.Width;
                int stX50 = stX + 50;
                for (int x = stX; x < stX50; x++)
                {
                    //above the line(3 pixels)
                    if (p[((y - 1) * stride) + x] == 0)
                    {
                        beforeCounter++;
                    }
                    if (p[((y - 2) * stride) + x] == 0)
                    {
                        beforeCounter++;
                    }
                    if (p[((y - 3) * stride) + x] == 0)
                    {
                        beforeCounter++;
                    }
                    //each 10 iterations
                    if (((x + 1) - stX) % 10 == 0)
                    {
                        if (beforeCounter > 15)
                        {
                            counter++;
                            beforeCounter = 0;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                int removeBefore = counter * 10;
                counter = 0;
                int endX50 = endX - 50;
                for (int x = endX; x > endX50; x--)
                {
                    //above the line(3 pixels)
                    if (p[((y - 1) * stride) + x] == 0)
                    {
                        afterCounter++;
                    }
                    if (p[((y - 2) * stride) + x] == 0)
                    {
                        afterCounter++;
                    }
                    if (p[((y - 3) * stride) + x] == 0)
                    {
                        afterCounter++;
                    }
                    //each 10 iterations
                    if (((endX + 1) - x) % 10 == 0)
                    {
                        if (afterCounter > 15)
                        {
                            counter++;
                            afterCounter = 0;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                int removeAfter = counter * 10;

                //correct the line
                if (removeBefore > 0)
                {
                    pLine.startX += removeBefore;
                    pLine.height -= removeBefore;
                }
                if (removeAfter > 0)
                {
                    pLine.height -= removeAfter;
                }

                return 0;
            }
            catch
            {
                return 2;
            }
            finally
            {
                if (src != null)
                    src.UnlockBits(bmData);
            }
        }

        //modified 8 jan,2007
        internal static int CorrectLineTerminals(BitmapData bmData, ref Line pLine)
        {
            //BitmapData bmData = null;
            try
            {
                //bmData = src.LockBits(new Rectangle(0, 0, src.Width, src.Height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);

                int stX = pLine.startX;
                int y = pLine.startY;
                int endX = pLine.startX + pLine.height;
                int endY = pLine.startY + pLine.width;

                int beforeCounter = 0;
                int afterCounter = 0;
                int counter = 0;
                int stride = bmData.Stride;
                System.IntPtr Scan0 = bmData.Scan0;

                byte* p = (byte*)(void*)Scan0;

                int nOffset = stride - bmData.Width;
                int stX50 = stX + 50;
                for (int x = stX; x < stX50; x++)
                {
                    //above the line(3 pixels)
                    if (p[((y - 1) * stride) + x] == 0)
                    {
                        beforeCounter++;
                    }
                    if (p[((y - 2) * stride) + x] == 0)
                    {
                        beforeCounter++;
                    }
                    if (p[((y - 3) * stride) + x] == 0)
                    {
                        beforeCounter++;
                    }
                    //each 10 iterations
                    if (((x + 1) - stX) % 10 == 0)
                    {
                        if (beforeCounter > 15)
                        {
                            counter++;
                            beforeCounter = 0;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                int removeBefore = counter * 10;
                counter = 0;
                int endX50 = endX - 50;
                for (int x = endX; x > endX50; x--)
                {
                    //above the line(3 pixels)
                    if (p[((y - 1) * stride) + x] == 0)
                    {
                        afterCounter++;
                    }
                    if (p[((y - 2) * stride) + x] == 0)
                    {
                        afterCounter++;
                    }
                    if (p[((y - 3) * stride) + x] == 0)
                    {
                        afterCounter++;
                    }
                    //each 10 iterations
                    if (((endX + 1) - x) % 10 == 0)
                    {
                        if (afterCounter > 15)
                        {
                            counter++;
                            afterCounter = 0;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                int removeAfter = counter * 10;

                //correct the line
                if (removeBefore > 0)
                {
                    pLine.startX += removeBefore;
                    pLine.height -= removeBefore;
                }
                if (removeAfter > 0)
                {
                    pLine.height -= removeAfter;
                }

                return 0;
            }
            catch
            {
                return 2;
            }
        }

        internal static int GetLinePoints(Bitmap src, Rectangle sigLine, ref ArrayList points)
        {
            BitmapData bmData = null;
            //Modify the signature line
            sigLine.Y -= 1;
            sigLine.Width += 2;

            try
            {
                bmData = src.LockBits(new Rectangle(0, 0, src.Width, src.Height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);

                int stX = sigLine.X;
                int stY = sigLine.Y;
                int endX = sigLine.X + sigLine.Height;
                int endY = sigLine.Y + sigLine.Width;
                int thresh = sigLine.Height / 10;

                int stride = bmData.Stride;
                System.IntPtr Scan0 = bmData.Scan0;
                byte* p = (byte*)(void*)Scan0;
                int nOffset = stride - bmData.Width;

                for (int x = stX; x < endX; x++)
                    for (int y = stY; y < endY; y++)
                        if (p[(y * stride) + x] == 0)
                            points.Add(new Point(x, y));
                return 0;
            }
            catch
            {
                return 2;
            }
            finally
            {
                src?.UnlockBits(bmData);
            }
        }

        internal static int ExtendLine(Bitmap src, ref Line pLine)
        {
            BitmapData bmData = null;
            try
            {
                bmData = src.LockBits(new Rectangle(0, 0, src.Width, src.Height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);

                int LWidth = pLine.width;
                int LHeight = pLine.height;
                int LX1 = pLine.startX;
                int LLX1 = LX1 - 50;
                int LY1 = pLine.startY;
                int LX2 = LX1 + LHeight;
                int LLX2 = LX2;
                int LY2 = LY1 + LWidth;
                int counter = 0;

                if (LX2 < src.Width - 50)
                    LLX2 = LX2 + 50;

                ArrayList yVal = new ArrayList();
                ArrayList yAfterVal = new ArrayList();

                int stride = bmData.Stride;
                System.IntPtr Scan0 = bmData.Scan0;

                byte* p = (byte*)(void*)Scan0;

                int nOffset = stride - bmData.Width;
                for (int y = LY1; y < LY2; ++y)
                {
                    counter = 0;
                    for (int x = LLX1; x < LX1; ++x)
                    {
                        if (p[(y * stride) + x] == 255)
                        {
                            counter++;
                        }
                    }
                    if (counter < (int)(10))
                    {
                        yVal.Add(y);
                    }

                    counter = 0;
                    for (int x = LX2; x < LLX2; ++x)
                    {
                        if (p[(y * stride) + x] == 255)
                        {
                            counter++;
                        }
                    }

                    if (counter < (int)(10) && LX2 != LLX2)
                    {
                        yAfterVal.Add(y);
                    }
                }
                if (((double)(LY2 - LY1) / 2) <= yVal.Count)
                {
                    pLine.startX -= 50;
                    pLine.height += 50;
                }
                if (((double)(LY2 - LY1) / 2) <= yAfterVal.Count)
                {
                    pLine.height += 50;
                }

                return 0;
            }
            catch
            {
                return 2;
            }
            finally
            {
                if (src != null)
                    src.UnlockBits(bmData);
            }
        }

        internal static int ExtendLine(BitmapData bmData, ref Line pLine)
        {
            try
            {
                int bmWidth = bmData.Width;
                int LWidth = pLine.width;
                int LHeight = pLine.height;
                int LX1 = pLine.startX;
                int LLX1 = LX1 - 50;
                int LY1 = pLine.startY;
                int LX2 = LX1 + LHeight;
                int LLX2 = LX2;
                int LY2 = LY1 + LWidth;
                int counter = 0;

                if (LLX1 < 0)
                {
                    LLX1 = 0;
                }
                if (LY1 < 0)
                {
                    LY1 = 0;
                    pLine.startY = 0;
                }
                if (LX2 < bmWidth - 50)
                {
                    LLX2 = LX2 + 50;
                }

                ArrayList yVal = new ArrayList();
                ArrayList yAfterVal = new ArrayList();

                int stride = bmData.Stride;
                System.IntPtr Scan0 = bmData.Scan0;

                byte* p = (byte*)(void*)Scan0;

                int nOffset = stride - bmData.Width;
                for (int y = LY1; y < LY2; ++y)
                {
                    counter = 0;

                    for (int x = LLX1; x < LX1; ++x)
                    {
                        if (p[(y * stride) + x] == 255)
                        {
                            counter++;
                        }
                    }
                    if (counter < (int)(10))
                    {
                        yVal.Add(y);
                    }

                    counter = 0;
                    for (int x = LX2; x < LLX2; ++x)
                    {
                        if (p[(y * stride) + x] == 255)
                        {
                            counter++;
                        }
                    }

                    if (counter < (int)(10) && LX2 != LLX2)
                    {
                        yAfterVal.Add(y);
                    }
                }//end y
                if (((double)(LY2 - LY1) / 2) <= yVal.Count)
                {
                    pLine.startX -= 50;
                    pLine.height += 50;
                }

                if (((double)(LY2 - LY1) / 2) <= yAfterVal.Count)
                {
                    pLine.height += 50;
                }

                return 0;
            }
            catch
            {
                return 2;
            }
        }

        /// <summary>
        /// Return Array of Lines
        /// </summary>
        /// <param name="src">The source image</param>
        /// <param name="LineType">Line Type (Horizontal Or Vertical)</param>
        /// <returns>null :if no lines returned</returns>
        internal static ArrayList GetLines(Bitmap src, string LineType)
        {
            BitmapData bmData = null;
            try
            {
                ArrayList VerticalRuns = new ArrayList();
                ArrayList HorizontalRuns = new ArrayList();
                ArrayList Lines = new ArrayList();

                //			//assume binary image
                //			if ( src.PixelFormat != PixelFormat.Format8bppIndexed )
                //				return ;

                bmData = src.LockBits(new Rectangle(0, 0, src.Width, src.Height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                //DateTime st=DateTime.Now ;
                //1. Horizontal Lines
                int nWidth = bmData.Width;
                int height = bmData.Height;
                if (LineType.ToUpper() == "H")
                {
                    RunLength.FindHRun(bmData, ref HorizontalRuns, 15, true, 10, 10, 10, 10);
                    int HorizontalRunsC = HorizontalRuns.Count;
                    //Horizontal Runs
                    for (int i = 0; i < HorizontalRunsC; i++)
                    {
                        if (RunLength.RemoveRunLength(HorizontalRuns, (HRun)(HorizontalRuns[i]), 5, 10, 140, 2) == true)
                        {
                            //Remove Run Length from the list
                            HorizontalRuns.RemoveAt(i);
                            i--;
                            HorizontalRunsC--;
                        }
                    }
                    RunLength.MakeLineFromHRun(HorizontalRuns, ref Lines, 70, 50);
                    Console.WriteLine(HorizontalRuns.Count);
                }
                else if (LineType.ToUpper() == "V")
                {
                    //Vertical Runs
                    RunLength.FindVRun(bmData, ref VerticalRuns, 15, Convert.ToUInt16(nWidth / 2), 10, Convert.ToUInt16(height / 8), Convert.ToUInt16(height / 8));
                    int VerticalRunsC = VerticalRuns.Count;
                    for (int j = 0; j < VerticalRunsC; j++)
                    {
                        if (RunLength.RemoveRunLength(VerticalRuns, (VRun)(VerticalRuns[j]), 7, 10, 40, 2) == true)
                        //if (RemoveRunLength(VerticalRuns,(VRun)(VerticalRuns[i]),5,10,70,2)==true)
                        {
                            //Remove Run Length from the list
                            VerticalRuns.RemoveAt(j);
                            j--;
                            VerticalRunsC--;
                        }
                    }

                    RunLength.MakeLineFromVRun(VerticalRuns, ref Lines, 40, 30);
                }
                return Lines;
            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                if (src != null)
                    src.UnlockBits(bmData);
            }
        }

        internal static int GetRectangleFromImage(Bitmap src, ref Rectangle Rect)// List<Rectangle> RectLst)
        {
            if (src.PixelFormat != PixelFormat.Format8bppIndexed)
                return 1;
            try
            {
                ArrayList HLines = new ArrayList();
                ArrayList VLinesAll = new ArrayList();

                int nres = FindLinesChecks(src, ref HLines, ref VLinesAll, "h");
                Line HLine1 = null;
                Line HLine2 = null;
                //ArrayList HLines = new ArrayList();// getLines(src,"H");
                int nWidth = src.Width;
                int height = src.Height;

                int HLinesC = HLines.Count;

                int indHL1 = -1;
                int indHL2 = -1;
                int LDiffVal = 2000;
                int StDiffVal = 2000;
                int nMaxLength = nWidth / 2;
                //Horizontal lines
                for (int i = 0; i < HLinesC; i++)
                {	//Check self properties
                    for (int j = 0; j < HLinesC; j++)
                    {
                        //compare legth
                        int LDiff = Math.Abs(((Line)HLines[i]).height - ((Line)HLines[j]).height);
                        int StDiff = Math.Abs(((Line)HLines[i]).startX - ((Line)HLines[j]).startX);
                        if ((LDiff < 150) &&
                            (StDiff < 28) &&
                            (Math.Abs(((Line)HLines[i]).startY - ((Line)HLines[j]).startY) < 150) &&
                            (Math.Abs(((Line)HLines[i]).startY - ((Line)HLines[j]).startY) > 28) &&
                            (Math.Abs(((Line)HLines[i]).height - ((Line)HLines[j]).height) < 28) &&
                            ((Line)HLines[i]).height != 0 && ((Line)HLines[i]).height < nMaxLength)    //Modified 13 March
                        {
                            //if better than the previous.
                            if (StDiff < StDiffVal || LDiff < LDiffVal)
                            {
                                //pixel density aboce threshold
                                indHL1 = i;
                                indHL2 = j;
                                StDiffVal = StDiff;
                                LDiffVal = LDiff;
                            }
                        }
                    }
                }
                //Vertical Lines
                //Make both lines same height
                if (indHL1 != -1 && indHL2 != -1)
                {
                    if (((Line)HLines[indHL1]).startY < ((Line)HLines[indHL2]).startY)
                    {
                        HLine1 = ((Line)HLines[indHL1]);
                        HLine2 = ((Line)HLines[indHL2]);
                    }
                    else
                    {
                        HLine2 = ((Line)HLines[indHL1]);
                        HLine1 = ((Line)HLines[indHL2]);
                    }

                    if (HLine1.height < HLine2.height)
                    {
                        HLine1.height = HLine2.height;
                    }
                    else
                    {
                        HLine2.height = HLine1.height;
                    }
                    Rect.X = HLine1.startX;
                    Rect.Y = HLine1.startY;
                    Rect.Height = HLine1.height;
                    Rect.Width = HLine2.startY + HLine2.width - (HLine1.startY);
                }
                else
                {
                    ArrayList VLines = GetLines(src, "V");
                    int VLinesC = VLines.Count;
                    for (int i = 0; i < HLinesC; i++)
                    {	//Check self properties
                        for (int j = 0; j < VLinesC; j++)  //X and Y inverted
                        {
                            int VLineX = ((Line)VLines[j]).startY;
                            int VLineY = ((Line)VLines[j]).startX;
                            {
                                if (Math.Abs(VLineY + ((Line)VLines[j]).height - ((Line)HLines[i]).startY) < 10 &&
                                    Math.Abs(VLineX - ((Line)HLines[i]).startX) < 10)
                                {
                                    Rect.X = VLineX;
                                    Rect.Y = VLineY;
                                    Rect.Height = ((Line)HLines[i]).height;
                                    Rect.Width = ((Line)HLines[i]).startY - VLineY;
                                }
                                else if (Math.Abs(VLineX - ((Line)HLines[i]).startX) < 10 &&
                                    Math.Abs(VLineY - ((Line)HLines[i]).startY) < 4)
                                {
                                    Rect.X = VLineX;
                                    Rect.Y = VLineY;
                                    Rect.Height = ((Line)HLines[i]).height;
                                    Rect.Width = ((Line)VLines[j]).height;
                                }
                            }
                        }
                    }
                }
                return 0;
            }
            catch (Exception)
            {
                return 2;
            }
            finally
            {
            }
        }

        internal static int GetCALines(Bitmap src, ref Rectangle amountBox, Line sigLine, Line dateLine, ArrayList HLinesAll)// Line HLine1,ref Line HLine2,ref Line VLine1,ref Line VLine2)
        {
            Line HLine1 = null;
            Line HLine2 = null;
            int dateY = 0;
            int sigY = src.Height;
            if (dateLine != null)
            {
                if (dateLine.startY < src.Height * 0.35)
                { dateY = dateLine.startY; }
            }
            if (sigLine != null)
            {
                if (sigLine.startY > src.Height * 0.65)
                { sigY = sigLine.startY; }
            }

            try
            {
                if (src.PixelFormat != PixelFormat.Format8bppIndexed)
                    return 1;

                ArrayList HLines = new ArrayList();// getLines(src,"H");
                int nWidth = src.Width;
                int height = src.Height;
                int HLinesAllC = HLinesAll.Count;
                //get the candidate lines
                for (int i = 0; i < HLinesAllC; i++)
                {
                    if (((Line)HLinesAll[i]).startX > (nWidth / 2) &&
                        ((Line)HLinesAll[i]).startY > dateY &&
                        ((Line)HLinesAll[i]).startY < sigY)
                    {
                        HLines.Add(HLinesAll[i]);
                    }
                }
                //ArrayList VLines=getLines(src,"V");
                int HLinesC = HLines.Count;

                int indHL1 = -1;
                int indHL2 = -1;
                //int indVL1=-1;
                //int indVL2=-1;
                int LDiffVal = 2000;
                int StDiffVal = 2000;

                //Horizontal lines
                for (int i = 0; i < HLinesC; i++)
                {	//Check self properties
                    for (int j = 0; j < HLinesC; j++)
                    {
                        int LDiff = Math.Abs(((Line)HLines[i]).height - ((Line)HLines[j]).height);
                        int StDiff = Math.Abs(((Line)HLines[i]).startX - ((Line)HLines[j]).startX);
                        if ((LDiff < 150) &&
                            (StDiff < 150) &&
                            (Math.Abs(((Line)HLines[i]).startY - ((Line)HLines[j]).startY) < 100) &&
                            (Math.Abs(((Line)HLines[i]).startY - ((Line)HLines[j]).startY) > 28) &&
                            ((Line)HLines[i]).height != 0)    //Modified 13 March
                        {
                            //if better than the previous.
                            if (StDiff < StDiffVal || LDiff < LDiffVal)
                            {
                                //pixel density aboce threshold
                                indHL1 = i;
                                indHL2 = j;
                                StDiffVal = StDiff;
                                LDiffVal = LDiff;
                            }
                        }
                    }
                }
                //Vertical Lines
                //Make both lines same height
                if (indHL1 != -1 && indHL2 != -1)
                {
                    if (((Line)HLines[indHL1]).startY < ((Line)HLines[indHL2]).startY)
                    {
                        HLine1 = ((Line)HLines[indHL1]);
                        HLine2 = ((Line)HLines[indHL2]);
                    }
                    else
                    {
                        HLine2 = ((Line)HLines[indHL1]);
                        HLine1 = ((Line)HLines[indHL2]);
                    }

                    if (HLine1.height < HLine2.height)
                    {
                        HLine1.height = HLine2.height;
                    }
                    else
                    {
                        HLine2.height = HLine1.height;
                    }
                    //make the box(Without Lines)
                    amountBox.X = HLine1.startX;
                    amountBox.Y = HLine1.startY;
                    amountBox.Height = HLine1.height;
                    //(With Lines)
                    amountBox.Width = HLine2.startY + HLine2.width - (HLine1.startY);
                }
                else
                {
                    ArrayList VLines = GetLines(src, "V");
                    int VLinesC = VLines.Count;
                    for (int i = 0; i < HLinesC; i++)
                    {	//Check self properties
                        //in 2nd part
                        if (((Line)HLines[i]).startX > (nWidth / 2) &&
                            ((Line)HLines[i]).startY > dateY &&
                            ((Line)HLines[i]).startY < sigY)
                        {
                            for (int j = 0; j < VLinesC; j++)  //X and Y inverted
                            {
                                int VLineX = ((Line)VLines[j]).startY;
                                int VLineY = ((Line)VLines[j]).startX;
                                if (VLineX > (nWidth / 2) &&
                                    VLineY > dateY &&
                                    VLineY < sigY)
                                {
                                    if (Math.Abs(VLineY + ((Line)VLines[j]).height - ((Line)HLines[i]).startY) < 10 &&
                                        Math.Abs(VLineX - ((Line)HLines[i]).startX) < 10)
                                    {
                                        amountBox.X = VLineX;
                                        amountBox.Y = VLineY;
                                        amountBox.Height = ((Line)HLines[i]).height;
                                        amountBox.Width = ((Line)HLines[i]).startY - VLineY;
                                    }
                                    else if (Math.Abs(VLineX - ((Line)HLines[i]).startX) < 10 &&
                                        Math.Abs(VLineY - ((Line)HLines[i]).startY) < 4)
                                    {
                                        amountBox.X = VLineX;
                                        amountBox.Y = VLineY;
                                        amountBox.Height = ((Line)HLines[i]).height;
                                        amountBox.Width = ((Line)VLines[j]).height;
                                    }
                                }
                            }
                        }
                    }
                }

                return 0;
            }
            catch (Exception)
            {
                return 2;
            }
        }

        internal static double GetPixelDensity(Bitmap b, int x1, int y1, int x2, int y2, int Thresh)
        {
            // GDI+ still lies to us - the return format is BGR, NOT RGB.
            BitmapData bmData = b.LockBits(new Rectangle(0, 0, b.Width, b.Height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
            int whitePixel = 0;
            int blackPixel = 0;
            int stride = bmData.Stride;
            System.IntPtr Scan0 = bmData.Scan0;

            byte* p = (byte*)(void*)Scan0;

            int nOffset = stride - b.Width;
            int nWidth = b.Width;
            int nHeight = b.Height;

            for (int y = 0; y < nHeight; ++y)
            {
                for (int x = 0; x < nWidth; ++x)
                {
                    if (p[0] < Thresh)
                    {
                        blackPixel++;
                    }
                    else if (p[0] > Thresh)
                    {
                        whitePixel++;
                    }
                    ++p;
                }
                p += nOffset;
            }

            b.UnlockBits(bmData);
            double retVal = Convert.ToDouble(blackPixel) / Convert.ToDouble(whitePixel);

            return retVal;
        }

        internal static Line ComposeLineWithoutCA(ArrayList HLines, Rectangle amountBox, Line PLine)
        {
            try
            {
                ArrayList PayeeLineCombo = new ArrayList();
                int HLinesC = HLines.Count;
                //PayeeLineCombo.Add(((Line)HLines[ind]));
                //values required in loop
                int amountBoxY = amountBox.Y;
                int PLineY = PLine.startY;
                int PLineX = PLine.startX;
                int amountBoxY2 = amountBox.Y + amountBox.Width;
                int amountBoxX = amountBox.X;
                int amountBoxX2 = amountBoxX + amountBox.Height;
                int PLineX2 = PLineX + PLine.height;
                if (PLineY >= amountBoxY && PLineY <= amountBoxY2 &&
                    PLineX2 > amountBoxX)
                {
                    PLine.height = amountBoxX - PLineX - 5;
                }
                //return array of Lines on same Horizon
                for (int i = 0; i < HLinesC; i++)
                {
                    if (Math.Abs(((Line)HLines[i]).startY - PLineY) <= 3)
                    {
                        if (((Line)HLines[i]).startY >= amountBoxY &&
                            ((Line)HLines[i]).startY <= amountBoxY2 + 2 &&
                            ((Line)HLines[i]).startX + ((Line)HLines[i]).height > amountBoxX)
                        //							((Line)HLines[i]).startX<=amountBoxX2)
                        { }
                        else if (((Line)HLines[i]).startY == PLineY &&
                            ((Line)HLines[i]).startX == PLineX)
                        { }
                        else
                        {
                            //it's on the same line
                            PayeeLineCombo.Add((HLines[i]));
                        }
                    }
                }

                int PayeeLineComboC = PayeeLineCombo.Count;
                int sX = 0;
                int h = 0;
                //more than one line in the array
                if (PayeeLineComboC > 0)
                {
                    for (int i = 0; i < PayeeLineComboC; i++)
                    {
                        bool res = ConnectHLines(PLine, ((Line)PayeeLineCombo[i]), ref sX, ref h);
                        if (res == true)
                        {
                            PLine.height = h;
                            PLine.startX = sX;
                        }
                    }
                }
                return PLine;
            }
            catch
            {
                return null;
            }
        }

        internal static bool ConnectHLines(Line lnRef, Line ln, ref int sX, ref int h)
        {
            try
            {
                if (ln.startX < lnRef.startX)
                {
                    h = Math.Max(lnRef.startX + lnRef.height, ln.startX + ln.height) - Math.Min(ln.startX, lnRef.startX);
                    //h=Math.Max(lnRef.height,ln.height);//(lnRef.startX+lnRef.height)-ln.startX;
                    sX = ln.startX;
                    return true;
                }
                else
                {
                    //					h=(ln.startX+ln.height)-lnRef.startX;
                    //h=Math.Max(lnRef.height,ln.height);
                    h = Math.Max(lnRef.startX + lnRef.height, ln.startX + ln.height) - Math.Min(ln.startX, lnRef.startX);
                    sX = lnRef.startX;
                    return true;
                }
            }
            catch
            {
                h = 0;
                sX = 0;
                return false;
            }
        }

        internal static bool ConnectVLines(Line lnRef, Line ln, ref int sY, ref int h)
        {
            try
            {
                if (ln.startY < lnRef.startY)
                {
                    h = Math.Max(lnRef.startY + lnRef.height, ln.startY + ln.height) - Math.Min(ln.startY, lnRef.startY);
                    //h=Math.Max(lnRef.height,ln.height);//(lnRef.startX+lnRef.height)-ln.startX;
                    sY = ln.startY;
                    return true;
                }
                else
                {
                    //					h=(ln.startX+ln.height)-lnRef.startX;
                    //h=Math.Max(lnRef.height,ln.height);
                    h = Math.Max(lnRef.startY + lnRef.height, ln.startY + ln.height) - Math.Min(ln.startY, lnRef.startY);
                    sY = lnRef.startY;
                    return true;
                }
            }
            catch
            {
                h = 0;
                sY = 0;
                return false;
            }
        }

        internal static int RemoveHLines(Bitmap src, ref Bitmap bresult)
        {
            BitmapData bmresData = null;
            //BitmapData bmData=null;
            try
            {
                DateTime st = DateTime.Now;
                ArrayList HorizontalRuns = new ArrayList();
                int HorizontalRunsC;

                if (src.PixelFormat != PixelFormat.Format8bppIndexed)
                    return 1;

                bresult = (Bitmap)src.Clone();
                bmresData = bresult.LockBits(new Rectangle(0, 0, src.Width, src.Height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);

                RunLength.FindHRun(bmresData, ref HorizontalRuns, 10, false, 0, 0, 0, 0);

                HorizontalRunsC = HorizontalRuns.Count;
                //bool bConnHRL=false;

                for (int i = 0; i < HorizontalRunsC; i++)
                {
                    if (RemoveRunLength(HorizontalRuns, (HRun)(HorizontalRuns[i]), 2, 10, 150, 2) == false)
                    {
                        RemoveHRunFromImage(((HRun)(HorizontalRuns[i])), ref bmresData, 5);
                    }
                }
                /////////////////////////////////
                RemoveOnePixel(ref bmresData, "h");
                ////

                return 0;
            }
            catch
            {
                return 2;
            }
            finally
            {
                //				if (bmData!=null)
                //					src.UnlockBits (bmData);
                if (bmresData != null)
                    bresult.UnlockBits(bmresData);
            }
        }

        internal static int RemoveVLines(Bitmap src, ref Bitmap bresult)
        {
            BitmapData bmresData = null;

            try
            {
                //DateTime st=DateTime.Now;
                ArrayList VerticalRuns = new ArrayList();
                int VerticalRunsC;

                if (src.PixelFormat != PixelFormat.Format8bppIndexed)
                    return 1;

                bresult = (Bitmap)src.Clone();
                bmresData = bresult.LockBits(new Rectangle(0, 0, src.Width, src.Height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);

                RunLength.FindVRun(bmresData, ref VerticalRuns, 20, 0, 0, 0, 0);

                DateTime st = DateTime.Now;
                VerticalRunsC = VerticalRuns.Count;

                for (int i = 0; i < VerticalRunsC; i++)
                {
                    if (RunLength.RemoveRunLength(VerticalRuns, (VRun)(VerticalRuns[i]), 15, 10, 50, 2) == false)
                    {
                        RunLength.RemoveVRunFromImage(((VRun)(VerticalRuns[i])), ref bmresData, 5);
                    }
                }

                //RemoveOnePixel(ref bmresData,"h");

                DateTime et = DateTime.Now;
                TimeSpan sp = et - st;
                //MessageBox.Show(sp.ToString() );

                return 0;
            }
            catch
            {
                return 2;
            }
            finally
            {
                if (bresult != null)
                    bresult.UnlockBits(bmresData);
            }
        }

        internal static int RemoveHVLines(Bitmap src, ref Bitmap bresult)
        {
            //DateTime st=DateTime.Now;
            BitmapData bmresData = null;

            try
            {
                ArrayList VerticalRuns = new ArrayList();
                int VerticalRunsC;
                ArrayList HorizontalRuns = new ArrayList();
                int HorizontalRunsC;

                if (src.PixelFormat != PixelFormat.Format8bppIndexed)
                    return 1;

                bresult = (Bitmap)src.Clone();
                bmresData = bresult.LockBits(new Rectangle(0, 0, src.Width, src.Height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);

                RunLength.FindVRun(bmresData, ref VerticalRuns, 20, 0, 0, 0, 0);
                RunLength.FindHRun(bmresData, ref HorizontalRuns, 20, false, 0, 0, 0, 0);

                //				DateTime st=DateTime.Now ;
                VerticalRunsC = VerticalRuns.Count;
                HorizontalRunsC = HorizontalRuns.Count;

                for (int i = 0; i < VerticalRunsC; i++)
                {
                    if (RunLength.RemoveRunLength(VerticalRuns, (VRun)(VerticalRuns[i]), 15, 10, 50, 2) == false)
                    {
                        RunLength.RemoveVRunFromImage(((VRun)(VerticalRuns[i])), ref bmresData, 10);
                    }
                }
                for (int i = 0; i < HorizontalRunsC; i++)
                {
                    if (RunLength.RemoveRunLength(HorizontalRuns, (HRun)(HorizontalRuns[i]), 15, 10, 150, 2) == false)
                    {
                        RunLength.RemoveHRunFromImage(((HRun)(HorizontalRuns[i])), ref bmresData, 5);
                    }
                }

                RemoveOnePixel(ref bmresData, "h");
                RemoveOnePixel(ref bmresData, "v");
                //				DateTime et=DateTime.Now;
                //				TimeSpan  sp=et-st;
                //				MessageBox.Show(sp.ToString() );
                return 0;
            }
            catch
            {
                return 2;
            }
            finally
            {
                if (bresult != null)
                    bresult.UnlockBits(bmresData);
            }
        }

        internal static void RemoveOnePixel(ref BitmapData bmData, string dir)
        {
            try
            {
                int stride = bmData.Stride;
                System.IntPtr Scan0 = bmData.Scan0;

                byte* p = (byte*)(void*)Scan0;

                int nOffset = stride - bmData.Width;
                int nWidth = bmData.Width;
                int height = bmData.Height;
                if (dir.ToLower() == "h")
                {
                    for (int y = 0; y < height - 1; ++y)
                    {
                        for (int x = 0; x < nWidth; ++x)
                        {
                            if ((p[(y * stride) + x] == 0) && (p[((y + 1) * stride) + x] == 255) && (p[((y - 1) * stride) + x] == 255))
                                p[(y * stride) + x] = 255;
                        }
                    }
                }
                else
                {
                    for (int y = 0; y < height; ++y)
                    {
                        for (int x = 0; x < nWidth - 1; ++x)
                        {
                            if ((p[(y * stride) + x] == 0) && (p[((y) * stride) + (x + 1)] == 255) && (p[((y) * stride) + (x - 1)] == 255))
                                p[(y * stride) + x] = 255;
                        }
                    }
                }
            }
            catch (Exception)
            {
                //MessageBox.Show (exc.Message );
            }
        }

        internal static int DSCC(Bitmap src, ref Bitmap bresult, bool isSevere)
        {
            BitmapData bmresData = null;
            BitmapData bmData = null;
            //DateTime st=DateTime.Now;

            try
            {
                //assume binary image
                if (src.PixelFormat != PixelFormat.Format8bppIndexed)
                    return 1;
                //Bitmap hLines=null;
                bresult = BitmapFactory.CreateGrayImage(src.Width, src.Height);

                ArrayList VerticalRuns = new ArrayList();
                ArrayList HorizontalRuns = new ArrayList();

                //			int indRuns=999;
                // GDI+ still lies to us - the return format is BGR, NOT RGB.
                bmresData = bresult.LockBits(new Rectangle(0, 0, src.Width, src.Height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                bmData = src.LockBits(new Rectangle(0, 0, src.Width, src.Height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                //BitmapData bmresData = bresult.LockBits(new Rectangle(0, 0, src.Width, src.Height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed );

                //Make the new Bitmap White
                int stride = bmresData.Stride;
                System.IntPtr Scan0 = bmresData.Scan0;

                unsafe
                {
                    byte* p = (byte*)(void*)Scan0;

                    int nOffset = stride - bmresData.Width;
                    int nWidth = bmresData.Width;
                    int height = bmresData.Height;

                    for (int y = 0; y < height; ++y)
                    {
                        for (int x = 0; x < nWidth; ++x)
                        {
                            p[(y * stride) + x] = 255;
                        }
                    }
                }

                //1. Horizontal Lines
                ///////////////////////Note//////////////////////
                //////for HL removel directly apply on the original image/////

                //method to return the vertical runs
                //FindVRun(bmData,ref VerticalRuns ,2);

                RunLength.FindHRun(bmData, ref HorizontalRuns, 20, isSevere, 0, 0, 0, 0);

                //FillRunsInImage(VerticalRuns ,ref bRData );

                //Testing:
                //Filling the Horizontal runs into an image
                int HorizontalRunsC = HorizontalRuns.Count;
                for (int i = 0; i < HorizontalRunsC; i++)
                {
                    if (RunLength.RemoveRunLength(HorizontalRuns, (HRun)(HorizontalRuns[i]), 5, 10, 150, 2) == false)
                    {
                        RunLength.FillHRunInImage(((HRun)(HorizontalRuns[i])), ref bmresData);
                    }
                }

                //2. Vertical Lines
                //FindVRun(bmData,ref VerticalRuns,2 );

                //Unlocking data

                //MessageBox.Show(sp.ToString() );

                //Return success
                return 0;
            }
            catch
            {
                return 2;
            }
            finally
            {
                if (bmData != null)
                    src.UnlockBits(bmData);
                if (bmresData != null)
                    bresult.UnlockBits(bmresData);
            }
        }
    }
}