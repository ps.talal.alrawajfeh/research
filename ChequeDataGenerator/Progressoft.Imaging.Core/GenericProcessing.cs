﻿using Progressoft.Imaging.Core.Common;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;

namespace Progressoft.Imaging.Core
{
    public static unsafe class GenericProcessing
    {
        /// <summary>
        /// Invert The Image Color.
        /// Works On 8, and 24 Bit.
        /// </summary>
        /// <param name="inputImage">The Base Image To Be Inverted.</param>
        ///<param name="InvertedBmp">The Inverted Bitmap.</param>
        /// <returns>Error Code:
        /// 0: Success.
        /// 1: Pixel Format Not Valid.
        /// 2: Generic Error.</returns>
        public static Bitmap InvertColor(Bitmap inputImage)
        {
            Bitmap outputImage = null;
            BitmapData bmData = null;
            try
            {
                if (inputImage.PixelFormat != PixelFormat.Format24bppRgb &&
                    inputImage.PixelFormat != PixelFormat.Format8bppIndexed &&
                    inputImage.PixelFormat != PixelFormat.Format1bppIndexed)
                    return null;

                int h = inputImage.Height;

                outputImage = inputImage.Clone() as Bitmap;
                bmData = outputImage.LockBits(new Rectangle(0, 0, outputImage.Width, h), ImageLockMode.ReadWrite, outputImage.PixelFormat);

                int stride = bmData.Stride;
                byte* p = (byte*)(void*)bmData.Scan0;

                for (int y = 0; y < h; ++y)
                    for (int x = 0; x < stride; ++x, p++)
                        p[0] = (byte)~p[0];

                outputImage.UnlockBits(bmData);
                return outputImage;
            }
            catch
            {
                return null;
            }
        }

        public static Rectangle GetBoundaries(Bitmap bitmap, byte backgroundColor = 255, bool invertedColor = false)
        {
            BitmapData bitmapData = null;

            try
            {
                var width = bitmap.Width;
                var height = bitmap.Height;

                bitmapData = bitmap.LockBits(
                    new Rectangle(0, 0, width, height),
                    ImageLockMode.ReadWrite,
                    PixelFormat.Format8bppIndexed);

                var offset = bitmapData.Stride - width;

                var ptr = (byte*)bitmapData.Scan0.ToPointer();
                var minX = width;
                var maxX = 0;
                var minY = height;
                var maxY = 0;

                for (var y = 0; y < height; y++)
                {
                    for (var x = 0; x < width; x++)
                    {
                        if (!invertedColor && ptr[0] < backgroundColor
                            || invertedColor && ptr[0] > backgroundColor)
                        {
                            if (x < minX)
                                minX = x;
                            if (y < minY)
                                minY = y;
                            if (x > maxX)
                                maxX = x;
                            if (y > maxY)
                                maxY = y;
                        }

                        ptr++;
                    }

                    ptr += offset;
                }

                var rectWidth = maxX - minX + 1;
                var rectHeight = maxY - minY + 1;
                return new Rectangle(minX, minY, rectWidth, rectHeight);
            }
            catch
            {
                return new Rectangle(0, 0, 0, 0);
            }
            finally
            {
                if (bitmapData != null)
                {
                    bitmap.UnlockBits(bitmapData);
                }
            }
        }

        public static Rectangle GetBoundaries(Bitmap inputImage, int verticalThreshold, int horizontalThreshold)
        {
            BitmapData bmData = null;
            Rectangle imgRect = new Rectangle(0, 0, inputImage.Width, inputImage.Height);

            try
            {
                int xInt = 0;
                int yInt = 0;
                int xEnd = 0;
                int yEnd = 0;
                if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed)
                    return imgRect;

                if (verticalThreshold < 0 || horizontalThreshold < 0)
                    return imgRect;
                //Get The Boundaries

                int w = inputImage.Width;
                int h = inputImage.Height;

                bmData = inputImage.LockBits(new Rectangle(0, 0, inputImage.Width, inputImage.Height),
                    ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);

                int stride = bmData.Stride;

                System.IntPtr Scan0 = bmData.Scan0;
                unsafe
                {
                    byte* p = (byte*)(void*)Scan0;
                    int ctr = 0;

                    #region Xint

                    for (int x = 0; x < w; x++)
                    {
                        ctr = 0;
                        for (int y = 0; y < h; y++)
                        {
                            if (p[(y * stride) + x] == 0)
                                ctr++;
                        } //for y
                        if (ctr > verticalThreshold)
                        {
                            ctr = -1;
                            xInt = x;
                            break;
                        }
                        if (ctr == -1)
                        {
                            break;
                        }
                    } //for x
                    if (ctr == -1)
                    {
                        ctr = 0;
                    }
                    else
                    {
                        xInt = 0;
                    }

                    #endregion Xint

                    #region Xend

                    for (int x = w - 1; x > -1; x--)
                    {
                        ctr = 0;
                        for (int y = 0; y < h; y++)
                        {
                            if (p[(y * stride) + x] == 0)
                                ctr++;
                        } //for y
                        if (ctr > verticalThreshold)
                        {
                            ctr = -1;
                            xEnd = x;
                            break;
                        }
                        if (ctr == -1)
                        {
                            break;
                        }
                    } //for x
                    if (ctr == -1)
                    {
                        ctr = 0;
                    }
                    else
                    {
                        xEnd = w;
                    }

                    #endregion Xend

                    #region yInt

                    for (int y = 0; y < h; y++)
                    {
                        ctr = 0;
                        for (int x = 0; x < w; x++)
                        {
                            if (p[(y * stride) + x] == 0)
                                ctr++;
                        } //for y
                        if (ctr > horizontalThreshold)
                        {
                            ctr = -1;
                            yInt = y;
                            break;
                        }
                        if (ctr == -1)
                        {
                            break;
                        }
                    } //for x
                    if (ctr == -1)
                    {
                        ctr = 0;
                    }
                    else
                    {
                        yInt = 0;
                    }

                    #endregion yInt

                    #region yEnd

                    for (int y = h - 1; y > -1; y--)
                    {
                        ctr = 0;
                        for (int x = 0; x < w; x++)
                        {
                            if (p[(y * stride) + x] == 0)
                                ctr++;
                        } //for y
                        if (ctr > horizontalThreshold)
                        {
                            ctr = -1;
                            yEnd = y;
                            break;
                        }
                        if (ctr == -1)
                        {
                            break;
                        }
                    } //for x
                    if (ctr != -1)
                        yEnd = h;

                    #endregion yEnd
                } //unsafe

                Rectangle result = new Rectangle(xInt, yInt, xEnd - xInt + 1, yEnd - yInt + 1);
                if (!imgRect.Contains(result))
                    result = imgRect;
                return result;
            } //try
            catch
            {
                return imgRect;
            }
            finally
            {
                if (bmData != null)
                    inputImage.UnlockBits(bmData);
            } //finally
        }

        public static float GetBlackPixelsPercentage(Bitmap inputImage)
        {
            return GetGrayValuePercentage(inputImage, 0);
        }

        public static float GetWhitePixelsPercentage(Bitmap inputImage)
        {
            return GetGrayValuePercentage(inputImage, 255);
        }

        public static float GetGrayValuePercentage(Bitmap inputImage, byte grayValue)
        {
            BitmapData bmData = null;
            try
            {
                if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed)
                    return float.MinValue;

                int w = inputImage.Width;
                int h = inputImage.Height;

                bmData = inputImage.LockBits(new Rectangle(0, 0, w, h),
                    ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);

                int stride = bmData.Stride;
                float nBlackCount = 0;
                System.IntPtr Scan0 = bmData.Scan0;
                byte* p = (byte*)(void*)Scan0;

                for (int x = 0; x < w; x++)
                {
                    for (int y = 0; y < h; y++)
                    {
                        if (p[(y * stride) + x] == grayValue)
                            nBlackCount++;
                    } //for y
                } //for x
                inputImage.UnlockBits(bmData);
                return nBlackCount / (w * h);
            }//try
            catch
            {
                return float.MinValue;
            }
        }

        public static Bitmap Masking(Bitmap originalImage, Bitmap maskImage, byte maskValue, byte nonMaskValue)
        {
            try
            {
                if (originalImage.PixelFormat != PixelFormat.Format8bppIndexed ||
                    maskImage.PixelFormat != PixelFormat.Format8bppIndexed ||
                    originalImage.Width != maskImage.Width ||
                    originalImage.Height != maskImage.Height)
                    return null;

                int width = originalImage.Width;
                int height = originalImage.Height;

                Bitmap outputBmp = BitmapFactory.CreateGrayImage(width, height);

                BitmapData bmpDataOrg = originalImage.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                BitmapData bmpDataMask = maskImage.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                BitmapData bmpDataOut = outputBmp.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);

                byte* pOrg = (byte*)bmpDataOrg.Scan0.ToPointer();
                byte* pMask = (byte*)bmpDataMask.Scan0.ToPointer();
                byte* pOut = (byte*)bmpDataOut.Scan0.ToPointer();
                int offset = bmpDataOrg.Stride - width;

                for (int y = 0; y < height; y++, pOrg += offset, pMask += offset, pOut += offset)
                    for (int x = 0; x < width; x++, pOrg++, pMask++, pOut++)
                        pOut[0] = (pMask[0] == maskValue) ? pOrg[0] : nonMaskValue;

                originalImage.UnlockBits(bmpDataOrg);
                maskImage.UnlockBits(bmpDataMask);
                outputBmp.UnlockBits(bmpDataOut);
                return outputBmp;
            }
            catch
            {
                return null;
            }
        }

        public static Bitmap CompareAndSet(Bitmap inputImage1, Bitmap inputImage2, byte valueToCompare, byte valueToSetIfEqual)
        {
            try
            {
                // check for the validity of the given format:
                if (inputImage1.PixelFormat != PixelFormat.Format8bppIndexed ||
                    inputImage2.PixelFormat != PixelFormat.Format8bppIndexed ||
                    inputImage1.Width != inputImage2.Width ||
                    inputImage1.Height != inputImage2.Height)
                    return null; // invalid pixel format or uncompatible images.

                int width = inputImage1.Width;
                int height = inputImage1.Height;

                // create the output image:
                Bitmap outputBmp = (Bitmap)inputImage1.Clone();

                // lock images to be processed:
                BitmapData bmpData1 = inputImage1.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                System.IntPtr scan01 = bmpData1.Scan0;
                int offset = bmpData1.Stride - width;

                BitmapData bmpData2 = inputImage2.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                System.IntPtr scan02 = bmpData2.Scan0;

                BitmapData outBmpData = outputBmp.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                System.IntPtr outScan0 = outBmpData.Scan0;

                byte* p1 = (byte*)(void*)scan01;
                byte* p2 = (byte*)(void*)scan02;
                byte* outP = (byte*)(void*)outScan0;
                for (int y = 0; y < height; y++, p1 += offset, p2 += offset, outP += offset)
                    for (int x = 0; x < width; x++, p1++, p2++, outP++)
                        if (p1[0] == valueToCompare && p2[0] == valueToCompare)
                            outP[0] = valueToSetIfEqual;

                inputImage1.UnlockBits(bmpData1);
                inputImage2.UnlockBits(bmpData2);
                outputBmp.UnlockBits(outBmpData);
                return outputBmp;
            }
            catch
            {
                return null;
            }
        }

        public static Bitmap CompareAndSetWithOriginalIfEqual(Bitmap originalImage, Bitmap imageToCompare, byte valueToCompare)
        {
            try
            {
                // check for the validity of the given format:
                if (originalImage.PixelFormat != PixelFormat.Format8bppIndexed ||
                    imageToCompare.PixelFormat != PixelFormat.Format8bppIndexed ||
                    originalImage.Width != imageToCompare.Width ||
                    originalImage.Height != imageToCompare.Height)
                    return null; // invalid pixel format or uncompatible images.

                int width = originalImage.Width;
                int height = originalImage.Height;

                // create the output image:
                Bitmap outputBmp = (Bitmap)originalImage.Clone();

                // lock images to be processed:
                BitmapData bmpData1 = originalImage.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                System.IntPtr scan01 = bmpData1.Scan0;
                int offset = bmpData1.Stride - width;

                BitmapData bmpData2 = imageToCompare.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                System.IntPtr scan02 = bmpData2.Scan0;

                BitmapData outBmpData = outputBmp.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                System.IntPtr outScan0 = outBmpData.Scan0;

                byte* p1 = (byte*)(void*)scan01;
                byte* p2 = (byte*)(void*)scan02;
                byte* outP = (byte*)(void*)outScan0;
                for (int y = 0; y < height; y++, p1 += offset, p2 += offset, outP += offset)
                    for (int x = 0; x < width; x++, p1++, p2++, outP++)
                        if (p2[0] == valueToCompare)
                            outP[0] = p1[0];
                        else
                            outP[0] = p2[0];

                originalImage.UnlockBits(bmpData1);
                imageToCompare.UnlockBits(bmpData2);
                outputBmp.UnlockBits(outBmpData);
                return outputBmp;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Clean the given components from image.
        /// </summary>
        /// <param name="bmpImage">expected to be binary</param>
        /// <param name="components">list of components to remove</param>
        /// <returns></returns>
        public static Bitmap RemoveComponents(
            Bitmap bitmap,
            List<ConnectedComponent> components,
            byte backgroundColor = 255)
        {
            BitmapData bitmapData = null;
            var output = (Bitmap)bitmap.Clone();

            try
            {
                if (components == null || bitmap.PixelFormat != PixelFormat.Format8bppIndexed)
                    return null;

                var width = bitmap.Width;
                var height = bitmap.Height;

                var lockRect = new Rectangle(0, 0, width, height);
                bitmapData = output.LockBits(
                    lockRect,
                    ImageLockMode.ReadWrite,
                    PixelFormat.Format8bppIndexed);

                var stride = bitmapData.Stride;
                var ptr = (byte*)bitmapData.Scan0.ToPointer();

                foreach (var component in components)
                {
                    if (!lockRect.Contains(component.Rectangle))
                        continue;

                    foreach (var point in component.Points)
                    {
                        ptr[point.Y * stride + point.X] = backgroundColor;
                    }
                }

                return output;
            }
            catch
            {
                return null;
            }
            finally
            {
                if (bitmapData != null)
                {
                    output.UnlockBits(bitmapData);
                }
            }
        }

        /// <summary>
        /// find and Clean black components from image. Only components that match
        /// the condition will be cleaned.
        /// </summary>
        /// <param name="inputImage">expected to be binary</param>
        /// <param name="shouldClean">Only components that match
        /// this condition will be cleaned.</param>
        /// <returns></returns>
        public static Bitmap FindAndRemoveComponents(Bitmap inputImage, Predicate<ConnectedComponent> shouldClean)
        {
            if (inputImage == null ||
                inputImage.PixelFormat != PixelFormat.Format8bppIndexed)
                return null;

            // get components ...
            List<ConnectedComponent> allComponents = ConnectedComponentAnalysis.GetComponents(inputImage, 0);
            // get count of components ...
            int iCompsCount = allComponents.Count;

            // mark small components as to be removed ...
            for (int i = 0; i < iCompsCount; i++)
                allComponents[i].AddFlag("remove", shouldClean(allComponents[i]));

            // remove the small components ...
            return RemoveComponents(inputImage, allComponents.Where(c => c.GetFlag<bool>("remove")).ToList());
        }

        public static void CenterOfGravity(List<ConnectedComponent> components, string cogFlag)
        {
            components.ForEach(c => c.AddFlag(cogFlag, PointsMathmatics.GetCenterOfGraphity(c.Points)));
        }

        public static Point CenterOfGravity(Bitmap inputImage)
        {
            Point pnt = new Point(0, 0);
            try
            {
                if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed)
                    return pnt;

                int width = inputImage.Width;
                int height = inputImage.Height;

                long sumFirst = 0;
                long sumY = 0;
                long sum = 0;

                BitmapData bmData = inputImage.LockBits(new Rectangle(0, 0, inputImage.Width, inputImage.Height), ImageLockMode.ReadWrite, inputImage.PixelFormat);

                int stride = bmData.Stride;
                System.IntPtr Scan0 = bmData.Scan0;

                byte* p = (byte*)(void*)Scan0;
                int nOffset = stride - inputImage.Width;

                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        sumFirst += (255 - p[0]) * (x + 1);
                        sumY += (255 - p[0]) * (y + 1);
                        sum += 255 - p[0];
                        p++;
                    }
                    p += nOffset;
                }

                if (sum != 0)
                {
                    pnt.X = Convert.ToInt32(sumFirst / sum);
                    pnt.Y = Convert.ToInt32(sumY / sum);
                }
                inputImage.UnlockBits(bmData);
                return pnt;
            }
            catch
            {
                return pnt;
            }
        }

        public static Bitmap PasteImageOver(Bitmap inputImage, Bitmap toPaste, int x, int y, int width, int height)
        {
            Rectangle imageRect = new Rectangle(0, 0, inputImage.Width + 1, inputImage.Height + 1);
            Rectangle rectangle = new Rectangle(x, y, width - x, height - y);
            if (!imageRect.Contains(rectangle))
                return null;
            Bitmap outImage;
            Bitmap image24Bits = ColorDepthConversion.To24Bits(inputImage);
            Graphics graphics = Graphics.FromImage(image24Bits);
            graphics.DrawImage(toPaste, x, y, width, height);
            switch (inputImage.PixelFormat)
            {
                case PixelFormat.Format8bppIndexed:
                    outImage = ColorDepthConversion.To8Bits(image24Bits);
                    break;

                case PixelFormat.Format1bppIndexed:
                    outImage = ColorDepthConversion.To1Bit(image24Bits);
                    break;

                default:
                    outImage = image24Bits.Clone() as Bitmap;
                    break;
            }
            image24Bits.Dispose();
            graphics.Dispose();
            return outImage;
        }

        public static Bitmap PasteImageOver(Bitmap inputImage, Bitmap toPaste, int x, int y)
        {
            return PasteImageOver(inputImage, toPaste, x, y, toPaste.Width, toPaste.Height);
        }

        public static Bitmap PasteImageOver(Bitmap inputImage, Bitmap toPaste, Point start)
        {
            return PasteImageOver(inputImage, toPaste, start.X, start.Y);
        }

        public static Bitmap PasteImageOver(Bitmap inputImage, Bitmap toPaste, Rectangle rectangle)
        {
            return PasteImageOver(inputImage, toPaste, rectangle.X, rectangle.Y, rectangle.Width, rectangle.Height);
        }

        private static bool AreAllPixelsEqualInVerticalStrip(
            byte* ptr,
            int stride,
            int x,
            int y1,
            int y2,
            byte color = 0)
        {
            ptr += x;

            var allEqual = true;
            for (var y = y1; y < y2; y++)
            {
                if (ptr[0] != color)
                {
                    allEqual = false;
                    break;
                }

                ptr += stride;
            }

            return allEqual;
        }

        private static bool AreAllPixelsEqualInHorizontalStrip(
            byte* ptr,
            int stride,
            int y,
            int x1,
            int x2,
            byte color = 0)
        {
            ptr += stride * y + x1;

            var allEqual = true;
            for (var x = x1; x < x2; x++)
            {
                if (ptr[0] != color)
                {
                    allEqual = false;
                    break;
                }

                ptr++;
            }

            return allEqual;
        }

        public static Rectangle AutoInflateRectangle(Bitmap bitmap, Rectangle rectangle, byte backgroundColor = 0)
        {
            var height = bitmap.Height;
            var width = bitmap.Width;

            var (x1, y1, x2, y2) = (rectangle.Left, rectangle.Top, rectangle.Right, rectangle.Bottom);

            BitmapData bitmapData = null;

            try
            {
                if (bitmap.PixelFormat != PixelFormat.Format8bppIndexed) return new Rectangle(0, 0, 0, 0);

                bitmapData = bitmap.LockBits(
                    new Rectangle(0, 0, width, height),
                    ImageLockMode.ReadWrite,
                    bitmap.PixelFormat);

                var stride = bitmapData.Stride;
                var ptr = (byte*)bitmapData.Scan0.ToPointer();

                while (y1 > 0 &&
                       !AreAllPixelsEqualInHorizontalStrip(ptr, stride, y1 - 1, x1, x2, backgroundColor))
                {
                    y1--;
                }

                while (x1 > 0 &&
                       !AreAllPixelsEqualInVerticalStrip(ptr, stride, x1 - 1, y1, y2, backgroundColor))
                {
                    x1--;
                }

                while (y2 < height &&
                       !AreAllPixelsEqualInHorizontalStrip(ptr, stride, y2, x1, x2, backgroundColor))
                {
                    y2++;
                }

                while (x2 < width &&
                       !AreAllPixelsEqualInVerticalStrip(ptr, stride, x2, y1, y2, backgroundColor))
                {
                    x2++;
                }

                if (x1 >= x2 || y1 >= y2)
                {
                    return new Rectangle(0, 0, 0, 0);
                }

                return new Rectangle(x1, y1, x2 - x1, y2 - y1);
            }
            catch
            {
                return new Rectangle(0, 0, 0, 0);
            }
            finally
            {
                if (bitmapData != null)
                {
                    bitmap.UnlockBits(bitmapData);
                }
            }
        }
    }
}