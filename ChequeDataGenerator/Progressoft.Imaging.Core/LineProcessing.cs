﻿using System;
using System.Drawing;
using System.Drawing.Imaging;

namespace Progressoft.Imaging.Core
{
    public static unsafe class LineProcessing
    {
        /// <summary>
        /// extract vertical & horizontal line map ...
        /// </summary>
        /// <param name="inputImage">expected to be binary</param>
        /// <param name="filterSize">filter size for horizontal & vertical morphology operations </param>
        /// <returns></returns>
        public static Bitmap ExtractLinesMap(Bitmap inputImage, int filterSize)
        {
            return ExtractLinesMap(inputImage, filterSize, filterSize);
        }

        /// <summary>
        /// extract vertical & horizontal line map ...
        /// </summary>
        /// <param name="inputImage">expected to be binary</param>
        /// <param name="horizontalFilterSize"> filter size for horizontal morphology operations</param>
        /// <param name="verticalFilterSize">filter size for vertical morphology operations</param>
        /// <returns></returns>
        public static Bitmap ExtractLinesMap(Bitmap inputImage, int horizontalFilterSize, int verticalFilterSize)
        {
            if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed ||
                horizontalFilterSize < 2 || verticalFilterSize < 2)
                return null;
            Bitmap hLinesErosion = ExtractHorizontalLinesMap(inputImage, horizontalFilterSize);
            Bitmap vLinesErosion = ExtractVerticalLinesMap(inputImage, verticalFilterSize);
            Bitmap outputImage = BitWiseOperations.BitWiseAnding(hLinesErosion, vLinesErosion);
            hLinesErosion.Dispose();
            vLinesErosion.Dispose();
            return outputImage;
        }

        /// <summary>
        /// extract horizontal line map ...
        /// </summary>
        /// <param name="inputImage">expected to be binary</param>
        /// <param name="horizontalFilterSize">filter size for horizontal morphology operations</param>
        /// <returns></returns>
        public static Bitmap ExtractHorizontalLinesMap(Bitmap inputImage, int horizontalFilterSize)
        {
            if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed ||
                horizontalFilterSize < 2)
                return null;
            Bitmap hLinesErosion1 = Morphology.BinaryHorizontalErosion(inputImage, horizontalFilterSize);
            Bitmap hLinesErosion2 = Morphology.BinaryHorizontalDilation(hLinesErosion1, horizontalFilterSize);
            hLinesErosion1.Dispose();
            return hLinesErosion2;
        }

        /// <summary>
        /// extract horizontal line map ...
        /// </summary>
        /// <param name="inputImage">expected to be binary</param>
        /// <param name="verticalFilterSize">filter size for vertical morphology operations</param>
        /// <returns></returns>
        public static Bitmap ExtractVerticalLinesMap(Bitmap inputImage, int verticalFilterSize)
        {
            if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed ||
                verticalFilterSize < 2)
                return null;
            Bitmap vLinesErosion1 = Morphology.BinaryVerticalErosion(inputImage, verticalFilterSize);
            Bitmap vLinesErosion2 = Morphology.BinaryVerticalDilation(vLinesErosion1, verticalFilterSize);
            vLinesErosion1.Dispose();
            return vLinesErosion2;
        }

        /// <summary>
        /// detect & remove vertical & horizontal lines from binary image.
        /// </summary>
        /// <param name="inputImage">expected to be binary image.</param>
        /// <param name="horizontalFilterSize"> filter size for horizontal morphology operations</param>
        /// <param name="verticalFilterSize">filter size for vertical morphology operations</param>
        /// <param name="replaceBy">gray value to replace lines-pixels.</param>
        /// <returns></returns>
        public static Bitmap RemoveLines(Bitmap inputImage, int horizontalFilterSize, int verticalFilterSize, byte replaceBy)
        {
            if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed ||
                horizontalFilterSize < 2 || verticalFilterSize < 2)
                return null;

            Bitmap bmpLinesMap = ExtractLinesMap(inputImage, horizontalFilterSize, verticalFilterSize);
            Bitmap bmpNoLines = GenericProcessing.CompareAndSet(inputImage, bmpLinesMap, 0, replaceBy);
            bmpLinesMap.Dispose();

            return bmpNoLines;
        }

        /// <summary>
        /// detect & remove vertical & horizontal lines from binary image.
        /// </summary>
        /// <param name="inputImage">expected to be binary image.</param>
        /// <param name="filterSize"> filter size for horizontal & vertical morphology operations</param>
        /// <param name="replaceBy">gray value to replace lines-pixels.</param>
        /// <returns></returns>
        public static Bitmap RemoveLines(Bitmap inputImage, int filterSize, byte replaceBy)
        {
            return RemoveLines(inputImage, filterSize, filterSize, replaceBy);
        }

        /// <summary>
        /// detect & remove horizontal lines from binary image.
        /// </summary>
        /// <param name="inputImage">expected to be binary image.</param>
        /// <param name="horizontalFilterSize">filter size for horizontal morphology operations</param>
        /// <param name="replaceBy">gray value to replace lines-pixels</param>
        /// <returns></returns>
        public static Bitmap RemoveHorizontalLines(Bitmap inputImage, int horizontalFilterSize, byte replaceBy)
        {
            if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed ||
                horizontalFilterSize < 2)
                return null;

            Bitmap bmpLinesMap = ExtractHorizontalLinesMap(inputImage, horizontalFilterSize);
            Bitmap bmpNoLines = GenericProcessing.CompareAndSet(inputImage, bmpLinesMap, 0, replaceBy);
            bmpLinesMap.Dispose();

            return bmpNoLines;
        }

        /// <summary>
        /// detect & remove vertical lines from binary image.
        /// </summary>
        /// <param name="inputImage">expected to be binary image.</param>
        /// <param name="verticalFilterSize">filter size for vertical morphology operations</param>
        /// <param name="replaceBy">gray value to replace lines-pixels</param>
        /// <returns></returns>
        public static Bitmap RemoveVerticalLines(Bitmap inputImage, int verticalFilterSize, byte replaceBy)
        {
            if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed ||
                verticalFilterSize < 2)
                return null;
            Bitmap bmpLinesMap = ExtractVerticalLinesMap(inputImage, verticalFilterSize);
            Bitmap bmpNoLines = GenericProcessing.CompareAndSet(inputImage, bmpLinesMap, 0, replaceBy);
            bmpLinesMap.Dispose();
            return bmpNoLines;
        }

        /// <summary>
		/// Applys linear Hough transform on 8-bit image to detect a skew angle in the specified
		/// range. This algorithm depends on lines rather than text density. Note
		/// that the first scan line will be skipped.
		/// </summary>
		/// <param name="bmpImg">
		/// An 8-bit bitmap image to setect its skew angle.
		/// </param>
		/// <param name="skewInterval">
		/// A positive integer number to search for the skew angle in the interval
		/// [-skewInterval,skewInterval], skewInterval should not exceed 45 degrees.
		/// Smaller intervlas require less time to process.
		/// </param>
		/// <param name="step_size">
		/// A positive float number in the range [0,1] indecates each one degree will
		/// be divided to (1/step_size) sub-degrees for accuracy reasons.
		/// Larger steps require less time to process.
		/// </param>
		/// <param name="expectedLineWidth">
		/// The expected width of the line that you search for such that it lies in
		/// the range[0, image-width].Small lines require less time to process.
		/// </param>
		/// <param name="topVerticalAreaToSkip">
		/// A positive float number in the range [0,1] indecates how mutch area should
		/// be skiped from the top of the image. Larger areas to skip require less
		/// time to process.
		/// </param>
		/// <param name="buttomVerticalAreaToSkip">
		/// A positive float number in the range [0,1] indecates how mutch area should
		/// be skiped from the bottom of the image. Larger areas to skip require less
		/// time to process.
		/// </param>
		/// <param name="leftHorizontalAreaToSkip">
		/// A positive float number in the range [0,1] indecates how mutch area should
		/// be skiped from the left of the image. Larger areas to skip require less
		/// time to process.
		/// </param>
		/// <param name="rightHorizontalAreaToSkip">
		/// A positive float number in the range [0,1] indecates how mutch area should
		/// be skiped from the right of the image. Larger areas to skip require less
		/// time to process.
		/// </param>
		/// <param name="verticalHop">
		/// A positive integer in the range [1,15] represents a gap of height
		/// "verticalHop" between horizontal scan lines will be skiped. Greater hops
		/// require less time to process.
		/// </param>
		/// <param name="horizontalHop">
		/// A positive integer in the range [1,15] represents a gap of width
		/// "horizontalHop" between vertical scan lines will be skiped. Greater hops
		/// require less time to process.
		/// </param>
		/// <param name="skipIfTopPixelIsForground">
		/// If true, the pixel above the pixel being proceesed will be examined, if it
		/// is a foreground pixel the current pixel being processed will be skiped
		/// assuming that the two pixels belong to the same line(e.g. a line of
		/// height 2 pixel).
		/// </param>
		/// <param name="threshold">
		/// A positive number in the range[0,255] indecates that pixels have
		/// intensity values less than it will be assumed forground pixels.
		/// </param>
		/// <returns>
		/// reurn skew angle.
		/// </returns>
		public static float ComputeSkewAngleUsingHoughTransforms
            (Bitmap bmpImg, int skewInterval, float step_size, int expectedLineWidth,
            float topVerticalAreaToSkip, float buttomVerticalAreaToSkip,
            float leftHorizontalAreaToSkip, float rightHorizontalAreaToSkip,
            int verticalHop, int horizontalHop,
            bool skipIfTopPixelIsForground, byte threshold)
        {
            try
            {
                // check for th validity of the given parameters:
                if (bmpImg == null || bmpImg.PixelFormat != PixelFormat.Format8bppIndexed ||
                    topVerticalAreaToSkip < 0 || topVerticalAreaToSkip > 1 ||
                    buttomVerticalAreaToSkip < 0 || buttomVerticalAreaToSkip > 1 ||
                    leftHorizontalAreaToSkip < 0 || leftHorizontalAreaToSkip > 1 ||
                    rightHorizontalAreaToSkip < 0 || rightHorizontalAreaToSkip > 1 ||
                    verticalHop < 1 || verticalHop > 15 ||
                    horizontalHop < 1 || horizontalHop > 15 ||
                    step_size < 0.1 || step_size > 1 ||
                    expectedLineWidth < 1 || expectedLineWidth > bmpImg.Width ||
                    skewInterval < 0 || skewInterval > 45)
                    return float.MinValue; // one or more illegal parameter was sent.

                // get width and height:
                int width = bmpImg.Width;
                int height = bmpImg.Height;

                // lock the bitmap to be processed:
                BitmapData bmpData = bmpImg.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, bmpImg.PixelFormat);
                System.IntPtr scan0 = bmpData.Scan0;
                int stride = bmpData.Stride;

                // compute area to skip from top, bottom, left and right:
                int skipTop = (int)(topVerticalAreaToSkip * height);
                // we must at least start from 1, because we read the top line:
                if (skipTop == 0)
                    skipTop = 1;
                int skipBottom = (int)(buttomVerticalAreaToSkip * height);
                int skipLeft = (int)(leftHorizontalAreaToSkip * width);
                int skipRight = (int)(rightHorizontalAreaToSkip * width);

                // thetas to be used:
                float T_1 = 90f - skewInterval, T_2 = 90f, T_3 = 90 + skewInterval;

                // used frequently to convert to radians:
                const float PI_D_180 = (float)Math.PI / 180f;

                // how many step in each degree:
                int position_factor = (int)(1f / step_size),
                    theta_numbers = (int)((T_3 + 1) / step_size); // degrees to be considred.

                // the accumollator array for the Hough transform:
                int[,] accum = new int[expectedLineWidth, theta_numbers];

                // temp. veriables:
                float newTheta = -1f;
                float radians = -1f;
                int r = -1;
                int y_T_stride = 0;
                int y_M1_T_stride = 0;

                byte* p = (byte*)(void*)scan0;

                for (int y = skipTop; y < height - skipBottom; y += verticalHop)
                {
                    y_T_stride = y * stride;
                    y_M1_T_stride = (y - 1) * stride;
                    for (int x = skipLeft; x < width - skipRight; x += horizontalHop)
                    {
                        if (p[y_T_stride + x] < threshold)
                        {
                            if (p[y_M1_T_stride + x] < threshold && skipIfTopPixelIsForground)
                                continue;

                            newTheta = T_1;
                            for (float theta = newTheta; theta <= T_2; theta += step_size)
                            {
                                radians = theta * PI_D_180;
                                r = (int)((x * Math.Cos(radians)) + (y * Math.Sin(radians)));
                                if (r > -1 && r < expectedLineWidth)
                                    accum[r, (int)(theta * position_factor)]++;
                            }

                            newTheta = T_2 + step_size;

                            for (float theta = newTheta; theta <= T_3; theta += step_size)
                            {
                                radians = theta * PI_D_180;
                                r = (int)((x * Math.Cos(radians)) + (y * Math.Sin(radians)));
                                if (r > -1 && r < expectedLineWidth)
                                    accum[r, (int)(theta * position_factor)]++;
                            }
                        }
                    }
                }

                bmpImg.UnlockBits(bmpData);

                // get the maximum peek in the accumolator array:
                int maximum = -1, t_max = -1, t = -1;
                int theta_number_M_1 = theta_numbers - 1;
                for (float i = T_1; i <= T_2; i += step_size)

                {
                    t = (int)(i / step_size);
                    if (t > theta_number_M_1) continue;
                    for (int r1 = 1; r1 < expectedLineWidth; r1++)
                    {
                        if (accum[r1, t] > maximum)
                        {
                            maximum = accum[r1, t];
                            t_max = t;
                        }
                    }
                }

                for (float i = (T_2 + step_size); i <= T_3; i += step_size)
                {
                    t = (int)(i / step_size);
                    if (t > theta_number_M_1) continue;
                    for (int r2 = 1; r2 < expectedLineWidth; r2++)
                    {
                        if (accum[r2, t] > maximum)
                        {
                            maximum = accum[r2, t];
                            t_max = t;
                        }
                    }
                }

                // compute the skew angle:
                float skewAngle = t_max / (float)position_factor;

                if (skewAngle > 0f && skewAngle < 91f)// first quarter.
                    skewAngle = 90f - skewAngle;
                else if (skewAngle > 90f && skewAngle < 180f)// second quarter
                    skewAngle = -1 * (skewAngle - 90f);

                return skewAngle;
            }
            catch
            {
                return float.MinValue;
            }
        }
    }
}