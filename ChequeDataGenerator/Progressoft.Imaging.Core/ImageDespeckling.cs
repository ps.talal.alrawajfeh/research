﻿using System.Drawing;
using System.Drawing.Imaging;

namespace Progressoft.Imaging.Core
{
    public static unsafe class ImageDespeckling
    {
        /// <summary>
        /// Despeckle black components.
        /// </summary>
        /// <param name="inputImage">expected to be binary</param>
        /// <param name="smallSpeckleSize">if speckle's size is less than or equal to this number it will be cleaned.</param>
        /// <returns></returns>
        public static Bitmap DespeckleByComponents(
            Bitmap inputImage,
            int smallSpeckleSize)
        {
            return GenericProcessing.FindAndRemoveComponents(inputImage,
                        c => c.Points.Count <= smallSpeckleSize);
        }

        /// <summary>
        /// Despeckle black components.
        /// </summary>
        /// <param name="inputImage">expected to be binary</param>
        /// <param name="smallSpeckleSize">if speckle's size is less than or equal to this number it will be cleaned.</param>
        /// <param name="smallSpeckleWidth">if speckle's width is less than or equal to this number it will be cleaned.</param>
        /// <param name="smallSpeckleHeight">if speckle's height is less than or equal to this number it will be cleaned.</param>
        /// <returns></returns>
        public static Bitmap DespeckleByComponents(
            Bitmap inputImage,
            int smallSpeckleSize,
            int smallSpeckleWidth,
            int smallSpeckleHeight)
        {
            return GenericProcessing.FindAndRemoveComponents(inputImage,
                        c => c.Points.Count <= smallSpeckleSize ||
                        c.Rectangle.Width <= smallSpeckleWidth ||
                        c.Rectangle.Height <= smallSpeckleHeight);
        }

        public static Bitmap DespeckleByCrimmins(Bitmap inputImage, int iterations)
        {
            if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed)
                return null;

            int iWidth = inputImage.Width;
            int iHeight = inputImage.Height;

            Bitmap bmpTmp1 = inputImage.Clone() as Bitmap;
            Bitmap bmpTmp2 = inputImage.Clone() as Bitmap;

            Rectangle rectLock = new Rectangle(0, 0, iWidth, iHeight);
            BitmapData bmpData1 = bmpTmp1.LockBits(rectLock, ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
            BitmapData bmpData2 = bmpTmp2.LockBits(rectLock, ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);

            int iStride = bmpData1.Stride;

            for (int its = 0; its < iterations; its++)
            {
                // round 1 ...
                //------------

                byte* p1 = (byte*)(void*)bmpData1.Scan0;
                byte* p2 = (byte*)(void*)bmpData2.Scan0;

                int iOffset = iStride - iWidth + 1;
                p1++;
                p2++;

                for (int y = 0; y < iHeight; ++y)
                {
                    for (int x = 1; x < iWidth; ++x)
                    {
                        if ((p1 - 1)[0] > p1[0] + 1)
                        {
                            p2[0] = (byte)(p1[0] + 1);
                        }
                        else
                        {
                            p2[0] = p1[0];
                        }

                        p1++;
                        p2++;
                    }
                    p1 += iOffset;
                    p2 += iOffset;
                }

                // round 2 ...
                //------------
                p1 = (byte*)(void*)bmpData1.Scan0;
                p2 = (byte*)(void*)bmpData2.Scan0;

                iOffset = iStride - iWidth + 2;
                p1++;
                p2++;

                for (int y = 0; y < iHeight; ++y)
                {
                    for (int x = 1; x < iWidth - 1; ++x)
                    {
                        if ((p2 - 1)[0] > p2[0] &&
                            (p2 + 1)[0] >= p2[0])
                        {
                            p1[0] = (byte)(p2[0] + 1);
                        }
                        else
                        {
                            p1[0] = p2[0];
                        }

                        p1++;
                        p2++;
                    }
                    p1 += iOffset;
                    p2 += iOffset;
                }

                // round 3 ...
                //------------
                p1 = (byte*)(void*)bmpData1.Scan0;
                p2 = (byte*)(void*)bmpData2.Scan0;

                iOffset = iStride - iWidth + 2;
                p1++;
                p2++;

                for (int y = 0; y < iHeight; ++y)
                {
                    for (int x = 1; x < iWidth - 1; ++x)
                    {
                        if ((p1 - 1)[0] >= p1[0] &&
                            (p1 + 1)[0] > p1[0])
                        {
                            p2[0] = (byte)(p1[0] + 1);
                        }
                        else
                        {
                            p2[0] = p1[0];
                        }

                        p1++;
                        p2++;
                    }
                    p1 += iOffset;
                    p2 += iOffset;
                }

                // round 4 ...
                //------------
                p1 = (byte*)(void*)bmpData1.Scan0;
                p2 = (byte*)(void*)bmpData2.Scan0;

                iOffset = iStride - iWidth + 1;

                for (int y = 0; y < iHeight; ++y)
                {
                    for (int x = 0; x < iWidth - 1; ++x)
                    {
                        if ((p2 + 1)[0] > p2[0] + 1)
                        {
                            p1[0] = (byte)(p2[0] + 1);
                        }
                        else
                        {
                            p1[0] = p2[0];
                        }

                        p1++;
                        p2++;
                    }
                    p1 += iOffset;
                    p2 += iOffset;
                }

                // round 5 ...
                //------------
                p1 = (byte*)(void*)bmpData1.Scan0;
                p2 = (byte*)(void*)bmpData2.Scan0;

                iOffset = iStride - iWidth + 1;
                p1++;
                p2++;

                for (int y = 0; y < iHeight; ++y)
                {
                    for (int x = 1; x < iWidth; ++x)
                    {
                        if ((p1 - 1)[0] < p1[0] - 1)
                        {
                            p2[0] = (byte)(p1[0] - 1);
                        }
                        else
                        {
                            p2[0] = p1[0];
                        }

                        p1++;
                        p2++;
                    }
                    p1 += iOffset;
                    p2 += iOffset;
                }

                // round 6 ...
                //------------
                p1 = (byte*)(void*)bmpData1.Scan0;
                p2 = (byte*)(void*)bmpData2.Scan0;

                iOffset = iStride - iWidth + 2;
                p1++;
                p2++;

                for (int y = 0; y < iHeight; ++y)
                {
                    for (int x = 1; x < iWidth - 1; ++x)
                    {
                        if ((p2 - 1)[0] < p2[0] &&
                            (p2 + 1)[0] <= p2[0])
                        {
                            p1[0] = (byte)(p2[0] - 1);
                        }
                        else
                        {
                            p1[0] = p2[0];
                        }

                        p1++;
                        p2++;
                    }
                    p1 += iOffset;
                    p2 += iOffset;
                }

                // round 7 ...
                //------------
                p1 = (byte*)(void*)bmpData1.Scan0;
                p2 = (byte*)(void*)bmpData2.Scan0;

                iOffset = iStride - iWidth + 2;
                p1++;
                p2++;

                for (int y = 0; y < iHeight; ++y)
                {
                    for (int x = 1; x < iWidth - 1; ++x)
                    {
                        if ((p1 - 1)[0] <= p1[0] &&
                            (p1 + 1)[0] < p1[0])
                        {
                            p2[0] = (byte)(p1[0] - 1);
                        }
                        else
                        {
                            p2[0] = p1[0];
                        }

                        p1++;
                        p2++;
                    }
                    p1 += iOffset;
                    p2 += iOffset;
                }

                // round 8 ...
                //------------
                p1 = (byte*)(void*)bmpData1.Scan0;
                p2 = (byte*)(void*)bmpData2.Scan0;

                iOffset = iStride - iWidth + 1;
                p1++;
                p2++;

                for (int y = 0; y < iHeight; ++y)
                {
                    for (int x = 0; x < iWidth - 1; ++x)
                    {
                        if ((p2 + 1)[0] < p2[0] - 1)
                        {
                            p1[0] = (byte)(p2[0] - 1);
                        }
                        else
                        {
                            p1[0] = p2[0];
                        }

                        p1++;
                        p2++;
                    }
                    p1 += iOffset;
                    p2 += iOffset;
                }

                // round 9 ...
                //----------------------------------------------------------
                p1 = (byte*)(void*)bmpData1.Scan0;
                p2 = (byte*)(void*)bmpData2.Scan0;

                iOffset = iStride - iWidth;
                p1 += iStride;
                p2 += iStride;

                for (int y = 1; y < iHeight; ++y)
                {
                    for (int x = 0; x < iWidth; ++x)
                    {
                        if ((p1 - iStride)[0] > p1[0] + 1)
                        {
                            p2[0] = (byte)(p1[0] + 1);
                        }
                        else
                        {
                            p2[0] = p1[0];
                        }

                        p1++;
                        p2++;
                    }
                    p1 += iOffset;
                    p2 += iOffset;
                }

                // round 10 ...
                //------------
                p1 = (byte*)(void*)bmpData1.Scan0;
                p2 = (byte*)(void*)bmpData2.Scan0;

                iOffset = iStride - iWidth;
                p1 += iStride;
                p2 += iStride;

                for (int y = 1; y < iHeight - 1; ++y)
                {
                    for (int x = 0; x < iWidth; ++x)
                    {
                        if ((p2 - iStride)[0] > p2[0] &&
                            (p2 + iStride)[0] >= p2[0])
                        {
                            p1[0] = (byte)(p2[0] + 1);
                        }
                        else
                        {
                            p1[0] = p2[0];
                        }

                        p1++;
                        p2++;
                    }
                    p1 += iOffset;
                    p2 += iOffset;
                }

                // round 10 ...
                //------------
                p1 = (byte*)(void*)bmpData1.Scan0;
                p2 = (byte*)(void*)bmpData2.Scan0;

                iOffset = iStride - iWidth;
                p1 += iStride;
                p2 += iStride;

                for (int y = 1; y < iHeight - 1; ++y)
                {
                    for (int x = 0; x < iWidth; ++x)
                    {
                        if ((p1 - iStride)[0] >= p1[0] &&
                            (p1 + iStride)[0] > p1[0])
                        {
                            p2[0] = (byte)(p1[0] + 1);
                        }
                        else
                        {
                            p2[0] = p1[0];
                        }

                        p1++;
                        p2++;
                    }
                    p1 += iOffset;
                    p2 += iOffset;
                }

                // round 11 ...
                //------------
                p1 = (byte*)(void*)bmpData1.Scan0;
                p2 = (byte*)(void*)bmpData2.Scan0;

                iOffset = iStride - iWidth;

                for (int y = 0; y < iHeight - 1; ++y)
                {
                    for (int x = 0; x < iWidth; ++x)
                    {
                        if ((p2 + iStride)[0] > p2[0] + 1)
                        {
                            p1[0] = (byte)(p2[0] + 1);
                        }
                        else
                        {
                            p1[0] = p2[0];
                        }

                        p1++;
                        p2++;
                    }
                    p1 += iOffset;
                    p2 += iOffset;
                }

                // round 12 ...
                //------------
                p1 = (byte*)(void*)bmpData1.Scan0;
                p2 = (byte*)(void*)bmpData2.Scan0;

                iOffset = iStride - iWidth;
                p1 += iStride;
                p2 += iStride;

                for (int y = 1; y < iHeight; ++y)
                {
                    for (int x = 0; x < iWidth; ++x)
                    {
                        if ((p1 - iStride)[0] < p1[0] - 1)
                        {
                            p2[0] = (byte)(p1[0] - 1);
                        }
                        else
                        {
                            p2[0] = p1[0];
                        }

                        p1++;
                        p2++;
                    }
                    p1 += iOffset;
                    p2 += iOffset;
                }

                // round 13 ...
                //-------------
                p1 = (byte*)(void*)bmpData1.Scan0;
                p2 = (byte*)(void*)bmpData2.Scan0;

                iOffset = iStride - iWidth;
                p1 += iStride;
                p2 += iStride;

                for (int y = 1; y < iHeight - 1; ++y)
                {
                    for (int x = 0; x < iWidth; ++x)
                    {
                        if ((p2 - iStride)[0] < p2[0] &&
                            (p2 + iStride)[0] <= p2[0])
                        {
                            p1[0] = (byte)(p2[0] - 1);
                        }
                        else
                        {
                            p1[0] = p2[0];
                        }

                        p1++;
                        p2++;
                    }
                    p1 += iOffset;
                    p2 += iOffset;
                }

                // round 14 ...
                //-------------
                p1 = (byte*)(void*)bmpData1.Scan0;
                p2 = (byte*)(void*)bmpData2.Scan0;

                iOffset = iStride - iWidth;
                p1 += iStride;
                p2 += iStride;

                for (int y = 1; y < iHeight - 1; ++y)
                {
                    for (int x = 0; x < iWidth; ++x)
                    {
                        if ((p1 - iStride)[0] <= p1[0] &&
                            (p1 + iStride)[0] < p1[0])
                        {
                            p2[0] = (byte)(p1[0] - 1);
                        }
                        else
                        {
                            p2[0] = p1[0];
                        }

                        p1++;
                        p2++;
                    }

                    p1 += iOffset;
                    p2 += iOffset;
                }

                // round 15 ...
                //-------------
                p1 = (byte*)(void*)bmpData1.Scan0;
                p2 = (byte*)(void*)bmpData2.Scan0;

                iOffset = iStride - iWidth;

                for (int y = 0; y < iHeight - 1; ++y)
                {
                    for (int x = 0; x < iWidth; ++x)
                    {
                        if ((p2 + iStride)[0] < p2[0] - 1)
                        {
                            p1[0] = (byte)(p2[0] - 1);
                        }
                        else
                        {
                            p1[0] = p2[0];
                        }

                        p1++;
                        p2++;
                    }

                    p1 += iOffset;
                    p2 += iOffset;
                }

                // round 16 ...
                //-------------
                p1 = (byte*)(void*)bmpData1.Scan0;
                p2 = (byte*)(void*)bmpData2.Scan0;

                iOffset = iStride - iWidth + 1;

                p1 += iStride;
                p2 += iStride;

                for (int y = 1; y < iHeight; ++y)
                {
                    for (int x = 0; x < iWidth - 1; ++x)
                    {
                        if ((p1 + 1 - iStride)[0] > p1[0] + 1)
                        {
                            p2[0] = (byte)(p1[0] + 1);
                        }
                        else
                        {
                            p2[0] = p1[0];
                        }

                        p1++;
                        p2++;
                    }
                    p1 += iOffset;
                    p2 += iOffset;
                }

                // round 17 ...
                //-------------
                p1 = (byte*)(void*)bmpData1.Scan0;
                p2 = (byte*)(void*)bmpData2.Scan0;

                iOffset = iStride - iWidth + 2;

                p1 += iStride + 1;
                p2 += iStride + 1;

                for (int y = 1; y < iHeight - 1; ++y)
                {
                    for (int x = 1; x < iWidth - 1; ++x)
                    {
                        if ((p2 + 1 - iStride)[0] > p2[0] &&
                            (p2 - 1 + iStride)[0] >= p2[0])
                        {
                            p1[0] = (byte)(p2[0] + 1);
                        }
                        else
                        {
                            p1[0] = p2[0];
                        }
                        p1++;
                        p2++;
                    }
                    p1 += iOffset;
                    p2 += iOffset;
                }

                // round 18 ...
                //-------------
                p1 = (byte*)(void*)bmpData1.Scan0;
                p2 = (byte*)(void*)bmpData2.Scan0;

                iOffset = iStride - iWidth + 2;

                p1 += iStride + 1;
                p2 += iStride + 1;

                for (int y = 1; y < iHeight - 1; ++y)
                {
                    for (int x = 1; x < iWidth - 1; ++x)
                    {
                        if ((p1 + 1 - iStride)[0] >= p1[0] &&
                             (p1 - 1 + iStride)[0] > p1[0])
                        {
                            p2[0] = (byte)(p1[0] + 1);
                        }
                        else
                        {
                            p2[0] = p1[0];
                        }

                        p1++;
                        p2++;
                    }
                    p1 += iOffset;
                    p2 += iOffset;
                }

                // round 19 ...
                //-------------
                p1 = (byte*)(void*)bmpData1.Scan0;
                p2 = (byte*)(void*)bmpData2.Scan0;

                iOffset = iStride - iWidth + 1;

                p1++;
                p2++;

                for (int y = 0; y < iHeight - 1; ++y)
                {
                    for (int x = 1; x < iWidth; ++x)
                    {
                        if ((p2 - 1 + iStride)[0] > p2[0] + 1)
                        {
                            p1[0] = (byte)(p2[0] + 1);
                        }
                        else
                        {
                            p1[0] = p2[0];
                        }

                        p1++;
                        p2++;
                    }

                    p1 += iOffset;
                    p2 += iOffset;
                }

                // round 20 ...
                //-------------
                p1 = (byte*)(void*)bmpData1.Scan0;
                p2 = (byte*)(void*)bmpData2.Scan0;

                iOffset = iStride - iWidth + 1;

                p1 += iStride;
                p2 += iStride;

                for (int y = 1; y < iHeight; ++y)
                {
                    for (int x = 0; x < iWidth - 1; ++x)
                    {
                        if ((p1 + 1 - iStride)[0] < p1[0] - 1)
                        {
                            p2[0] = (byte)(p1[0] - 1);
                        }
                        else
                        {
                            p2[0] = p1[0];
                        }

                        p1++;
                        p2++;
                    }

                    p1 += iOffset;
                    p2 += iOffset;
                }

                // round 21 ...
                //-------------
                p1 = (byte*)(void*)bmpData1.Scan0;
                p2 = (byte*)(void*)bmpData2.Scan0;

                iOffset = iStride - iWidth + 2;

                p1 += iStride + 1;
                p2 += iStride + 1;

                for (int y = 1; y < iHeight - 1; ++y)
                {
                    for (int x = 1; x < iWidth - 1; ++x)
                    {
                        if ((p2 + 1 - iStride)[0] < p2[0] &&
                            (p2 - 1 + iStride)[0] <= p2[0])
                        {
                            p1[0] = (byte)(p2[0] - 1);
                        }
                        else
                        {
                            p1[0] = p2[0];
                        }

                        p1++;
                        p2++;
                    }
                    p1 += iOffset;
                    p2 += iOffset;
                }

                // round 22 ...
                //-------------
                p1 = (byte*)(void*)bmpData1.Scan0;
                p2 = (byte*)(void*)bmpData2.Scan0;

                iOffset = iStride - iWidth + 2;

                p1 += iStride + 1;
                p2 += iStride + 1;

                for (int y = 1; y < iHeight - 1; ++y)
                {
                    for (int x = 1; x < iWidth - 1; ++x)
                    {
                        if ((p1 + 1 - iStride)[0] <= p1[0] &&
                             (p1 - 1 + iStride)[0] < p1[0])
                        {
                            p2[0] = (byte)(p1[0] - 1);
                        }
                        else
                        {
                            p2[0] = p1[0];
                        }

                        p1++;
                        p2++;
                    }
                    p1 += iOffset;
                    p2 += iOffset;
                }

                // round 22 ...
                //-------------
                p1 = (byte*)(void*)bmpData1.Scan0;
                p2 = (byte*)(void*)bmpData2.Scan0;

                iOffset = iStride - iWidth + 1;

                p1++;
                p2++;

                for (int y = 0; y < iHeight - 1; ++y)
                {
                    for (int x = 1; x < iWidth; ++x)
                    {
                        if ((p2 - 1 + iStride)[0] < p2[0] - 1)
                        {
                            p1[0] = (byte)(p2[0] - 1);
                        }
                        else
                        {
                            p1[0] = p2[0];
                        }

                        p1++;
                        p2++;
                    }
                    p1 += iOffset;
                    p2 += iOffset;
                }

                // round 23 ...
                //-------------
                p1 = (byte*)(void*)bmpData1.Scan0;
                p2 = (byte*)(void*)bmpData2.Scan0;

                iOffset = iStride - iWidth + 1;

                for (int y = 0; y < iHeight - 1; ++y)
                {
                    for (int x = 0; x < iWidth - 1; ++x)
                    {
                        if ((p1 + 1 + iStride)[0] > p1[0] + 1)
                        {
                            p2[0] = (byte)(p1[0] + 1);
                        }
                        else
                        {
                            p2[0] = p1[0];
                        }

                        p1++;
                        p2++;
                    }
                    p1 += iOffset;
                    p2 += iOffset;
                }

                // round 23 ...
                //-------------
                p1 = (byte*)(void*)bmpData1.Scan0;
                p2 = (byte*)(void*)bmpData2.Scan0;

                iOffset = iStride - iWidth + 2;

                p1 += iStride + 1;
                p2 += iStride + 1;

                for (int y = 1; y < iHeight - 1; ++y)
                {
                    for (int x = 1; x < iWidth - 1; ++x)
                    {
                        if ((p2 + 1 + iStride)[0] > p2[0] &&
                            (p2 - 1 - iStride)[0] >= p2[0])
                        {
                            p1[0] = (byte)(p2[0] + 1);
                        }
                        else
                        {
                            p1[0] = p2[0];
                        }
                        p1++;
                        p2++;
                    }
                    p1 += iOffset;
                    p2 += iOffset;
                }

                // round 24 ...
                //-------------
                p1 = (byte*)(void*)bmpData1.Scan0;
                p2 = (byte*)(void*)bmpData2.Scan0;

                iOffset = iStride - iWidth + 2;

                p1 += iStride + 1;
                p2 += iStride + 1;

                for (int y = 1; y < iHeight - 1; ++y)
                {
                    for (int x = 1; x < iWidth - 1; ++x)
                    {
                        if ((p1 + 1 + iStride)[0] >= p1[0] &&
                            (p1 - 1 - iStride)[0] > p1[0])
                        {
                            p2[0] = (byte)(p1[0] + 1);
                        }
                        else
                        {
                            p2[0] = p1[0];
                        }

                        p1++;
                        p2++;
                    }
                    p1 += iOffset;
                    p2 += iOffset;
                }

                // round 25 ...
                //-------------
                p1 = (byte*)(void*)bmpData1.Scan0;
                p2 = (byte*)(void*)bmpData2.Scan0;

                iOffset = iStride - iWidth + 1;

                p1 += iStride + 1;
                p2 += iStride + 1;

                for (int y = 1; y < iHeight; ++y)
                {
                    for (int x = 1; x < iWidth; ++x)
                    {
                        if ((p2 - 1 - iStride)[0] > p2[0] + 1)
                        {
                            p1[0] = (byte)(p2[0] + 1);
                        }
                        else
                        {
                            p1[0] = p2[0];
                        }
                        p1++;
                        p2++;
                    }
                    p1 += iOffset;
                    p2 += iOffset;
                }

                // round 25 ...
                //-------------
                p1 = (byte*)(void*)bmpData1.Scan0;
                p2 = (byte*)(void*)bmpData2.Scan0;

                iOffset = iStride - iWidth + 1;

                for (int y = 0; y < iHeight - 1; ++y)
                {
                    for (int x = 0; x < iWidth - 1; ++x)
                    {
                        if ((p1 + 1 + iStride)[0] < p1[0] - 1)
                        {
                            p2[0] = (byte)(p1[0] - 1);
                        }
                        else
                        {
                            p2[0] = p1[0];
                        }
                        p1++;
                        p2++;
                    }
                    p1 += iOffset;
                    p2 += iOffset;
                }

                // round 26 ...
                //-------------
                p1 = (byte*)(void*)bmpData1.Scan0;
                p2 = (byte*)(void*)bmpData2.Scan0;

                iOffset = iStride - iWidth + 2;

                p1 += iStride + 1;
                p2 += iStride + 1;

                for (int y = 1; y < iHeight - 1; ++y)
                {
                    for (int x = 1; x < iWidth - 1; ++x)
                    {
                        if ((p2 + 1 + iStride)[0] < p2[0] &&
                            (p2 - 1 - iStride)[0] <= p2[0])
                        {
                            p1[0] = (byte)(p2[0] - 1);
                        }
                        else
                        {
                            p1[0] = p2[0];
                        }
                        p1++;
                        p2++;
                    }
                    p1 += iOffset;
                    p2 += iOffset;
                }

                // round 27 ...
                //-------------
                p1 = (byte*)(void*)bmpData1.Scan0;
                p2 = (byte*)(void*)bmpData2.Scan0;

                iOffset = iStride - iWidth + 2;

                p1 += iStride + 1;
                p2 += iStride + 1;

                for (int y = 1; y < iHeight - 1; ++y)
                {
                    for (int x = 1; x < iWidth - 1; ++x)
                    {
                        if ((p1 + 1 + iStride)[0] <= p1[0] &&
                            (p1 - 1 - iStride)[0] < p1[0])
                        {
                            p2[0] = (byte)(p1[0] - 1);
                        }
                        else
                        {
                            p2[0] = p1[0];
                        }

                        p1++;
                        p2++;
                    }
                    p1 += iOffset;
                    p2 += iOffset;
                }

                // round 28 ...
                //-------------
                p1 = (byte*)(void*)bmpData1.Scan0;
                p2 = (byte*)(void*)bmpData2.Scan0;

                iOffset = iStride - iWidth + 1;

                p1 += iStride + 1;
                p2 += iStride + 1;

                for (int y = 1; y < iHeight; ++y)
                {
                    for (int x = 1; x < iWidth; ++x)
                    {
                        if ((p2 - 1 - iStride)[0] < p2[0] - 1)
                        {
                            p1[0] = (byte)(p2[0] - 1);
                        }
                        else
                        {
                            p1[0] = p2[0];
                        }

                        p1++;
                        p2++;
                    }
                    p1 += iOffset;
                    p2 += iOffset;
                }
            }

            bmpTmp1.UnlockBits(bmpData1);
            bmpTmp2.UnlockBits(bmpData2);

            bmpTmp2.Dispose();

            return bmpTmp1;
        }
    }
}