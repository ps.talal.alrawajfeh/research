﻿using System;
using System.Diagnostics;

namespace Progressoft.Imaging.Core.Fourier
{
    internal enum FourierDirection
    {
        Forward = 1,
        Backward = -1,
    }

    internal static class FourierUtils
    {
        private static void Swap(ref float a, ref float b)
        {
            float temp = a;
            a = b;
            b = temp;
        }

        //-------------------------------------------------------------------------------------

        private const int cMaxLength = 4096;
        private const int cMinLength = 1;

        private const int cMaxBits = 12;
        private const int cMinBits = 0;

        private static bool IsPowerOf2(int x)
        {
            return (x == Pow2(Log2(x)));
        }

        private static int Pow2(int exponent)
        {
            if (exponent >= 0 && exponent < 31)
            {
                return 1 << exponent;
            }
            return 0;
        }

        private static int Log2(int x)
        {
            if (x <= 65536)
            {
                if (x <= 256)
                {
                    if (x <= 16)
                    {
                        if (x <= 4)
                        {
                            if (x <= 2)
                            {
                                if (x <= 1)
                                {
                                    return 0;
                                }
                                return 1;
                            }
                            return 2;
                        }
                        if (x <= 8)
                            return 3;
                        return 4;
                    }
                    if (x <= 64)
                    {
                        if (x <= 32)
                            return 5;
                        return 6;
                    }
                    if (x <= 128)
                        return 7;
                    return 8;
                }
                if (x <= 4096)
                {
                    if (x <= 1024)
                    {
                        if (x <= 512)
                            return 9;
                        return 10;
                    }
                    if (x <= 2048)
                        return 11;
                    return 12;
                }
                if (x <= 16384)
                {
                    if (x <= 8192)
                        return 13;
                    return 14;
                }
                if (x <= 32768)
                    return 15;
                return 16;
            }
            if (x <= 16777216)
            {
                if (x <= 1048576)
                {
                    if (x <= 262144)
                    {
                        if (x <= 131072)
                            return 17;
                        return 18;
                    }
                    if (x <= 524288)
                        return 19;
                    return 20;
                }
                if (x <= 4194304)
                {
                    if (x <= 2097152)
                        return 21;
                    return 22;
                }
                if (x <= 8388608)
                    return 23;
                return 24;
            }
            if (x <= 268435456)
            {
                if (x <= 67108864)
                {
                    if (x <= 33554432)
                        return 25;
                    return 26;
                }
                if (x <= 134217728)
                    return 27;
                return 28;
            }
            if (x <= 1073741824)
            {
                if (x <= 536870912)
                    return 29;
                return 30;
            }
            return 31;
        }

        //-------------------------------------------------------------------------------------

        private static readonly int[][] _reversedBits = new int[cMaxBits][];

        private static int[] GetReversedBits(int numberOfBits)
        {
            Debug.Assert(numberOfBits >= cMinBits);
            Debug.Assert(numberOfBits <= cMaxBits);
            if (_reversedBits[numberOfBits - 1] == null)
            {
                int maxBits = Pow2(numberOfBits);
                int[] reversedBits = new int[maxBits];
                for (int i = 0; i < maxBits; i++)
                {
                    int oldBits = i;
                    int newBits = 0;
                    for (int j = 0; j < numberOfBits; j++)
                    {
                        newBits = (newBits << 1) | (oldBits & 1);
                        oldBits >>= 1;
                    }
                    reversedBits[i] = newBits;
                }
                _reversedBits[numberOfBits - 1] = reversedBits;
            }
            return _reversedBits[numberOfBits - 1];
        }

        //-------------------------------------------------------------------------------------

        private static void ReorderArray(float[] data)
        {
            Debug.Assert(data != null);

            int length = data.Length / 2;

            Debug.Assert(IsPowerOf2(length));
            Debug.Assert(length >= cMinLength);
            Debug.Assert(length <= cMaxLength);

            int[] reversedBits = FourierUtils.GetReversedBits(FourierUtils.Log2(length));
            for (int i = 0; i < length; i++)
            {
                int swap = reversedBits[i];
                if (swap > i)
                {
                    FourierUtils.Swap(ref data[(i << 1)], ref data[(swap << 1)]);
                    FourierUtils.Swap(ref data[(i << 1) + 1], ref data[(swap << 1) + 1]);
                }
            }
        }

        private static void ReorderArray(ComplexF[] data)
        {
            Debug.Assert(data != null);

            int length = data.Length;

            Debug.Assert(FourierUtils.IsPowerOf2(length));
            Debug.Assert(length >= cMinLength);
            Debug.Assert(length <= cMaxLength);

            int[] reversedBits = FourierUtils.GetReversedBits(Log2(length));
            for (int i = 0; i < length; i++)
            {
                int swap = reversedBits[i];
                if (swap > i)
                {
                    ComplexF temp = data[i];
                    data[i] = data[swap];
                    data[swap] = temp;
                }
            }
        }

        //======================================================================================

        private static int[][] _reverseBits = null;

        private static int _ReverseBits(int bits, int n)
        {
            int bitsReversed = 0;
            for (int i = 0; i < n; i++)
            {
                bitsReversed = (bitsReversed << 1) | (bits & 1);
                bits >>= 1;
            }
            return bitsReversed;
        }

        private static void InitializeReverseBits(int levels)
        {
            _reverseBits = new int[levels + 1][];
            for (int j = 0; j < (levels + 1); j++)
            {
                int count = (int)Math.Pow(2, j);
                _reverseBits[j] = new int[count];
                for (int i = 0; i < count; i++)
                {
                    _reverseBits[j][i] = _ReverseBits(i, j);
                }
            }
        }

        private static int _lookupTabletLength = -1;
        private static double[,][] _uRLookup = null;
        private static double[,][] _uILookup = null;
        private static float[,][] _uRLookupF = null;
        private static float[,][] _uILookupF = null;

        private static void SyncLookupTableLength(int length)
        {
            if (length > _lookupTabletLength)
            {
                int level = (int)Math.Ceiling(Math.Log(length, 2));
                InitializeReverseBits(level);
                InitializeComplexRotations(level);
                _lookupTabletLength = length;
            }
        }

        private static void InitializeComplexRotations(int levels)
        {
            int ln = levels;

            _uRLookup = new double[levels + 1, 2][];
            _uILookup = new double[levels + 1, 2][];

            _uRLookupF = new float[levels + 1, 2][];
            _uILookupF = new float[levels + 1, 2][];

            int N = 1;
            for (int level = 1; level <= ln; level++)
            {
                int M = N;
                N <<= 1;

                double uR = 1;
                double uI = 0;
                double angle = Math.PI / M * 1;
                double wR = Math.Cos(angle);
                double wI = Math.Sin(angle);

                _uRLookup[level, 0] = new double[M];
                _uILookup[level, 0] = new double[M];
                _uRLookupF[level, 0] = new float[M];
                _uILookupF[level, 0] = new float[M];

                for (int j = 0; j < M; j++)
                {
                    double tempUr = _uRLookup[level, 0][j] = uR;
                    _uRLookupF[level, 0][j] = (float)tempUr;

                    double tempUi = _uILookup[level, 0][j] = uI;
                    _uILookupF[level, 0][j] = (float)tempUi;
                    double uwI = uR * wI + uI * wR;
                    uR = uR * wR - uI * wI;
                    uI = uwI;
                }

                // negative sign ( i.e. [M,1] )
                uR = 1;
                uI = 0;
                angle = Math.PI / M * -1;
                wR = Math.Cos(angle);
                wI = Math.Sin(angle);

                _uRLookup[level, 1] = new double[M];
                _uILookup[level, 1] = new double[M];
                _uRLookupF[level, 1] = new float[M];
                _uILookupF[level, 1] = new float[M];

                for (int j = 0; j < M; j++)
                {
                    double tempUr = _uRLookup[level, 1][j] = uR;
                    _uRLookupF[level, 1][j] = (float)tempUr;

                    double tempUi = _uILookup[level, 1][j] = uI;
                    _uILookupF[level, 1][j] = (float)tempUi;
                    double uwI = uR * wI + uI * wR;
                    uR = uR * wR - uI * wI;
                    uI = uwI;
                }
            }
        }

        //======================================================================================
        //======================================================================================

        private static bool _bufferFLocked = false;
        private static float[] _bufferF = new float[0];

        private static void LockBufferF(int length, ref float[] buffer)
        {
            Debug.Assert(!_bufferFLocked);
            _bufferFLocked = true;
            if (length >= _bufferF.Length)
            {
                _bufferF = new float[length];
            }
            buffer = _bufferF;
        }

        private static void UnlockBufferF(ref float[] buffer)
        {
            Debug.Assert(_bufferF == buffer);
            Debug.Assert(_bufferFLocked);
            _bufferFLocked = false;
            buffer = null;
        }

        private static void LinearFFT(float[] data, int start, int inc, int length, FourierDirection direction)
        {
            Debug.Assert(data != null);
            Debug.Assert(start >= 0);
            Debug.Assert(inc >= 1);
            Debug.Assert(length >= 1);
            Debug.Assert((start + inc * (length - 1)) * 2 < data.Length);

            // copy to buffer
            float[] buffer = null;
            LockBufferF(length * 2, ref buffer);
            int j = start;
            for (int i = 0; i < length * 2; i++)
            {
                buffer[i] = data[j];
                j += inc;
            }

            FFT(buffer, length, direction);

            // copy from buffer
            j = start;
            for (int i = 0; i < length; i++)
            {
                data[j] = buffer[i];
                j += inc;
            }
            UnlockBufferF(ref buffer);
        }

        //======================================================================================
        //======================================================================================

        private static bool _bufferCFLocked = false;
        private static ComplexF[] _bufferCF = new ComplexF[0];

        private static void LockBufferCF(int length, ref ComplexF[] buffer)
        {
            Debug.Assert(length >= 0);
            Debug.Assert(!_bufferCFLocked);

            _bufferCFLocked = true;
            if (length != _bufferCF.Length)
            {
                _bufferCF = new ComplexF[length];
            }
            buffer = _bufferCF;
        }

        private static void UnlockBufferCF(ref ComplexF[] buffer)
        {
            Debug.Assert(_bufferCF == buffer);
            Debug.Assert(_bufferCFLocked);

            _bufferCFLocked = false;
            buffer = null;
        }

        private static void LinearFFT(ComplexF[] data, int start, int inc, int length, FourierDirection direction)
        {
            Debug.Assert(data != null);
            Debug.Assert(start >= 0);
            Debug.Assert(inc >= 1);
            Debug.Assert(length >= 1);
            Debug.Assert((start + inc * (length - 1)) < data.Length);

            // copy to buffer
            ComplexF[] buffer = null;
            LockBufferCF(length, ref buffer);
            int j = start;
            for (int i = 0; i < length; i++)
            {
                buffer[i] = data[j];
                j += inc;
            }

            FFT(buffer, length, direction);

            // copy from buffer
            j = start;
            for (int i = 0; i < length; i++)
            {
                data[j] = buffer[i];
                j += inc;
            }
            UnlockBufferCF(ref buffer);
        }

        /// <summary>
        /// Compute a 1D fast Fourier transform of a dataset of complex numbers (as pairs of float's).
        /// </summary>
        /// <param name="data"></param>
        /// <param name="length"></param>
        /// <param name="direction"></param>
        public static void FFT(float[] data, int length, FourierDirection direction)
        {
            Debug.Assert(data != null);
            Debug.Assert(data.Length >= length * 2);
            Debug.Assert(IsPowerOf2(length));

            FourierUtils.SyncLookupTableLength(2048);

            int ln = FourierUtils.Log2(length);

            // reorder array
            FourierUtils.ReorderArray(data);

            // successive doubling
            int N = 1;
            int signIndex = (direction == FourierDirection.Forward) ? 0 : 1;
            for (int level = 1; level <= ln; level++)
            {
                int M = N;
                N <<= 1;

                float[] uRLookup = _uRLookupF[level, signIndex];
                float[] uILookup = _uILookupF[level, signIndex];

                for (int j = 0; j < M; j++)
                {
                    float uR = uRLookup[j];
                    float uI = uILookup[j];

                    for (int evenT = j; evenT < length; evenT += N)
                    {
                        int even = evenT << 1;
                        int odd = (evenT + M) << 1;

                        float r = data[odd];
                        float i = data[odd + 1];

                        float odduR = r * uR - i * uI;
                        float odduI = r * uI + i * uR;

                        r = data[even];
                        i = data[even + 1];

                        data[even] = r + odduR;
                        data[even + 1] = i + odduI;

                        data[odd] = r - odduR;
                        data[odd + 1] = i - odduI;
                    }
                }
            }
        }

        /// <summary>
        /// Compute a 1D fast Fourier transform of a dataset of complex numbers.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="length"></param>
        /// <param name="direction"></param>
        public static void FFT(ComplexF[] data, int length, FourierDirection direction)
        {
            if (data == null)
            {
                throw new ArgumentNullException("data");
            }
            if (data.Length < length)
            {
                throw new ArgumentOutOfRangeException("length", length, "must be at least as large as 'data.Length' parameter");
            }
            if (!IsPowerOf2(length))
            {
                throw new ArgumentOutOfRangeException("length", length, "must be a power of 2");
            }

            FourierUtils.SyncLookupTableLength(2048);

            int ln = FourierUtils.Log2(length);

            // reorder array
            FourierUtils.ReorderArray(data);

            // successive doubling
            int N = 1;
            int signIndex = (direction == FourierDirection.Forward) ? 0 : 1;

            for (int level = 1; level <= ln; level++)
            {
                int M = N;
                N <<= 1;

                float[] uRLookup = _uRLookupF[level, signIndex];
                float[] uILookup = _uILookupF[level, signIndex];

                for (int j = 0; j < M; j++)
                {
                    float uR = uRLookup[j];
                    float uI = uILookup[j];

                    for (int even = j; even < length; even += N)
                    {
                        int odd = even + M;

                        float r = data[odd].Re;
                        float i = data[odd].Im;

                        float odduR = r * uR - i * uI;
                        float odduI = r * uI + i * uR;

                        r = data[even].Re;
                        i = data[even].Im;

                        data[even].Re = r + odduR;
                        data[even].Im = i + odduI;

                        data[odd].Re = r - odduR;
                        data[odd].Im = i - odduI;
                    }
                }
            }
        }

        /// <summary>
        /// Compute a 2D fast fourier transform on a data set of complex numbers (represented as pairs of floats)
        /// </summary>
        /// <param name="data"></param>
        /// <param name="xLength"></param>
        /// <param name="yLength"></param>
        /// <param name="direction"></param>
        public static void FFT2(float[] data, int xLength, int yLength, FourierDirection direction)
        {
            if (data == null)
            {
                throw new ArgumentNullException("data");
            }
            if (data.Length < xLength * yLength * 2)
            {
                throw new ArgumentOutOfRangeException("data.Length", data.Length, "must be at least as large as 'xLength * yLength * 2' parameter");
            }
            if (!IsPowerOf2(xLength))
            {
                throw new ArgumentOutOfRangeException("xLength", xLength, "must be a power of 2");
            }
            if (!IsPowerOf2(yLength))
            {
                throw new ArgumentOutOfRangeException("yLength", yLength, "must be a power of 2");
            }

            int xInc = 1;
            int yInc = xLength;

            if (xLength > 1)
            {
                for (int y = 0; y < yLength; y++)
                {
                    int xStart = y * yInc;
                    FourierUtils.LinearFFT(data, xStart, xInc, xLength, direction);
                }
            }

            if (yLength > 1)
            {
                for (int x = 0; x < xLength; x++)
                {
                    int yStart = x * xInc;
                    FourierUtils.LinearFFT(data, yStart, yInc, yLength, direction);
                }
            }
        }

        /// <summary>
        /// Compute a 2D fast fourier transform on a data set of complex numbers
        /// </summary>
        /// <param name="data"></param>
        /// <param name="xLength"></param>
        /// <param name="yLength"></param>
        /// <param name="direction"></param>
        public static void FFT2(ComplexF[] data, int xLength, int yLength, FourierDirection direction)
        {
            if (data == null)
            {
                throw new ArgumentNullException("data");
            }
            if (data.Length < xLength * yLength)
            {
                throw new ArgumentOutOfRangeException("data.Length", data.Length, "must be at least as large as 'xLength * yLength' parameter");
            }
            if (!IsPowerOf2(xLength))
            {
                throw new ArgumentOutOfRangeException("xLength", xLength, "must be a power of 2");
            }
            if (!IsPowerOf2(yLength))
            {
                throw new ArgumentOutOfRangeException("yLength", yLength, "must be a power of 2");
            }

            int xInc = 1;
            int yInc = xLength;

            if (xLength > 1)
            {
                for (int y = 0; y < yLength; y++)
                {
                    int xStart = y * yInc;
                    FourierUtils.LinearFFT(data, xStart, xInc, xLength, direction);
                }
            }

            if (yLength > 1)
            {
                for (int x = 0; x < xLength; x++)
                {
                    int yStart = x * xInc;
                    FourierUtils.LinearFFT(data, yStart, yInc, yLength, direction);
                }
            }
        }
    }
}