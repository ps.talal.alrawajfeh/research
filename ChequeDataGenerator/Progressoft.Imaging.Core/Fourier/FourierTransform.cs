﻿using System;

namespace Progressoft.Imaging.Core.Fourier
{
    public class FourierTransform
    {
        public bool Forward(ref FourierImage cimg)
        {
            try
            {
                if (cimg == null)
                    return false;

                float scale = 1f / (float)Math.Sqrt(cimg.Width * cimg.Height);
                ComplexF[] data = cimg.Data;

                int offset = 0;
                for (int y = 0; y < cimg.Height; y++)
                    for (int x = 0; x < cimg.Width; x++, offset++)
                        if (((x + y) & 0x1) != 0)
                            data[offset] *= -1;

                FourierUtils.FFT2(data, cimg.Width, cimg.Height, FourierDirection.Forward);
                cimg.FrequencySpace = true;
                for (int i = 0; i < data.Length; i++)
                    data[i] *= scale;
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Backward(ref FourierImage ComplexCimageFFT)
        {
            return Backward(ref ComplexCimageFFT, (data, _, __) => data);
        }

        public bool Backward(ref FourierImage ComplexCimageFFT, Func<ComplexF[], int, int, ComplexF[]> complexFilter)
        {
            try
            {
                if (ComplexCimageFFT == null)
                    return false;

                float scale = 1f / (float)Math.Sqrt(ComplexCimageFFT.Width * ComplexCimageFFT.Height);
                ComplexF[] data = ComplexCimageFFT.Data;

                data = complexFilter(data, ComplexCimageFFT.Width, ComplexCimageFFT.Height);

                FourierUtils.FFT2(data, ComplexCimageFFT.Width, ComplexCimageFFT.Height, FourierDirection.Backward);

                int offset = 0;
                for (int y = 0; y < ComplexCimageFFT.Height; y++)
                    for (int x = 0; x < ComplexCimageFFT.Width; x++, offset++)
                        if (((x + y) & 0x1) != 0)
                            data[offset] *= -1;
                ComplexCimageFFT.FrequencySpace = false;

                for (int i = 0; i < data.Length; i++)
                    data[i] *= scale;

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}