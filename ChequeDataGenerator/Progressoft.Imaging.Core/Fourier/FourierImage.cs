﻿using System;
using System.Drawing;
using System.Drawing.Imaging;

namespace Progressoft.Imaging.Core.Fourier
{
    public sealed unsafe class FourierImage
    {
        public FourierImage(Bitmap bmpImg)
        {
            Bitmap bitmap = (Bitmap)bmpImg.Clone();

            Size correctSize = new Size(
                (int)Math.Pow(2, Math.Ceiling(Math.Log(bitmap.Width, 2))),
                (int)Math.Pow(2, Math.Ceiling(Math.Log(bitmap.Height, 2))));
            if (correctSize != bitmap.Size)
            {
                bitmap = new Bitmap(bitmap, correctSize);
            }

            Size = correctSize;
            Data = new ComplexF[this.Width * this.Height];
            Rectangle rect = new Rectangle(0, 0, this.Width, this.Height);
            BitmapData bitmapData = bitmap.LockBits(rect, ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
            int* colorData = (int*)bitmapData.Scan0.ToPointer();
            for (int i = 0; i < this.Width * this.Height; i++)
            {
                Color c = Color.FromArgb(colorData[i]);
                Data[i].Re = ((float)c.R + (float)c.G + (float)c.B) / (3f * 256f);
            }
            bitmap.UnlockBits(bitmapData);
            bitmap.Dispose();
        }

        public ComplexF[] Data { get; } = null;
        public Size Size { get; }

        public int Width
        {
            get { return Size.Width; }
        }

        public int Height
        {
            get { return Size.Height; }
        }

        public bool FrequencySpace { get; set; } = false;

        public Bitmap ToBitmap()
        {
            Bitmap bitmap = new Bitmap(this.Width, this.Height, PixelFormat.Format32bppArgb);
            Rectangle rect = new Rectangle(0, 0, this.Width, this.Height);
            BitmapData bitmapData = bitmap.LockBits(rect, ImageLockMode.WriteOnly, PixelFormat.Format32bppArgb);
            int* colorData = (int*)bitmapData.Scan0.ToPointer();
            for (int i = 0; i < this.Width * this.Height; i++)
            {
                int c = Math.Min(255, Math.Max(0, (int)(256 * Data[i].GetModulus())));
                colorData[i] = Color.FromArgb(c, c, c).ToArgb();
            }
            bitmap.UnlockBits(bitmapData);
            return bitmap;
        }
    }
}