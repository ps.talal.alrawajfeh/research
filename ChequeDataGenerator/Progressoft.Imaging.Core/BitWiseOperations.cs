using System.Drawing;
using System.Drawing.Imaging;

namespace Progressoft.Imaging.Core
{
    /// <summary>
    /// bitwise operations.
    /// </summary>
    public static unsafe class BitWiseOperations
    {
        /// <summary>
        /// Applies bitwise Anding.
        /// </summary>
        /// <param name="bmpImg1"> Left hand side of the Anding operation.</param>
        /// <param name="bmpImg2"> Right hand side of the Anding operation.</param>
        /// <param name="outputBmp"></param>
        /// <returns>
        /// Bitmap Object: Success.
        /// null: failed.
        /// </returns>
        public static Bitmap BitWiseAnding(Bitmap bmpImg1, Bitmap bmpImg2)
        {
            BitmapData bmpData1 = null;
            BitmapData bmpData2 = null;
            BitmapData outBmpData = null;
            Bitmap outputBmp = null;

            try
            {
                // check for the validity of the given format:
                if (bmpImg1.PixelFormat != PixelFormat.Format8bppIndexed ||
                    bmpImg2.PixelFormat != PixelFormat.Format8bppIndexed ||
                    bmpImg1.Width != bmpImg2.Width ||
                    bmpImg1.Height != bmpImg2.Height)
                    return null; // invalid pixel format or uncompatible images.

                int width = bmpImg1.Width;
                int height = bmpImg1.Height;

                // create the output image:
                outputBmp = (Bitmap)bmpImg1.Clone();

                // lock images to be processed:
                bmpData1 = bmpImg1.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                System.IntPtr scan01 = bmpData1.Scan0;
                int offset1 = bmpData1.Stride - width;

                bmpData2 = bmpImg2.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                System.IntPtr scan02 = bmpData2.Scan0;

                outBmpData = outputBmp.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                System.IntPtr outScan0 = outBmpData.Scan0;

                // get pointers to image data:
                byte* p1 = (byte*)(void*)scan01;
                byte* p2 = (byte*)(void*)scan02;
                byte* outP = (byte*)(void*)outScan0;

                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        outP[0] = (byte)(p1[0] & p2[0]);
                        p1++;
                        p2++;
                        outP++;
                    }
                    p1 += offset1;
                    p2 += offset1;
                    outP += offset1;
                }
            }
            catch
            {
                return null;
            }
            finally
            {
                if (bmpImg1 != null && bmpData1 != null)
                    bmpImg1.UnlockBits(bmpData1);
                if (bmpImg2 != null && bmpData2 != null)
                    bmpImg2.UnlockBits(bmpData2);
                if (outputBmp != null && outBmpData != null)
                    outputBmp.UnlockBits(outBmpData);
            }
            return outputBmp;
        }
    }
}