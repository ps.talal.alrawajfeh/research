﻿namespace Progressoft.Imaging.Core
{
    public enum BinarizationMethod
    {
        None,
        OtsuMethod,
        DynamicAdaptiveMethod,
        IterativeMethod,
        MeanMinusAsdMethod,
        MeanMethod,
        CleverMethod,
        MeanMinusSdMethod,
        StaticMethod,
        SauvolaPietikainen,
        Niblak,
        WolfEtEls,
        BitPlane,
        BackgroundAdaptive
    }
}