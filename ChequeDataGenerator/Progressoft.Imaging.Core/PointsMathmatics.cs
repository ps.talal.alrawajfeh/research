﻿using Progressoft.Imaging.Core.Common;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;

namespace Progressoft.Imaging.Core
{
    public static unsafe class PointsMathmatics
    {
        private static readonly Point[] PolygonExpansionDirections = new Point[]
            {
                new Point(-1,-1), // left x top

                new Point(0, -1), // Middle(left,right) x top

                new Point(1,-1),  // right x top

                new Point(1, 0 ), // right x middle(top,bottom)

                new Point(1, 1),  // right x bottom

                new Point(0, 1),  // Middle(left,right) x bottom

                new Point(-1, 1), // left x bottom

                new Point(-1, 0)  // left x middle(top,bottom)
            };

        /// <summary>
        /// find the slop of the lines which connects between the given 2 points.
        /// </summary>
        /// <param name="pntStart"></param>
        /// <param name="pntEnd"></param>
        /// <returns></returns>
        public static float CalculateLineSlop(PointF pntA, PointF pntB)
        {
            return (pntB.X == pntA.X) ?
                0.0f : (pntB.Y - pntA.Y) / (pntB.X - pntA.X);
        }

        public static float CalculateLineSlop(Point pntA, Point pntB)
        {
            return CalculateLineSlop(ToPointF(pntA), ToPointF(pntB));
        }

        /// <summary>
        /// find the direction in degrees for the lines which connects between
        /// the given 2 points.
        /// </summary>
        /// <param name="pntStart"></param>
        /// <param name="pntEnd"></param>
        /// <returns></returns>
        public static float CalculateLineDirection(PointF pntA, PointF pntB)
        {
            // move point B to make it relative to 0,0 ...
            PointF p2 = new PointF(pntB.X - pntA.X, pntB.Y - pntA.Y);
            // compute the angle ...
            float fAngle = (float)(Math.Atan2(p2.Y, p2.X) * (180.0 / Math.PI));

            if (fAngle < 0)
                fAngle = 360 + fAngle;

            return fAngle;
        }

        public static float CalculateLineDirection(Point pntA, Point pntB)
        {
            return CalculateLineDirection(ToPointF(pntA), ToPointF(pntB));
        }

        /// <summary>
        /// computes the angle in degrees at point B defined by the 3 given points.
        /// Note that the value is the opposite of what you might expect because Y coordinates increase downward.
        /// </summary>
        /// <param name="Ax"></param>
        /// <param name="Ay"></param>
        /// <param name="Bx"></param>
        /// <param name="By"></param>
        /// <param name="Cx"></param>
        /// <param name="Cy"></param>
        /// <returns></returns>
        public static float CalculateAngle(PointF pntA, PointF pntB, PointF pntC)
        {
            // Get the dot product and cross product.
            float fDotProduct = DotProduct(pntA, pntB, pntC);
            float fCrossProduct = CrossProductLength(pntA, pntB, pntC);
            // Calculate the angle.
            return (float)(57.2957795 * Math.Atan2(fCrossProduct, fDotProduct));
        }

        public static float CalculateAngle(Point pntA, Point pntB, Point pntC)
        {
            return CalculateAngle(ToPointF(pntA), ToPointF(pntB), ToPointF(pntC));
        }

        /// <summary>
        /// Return the dot product AB � BC.
        /// Note that AB � BC = |AB| * |BC| * Cos(theta).
        /// </summary>
        /// <param name="Ax"></param>
        /// <param name="Ay"></param>
        /// <param name="Bx"></param>
        /// <param name="By"></param>
        /// <param name="Cx"></param>
        /// <param name="Cy"></param>
        /// <returns></returns>
        public static float DotProduct(PointF pntA, PointF pntB, PointF pntC)
        {
            // Get the vectors' coordinates.
            float BAx = pntA.X - pntB.X;
            float BAy = pntA.Y - pntB.Y;
            float BCx = pntC.X - pntB.X;
            float BCy = pntC.Y - pntB.Y;

            // Calculate the dot product.
            return BAx * BCx + BAy * BCy;
        }

        public static float DotProduct(Point pntA, Point pntB, Point pntC)
        {
            return DotProduct(ToPointF(pntA), ToPointF(pntB), ToPointF(pntC));
        }

        /// <summary>
        /// Return the cross product AB x BC.
        /// The cross product is a vector perpendicular to AB
        /// and BC having length |AB| * |BC| * Sin(theta) and
        /// with direction given by the right-hand rule.
        /// For two vectors in the X-Y plane, the result is a
        /// vector with X and Y components 0 so the Z component
        /// gives the vector's length and direction.
        /// </summary>
        /// <param name="Ax"></param>
        /// <param name="Ay"></param>
        /// <param name="Bx"></param>
        /// <param name="By"></param>
        /// <param name="Cx"></param>
        /// <param name="Cy"></param>
        /// <returns></returns>
        public static float CrossProductLength(PointF pntA, PointF pntB, PointF pntC)
        {
            // Get the vectors' coordinates.
            float BAx = pntA.X - pntB.X;
            float BAy = pntA.Y - pntB.Y;
            float BCx = pntC.X - pntB.X;
            float BCy = pntC.Y - pntB.Y;

            // Calculate the Z coordinate of the cross product.
            return BAx * BCy - BAy * BCx;
        }

        public static float CrossProductLength(Point pntA, Point pntB, Point pntC)
        {
            return DotProduct(ToPointF(pntA), ToPointF(pntB), ToPointF(pntC));
        }

        /// <summary>
        /// Computes center of graphity ( the mean point) for the given array of points ...
        /// </summary>
        /// <param name="points"></param>
        /// <returns></returns>
        public static Point GetCenterOfGraphity(List<Point> points)
        {
            if (points != null)
            {
                int iPointsCount = points.Count;

                if (iPointsCount > 0)
                {
                    // compute center of gravity  ...
                    float sumX = 0;
                    float sumY = 0;

                    for (int pnt = 0; pnt < iPointsCount; pnt++)
                    {
                        sumX += points[pnt].X;
                        sumY += points[pnt].Y;
                    }

                    return new Point((int)Math.Round(sumX / iPointsCount),
                                      (int)Math.Round(sumY / iPointsCount));
                }
            }
            return new Point();
        }

        /// <summary>
        /// Returns the boundary points ( minimum X, minimum Y, maximum X, maximum Y )...
        /// </summary>
        /// <param name="points"></param>
        /// <param name="pntLeftTop"></param>
        /// <param name="pntRightBottom"></param>
        public static Rectangle GetContainerRectangle(List<Point> points)
        {
            if (points != null)
            {
                int iPointsCount = points.Count;
                int minX = int.MaxValue;
                int minY = int.MaxValue;
                int maxX = int.MinValue;
                int maxY = int.MinValue;

                for (int i = 0; i < iPointsCount; i++)
                {
                    if (points[i].X < minX)
                        minX = points[i].X;

                    if (points[i].Y < minY)
                        minY = points[i].Y;

                    if (points[i].X > maxX)
                        maxX = points[i].X;

                    if (points[i].Y > maxY)
                        maxY = points[i].Y;
                }

                return new Rectangle(minX, minY, maxX - minX + 1, maxY - minY + 1);
            }

            return new Rectangle();
        }

        public static RectangleF GetContainerRectangle(List<PointF> points)
        {
            if (points != null)
            {
                int iPointsCount = points.Count;
                float minX = int.MaxValue;
                float minY = int.MaxValue;
                float maxX = int.MinValue;
                float maxY = int.MinValue;

                for (int i = 0; i < iPointsCount; i++)
                {
                    if (points[i].X < minX)
                        minX = points[i].X;

                    if (points[i].Y < minY)
                        minY = points[i].Y;

                    if (points[i].X > maxX)
                        maxX = points[i].X;

                    if (points[i].Y > maxY)
                        maxY = points[i].Y;
                }

                return new RectangleF(minX, minY, maxX - minX + 1, maxY - minY + 1);
            }

            return new Rectangle();
        }

        /// <summary>
        /// calculate the length of the line defined by the given 2 points.
        /// </summary>
        /// <param name="pntA"></param>
        /// <param name="pntB"></param>
        /// <returns></returns>
        public static float GetLineLength(PointF pntA, PointF pntB)
        {
            // the length of the line connecting both points ...
            return (float)Math.Sqrt(((pntA.X - pntB.X) * (pntA.X - pntB.X)) +
                                    ((pntA.Y - pntB.Y) * (pntA.Y - pntB.Y)));
        }

        public static float GetLineLength(Point pntA, Point pntB)
        {
            return GetLineLength(ToPointF(pntA), ToPointF(pntB));
        }

        /// <summary>
        /// return the mid points between the given 2 points.
        /// </summary>
        /// <param name="pntA"></param>
        /// <param name="pntB"></param>
        /// <returns></returns>
        public static PointF GetMidPoint(PointF pntA, PointF pntB)
        {
            // compute the mid point ...
            float fX = (float)Math.Round((pntA.X + pntB.X) / 2.0f, 2);
            float fY = (float)Math.Round((pntA.Y + pntB.Y) / 2.0f, 2);
            //
            return new PointF(fX, fY);
        }

        public static Point GetMidPoint(Point pntA, Point pntB)
        {
            // compute the mid point ...
            int iX = (int)Math.Round((pntA.X + pntB.X) / 2.0f);
            int iY = (int)Math.Round((pntA.Y + pntB.Y) / 2.0f);
            return new Point(iX, iY);
        }

        /// <summary>
        /// Get the arc-length of the given curve represented as points.
        /// </summary>
        /// <param name="points"></param>
        /// <returns></returns>
        public static float GetArcLength(PointF[] points)
        {
            float fArcLength = 0;

            // get length ...
            int iLength = points.Length;

            if (iLength > 1)
            {
                iLength--; // skip the last point ... cause no points after  ...
                for (int i = 0; i < iLength; i++)
                {
                    float fLength = GetLineLength(points[i], points[i + 1]);

                    fArcLength += fLength;
                }
            }

            return fArcLength;
        }

        public static float GetArcLength(Point[] points)
        {
            return GetArcLength(points.Select((p) => ToPointF(p)).ToArray());
        }

        public static Bitmap PointsToBitmap(List<Point> points, int padding, byte grayColor)
        {
            if (points != null && points.Count > 0)
            {
                Rectangle rectComp = PointsMathmatics.GetContainerRectangle(points);
                Rectangle rectLock = new Rectangle(0, 0, rectComp.Width, rectComp.Height);
                Bitmap bmpComp = BitmapFactory.CreateGrayImage(rectComp.Width, rectComp.Height);

                BitmapData bmpDataComp = bmpComp.LockBits(rectLock, ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);

                int iStride = bmpDataComp.Stride;
                int iOffset = iStride - rectComp.Width;

                byte* p = (byte*)(void*)bmpDataComp.Scan0;
                for (int y = 0; y < rectComp.Height; y++)
                {
                    for (int x = 0; x < rectComp.Width; x++)
                    {
                        p[0] = 255;
                        p++;
                    }
                    p += iOffset;
                }

                p = (byte*)(void*)bmpDataComp.Scan0;

                foreach (Point pnt in points)
                    p[(pnt.Y - rectComp.Y + padding) * iStride + (pnt.X - rectComp.X + padding)] = grayColor;

                bmpComp.UnlockBits(bmpDataComp);

                return bmpComp;
            }
            return null;
        }

        /// <summary>
        /// Apply translation transformation for the given array of points ...
        /// </summary>
        /// <param name="points"></param>
        /// <param name="fOffsetX"></param>
        /// <param name="fOffsetY"></param>
        /// <returns></returns>
        public static PointF[] TranslatePoints(PointF[] points, float fOffsetX, float fOffsetY)
        {
            // copy the source array ...
            PointF[] translated = new PointF[points.Length];
            Array.Copy(points, translated, points.Length);

            // apply the transform ...
            Matrix matrix = new Matrix();
            matrix.Translate(fOffsetX, fOffsetY);
            matrix.TransformPoints(translated);

            //
            matrix.Dispose();

            // return the result ...
            return translated;
        }

        public static Point[] TranslatePoints(Point[] points, int offsetX, int OffsetY)
        {
            // copy the source array ...
            Point[] translated = new Point[points.Length];
            Array.Copy(points, translated, points.Length);
            // apply the transform ...
            Matrix matrix = new Matrix();
            matrix.Translate(offsetX, OffsetY);
            matrix.TransformPoints(translated);
            //
            matrix.Dispose();
            // return the result ...
            return translated;
        }

        /// <summary>
        /// Apply scale transformation for the given array of points ...
        /// </summary>
        /// <param name="points"></param>
        /// <param name="fScaleFactor"></param>
        /// <returns></returns>
        public static PointF[] ScalePoints(PointF[] points, float fScaleFactor)
        {
            // copy the source array ...
            PointF[] scaled = new PointF[points.Length];
            Array.Copy(points, scaled, points.Length);

            // apply the transform ...
            Matrix matrix = new Matrix();
            matrix.Scale(fScaleFactor, fScaleFactor);
            matrix.TransformPoints(scaled);

            //
            matrix.Dispose();

            // return the result ...
            return scaled;
        }

        public static Point[] ScalePoints(Point[] points, float fScaleFactor)
        {
            // copy the source array ...
            Point[] scaled = new Point[points.Length];
            Array.Copy(points, scaled, points.Length);
            // apply the transform ...
            Matrix matrix = new Matrix();
            matrix.Scale(fScaleFactor, fScaleFactor);
            matrix.TransformPoints(scaled);
            //
            matrix.Dispose();
            // return the result ...
            return scaled;
        }

        /// <summary>
        /// Splite the given array of points into 2 arrays. The first one holds X coordinates
        /// and the second one will hold the Y coordinates.
        /// </summary>
        /// <param name="points"></param>
        /// <param name="xCoords"></param>
        /// <param name="yCoords"></param>
        public static void SplitXYCoordinates(PointF[] points, out float[] xCoords, out float[] yCoords)
        {
            // get count of points ...
            int iLength = points.Length;

            // define x, y arrays ...
            xCoords = new float[iLength];
            yCoords = new float[iLength];

            // fill the arrays ...
            for (int i = 0; i < iLength; i++)
            {
                xCoords[i] = points[i].X;
                yCoords[i] = points[i].Y;
            }
        }

        public static void SplitXYCoordinates(List<PointF> points, out List<float> xCoords, out List<float> yCoords)
        {
            SplitXYCoordinates(points.ToArray(), out float[] xs, out float[] ys);
            xCoords = new List<float>(xs);
            yCoords = new List<float>(ys);
        }

        public static void SplitXYCoordinates(Point[] points, out int[] xCoords, out int[] yCoords)
        {
            // get count of points ...
            int iLength = points.Length;

            // define x, y arrays ...
            xCoords = new int[iLength];
            yCoords = new int[iLength];

            // fill the arrays ...
            for (int i = 0; i < iLength; i++)
            {
                xCoords[i] = points[i].X;
                yCoords[i] = points[i].Y;
            }
        }

        public static void SplitXYCoordinates(List<Point> points, out List<int> xCoords, out List<int> yCoords)
        {
            SplitXYCoordinates(points.ToArray(), out int[] xs, out int[] ys);
            xCoords = new List<int>(xs);
            yCoords = new List<int>(ys);
        }

        /// <summary>
        /// Mergs x,y arrays togather to points-array...
        /// </summary>
        /// <param name="xCoords"></param>
        /// <param name="yCoords"></param>
        /// <returns></returns>
        public static PointF[] MergeXYCoordinates(float[] xCoords, float[] yCoords)
        {
            int iLength = xCoords.Length;
            PointF[] points = new PointF[iLength];
            for (int i = 0; i < iLength; i++)
                points[i] = new PointF(xCoords[i], yCoords[i]);
            return points;
        }

        public static List<PointF> MergeXYCoordinates(List<float> xCoords, List<float> yCoords)
        {
            int iLength = xCoords.Count;
            List<PointF> points = new List<PointF>();
            for (int i = 0; i < iLength; i++)
                points.Add(new PointF(xCoords[i], yCoords[i]));
            return points;
        }

        public static Point[] MergeXYCoordinates(int[] xCoords, int[] yCoords)
        {
            int iLength = xCoords.Length;
            Point[] points = new Point[iLength];
            for (int i = 0; i < iLength; i++)
                points[i] = new Point(xCoords[i], yCoords[i]);
            return points;
        }

        public static List<Point> MergeXYCoordinates(List<int> xCoords, List<int> yCoords)
        {
            int iLength = xCoords.Count;
            List<Point> points = new List<Point>();
            for (int i = 0; i < iLength; i++)
                points.Add(new Point(xCoords[i], yCoords[i]));
            return points;
        }

        /// <summary>
        /// Smoothing the given curve  ...
        /// </summary>
        /// <param name="points"></param>
        /// <returns></returns>
        public static PointF[] SmoothCurve(PointF[] points)
        {
            SplitXYCoordinates(points, out float[] xPoints, out float[] yPoints);
            xPoints = DoubleExponentialSmoothing(xPoints, 0.35f, 0.35f);
            yPoints = DoubleExponentialSmoothing(yPoints, 0.35f, 0.35f);
            return MergeXYCoordinates(xPoints, yPoints);
        }

        public static List<PointF> SmoothCurve(List<PointF> points)
        {
            SplitXYCoordinates(points.ToArray(), out float[] xPoints, out float[] yPoints);
            xPoints = DoubleExponentialSmoothing(xPoints, 0.35f, 0.35f);
            yPoints = DoubleExponentialSmoothing(yPoints, 0.35f, 0.35f);
            return MergeXYCoordinates(new List<float>(xPoints), new List<float>(yPoints));
        }

        /// <summary>
        /// Applies double exponential smoothing ...
        /// </summary>
        /// <param name="dValues"></param>
        /// <param name="fSmoothingFactor"></param>
        /// <param name="fTrendSmoothingFactor"></param>
        /// <returns></returns>
        public static float[] DoubleExponentialSmoothing(float[] dValues, float fSmoothingFactor, float fTrendSmoothingFactor)
        {
            // gt length  ...
            int iLength = dValues.Length;

            // holds the smoothed values ...
            float[] dSmoothedValues = new float[iLength];

            // initially at t = 0, the values will be as followings ...
            dSmoothedValues[0] = dValues[0];
            float dB = (dValues[iLength - 1] - dValues[0]) / (iLength - 1);
            float dF = dSmoothedValues[0] + dB;

            // smooth the values ...
            for (int i = 1; i < iLength; i++)
            {
                dSmoothedValues[i] = fSmoothingFactor * dValues[i] + (1 - fSmoothingFactor) * dF;

                // calculate next F & B ...
                dB = fTrendSmoothingFactor * (dSmoothedValues[i] - dSmoothedValues[i - 1]) + (1 - fTrendSmoothingFactor) * dB;
                dF = dSmoothedValues[i] + dB;
            }

            // return the smoothed values ...
            return dSmoothedValues;
        }

        public static List<Point> GetIntersectionPoints(Rectangle area, List<Point> points, Rectangle pointsContainer)
        {
            if (area.Contains(pointsContainer))
                return points;

            List<Point> intersectionPoints = new List<Point>();
            foreach (Point pnt in points)
                if (area.Contains(pnt))
                    intersectionPoints.Add(pnt);

            return intersectionPoints;
        }

        public static List<Point> GetIntersectionPoints(Rectangle area, List<Point> points)
        {
            return GetIntersectionPoints(area, points, GetContainerRectangle(points));
        }

        public static bool IsPointInsidePolygon(List<Point> polygon, Point point)
        {
            bool isInside = false;
            int count = polygon.Count;
            for (int i = 0, j = count - 1; i < count; j = i++)
            {
                if (((polygon[i].Y > point.Y) != (polygon[j].Y > point.Y)) &&
                     (point.X < (polygon[j].X - polygon[i].X) * (point.Y - polygon[i].Y) / (polygon[j].Y - polygon[i].Y) + polygon[i].X))
                {
                    isInside = !isInside;
                }
            }
            return isInside;
        }

        public static bool IsPointInsidePolygon(List<Point> polygon, int x, int y)
        {
            bool isInside = false;
            int count = polygon.Count;
            for (int i = 0, j = count - 1; i < count; j = i++)
            {
                if (((polygon[i].Y > y) != (polygon[j].Y > y)) &&
                     (x < (polygon[j].X - polygon[i].X) * (y - polygon[i].Y) / (polygon[j].Y - polygon[i].Y) + polygon[i].X))
                {
                    isInside = !isInside;
                }
            }
            return isInside;
        }

        public static List<Point> ExpandPolygonPoints(List<Point> polygon, int factor = 1)
        {
            Rectangle containerRect = PointsMathmatics.GetContainerRectangle(polygon);
            List<Point> lstAnchorExpansionPoints = GetAnchorExpansionPointsForPolygon(containerRect);

            int anchorPointsCount = lstAnchorExpansionPoints.Count;
            int polygonPointsCount = polygon.Count;

            // for each polygon point:
            //      p = search for the nearset anchor-point & get its position.
            //      given that each anchor point has its expansion-direction, get the expansion direction at position (p) and expand
            //      the point.
            //

            List<Point> expandedPolygon = new List<Point>();
            for (int i = 0; i < polygonPointsCount; i++)
            {
                float minDistance = int.MaxValue;
                int index = 0;
                for (int j = 0; j < anchorPointsCount; j++)
                {
                    float distance = PointsMathmatics.GetLineLength(polygon[i], lstAnchorExpansionPoints[j]);
                    if (distance < minDistance)
                    {
                        minDistance = distance;
                        index = j;
                    }
                }

                expandedPolygon.Add(
                    new Point()
                    {
                        X = polygon[i].X + PolygonExpansionDirections[index].X * factor,
                        Y = polygon[i].Y + PolygonExpansionDirections[index].Y * factor
                    });
            }

            return expandedPolygon;
        }

        public static List<Point> GetAllPointsInsidePolygon(List<Point> polygon, bool expandPolygon)
        {
            List<Point> lstPointsInside = new List<Point>();
            List<Point> polygonTmp = expandPolygon ? ExpandPolygonPoints(polygon) : polygon;

            Rectangle containerRect = PointsMathmatics.GetContainerRectangle(polygon);

            for (int x = containerRect.X; x < containerRect.Right; x++)
            {
                for (int y = containerRect.Y; y < containerRect.Bottom; y++)
                {
                    if (PointsMathmatics.IsPointInsidePolygon(polygonTmp, x, y))
                        lstPointsInside.Add(new Point(x, y));
                }
            }

            return lstPointsInside;
        }

        private static PointF ToPointF(Point pnt)
        {
            return new PointF(pnt.X, pnt.Y);
        }

        private static List<Point> GetAnchorExpansionPointsForPolygon(Rectangle containerRect)
        {
            return new List<Point>()
            {
                new Point(containerRect.Left, containerRect.Top),                                      // left x top
                new Point(containerRect.Left + containerRect.Width/2, containerRect.Top),               // Middle(left,right) x top
                new Point(containerRect.Right-1, containerRect.Top),                                  // right x top

                new Point(containerRect.Right-1, containerRect.Top + containerRect.Height/2 ),         // right x middle(top,bottom)

                new Point(containerRect.Right-1, containerRect.Bottom-1),                              // right x bottom

                new Point(containerRect.Left + containerRect.Width/2, containerRect.Bottom-1),         // Middle(left,right) x bottom

                new Point(containerRect.Left, containerRect.Bottom-1),                                  // left x bottom

                new Point(containerRect.Left, containerRect.Top + containerRect.Height/2)              // left x middle(top,bottom)
            };
        }
    }
}