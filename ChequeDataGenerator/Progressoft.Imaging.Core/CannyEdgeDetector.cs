﻿using Progressoft.Imaging.Core.Common;
using System;
using System.Drawing;
using System.Drawing.Imaging;

namespace Progressoft.Imaging.Core
{
    internal static unsafe class CannyEdgeDetector
    {
        private const int ORI_SCALE = 40;
        private const int MAG_SCALE = 20;

        /// <summary>
        /// Applies The Canny Operator To An 8 Bit Bitmap
        /// </summary>
        /// <param name="im">The Bitmap To Be Filtered</param>
        /// <param name="LowThershold">Lower Threshold (Default 0)</param>
        /// <param name="HighThreshold">High Threshold (Default -1)</param>
        /// <param name="GSD">Gaussian Standard Deviation (Default 1)</param>
        /// <param name="outputImage">Reference To The Resulted Bitmap</param>
        /// <returns>Error Code:
        /// 0: Success.
        /// 1: Invalid Pixel Format.</returns>
        public static int CannyFilter(Bitmap inputImage, int LowThershold, int HighThreshold, ref Bitmap outputImage)
        {
            try
            {
                if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed) return 1;
                Bitmap im = (Bitmap)inputImage.Clone();

                int i, j;
                float s = 1.0F;
                int low = 0, high = -1;
                low = LowThershold;
                high = HighThreshold;
                Bitmap mag = (Bitmap)im.Clone();
                Bitmap oriim = (Bitmap)im.Clone();

                int imW = im.Width;
                int imH = im.Height;

                int halfWidth = imW / 2;

                CannyOperator(ref im, ref mag, ref oriim, ref s);
                /* Hysteresis thresholding of edge pixels */

                Hysteresis(high, low, halfWidth, ref im, ref mag, ref oriim);

                int[,] imData = BitmapUtils.ToIntArray2D(im);

                for (i = 0; i < halfWidth; i++)
                    for (j = 0; j < imW; j++)
                        imData[i, j] = 255;

                for (i = imH - 1; i > imH - 1 - halfWidth; i--)
                    for (j = 0; j < imW; j++)
                        imData[i, j] = 255;

                for (i = 0; i < imH; i++)
                    for (j = 0; j < halfWidth; j++)
                        imData[i, j] = 255;

                for (i = 0; i < imH; i++)
                    for (j = imW - halfWidth - 1; j < imW; j++)
                        imData[i, j] = 255;

                outputImage = BitmapUtils.ToBitmap(imData);
                return 0;
            }
            catch
            {
                return 1;
            }
        } //CannyFilter

        /// <summary>
        /// Internal Usage For Canny Operator Public Method
        /// </summary>
        /// <param name="OriginBmp">Original Image</param>
        /// <param name="magim">First Copy Of The Image</param>
        /// <param name="oriim">Second Copy Of The Image</param>
        /// <param name="s">Gaussian Standard Deviation</param>
        private static void CannyOperator(ref Bitmap im, ref Bitmap mag, ref Bitmap ori, ref float s)
        {
            int width = 0;
            float[,] smx;
            float[,] smy;
            float[,] dx;
            float[,] dy;
            int i, j;

            float[] gau = new float[20];
            float[] dgau = new float[20];
            float z;

            int[,] magData = BitmapUtils.ToIntArray2D(mag);
            for (i = 0; i < 20; i++)
            {
                gau[i] = MeanGauss(i, s);
                if (gau[i] < 0.005)
                {
                    width = i;
                    break;
                }
                dgau[i] = DGauss(i, s);
            }

            smx = new float[im.Height, im.Width];
            smy = new float[im.Height, im.Width];

            Seperable_convolution(ref im, ref gau, width, ref smx, ref smy);

            /* Now convolve smoothed data with a derivative */

            dx = new float[im.Height, im.Width];
            Dxy_seperable_convolution(smx, im.Height, im.Width, dgau, width, ref dx, 1);

            dy = new float[im.Height, im.Width];
            Dxy_seperable_convolution(smy, im.Height, im.Width, dgau, width, ref dy, 0);

            /* Create an image of the norm of dx,dy */
            for (i = 0; i < im.Height; i++)
                for (j = 0; j < im.Width; j++)
                {
                    z = Norm(dx[i, j], dy[i, j]);
                    magData[i, j] = (int)(z * MAG_SCALE);
                }

            mag = BitmapUtils.ToBitmap(magData);

            /* Non-maximum suppression - edge pixels should be a local max */

            Nonmax_suppress(ref dx, ref dy, ref mag, ref ori);
        } //canny operator

        #region Gaussian

        private static float Gauss(float x, float sigma)
        {
            float xx;

            if (sigma == 0) return 0.0F;
            xx = (float)Math.Exp(((-x * x) / (2 * sigma * sigma)));
            return xx;
        } //gauss

        private static float MeanGauss(float x, float sigma)
        {
            float z;

            z = (float)((Gauss(x, sigma) + Gauss((float)(x + 0.5), sigma) + Gauss((float)(x - 0.5), sigma)) / 3.0);
            z = (float)(z / (Math.PI * 2.0 * sigma * sigma));
            return z;
        } // meanGauss

        private static float DGauss(float x, float sigma)
        {
            return -x / (sigma * sigma) * Gauss(x, sigma);
        } // dGauss

        #endregion Gaussian

        private static float Norm(float x, float y)
        {
            return (float)Math.Sqrt(x * x + y * y);
        }

        private static void Hysteresis(int high, int low, int halfWidth, ref Bitmap im, ref Bitmap mag, ref Bitmap oriim)
        {
            int i, j;
            int imW = im.Width;
            int imH = im.Height;
            int[,] imData = BitmapUtils.ToIntArray2D(im);
            int[,] magData = BitmapUtils.ToIntArray2D(mag);
            int[,] oriData = BitmapUtils.ToIntArray2D(oriim);

            for (i = 0; i < imH; i++)
                for (j = 0; j < imW; j++)
                    imData[i, j] = 0;

            if (high < low)
            {
                Estimate_thresh(mag, halfWidth, ref high, ref low);
            }
            /* For each edge with a magnitude above the high threshold, begin
               tracing edge pixels that are above the low threshold.                */

            for (i = 0; i < imH; i++)
                for (j = 0; j < imW; j++)
                    if (magData[i, j] >= high)
                        Trace(i, j, low, im, mag, oriim, imData, magData, oriData);

            /* Make the edge black (to be the same as the other methods) */
            for (i = 0; i < imH; i++)
                for (j = 0; j < imW; j++)
                    if (imData[i, j] == 0) imData[i, j] = 255;
                    else imData[i, j] = 0;

            im = BitmapUtils.ToBitmap(imData);
        }

        /*      TRACE - recursively trace edge pixels that have a threshold > the low
           edge threshold, continuing from the pixel at (i,j).                     */

        private static int Trace(int i, int j, int low, Bitmap im, Bitmap mag, Bitmap ori, int[,] imData, int[,] magData, int[,] oriData)
        {
            int n, m, flag;

            if (imData[i, j] == 0)
            {
                imData[i, j] = 255;
                flag = 0;
                for (n = -1; n < 1; n++)
                {
                    for (m = -1; m < 1; m++)
                    {
                        if (i == 0 && m == 0) continue;
                        if (Range(mag, i + n, j + m) != 0 && magData[i + n, j + m] >= low &&
                            Trace(i + n, j + m, low, im, mag, ori, imData, magData, oriData) == 0)
                        {
                            flag = 1;
                            break;
                        }
                    }
                    if (flag == 0) break;
                }
                return (1);
            }
            return (0);
        }

        private static void Seperable_convolution(ref Bitmap im, ref float[] gau, int width, ref float[,] smx, ref float[,] smy)
        {
            int i, j, k, I1, I2, nr, nc;
            float x, y;

            nr = im.Height;
            nc = im.Width;
            int[,] imData = BitmapUtils.ToIntArray2D(im);

            for (i = 0; i < nr; i++)
            {
                for (j = 0; j < nc; j++)
                {
                    x = gau[0] * imData[i, j]; y = gau[0] * imData[i, j];
                    for (k = 1; k < width; k++)
                    {
                        I1 = (i + k) % nr; I2 = (i - k + nr) % nr;
                        y += gau[k] * imData[I1, j] + gau[k] * imData[I2, j];
                        I1 = (j + k) % nc; I2 = (j - k + nc) % nc;
                        x += gau[k] * imData[i, I1] + gau[k] * imData[i, I2];
                    }
                    smx[i, j] = x; smy[i, j] = y;
                }
            }
            BitmapUtils.ToBitmap(imData);
        }

        private static void Dxy_seperable_convolution(float[,] im, int nr, int nc, float[] gau, int width, ref float[,] sm, int which)
        {
            int i, j, k, I1, I2;
            float x;

            for (i = 0; i < nr; i++)
                for (j = 0; j < nc; j++)
                {
                    x = 0.0F;
                    for (k = 1; k < width; k++)
                    {
                        if (which == 0)
                        {
                            I1 = (i + k) % nr; I2 = (i - k + nr) % nr;
                            x += -gau[k] * im[I1, j] + gau[k] * im[I2, j];
                        }
                        else
                        {
                            I1 = (j + k) % nc; I2 = (j - k + nc) % nc;
                            x += -gau[k] * im[i, I1] + gau[k] * im[i, I2];
                        }
                    }
                    sm[i, j] = x;
                }
        }

        private static void Nonmax_suppress(ref float[,] dx, ref float[,] dy, ref Bitmap mag, ref Bitmap ori)
        {
            int i, j;
            float xx, yy, g2, g1, g3, g4, g, xc, yc;

            int magW = mag.Width;
            int magH = mag.Height;
            int[,] magData = BitmapUtils.ToIntArray2D(mag);
            int[,] oriData = BitmapUtils.ToIntArray2D(ori);

            for (i = 1; i < magH - 1; i++)
            {
                for (j = 1; j < magW - 1; j++)
                {
                    magData[i, j] = 0;

                    /* Treat the x and y derivatives as components of a vector */
                    xc = dx[i, j];
                    yc = dy[i, j];
                    if (Math.Abs(xc) < 0.01 && Math.Abs(yc) < 0.01) continue;

                    g = Norm(xc, yc);

                    /* Follow the gradient direction, as indicated by the direction of
                       the vector (xc, yc); retain pixels that are a local maximum. */

                    if (Math.Abs(yc) > Math.Abs(xc))
                    {
                        /* The Y component is biggest, so gradient direction is basically UP/DOWN */
                        xx = Math.Abs(xc) / Math.Abs(yc);
                        yy = 1.0F;

                        g2 = Norm(dx[i - 1, j], dy[i - 1, j]);
                        g4 = Norm(dx[i + 1, j], dy[i + 1, j]);
                        if (xc * yc > 0.0)
                        {
                            g3 = Norm(dx[i + 1, j + 1], dy[i + 1, j + 1]);
                            g1 = Norm(dx[i - 1, j - 1], dy[i - 1, j - 1]);
                        }
                        else
                        {
                            g3 = Norm(dx[i + 1, j - 1], dy[i + 1, j - 1]);
                            g1 = Norm(dx[i - 1, j + 1], dy[i - 1, j + 1]);
                        }
                    }
                    else
                    {
                        /* The X component is biggest, so gradient direction is basically LEFT/RIGHT */
                        xx = Math.Abs(yc) / Math.Abs(xc);
                        yy = 1.0F;

                        g2 = Norm(dx[i, j + 1], dy[i, j + 1]);
                        g4 = Norm(dx[i, j - 1], dy[i, j - 1]);
                        if (xc * yc > 0.0)
                        {
                            g3 = Norm(dx[i - 1, j - 1], dy[i - 1, j - 1]);
                            g1 = Norm(dx[i + 1, j + 1], dy[i + 1, j + 1]);
                        }
                        else
                        {
                            g1 = Norm(dx[i - 1, j + 1], dy[i - 1, j + 1]);
                            g3 = Norm(dx[i + 1, j - 1], dy[i + 1, j - 1]);
                        }
                    }

                    /* Compute the interpolated value of the gradient magnitude */
                    if ((g > (xx * g1 + (yy - xx) * g2)) &&
                        (g > (xx * g3 + (yy - xx) * g4)))
                    {
                        if (g * MAG_SCALE <= 255)
                            magData[i, j] = (char)(g * MAG_SCALE);
                        else
                            magData[i, j] = 255;
                        oriData[i, j] = (int)Math.Atan2(yc, xc) * ORI_SCALE;
                    }
                    else
                    {
                        magData[i, j] = 0;
                        oriData[i, j] = 0;
                    }
                }
            }
            mag = BitmapUtils.ToBitmap(magData);
            ori = BitmapUtils.ToBitmap(oriData);
        }

        private static void Estimate_thresh(Bitmap mag, int halfWidth, ref int hi, ref int low)
        {
            int i, j, k;
            int[] hist = new int[256];
            int count;

            int magW = mag.Width;
            int magH = mag.Height;
            int[,] magData = BitmapUtils.ToIntArray2D(mag);

            /* Build a histogram of the magnitude image. */
            for (k = 0; k < 256; k++) hist[k] = 0;

            for (i = halfWidth; i < magH - halfWidth; i++)
                for (j = halfWidth; j < magW - halfWidth; j++)
                    hist[magData[i, j]]++;

            /* The high threshold should be > 80 or 90% of the pixels
            */
            j = magH;
            if (j < magW) j = magW;
            j = (int)(0.9 * j);
            k = 255;

            count = hist[255];
            while (count < j)
            {
                k--;
                if (k < 0) break;
                count += hist[k];
            }
            hi = k;

            i = 0;
            while (hist[i] == 0) i++;

            low = Convert.ToInt32((hi + i) / 2.0);
        }

        private static int Range(Bitmap im, int i, int j)
        {
            int w = im.Width;
            int h = im.Height;
            if ((i < 0) || (i > h)) return 0;
            if ((j < 0) || (j > w)) return 0;
            return 1;
        }
    }
}