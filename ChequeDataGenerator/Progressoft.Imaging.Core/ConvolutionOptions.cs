﻿using System;

namespace Progressoft.Imaging.Core
{
    public class ConvolutionOptions
    {
        public bool PreservceEdgesColor { get; }
        public byte EdgeGrayValue { get; }

        public float[,] Kernel { get; }
        public bool InvertColor { get; }
        public float Weight { get; }
        public int LeftMargine { get; }
        public bool IsOddKernel { get; }
        public float Bias { get; }

        public int KernelSize { get; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Major Code Smell", "S112:General exceptions should never be thrown", Justification = "<Pending>")]
        public ConvolutionOptions(
                            float[,] kernel,
            bool preservceEdgesColor = true,
            byte edgeGrayValue = 255,
            bool invertColor = false,
            float weight = 1,
            float bias = 0)
        {
            PreservceEdgesColor = preservceEdgesColor;
            EdgeGrayValue = edgeGrayValue;
            Kernel = kernel;
            InvertColor = invertColor;
            Weight = weight;
            KernelSize = Kernel.GetLength(0);
            Bias = bias;
            if (KernelSize % 2 == 0)
            {
                LeftMargine = 0;
                IsOddKernel = false;
            }
            else
            {
                LeftMargine = Kernel.GetLength(0) / 2;
                IsOddKernel = true;
            }

            if (kernel.GetLength(0) != Kernel.GetLength(1))
                throw new Exception("Kernel's width & height must be equivalent");
        }
    }

    public class GradientConvolutionOptions
    {
        public bool PreservceEdgesColor { get; }
        public byte EdgeGrayValue { get; }

        public float[,] KernelX { get; }
        public float[,] KernelY { get; }
        public bool InvertColor { get; }
        public float Weight { get; }
        public int LeftMargine { get; }
        public bool IsOddKernel { get; }
        public float Bias { get; }

        public int KernelSize { get; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Major Code Smell", "S112:General exceptions should never be thrown", Justification = "<Pending>")]
        public GradientConvolutionOptions(
                            float[,] kernelGx,
                            float[,] kernelGy,
            bool preservceEdgesColor = true,
            byte edgeGrayValue = 255,
            bool invertColor = false,
            float weight = 1,
            float bias = 0)
        {
            if (kernelGx.GetLength(0) != kernelGx.GetLength(1))
                throw new Exception("X-Kernel's width & height must be equivalent");

            if (kernelGy.GetLength(0) != kernelGy.GetLength(1))
                throw new Exception("Y-Kernel's width & height must be equivalent");

            if (kernelGy.GetLength(0) != kernelGx.GetLength(0))
                throw new Exception("Both kernels must have same size.");

            PreservceEdgesColor = preservceEdgesColor;
            EdgeGrayValue = edgeGrayValue;
            KernelX = kernelGx;
            KernelY = kernelGy;
            InvertColor = invertColor;
            Weight = weight;
            KernelSize = KernelX.GetLength(0);
            Bias = bias;
            if (KernelSize % 2 == 0)
            {
                LeftMargine = 0;
                IsOddKernel = false;
            }
            else
            {
                LeftMargine = KernelX.GetLength(0) / 2;
                IsOddKernel = true;
            }
        }
    }
}