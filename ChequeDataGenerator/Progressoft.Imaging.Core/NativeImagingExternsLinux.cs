﻿using System.Runtime.InteropServices;

namespace Progressoft.Imaging.Core
{
    internal static unsafe class NativeImagingExternsLinux
    {
        private const string DLL_PATH = @"libNativeImaging.so";

        [DllImport(DLL_PATH,
            CharSet = CharSet.Ansi,
            CallingConvention = CallingConvention.Cdecl,
            EntryPoint = "backgroundAdaptiveThresholdApi")]
        public static extern void BackgroundAdaptiveThreshold(void* image,
            int width,
            int height,
            int stride,
            double alpha,
            void* outputImage,
            int outputImageStride);

        [DllImport(DLL_PATH,
            CharSet = CharSet.Ansi,
            CallingConvention = CallingConvention.Cdecl,
            EntryPoint = "resizeBiCubicInterpolationApi")]
        public static extern void ResizeBiCubicInterpolation(void* image,
            int dstWidth,
            int dstHeight,
            int srcWidth,
            int srcHeight,
            int srcStride,
            int srcChannels,
            void* outputImage,
            int outputImageStride);
    }
}