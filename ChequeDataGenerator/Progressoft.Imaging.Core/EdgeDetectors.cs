using Progressoft.Imaging.Core.Common;
using System.Drawing;
using System.Drawing.Imaging;

namespace Progressoft.Imaging.Core
{
    public static unsafe class EdgeDetectors
    {
        public static Bitmap Sobel3x3(Bitmap inputImage)
        {
            float[,] SobelMask3X3GX = new float[3, 3]
                                                 {
                                                    {-1, 0, 1},
                                                    {-1, 0, 1},
                                                    {-1, 0, 1}
                                                 };
            float[,] SobelMask3X3GY = new float[3, 3]
                                         {
                                                {1, 1, 1},
                                                {0, 0, 0},
                                                {-1, -1, -1}
                                         };
            GradientConvolutionOptions options = new GradientConvolutionOptions(
                SobelMask3X3GX,
                SobelMask3X3GY,
                false,
                0,
                true,
                1);
            return ImageConvolution.Convolute(inputImage, options);
        }

        public static Bitmap Roberts2x2(Bitmap inputImage)
        {
            float[,] robertX = new float[2, 2]
                                                 {
                                                    {1, 0},
                                                    {0, -1}
                                                 };
            float[,] robertY = new float[2, 2]
                                         {
                                                {0, 1},
                                                    {-1, 0}
                                         };
            GradientConvolutionOptions options = new GradientConvolutionOptions(
                robertX,
                robertY,
                false,
                0,
                true,
                1);
            return ImageConvolution.Convolute(inputImage, options);
        }

        public static Bitmap Laplacian5x5(Bitmap inputImage)
        {
            float[,] kernel = new float[5, 5]
                                       {
                                        {-1, -1, -1, -1, -1},
                                        {-1, -1, -1, -1, -1},
                                        {-1, -1, 24, -1, -1},
                                        {-1, -1, -1, -1, -1},
                                        {-1, -1, -1, -1, -1}
                                       };
            ConvolutionOptions options = new ConvolutionOptions(kernel, false, 0, true, 1, 0);
            return ImageConvolution.Convolute(inputImage, options);
        }

        public static Bitmap Laplacian3x3(Bitmap inputImage)
        {
            float[,] kernel = new float[3, 3]
                                         { { -1, -1, -1, },
                                         { -1,  8, -1, },
                                         { -1, -1, -1, }, };
            ConvolutionOptions options = new ConvolutionOptions(kernel, false, 0, true, 1, 0);
            return ImageConvolution.Convolute(inputImage, options);
        }

        public static Bitmap LaplacianOfGaussian5x5(Bitmap inputImage)
        {
            float[,] kernel = new float[5, 5]
                                         { {  0,  0, -1,  0,  0 },
                                          {  0, -1, -2, -1,  0 },
                                          { -1, -2, 16, -2, -1 },
                                          {  0, -1, -2, -1,  0 },
                                          {  0,  0, -1,  0,  0 } };
            ConvolutionOptions options = new ConvolutionOptions(kernel, false, 0, true, 1, 0);
            return ImageConvolution.Convolute(inputImage, options);
        }

        public static Bitmap BinaryContoursDetection(Bitmap inputImage)
        {
            // the needed BitmapData objects to lock images data:

            try
            {
                if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed)
                    return null;

                // get width and height:
                int width = inputImage.Width;
                int height = inputImage.Height;
                Rectangle rectLock = new Rectangle(0, 0, width, height);
                // lock the bitmap to be processed:
                BitmapData bmpData = inputImage.LockBits(rectLock, ImageLockMode.ReadWrite, inputImage.PixelFormat);
                System.IntPtr scan0 = bmpData.Scan0;
                int stride = bmpData.Stride;
                int offset = stride - width;

                Bitmap outputImage = BitmapFactory.CreateGrayImage(width, height);
                BitmapData outBmpData = outputImage.LockBits(rectLock, ImageLockMode.ReadWrite, outputImage.PixelFormat);
                System.IntPtr outScan0 = outBmpData.Scan0;
                int outStride = outBmpData.Stride;
                int outOffset = outStride - width;

                byte* p = (byte*)(void*)scan0;
                byte* outP = (byte*)(void*)outScan0;

                for (int y = 0; y < height; y++)
                {
                    outP[y * stride] = 255;
                    outP[(y * stride) + (width - 1)] = 255;
                }

                for (int x = 0; x < width; x++)
                {
                    outP[x] = p[x];
                    outP[((height - 1) * stride) + x] = p[(height - 1) * stride + x];
                }

                p += stride + 1;
                outP += outStride + 1;

                for (int y = 1; y < height - 1; y++)
                {
                    for (int x = 1; x < width - 1; x++)
                    {
                        // find the upper side of the contour:
                        if ((p - stride)[0] == 0)
                            outP[0] = 255;
                        else
                            outP[0] = p[0];

                        // find the left side of the contour and merge it
                        // with previouse result:
                        if ((p - 1)[0] == 0)
                            outP[0] = (byte)(outP[0] & 255);
                        else
                            outP[0] = (byte)(outP[0] & p[0]);

                        // find the bottom side of the contour and merge it
                        // with previouse result:
                        if ((p + stride)[0] == 0)
                            outP[0] = (byte)(outP[0] & 255);
                        else
                            outP[0] = (byte)(outP[0] & p[0]);

                        // find the right side of the contour and merge it
                        //with previouse result:
                        if ((p + 1)[0] == 0)
                            outP[0] = (byte)(outP[0] & 255);
                        else
                            outP[0] = (byte)(outP[0] & p[0]);

                        p++;
                        outP++;
                    }
                    p += offset + 2;
                    outP += outOffset + 2;
                }
                inputImage.UnlockBits(bmpData);
                outputImage.UnlockBits(outBmpData);
                return outputImage;
            }
            catch
            {
                return null;
            }
        }

        public static Bitmap Canny(Bitmap inputImage, int LowThershold, int HighThreshold)
        {
            Bitmap outputImage = null;
            CannyEdgeDetector.CannyFilter(inputImage, LowThershold, HighThreshold, ref outputImage);
            return outputImage;
        }
    }
}