﻿using System;
using System.Collections;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using Progressoft.Imaging.Core.Common;

namespace Progressoft.Imaging.Core
{
    public static unsafe class Binarization
    {
        private const double FloatEpsilon = 1e-5;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0066:Convert switch statement to expression",
            Justification = "<Pending>")]
        public static Bitmap BinarizeImage(Bitmap bmpImage, BinarizationMethod iBinarizationMethod,
            byte threshold = 128)
        {
            Bitmap bmp8Bits = ColorDepthConversion.To8Bits(bmpImage, false);

            Bitmap bmpBinarized = null;
            if (bmp8Bits != null)
            {
                switch (iBinarizationMethod)
                {
                    case BinarizationMethod.CleverMethod:
                        bmpBinarized = Clever(bmp8Bits);
                        break;

                    case BinarizationMethod.OtsuMethod:
                        bmpBinarized = Otsu(bmp8Bits, out byte iTh);
                        break;

                    case BinarizationMethod.DynamicAdaptiveMethod:
                        bmpBinarized = DynamicAdaptive(bmp8Bits, 35, 55);
                        break;

                    case BinarizationMethod.IterativeMethod:
                        bmpBinarized = BinarizationBasedIterativeMethod(bmp8Bits);
                        break;

                    case BinarizationMethod.MeanMinusSdMethod:
                        bmpBinarized = ThresholdingBasedOnMean_Minus_SD(bmp8Bits);
                        break;

                    case BinarizationMethod.MeanMinusAsdMethod:
                        bmpBinarized = ThresholdingBasedOnMean_Minus_ASD(bmp8Bits);
                        break;

                    case BinarizationMethod.MeanMethod:
                        bmpBinarized = MeanThreshold(bmp8Bits, threshold, 0.8f);
                        break;

                    case BinarizationMethod.Niblak:
                        bmpBinarized = Niblack(bmp8Bits, 20, 0.2f);
                        break;

                    case BinarizationMethod.SauvolaPietikainen:
                        bmpBinarized = SauvolaPietikainen(bmp8Bits, 20, 0.5f, 128.0f);
                        break;

                    case BinarizationMethod.WolfEtEls:
                        bmpBinarized = Wolf(bmp8Bits, 20, 0.5f);
                        break;

                    case BinarizationMethod.BitPlane:
                        bmpBinarized = BitPlane(bmp8Bits);
                        break;

                    case BinarizationMethod.BackgroundAdaptive:
                        bmpBinarized = NativeImagingWrapper.BackgroundAdaptiveThreshold(bmp8Bits);
                        break;

                    default:
                        bmpBinarized = bmp8Bits.Clone() as Bitmap;
                        break;
                }

                bmp8Bits.Dispose();
            }

            return bmpBinarized;
        }

        public static Bitmap Threshold(Bitmap inputImage, byte thresholdValue, bool applyBinarization)
        {
            try
            {
                if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed) return null;

                int w = inputImage.Width;
                int h = inputImage.Height;

                Bitmap outputImage = inputImage.Clone() as Bitmap;
                BitmapData bmData = outputImage.LockBits(new Rectangle(0, 0, w, h), ImageLockMode.ReadWrite,
                    inputImage.PixelFormat);
                byte* p = (byte*) (void*) bmData.Scan0;
                int nOffset = bmData.Stride - w;

                for (int y = 0; y < h; y++, p += nOffset)
                for (int x = 0; x < w; x++, p++)
                    p[0] = (p[0] > thresholdValue) ? (byte) 255 :
                        applyBinarization ? (byte) 0 : p[0];

                outputImage.UnlockBits(bmData);
                return outputImage;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static Bitmap MeanThreshold(Bitmap inputImage, byte thresholdValue, float weight)
        {
            float weightedMean = weight * Statistics.GetMeanAboveThreshold(inputImage, thresholdValue);
            return weightedMean > -1 && weightedMean < 256 ? Threshold(inputImage, (byte) weightedMean, true) : null;
        }

        public static Bitmap OpenCvOtsu(Bitmap inputImage)
        {
            BitmapData inputImageData = null;
            BitmapData outputImageData = null;
            Bitmap outputImage = null;

            try
            {
                if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed) return null;

                int height = inputImage.Height;
                int width = inputImage.Width;

                outputImage = BitmapFactory.CreateGrayImage(width, height);

                inputImageData = inputImage.LockBits(new Rectangle(0, 0, width, height),
                    ImageLockMode.ReadWrite,
                    inputImage.PixelFormat);
                outputImageData = outputImage.LockBits(new Rectangle(0, 0, width, height),
                    ImageLockMode.ReadWrite,
                    outputImage.PixelFormat);

                int inputStride = inputImageData.Stride;
                int inputOffset = inputStride - inputImageData.Width;
                byte* inputImageDataPtr = (byte*) (void*) inputImageData.Scan0;

                var histogram = new int[256];
                for (var y = 0; y < height; y++)
                {
                    for (var x = 0; x < width; x++)
                    {
                        int i = inputImageDataPtr[0];
                        histogram[i]++;
                        inputImageDataPtr++;
                    }

                    inputImageDataPtr += inputOffset;
                }

                double scale = 1.0 / (height * width);
                double mu = histogram.Select((x, i) => x * i).Sum() * scale;

                double q1 = 0;
                var maxVal = 0;
                double maxSigma = -1;
                double mu1 = 0;

                for (int t = 0; t < 256; t++)
                {
                    var pt = histogram[t] * scale;
                    mu1 *= q1;
                    q1 += pt;
                    var q2 = 1.0 - q1;

                    if (Math.Min(q1, q2) < FloatEpsilon || Math.Max(q1, q2) > 1.0 - FloatEpsilon)
                    {
                        continue;
                    }

                    mu1 = (mu1 + t * pt) / q1;
                    double mu2 = (mu - q1 * mu1) / q2;

                    var sigma = q1 * q2 * (mu1 - mu2) * (mu1 - mu2);
                    if (sigma > maxSigma)
                    {
                        maxSigma = sigma;
                        maxVal = t;
                    }
                }

                int outputStride = outputImageData.Stride;
                int outputOffset = outputStride - width;
                byte* outputImageDataPtr = (byte*) (void*) outputImageData.Scan0;
                inputImageDataPtr = (byte*) (void*) inputImageData.Scan0;

                byte threshold = Convert.ToByte(maxVal);
                var black = Convert.ToByte(0);
                var white = Convert.ToByte(255);

                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        *outputImageDataPtr = *inputImageDataPtr > threshold ? white : black;
                        outputImageDataPtr++;
                        inputImageDataPtr++;
                    }

                    outputImageDataPtr += outputOffset;
                    inputImageDataPtr += inputOffset;
                }

                return outputImage;
            }
            catch
            {
                return outputImage;
            }
            finally
            {
                if (inputImageData != null)
                {
                    inputImage.UnlockBits(inputImageData);
                }

                if (outputImage != null && outputImageData != null)
                {
                    outputImage.UnlockBits(outputImageData);
                }
            }
        }

        public static int OpenCvOtsuThreshold(Bitmap inputImage)
        {
            BitmapData inputImageData = null;

            try
            {
                if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed) return -1;

                int height = inputImage.Height;
                int width = inputImage.Width;

                inputImageData = inputImage.LockBits(new Rectangle(0, 0, width, height),
                    ImageLockMode.ReadWrite,
                    inputImage.PixelFormat);

                int stride = inputImageData.Stride;
                int offset = stride - inputImage.Width;
                var scan0 = inputImageData.Scan0;
                byte* inputImageDataPtr = (byte*) (void*) scan0;

                var histogram = new int[256];
                for (var y = 0; y < height; y++)
                {
                    for (var x = 0; x < width; x++)
                    {
                        int i = inputImageDataPtr[0];
                        histogram[i]++;
                        inputImageDataPtr++;
                    }

                    inputImageDataPtr += offset;
                }

                double scale = 1.0 / (height * width);
                double mu = histogram.Select((x, i) => x * i).Sum() * scale;

                double q1 = 0;
                var maxVal = 0;
                double maxSigma = -1;
                double mu1 = 0;

                for (int t = 0; t < 256; t++)
                {
                    var pt = histogram[t] * scale;
                    mu1 *= q1;
                    q1 += pt;
                    var q2 = 1.0 - q1;

                    if (Math.Min(q1, q2) < FloatEpsilon || Math.Max(q1, q2) > 1.0 - FloatEpsilon)
                    {
                        continue;
                    }

                    mu1 = (mu1 + t * pt) / q1;
                    double mu2 = (mu - q1 * mu1) / q2;

                    var sigma = q1 * q2 * (mu1 - mu2) * (mu1 - mu2);
                    if (sigma > maxSigma)
                    {
                        maxSigma = sigma;
                        maxVal = t;
                    }
                }

                return Convert.ToByte(maxVal);
            }
            catch
            {
                return -1;
            }
            finally
            {
                if (inputImageData != null)
                {
                    inputImage.UnlockBits(inputImageData);
                }
            }
        }

        public static int OpenCvOtsuThresholdNonZero(Bitmap inputImage)
        {
            BitmapData inputImageData = null;

            try
            {
                if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed) return -1;

                int height = inputImage.Height;
                int width = inputImage.Width;

                inputImageData = inputImage.LockBits(new Rectangle(0, 0, width, height),
                    ImageLockMode.ReadWrite,
                    inputImage.PixelFormat);

                int stride = inputImageData.Stride;
                int offset = stride - inputImage.Width;
                var scan0 = inputImageData.Scan0;
                byte* inputImageDataPtr = (byte*) (void*) scan0;

                var histogram = new int[256];
                int n = 0;

                for (var y = 0; y < height; y++)
                {
                    for (var x = 0; x < width; x++)
                    {
                        int i = inputImageDataPtr[0];
                        if (i > 0)
                        {
                            histogram[i]++;
                            n++;
                        }

                        inputImageDataPtr++;
                    }

                    inputImageDataPtr += offset;
                }

                double scale = 1.0 / n;
                double mu = histogram.Select((x, i) => x * i).Sum() * scale;

                double q1 = 0;
                var maxVal = 0;
                double maxSigma = -1;
                double mu1 = 0;

                for (int t = 0; t < 256; t++)
                {
                    var pt = histogram[t] * scale;
                    mu1 *= q1;
                    q1 += pt;
                    var q2 = 1.0 - q1;

                    if (Math.Min(q1, q2) < FloatEpsilon || Math.Max(q1, q2) > 1.0 - FloatEpsilon)
                    {
                        continue;
                    }

                    mu1 = (mu1 + t * pt) / q1;
                    double mu2 = (mu - q1 * mu1) / q2;

                    var sigma = q1 * q2 * (mu1 - mu2) * (mu1 - mu2);
                    if (sigma > maxSigma)
                    {
                        maxSigma = sigma;
                        maxVal = t;
                    }
                }

                return Convert.ToByte(maxVal);
            }
            catch
            {
                return -1;
            }
            finally
            {
                if (inputImageData != null)
                {
                    inputImage.UnlockBits(inputImageData);
                }
            }
        }

        public static Bitmap Otsu(Bitmap inputImage, out byte otsuThreshold)
        {
            return Otsu(inputImage, true, out otsuThreshold);
        }

        public static Bitmap Otsu(Bitmap inputImage, bool applyBinarization, out byte otsuThreshold)
        {
            Bitmap outputImage = null;
            BitmapData bmDataout = null;
            otsuThreshold = 0;
            try
            {
                if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed) return null;
                outputImage = (Bitmap) inputImage.Clone();
                bmDataout = outputImage.LockBits(new Rectangle(0, 0, outputImage.Width, outputImage.Height),
                    ImageLockMode.ReadWrite, outputImage.PixelFormat);

                int stride = bmDataout.Stride;
                int h = outputImage.Height;
                int w = outputImage.Width;
                System.IntPtr Scan0 = bmDataout.Scan0;
                int[] hist = new int[260];
                float y, z, ut, vt;
                float[] pb = new float[260];
                int m;
                for (int i = 0; i < 256; i++)
                {
                    hist[i] = 0;
                    pb[i] = (float) 0.0;
                }

                byte* p = (byte*) (void*) Scan0;

                int nOffset = stride - outputImage.Width;
                long N = w * h;
                int otsus;

                for (int yy = 0; yy < h; ++yy)
                {
                    for (int x = 0; x < w; ++x)
                    {
                        int kk = p[0];
                        hist[kk] += 1;
                        p++;
                    }

                    p += nOffset;
                }

                for (int i = 0; i < 256; i++)
                {
                    pb[i] = (float) (hist[i]) / (float) N;
                }

                ut = pb.Take(256).Select((pbValue, index) => index * pbValue).Sum();

                vt = (float) 0.0;
                for (int i = 0; i < 256; i++)
                    vt += (i - ut) * (i - ut) * pb[i];
                int j = -1, k = -1;
                for (int i = 0; i < 256; i++)
                {
                    if ((j < 0) && (pb[i] > 0.0)) j = i;
                    if (pb[i] > 0.0) k = i;
                }

                z = (float) -1.0;
                m = -1;
                for (int i = j; i <= k; i++)
                {
                    y = Nu(pb, i, ut, vt);
                    if (y >= z)
                    {
                        z = y;
                        m = i;
                    }
                }

                otsus = m;
                otsuThreshold = (byte) otsus;
                byte* p1 = (byte*) (void*) Scan0;
                for (int yz = 0; yz < h; ++yz)
                {
                    for (int x = 0; x < w; ++x)
                    {
                        p1[0] = (p1[0] >= otsus) ? (byte) 255 :
                            applyBinarization ? (byte) 0 : p1[0];
                        p1++;
                    }

                    p1 += nOffset;
                }

                outputImage.UnlockBits(bmDataout);
                return outputImage;
            } //try
            catch
            {
                return null;
            }
        }

        public static Bitmap OtsuGray(Bitmap inputImage, out byte otsuThreshold)
        {
            return Otsu(inputImage, false, out otsuThreshold);
        }

        /// <summary>
        /// gets the statistics values for not 255-intensity-values.
        /// </summary>
        /// <param name="inputImage"></param>
        /// <param name="fMean"></param>
        /// <param name="fStdDev"></param>
        /// <param name="fAbsStdDev"></param>
        /// <returns></returns>
        public static bool GetNon255Statistics(Bitmap inputImage, out float fMean, out float fStdDev,
            out float fAbsStdDev)
        {
            fMean = 0;
            fStdDev = 0;
            fAbsStdDev = 0;
            float non255Count = 0f;

            try
            {
                if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed)
                    return false;

                int width = inputImage.Width;
                int height = inputImage.Height;

                BitmapData bmpData = inputImage.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite,
                    inputImage.PixelFormat);
                System.IntPtr scan0 = bmpData.Scan0;
                int stride = bmpData.Stride;

                byte* p = (byte*) (void*) scan0;
                int offset = stride - width;

                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        if (p[0] != 255)
                        {
                            fMean += p[0];
                            non255Count++;
                        }

                        p++;
                    }

                    p += offset;
                }

                fMean /= non255Count;

                p = (byte*) (void*) scan0;

                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        if (p[0] != 255)
                        {
                            fStdDev += (float) Math.Pow(p[0] - fMean, 2d);
                            fAbsStdDev += Math.Abs(p[0] - fMean);
                        }

                        p++;
                    }

                    p += offset;
                }

                fStdDev = (float) Math.Sqrt(fStdDev / (non255Count - 1));
                fAbsStdDev /= (non255Count);

                inputImage.UnlockBits(bmpData);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static Bitmap BinarizationBasedIterativeMethod(Bitmap inputImage)
        {
            if (!GetNon255Statistics(inputImage, out float mean, out float stdDev, out _))
                return null;
            float intial = mean - stdDev;
            float kth = Get_KthMean_ForIterativeThresholding(inputImage, intial);
            return kth > -1 ? Threshold(inputImage, (byte) kth, true) : null;
        }

        public static Bitmap BitPlane(Bitmap inputImage)
        {
            try
            {
                if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed)
                    return null;

                int width = inputImage.Width;
                int height = inputImage.Height;

                Bitmap outputImage = (Bitmap) inputImage.Clone();

                BitmapData outBmpData = outputImage.LockBits(new Rectangle(0, 0, width, height),
                    ImageLockMode.ReadWrite, inputImage.PixelFormat);
                System.IntPtr outScan0 = outBmpData.Scan0;
                int outOffset = outBmpData.Stride - width;

                byte* outP = (byte*) (void*) outScan0;

                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        outP[0] = (byte) (outP[0] & 0x81);
                        outP[0] = (byte) (outP[0] >> 7);
                        outP[0] = (byte) (outP[0] * 255);

                        outP++;
                    }

                    outP += outOffset;
                }

                outputImage.UnlockBits(outBmpData);
                return outputImage;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Binarization based on (Mean - Abs_Std_Dev)
        /// </summary>
        /// <param name="bmpImg"></param>
        /// <param name="bmpResult"></param>
        /// <returns></returns>
        public static Bitmap ThresholdingBasedOnMean_Minus_ASD(Bitmap bmpImg)
        {
            if (!GetNon255Statistics(bmpImg, out float mean, out _, out float absStdDev))
                return null;
            return Threshold(bmpImg, (byte) (mean - absStdDev), true);
        }

        /// <summary>
        /// Binarization based on (Mean - Std_Dev)
        /// </summary>
        /// <param name="bmpImg"></param>
        /// <param name="bmpResult"></param>
        /// <returns></returns>
        public static Bitmap ThresholdingBasedOnMean_Minus_SD(Bitmap bmpImg)
        {
            if (!GetNon255Statistics(bmpImg, out float mean, out float stdDev, out _))
                return null;
            return Threshold(bmpImg, (byte) (mean - stdDev), true);
        }

        /// <summary>
        /// Binarization based on (2 * Std_Dev)
        /// </summary>
        /// <param name="bmpImg"></param>
        /// <param name="bmpResult"></param>
        /// <returns></returns>
        public static Bitmap ThresholdingBasedOn2SD(Bitmap bmpImg)
        {
            if (!GetNon255Statistics(bmpImg, out _, out float stdDev, out _))
                return null;
            return Threshold(bmpImg, (byte) (2 * stdDev), true);
        }

        public static Bitmap ThresholdingBasedOn2ASD(Bitmap bmpImg)
        {
            if (!GetNon255Statistics(bmpImg, out _, out _, out float absStdDev))
                return null;
            return Threshold(bmpImg, (byte) (2 * absStdDev), true);
        }

        public static Bitmap Clever(Bitmap inputImage)
        {
            //1. apply mean method
            Bitmap bmpTemp = (Bitmap) inputImage.Clone();
            Bitmap bmpOriginal = (Bitmap) inputImage.Clone();
            Bitmap m_Bitmap = MeanThreshold(inputImage, 50, 0.8f);

            try
            {
                //2. Find E on Mean method result:

                #region "Declare"

                int nWidth = m_Bitmap.Width;
                int nHeight = m_Bitmap.Height;
                float E_mean = 0;
                float E_otsu = 0;

                #endregion "Declare"

                #region "E_mean"

                Bitmap bmpStat =
                    m_Bitmap.Clone(
                        new Rectangle((int) (0.4 * nWidth), (int) (0.3 * nHeight), (int) (0.6 * nWidth),
                            (int) (0.7 * nHeight)), m_Bitmap.PixelFormat);
                float meanVal = Statistics.GetMean(bmpStat);
                float SD = Statistics.GetStandardDeviation(bmpStat, meanVal);
                if (bmpStat != null)
                {
                    bmpStat.Dispose();
                }

                //B. Find the following formula:
                //E=(Mean-SD)/SD
                E_mean = (meanVal - SD) / SD;

                #endregion "E_mean"

                if (E_mean < 1.3 && nWidth > 300 && nHeight > 300)
                {
                    //If E<1.2 use Dynamic

                    #region "Dynamic Adaptive"

                    bmpTemp = DynamicAdaptive(bmpTemp, 35, 55);
                    m_Bitmap = (Bitmap) bmpTemp.Clone();
                    return m_Bitmap;

                    #endregion "Dynamic Adaptive"
                }
                else if (1.3 < E_mean && E_mean < 2.5)
                {
                    //1<E<2.5 use Otsu

                    #region "Otsu"

                    bmpTemp = Otsu(bmpTemp, out byte otsuVal);

                    #endregion "Otsu"

                    //C. if otsu is found apply the follwing:
                    //find E2 in same way.
                    //if E<1 return to the mean method.(E>1 make the otsu)

                    #region "E_otsu"

                    Bitmap bmpStat2 =
                        bmpTemp.Clone(
                            new Rectangle((int) (0.4 * nWidth), (int) (0.3 * nHeight), (int) (0.6 * nWidth),
                                (int) (0.7 * nHeight)), bmpTemp.PixelFormat);
                    meanVal = Statistics.GetMean(bmpStat2);
                    SD = Statistics.GetStandardDeviation(bmpStat2, meanVal);
                    E_otsu = (meanVal - SD) / SD;
                    if (bmpStat2 != null)
                    {
                        bmpStat2.Dispose();
                    }

                    #endregion "E_otsu"

                    #region "Decision on Otsu"

                    if (E_otsu < 0.5)
                    {
                        //use Dynamic
                        bmpTemp = DynamicAdaptive(bmpOriginal, 35, 55);
                        m_Bitmap = (Bitmap) bmpTemp.Clone();
                        return m_Bitmap;
                    }
                    else if (E_mean < 1.3 || E_otsu < 0.5 || E_mean > 2.5 || E_otsu > 3.5)
                    {
                        //Use Mean method:
                        return m_Bitmap;
                    }
                    else
                    {
                        ArrayList HLinesOtsu = new ArrayList();
                        ArrayList HLinesMean = new ArrayList();
                        //HLines by otsu
                        RunLength.FindAllHLines(
                            bmpTemp.Clone(
                                new Rectangle((int) (0.4 * nWidth), (int) (0.3 * nHeight), (int) (0.6 * nWidth),
                                    (int) (0.7 * nHeight)), m_Bitmap.PixelFormat), ref HLinesOtsu);
                        RunLength.FindAllHLines(
                            m_Bitmap.Clone(
                                new Rectangle((int) (0.4 * nWidth), (int) (0.3 * nHeight), (int) (0.6 * nWidth),
                                    (int) (0.7 * nHeight)), m_Bitmap.PixelFormat), ref HLinesMean);

                        int OtsuCount = HLinesOtsu.Count;
                        int MeanCount = HLinesMean.Count;
                        double OtsuL = 0;
                        double MeanL = 0;

                        for (int i = 0; i < OtsuCount; i++)
                            if (((Line) HLinesOtsu[i]).height > 150)
                                OtsuL += ((Line) HLinesOtsu[i]).height;
                        for (int j = 0; j < MeanCount; j++)
                            if (((Line) HLinesMean[j]).height > 150)
                                MeanL += ((Line) HLinesMean[j]).height;

                        if (Math.Abs(MeanL / OtsuL - 1.00) < 0.260)
                        {
                            //Use otsu
                            m_Bitmap = (Bitmap) bmpTemp.Clone();
                            return m_Bitmap;
                        }
                        else
                        {
                            return m_Bitmap;
                        }
                    }

                    #endregion "Decision on Otsu"
                }
                else
                {
                    //else Keep the mean.
                    return m_Bitmap;
                }
            }
            catch
            {
                return null;
            }
            finally
            {
                #region "Release Resources"

                if (bmpTemp != null)
                {
                    bmpTemp.Dispose();
                }

                if (bmpOriginal != null)
                {
                    bmpOriginal.Dispose();
                }

                #endregion "Release Resources"
            }
        } //clever binarization

        public static Bitmap DynamicAdaptive(Bitmap b, int WindowSize = 35, int ContrastLevel = 55)
        {
            Bitmap bResult = null;
            BitmapData bmData = null; // for bResult
            BitmapData bmDataB = null; //b
            try
            {
                if (b.PixelFormat != PixelFormat.Format8bppIndexed)
                    return null;
                bResult = (Bitmap) b.Clone();
                //find the ratio by which the pixel density will be calculated upon
                double ThresholdRatio = 0.5;
                double Factor = 0;
                double g06 = 0.1;
                double g1 = 0.2;
                double g2 = 0.3;
                double g3 = 0.4;
                double g4 = 0.6;
                double g5 = 0.6;
                double l06 = 0.08;
                double g15 = 0.2;
                long nWhiteRatio = WindowSize * WindowSize * 255;
                int h = bResult.Height;
                int w = bResult.Width;
                //read it

                bmData = bResult.LockBits(new Rectangle(0, 0, bResult.Width, bResult.Height), ImageLockMode.ReadWrite,
                    bResult.PixelFormat);
                bmDataB = b.LockBits(new Rectangle(0, 0, b.Width, b.Height), ImageLockMode.ReadWrite, b.PixelFormat);
                int stride = bmData.Stride;
                int strideb = bmDataB.Stride;
                unsafe
                {
                    System.IntPtr Scan0 = bmData.Scan0;
                    System.IntPtr Scan0b = bmDataB.Scan0;
                    byte* p = (byte*) (void*) Scan0;
                    byte* pb = (byte*) (void*) Scan0b;

                    for (int y = 0; y < h; y++)
                    for (int x = 0; x < w; x++)
                        p[(strideb * y) + x] = 255;

                    int TotalWindowH = h;
                    int TotalWindowW = w;
                    while (TotalWindowH % WindowSize != 0) TotalWindowH--;
                    while (TotalWindowW % WindowSize != 0) TotalWindowW--;

                    //now traverse through the points
                    int CenterX = ((WindowSize - 1) / 2) + 1;
                    int CenterY = CenterX;

                    long nBlackRatio = 0;
                    for (int y = CenterY; y < TotalWindowH - CenterY; y += CenterY)
                    {
                        for (int x = CenterX; x < TotalWindowW - CenterX; x += CenterX)
                        {
                            nBlackRatio = 0;
                            int Max = 0;
                            int Min = 1000;
                            for (int x2 = x - CenterX; x2 < x + CenterX - 1; x2++)
                            {
                                for (int y2 = y - CenterY; y2 < y + CenterY - 1; y2++)
                                {
                                    nBlackRatio += pb[(stride * y2) + x2];
                                    if (pb[(stride * y2) + x2] > Max) Max = pb[(stride * y2) + x2];
                                    if (pb[(stride * y2) + x2] < Min) Min = pb[(stride * y2) + x2];
                                } //for y2
                            } //for x2

                            //find the pixel density, and determine the factor according to it.
                            double Ratio = (double) nBlackRatio / nWhiteRatio;
                            //if the window is bright then the value is near 1.
                            //if the window is dark then the value is near 0.

                            if (Ratio > ThresholdRatio) Factor = g5;
                            else if (Ratio >= 0.3 && Ratio < 0.4) Factor = g3;
                            else if (Ratio >= 0.4 && Ratio <= 0.5) Factor = g4;
                            else if (Ratio >= 0.2 && Ratio < 0.3) Factor = g2;
                            else if (Ratio >= 0.15 && Ratio < 0.2) Factor = g15;
                            else if (Ratio >= 0.1 && Ratio < 0.15) Factor = g1;
                            else if (Ratio > 0.06 && Ratio < 0.1) Factor = g06;
                            else Factor = l06;

                            double Threshold = 0;

                            //Three Aspects To Integrate:
                            //1. Static Threshold If The Pixel Density Is Too Low.
                            //2. If The Pixel Density Is In The Middle, Use The Factor Parameters.
                            //3. If The Pixel Density Is Very High, Take The Darkest Value Or Near It.
                            if ((Max - Min) < ContrastLevel)
                                Threshold = -1; //set all background.
                            else if (Ratio > 0.5)
                                Threshold = 100;
                            else if (Ratio > 0.4)
                                Threshold = 70;
                            else if (Ratio > 0.3)
                                Threshold = 60;
                            else if (Ratio > 0.15)
                                Threshold = Factor * (Max - Min);
                            else if (Ratio > 0.12)
                                Threshold = Min + 20;
                            else
                                Threshold = Min + 5;

                            for (int x2 = x - CenterX; x2 < x + CenterX - 1; x2++)
                            {
                                for (int y2 = y - CenterY; y2 < y + CenterY - 1; y2++)
                                {
                                    if (pb[(stride * y2) + x2] > 20 && Ratio < 0.25)
                                    {
                                        p[(stride * y2) + x2] = 255;
                                        continue;
                                    }

                                    if (pb[(stride * y2) + x2] > Threshold)
                                        p[(stride * y2) + x2] = 255;
                                    else
                                        p[(stride * y2) + x2] = 0;
                                } //for y2
                            } //for x2
                        } //for X
                    } //for y

                    return bResult;
                }
            } //try
            catch
            {
                return null;
            }
            finally
            {
                if (bmData != null)
                {
                    bResult.UnlockBits(bmData);
                }

                if (bmDataB != null)
                {
                    b.UnlockBits(bmDataB);
                }
            }
        } //DynamicAdaptive

        /// <summary>
        /// Applies Niblack local thresholding.
        /// Theshold = mean - k * standard_deviation
        /// </summary>
        /// <param name="inputImage">Input image.</param>
        /// <param name="iWindowWidth">Window size</param>
        /// <param name="fK">Value of parameter k in thresholding formula</param>
        /// <returns></returns>
        public static Bitmap Niblack(Bitmap inputImage, int iWindowWidth, float fK)
        {
            if (inputImage == null ||
                inputImage.PixelFormat != PixelFormat.Format8bppIndexed ||
                fK <= 0)
                return null;

            Bitmap outputImage = inputImage.Clone() as Bitmap;

            int width = outputImage.Width;
            int height = outputImage.Height;

            BitmapData bmpDataOutput = outputImage.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite,
                PixelFormat.Format8bppIndexed);
            System.IntPtr scan02 = bmpDataOutput.Scan0;
            int iStride = bmpDataOutput.Stride;

            int xBlocks = (width / iWindowWidth);
            int yBlocks = (height / iWindowWidth);

            byte* p1 = (byte*) (void*) scan02;

            for (int xb = 0; xb <= xBlocks; xb++)
            {
                int xBkStart = xb * iWindowWidth;
                int xBkend = xBkStart + iWindowWidth;
                if (xb == xBlocks)
                {
                    xBkend = width - 1;
                }

                for (int yb = 0; yb <= yBlocks; yb++)
                {
                    int yBkStart = yb * iWindowWidth;
                    int yBkend = yBkStart + iWindowWidth;

                    if (yb == yBlocks)
                    {
                        yBkend = height - 1;
                    }

                    // compute the mean ...
                    float fMean = 0.0f;
                    float fCount = 0.0f;
                    for (int y = yBkStart; y < yBkend; y++)
                    {
                        int iY_Times_Stride = y * iStride;
                        for (int x = xBkStart; x < xBkend; x++)
                        {
                            fMean += p1[iY_Times_Stride + x];
                            fCount++;
                        }
                    }

                    fMean /= fCount;

                    // compute the standard deviation ...
                    float fStd = 0.0f;
                    for (int y = yBkStart; y < yBkend; y++)
                    {
                        int iY_Times_Stride = y * iStride;
                        for (int x = xBkStart; x < xBkend; x++)
                        {
                            fStd += (p1[iY_Times_Stride + x] - fMean) *
                                    (p1[iY_Times_Stride + x] - fMean);
                        }
                    }

                    fStd *= (1.0f / fCount);
                    fStd = (float) Math.Sqrt(fStd);

                    // compute Niblack threshold ...
                    int iNiblack = (int) Math.Round(fMean - fK * fStd);

                    // apply the binarization ...
                    for (int y = yBkStart; y < yBkend; y++)
                    {
                        int iY_Times_Stride = y * iStride;
                        for (int x = xBkStart; x < xBkend; x++)
                        {
                            if (p1[iY_Times_Stride + x] >= iNiblack)
                            {
                                p1[iY_Times_Stride + x] = 255;
                            }
                            else
                            {
                                p1[iY_Times_Stride + x] = 0;
                            }
                        }
                    }
                }
            }

            outputImage.UnlockBits(bmpDataOutput);
            return outputImage;
        }

        /// <summary>
        /// Applies Sauvola local thresholding.
        /// Threshold = mean * (1 + k * ((standard_deviation / R) - 1))
        /// </summary>
        /// <param name="inputImage">Input image.</param>
        /// <param name="iWindowWidth">Window size </param>
        /// <param name="fK">Value of the positive parameter k</param>
        /// <param name="fR">Value of the positive parameter R</param>
        /// <returns></returns>
        public static Bitmap SauvolaPietikainen(Bitmap inputImage, int iWindowWidth, float fK, float fR)
        {
            if (inputImage == null ||
                inputImage.PixelFormat != PixelFormat.Format8bppIndexed ||
                fK <= 0 || fR <= 0)
                return null;

            Bitmap outputImage = inputImage.Clone() as Bitmap;

            int width = outputImage.Width;
            int height = outputImage.Height;

            BitmapData bmpDataOutput = outputImage.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite,
                PixelFormat.Format8bppIndexed);
            System.IntPtr scan02 = bmpDataOutput.Scan0;
            int iStride = bmpDataOutput.Stride;

            int xBlocks = (width / iWindowWidth);
            int yBlocks = (height / iWindowWidth);

            byte* p1 = (byte*) (void*) scan02;

            for (int xb = 0; xb <= xBlocks; xb++)
            {
                int xBkStart = xb * iWindowWidth;
                int xBkend = xBkStart + iWindowWidth;
                if (xb == xBlocks)
                {
                    xBkend = width - 1;
                }

                for (int yb = 0; yb <= yBlocks; yb++)
                {
                    int yBkStart = yb * iWindowWidth;
                    int yBkend = yBkStart + iWindowWidth;

                    if (yb == yBlocks)
                    {
                        yBkend = height - 1;
                    }

                    // compute the mean ...
                    float fMean = 0.0f;
                    float fCount = 0.0f;
                    for (int y = yBkStart; y < yBkend; y++)
                    {
                        int iY_Times_Stride = y * iStride;
                        for (int x = xBkStart; x < xBkend; x++)
                        {
                            fMean += p1[iY_Times_Stride + x];
                            fCount++;
                        }
                    }

                    fMean /= fCount;

                    // compute the standard deviation ...
                    float fStd = 0.0f;
                    for (int y = yBkStart; y < yBkend; y++)
                    {
                        int iY_Times_Stride = y * iStride;
                        for (int x = xBkStart; x < xBkend; x++)
                        {
                            fStd += (p1[iY_Times_Stride + x] - fMean) *
                                    (p1[iY_Times_Stride + x] - fMean);
                        }
                    }

                    fStd *= (1.0f / fCount);
                    fStd = (float) Math.Sqrt(fStd);

                    // compute Sauvola-Pietikainen threshold ...
                    int iSauvolaPietikainen = (int) Math.Round(fMean * (1 + fK * (fStd / fR - 1)));

                    // apply the binarization ...
                    for (int y = yBkStart; y < yBkend; y++)
                    {
                        int iY_Times_Stride = y * iStride;
                        for (int x = xBkStart; x < xBkend; x++)
                        {
                            if (p1[iY_Times_Stride + x] >= iSauvolaPietikainen)
                            {
                                p1[iY_Times_Stride + x] = 255;
                            }
                            else
                            {
                                p1[iY_Times_Stride + x] = 0;
                            }
                        }
                    }
                }
            }

            outputImage.UnlockBits(bmpDataOutput);

            return outputImage;
        }

        /// <summary>
        /// Applies Wolf et al. local thresholding.
        /// threshold= (1 - k) * mean + k* minimum_gay_value +
        ///           k* (standard_deviation / max_standard_deviation) *
        ///           (mean - minimum_gay_value)
        /// </summary>
        /// <param name="inputImage">Input image.</param>
        /// <param name="iWindowWidth">Window size</param>
        /// <param name="fK">Value of the positive parameter k</param>
        /// <returns></returns>
        public static Bitmap Wolf(Bitmap inputImage, int iWindowWidth, float fK)
        {
            if (inputImage == null ||
                inputImage.PixelFormat != PixelFormat.Format8bppIndexed ||
                fK <= 0)
                return null;

            Bitmap outputImage = inputImage.Clone() as Bitmap;

            int width = outputImage.Width;
            int height = outputImage.Height;

            BitmapData bmpdata2 = outputImage.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite,
                PixelFormat.Format8bppIndexed);
            System.IntPtr scan02 = bmpdata2.Scan0;
            int iStride = bmpdata2.Stride;

            int xBlocks = (width / iWindowWidth);
            int yBlocks = (height / iWindowWidth);

            byte* p1 = (byte*) (void*) scan02;

            // holds the minimum intesity value ...
            int iMin = 255;
            float fStdMax = -1.0f;
            float[,] fMeans = new float[xBlocks + 1, yBlocks + 1];
            float[,] fStds = new float[xBlocks + 1, yBlocks + 1];
            byte[,] byteMins = new byte[xBlocks + 1, yBlocks + 1];
            byte[,] byteMaxs = new byte[xBlocks + 1, yBlocks + 1];

            for (int xb = 0; xb <= xBlocks; xb++)
            {
                int xBkStart = xb * iWindowWidth;
                int xBkend = xBkStart + iWindowWidth;
                if (xb == xBlocks)
                {
                    xBkend = width - 1;
                }

                for (int yb = 0; yb <= yBlocks; yb++)
                {
                    int yBkStart = yb * iWindowWidth;
                    int yBkend = yBkStart + iWindowWidth;

                    if (yb == yBlocks)
                    {
                        yBkend = height - 1;
                    }

                    // compute the mean ...
                    float fMean = 0.0f;
                    float fCount = 0.0f;
                    byte byteMin = 255;
                    byte byteMax = 0;
                    for (int y = yBkStart; y < yBkend; y++)
                    {
                        int iY_Times_Stride = y * iStride;
                        for (int x = xBkStart; x < xBkend; x++)
                        {
                            fMean += p1[iY_Times_Stride + x];
                            fCount++;

                            // hold the minimum intensity value ...
                            if (p1[iY_Times_Stride + x] < iMin)
                            {
                                iMin = p1[iY_Times_Stride + x];
                            }

                            if (p1[iY_Times_Stride + x] < byteMin)
                            {
                                byteMin = p1[iY_Times_Stride + x];
                            }

                            if (p1[iY_Times_Stride + x] > byteMax)
                            {
                                byteMax = p1[iY_Times_Stride + x];
                            }
                        }
                    }

                    fMean /= fCount;

                    //hold the mean of the current block ...
                    fMeans[xb, yb] = fMean;

                    // holds the minimum intensuty value for this window ...
                    byteMins[xb, yb] = byteMin;
                    // holds the maximum intensuty value for this window ...
                    byteMaxs[xb, yb] = byteMax;

                    // compute the standard deviation ...
                    float fStd = 0.0f;
                    for (int y = yBkStart; y < yBkend; y++)
                    {
                        int iY_Times_Stride = y * iStride;
                        for (int x = xBkStart; x < xBkend; x++)
                        {
                            fStd += (p1[iY_Times_Stride + x] - fMean) *
                                    (p1[iY_Times_Stride + x] - fMean);
                        }
                    }

                    fStd *= (1.0f / fCount);
                    fStd = (float) Math.Sqrt(fStd);

                    // hold the maximum standard deviation ...
                    if (fStd > fStdMax)
                    {
                        fStdMax = fStd;
                    }

                    //hold the standard deviation of the current block ...
                    fStds[xb, yb] = fStd;
                }
            }

            for (int xb = 0; xb <= xBlocks; xb++)
            {
                int xBkStart = xb * iWindowWidth;
                int xBkend = xBkStart + iWindowWidth;
                if (xb == xBlocks)
                {
                    xBkend = width - 1;
                }

                for (int yb = 0; yb <= yBlocks; yb++)
                {
                    int yBkStart = yb * iWindowWidth;
                    int yBkend = yBkStart + iWindowWidth;

                    if (yb == yBlocks)
                    {
                        yBkend = height - 1;
                    }

                    // now we need to calculate Wolf's threshold
                    // for this window ... (original threshold)
                    int iWolf = (int) Math.Round(
                        (1 - fK) * fMeans[xb, yb] +
                        fK * iMin +
                        fK * (fStds[xb, yb] / fStdMax) * (fMeans[xb, yb] - iMin)
                    );

                    for (int y = yBkStart; y < yBkend; y++)
                    {
                        int iY_Times_Stride = y * iStride;
                        for (int x = xBkStart; x < xBkend; x++)
                        {
                            if (p1[iY_Times_Stride + x] >= iWolf)
                                p1[iY_Times_Stride + x] = 255;
                            else
                                p1[iY_Times_Stride + x] = 0;
                        }
                    }
                }
            }

            outputImage.UnlockBits(bmpdata2);
            return outputImage;
        }

        /// <summary>
        /// Used In Otsu's Method
        /// </summary>
        /// <param name="pb"></param>
        /// <param name="k"></param>
        /// <param name="ut"></param>
        /// <param name="vt"></param>
        /// <returns></returns>
        private static float Nu(float[] pb, int k, float ut, float vt) /*used in Otsu's*/
        {
            float y = pb.Take(k).Sum();
            float x = (ut * y) - pb.Take(k).Select((pbValue, index) => index * pbValue).Sum();
            x *= x;
            y *= ((float) 1.0 - y);
            x = (y > 0) ? x / y : 0f;
            return x / vt;
        }

        /// <summary>
        /// gets the kth-mean threshold based on iterative method.
        /// </summary>
        /// <param name="inputImage"></param>
        /// <param name="intialTh"></param>
        /// <param name="kthMeanTh"></param>
        /// <returns></returns>
        private static float Get_KthMean_ForIterativeThresholding(Bitmap inputImage, float intialTh)
        {
            try
            {
                if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed)
                    return float.MinValue;

                int width = inputImage.Width;
                int height = inputImage.Height;

                BitmapData bmpData = inputImage.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite,
                    inputImage.PixelFormat);
                System.IntPtr scan0 = bmpData.Scan0;
                int stride = bmpData.Stride;

                float avgM = intialTh;
                float kthMeanTh = 0;
                do
                {
                    kthMeanTh = avgM;

                    float m1 = 0f;
                    int m1PointsCount = 0;
                    float m2 = 0f;
                    int m2PointsCount = 0;

                    byte* p = (byte*) (void*) scan0;
                    int offset = stride - width;

                    for (int y = 0; y < height; y++)
                    {
                        for (int x = 0; x < width; x++)
                        {
                            if (p[0] != 255)
                            {
                                if (p[0] > kthMeanTh)
                                {
                                    m1 += p[0];
                                    m1PointsCount++;
                                }
                                else
                                {
                                    m2 += p[0];
                                    m2PointsCount++;
                                }
                            }

                            p++;
                        }

                        p += offset;
                    }

                    if (m1PointsCount == 0) m1PointsCount = 1;
                    if (m2PointsCount == 0) m2PointsCount = 1;

                    m1 /= m1PointsCount;
                    m2 /= m2PointsCount;

                    avgM = (m1 + m2) / 2.0f;
                } while ((int) Math.Round(avgM) != (int) Math.Round(kthMeanTh));

                inputImage.UnlockBits(bmpData);
                return kthMeanTh;
            }
            catch
            {
                return float.MinValue;
            }
        }

        private static Bitmap SubtractAndInvert(Bitmap bitmap1, Bitmap bitmap2)
        {
            Bitmap outputBitmap = null;

            BitmapData bitmapData1 = null;
            BitmapData bitmapData2 = null;
            BitmapData outputBitmapData = null;

            try
            {
                if (bitmap1.PixelFormat != PixelFormat.Format8bppIndexed ||
                    bitmap2.PixelFormat != PixelFormat.Format8bppIndexed ||
                    bitmap1.Width != bitmap2.Width ||
                    bitmap1.Height != bitmap2.Height)
                    return null;

                int width = bitmap1.Width;
                int height = bitmap1.Height;

                outputBitmap = BitmapFactory.CreateGrayImage(width, height);

                bitmapData1 = bitmap1.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite,
                    PixelFormat.Format8bppIndexed);
                bitmapData2 = bitmap2.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite,
                    PixelFormat.Format8bppIndexed);
                outputBitmapData = outputBitmap.LockBits(new Rectangle(0, 0, width, height),
                    ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);

                byte* ptr1 = (byte*) bitmapData1.Scan0.ToPointer();
                byte* ptr2 = (byte*) bitmapData2.Scan0.ToPointer();
                byte* outputPtr = (byte*) outputBitmapData.Scan0.ToPointer();

                int offset1 = bitmapData1.Stride - width;
                int offset2 = bitmapData2.Stride - width;
                int outputOffset = outputBitmapData.Stride - width;

                for (int y = 0; y < height; y++, ptr1 += offset1, ptr2 += offset2, outputPtr += outputOffset)
                {
                    for (int x = 0; x < width; x++, ptr1++, ptr2++, outputPtr++)
                    {
                        outputPtr[0] = ptr1[0] < ptr2[0] ? (byte) 0 : (byte) (255 - ptr1[0] + ptr2[0]);
                    }
                }

                return outputBitmap;
            }
            catch
            {
                return outputBitmap;
            }
            finally
            {
                if (bitmapData1 != null)
                {
                    bitmap1.UnlockBits(bitmapData1);
                }

                if (bitmapData2 != null)
                {
                    bitmap2.UnlockBits(bitmapData2);
                }

                if (outputBitmap != null && outputBitmapData != null)
                {
                    outputBitmap.UnlockBits(outputBitmapData);
                }
            }
        }

        private static Bitmap GaussianBlur3x3(Bitmap inputBitmap8Bits)
        {
            float[,] gaussianKernel =
            {
                {1, 2, 1,},
                {2, 4, 2,},
                {1, 2, 1,},
            };

            ConvolutionOptions options = new ConvolutionOptions(gaussianKernel, true, 0, false, 1.0f / 16, 0);
            return ImageConvolution.Convolute(inputBitmap8Bits, options);
        }

        public static Bitmap BackgroundAdaptive(Bitmap inputBitmap, float alpha = 0.75f)
        {
            Bitmap inputBitmap8Bits = null;
            Bitmap bitmap = null;
            Bitmap backgroundEstimation = null;
            Bitmap subtracted = null;
            Bitmap outputBitmap = null;
            BitmapData bitmapData = null;
            BitmapData outputData = null;
            BitmapData subtractedData = null;

            try
            {
                inputBitmap8Bits = ColorDepthConversion.To8Bits(inputBitmap);
                bitmap = GaussianBlur3x3(inputBitmap8Bits);
                backgroundEstimation = Morphology.Erosion7x7(bitmap);
                subtracted = SubtractAndInvert(backgroundEstimation, bitmap);

                var threshold = Convert.ToByte(OpenCvOtsuThreshold(bitmap));
                var subtractedThreshold = Convert.ToByte(OpenCvOtsuThreshold(subtracted));
                int strictThreshold = (int) (threshold * alpha);

                var width = bitmap.Width;
                var height = bitmap.Height;

                outputBitmap = BitmapFactory.CreateGrayImage(width, height);

                var rectangle = new Rectangle(0, 0, width, height);

                bitmapData = bitmap.LockBits(rectangle,
                    ImageLockMode.ReadWrite,
                    PixelFormat.Format8bppIndexed);
                outputData = outputBitmap.LockBits(rectangle,
                    ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                subtractedData = subtracted.LockBits(rectangle,
                    ImageLockMode.ReadWrite,
                    PixelFormat.Format8bppIndexed);

                byte* bitmapPtr = (byte*) (void*) bitmapData.Scan0;
                byte* outputPtr = (byte*) (void*) outputData.Scan0;
                byte* subtractedPtr = (byte*) (void*) subtractedData.Scan0;

                if (bitmapPtr == null)
                {
                    return null;
                }

                if (outputPtr == null)
                {
                    return null;
                }

                if (subtractedPtr == null)
                {
                    return null;
                }

                int bitmapStride = bitmapData.Stride;
                int outputStride = bitmapData.Stride;
                int subtractedStride = subtractedData.Stride;

                int bitmapOffset = bitmapStride - width;
                int outputOffset = outputStride - width;
                int subtractedOffset = subtractedStride - width;

                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        if (subtractedPtr[0] <= subtractedThreshold && bitmapPtr[0] < threshold)
                        {
                            outputPtr[0] = 0;
                        }
                        else if (bitmapPtr[0] <= strictThreshold)
                        {
                            outputPtr[0] = 0;
                        }
                        else
                        {
                            outputPtr[0] = 255;
                        }

                        bitmapPtr++;
                        outputPtr++;
                        subtractedPtr++;
                    }

                    bitmapPtr += bitmapOffset;
                    outputPtr += outputOffset;
                    subtractedPtr += subtractedOffset;
                }

                return outputBitmap;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);

                return outputBitmap;
            }
            finally
            {
                if (bitmapData != null)
                {
                    bitmap.UnlockBits(bitmapData);
                }

                if (subtractedData != null)
                {
                    subtracted.UnlockBits(subtractedData);
                }

                if (outputBitmap != null && outputData != null)
                {
                    outputBitmap.UnlockBits(outputData);
                }

                inputBitmap8Bits?.Dispose();
                bitmap?.Dispose();
                backgroundEstimation?.Dispose();
                subtracted?.Dispose();
            }
        }
    }
}