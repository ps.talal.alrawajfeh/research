using System;
using System.Runtime.InteropServices;

namespace Progressoft.Imaging.Core
{
    internal unsafe class NativeImagingExterns
    {
        public static void BackgroundAdaptiveThreshold(void* image,
            int width,
            int height,
            int stride,
            double alpha,
            void* outputImage,
            int outputImageStride)
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                NativeImagingExternsWindows.BackgroundAdaptiveThreshold(image,
                    width,
                    height,
                    stride,
                    alpha,
                    outputImage,
                    outputImageStride);
                return;
            }

            if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                NativeImagingExternsLinux.BackgroundAdaptiveThreshold(image,
                    width,
                    height,
                    stride,
                    alpha,
                    outputImage,
                    outputImageStride);
                return;
            }

            throw new NotImplementedException("NativeImaging is not implemented for the current operating system");
        }

        public static void ResizeBiCubicInterpolation(void* image,
            int dstWidth,
            int dstHeight,
            int srcWidth,
            int srcHeight,
            int srcStride,
            int srcChannels,
            void* outputImage,
            int outputImageStride)
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                NativeImagingExternsWindows.ResizeBiCubicInterpolation(image,
                    dstWidth,
                    dstHeight,
                    srcWidth,
                    srcHeight,
                    srcStride,
                    srcChannels,
                    outputImage,
                    outputImageStride);
                return;
            }

            if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                NativeImagingExternsLinux.ResizeBiCubicInterpolation(image,
                    dstWidth,
                    dstHeight,
                    srcWidth,
                    srcHeight,
                    srcStride,
                    srcChannels,
                    outputImage,
                    outputImageStride);
                return;
            }

            throw new NotImplementedException("NativeImaging is not implemented for the current operating system");
        }
    }
}