﻿using Progressoft.Imaging.Core.Common;
using System;
using System.Drawing;
using System.Drawing.Imaging;

namespace Progressoft.Imaging.Core
{
    public struct CrossCorrellationResult
    {
        public float Value { get; }
        public Point Position { get; }

        public CrossCorrellationResult(float value, Point position)
        {
            Value = value;
            Position = position;
        }
    }

    public static unsafe class NormalizedCrossCorrellation
    {
        public static CrossCorrellationResult FastNormalizedCrossCorrellation(Bitmap bmpFxy, Rectangle rectRoI, Bitmap bmpGij)
        {
            float meanGij = Statistics.GetMean(bmpGij);
            float stdDevGij = Statistics.GetStandardDeviation(bmpGij, meanGij);
            return FastNormalizedCrossCorrellation(
                bmpFxy,
                rectRoI,
                bmpGij,
                meanGij,
                stdDevGij);
        }

        public static CrossCorrellationResult FastNormalizedCrossCorrellation(Bitmap bmpFxy, Rectangle rectRoI,
                                                                              Bitmap bmpGij, float meanGij, float stdDevGij)
        {
            if (bmpFxy.PixelFormat != PixelFormat.Format8bppIndexed ||
                bmpGij.PixelFormat != PixelFormat.Format8bppIndexed)
                return new CrossCorrellationResult(-1, new Point(-1, -1));

            // Normalized cross correlation function (NCCF) between 2 images 'F' & 'G' is given by :
            //  where:
            //          n =  count of pixels in each image
            //          Fxy, Gxy = pixel values of both images at the same position ...
            //          MF, GM = the mean of both images ...
            //          SF, SG =  the standard deviation of both images ...

            // They hold the maximum resulted value (Peak) from the NCCF and
            // at which point ...
            float matchValue = float.MinValue;
            Point matchPosition = Point.Empty;

            // get widths and heights of the given images ...
            int widthFxy = bmpFxy.Width;
            int heightFxy = bmpFxy.Height;

            int widthGij = bmpGij.Width;
            int heightGij = bmpGij.Height;

            // Get the bounding rectangles of the given images...
            Rectangle rectFxy = new Rectangle(0, 0, widthFxy, heightFxy);
            Rectangle rectGij = new Rectangle(0, 0, widthGij, heightGij);

            // the region of interest should be within the F image and of size
            // greature than or equal to the G image ...
            if (!(rectFxy.Contains(rectRoI) &&
                rectRoI.Width >= rectGij.Width &&
                rectRoI.Height >= rectGij.Height))
                return new CrossCorrellationResult(-1, new Point(-1, -1));

            // Compute number of pixels in a one window to be used in statistical
            // computing  ...
            float dPixelsCount = widthGij * heightGij;

            fixed (byte* pFxyFixed = BitmapUtils.ToByteArray1D(bmpFxy),
                            pGijFixed = BitmapUtils.ToByteArray1D(bmpGij))
            {
                // Get a pointers for each one ...
                byte* pFxy = pFxyFixed;
                byte* pGij = pGijFixed;

                // compute the upper limits for both of u and v ...
                int upperU = rectRoI.X + ((rectRoI.Width + 1) - widthGij);
                int upperV = rectRoI.Y + ((rectRoI.Height + 1) - heightGij);
                int upperX = rectRoI.X + widthGij;
                int upperY = rectRoI.Y + heightGij;
                int y_T_stride1 = 0;
                int y_T_stride2 = 0;
                int prevV = 0;
                int lastV = 0;

                int rowWindowSum = 0;
                int columnWindowSum = 0;
                float windowMean = 0f;
                double cnnSumSpace = 0f;
                float windowStdDev = 0f;
                float winStdDev_x_stdDevG = 0f;
                int iFxyIndex = 0;
                float tmpValue1 = 0;
                int start_of_stride = rectRoI.Y * widthFxy;

                y_T_stride1 = start_of_stride;
                for (int y = rectRoI.Y; y < upperY; ++y, y_T_stride1 += widthFxy)
                    for (int x = rectRoI.X; x < upperX; ++x)
                        rowWindowSum += pFxy[y_T_stride1 + x];

                // we are only interested in the region of the interest ...
                for (int u = rectRoI.X; u < upperU; u++)
                {
                    // compute the upper limits for x ...
                    upperX = u + widthGij;
                    columnWindowSum = rowWindowSum;

                    prevV = start_of_stride;
                    upperY = rectRoI.Y + heightGij;
                    lastV = upperY * widthFxy;
                    y_T_stride1 = start_of_stride + u;
                    for (int v = rectRoI.Y; v < upperV; v++, prevV += widthFxy, upperY++, lastV += widthFxy, y_T_stride1 += widthFxy)
                    {
                        // compute the upper limit for y ...
                        windowMean = columnWindowSum / dPixelsCount;

                        // compute the result of NCCF into 'cnnSumSpace' and compute the
                        // standard deviation at the same time to increase
                        // performance..
                        cnnSumSpace = 0;
                        windowStdDev = 0f;
                        byte* pGijTmp = pGij;
                        y_T_stride2 = y_T_stride1;
                        for (int y = v; y < upperY; y++, y_T_stride2 += widthFxy)
                        {
                            iFxyIndex = y_T_stride2;
                            for (int x = u; x < upperX; x++, iFxyIndex++, pGijTmp++)
                            {
                                tmpValue1 = pFxy[iFxyIndex] - windowMean;
                                cnnSumSpace += tmpValue1 * (pGijTmp[0] - meanGij);
                                windowStdDev += tmpValue1 * tmpValue1;
                            }
                        }

                        // compute STD of the current window ...
                        windowStdDev = (float)Math.Sqrt(windowStdDev / dPixelsCount);

                        // multiply both STDs ...
                        winStdDev_x_stdDevG = windowStdDev * stdDevGij;

                        // in case of 0, update it ...
                        if (winStdDev_x_stdDevG == 0)
                            winStdDev_x_stdDevG = 0.00000000000000000000000000000000000000000001f; // almost zero ...

                        // compute the cross correlation value for the current window  ...
                        cnnSumSpace /= (dPixelsCount * winStdDev_x_stdDevG);

                        // Keep the maximum peak in the NCCF space...
                        if (cnnSumSpace > matchValue)
                        {
                            matchValue = (float)cnnSumSpace;
                            matchPosition.X = u;
                            matchPosition.Y = v;
                        }

                        for (int x = u; x < upperX; ++x)
                            columnWindowSum = columnWindowSum - pFxy[prevV + x] + pFxy[lastV + x];
                    } // for v

                    upperY = rectRoI.Y + heightGij;
                    y_T_stride1 = start_of_stride + u;
                    y_T_stride2 = start_of_stride + upperX;
                    for (int y = rectRoI.Y; y < upperY; ++y, y_T_stride1 += widthFxy, y_T_stride2 += widthFxy)
                        rowWindowSum = rowWindowSum - pFxy[y_T_stride1] + pFxy[y_T_stride2];
                } // for u
            }

            return new CrossCorrellationResult(matchValue, matchPosition);
        }
    }
}