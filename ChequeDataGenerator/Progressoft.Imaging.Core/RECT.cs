﻿using System.Runtime.InteropServices;

namespace Progressoft.Imaging.Core
{
    // Define the reactangle-struct as in windef.h -- Basic Windows Type Definitions
    [StructLayout(LayoutKind.Sequential)]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Minor Code Smell", "S101:Types should be named in PascalCase", Justification = "<Pending>")]
    internal struct RECT
    {
        public int Left;
        public int Top;
        public int Right;
        public int Bottom;
    };
} // namespace PSChequeQualityControl