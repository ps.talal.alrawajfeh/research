﻿using Progressoft.Imaging.Core.Common;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;

namespace Progressoft.Imaging.Core
{
    public static unsafe class ImageChannels
    {
        private static readonly List<int> _validChannelCount = new List<int>() { 1, 3, 4 };

        public static Bitmap[] Split(Bitmap inputImage)
        {
            PixelFormat pixelFormat = inputImage.PixelFormat;
            int outChannelsCount = Math.Max(1, Image.GetPixelFormatSize(pixelFormat) / 8);
            if (pixelFormat == PixelFormat.Format1bppIndexed)
                return new Bitmap[] { ColorDepthConversion.To8Bits(inputImage) };
            else if (pixelFormat == PixelFormat.Format8bppIndexed)
                return new Bitmap[] { inputImage.Clone() as Bitmap };
            else
                return DoSplit(inputImage, pixelFormat, outChannelsCount);
        }

        public static Bitmap Join(Bitmap[] inputImages)
        {
            if (inputImages == null)
                throw new ArgumentNullException("inputImages", "Argument is set to null");

            if (inputImages.Any(bmp => bmp == null))
                throw new ArgumentNullException("inputImages", "The given input array has some null items");

            if (!_validChannelCount.Contains(inputImages.Length))
                throw new ArgumentNullException("inputImages", "Number of images should be 1,3 or 4");

            if (inputImages.Skip(1).Any(bmp => bmp.Width != inputImages[0].Width || bmp.Height != inputImages[0].Height))
                throw new ArgumentNullException("inputImages", "Images width & height must be matched");

            Bitmap[] inputImages8bits = inputImages.Select(bmp => ColorDepthConversion.To8Bits(bmp)).ToArray();

            Bitmap outputImage = (inputImages8bits.Length == 1) ? inputImages[0].Clone() as Bitmap :
                                                                DoJoin(inputImages8bits);
            for (int i = 0; i < inputImages8bits.Length; i++)
                inputImages8bits[i].Dispose();

            return outputImage;
        }

        private static Bitmap DoJoin(Bitmap[] inputImages)
        {
            int channelsCount = inputImages.Length;
            int width = inputImages[0].Width;
            int height = inputImages[0].Height;
            Rectangle rectLock = new Rectangle(0, 0, width, height);

            Bitmap outputImage = channelsCount == 3 ? BitmapFactory.Create24ColoredImage(width, height) :
                                                     BitmapFactory.Create32ColoredImage(width, height);

            BitmapData[] bmpInsData = inputImages.Select(bmp => bmp.LockBits(rectLock, ImageLockMode.ReadOnly, bmp.PixelFormat)).ToArray();
            BitmapData bmpOutData = outputImage.LockBits(rectLock, ImageLockMode.WriteOnly, outputImage.PixelFormat);

            int inOffset = bmpInsData[0].Stride - width;
            int outOffset = bmpOutData.Stride - channelsCount * width;

            byte* outPtr = (byte*)bmpOutData.Scan0.ToPointer();
            byte*[] inPtr = new byte*[channelsCount];
            for (int i = 0; i < channelsCount; i++)
                inPtr[i] = (byte*)bmpInsData[i].Scan0.ToPointer();

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    for (int p = 0; p < channelsCount; p++)
                    {
                        outPtr[0] = inPtr[p][0];
                        inPtr[p]++;
                        outPtr++;
                    }
                }
                outPtr += outOffset;
                for (int p = 0; p < channelsCount; p++)
                    inPtr[p] += inOffset;
            }

            outputImage.UnlockBits(bmpOutData);
            for (int i = 0; i < channelsCount; i++)
                inputImages[i].UnlockBits(bmpInsData[i]);

            return outputImage;
        }

        private static Bitmap[] DoSplit(Bitmap inputImage, PixelFormat pixelFormat, int outChannelsCount)
        {
            int width = inputImage.Width;
            int height = inputImage.Height;
            Rectangle rectLock = new Rectangle(0, 0, width, height);

            BitmapData bmpInData = inputImage.LockBits(rectLock, ImageLockMode.ReadOnly, pixelFormat);
            Bitmap[] outputImages = Enumerable.Range(0, outChannelsCount).Select(_ => BitmapFactory.CreateGrayImage(width, height)).ToArray();
            BitmapData[] bmpOutsData = outputImages.Select(bmp => bmp.LockBits(rectLock, ImageLockMode.WriteOnly, bmp.PixelFormat)).ToArray();

            int inOffset = bmpInData.Stride - outChannelsCount * width;
            int outOffset = bmpOutsData[0].Stride - width;

            byte* inPtr = (byte*)bmpInData.Scan0.ToPointer();
            byte*[] outPtr = new byte*[outChannelsCount];
            for (int i = 0; i < outChannelsCount; i++)
                outPtr[i] = (byte*)bmpOutsData[i].Scan0.ToPointer();

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    for (int p = 0; p < outChannelsCount; p++)
                    {
                        outPtr[p][0] = inPtr[0];
                        outPtr[p]++;
                        inPtr++;
                    }
                }
                inPtr += inOffset;
                for (int p = 0; p < outChannelsCount; p++)
                    outPtr[p] += outOffset;
            }

            inputImage.UnlockBits(bmpInData);
            for (int i = 0; i < outChannelsCount; i++)
                outputImages[i].UnlockBits(bmpOutsData[i]);

            return outputImages;
        }
    }
}