﻿using Progressoft.Imaging.Core.Common;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace Progressoft.Imaging.Core
{
    public static unsafe class ImageCropping
    {
        public static Bitmap Crop(Bitmap bmpImage, Rectangle rectToCrop, bool adjustAreaBeforCropping = true)
        {
            if (bmpImage == null)
                return null;

            Rectangle rectImage = new Rectangle(0, 0, bmpImage.Width, bmpImage.Height);
            Rectangle rectTmp = new Rectangle(rectToCrop.Location, rectToCrop.Size);

            if (!rectImage.Contains(rectTmp))
            {
                if (adjustAreaBeforCropping)
                {
                    if (rectTmp.X >= rectImage.Width)
                        rectTmp.X = rectImage.Width - rectTmp.Width;

                    if (rectTmp.X < 0)
                        rectTmp.X = 0;

                    if (rectTmp.Y >= rectImage.Height)
                        rectTmp.Y = rectImage.Height - rectTmp.Height;

                    if (rectTmp.Y < 0)
                        rectTmp.Y = 0;

                    if (rectTmp.X + rectTmp.Width > rectImage.Width)
                        rectTmp.Width = rectImage.Width - rectTmp.X;

                    if (rectTmp.Y + rectTmp.Height > rectImage.Height)
                        rectTmp.Height = rectImage.Height - rectTmp.Y;
                }
                else
                    return null;
            }

            return bmpImage.Clone(rectTmp, bmpImage.PixelFormat);
        }

        [Obsolete("Old version. Please use Crop(Bitmap bmpImage, List<Point> polygon, bool expandPolygon).")]
        public static Bitmap Crop(Bitmap bmpImage, List<Point> polygon)
        {
            Rectangle containerRect = PointsMathmatics.GetContainerRectangle(polygon);
            Bitmap tmp8bit = DrawWhiteRegion(polygon, bmpImage.Size);
            Bitmap maskedImage = GenericProcessing.Masking(bmpImage, tmp8bit, 255, 255);
            tmp8bit.Dispose();
            Bitmap finalResult = ImageCropping.Crop(maskedImage, containerRect);
            maskedImage.Dispose();
            return finalResult;
        }

        public static Bitmap Crop(Bitmap bmpImage, List<Point> polygon, bool expandPolygon)
        {
            Rectangle containerRect = PointsMathmatics.GetContainerRectangle(polygon);

            List<Point> polygonPoints = PointsMathmatics.GetAllPointsInsidePolygon(polygon, expandPolygon);

            Bitmap cropped = BitmapFactory.CreateGrayImage(containerRect.Width, containerRect.Height, 255);

            BitmapData croppedBmpData = cropped.LockBits(new Rectangle(0, 0, containerRect.Width, containerRect.Height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
            int strideCropped = croppedBmpData.Stride;
            byte* croppedPtr = (byte*)croppedBmpData.Scan0.ToPointer();

            BitmapData orgBmpData = bmpImage.LockBits(new Rectangle(0, 0, bmpImage.Width, bmpImage.Height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
            int strideOrg = orgBmpData.Stride;
            byte* orgPtr = (byte*)orgBmpData.Scan0.ToPointer();

            polygonPoints.ForEach(pnt =>
                                  croppedPtr[(pnt.Y - containerRect.Y) * strideCropped + (pnt.X - containerRect.X)] = orgPtr[pnt.Y * strideOrg + pnt.X]);

            cropped.UnlockBits(croppedBmpData);
            bmpImage.UnlockBits(orgBmpData);

            return cropped;
        }

        public static List<Bitmap> Crop(Bitmap bmpImage, List<Rectangle> rectAreas, bool adjustAreaBeforCropping = true)
        {
            List<Bitmap> croppedAreas = new List<Bitmap>();
            foreach (Rectangle rect in rectAreas)
                croppedAreas.Add(Crop(bmpImage, rect, adjustAreaBeforCropping));
            return croppedAreas;
        }

        public static Bitmap Crop(Bitmap bmpImage, int xStart, int yStart, int xEnd, int yEnd, bool adjustAreaBeforCropping = true)
        {
            return Crop(bmpImage, new Rectangle(xStart, yStart, xEnd - xStart + 1, yEnd - yStart + 1), adjustAreaBeforCropping);
        }

        public static Bitmap Crop(Bitmap bmpImage, Point topLeft, Point bottomRight, bool adjustAreaBeforCropping = true)
        {
            return Crop(bmpImage, topLeft.X, topLeft.Y, bottomRight.X, bottomRight.Y, adjustAreaBeforCropping);
        }

        private static Bitmap DrawWhiteRegion(List<Point> polygon, Size size)
        {
            GraphicsPath gp = new GraphicsPath();
            gp.AddPolygon(polygon.ToArray());
            Region regToCrop = new Region(gp);

            Bitmap tmp24 = BitmapFactory.Create24ColoredImage(size.Width, size.Height);
            Graphics g = Graphics.FromImage(tmp24);
            g.FillRegion(Brushes.White, regToCrop);
            g.Dispose();
            regToCrop.Dispose();
            gp.Dispose();
            Bitmap tmp8bit = ColorDepthConversion.To8Bits(tmp24);
            tmp24.Dispose();
            return tmp8bit;
        }
    }
}