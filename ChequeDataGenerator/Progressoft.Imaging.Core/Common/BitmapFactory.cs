﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;

namespace Progressoft.Imaging.Core.Common
{
    public static class BitmapFactory
    {
        public static Bitmap CreateGrayImage(int width, int height, byte backgroundColor = 0)
        {
            Bitmap bmp = new Bitmap(width, height, PixelFormat.Format8bppIndexed)
            {
                Palette = BitmapFactory.CreateGrayPalette()
            };

            if (backgroundColor > 0)
                UpdateBackgroundColor(ref bmp, Color.FromArgb(backgroundColor, backgroundColor, backgroundColor));

            return bmp;
        }

        public static Bitmap CreateBinaryImage(int width, int height, bool withWhiteBackground = false)
        {
            Bitmap bmp = new Bitmap(width, height, PixelFormat.Format1bppIndexed)
            {
                Palette = BitmapFactory.CreateBinaryPalette()
            };

            if (withWhiteBackground)
                UpdateBackgroundColor(ref bmp, Color.FromArgb(255, 255, 255));

            return bmp;
        }

        public static Bitmap Create24ColoredImage(int width, int height, KnownColor color = KnownColor.Black)
        {
            Bitmap bmp = new Bitmap(width, height, PixelFormat.Format24bppRgb);
            if (color != KnownColor.Black)
                UpdateBackgroundColor(ref bmp, Color.FromKnownColor(color));
            return bmp;
        }

        public static Bitmap Create32ColoredImage(int width, int height, KnownColor color = KnownColor.Black)
        {
            Bitmap bmp = new Bitmap(width, height, PixelFormat.Format32bppArgb);
            if (color != KnownColor.Black)
                UpdateBackgroundColor(ref bmp, Color.FromKnownColor(color));
            return bmp;
        }

        public static ColorPalette CreateBinaryPalette()
        {
            ColorPalette colorPalette = Create(PixelFormat.Format1bppIndexed);
            colorPalette.Entries[0] = Color.FromArgb(255, 0, 0, 0);
            colorPalette.Entries[1] = Color.FromArgb(255, 255, 255, 255);
            return colorPalette;
        }

        public static ColorPalette CreateGrayPalette()
        {
            ColorPalette colorPalette = Create(PixelFormat.Format8bppIndexed);
            for (int i = 0; i < 256; i++)
                colorPalette.Entries[i] = Color.FromArgb(255, i, i, i);
            return colorPalette;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0066:Convert switch statement to expression", Justification = "<Pending>")]
        public static Bitmap CreateImage(int width, int height, PixelFormat pixelFormat, KnownColor color = KnownColor.Black)
        {
            switch (pixelFormat)
            {
                case PixelFormat.Format8bppIndexed:
                    return CreateGrayImage(width, height, Color.FromKnownColor(color).R);

                case PixelFormat.Format24bppRgb:
                    return Create24ColoredImage(width, height, color);

                case PixelFormat.Format32bppArgb:
                    return Create32ColoredImage(width, height, color);

                case PixelFormat.Format32bppRgb:
                    return Create32ColoredImage(width, height, color);

                case PixelFormat.Format1bppIndexed:
                    return CreateBinaryImage(width, height, Color.FromKnownColor(color).R == 255);

                default:
                    return null;
            }
        }

        public static unsafe void UpdateBackgroundColor(ref Bitmap inputBitmap, Color color)
        {
            if (Image.GetPixelFormatSize(inputBitmap.PixelFormat) >= 24)
            {
                Graphics g = Graphics.FromImage(inputBitmap);
                g.Clear(color);
                g.Dispose();
            }
            else
            {
                int width = inputBitmap.Width;
                int height = inputBitmap.Height;
                BitmapData outBmpData = inputBitmap.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, inputBitmap.PixelFormat);
                byte* pStride = (byte*)outBmpData.Scan0.ToPointer();
                int stride = outBmpData.Stride;
                byte[] strideLine = Enumerable.Range(0, stride).Select(_ => color.R).ToArray();
                for (int y = 0; y < height; y++, pStride += stride)
                    Marshal.Copy(strideLine, 0, new IntPtr(pStride), stride);
                inputBitmap.UnlockBits(outBmpData);
            }
        }

        private static ColorPalette Create(PixelFormat pixelFormat)
        {
            Bitmap bitmap = new Bitmap(1, 1, pixelFormat);
            ColorPalette palette = bitmap.Palette;
            bitmap.Dispose();
            return palette;
        }
    }
}