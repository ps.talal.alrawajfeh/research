﻿namespace Progressoft.Imaging.Core.Common
{
    internal static class Constants
    {
        internal const int MAX_EXPECTED_IMAGE_LENGTH = 10000000;
    }
}