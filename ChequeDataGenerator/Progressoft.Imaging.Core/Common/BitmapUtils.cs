﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;

namespace Progressoft.Imaging.Core.Common
{
    public static unsafe class BitmapUtils
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Blocker Code Smell", "S2368:Public methods should not have multidimensional array parameters", Justification = "<Pending>")]
        public static Bitmap ToBitmap(int[,] DTab)
        {
            Bitmap newBmp = BitmapFactory.CreateGrayImage(DTab.GetLength(1), DTab.GetLength(0));
            int w = newBmp.Width;
            int h = newBmp.Height;

            BitmapData bmData = newBmp.LockBits(new Rectangle(0, 0, newBmp.Width, newBmp.Height),
                ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
            int stride = bmData.Stride;
            System.IntPtr Scan0 = bmData.Scan0;

            byte* p = (byte*)(void*)Scan0;

            int nOffset = stride - w;

            for (int y = 0; y < h; y++)
            {
                for (int x = 0; x < w; x++)
                {
                    p[0] = (byte)DTab[y, x];
                    p++;
                }
                p += nOffset;
            }
            newBmp.UnlockBits(bmData);
            return newBmp;
        }

        public static int[,] ToIntArray2D(Bitmap inputImage)
        {
            if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed)
                return null;

            BitmapData bmData = inputImage.LockBits(new Rectangle(0, 0, inputImage.Width, inputImage.Height),
                ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
            int w = inputImage.Width;
            int h = inputImage.Height;
            int stride = bmData.Stride;
            System.IntPtr Scan0 = bmData.Scan0;
            int[,] array = new int[h, w];

            byte* p = (byte*)(void*)Scan0;

            int nOffset = stride - w;
            for (int y = 0; y < h; y++)
            {
                for (int x = 0; x < w; x++)
                {
                    array[y, x] = (int)p[0];
                    p++;
                } //for x
                p += nOffset;
            } //for y
            inputImage.UnlockBits(bmData);
            return array;
        }

        public static byte[] ToByteArray1D(Bitmap bmp)
        {
            int width = bmp.Width;
            int height = bmp.Height;

            BitmapData bmpData = bmp.LockBits(new System.Drawing.Rectangle(0, 0, width, height), ImageLockMode.ReadOnly, bmp.PixelFormat);
            byte* p = (byte*)bmpData.Scan0.ToPointer();
            int stride = bmpData.Stride;
            int bytesPerRow = (bmp.PixelFormat != PixelFormat.Format1bppIndexed) ?
                        (System.Drawing.Image.GetPixelFormatSize(bmp.PixelFormat) / 8) * width :
                        (int)Math.Ceiling(width / 8f);
            byte[] bytes = new byte[bytesPerRow * height];
            int index = 0;
            fixed (byte* b = bytes)
            {
                for (int y = 0; y < height; y++, p += stride, index += bytesPerRow)
                    Marshal.Copy(new IntPtr(p), bytes, index, bytesPerRow);
            }

            bmp.UnlockBits(bmpData);
            return bytes;
        }

        public static byte[] ConvertImageToByteBuffer(Image image, ImageFormat format)
        {
            MemoryStream ms = new MemoryStream();
            image.Save(ms, format);
            byte[] binaryBuffer = ms.ToArray();
            ms.Close();
            ms.Dispose();
            return binaryBuffer;
        }

        public static Bitmap ConvertByteBufferToBitmap(byte[] bytes)
        {
            return (Bitmap)TypeDescriptor.GetConverter(typeof(Bitmap))
                                         .ConvertFrom(bytes);
            //Stream ms = new MemoryStream(bytes);
            //Bitmap outputImage1 = Image.FromStream(ms) as Bitmap;
            //Bitmap outputImage2 = DeepCopy(outputImage1);
            //ms.Close();
            //ms.Dispose();
            //outputImage1.Dispose();
            //return outputImage2;
        }

        public static Bitmap DeepCopy(Bitmap inputImage)
        {
            if (inputImage == null)
                return null;

            PixelFormat pixelFormat = inputImage.PixelFormat;
            int width = inputImage.Width;
            int height = inputImage.Height;
            Rectangle rectLock = new Rectangle(0, 0, width, height);

            Bitmap outputImage = BitmapFactory.CreateImage(width, height, pixelFormat);
            BitmapData bmpDataIn = inputImage.LockBits(rectLock, ImageLockMode.ReadWrite, pixelFormat);
            BitmapData bmpDataOut = outputImage.LockBits(rectLock, ImageLockMode.ReadWrite, pixelFormat);

            int strideIn = bmpDataIn.Stride;
            byte* pIn = (byte*)bmpDataIn.Scan0.ToPointer();
            byte* pOut = (byte*)bmpDataOut.Scan0.ToPointer();
            for (int y = 0; y < height; y++, pIn += strideIn, pOut += strideIn)
                Buffer.MemoryCopy(pIn, pOut, strideIn, strideIn);

            inputImage.UnlockBits(bmpDataIn);
            outputImage.UnlockBits(bmpDataOut);

            return outputImage;
        }

        public static string ConvertBitmapToBase64Image(Bitmap signatureCardImage, ImageFormat format)
        {
            Bitmap signatureCardImage8Bit = ColorDepthConversion.To8Bits(signatureCardImage);
            byte[] imageBuffer = BitmapUtils.ConvertImageToByteBuffer(signatureCardImage8Bit, format);
            signatureCardImage8Bit?.Dispose();
            return Convert.ToBase64String(imageBuffer);
        }

        public static Bitmap ConvertBase64ImageToBitmap(string base64Image)
        {
            byte[] imageBuffer = Convert.FromBase64String(base64Image);
            return BitmapUtils.ConvertByteBufferToBitmap(imageBuffer);
        }
    }
}