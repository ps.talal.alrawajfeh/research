﻿using System;

namespace Progressoft.Imaging.Core
{
    /// <summary>
    /// A structure to hold the minimum and the maximum values.
    /// </summary>
    public class ValueRange<T> where T : System.IComparable
    {
        /// <summary>
        /// Holds the minimumvalue.
        /// </summary>
        public T Minimum { get; }

        /// <summary>
        /// Holds the maximum value.
        /// </summary>
        public T Maximum { get; }

        public ValueRange(T minimum, T maximum)
        {
            if (minimum.CompareTo(maximum) > 0)
                throw new ArgumentException("Minimum value must be less than the maximum one");
            Minimum = minimum;
            Maximum = maximum;
        }
    }
}