using System;
using System.Drawing;
using System.Drawing.Imaging;
using Progressoft.Imaging.Core.Common;

namespace Progressoft.Imaging.Core
{
    public unsafe class NativeImagingWrapper
    {
        public static Bitmap BackgroundAdaptiveThreshold(Bitmap inputImage, double alpha = 0.75)
        {
            Bitmap outputImage = null;
            BitmapData outputData = null;
            BitmapData inputData = null;

            try
            {
                if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed) return null;

                int width = inputImage.Width;
                int height = inputImage.Height;

                outputImage = BitmapFactory.CreateGrayImage(width, height);
                outputData = outputImage.LockBits(new Rectangle(0, 0, outputImage.Width, outputImage.Height),
                    ImageLockMode.ReadWrite,
                    outputImage.PixelFormat);
                inputData = inputImage.LockBits(new Rectangle(0, 0, inputImage.Width, inputImage.Height),
                    ImageLockMode.ReadWrite,
                    inputImage.PixelFormat);

                int inputStride = inputData.Stride;
                int outputStride = outputData.Stride;

                void* outputPtr = (void*) outputData.Scan0;
                void* inputPtr = (void*) inputData.Scan0;

                NativeImagingExterns.BackgroundAdaptiveThreshold(inputPtr,
                    width,
                    height,
                    inputStride,
                    alpha,
                    outputPtr,
                    outputStride);

                return outputImage;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);

                return outputImage;
            }
            finally
            {
                if (inputData != null)
                {
                    inputImage.UnlockBits(inputData);
                }

                if (outputImage != null && outputData != null)
                {
                    outputImage.UnlockBits(outputData);
                }
            }
        }

        public static Bitmap ResizeBiCubicInterpolation(Bitmap inputImage, int dstWidth, int dstHeight)
        {
            Bitmap outputImage = null;
            BitmapData outputData = null;
            BitmapData inputData = null;

            try
            {
                int srcWidth = inputImage.Width;
                int srcHeight = inputImage.Height;

                outputImage = BitmapFactory.CreateImage(dstWidth, dstHeight, inputImage.PixelFormat);

                inputData = inputImage.LockBits(new Rectangle(0, 0, srcWidth, srcHeight),
                    ImageLockMode.ReadWrite,
                    inputImage.PixelFormat);
                outputData = outputImage.LockBits(new Rectangle(0, 0, dstWidth, dstHeight),
                    ImageLockMode.ReadWrite,
                    outputImage.PixelFormat);

                int srcStride = inputData.Stride;
                int dstStride = outputData.Stride;
                int channels = Math.Max(1, Image.GetPixelFormatSize(inputData.PixelFormat) / 8);

                void* outputPtr = (void*) outputData.Scan0;
                void* inputPtr = (void*) inputData.Scan0;

                NativeImagingExterns.ResizeBiCubicInterpolation(inputPtr,
                    dstWidth,
                    dstHeight,
                    srcWidth,
                    srcHeight,
                    srcStride,
                    channels,
                    outputPtr,
                    dstStride);

                return outputImage;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);

                return outputImage;
            }
            finally
            {
                if (inputData != null)
                {
                    inputImage.UnlockBits(inputData);
                }

                if (outputImage != null && outputData != null)
                {
                    outputImage.UnlockBits(outputData);
                }
            }
        }
    }
}