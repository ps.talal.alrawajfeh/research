using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;

namespace Progressoft.Imaging.Core
{
    public sealed class ConnectedComponent : IDisposable
    {
        private Point _centerOfGravity;
        private Bitmap _image;
        private Dictionary<string, object> _flags { get; }

        public Rectangle Rectangle { get; }
        public List<Point> Points { get; }

        public Point CenterOfGravity
        {
            get
            {
                if (_centerOfGravity.IsEmpty)
                    _centerOfGravity = PointsMathmatics.GetCenterOfGraphity(Points);
                return _centerOfGravity;
            }
        }

        public Bitmap Image
        {
            get
            {
                return _image ??= PointsMathmatics.PointsToBitmap(Points, 0, 0);
            }
        }

        public ConnectedComponent(List<Point> points)
        {
            Rectangle = PointsMathmatics.GetContainerRectangle(points);
            Points = points;
            _flags = new Dictionary<string, object>();
        }

        public void AddFlag(string flagName, object flagValue)
        {
            _flags.Add(flagName, flagValue);
        }

        public T GetFlag<T>(string flagName)
        {
            return (T)_flags[flagName];
        }

        public void RemoveFlag(string flagName)
        {
            _flags.Remove(flagName);
        }

        public void Dispose()
        {
            _image?.Dispose();
        }
    }

    public static unsafe class ConnectedComponentAnalysis
    {
        public static List<ConnectedComponent> GetComponents(Bitmap bmpImage, Rectangle rectAOI, byte grayColor)
        {
            List<ConnectedComponent> allComponents = new List<ConnectedComponent>();

            try
            {
                int width = bmpImage.Width;
                int height = bmpImage.Height;

                BitmapData bmpData = bmpImage.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                System.IntPtr scan0 = bmpData.Scan0;
                int stride = bmpData.Stride;

                //0: not market,1: not marked but on the stack,2: marked.
                int[,] status = new int[height, width];
                // to trace connected components(simulates recursion):
                Stack s = new Stack();
                //holds detected components:
                // temp. variables to hold  a point:
                int xp;
                int yp;
                // temp. variable:
                int j_T_s, y_T_s;

                // find all connected components:
                byte* p = (byte*)(void*)scan0;

                int iTopY = rectAOI.Y + rectAOI.Height;
                int iTopX = rectAOI.X + rectAOI.Width;
                for (int y = rectAOI.Y; y < iTopY; y++)
                {
                    y_T_s = y * stride;

                    for (int x = rectAOI.X; x < iTopX; x++)
                    {
                        if (p[y_T_s + x] == grayColor && // if the forground pixel not visited:
                            status[y, x] == 0)   // not visited up to now:
                        {
                            // a new component found:
                            List<Point> newComponent = new List<Point>
                            {
                                // add the current unvisited pixel to
                                // to the component to which belong:
                                new Point(x, y)
                            };
                            status[y, x] = 2; //  mark it as visited:

                            // Push the forground-neighboors pixels to
                            // the stack to be traced:
                            for (int j = y - 1; j <= y + 1; j++)
                            {
                                if (j < 0 || j >= height) continue;
                                j_T_s = j * stride;
                                for (int i = x - 1; i <= x + 1; i++)
                                {
                                    if (i < 0 || i >= width) continue;
                                    if (p[j_T_s + i] == grayColor && status[j, i] == 0)
                                    {
                                        // Push and mark it as found in the stack
                                        s.Push(new Point(i, j));
                                        status[j, i] = 1;
                                    }
                                }// for i.
                            }// for j.

                            // trace all pixels pushed on the stack:
                            while (s.Count != 0)
                            {
                                // pop the top pixel from the stack:
                                Point pnt = (Point)s.Pop();
                                xp = pnt.X;
                                yp = pnt.Y;

                                // add it to the component to which it belong:
                                newComponent.Add(pnt);
                                // and mark it as visited:
                                status[yp, xp] = 2;

                                // Push the forground-neighboors pixels to
                                // the stack to be traced:
                                for (int j = yp - 1; j <= yp + 1; j++)
                                {
                                    if (j < 0 || j > height - 1) continue;
                                    j_T_s = j * stride;
                                    for (int i = xp - 1; i <= xp + 1; i++)
                                    {
                                        if (i < 0 || i > width - 1) continue;
                                        // Push and mark it as found in the stack
                                        if (p[j_T_s + i] == grayColor && status[j, i] == 0)
                                        {
                                            s.Push(new Point(i, j));
                                            status[j, i] = 1;
                                        }
                                    }// for i.
                                } // for j.
                            }// while.

                            // add the cuurent found component:
                            allComponents.Add(new ConnectedComponent(newComponent));
                        }// if.
                    }
                }

                bmpImage.UnlockBits(bmpData);
                return allComponents;
            }
            catch
            {
                allComponents.Clear();
                return allComponents;
            }
        }

        public static List<ConnectedComponent> GetComponents(Bitmap bmpImage, byte grayColor)
        {
            return bmpImage != null ?
                GetComponents(bmpImage, new Rectangle(0, 0, bmpImage.Width, bmpImage.Height), grayColor) :
                new List<ConnectedComponent>();
        }

        public static ConnectedComponent GetComponentAtPoint(Bitmap bmpImage, int grayColor, Point pntStart)
        {
            try
            {
                int width = bmpImage.Width;
                int height = bmpImage.Height;

                BitmapData bmpData = bmpImage.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
                System.IntPtr scan0 = bmpData.Scan0;
                int stride = bmpData.Stride;

                //0: not market,1: not marked but on the stack,2: marked.
                int[,] status = new int[height, width];

                // to trace connected components(simulates recursion):
                Stack stack = new Stack();

                // temp. variables to hold  a point:
                int xp;
                int yp;
                // temp. variable:
                int j_T_s, y_T_s;

                // find all connected components:
                byte* p = (byte*)(void*)scan0;
                int y = pntStart.Y;
                y_T_s = y * stride;
                int x = pntStart.X;

                // a new component found:
                List<Point> newComponent = new List<Point>();

                if (p[y_T_s + x] == grayColor) // surly not visited up to now:
                {
                    stack.Push(pntStart);

                    // trace all pixels pushed on the stack:
                    while (stack.Count != 0)
                    {
                        // pop the top pixel from the stack:
                        Point pnt = (Point)stack.Pop();
                        xp = pnt.X;
                        yp = pnt.Y;

                        // add it to the component to which it belong:
                        newComponent.Add(pnt);

                        // and mark it as visited:
                        status[yp, xp] = 2;

                        // Push the forground-neighboors pixels to
                        // the stack to be traced:
                        for (int j = yp - 1; j <= yp + 1; j++)
                        {
                            if (j < 0 || j > height - 1) continue;
                            j_T_s = j * stride;

                            for (int i = xp - 1; i <= xp + 1; i++)
                            {
                                if (i < 0 || i > width - 1) continue;
                                if (p[j_T_s + i] == grayColor && status[j, i] == 0)
                                {
                                    stack.Push(new Point(i, j));
                                    status[j, i] = 1;
                                }
                            }
                        }
                    }
                }

                bmpImage.UnlockBits(bmpData);
                return new ConnectedComponent(newComponent);
            }
            catch
            {
                return null;
            }
        }
    }
}