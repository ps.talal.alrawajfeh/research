﻿using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Text;
using Progressoft.Imaging.Core;
using Progressoft.Imaging.Core.Common;

namespace ChequeDataGenerator;

public static class RandomUtils
{
    public static void Shuffle<T>(Random random, List<T> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            int k = random.Next(n--);
            (list[n], list[k]) = (list[k], list[n]);
        }
    }

    public static double NextGaussianRandomNumber(Random random,
        double mu = 0.0,
        double sigma = 1.0)
    {
        double u1 = 1.0 - random.NextDouble();
        double u2 = 1.0 - random.NextDouble();
        double randomStandardNormal = Math.Sqrt(-2.0 * Math.Log(u1)) * Math.Sin(2.0 * Math.PI * u2);
        return mu + sigma * randomStandardNormal;
    }

    public static bool NextBool(Random random)
    {
        return random.Next(0, 10) <= 4;
    }

    public static double NextDouble(Random random, double minimum, double maximum)
    {
        return random.NextDouble() * (maximum - minimum) + minimum;
    }

    public static float NextFloat(Random random, float minimum, float maximum)
    {
        return Convert.ToSingle(NextDouble(random, minimum, maximum));
    }
}

public class MinRecurrenceSampler<T>
{
    private readonly List<T> _items;
    private readonly Random _random = new Random(666);
    private List<int> _indices;


    public MinRecurrenceSampler(List<T> items)
    {
        _items = new List<T>(items);
        _indices = Enumerable.Range(0, items.Count).ToList();
        RandomUtils.Shuffle(_random, _indices);
    }

    public T Next()
    {
        var count = _indices.Count;
        if (count == 0)
        {
            count = _items.Count;
            _indices = Enumerable.Range(0, count).ToList();
            RandomUtils.Shuffle(_random, _indices);
        }

        var i = _random.Next(0, count);
        var index = _indices[i];
        _indices.RemoveAt(i);

        return _items[index];
    }
}

public static class Program
{
    private static readonly Random Random = new(666);

    public static Bitmap GetPrintedText(string text,
        string fontPath,
        float fontSize)
    {
        var dummyBitmap = new Bitmap(1, 1);
        float textWidth;
        float textHeight;

        PrivateFontCollection privateFontCollection = new PrivateFontCollection();
        privateFontCollection.AddFontFile(fontPath);
        var font = new Font(privateFontCollection.Families[0], fontSize, FontStyle.Regular);

        using (var graphics = Graphics.FromImage(dummyBitmap))
        {
            var size = graphics.MeasureString(text, font);
            textWidth = size.Width;
            textHeight = size.Height;
        }

        var bitmap = new Bitmap(Convert.ToInt32(Math.Ceiling(textWidth)) + 1,
            Convert.ToInt32(Math.Ceiling(textHeight)) + 1);

        using (var graphics = Graphics.FromImage(bitmap))
        {
            graphics.Clear(Color.White);
            graphics.DrawString(text,
                font,
                new SolidBrush(Color.Black),
                new PointF(0, 0));
        }

        return bitmap;
    }

    public static unsafe Bitmap BlendForegroundOnBackground(Bitmap foreground,
        Bitmap background,
        Point position,
        float alpha = 0.25f,
        float beta = 0.9f)
    {
        Bitmap invertedForeground = null;
        BitmapData invertedForegroundData = null;
        Bitmap mappedForeground = null;
        BitmapData mappedForegroundData = null;
        Bitmap invertedBackground = null;
        BitmapData invertedBackgroundData = null;
        Bitmap result = null;
        BitmapData resultData = null;

        try
        {
            var foregroundWidth = foreground.Width;
            var foregroundHeight = foreground.Height;

            var backgroundImageCropped = ImageCropping.Crop(background,
                new Rectangle(position.X, position.Y,
                    position.X + foregroundWidth, position.Y + foregroundHeight));

            invertedForeground = GenericProcessing.InvertColor(foreground);
            invertedBackground = GenericProcessing.InvertColor(backgroundImageCropped);

            var backgroundMean = Statistics.GetMean(invertedBackground);
            if (!IsValueValidPixel(backgroundMean))
            {
                backgroundMean = 0;
            }

            var backgroundStd = Statistics.GetStandardDeviation(invertedBackground, backgroundMean);
            if (!IsValueValidPixel(backgroundStd))
            {
                backgroundStd = 0;
            }

            var invertedForegroundThreshold = Binarization.OpenCvOtsuThresholdNonZero(invertedForeground);

            float lightValuesMean = 0f;
            int lightValuesCount = 0;
            int lightValuesMin = 255;

            var foregroundPixelFormat = invertedForeground.PixelFormat;
            var foregroundRectangle = new Rectangle(0, 0, foregroundWidth, foregroundHeight);
            invertedForegroundData = invertedForeground.LockBits(foregroundRectangle,
                ImageLockMode.ReadWrite,
                foregroundPixelFormat);

            byte* foregroundDataPtr = (byte*) (void*) invertedForegroundData.Scan0;
            int foregroundDataStride = invertedForegroundData.Stride;
            int foregroundDataOffset = foregroundDataStride - foregroundWidth;

            int foregroundColor = 0;

            for (int y = 0; y < foregroundHeight; y++)
            {
                for (int x = 0; x < foregroundWidth; x++)
                {
                    int value = *foregroundDataPtr;
                    if (value > 0 && value <= invertedForegroundThreshold)
                    {
                        lightValuesMean += value;
                        lightValuesMin = Math.Min(lightValuesMin, value);
                        lightValuesCount++;
                    }

                    if (value > 0)
                    {
                        foregroundColor = Math.Max(foregroundColor, value);
                    }

                    foregroundDataPtr++;
                }

                foregroundDataPtr += foregroundDataOffset;
            }

            lightValuesMean /= lightValuesCount;

            byte* mappedForegroundDataPtr;
            int mappedForegroundDataStride;
            int mappedForegroundDataOffset;

            if (lightValuesCount > 0)
            {
                int minMapped = Convert.ToInt32(Math.Max(Math.Min(backgroundMean - alpha * backgroundStd, 255), 0));
                int maxMapped = Convert.ToInt32(Math.Max(Math.Min(backgroundMean + alpha * backgroundStd, 255), 0));

                foregroundDataPtr = (byte*) (void*) invertedForegroundData.Scan0;
                mappedForeground = BitmapFactory.CreateImage(foregroundWidth, foregroundHeight, foregroundPixelFormat);
                mappedForegroundData = mappedForeground.LockBits(foregroundRectangle,
                    ImageLockMode.ReadWrite,
                    foregroundPixelFormat);

                mappedForegroundDataPtr = (byte*) (void*) mappedForegroundData.Scan0;
                mappedForegroundDataStride = mappedForegroundData.Stride;
                mappedForegroundDataOffset = mappedForegroundDataStride - foregroundWidth;

                float slope1 = (backgroundMean - minMapped) / (lightValuesMean - lightValuesMin);
                float slope2 = (maxMapped - backgroundMean) / (invertedForegroundThreshold - lightValuesMean);
                float slope3 = ((float) (foregroundColor - maxMapped)) / ((float) (255 - invertedForegroundThreshold));

                for (int y = 0; y < foregroundHeight; y++)
                {
                    for (int x = 0; x < foregroundWidth; x++)
                    {
                        int value = *foregroundDataPtr;

                        if (value > 0)
                        {
                            float newValue;
                            if (value >= lightValuesMin && value <= lightValuesMean)
                            {
                                newValue = Math.Max(Math.Min(slope1 * (value - lightValuesMin) + minMapped, 255), 0);
                            }
                            else if (value >= lightValuesMean && value <= invertedForegroundThreshold)
                            {
                                newValue = Math.Max(Math.Min(slope2 * (value - lightValuesMean) + backgroundMean, 255),
                                    0);
                            }
                            else
                            {
                                newValue = Math.Max(
                                    Math.Min(slope3 * (value - invertedForegroundThreshold) + maxMapped, 255), 0);
                            }

                            if (float.IsNaN(newValue))
                            {
                                *mappedForegroundDataPtr = *foregroundDataPtr;
                            }
                            else
                            {
                                *mappedForegroundDataPtr = Convert.ToByte(newValue);
                            }
                        }
                        else
                        {
                            *mappedForegroundDataPtr = 0;
                        }

                        foregroundDataPtr++;
                        mappedForegroundDataPtr++;
                    }

                    foregroundDataPtr += foregroundDataOffset;
                    mappedForegroundDataPtr += mappedForegroundDataOffset;
                }
            }
            else
            {
                mappedForeground = (Bitmap) invertedForeground.Clone();
                mappedForegroundData = mappedForeground.LockBits(foregroundRectangle,
                    ImageLockMode.ReadWrite,
                    foregroundPixelFormat);
                mappedForegroundDataStride = mappedForegroundData.Stride;
                mappedForegroundDataOffset = mappedForegroundDataStride - foregroundWidth;
            }

            mappedForegroundDataPtr = (byte*) (void*) mappedForegroundData.Scan0;

            invertedBackgroundData = invertedBackground.LockBits(
                foregroundRectangle, ImageLockMode.ReadWrite,
                invertedBackground.PixelFormat);

            byte* backgroundDataPtr = (byte*) (void*) invertedBackgroundData.Scan0;
            int backgroundDataStride = invertedBackgroundData.Stride;
            int backgroundDataOffset = backgroundDataStride - foregroundWidth;

            result = (Bitmap) background.Clone();
            resultData = result.LockBits(new Rectangle(0, 0, result.Width, result.Height),
                ImageLockMode.ReadWrite,
                result.PixelFormat);

            byte* resultDataPtr = (byte*) (void*) resultData.Scan0;
            int resultDataStride = resultData.Stride;

            for (int y = 0; y < foregroundHeight; y++)
            {
                for (int x = 0; x < foregroundWidth; x++)
                {
                    int foregroundValue = *mappedForegroundDataPtr;
                    if (foregroundValue > 0)
                    {
                        int backgroundValue = *backgroundDataPtr;
                        float weightedSum = beta * foregroundValue + (1.0f - beta) * backgroundValue;
                        weightedSum = Math.Max(Math.Min(255 - weightedSum, 255), 0);
                        *(resultDataPtr + (position.Y + y) * resultDataStride + (position.X + x)) =
                            Convert.ToByte(weightedSum);
                    }

                    mappedForegroundDataPtr++;
                    backgroundDataPtr++;
                }

                mappedForegroundDataPtr += mappedForegroundDataOffset;
                backgroundDataPtr += backgroundDataOffset;
            }

            return result;
        }
        catch (Exception exception)
        {
            Console.WriteLine(exception.Message);
            Console.WriteLine(exception.StackTrace);
            return result;
        }
        finally
        {
            if (invertedForeground != null && invertedForegroundData != null)
            {
                invertedForeground.UnlockBits(invertedForegroundData);
            }

            if (mappedForeground != null && mappedForegroundData != null)
            {
                mappedForeground.UnlockBits(mappedForegroundData);
            }

            if (invertedBackground != null && invertedBackgroundData != null)
            {
                invertedBackground.UnlockBits(invertedBackgroundData);
            }

            if (result != null && resultData != null)
            {
                result.UnlockBits(resultData);
            }

            if (invertedForeground != null)
            {
                invertedForeground.Dispose();
            }

            if (mappedForeground != null)
            {
                mappedForeground.Dispose();
            }

            if (invertedBackground != null)
            {
                invertedBackground.Dispose();
            }
        }
    }

    public static unsafe Bitmap ChangeBrightness(Bitmap bitmap, int amount)
    {
        BitmapData bitmapData = null;
        Bitmap result = null;
        BitmapData resultData = null;

        try
        {
            var bitmapWidth = bitmap.Width;
            var bitmapHeight = bitmap.Height;
            var bitmapPixelFormat = bitmap.PixelFormat;

            result = BitmapFactory.CreateImage(bitmapWidth, bitmapHeight, bitmapPixelFormat);
            var rectangle = new Rectangle(0, 0, bitmapWidth, bitmapHeight);
            bitmapData = bitmap.LockBits(rectangle, ImageLockMode.ReadWrite, bitmapPixelFormat);
            resultData = result.LockBits(rectangle, ImageLockMode.ReadWrite, bitmapPixelFormat);

            int bitmapDataOffset = bitmapData.Stride - bitmapWidth;
            int resultDataOffset = resultData.Stride - bitmapWidth;

            byte* bitmapDataPtr = (byte*) (void*) bitmapData.Scan0;
            byte* resultDataPtr = (byte*) (void*) resultData.Scan0;

            for (int y = 0; y < bitmapHeight; y++)
            {
                for (int x = 0; x < bitmapWidth; x++)
                {
                    *resultDataPtr = Convert.ToByte(Math.Max(Math.Min(*bitmapDataPtr + amount, 255), 0));
                    bitmapDataPtr++;
                    resultDataPtr++;
                }

                bitmapDataPtr += bitmapDataOffset;
                resultDataPtr += resultDataOffset;
            }

            return result;
        }
        catch
        {
            return result;
        }
        finally
        {
            if (bitmapData != null)
            {
                bitmap.UnlockBits(bitmapData);
            }

            if (result != null && resultData != null)
            {
                result.UnlockBits(resultData);
            }
        }
    }

    public static unsafe Bitmap ChangeContrast(Bitmap bitmap, float factor)
    {
        BitmapData bitmapData = null;
        Bitmap result = null;
        BitmapData resultData = null;

        try
        {
            var bitmapWidth = bitmap.Width;
            var bitmapHeight = bitmap.Height;
            var bitmapPixelFormat = bitmap.PixelFormat;

            result = BitmapFactory.CreateImage(bitmapWidth, bitmapHeight, bitmapPixelFormat);
            var rectangle = new Rectangle(0, 0, bitmapWidth, bitmapHeight);
            bitmapData = bitmap.LockBits(rectangle, ImageLockMode.ReadWrite, bitmapPixelFormat);
            resultData = result.LockBits(rectangle, ImageLockMode.ReadWrite, bitmapPixelFormat);

            int bitmapDataOffset = bitmapData.Stride - bitmapWidth;
            int resultDataOffset = resultData.Stride - bitmapWidth;

            byte* bitmapDataPtr = (byte*) (void*) bitmapData.Scan0;
            byte* resultDataPtr = (byte*) (void*) resultData.Scan0;

            for (int y = 0; y < bitmapHeight; y++)
            {
                for (int x = 0; x < bitmapWidth; x++)
                {
                    *resultDataPtr = Convert.ToByte(Math.Max(Math.Min(*bitmapDataPtr * factor, 255), 0));
                    bitmapDataPtr++;
                    resultDataPtr++;
                }

                bitmapDataPtr += bitmapDataOffset;
                resultDataPtr += resultDataOffset;
            }

            return result;
        }
        catch
        {
            return result;
        }
        finally
        {
            if (bitmapData != null)
            {
                bitmap.UnlockBits(bitmapData);
            }

            if (result != null && resultData != null)
            {
                result.UnlockBits(resultData);
            }
        }
    }

    public static unsafe Bitmap LimitColorRange(Bitmap bitmap, byte minColor, byte maxColor)
    {
        BitmapData bitmapData = null;
        Bitmap result = null;
        BitmapData resultData = null;

        try
        {
            var bitmapWidth = bitmap.Width;
            var bitmapHeight = bitmap.Height;
            var bitmapPixelFormat = bitmap.PixelFormat;

            result = BitmapFactory.CreateImage(bitmapWidth, bitmapHeight, bitmapPixelFormat);
            var rectangle = new Rectangle(0, 0, bitmapWidth, bitmapHeight);
            bitmapData = bitmap.LockBits(rectangle, ImageLockMode.ReadWrite, bitmapPixelFormat);
            resultData = result.LockBits(rectangle, ImageLockMode.ReadWrite, bitmapPixelFormat);

            int bitmapDataOffset = bitmapData.Stride - bitmapWidth;
            int resultDataOffset = resultData.Stride - bitmapWidth;

            byte* bitmapDataPtr = (byte*) (void*) bitmapData.Scan0;
            byte* resultDataPtr = (byte*) (void*) resultData.Scan0;

            for (int y = 0; y < bitmapHeight; y++)
            {
                for (int x = 0; x < bitmapWidth; x++)
                {
                    *resultDataPtr = Math.Max(Math.Min(*bitmapDataPtr, maxColor), minColor);
                    bitmapDataPtr++;
                    resultDataPtr++;
                }

                bitmapDataPtr += bitmapDataOffset;
                resultDataPtr += resultDataOffset;
            }

            return result;
        }
        catch
        {
            return result;
        }
        finally
        {
            if (bitmapData != null)
            {
                bitmap.UnlockBits(bitmapData);
            }

            if (result != null && resultData != null)
            {
                result.UnlockBits(resultData);
            }
        }
    }

    public static unsafe Bitmap ApplyGradient(Bitmap bitmap, int startColor, int endColor, bool horizontal = false)
    {
        Bitmap result = null;
        BitmapData resultData = null;
        BitmapData bitmapData = null;

        try
        {
            int length = horizontal ? bitmap.Width : bitmap.Height;
            float colorDelta = (Convert.ToSingle(endColor) - Convert.ToSingle(startColor)) / length;
            float currentColor = startColor;

            var bitmapWidth = bitmap.Width;
            var bitmapHeight = bitmap.Height;
            var bitmapPixelFormat = bitmap.PixelFormat;

            result = BitmapFactory.CreateImage(bitmapWidth, bitmapHeight, bitmapPixelFormat);
            var rectangle = new Rectangle(0, 0, bitmapWidth, bitmapHeight);
            bitmapData = bitmap.LockBits(rectangle, ImageLockMode.ReadWrite, bitmapPixelFormat);
            resultData = result.LockBits(rectangle, ImageLockMode.ReadWrite, bitmapPixelFormat);


            var bitmapDataStride = bitmapData.Stride;
            var resultDataStride = resultData.Stride;

            int bitmapDataOffset = bitmapDataStride - bitmapWidth;
            int resultDataOffset = resultDataStride - bitmapWidth;

            byte* bitmapDataPtr = (byte*) (void*) bitmapData.Scan0;
            byte* resultDataPtr = (byte*) (void*) resultData.Scan0;

            if (!horizontal)
            {
                for (int y = 0; y < bitmapHeight; y++)
                {
                    for (int x = 0; x < bitmapWidth; x++)
                    {
                        *resultDataPtr = Convert.ToByte(Math.Max(Math.Min(*bitmapDataPtr + currentColor, 255), 0));
                        bitmapDataPtr++;
                        resultDataPtr++;
                    }

                    bitmapDataPtr += bitmapDataOffset;
                    resultDataPtr += resultDataOffset;
                    currentColor += colorDelta;
                }
            }
            else
            {
                for (int x = 0; x < bitmapWidth; x++)
                {
                    for (int y = 0; y < bitmapHeight; y++)
                    {
                        var offset = y * resultDataStride + x;
                        var ptr1 = bitmapDataPtr + offset;
                        var ptr2 = resultDataPtr + offset;
                        *ptr2 = Convert.ToByte(Math.Max(Math.Min(*ptr1 + currentColor, 255), 0));
                    }

                    currentColor += colorDelta;
                }
            }

            return result;
        }
        catch
        {
            return result;
        }
        finally
        {
            if (bitmapData != null)
            {
                bitmap.UnlockBits(bitmapData);
            }

            if (result != null && resultData != null)
            {
                result.UnlockBits(resultData);
            }
        }
    }

    public static unsafe Bitmap AddGaussianNoise(Bitmap bitmap, double mu = 0.0, double sigma = 1.0)
    {
        BitmapData bitmapData = null;
        Bitmap result = null;
        BitmapData resultData = null;

        try
        {
            var bitmapWidth = bitmap.Width;
            var bitmapHeight = bitmap.Height;
            var bitmapPixelFormat = bitmap.PixelFormat;

            result = BitmapFactory.CreateImage(bitmapWidth, bitmapHeight, bitmapPixelFormat);
            var rectangle = new Rectangle(0, 0, bitmapWidth, bitmapHeight);
            bitmapData = bitmap.LockBits(rectangle, ImageLockMode.ReadWrite, bitmapPixelFormat);
            resultData = result.LockBits(rectangle, ImageLockMode.ReadWrite, bitmapPixelFormat);

            int bitmapDataOffset = bitmapData.Stride - bitmapWidth;
            int resultDataOffset = resultData.Stride - bitmapWidth;

            byte* bitmapDataPtr = (byte*) (void*) bitmapData.Scan0;
            byte* resultDataPtr = (byte*) (void*) resultData.Scan0;

            for (int y = 0; y < bitmapHeight; y++)
            {
                for (int x = 0; x < bitmapWidth; x++)
                {
                    *resultDataPtr = Convert.ToByte(Math.Max(
                        Math.Min(*bitmapDataPtr + RandomUtils.NextGaussianRandomNumber(Random, mu, sigma), 255), 0));
                    bitmapDataPtr++;
                    resultDataPtr++;
                }

                bitmapDataPtr += bitmapDataOffset;
                resultDataPtr += resultDataOffset;
            }

            return result;
        }
        catch
        {
            return result;
        }
        finally
        {
            if (bitmapData != null)
            {
                bitmap.UnlockBits(bitmapData);
            }

            if (result != null && resultData != null)
            {
                result.UnlockBits(resultData);
            }
        }
    }

    private static ImageCodecInfo GetEncoder(ImageFormat format)
    {
        ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();
        foreach (ImageCodecInfo codec in codecs)
        {
            if (codec.FormatID == format.Guid)
            {
                return codec;
            }
        }

        return null;
    }

    public static Bitmap CompressImage(Bitmap bitmap, int quality)
    {
        EncoderParameters parameters = new EncoderParameters(1);
        parameters.Param[0] = new EncoderParameter(Encoder.Quality, quality);
        ImageCodecInfo jpgEncoder = GetEncoder(ImageFormat.Jpeg);

        using var memoryStream = new MemoryStream();
        bitmap.Save(memoryStream, jpgEncoder, parameters);
        var typeConverter = TypeDescriptor.GetConverter(typeof(Bitmap));
        var jpegCompressed = (Bitmap) typeConverter.ConvertFrom(memoryStream.ToArray());
        var jpegCompressedGray = ColorDepthConversion.To8Bits(jpegCompressed);
        jpegCompressed.Dispose();

        return jpegCompressedGray;
    }

    public static float Halve(int x)
    {
        return Convert.ToSingle(Math.Round(Convert.ToDouble(x) / 2D));
    }

    public static List<int> GetShuffledIndices(int count)
    {
        var indices = Enumerable.Range(0, count).ToList();
        RandomUtils.Shuffle(Random, indices);
        return indices;
    }

    public static Bitmap CropImageBoundaries(Bitmap bitmap)
    {
        var rectangle = GenericProcessing.GetBoundaries(bitmap);
        return ImageCropping.Crop(bitmap, rectangle);
    }

    public static Bitmap CreateRandomPrintedLine(List<string> words,
        string delimiter,
        int maxWords,
        int maxImageWidth,
        MinRecurrenceSampler<FontFamily> fontFamiliesSampler)
    {
        var dummyBitmap = new Bitmap(1, 1);
        var font = new Font(fontFamiliesSampler.Next(), Random.Next(11, 25), FontStyle.Regular);

        string text = null;
        using var dummyGraphics = Graphics.FromImage(dummyBitmap);
        var textSize = new SizeF(0, 0);
        List<int> indices = GetShuffledIndices(words.Count);
        foreach (int i in indices)
        {
            textSize = dummyGraphics.MeasureString(words[i],
                font);
            if (textSize.Width < maxImageWidth)
            {
                text = words[i];
                break;
            }
        }

        if (text == null)
        {
            return null;
        }

        int numberOfParts = Random.Next(1, maxWords);
        while (numberOfParts > 1)
        {
            indices = GetShuffledIndices(words.Count);
            foreach (int i in indices)
            {
                string newText = $"{text}{delimiter}{words[i]}";
                var newTextSize = dummyGraphics.MeasureString(newText, font);
                if (newTextSize.Width < maxImageWidth)
                {
                    text = newText;
                    textSize = newTextSize;
                    break;
                }
            }

            numberOfParts--;
        }

        Bitmap bitmap = new Bitmap(Convert.ToInt32(textSize.Width),
            Convert.ToInt32(textSize.Height),
            PixelFormat.Format24bppRgb);

        using (var graphics = Graphics.FromImage(bitmap))
        {
            graphics.Clear(Color.White);
            graphics.DrawString(text, font, Brushes.Black, 0, 0);
        }

        var bitmapGray = ColorDepthConversion.To8Bits(bitmap);
        bitmap.Dispose();

        var cropped = CropImageBoundaries(bitmapGray);
        bitmapGray.Dispose();

        Bitmap result;
        if (RandomUtils.NextBool(Random))
        {
            result = GeometricTransforms.Rotate(cropped, Random.Next(-3, 4), Color.White);
            cropped.Dispose();
        }
        else
        {
            result = cropped;
        }

        return result;
    }

    private static (PrivateFontCollection, List<FontFamily>) LoadFonts(string fontsDirPath)
    {
        var fontsPaths = Directory.GetFiles(fontsDirPath);

        PrivateFontCollection privateFontCollection = new PrivateFontCollection();
        foreach (var fontPath in fontsPaths)
        {
            privateFontCollection.AddFontFile(fontPath);
        }

        var fontFamilies = privateFontCollection.Families.ToList();
        return (privateFontCollection, fontFamilies);
    }

    private static List<Bitmap> LoadGrayImagesCropped(string imagesDirPath)
    {
        var imagesPaths = Directory.GetFiles(imagesDirPath);
        List<Bitmap> images = new List<Bitmap>();

        foreach (var imagePath in imagesPaths)
        {
            var image = new Bitmap(imagePath);
            var gray = ColorDepthConversion.To8Bits(image);
            image.Dispose();
            var cropped = CropImageBoundaries(gray);
            gray.Dispose();
            images.Add(cropped);
        }

        return images;
    }

    public static Bitmap GaussianBlur3x3(Bitmap inputBitmap8Bits)
    {
        float[,] gaussianKernel =
        {
            {1, 2, 1,},
            {2, 4, 2,},
            {1, 2, 1,},
        };

        ConvolutionOptions options = new ConvolutionOptions(gaussianKernel, true, 0, false, 1.0f / 16, 0);
        return ImageConvolution.Convolute(inputBitmap8Bits, options);
    }

    public static Bitmap SmoothenSignature(Bitmap bitmap, int scaleFactor = 2)
    {
        var upScaled =
            GeometricTransforms.ResizeBiCubicInterpolation(bitmap, (double) scaleFactor, (double) scaleFactor);
        var smoothed = GaussianBlur3x3(upScaled);
        var downScaled = GeometricTransforms.ResizeBiCubicInterpolation(smoothed, 1D / scaleFactor, 1D / scaleFactor);
        upScaled.Dispose();
        smoothed.Dispose();
        return downScaled;
    }

    public static unsafe bool IsImageBlank(Bitmap bitmap)
    {
        BitmapData bitmapData = null;

        try
        {
            var bitmapWidth = bitmap.Width;
            var bitmapHeight = bitmap.Height;
            var bitmapPixelFormat = bitmap.PixelFormat;

            var rectangle = new Rectangle(0, 0, bitmapWidth, bitmapHeight);
            bitmapData = bitmap.LockBits(rectangle, ImageLockMode.ReadWrite, bitmapPixelFormat);

            int bitmapDataOffset = bitmapData.Stride - bitmapWidth;
            byte* bitmapDataPtr = (byte*) (void*) bitmapData.Scan0;

            for (int y = 0; y < bitmapHeight; y++)
            {
                for (int x = 0; x < bitmapWidth; x++)
                {
                    if (*bitmapDataPtr != 255)
                    {
                        return false;
                    }

                    bitmapDataPtr++;
                }

                bitmapDataPtr += bitmapDataOffset;
            }

            return true;
        }
        catch
        {
            return false;
        }
        finally
        {
            if (bitmapData != null)
            {
                bitmap.UnlockBits(bitmapData);
            }
        }
    }

    public static int EstimateCurveThickness(Bitmap bitmap)
    {
        var binary = Binarization.OpenCvOtsu(bitmap);
        int n = 0;

        while (!IsImageBlank(binary))
        {
            var eroded = Morphology.Erosion2X2(binary);
            binary.Dispose();
            binary = eroded;
            n++;
        }

        binary.Dispose();
        return n + 1;
    }

    public static unsafe Bitmap DrawForegroundOverBackground(Bitmap foreground,
        Bitmap background,
        Point position,
        byte transparentColor = 255)
    {
        Bitmap result = null;
        BitmapData foregroundData = null;
        BitmapData resultData = null;

        try
        {
            result = (Bitmap) background.Clone();

            var resultWidth = result.Width;
            var resultHeight = result.Height;
            var foregroundWidth = foreground.Width;
            var foregroundHeight = foreground.Height;

            resultData = result.LockBits(new Rectangle(0, 0, resultWidth, resultHeight),
                ImageLockMode.ReadWrite,
                result.PixelFormat);


            foregroundData = foreground.LockBits(new Rectangle(0, 0, foregroundWidth, foregroundHeight),
                ImageLockMode.ReadWrite,
                foreground.PixelFormat);

            var resultDataStride = resultData.Stride;
            var foregroundDataStride = foregroundData.Stride;

            var resultDataOffset = resultDataStride - resultWidth;
            var foregroundDataOffset = foregroundDataStride - foregroundWidth;

            byte* resultDataPtr = (byte*) (void*) resultData.Scan0;
            byte* foregroundDataPtr = (byte*) (void*) foregroundData.Scan0;

            for (int y = 0; y < foregroundHeight; y++)
            {
                var resultDataPtrTemp = resultDataPtr + (position.Y + y) * resultDataStride + position.X;
                for (int x = 0; x < foregroundWidth; x++)
                {
                    byte value = *foregroundDataPtr;
                    if (value != transparentColor)
                    {
                        *resultDataPtrTemp = value;
                    }

                    foregroundDataPtr++;
                    resultDataPtrTemp++;
                }

                foregroundDataPtr += foregroundDataOffset;
            }

            return result;
        }
        catch
        {
            return result;
        }
        finally
        {
            if (result != null && resultData != null)
            {
                result.UnlockBits(resultData);
            }

            if (foregroundData != null)
            {
                foreground.UnlockBits(foregroundData);
            }
        }
    }

    public static unsafe Bitmap RandomizeForegroundColor(Bitmap bitmap, Random random, byte minColor, byte maxColor)
    {
        BitmapData bitmapData = null;
        Bitmap result = null;
        BitmapData resultData = null;

        try
        {
            var bitmapWidth = bitmap.Width;
            var bitmapHeight = bitmap.Height;
            var bitmapPixelFormat = bitmap.PixelFormat;
            var threshold = Binarization.OpenCvOtsuThreshold(bitmap);

            result = BitmapFactory.CreateImage(bitmapWidth, bitmapHeight, bitmapPixelFormat);
            var rectangle = new Rectangle(0, 0, bitmapWidth, bitmapHeight);
            bitmapData = bitmap.LockBits(rectangle, ImageLockMode.ReadWrite, bitmapPixelFormat);
            resultData = result.LockBits(rectangle, ImageLockMode.ReadWrite, bitmapPixelFormat);

            int bitmapDataOffset = bitmapData.Stride - bitmapWidth;
            int resultDataOffset = resultData.Stride - bitmapWidth;

            byte* bitmapDataPtr = (byte*) (void*) bitmapData.Scan0;
            byte* resultDataPtr = (byte*) (void*) resultData.Scan0;

            for (int y = 0; y < bitmapHeight; y++)
            {
                for (int x = 0; x < bitmapWidth; x++)
                {
                    if (*bitmapDataPtr <= threshold)
                    {
                        *resultDataPtr = Convert.ToByte(random.Next(minColor, maxColor));
                    }
                    else
                    {
                        *resultDataPtr = 255;
                    }

                    bitmapDataPtr++;
                    resultDataPtr++;
                }

                bitmapDataPtr += bitmapDataOffset;
                resultDataPtr += resultDataOffset;
            }

            return result;
        }
        catch
        {
            return result;
        }
        finally
        {
            if (bitmapData != null)
            {
                bitmap.UnlockBits(bitmapData);
            }

            if (result != null && resultData != null)
            {
                result.UnlockBits(resultData);
            }
        }
    }

    public static bool IsValueValidPixel(float value)
    {
        return !float.IsNaN(value) && !float.IsInfinity(value) && value >= 0 && value <= 255;
    }

    public static bool IsValueValidPixel(double value)
    {
        return !double.IsNaN(value) && !double.IsInfinity(value) && value >= 0 && value <= 255;
    }


    public static void Main(string[] args)
    {
        const string backgroundsDirPath = @"C:\Users\USER\Desktop\UNet\BaseClean";
        const string signaturesDirPath = @"C:\Users\USER\Development\Data\ASV\Raw\signatures";
        const string fontsDirPath = @"C:\Users\USER\Desktop\UNet\ChequeDataGenerator\fonts";
        const string handWrittenFontsDirPath = @"C:\Users\USER\Desktop\UNet\ChequeDataGenerator\handwritten_fonts";
        const string handwrittenLinesPaths =
            @"C:\Users\USER\Desktop\UNet\ChequeDataGenerator\handwritten_lines";
        const string wordsFilePath = @"C:\Users\USER\Desktop\UNet\ChequeDataGenerator\words.txt";
        const int generatedDataSetSize = 210000; // 430000
        const string inputImagesDirPath = @"C:\Users\USER\Desktop\UNet\input";
        const string outputImagesDirPath = @"C:\Users\USER\Desktop\UNet\output";

        if (!Directory.Exists(inputImagesDirPath))
        {
            Directory.CreateDirectory(inputImagesDirPath);
        }

        if (!Directory.Exists(outputImagesDirPath))
        {
            Directory.CreateDirectory(outputImagesDirPath);
        }

        var backgrounds = Directory.GetFiles(backgroundsDirPath).Select(x =>
        {
            var bitmap = new Bitmap(x);
            var result = ColorDepthConversion.To8Bits(bitmap);
            bitmap.Dispose();
            return result;
        }).ToList();

        var signatures = Directory.GetFiles(signaturesDirPath).Select(x =>
        {
            var bitmap = new Bitmap(x);
            var result = ColorDepthConversion.To8Bits(bitmap);
            bitmap.Dispose();
            return result;
        }).ToList();

        var (privateFontCollection, fontFamilies) = LoadFonts(fontsDirPath);
        var (privateHandwrittenFontCollection, handwrittenFontFamilies) = LoadFonts(handWrittenFontsDirPath);
        var handwrittenLines = LoadGrayImagesCropped(handwrittenLinesPaths);

        var backgroundsSampler = new MinRecurrenceSampler<Bitmap>(backgrounds);
        var signaturesSampler = new MinRecurrenceSampler<Bitmap>(signatures);
        var fontFamiliesSampler = new MinRecurrenceSampler<FontFamily>(fontFamilies);
        var handwrittenFontFamiliesSampler = new MinRecurrenceSampler<FontFamily>(handwrittenFontFamilies);
        var handwrittenLinesSampler = new MinRecurrenceSampler<Bitmap>(handwrittenLines);
        var words = File.ReadAllLines(wordsFilePath).ToList();

        for (int i = 1; i <= generatedDataSetSize; i++)
        {
            var background = backgroundsSampler.Next();
            var backgroundAugmented = AugmentBackground(background);

            var signature = signaturesSampler.Next();

            Bitmap signatureAugmented;
            if (RandomUtils.NextBool(Random))
            {
                signatureAugmented = GeometricTransforms.Rotate(signature, Random.Next(1, 359), Color.White);
            }
            else
            {
                signatureAugmented = (Bitmap) signature.Clone();
            }

            int signatureWidth = signatureAugmented.Width;
            int signatureHeight = signatureAugmented.Height;
            Bitmap signatureResized;

            if (RandomUtils.NextBool(Random) && signatureWidth < background.Width &&
                signatureHeight < background.Height)
            {
                signatureResized = (Bitmap) signatureAugmented.Clone();
            }
            else
            {
                signatureWidth = Random.Next(160, 320);
                signatureHeight = Random.Next(50, 160);
                signatureResized =
                    GeometricTransforms.ResizeBiCubicInterpolation(signatureAugmented, signatureWidth, signatureHeight);
            }

            var lineType = Random.Next(2);
            /*var line = CreateRandomPrintedLine(words, " ", 6, 400,
                lineType == 0 ? fontFamiliesSampler : handwrittenFontFamiliesSampler);*/
            var line = CreateRandomPrintedLine(words, " ", 6, 400, fontFamiliesSampler);
            if (line.Width > 400)
            {
                var factor = 400D / line.Width;
                var newLine = GeometricTransforms.ResizeBiCubicInterpolation(line, factor, factor);
                line.Dispose();
                line = newLine;
            }

            // create training input image 
            var backgroundMean = Statistics.GetMean(backgroundAugmented);
            if (!IsValueValidPixel(backgroundMean))
            {
                backgroundMean = 0;
            }

            var backgroundStd = Statistics.GetStandardDeviation(backgroundAugmented, backgroundMean);
            if (!IsValueValidPixel(backgroundStd))
            {
                backgroundStd = 0;
            }

            var upperLimit = Convert.ToInt32(Math.Max(Math.Min(backgroundMean - backgroundStd, 255), 10));
            var lowerLimit = Random.Next(0, Convert.ToInt32(Math.Max(backgroundMean - 2 * backgroundStd, 0)));

            Bitmap finalLine;
            Bitmap finalSignature;
            Bitmap backgroundWithLine;
            Bitmap backgroundWithLineAndSignature;
            var signatureAreaX = Random.Next(0, 400 - signatureWidth);
            var signatureAreaY = Random.Next(0, 200 - signatureHeight);
            var lineAreaX = Random.Next(0, 400 - line.Width);
            var lineAreaY = Random.Next(0, 10);

            var drawingType = Random.Next(1, 5);
            if (drawingType <= 2)
            {
                finalSignature = RandomizeForegroundColor(signatureResized, Random, Convert.ToByte(lowerLimit),
                    Convert.ToByte(upperLimit));
                finalLine = RandomizeForegroundColor(line, Random, Convert.ToByte(lowerLimit),
                    Convert.ToByte(upperLimit));
                backgroundWithLine =
                    DrawForegroundOverBackground(finalLine, backgroundAugmented, new Point(lineAreaX, lineAreaY));
                backgroundWithLineAndSignature = DrawForegroundOverBackground(finalSignature, backgroundWithLine,
                    new Point(signatureAreaX, signatureAreaY));
            }
            else if (drawingType == 3)
            {
                finalSignature = LimitColorRange(signatureResized, (byte) lowerLimit, 255);
                finalLine = LimitColorRange(line, (byte) lowerLimit, 255);

                backgroundWithLine = BlendForegroundOnBackground(finalLine,
                    backgroundAugmented,
                    new Point(lineAreaX, lineAreaY),
                    RandomUtils.NextFloat(Random, 0.25f, 0.5f),
                    RandomUtils.NextFloat(Random, 0.75f, 1f)
                );

                backgroundWithLineAndSignature = BlendForegroundOnBackground(finalSignature,
                    backgroundWithLine,
                    new Point(signatureAreaX, signatureAreaY),
                    RandomUtils.NextFloat(Random, 0.25f, 0.5f),
                    RandomUtils.NextFloat(Random, 0.75f, 1f)
                );
            }
            else
            {
                if (RandomUtils.NextBool(Random))
                {
                    var tempSignature = Binarization.OpenCvOtsu(signatureResized);
                    var tempLine = Binarization.OpenCvOtsu(line);

                    finalSignature = LimitColorRange(tempSignature, (byte) lowerLimit, 255);
                    finalLine = LimitColorRange(tempLine, (byte) lowerLimit, 255);

                    tempSignature.Dispose();
                    tempLine.Dispose();

                    backgroundWithLine =
                        DrawForegroundOverBackground(finalLine, backgroundAugmented, new Point(lineAreaX, lineAreaY));
                    backgroundWithLineAndSignature = DrawForegroundOverBackground(finalSignature, backgroundWithLine,
                        new Point(signatureAreaX, signatureAreaY));
                }
                else
                {
                    var tempSignature = (Bitmap) signatureResized.Clone();
                    var tempLine = (Bitmap) line.Clone();

                    finalSignature = LimitColorRange(tempSignature, (byte) lowerLimit, 255);
                    finalLine = LimitColorRange(tempLine, (byte) lowerLimit, 255);

                    tempSignature.Dispose();
                    tempLine.Dispose();

                    backgroundWithLine =
                        DrawForegroundOverBackground(finalLine, backgroundAugmented, new Point(lineAreaX, lineAreaY));
                    backgroundWithLineAndSignature = DrawForegroundOverBackground(finalSignature, backgroundWithLine,
                        new Point(signatureAreaX, signatureAreaY));
                }
            }

            Bitmap inputImage;
            if (RandomUtils.NextBool(Random))
            {
                inputImage = RandomUtils.NextBool(Random)
                    ? AddGaussianNoise(backgroundWithLineAndSignature, 0.0, Random.Next(10, 30))
                    : GaussianBlur3x3(backgroundWithLineAndSignature);
            }
            else
            {
                if (Random.Next(1, 6) == 1)
                {
                    var startColor = 0;
                    var endColor = Random.Next(10, 21);
                    if (RandomUtils.NextBool(Random))
                    {
                        startColor = -startColor;
                        endColor = -endColor;
                    }

                    if (RandomUtils.NextBool(Random))
                    {
                        (startColor, endColor) = (endColor, startColor);
                    }

                    inputImage = ApplyGradient(backgroundWithLineAndSignature, startColor, endColor,
                        RandomUtils.NextBool(Random));
                }
                else
                {
                    inputImage = (Bitmap) backgroundWithLineAndSignature.Clone();
                }
            }

            inputImage.Save(Path.Combine(inputImagesDirPath, $"sample-{i}.png"), ImageFormat.Png);

            // create training output image
            var outputImage = BitmapFactory.CreateGrayImage(400, 200, 255);
            var signatureBinarized = Binarization.OpenCvOtsu(signatureResized);
            var lineBinarized = Binarization.OpenCvOtsu(line);

            var outputWithLine =
                DrawForegroundOverBackground(lineBinarized, outputImage, new Point(lineAreaX, lineAreaY));
            var outputWithSignatureAndLine = DrawForegroundOverBackground(signatureBinarized,
                outputWithLine,
                new Point(signatureAreaX, signatureAreaY));

            outputWithSignatureAndLine.Save(Path.Combine(outputImagesDirPath, $"sample-{i}.png"), ImageFormat.Png);

            signatureAugmented.Dispose();
            backgroundAugmented.Dispose();
            signatureResized.Dispose();
            finalSignature.Dispose();
            line.Dispose();
            finalLine.Dispose();
            backgroundAugmented.Dispose();
            backgroundWithLine.Dispose();
            backgroundWithLineAndSignature.Dispose();
            inputImage.Dispose();
            inputImage.Dispose();
            signatureBinarized.Dispose();
            lineBinarized.Dispose();
            outputImage.Dispose();
            outputWithLine.Dispose();
            outputWithSignatureAndLine.Dispose();
        }
    }

    private static Bitmap AugmentBackground(Bitmap background)
    {
        if (RandomUtils.NextBool(Random))
        {
            int augmentationType = Random.Next(1, 5);

            switch (augmentationType)
            {
                case 1:
                    return ChangeBrightness(background, Random.Next(-50, 50));
                case 2:
                    return ChangeContrast(background, RandomUtils.NextFloat(Random, 0.5f, 1.5f));
                case 3:
                {
                    var temp = ChangeBrightness(background, Random.Next(-50, 50));
                    var backgroundAugmented = ChangeContrast(temp, RandomUtils.NextFloat(Random, 0.5f, 1.5f));
                    temp.Dispose();
                    return backgroundAugmented;
                }
                default:
                {
                    var temp = ChangeContrast(background, RandomUtils.NextFloat(Random, 0.5f, 1.5f));
                    var backgroundAugmented = ChangeBrightness(temp, Random.Next(-50, 50));
                    temp.Dispose();
                    return backgroundAugmented;
                }
            }
        }

        return (Bitmap) background.Clone();
    }
}