#!/usr/bin/python3.6

import math

EPOCHS = 20
DATA_POINTS = 8704000
BATCH_SIZE = 128
LEARNING_RATE = 0.1
DECAY_RATE = 7.352941176470589e-07

# 0.05 --> 1e-4
# DECAY_RATE = 0.3 * LEARNING_RATE / EPOCHS


def calculate_decay_after_iterations(iterations):
    return LEARNING_RATE / (1.0 + DECAY_RATE * iterations)


def calculate_decay_after_epochs(epochs):
    return calculate_decay_after_iterations(math.ceil(DATA_POINTS / BATCH_SIZE) * (epochs + 1))


def all_iterations():
    return math.ceil(DATA_POINTS / BATCH_SIZE) * EPOCHS


# iterations = data_points / batch_size * epochs
def calculate_decay_rate(final_lr, iterations):
    return (LEARNING_RATE / final_lr - 1.0) / iterations


def main():
    # for i in range(all_iterations()):
    #     print(calculate_decay_after_iterations(i))

    print('calculated decay rate:')
    print(calculate_decay_rate(0.05, DATA_POINTS / BATCH_SIZE * EPOCHS))

    print('\nlr on each epoch:')
    for i in range(EPOCHS):
        print(calculate_decay_after_epochs(i))


if __name__ == "__main__":
    main()
