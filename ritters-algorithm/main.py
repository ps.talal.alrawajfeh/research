import mimetypes
import os
import pickle
import random

import cv2
import numpy as np
from keras.applications.densenet import DenseNet201, preprocess_input
from scipy.spatial import distance
from tqdm import tqdm

IMAGE_CLASSES_FEATURE_VECTORS_FILE = 'image_classes_feature_vectors.pickle'

IMAGE_PATHS = ['/home/user/Development/data/trainingSignatures',
               '/home/user/Development/data/CABHBTF']

PRE_TRAINED_MODEL = DenseNet201(include_top=False,
                                weights='imagenet',
                                input_shape=(224, 224, 3),
                                pooling='avg')


def furthest_point_from(points, point):
    max_distance = 0.0
    furthest_point = None
    for p in points:
        dist = distance.euclidean(p, point)
        if dist > max_distance:
            max_distance = dist
            furthest_point = p
    return furthest_point


def smallest_enclosing_hypersphere(points):
    x = random.choice(points)
    y = furthest_point_from(points, x)
    z = furthest_point_from(points, y)
    center = (y + z) / 2
    radius = distance.euclidean(center, z)
    for p in points:
        dist = distance.euclidean(p, center)
        if dist > radius:
            d = dist - radius
            difference = p - center
            directional_vector = difference / np.sum(np.square(difference))
            center += directional_vector * d / 2
            radius += d / 2
    return center, radius


def get_extensions_for_type(general_type):
    for ext in mimetypes.types_map:
        if mimetypes.types_map[ext].split('/')[0] == general_type:
            yield ext.lower()


def memoize(function):
    memo = {}

    def wrapper(*args):
        if args in memo:
            return memo[args]
        else:
            rv = function(*args)
            memo[args] = rv
            return rv

    return wrapper


@memoize
def get_image_extensions():
    return tuple(get_extensions_for_type('image'))


def file_extension(path):
    return os.path.splitext(os.path.basename(path))[1].lower()


def is_image(path):
    return file_extension(path) in get_image_extensions()


def class_from_file_name(file_name):
    return file_name[0:9]


def extract_classes(iterable,
                    class_lambda,
                    item_mapper_lambda=lambda x: x):
    classes = dict()
    for item in iterable:
        c = class_lambda(item)
        if c not in classes:
            classes[c] = []
        classes[c].append(item_mapper_lambda(item))
    return classes


def extract_classes_from_file_names(paths,
                                    file_name_class_lambda=class_from_file_name):
    image_classes = dict()
    for path in paths:
        image_classes.update(extract_classes(filter(is_image, os.listdir(path)),
                                             file_name_class_lambda,
                                             lambda f: os.path.join(path, f)))
    return image_classes


def threshold(image):
    return cv2.threshold(
        image,
        0,
        255,
        cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]


def pre_process_image(image):
    resized = cv2.resize(image,
                         (224, 224),
                         interpolation=cv2.INTER_CUBIC)
    return 255 - threshold(resized)


def replicate_channel(image, replicas=3):
    return np.concatenate([(np.expand_dims(image, axis=-1)) for _ in range(replicas)],
                          axis=-1)


def images_to_feature_vectors(image_batch,
                              model=PRE_TRAINED_MODEL,
                              model_specific_pre_processor=preprocess_input,
                              image_pre_processor=pre_process_image):
    return model.predict(
        model_specific_pre_processor(
            np.array(
                [replicate_channel(image_pre_processor(image))
                 for image in image_batch]
            )))


def serialize_object(obj, file):
    with open(file, 'wb') as f:
        pickle.dump(obj, f)


def deserialize_object(file):
    with open(file, 'rb') as f:
        return pickle.load(f)


def main():
    if not os.path.isfile(IMAGE_CLASSES_FEATURE_VECTORS_FILE):
        image_classes = extract_classes_from_file_names(IMAGE_PATHS)
        image_classes_feature_vectors = dict()

        progress_bar = tqdm(total=len(image_classes))
        for key in image_classes:
            images = [cv2.imread(image, cv2.IMREAD_GRAYSCALE) for image in image_classes[key]]
            image_classes_feature_vectors[key] = images_to_feature_vectors(images)
            progress_bar.update()
        progress_bar.close()

        serialize_object(image_classes_feature_vectors, IMAGE_CLASSES_FEATURE_VECTORS_FILE)
    else:
        image_classes_feature_vectors = deserialize_object(IMAGE_CLASSES_FEATURE_VECTORS_FILE)

    averages = []
    progress_bar = tqdm(total=len(image_classes_feature_vectors))
    for feature_vector_class in image_classes_feature_vectors:
        first_class = image_classes_feature_vectors[feature_vector_class]
        centroid, radius = smallest_enclosing_hypersphere(first_class)

        false_positive = 0
        n = 0
        for key in image_classes_feature_vectors:
            if key == feature_vector_class:
                continue
            for feature_vector in image_classes_feature_vectors[key]:
                dist = distance.euclidean(centroid, feature_vector)
                if dist < radius:
                    print('found fp')
                    false_positive += 1
                n += 1

        averages.append(false_positive / n)
        progress_bar.update()
    progress_bar.close()

    print(f'max: {np.max(averages)}')
    print(f'min: {np.min(averages)}')
    print(f'mean: {np.mean(averages)}')
    print(f'std: {np.std(averages)}')


# def main():
#     points = np.array([[1, 2],
#                        [3, 3],
#                        [2, 5],
#                        [10, 2],
#                        [3, 6],
#                        [9, 4]], np.float)
#
#     center, radius = smallest_enclosing_hypersphere(points)
#
#     image = np.ones((20, 20, 3), np.uint8) * 255
#     cv2.circle(image, tuple(np.int0(center)), int(round(radius)), (0, 0, 0), 1)
#     for p in points:
#         image[int(p[1]), int(p[0])] = [255, 0, 0]
#     plt.imshow(image)
#     plt.show()
#     print((center, radius))


if __name__ == '__main__':
    main()
