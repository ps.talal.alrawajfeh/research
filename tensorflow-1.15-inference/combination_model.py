import math
import pickle
import random
from tensorflow.python.platform import gfile
import tensorflow as tf

import numpy as np
from scipy.interpolate import interp1d
from tensorflow.keras.utils import Sequence
from tqdm import tqdm
import keras.backend as K

from reporting import generate_report, generate_verification_simulation_report

TEST_FEATURE_VECTOR_CLASSES_FILE = 'feature_vector_classes.pickle'
TRIPLET_MODEL_FILE_NAME = 'C:\\Users\\USER\\Desktop\\Triplet.pb'
SIAMESE_MODEL_FILE_NAME = 'C:\\Users\\USER\\Desktop\\Siamese.pb'
EPSILON = 1e-6


def euclidean_distance(x, y):
    squared_euclidean_distance = np.sum(np.square(np.subtract(x, y)), axis=-1)
    return np.sqrt(np.maximum(squared_euclidean_distance, 0.0))


def predict(embedding_graph_def,
            siamese_graph_def,
            input_batch1,
            input_batch2):
    distance_to_probability = interp1d([0.0, 0.6, 0.9, 1.3, 2.0],
                                       [1.0, 0.9, 0.5, 0.1, 0.0])
    siamese_output_mapper = interp1d([0.0, 0.7, 0.95, 1.0],
                                     [0.0, 0.5, 0.75, 1.0])

    output_tensor = embedding_graph_def.graph.get_tensor_by_name(f'embedding/lambda_1/l2_normalize:0')
    embeddings1 = embedding_graph_def.run(output_tensor, {f'embedding/input_1:0': input_batch1})
    embeddings2 = embedding_graph_def.run(output_tensor, {f'embedding/input_1:0': input_batch2})

    output_tensor = siamese_graph_def.graph.get_tensor_by_name(f'siamese/concatenate_1/concat:0')
    siamese_output = siamese_graph_def.run(output_tensor,
                              {f'siamese/input_1:0': input_batch1, f'siamese/input_2:0': input_batch2})[:, 0]

    distances = euclidean_distance(embeddings1, embeddings2)
    triplet_probabilities = distance_to_probability(distances)
    siamese_mapped_output = siamese_output_mapper(siamese_output)

    triplet_probabilities[triplet_probabilities >= 1.0 - EPSILON] = 1.0
    siamese_mapped_output[siamese_mapped_output < 0.35] = 0.0

    combined_values = 0.5 * triplet_probabilities + 0.5 * siamese_mapped_output
    combined_values[combined_values < 0.35] = 0.0
    return combined_values


def shuffle_pairs(pairs):
    references, others, true_outputs = pairs
    indices = list(range(len(references)))
    random.shuffle(indices)

    shuffled_references = [references[indices[i]] for i in range(len(references))]
    shuffled_others = [others[indices[i]] for i in range(len(references))]
    shuffled_true_outputs = [true_outputs[indices[i]] for i in range(len(references))]

    return shuffled_references, shuffled_others, shuffled_true_outputs


def to_float_array(np_array):
    return np.array(np_array, np.float32)


def generate_pairs(feature_vector_classes,
                   references_per_class=14,
                   forgery_classes_per_reference=4,
                   forgeries_per_forgery_class=4):
    keys = [*feature_vector_classes]
    number_of_classes = len(keys)

    references = []
    others = []
    labels = []

    for c in range(number_of_classes - forgery_classes_per_reference):
        c = random.randint(0, len(keys) - 1)
        key = keys.pop(c)

        reference_indices = random.sample(list(range(len(feature_vector_classes[key]))),
                                          min(references_per_class, len(feature_vector_classes[key])))

        for r in reference_indices:
            reference = [key, r]
            genuines = [[key, i] for i in range(len(feature_vector_classes[key]))]
            references.extend([reference] * len(genuines))
            others.extend(genuines)
            labels.extend([1] * len(genuines))

            forgery_keys = random.sample(keys, min(forgery_classes_per_reference, len(keys)))
            for forgery_key in forgery_keys:
                forgery_class = feature_vector_classes[forgery_key]
                forgery_indices = random.sample(list(range(len(forgery_class))),
                                                min(forgeries_per_forgery_class, len(forgery_class)))
                forgeries = [[forgery_key, f] for f in forgery_indices]

                references.extend([reference] * len(forgeries))
                others.extend(forgeries)
                labels.extend([0] * len(forgeries))

    return references, others, labels


class DataGenerator(Sequence):
    def __init__(self,
                 feature_vector_classes,
                 pairs,
                 batch_size,
                 count):
        self.batch_size = batch_size
        self.count = count
        self.feature_vector_classes = feature_vector_classes
        self.references, self.others, self.true_labels = shuffle_pairs(pairs)

    def __len__(self):
        return math.ceil(self.count / self.batch_size)

    def __getitem__(self, index):
        references, others, true_labels = [], [], []
        for i in range(index * self.batch_size,
                       min((index + 1) * self.batch_size, len(self.references))):
            reference_pair = self.references[i]
            other_pair = self.others[i]
            reference_image = self.feature_vector_classes[reference_pair[0]][reference_pair[1]]
            key = other_pair[0]
            other_image = self.feature_vector_classes[key][other_pair[1]]
            references.append(to_float_array(reference_image))
            others.append(to_float_array(other_image))
            true_labels.append(to_float_array(self.true_labels[i]))

        if len(references) == 0:
            return self.__getitem__(0)

        return [np.array(references), np.array(others)], np.array(true_labels)

    def on_epoch_end(self):
        self.references, self.others, self.true_labels = shuffle_pairs([self.references,
                                                                        self.others,
                                                                        self.true_labels])


def deserialize_object(file):
    with open(file, 'rb') as f:
        return pickle.load(f)


def evaluate():
    f = gfile.FastGFile(TRIPLET_MODEL_FILE_NAME, 'rb')
    embedding_graph_def = tf.GraphDef()
    embedding_graph_def.ParseFromString(f.read())
    f.close()

    f = gfile.FastGFile(SIAMESE_MODEL_FILE_NAME, 'rb')
    siamese_graph_def = tf.GraphDef()
    siamese_graph_def.ParseFromString(f.read())
    f.close()

    with tf.Graph().as_default() as graph:
        tf.import_graph_def(embedding_graph_def, name='embedding')
        embedding_graph = graph
        embedding_session = tf.Session(graph=embedding_graph)

    with tf.Graph().as_default() as graph:
        tf.import_graph_def(siamese_graph_def, name='siamese')
        siamese_graph = graph
        siamese_session = tf.Session(graph=siamese_graph)

    feature_vector_classes = deserialize_object(TEST_FEATURE_VECTOR_CLASSES_FILE)

    test_pairs = generate_pairs(feature_vector_classes)
    test_data_generator = DataGenerator(feature_vector_classes,
                                        test_pairs,
                                        512,
                                        len(test_pairs[0]))

    progress_bar = tqdm(total=len(test_data_generator))

    predictions = []
    true_labels = []
    for i in range(len(test_data_generator)):
        [input_batch1, input_batch2], labels = test_data_generator[i]
        probabilities = predict(embedding_session,
                                siamese_session,
                                input_batch1,
                                input_batch2)
        true_labels.extend(labels)
        predictions.extend(probabilities)
        progress_bar.update()
    progress_bar.close()

    predictions = np.array(predictions)
    true_labels = np.array(true_labels)

    generate_report(predictions,
                    true_labels,
                    5,
                    'combined_model_report.txt',
                    'combined_model_genuine_distribution.png',
                    'combined_model_forgery_distribution.png',
                    'combined_model_mixed_distributions.png')

    generate_verification_simulation_report(feature_vector_classes,
                                            lambda x, y: predict(embedding_session,
                                                                 siamese_session,
                                                                 np.array(x),
                                                                 np.array(y)),
                                            'combined_model_report.txt')


if __name__ == '__main__':
    evaluate()
