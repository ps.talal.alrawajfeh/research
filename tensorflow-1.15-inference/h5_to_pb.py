import os

import keras.backend as K
import tensorflow as tf
from keras import Model, Input
from keras.layers import ReLU, Lambda, Dense, BatchNormalization, Concatenate, Dropout, GaussianNoise, Activation, Conv2D, MaxPooling2D, UpSampling2D
from tensorflow.python.framework.graph_util import convert_variables_to_constants

EMBEDDING_VECTOR_SIZE = 1920
LEAKY_RELU_ALPHA = 0.05
LABEL_SMOOTHING_ALPHA = 0.1
EMBEDDING_LAYER_SIZE = 512


def freeze_session(session, keep_var_names=None, output_names=None, clear_devices=True):
    graph = session.graph
    with graph.as_default():
        freeze_var_names = list(set(v.op.name for v in tf.global_variables()).difference(keep_var_names or []))
        output_names = output_names or []
        output_names += [v.op.name for v in tf.global_variables()]
        input_graph_def = graph.as_graph_def()
        if clear_devices:
            for node in input_graph_def.node:
                node.device = ''
        frozen_graph = convert_variables_to_constants(session,
                                                      input_graph_def,
                                                      output_names,
                                                      freeze_var_names)
        return frozen_graph


def leaky_relu6(negative_slope=LEAKY_RELU_ALPHA):
    return ReLU(max_value=6, negative_slope=negative_slope)


def absolute_difference(tensors):
    x, y = tensors
    return tf.abs(tf.reduce_sum(tf.stack([x, -y], axis=1), axis=1))


def siamese_net(input_shape=(EMBEDDING_VECTOR_SIZE,)):
    input1 = Input(input_shape)
    input2 = Input(input_shape)

    embedding_input = Input(input_shape)
    embedding = BatchNormalization()(embedding_input)
    # embedding = Dropout(0.5)(embedding)
    embedding = GaussianNoise(stddev=1e-7)(embedding)

    fc1 = Dense(1024,
                use_bias=False,
                kernel_initializer='he_normal')(embedding)
    fc1 = BatchNormalization()(fc1)
    fc1 = leaky_relu6()(fc1)
    fc1 = Dropout(0.5)(fc1)

    fc2 = Dense(1024,
                use_bias=False,
                kernel_initializer='he_normal')(fc1)
    fc2 = BatchNormalization()(fc2)
    fc2 = leaky_relu6()(fc2)

    embedding_model = Model(inputs=[embedding_input], outputs=[fc2])

    input_embedding1 = embedding_model(input1)
    input_embedding2 = embedding_model(input2)
    dichotomy = Lambda(absolute_difference)([input_embedding1, input_embedding2])
    dichotomy = BatchNormalization()(dichotomy)
    dichotomy = Dropout(0.5)(dichotomy)

    dichotomy_fc1 = Dense(1024,
                          use_bias=False,
                          kernel_initializer='glorot_normal')(dichotomy)
    dichotomy_fc1 = BatchNormalization()(dichotomy_fc1)
    dichotomy_fc1 = leaky_relu6()(dichotomy_fc1)
    dichotomy_fc1 = Dropout(0.5)(dichotomy_fc1)

    dichotomy_rbf = Lambda(lambda x: tf.norm(x, ord='euclidean', axis=-1, keepdims=True))(dichotomy_fc1)
    dichotomy_rbf = Dense(1,
                          use_bias=False,
                          kernel_initializer=tf.constant_initializer(value=0.5),
                          kernel_constraint=tf.keras.constraints.NonNeg())(dichotomy_rbf)
    dichotomy_rbf = Lambda(lambda x: tf.exp(-x))(dichotomy_rbf)

    dichotomy_rbf = Dense(1,
                          activation='sigmoid',
                          kernel_initializer='he_normal')(dichotomy_rbf)
    dichotomy_rbf_inverted = Lambda(lambda x: 1.0 - x)(dichotomy_rbf)
    output = Concatenate()([dichotomy_rbf, dichotomy_rbf_inverted])

    return Model(inputs=[input1, input2], outputs=[output])


def embedding_network(input_shape=(EMBEDDING_VECTOR_SIZE,)):
    input_layer = Input(input_shape)
    embedding = BatchNormalization()(input_layer)
    # embedding = Dropout(0.5)(embedding)
    embedding = GaussianNoise(stddev=1e-7)(embedding)

    fc1 = Dense(1024,
                use_bias=False,
                kernel_initializer='glorot_uniform')(embedding)
    fc1 = BatchNormalization()(fc1)
    fc1 = Activation('tanh')(fc1)

    concatenated = Concatenate()([embedding, fc1])
    concatenated = BatchNormalization()(concatenated)
    concatenated = Dropout(0.5)(concatenated)

    fc2 = Dense(1024,
                use_bias=False,
                kernel_initializer='glorot_uniform')(concatenated)
    fc2 = BatchNormalization()(fc2)
    fc2 = Activation('tanh')(fc2)
    fc2 = Dropout(0.5)(fc2)

    output = Dense(EMBEDDING_LAYER_SIZE,
                   use_bias=False,
                   activation='tanh',
                   kernel_initializer='glorot_uniform')(fc2)

    output = Lambda(lambda x: tf.math.l2_normalize(x, axis=-1))(output)

    return Model(inputs=[input_layer], outputs=[output])


INPUT_SIZE = (748, 200)
INPUT_SHAPE = (INPUT_SIZE[1], INPUT_SIZE[0], 1)


def build_model(input_shape=INPUT_SHAPE):
    input_layer = Input(shape=input_shape)

    conv1 = Conv2D(96, (3, 3), activation='relu', padding='same')(
        input_layer)  # 28 x 28 x 32
    conv1 = BatchNormalization()(conv1)
    conv1 = Conv2D(96, (3, 3), activation='relu', padding='same')(conv1)
    conv1 = BatchNormalization()(conv1)
    pool1 = MaxPooling2D(pool_size=(2, 2))(conv1)  # 14 x 14 x 32
    conv2 = Conv2D(128, (3, 3), activation='relu',
                   padding='same')(pool1)  # 14 x 14 x 64
    conv2 = BatchNormalization()(conv2)
    conv2 = Conv2D(128, (3, 3), activation='relu', padding='same')(conv2)
    conv2 = BatchNormalization()(conv2)
    pool2 = MaxPooling2D(pool_size=(2, 2))(conv2)  # 7 x 7 x 64
    conv3 = Conv2D(256, (3, 3), activation='relu', padding='same')(
        pool2)  # 7 x 7 x 128 (small and thick)
    conv3 = BatchNormalization()(conv3)
    conv3 = Conv2D(256, (3, 3), activation='relu', padding='same')(conv3)
    conv3 = BatchNormalization()(conv3)
    conv4 = Conv2D(384, (3, 3), activation='relu', padding='same')(
        conv3)  # 7 x 7 x 256 (small and thick)
    conv4 = BatchNormalization()(conv4)
    conv4 = Conv2D(384, (3, 3), activation='relu', padding='same')(conv4)
    conv4 = BatchNormalization()(conv4)

    conv5 = Conv2D(256, (3, 3), activation='relu',
                   padding='same')(conv4)  # 7 x 7 x 128
    conv5 = BatchNormalization()(conv5)
    conv5 = Conv2D(256, (3, 3), activation='relu', padding='same')(conv5)
    conv5 = BatchNormalization()(conv5)
    conv6 = Conv2D(128, (3, 3), activation='relu',
                   padding='same')(conv5)  # 7 x 7 x 64
    conv6 = BatchNormalization()(conv6)
    conv6 = Conv2D(128, (3, 3), activation='relu', padding='same')(conv6)
    conv6 = BatchNormalization()(conv6)
    up1 = UpSampling2D((2, 2))(conv6)  # 14 x 14 x 64
    conv7 = Conv2D(96, (3, 3), activation='relu',
                   padding='same')(up1)  # 14 x 14 x 32
    conv7 = BatchNormalization()(conv7)
    conv7 = Conv2D(96, (3, 3), activation='relu', padding='same')(conv7)
    conv7 = BatchNormalization()(conv7)
    up2 = UpSampling2D((2, 2))(conv7)  # 28 x 28 x 32
    decoded = Conv2D(1, (3, 3), activation='sigmoid',
                     padding='same')(up2)  # 28 x 28 x 1

    return Model(input_layer, decoded)


if __name__ == '__main__':
    # input_path = '/home/u764/Downloads/siamese'
    output_path = '/home/u764/Downloads/autoencoder'
    # model = tf.keras.models.load_model(input_path)

    sess = tf.get_default_session()
    # tf.keras.models.save_model(model, output_path)
    # tf.saved_model.save(model, output_path)
    # input_path = '/home/u764/Downloads/h5_models/dense_net201.h5'
    # output_path = '/home/u764/Downloads/h5_models/dense_net201.pb'
    #

    model = build_model()
    model.load_weights("/home/u764/Development/autoencoder.h5")
    K.set_learning_phase(0)
    #
    # # model = siamese_net()
    # model = DenseNet201(include_top=False,
    #                     weights='imagenet',
    #                     input_shape=(224, 224, 3),
    #                     pooling='avg')
    # model.load_weights(input_path)
    #
    tf.train.write_graph(freeze_session(K.get_session(),
                                        output_names=[out.op.name for out in model.outputs]),
                         os.path.dirname(output_path),
                         os.path.basename(output_path),
                         as_text=False)
