import cv2
import numpy as np
import tensorflow as tf
from scipy.interpolate import interp1d
from tensorflow.keras.applications.densenet import preprocess_input
from tensorflow.python.platform import gfile

EPSILON = 1e-6


def threshold_otsu(image):
    return cv2.threshold(image,
                         0,
                         255,
                         cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]


def remove_white_border_non_binary(image):
    binary_image = threshold_otsu(image)

    mask = (255 - binary_image) > 0

    height, width = binary_image.shape[0:2]
    mask1, mask2 = mask.any(0), mask.any(1)
    x1, x2 = mask1.argmax(), width - mask1[::-1].argmax()
    y1, y2 = mask2.argmax(), height - mask2[::-1].argmax()

    return image[y1:y2, x1:x2]


def pre_process_image(image):
    # new_image = remove_white_border_non_binary(image)
    # new_image = cv2.resize(new_image, (224, 224),
    #                        interpolation=cv2.INTER_CUBIC)
    # return 255 - threshold_otsu(new_image)
    return 255 - image


def replicate_channel(image, replicas=3):
    return np.concatenate([(np.expand_dims(image, axis=-1)) for _ in range(replicas)],
                          axis=-1)


def euclidean_distance(x, y):
    squared_euclidean_distance = np.sum(np.square(np.subtract(x, y)), axis=-1)
    return np.sqrt(np.maximum(squared_euclidean_distance, 0.0))


def predict(embedding_session,
            siamese_session,
            input_batch1,
            input_batch2):
    distance_to_probability = interp1d([0.0, 0.6, 0.9, 1.3, 2.0],
                                       [1.0, 0.9, 0.5, 0.1, 0.0])
    siamese_output_mapper = interp1d([0.0, 0.7, 0.95, 1.0],
                                     [0.0, 0.5, 0.75, 1.0])

    # output_tensor = embedding_session.graph.get_tensor_by_name(f'embedding/lambda_1/l2_normalize:0')
    # embeddings1 = embedding_session.run(output_tensor, {f'embedding/input_1:0': input_batch1})
    # embeddings2 = embedding_session.run(output_tensor, {f'embedding/input_1:0': input_batch2})
    output_tensor = embedding_session.graph.get_tensor_by_name(f'embedding/lambda_2/l2_normalize:0')
    embeddings1 = embedding_session.run(output_tensor, {f'embedding/input_2:0': input_batch1})
    embeddings2 = embedding_session.run(output_tensor, {f'embedding/input_2:0': input_batch2})

    # output_tensor = siamese_session.graph.get_tensor_by_name(f'siamese/concatenate_1/concat:0')
    # siamese_output = siamese_session.run(output_tensor,
    #                                      {f'siamese/input_1:0': input_batch1, f'siamese/input_2:0': input_batch2})[:, 0]
    output_tensor = siamese_session.graph.get_tensor_by_name('siamese/dense_2/Sigmoid:0')
    siamese_output = siamese_session.run(output_tensor,
                                         {f'siamese/input_1:0': input_batch1, f'siamese/input_2:0': input_batch2})[:, 0]

    distances = euclidean_distance(embeddings1, embeddings2)
    triplet_probabilities = distance_to_probability(distances)
    siamese_mapped_output = siamese_output_mapper(siamese_output)

    triplet_probabilities[triplet_probabilities >= 1.0 - EPSILON] = 1.0
    siamese_mapped_output[siamese_mapped_output < 0.35] = 0.0

    combined_values = 0.5 * triplet_probabilities + 0.5 * siamese_mapped_output
    combined_values[combined_values < 0.35] = 0.0
    return combined_values


def main():
    f = gfile.FastGFile('/home/u764/Downloads/models/Dense.pb', 'rb')
    # f = gfile.FastGFile('/home/u764/Development/progressoft/asv/model/models_jan_16_2022/Dense/Dense.pb', 'rb')
    dense_graph_def = tf.GraphDef()
    dense_graph_def.ParseFromString(f.read())
    f.close()

    f = gfile.FastGFile('/home/u764/Downloads/models/triplet_mixed_dpi.pb', 'rb')
    # f = gfile.FastGFile('/home/u764/Development/progressoft/asv/model/models_jan_16_2022/Triplet/Triplet.pb', 'rb')
    embedding_graph_def = tf.GraphDef()
    embedding_graph_def.ParseFromString(f.read())
    f.close()

    f = gfile.FastGFile('/home/u764/Downloads/models/siamese_mixed_dpi.pb', 'rb')
    # f = gfile.FastGFile('/home/u764/Development/progressoft/asv/model/models_jan_16_2022/Siamese/Siamese.pb', 'rb')
    siamese_graph_def = tf.GraphDef()
    siamese_graph_def.ParseFromString(f.read())
    f.close()

    with tf.Graph().as_default() as graph:
        tf.import_graph_def(dense_graph_def, name='dense')
        dense_graph = graph
        dense_session = tf.Session(graph=dense_graph)

    with tf.Graph().as_default() as graph:
        tf.import_graph_def(embedding_graph_def, name='embedding')
        embedding_graph = graph
        embedding_session = tf.Session(graph=embedding_graph)

    with tf.Graph().as_default() as graph:
        tf.import_graph_def(siamese_graph_def, name='siamese')
        siamese_graph = graph
        siamese_session = tf.Session(graph=siamese_graph)

    # image1 = cv2.imread('/home/u764/Downloads/Temp-Signatures/Sig_62.tif', cv2.IMREAD_GRAYSCALE)
    # image2 = cv2.imread('/home/u764/Downloads/Temp-Signatures/Sig_67.tif', cv2.IMREAD_GRAYSCALE)
    # image3 = cv2.imread('/home/u764/Downloads/Temp-Signatures/Sig_86.tif', cv2.IMREAD_GRAYSCALE)
    # image4 = cv2.imread('/home/u764/Downloads/Temp-Signatures/Sig_96.tif', cv2.IMREAD_GRAYSCALE)

    image1 = cv2.imread('/home/u764/Downloads/old-resize/Sig_62.png', cv2.IMREAD_GRAYSCALE)
    image2 = cv2.imread('/home/u764/Downloads/old-resize/Sig_67.png', cv2.IMREAD_GRAYSCALE)
    image3 = cv2.imread('/home/u764/Downloads/old-resize/Sig_86.png', cv2.IMREAD_GRAYSCALE)
    image4 = cv2.imread('/home/u764/Downloads/old-resize/Sig_96.png', cv2.IMREAD_GRAYSCALE)

    images = [image1, image2, image3, image4]

    output_tensor = dense_session.graph.get_tensor_by_name(f'dense/avg_pool/Mean:0')
    x = preprocess_input(np.array(
        [replicate_channel(pre_process_image(image))
         for image in images]))
    feature_vector1, feature_vector2, feature_vector3, feature_vector4 = dense_session.run(output_tensor,
                                                                                           {f'dense/input_1:0': x})

    print(predict(embedding_session,
                  siamese_session,
                  np.array([feature_vector1,
                            feature_vector1,
                            feature_vector1,
                            feature_vector2,
                            feature_vector2,
                            feature_vector3]),
                  np.array([feature_vector2,
                            feature_vector3,
                            feature_vector4,
                            feature_vector3,
                            feature_vector4,
                            feature_vector4])))


if __name__ == '__main__':
    main()
