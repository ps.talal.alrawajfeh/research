import os

import cv2


def threshold_otsu(image):
    return cv2.threshold(image,
                         0,
                         255,
                         cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]


def remove_white_border_non_binary(image):
    binary_image = threshold_otsu(image)

    mask = (255 - binary_image) > 0

    height, width = binary_image.shape[0:2]
    mask1, mask2 = mask.any(0), mask.any(1)
    x1, x2 = mask1.argmax(), width - mask1[::-1].argmax()
    y1, y2 = mask2.argmax(), height - mask2[::-1].argmax()

    return image[y1:y2, x1:x2]


def main():
    base_dir = '/home/u764/Development/data/signatures'
    output_dir = '/home/u764/Development/data/signatures-py'

    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)

    files = os.listdir(base_dir)

    images = []
    for f in files:
        try:
            path = os.path.join(base_dir, f)
            image = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
        except:
            continue
        images.append((path, image))

    for path, image in images:
        file_name = os.path.basename(path).split('.')[0]
        new_image = remove_white_border_non_binary(image)
        new_image = cv2.resize(new_image, (224, 224),
                               interpolation=cv2.INTER_CUBIC)
        new_image = threshold_otsu(new_image)
        cv2.imwrite(os.path.join(output_dir, file_name + '.png'), new_image)


if __name__ == '__main__':
    main()
