#!/usr/bin/python3.6

import time

import keras.backend as K
import numpy as np
import tensorflow as tf
from keras import Model, Input
from keras.layers import Lambda
from tensorflow.python.platform import gfile

MODEL_FILE_PATH = '/home/u764/Development/progressoft/asv/model/models_jan_16_2022/Siamese/Siamese.pb'
MODEL_INPUT_TENSOR_NAME = 'input_1'
MODEL_OUTPUT_TENSOR_NAME = 'concatenate_1/concat'
BENCHMARK_ITERATIONS = 1000
INPUT_SHAPE = (1920,)


def main():
    f = gfile.FastGFile(MODEL_FILE_PATH, 'rb')
    graph_def = tf.GraphDef()
    graph_def.ParseFromString(f.read())
    f.close()

    with K.get_session() as sess:
        sess.graph.as_default()
        tf.import_graph_def(graph_def)

        size = 1
        for i in INPUT_SHAPE:
            size *= i
        x = np.random.normal(0, 1.0, (size,)).reshape((1,) + INPUT_SHAPE)

        output_tensor = sess.graph.get_tensor_by_name(f'import/{MODEL_OUTPUT_TENSOR_NAME}:0')

        start_time = time.time()
        for i in range(BENCHMARK_ITERATIONS):
            predictions = sess.run(output_tensor, {f'import/input_1:0': x, f'import/input_2:0': x})
            # print(i)
        end_time = time.time()

    print(f'average inference takes: {(end_time - start_time) / BENCHMARK_ITERATIONS}')


if __name__ == '__main__':
    main()
