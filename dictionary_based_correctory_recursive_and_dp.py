import sys
import time

import numpy as np
import textdistance

ENGLISH_AMOUNTS_DICTIONARY = [
    'zero',
    'one',
    'two',
    'three',
    'four',
    'five',
    'six',
    'seven',
    'eight',
    'nine',
    'ten',
    'eleven',
    'twelve',
    'thirteen',
    'fourteen',
    'fifteen',
    'sixteen',
    'seventeen',
    'eighteen',
    'nineteen',
    'twenty',
    'thirty',
    'forty',
    'fifty',
    'sixty',
    'seventy',
    'eighty',
    'ninety',
    'hundred',
    'thousand',
    'million',
    'only',
    'and'
]

ARABIC_AMOUNTS_DICTIONARY = [
    'صفر',
    'واحد',
    'اثنان',
    'اثنين',
    'ثلاثة',
    'ثلاث',
    'اربعة',
    'اربع',
    'خمسة',
    'خمس',
    'ستة',
    'ست',
    'سبعة',
    'سبع',
    'ثمانية',
    'ثمان',
    'تسعة',
    'تسع',
    'عشرة',
    'عشر',
    'احد',
    'اثنا',
    'عشرون',
    'ثلاثون',
    'اربعون',
    'خمسون',
    'ستون',
    'سبعون',
    'ثمانون',
    'تسعون',
    'مئة',
    'مئتان',
    'مئتا',
    'الف',
    'الاف',
    'الفان',
    'مليون',
    'مليونان',
    'ملايين',
    'و',
    'فقط',
    'لا',
    'غير'
]


def word_of_min_distance(text, dictionary):
    min_distance = sys.maxsize
    best_word = None
    for word in dictionary:
        distance = textdistance.damerau_levenshtein(text, word)
        if distance < min_distance:
            min_distance = distance
            best_word = word
    return best_word, min_distance


def find_non_overlapping_start_end_pairs_with_min_cost(text,
                                                       dictionary,
                                                       segments_distances_matrix,
                                                       path_dict,
                                                       start_index=0):
    max_index = len(text)
    min_cost = sys.maxsize
    best_start_end_pairs = None

    if start_index == max_index:
        return 0, []

    for start in range(start_index, max_index):
        for end in range(start + 1, max_index + 1):
            if end in path_dict:
                cost, start_end_pairs = path_dict[end]
            else:
                cost, start_end_pairs = find_non_overlapping_start_end_pairs_with_min_cost(text,
                                                                                           dictionary,
                                                                                           segments_distances_matrix,
                                                                                           path_dict,
                                                                                           end)
                path_dict[end] = (cost, start_end_pairs)

            min_distance = segments_distances_matrix[start, end]
            ignored_characters = start - start_index
            cost = ignored_characters + min_distance + cost
            other_cost = ignored_characters + min_distance + max_index - end

            if other_cost < min_cost:
                min_cost = cost
                best_start_end_pairs = [(start, end)]

            if cost < min_cost:
                min_cost = cost
                best_start_end_pairs = [(start, end)] + start_end_pairs

    return min_cost, best_start_end_pairs


def correct_text(text, dictionary):
    max_index = len(text) + 1
    segments_distances_matrix = np.zeros((max_index, max_index), np.int64)
    best_words_matrix = []
    for i in range(max_index):
        best_words_matrix.append([])
        for j in range(max_index):
            best_words_matrix[i].append('')

    max_int64 = np.iinfo(np.int64).max
    for start in range(0, max_index):
        for end in range(0, max_index):
            if end <= start:
                segments_distances_matrix[start, end] = max_int64
                continue

            segment = text[start:end]
            best_word, min_distance = word_of_min_distance(segment, dictionary)
            segments_distances_matrix[start, end] = min_distance
            best_words_matrix[start][end] = best_word

    path_dict = dict()
    min_cost, best_start_end_pairs = find_non_overlapping_start_end_pairs_with_min_cost(text,
                                                                                        dictionary,
                                                                                        segments_distances_matrix,
                                                                                        path_dict)
    words = []
    for start, end in best_start_end_pairs:
        words.append(best_words_matrix[start][end])
    return words


def correct_text_dp(text, dictionary):
    max_index = len(text) + 1
    segments_distances_matrix = np.zeros((max_index, max_index), np.int64)
    best_words_matrix = []
    for i in range(max_index):
        best_words_matrix.append([])
        for j in range(max_index):
            best_words_matrix[i].append('')

    max_int64 = np.iinfo(np.int64).max
    for start in range(0, max_index):
        for end in range(0, max_index):
            if end <= start:
                segments_distances_matrix[start, end] = max_int64
                continue

            segment = text[start:end]
            best_word, min_distance = word_of_min_distance(segment, dictionary)
            segments_distances_matrix[start, end] = min_distance
            best_words_matrix[start][end] = best_word

    best_start_end_pairs = []

    start_min_end_cache = dict()
    for i in range(max_index):
        section = segments_distances_matrix[i, i + 1:]
        start_min_end_cache[i] = max_int64 if section.shape[0] == 0 else np.argmin(section)

    previous_end = 0
    while previous_end < max_index:
        min_cost = sys.maxsize
        best_start_end_pair = None
        for i in range(previous_end, max_index - 1):
            j = start_min_end_cache[i]
            cost = i - previous_end + segments_distances_matrix[i, j]
            if cost < min_cost:
                min_cost = cost
                best_start_end_pair = (i, j)
        if best_start_end_pair is None:
            break
        i, j = best_start_end_pair
        other_cost = max_index - 1 - previous_end
        if other_cost < min_cost:
            break
        best_start_end_pairs.append(best_start_end_pair)
        previous_end = j

    words = []
    for start, end in best_start_end_pairs:
        words.append(best_words_matrix[start][end])
    return words


def post_process(text, language='en'):
    if language == 'en':
        text = text.replace('-', ' ').replace('_', ' ').replace(',', ' ')
        text = ''.join([c for c in text if c in 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ '])
        text = text.lower().strip()
        dictionary = ENGLISH_AMOUNTS_DICTIONARY
    elif language == 'ar':
        text = text.replace('-', ' ').replace('،', ' ')
        text = text.replace(' و', ' و ').replace('أ', 'ا').replace('آ', 'ا').replace('إ', 'ا')
        text = ''.join([c for c in text if c in 'ا ب ت ث ج ح خ د ذ ر ز س ش ص ض ط ظ ع غ ف ق ك ل م ن ه و ي ئ ة'])
        text = text.replace('مائة', ' مئة ')
        text = text.replace('مئتان', ' مئتان ')
        text = text.strip()
        dictionary = ARABIC_AMOUNTS_DICTIONARY
    else:
        raise Exception(f'Dictionary not defined for language: {language}.')

    words = text.split(' ')
    new_words = []
    for word in words:
        if word.strip() == '':
            continue

        closest_words = correct_text(word, dictionary)
        new_words.extend(closest_words)

    return ' '.join(new_words)


def main():
    start_time = time.time()
    for i in range(100):
        print(' '.join(correct_text_dp('onertwothreeyfourfivesixseven eight', ENGLISH_AMOUNTS_DICTIONARY)))
    end_time = time.time()
    print((end_time - start_time) / 100)


if __name__ == '__main__':
    main()
