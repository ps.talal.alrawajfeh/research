#include <iostream>
#include <Eigen/Dense>
#include <vector>
#include <tuple>
#include <random>
#include <cmath>
#include <chrono>

using Eigen::MatrixXd;
using Eigen::VectorXd;
using Eigen::ColPivHouseholderQR;
using Eigen::BDCSVD;

MatrixXd computeMseLossDerivativeCoefficientsMatrix(MatrixXd &designMatrix, double l2Regularization = 0.0) {
    long numberOfParameters = designMatrix.cols();
    MatrixXd coefficientsMatrix(numberOfParameters, numberOfParameters);
    long n = designMatrix.rows();
    auto designMatrixData = designMatrix.data();
    auto coefficientsMatrixData = coefficientsMatrix.data();

    long columnIndex1 = 0;
    for (long col = 0; col < numberOfParameters; col++) {
        long columnIndex2 = 0;
        for (long row = 0; row < numberOfParameters; row++) {
            double sum = 0.0;

            auto ptr1 = designMatrixData + columnIndex1;
            auto ptr2 = designMatrixData + columnIndex2;

            for (long i = 0; i < n; i++) {
                sum += *ptr1 * *ptr2;
                ptr1++;
                ptr2++;
            }

            if (row == col) {
                sum += static_cast<double>(n) * l2Regularization;
            }

            *coefficientsMatrixData = sum;
            coefficientsMatrixData++;
            columnIndex2 += n;
        }

        columnIndex1 += n;
    }

    return coefficientsMatrix;
}

VectorXd computeMseLossDerivativeConstantsVector(MatrixXd &designMatrix, VectorXd &y) {
    long numberOfParameters = designMatrix.cols();
    VectorXd constantsVector(numberOfParameters);
    auto constantsVectorData = constantsVector.data();
    long n = designMatrix.rows();
    auto yData = y.data();
    auto designMatrixData = designMatrix.data();

    long designMatrixDataIndex = 0;
    for (long col = 0; col < numberOfParameters; col++) {
        double sum = 0.0;

        auto designMatrixPtr = designMatrixData + designMatrixDataIndex;
        auto yDataPtr = yData;
        for (long i = 0; i < n; i++) {
            sum += *yDataPtr * *designMatrixPtr;
            yDataPtr++;
            designMatrixPtr++;
        }

        *constantsVectorData = sum;
        constantsVectorData++;
        designMatrixDataIndex += n;
    }

    return constantsVector;
}

std::tuple<bool, VectorXd> solveExactLeastSquares(MatrixXd &designMatrix, VectorXd &y, double l2Regularization = 0.0) {
    auto coefficientsMatrix = computeMseLossDerivativeCoefficientsMatrix(designMatrix, l2Regularization);
    if (abs(coefficientsMatrix.determinant()) < std::numeric_limits<double>::epsilon()) {
        return std::make_tuple(false, VectorXd(0));
    }

    auto constantsVector = computeMseLossDerivativeConstantsVector(designMatrix, y);
    auto solution = coefficientsMatrix.inverse() * constantsVector;
    return std::make_tuple(true, solution);
}

VectorXd solveLeastSquares(MatrixXd &designMatrix, VectorXd &y) {
    auto result = solveExactLeastSquares(designMatrix, y);
    if (std::get<0>(result)) {
        VectorXd solution = std::get<1>(result);
        return solution;
    }
    return designMatrix.bdcSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(y);
}

std::tuple<double, double> fitLinearFunction(std::vector<double> &timeSeries) {
    auto n = timeSeries.size();
    MatrixXd designMatrix(n, 2);
    auto designMatrixData = designMatrix.data();

    for (long i = 0; i < n; i++) {
        *designMatrixData = static_cast<double>(i);
        designMatrixData++;
    }

    for (long i = 0; i < n; i++) {
        *designMatrixData = 1.0;
        designMatrixData++;
    }

    VectorXd y(n);
    auto yData = y.data();
    auto timeSeriesData = timeSeries.data();

    for (long i = 0; i < n; i++) {
        *yData = *timeSeriesData;
        yData++;
        timeSeriesData++;
    }

    auto solution = solveLeastSquares(designMatrix, y);
    return std::make_tuple(solution[0], solution[1]);
}

double sampleAutoCovariance(std::vector<double> &timeSeries, long lag) {
    long n = std::abs(lag);
    unsigned long count = timeSeries.size();

    double mean = 0.0;
    for (double value: timeSeries) {
        mean += value;
    }

    auto countDouble = static_cast<double>(count);
    mean /= countDouble;

    double sum = 0.0;
    for (long i = 0; i < count - n; i++) {
        sum += (timeSeries[n + i] - mean) * (timeSeries[i] - mean);
    }

    return sum / countDouble;
}

unsigned long findSeasonalityPeriod(std::vector<double> &timeSeries) {
    unsigned long count = timeSeries.size();

    auto trendParameters = fitLinearFunction(timeSeries);
    auto trendSlope = std::get<0>(trendParameters);
    auto trendIntercept = std::get<1>(trendParameters);
    std::vector<double> deTrendedSeries;

    for (long i = 0; i < count; i++) {
        auto value = timeSeries[i];
        auto trend = trendSlope * static_cast<double>(i) + trendIntercept;
        deTrendedSeries.push_back(value - trend);
    }

    unsigned long maxPeriod = std::min<unsigned long>(365, count / 2);

    std::vector<double> acfValues;
    double zeroLagAutoCovariance = sampleAutoCovariance(timeSeries, 0);
    for (long i = 0; i <= maxPeriod; i++) {
        acfValues.push_back(sampleAutoCovariance(deTrendedSeries, i) / zeroLagAutoCovariance);
    }

    auto acfValuesCount = maxPeriod + 1;
    auto countDouble = static_cast<double>(acfValuesCount);

    std::vector<double> mappedAcfValues;
    double maxValue = std::numeric_limits<double>::min();
    long maxValueIndex = -1;
    for (long i = 1; i <= acfValuesCount; i++) {
        auto value = std::pow(static_cast<double>(i) / countDouble, 1.0 / 4.0);
        value *= acfValues[i - 1];
        if (i > 1 && maxValue < value) {
            maxValue = value;
            maxValueIndex = i - 1;
        }
        mappedAcfValues.push_back(value);
    }

    if (maxValueIndex == acfValuesCount - 1) {
        return acfValuesCount - 1;
    }

    double maxMappedValue = std::numeric_limits<double>::min();
    long maxMappedValueIndex = -1;

    for (int i = 1; i < acfValuesCount - 1; i++) {
        auto currentValue = mappedAcfValues[i];
        if (currentValue <= std::max(mappedAcfValues[i - 1], mappedAcfValues[i + 1]) && currentValue > 0) {
            continue;
        }

        if (maxMappedValue < currentValue) {
            maxMappedValue = currentValue;
            maxMappedValueIndex = i;
        }
    }

    auto bestPeriod = maxMappedValueIndex;
    if (bestPeriod == 0) {
        bestPeriod = maxValueIndex;
    }

    return bestPeriod;
}

class FourierSeriesModel {
private:
    long period;
    std::vector<double> coefficients;
public:
    FourierSeriesModel(long period, std::vector<double> &coefficients) {
        this->period = period;
        this->coefficients = coefficients;
    }

    double forecast(long timeIndex) {
        auto timeIndexDouble = static_cast<double>(timeIndex);
        double angle = 2.0 * M_PI * timeIndexDouble / static_cast<double>(this->period);

        double result = coefficients[0] * timeIndexDouble + coefficients[1];
        long fourierCoefficients = (static_cast<long>(coefficients.size()) - 2) / 2;

        for (int j = 0; j < fourierCoefficients; j++) {
            result += coefficients[2 + j] * sin(angle * (j + 1));
        }

        for (int j = 0; j < fourierCoefficients; j++) {
            result += coefficients[2 + fourierCoefficients + j] * cos(angle * (j + 1));
        }

        return result;
    }
};

std::tuple<double, FourierSeriesModel, bool> fitFourierSeriesModelWithLinearTrend(std::vector<double> &timeSeries,
                                                                                  double periodTolerance = 0.0) {
    auto seasonality = findSeasonalityPeriod(timeSeries);
    auto seasonalityDouble = static_cast<double>(seasonality);

    auto startPeriod = static_cast<long>(floor(seasonalityDouble * (1.0 - periodTolerance)));
    auto endPeriod = static_cast<long>(ceil(seasonalityDouble * (1 + periodTolerance)));

    double minError = std::numeric_limits<double>::max();
    long bestPeriod = 0;
    VectorXd bestSolution;
    bool foundSolution = false;

    unsigned long count = timeSeries.size();
    VectorXd y(count);
    auto yData = y.data();
    for (long i = 0; i < count; i++) {
        *yData = timeSeries[i];
        yData++;
    }

    for (long period = startPeriod; period <= endPeriod; period++) {
        auto periodDouble = static_cast<double>(period);
        for (long numberOfCoefficients = 1; numberOfCoefficients <= period; numberOfCoefficients++) {
            MatrixXd designMatrix(count, 2 * numberOfCoefficients + 2);

            auto designMatrixData = designMatrix.data();

            for (long i = 0; i < count; i++) {
                *designMatrixData = static_cast<double>(i);
                designMatrixData++;
            }

            for (long i = 0; i < count; i++) {
                *designMatrixData = 1.0;
                designMatrixData++;
            }

            for (long j = 1; j <= numberOfCoefficients; j++) {
                auto jDouble = static_cast<double>(j);
                for (long i = 0; i < count; i++) {
                    *designMatrixData = sin(2.0 * M_PI * static_cast<double>(i) * jDouble / periodDouble);
                    designMatrixData++;
                }
            }

            for (long j = 1; j <= numberOfCoefficients; j++) {
                auto jDouble = static_cast<double>(j);
                for (long i = 0; i < count; i++) {
                    *designMatrixData = cos(2.0 * M_PI * static_cast<double>(i) * jDouble / periodDouble);
                    designMatrixData++;
                }
            }

            auto solution = solveLeastSquares(designMatrix, y);
            if (solution.size() == 0) {
                continue;
            }
            double error = (designMatrix * solution - y).array().square().sum();

            if (error < minError) {
                minError = error;
                bestPeriod = period;
                bestSolution = solution;
                foundSolution = true;
            }
        }
    }

    if (!foundSolution) {
        std::vector<double> coefficients;
        return std::make_tuple(std::numeric_limits<double>::max(), FourierSeriesModel(-1, coefficients), false);
    }

    std::vector<double> bestCoefficients;
    for (long i = 0; i < bestSolution.rows(); i++) {
        bestCoefficients.push_back(bestSolution[i]);
    }

    return std::make_tuple(minError, FourierSeriesModel(bestPeriod, bestCoefficients), true);
}

std::vector<double> forecastNextValues(std::vector<double> &values, long steps, double alpha=0.75) {
    auto result = fitFourierSeriesModelWithLinearTrend(values);
    auto historyLength = static_cast<long>(values.size());
    std::vector<double> nextValues;

    if (std::get<2>(result)) {
        auto model = std::get<1>(result);

        for(long i = 0; i < steps; i++) {
            nextValues.push_back(model.forecast(historyLength + i));
        }

        return nextValues;
    }

    auto currentAverage = values[0];
    for (long j = 1; j < historyLength; j++) {
        currentAverage = (1.0 - alpha) * currentAverage + alpha * values[j];
    }

    auto lastValue = values[historyLength - 1];
    nextValues.push_back(currentAverage);
    for(long j = 0; j < steps - 1; j++) {
        currentAverage = (1.0 - alpha) * currentAverage + alpha * lastValue;
        nextValues.push_back(currentAverage);
    }

    return nextValues;
}

long randomInteger(long a, long b) {
    return random() % (b - a + 1) + a;
}

double randomDouble(double a, double b) {
    const long maxRand = 1000000L;
    return a + (b - a) * static_cast<double>(random() % maxRand) / static_cast<double>(maxRand);
}

int main() {
//    MatrixXd m(4, 2);
//    m(0, 0) = 1;
//    m(1, 0) = 2;
//    m(2, 0) = 3;
//    m(3, 0) = 4;
//    m(0, 1) = 5;
//    m(1, 1) = 6;
//    m(2, 1) = 7;
//    m(3, 1) = 8;
//
//    std::cout << m << std::endl;

//    std::vector<double> values = {3.0, 5.0, 7.0, 9.0, 11.0, 13.0, 15.0, 17.0, 19.0, 21.0};
//    auto result = fitLinearFunction(values);
//    std::cout << std::get<0>(result) << std::endl;
//    std::cout << std::get<1>(result) << std::endl;
//     std::vector<double> timeSeries = { 1.1, 2.05, 4.96, 1.02, 2.11, 5.03, 0.9, 2.0, 5.0, 1.0, 2.01, 5.0 };
//std::cout << findSeasonalityPeriod(timeSeries) << std::endl;
    auto start = std::chrono::system_clock::now();

    int patternMaxLength = 20;
    int patternMaxValue = 30;
    int timeSeriesLength = 100;
    double noiseRatio = 0.05;

    long n = 0;
    double totalError = 0.0;
    for (long i = 0; i < 1000; i++) {
        auto patternLength = randomInteger(1, patternMaxLength);
        std::vector<double> pattern;
        for (long j = 0; j < patternLength; j++) {
            pattern.push_back(static_cast<double>(randomInteger(1, patternMaxValue)));
        }

        auto repetitions = timeSeriesLength / patternLength;
        std::vector<double> timeSeries;
        for (long j = 0; j < repetitions; j++) {
            int maxK = patternLength;
//            if (j == repetitions - 1) {
//                maxK = randomInteger(1, patternLength);
//            }
            for (int k = 0; k < maxK; k++) {
                auto value = pattern[k] * (1 + randomDouble(-noiseRatio, noiseRatio));
                timeSeries.push_back(value);
            }
        }

        auto trainingDataSize = timeSeries.size() / 2;
        std::vector<double> trainingTimeSeries(timeSeries.cbegin(), timeSeries.cbegin() + trainingDataSize);
        auto validationDataSize = timeSeries.size() - trainingDataSize;

        auto forecasts = forecastNextValues(trainingTimeSeries, validationDataSize);

        long error = 0;
        for (long j = 0; j < validationDataSize; j++) {
            auto forecasted = forecasts[j];
            auto actual = timeSeries[trainingDataSize + j];
//            std::cout << forecasted << ", " << actual << std::endl;
            if (abs(actual) < 1e-7) {
                if (abs(forecasted - actual) > 0.2) {
                    error++;
                }
            } else if (abs((forecasted - actual) / actual) > 0.2) {
                error++;
            }
        }
        totalError += static_cast<double>(error) / static_cast<double>(validationDataSize);
        n++;

//        std::cout << "pattern:" << " (" << patternLength << ")" << std::endl;
//        for (int k = 0; k < patternLength; k++) {
//            std::cout << pattern[k];
//            if (k < patternLength - 1) {
//                std::cout << ", ";
//            }
//        }
//        std::cout << std::endl;

//        std::cout << "timeSeries:" << " (" << timeSeries.size() << ")" << std::endl;
//        for (int k = 0; k < timeSeries.size(); k++) {
//            std::cout << timeSeries[k];
//            if (k < timeSeries.size() - 1) {
//                std::cout << ", ";
//            }
//        }
//        std::cout << std::endl;

//        unsigned long inferredPeriod = findSeasonalityPeriod(timeSeries);
//        std::cout << "inferred pattern length: " << inferredPeriod << std::endl;
//        std::cout << "-------------------------------------" << std::endl;

//        if (inferredPeriod < patternLength || inferredPeriod % patternLength != 0) {
//            totalError += 1.0;
//        }
//        n++;
    }

    std::cout << "error: " << totalError / static_cast<double>(n) << std::endl;
    auto end = std::chrono::system_clock::now();
    std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count() << " ms" << std::endl;

//    Eigen::MatrixXf A = Eigen::MatrixXf::Random(3, 2);
//    Eigen::VectorXf b = Eigen::VectorXf::Random(3);
//    std::cout << "The solution using the QR decomposition is:\n"
//         << A.colPivHouseholderQr().solve(b) << std::endl;
//    double *matrixData = m.data();
//
//    for (int i = 0; i < 4; i++) {
//        std::cout << *matrixData << std::endl;
//        matrixData++;
//    }

    return 0;
}
