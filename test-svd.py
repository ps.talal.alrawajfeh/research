import numpy as np


def main1():
    x = np.array([[1.0, 0.0, 0.0], [1.0, 1.0, 0.0], [0.0, 0.0, 0.0]])

    c = np.matmul(np.transpose(x), x)

    c_pow = c
    n = 20
    for i in range(n):
        c_pow = np.matmul(c_pow, c)

    print(c_pow)

    v1 = c_pow[:, 0]
    v1_norm = v1 / np.sqrt(np.sum(np.square(v1)))
    print(v1_norm)

    sigma = v1 / (v1_norm * v1_norm[0])
    print(np.power(np.sqrt(sigma), 1.0 / (n + 1)))


def frobenius_norm(matrix):
    return np.sqrt(np.sum(np.square(matrix)))


def get_first_singular_value_and_vector(hermitian_matrix, iterations=100):
    c = hermitian_matrix
    c_square = c @ c

    c_pow = c_square
    for _ in range(iterations):
        c_pow = (c_pow @ c) / frobenius_norm(c_pow)

    v1 = c_pow[:, 0]
    v1_norm = v1 / frobenius_norm(v1)

    i = 0
    while i < len(v1):
        if abs(v1[i]) > 0:
            break
        i += 1

    sigma_1 = v1[i] / (v1_norm[i] * v1_norm[0])
    sigma_1 = np.sqrt(sigma_1)
    return sigma_1, v1_norm


def main2():
    x = np.array([[1.0, 0.0, 0.0], [1.0, 1.0, 0.0], [0.0, 0.0, 0.0]])

    c = np.matmul(np.transpose(x), x)

    sigma_1, v1_norm = get_first_singular_value_and_vector(c)

    singular_values = [sigma_1]
    singular_vectors = [v1_norm]

    current_c = c - sigma_1 ** 2 * (v1_norm.reshape(-1, 1) @ v1_norm.reshape(1, -1))

    for i in range(1, x.shape[0]):
        sigma_i, vi_norm = get_first_singular_value_and_vector(current_c)
        singular_values.append(sigma_i)
        singular_vectors.append(vi_norm)

        current_c = current_c - sigma_i ** 2 * (vi_norm.reshape(-1, 1) @ vi_norm.reshape(1, -1))
    
    print(singular_values)
    U, S, Vh = np.linalg.svd(x, full_matrices=True)
    print(S)


if __name__ == "__main__":
    main2()
