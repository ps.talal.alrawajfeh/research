import math
import os

import cv2
import numpy as np
import pytesseract
from matplotlib import pyplot as plt

MICR_AREA_HEIGHT_PERCENTAGE = 0.22
POSITIONAL_BIAS_WEIGHT = 5.0
POSITIONAL_BIAS_DIMINISHING_FACTOR = 0.3
MICR_LOCATION_FUZZY_RESPONSE_THRESHOLD = 0.25
MICR_DETECTION_VERTICAL_SAFETY_MARGIN_SIZE = 20
MICR_EXTRACTION_MARGIN_SIZE = 10
MICR_DILATION_FILTER_SIZE = (1, 7)


def view_image(image, title=''):
    plt.imshow(image, cmap=plt.cm.gray)
    plt.title(title)
    plt.show()


def to_int(number):
    return int(math.ceil(number))


def bell_shaped_density(mean, std, x):
    return math.exp(-0.5 * ((x - mean) / std) ** 2)


def crop_foreground(image):
    mask = image > 0
    height, width = image.shape
    width_mask, height_mask = mask.any(0), mask.any(1)
    x1, x2 = width_mask.argmax(), width - width_mask[::-1].argmax()
    y1, y2 = height_mask.argmax(), height - height_mask[::-1].argmax()
    return image[y1:y2, x1:x2]


def remove_black_border(image):
    column_max = np.max(image.T, axis=-1)
    x1 = np.argmin(column_max)
    x2 = image.shape[1] - np.argmin(column_max[::-1])
    return image[0:image.shape[0], x1:x2], (x1, x2)


def adjust_value_to_range(value, min_range, max_range):
    if value < min_range:
        return min_range
    if value > max_range:
        return max_range
    return value


def otsu_threshold(image):
    return cv2.threshold(image, 127, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]


def get_connected_region(image, start_point, visits_matrix):
    if image[start_point[1], start_point[0]] == 0:
        return []

    region = [start_point]
    queue = [start_point]

    while len(queue) > 0:
        parent = queue.pop(0)
        visits_matrix[parent[1], parent[0]] = 1

        for dy in range(-1, 2):
            for dx in range(-1, 2):
                x, y = parent
                x += dx
                y += dy
                if x < 0 or x >= image.shape[1]:
                    continue
                if y < 0 or y >= image.shape[0]:
                    continue
                if visits_matrix[y, x] == 1:
                    continue
                if image[y, x] == 255 and (x, y) not in queue:
                    queue.append((x, y))
                    region.append((x, y))

    return region


def get_connected_regions(image):
    visits_matrix = np.zeros(image.shape, np.uint8)

    regions = []
    for y in range(image.shape[0]):
        for x in range(image.shape[1]):
            if image[y, x] == 255 and visits_matrix[y, x] == 0:
                regions.append(get_connected_region(image, (x, y), visits_matrix))

    return regions


def find_largest_region(regions):
    largest_size = -1
    largest_region = None
    for r in regions:
        r_np = np.array(r)
        r_x1 = np.min(r_np[:, :1])
        r_y1 = np.min(r_np[:, 1:])
        r_x2 = np.max(r_np[:, :1])
        r_y2 = np.max(r_np[:, 1:])

        size = (r_x2 - r_x1) * (r_y2 - r_y1)
        if size > largest_size:
            largest_size = size
            largest_region = [r_x1, r_y1, r_x2, r_y2]
    return largest_region


def smear_micr(binarized_image):
    return cv2.dilate(binarized_image, np.ones(MICR_DILATION_FILTER_SIZE), iterations=5)


def extract_micr(cheque_image):
    micr_area_height = to_int(cheque_image.shape[0] * MICR_AREA_HEIGHT_PERCENTAGE)
    micr_area = cheque_image[-micr_area_height:, :]

    height = micr_area.shape[0]

    y1, y2 = find_micr_vertical_location(micr_area)

    y1 = adjust_value_to_range(y1 - MICR_DETECTION_VERTICAL_SAFETY_MARGIN_SIZE, 0, height)
    y2 = adjust_value_to_range(y2 + MICR_DETECTION_VERTICAL_SAFETY_MARGIN_SIZE, 0, height)

    micr_area = micr_area[y1:y2, :]
    binarized = 255 - otsu_threshold(micr_area)
    binarized, (x1, x2) = remove_black_border(binarized)
    binarized = smear_micr(binarized)

    regions = get_connected_regions(binarized)
    largest_region = find_largest_region(regions)

    r_x1, r_y1, r_x2, r_y2 = largest_region
    r_x1 = adjust_value_to_range(r_x1 - MICR_EXTRACTION_MARGIN_SIZE, 0, binarized.shape[1])
    r_y1 = adjust_value_to_range(r_y1 - MICR_EXTRACTION_MARGIN_SIZE, 0, binarized.shape[0])
    r_x2 = adjust_value_to_range(r_x2 + MICR_EXTRACTION_MARGIN_SIZE, 0, binarized.shape[1])
    r_y2 = adjust_value_to_range(r_y2 + MICR_EXTRACTION_MARGIN_SIZE, 0, binarized.shape[0])

    micr_area = micr_area[:, x1:x2]
    return micr_area[r_y1: r_y2, r_x1: r_x2]


def find_micr_vertical_location(micr_area):
    height = micr_area.shape[0]
    horizontal_means = np.mean(micr_area, axis=-1)
    horizontal_stds = np.std(micr_area, axis=-1)
    center = height // 2

    positional_bias = [bell_shaped_density(center,
                                           center * POSITIONAL_BIAS_DIMINISHING_FACTOR,
                                           x) * POSITIONAL_BIAS_WEIGHT
                       for x in range(height)]

    inverted_mean_variance_fuzzy_conjunction = (255 - horizontal_means) * horizontal_stds ** 2
    fuzzy_responses = inverted_mean_variance_fuzzy_conjunction * positional_bias
    max_fuzzy_response = np.max(fuzzy_responses)
    normalized_responses = fuzzy_responses / max_fuzzy_response
    chosen_responses_mask = np.array(normalized_responses > MICR_LOCATION_FUZZY_RESPONSE_THRESHOLD, np.uint8)

    return chosen_responses_mask.argmax(), height - chosen_responses_mask[::-1].argmax()


def main():
    base_dir = '/home/u764/Development/progressoft/asv-datasets/qnb_cheques/all/'

    for f in os.listdir(base_dir):
        print(base_dir + f)
        extracted = extract_micr(cv2.imread(base_dir + f, cv2.IMREAD_GRAYSCALE))
        view_image(extracted,
                   pytesseract.image_to_string(extracted,
                                               config='-l mcr --oem 1 --psm 13 -c load_system_dawg=0').strip())


if __name__ == '__main__':
    main()

