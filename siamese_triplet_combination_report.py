#!/usr/bin/python3.6

import numpy as np
import pickle


def load_object(file):
    with open(file, 'rb') as f:
        return pickle.load(f)


def calculate_false_acceptance_false_rejection_rates(outputs, labels):
    forgery_indices = [i for i in range(
        len(labels)) if labels[i] == 0]
    expected_forgery_outputs = outputs[forgery_indices]

    genuine_indices = [i for i in range(
        len(labels)) if labels[i] == 1]
    expected_genuine_outputs = outputs[genuine_indices]

    for threshold in [float(i) for i in range(0, 101)]:
        false_acceptances = np.sum(
            np.array(expected_forgery_outputs >= threshold, np.uint8))
        false_acceptance_rate = false_acceptances / len(forgery_indices)

        false_rejections = np.sum(
            np.array(expected_genuine_outputs < threshold, np.uint8))
        false_rejection_rate = false_rejections / len(genuine_indices)

        false_acceptance_rate = float(np.round(false_acceptance_rate * 100, 3))
        false_rejection_rate = float(np.round(false_rejection_rate * 100, 3))

        yield f'{threshold}\t{false_acceptance_rate}\t{false_rejection_rate}'


def calculate_outputs_histogram(outputs):
    hist, _ = np.histogram(outputs, np.arange(0, 101))
    verifications_100 = np.sum(np.array(outputs == 100.0, np.uint8))

    for i in range(0, 100):
        yield f'{i}\t-\t{i + 1}\t~\t{hist[i]}'

    yield f'{100}\t\t\t~\t{verifications_100}'


def main():
    test_data = np.array(load_object('test_data.pickle'), np.float32)
    test_labels = np.array(load_object('test_labels.pickle'), np.uint8)

    siamese_outputs = np.round(test_data[:, 0:1].ravel() * 100, 7)
    triplet_outputs = np.round(test_data[:, 2:3].ravel() * 100, 7)
    combination_outputs = np.round(0.5 * siamese_outputs + 0.5 * triplet_outputs, 7)

    with open('combination_report.txt', 'a') as f:
        f.write('======= siamese outputs histogram =======\n')
        for s in calculate_outputs_histogram(siamese_outputs):
            f.write(s + '\n')

        f.write('\n\n======= siamese statistics =======\nTHR\tFAR\tFRR\n')
        for s in calculate_false_acceptance_false_rejection_rates(siamese_outputs, test_labels):
            f.write(s + '\n')

        f.write('\n\n======= triplet outputs histogram =======\n')
        for s in calculate_outputs_histogram(triplet_outputs):
            f.write(s + '\n')

        f.write('\n\n======= triplet statistics =======\nTHR\tFAR\tFRR\n')
        for s in calculate_false_acceptance_false_rejection_rates(triplet_outputs, test_labels):
            f.write(s + '\n')

        f.write('\n\n======= combination outputs histogram =======\n')
        for s in calculate_outputs_histogram(combination_outputs):
            f.write(s + '\n')

        f.write('\n\n======= combination statistics =======\nTHR\tFAR\tFRR\n')
        for s in calculate_false_acceptance_false_rejection_rates(combination_outputs, test_labels):
            f.write(s + '\n')



if __name__ == "__main__":
    main()
