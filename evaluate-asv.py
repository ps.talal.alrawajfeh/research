import math
import os
import pickle
import random

import numpy as np
from matplotlib import pyplot as plt
from scipy.interpolate import interp1d
from sklearn.metrics import (accuracy_score, classification_report,
                             confusion_matrix)
from tensorflow.keras.models import load_model
from tensorflow.keras.utils import Sequence
from terminaltables import AsciiTable
from tqdm import tqdm
import cv2
import mimetypes
from tensorflow.keras.applications.densenet import DenseNet201, preprocess_input


PRE_TRAINED_MODEL_PRE_PROCESSOR = preprocess_input
PRE_TRAINED_MODEL = DenseNet201(include_top=False,
                                weights='imagenet',
                                input_shape=(224, 224, 3),
                                pooling='avg')


TEST_FEATURE_VECTOR_CLASSES_FILE = 'test_feature_vector_classes.pickle'
CLASS_NUMBER_LENGTH = 9
NUMBER_OF_IMAGES_PER_CLASS_THRESHOLD = 2
TRIPLET_MODEL_FILE_NAME = 'embedding_network.h5'
SIAMESE_MODEL_FILE_NAME = 'siamese.h5'
DENSE_MODEL_FILE_NAME = 'densenet.h5'
SIGNATURE_CLASSES_PATHS = ['/home/u764/Development/data/signatures']
EPSILON = 1e-6


def calculate_false_acceptance_rate_per_threshold(forgeries_count,
                                                  histogram_bins,
                                                  forgery_probabilities):
    false_acceptances = forgery_probabilities
    false_acceptances_histogram = np.histogram(
        false_acceptances, histogram_bins)[0]
    false_acceptance_per_threshold = np.cumsum(false_acceptances_histogram)
    false_acceptance_per_threshold = forgeries_count - false_acceptance_per_threshold
    false_acceptance_rate_per_threshold = false_acceptance_per_threshold / forgeries_count
    return false_acceptance_rate_per_threshold


def calculate_false_rejection_rate_per_threshold(genuines_count,
                                                 histogram_bins,
                                                 genuine_probabilities):
    false_rejections = genuine_probabilities
    false_rejections_histogram = np.histogram(
        false_rejections, histogram_bins)[0]
    false_rejection_per_threshold = np.cumsum(false_rejections_histogram)
    false_rejection_rate_per_threshold = false_rejection_per_threshold / genuines_count
    return false_rejection_rate_per_threshold


def append_lines_to_report(file_path, lines):
    with open(file_path, 'a') as f:
        if isinstance(lines, list):
            f.writelines([line + '\n' for line in lines])
        elif isinstance(lines, str):
            f.writelines(lines + '\n')
        else:
            f.writelines(lines)


def write_distributions_table(report_file_path,
                              false_acceptance_rate_per_threshold,
                              false_rejection_rate_per_threshold,
                              histogram_bin_size):
    table = [['Threshold', 'FA Rate', 'FR Rate']]
    for i in range(0, 100 // histogram_bin_size):
        if i == 0:
            far = 100.0
            frr = 0.0
        else:
            far = round(false_acceptance_rate_per_threshold[i - 1] * 100, 2)
            frr = round(false_rejection_rate_per_threshold[i - 1] * 100, 2)
        table.append([f'{i * histogram_bin_size}%', f'{far}%', f'{frr}%'])
    table.append([f'{100}%', f'{0.0}%', f'{100.0}%'])
    table = AsciiTable(table).table
    print(table)
    append_lines_to_report(report_file_path, table)


def write_expected_false_acceptances_table(report_file_path,
                                           false_acceptance_rate_per_threshold,
                                           histogram_bin_size):
    table = [['Threshold',
              'FA Count per 10K verification',
              'FA Count per 100K verification',
              'FA Count per 1M verification']]
    for i in range(0, 100 // histogram_bin_size):
        if i == 0:
            far = 1.0
        else:
            far = false_acceptance_rate_per_threshold[i - 1]
        table.append([f'{i * histogram_bin_size}%',
                      f'{int(far * 10000)}',
                      f'{int(far * 100000)}',
                      f'{int(far * 1000000)}'])
    table.append([f'{100}%', f'{0}', f'{0}', f'{0}'])
    table = AsciiTable(table).table
    print(table)
    append_lines_to_report(report_file_path, table)


def write_expected_false_rejections_table(report_file_path,
                                          false_rejection_rate_per_threshold,
                                          histogram_bin_size):
    table = [['Threshold',
              'FR Count per 10K verification',
              'FR Count per 100K verification',
              'FR Count per 1M verification']]
    for i in range(0, 100 // histogram_bin_size):
        if i == 0:
            frr = 0.0
        else:
            frr = false_rejection_rate_per_threshold[i - 1]
        table.append([f'{i * histogram_bin_size}%',
                      f'{int(frr * 10000)}',
                      f'{int(frr * 100000)}',
                      f'{int(frr * 1000000)}'])
    table.append([f'{100}%', f'{10000}', f'{100000}', f'{1000000}'])
    table = AsciiTable(table).table
    print(table)
    append_lines_to_report(report_file_path, table)


def plot_distributions(genuine_distribution_file_path,
                       forgery_distribution_file_path,
                       mixed_distributions_file_path,
                       genuine_probabilities,
                       forgery_probabilities,
                       histogram_bins):
    plt.hist(genuine_probabilities,
             histogram_bins,
             color='blue',
             label='genuine')
    plt.legend(loc='best')
    plt.savefig(genuine_distribution_file_path, dpi=200)
    plt.close('all')

    plt.hist(forgery_probabilities,
             histogram_bins,
             color='red',
             label='forgery')
    plt.legend(loc='best')
    plt.savefig(forgery_distribution_file_path, dpi=200)
    plt.close('all')

    plt.hist(genuine_probabilities,
             histogram_bins,
             color='blue',
             label='genuine')
    plt.legend(loc='best')
    plt.hist(forgery_probabilities,
             histogram_bins,
             color='red',
             label='forgery')
    plt.legend(loc='best')
    plt.savefig(mixed_distributions_file_path, dpi=200)
    plt.close('all')


def write_classification_report(report_file_path,
                                all_true_labels,
                                all_predicted_labels):
    accuracy = accuracy_score(1 - all_true_labels, 1 - all_predicted_labels)
    append_lines_to_report(report_file_path,
                           f'\ntest accuracy: {round(accuracy * 100, 2)}%\n')

    append_lines_to_report(report_file_path,
                           'confusion matrix:')
    matrix = confusion_matrix(1 - all_true_labels, 1 - all_predicted_labels)
    append_lines_to_report(report_file_path,
                           f'{matrix}')

    frr = round((matrix[0, 1] / np.sum(matrix[0])) * 100, 2)
    far = round((matrix[1, 0] / np.sum(matrix[1])) * 100, 2)
    append_lines_to_report(report_file_path,
                           f'FRR: {frr}%')
    append_lines_to_report(report_file_path,
                           f'FAR: {far}%')

    append_lines_to_report(report_file_path,
                           '\nclassification report:')
    append_lines_to_report(report_file_path,
                           f'{classification_report(1 - all_true_labels, 1 - all_predicted_labels)}')


def generate_report(predictions,
                    labels,
                    histogram_bin_size,
                    report_file_path,
                    genuine_distribution_file_path,
                    forgery_distribution_file_path,
                    mixed_distributions_file_path):
    labels_mask = labels.ravel() > 0.5
    genuine_probabilities = predictions[labels_mask] * 100
    forgery_probabilities = predictions[~labels_mask] * 100
    genuines_count = np.count_nonzero(labels_mask)
    forgeries_count = np.count_nonzero(~labels_mask)

    histogram_bins = list(range(0, 101, histogram_bin_size))

    false_acceptance_rate_per_threshold = calculate_false_acceptance_rate_per_threshold(forgeries_count,
                                                                                        histogram_bins,
                                                                                        forgery_probabilities)

    false_rejection_rate_per_threshold = calculate_false_rejection_rate_per_threshold(genuines_count,
                                                                                      histogram_bins,
                                                                                      genuine_probabilities)

    write_classification_report(report_file_path,
                                np.array(labels_mask, np.int32),
                                np.array(predictions.ravel() > 0.5, np.int32))

    write_distributions_table(report_file_path,
                              false_acceptance_rate_per_threshold,
                              false_rejection_rate_per_threshold,
                              histogram_bin_size)
    append_lines_to_report(report_file_path, '')

    write_expected_false_acceptances_table(report_file_path,
                                           false_acceptance_rate_per_threshold,
                                           histogram_bin_size)
    append_lines_to_report(report_file_path, '')

    write_expected_false_rejections_table(report_file_path,
                                          false_rejection_rate_per_threshold,
                                          histogram_bin_size)

    plot_distributions(genuine_distribution_file_path,
                       forgery_distribution_file_path,
                       mixed_distributions_file_path,
                       genuine_probabilities,
                       forgery_probabilities,
                       histogram_bins)


class BatchVerification:
    def __init__(self, verification_lambda, batch_size=512):
        self.batch_size = batch_size
        self.verification_lambda = verification_lambda

        self.input_batch1 = []
        self.input_batch2 = []
        self.count = 0
        self.predictions = []
        self.classes = []
        self.signature_ids = []

    def __predict(self):
        self.predictions.extend(self.verification_lambda(
            self.input_batch1, self.input_batch2))
        self.input_batch1 = []
        self.input_batch2 = []
        self.count = 0

    def feed(self, signature_id, other_class, item1, item2):
        self.input_batch1.append(item1)
        self.input_batch2.append(item2)
        self.signature_ids.append(signature_id)
        self.classes.append(other_class)
        self.count += 1

        if self.count == self.batch_size:
            self.__predict()

    def finalize(self):
        self.__predict()

    def get_predictions(self):
        predictions_dict = dict()
        for i in range(len(self.signature_ids)):
            signature_id = self.signature_ids[i]
            if signature_id not in predictions_dict:
                predictions_dict[signature_id] = []
            predictions_dict[signature_id].append(
                (self.classes[i], self.predictions[i]))
        return predictions_dict


def simulate_verification(feature_vector_classes,
                          verification_lambda,
                          signatories=10,
                          max_signatures_per_signatory=5,
                          number_of_signatures_to_verify=1,
                          number_of_verifications=1000,
                          verification_threshold=0.7):
    fa_count = 0
    fr_count = 0
    coverage = 0

    for i in range(number_of_verifications):
        keys = [*feature_vector_classes]
        chosen_classes = random.sample(keys, signatories)

        chosen_signatures_to_verify_classes = random.sample(
            chosen_classes, number_of_signatures_to_verify)
        chosen_signatures = []
        for j in range(number_of_signatures_to_verify):
            chosen_signature_to_verify_class = chosen_signatures_to_verify_classes[j]
            n = len(feature_vector_classes[chosen_signature_to_verify_class])
            chosen_signatures.append([chosen_signature_to_verify_class,
                                      random.randint(0, n - 1)])

        batch_verification = BatchVerification(verification_lambda)
        for signature_class, signature_index in chosen_signatures:
            feature_vector = feature_vector_classes[signature_class][signature_index]
            for c in chosen_classes:
                n = len(feature_vector_classes[c])
                feature_vectors_indices = list(range(n))
                chosen_feature_vectors_indices = random.sample(feature_vectors_indices,
                                                               min(max_signatures_per_signatory + 1, n))
                if c == signature_class and signature_index in chosen_feature_vectors_indices:
                    chosen_feature_vectors_indices.remove(signature_index)
                if len(chosen_feature_vectors_indices) > max_signatures_per_signatory:
                    chosen_feature_vectors_indices.pop(0)

                feature_vectors = feature_vector_classes[c]
                for j in feature_vectors_indices:
                    batch_verification.feed((signature_class, signature_index),
                                            c,
                                            feature_vector,
                                            feature_vectors[j])
        batch_verification.finalize()

        correct_verifications = 0
        predictions_dict = batch_verification.get_predictions()
        for signature_id in predictions_dict:
            signature_verifications = predictions_dict[signature_id]
            highest_probability = 0.0
            highest_probability_class = None
            for c, probability in signature_verifications:
                if probability > highest_probability:
                    highest_probability = probability
                    highest_probability_class = c

            if highest_probability >= verification_threshold:
                if highest_probability_class != signature_id[0]:
                    fa_count += 1
                else:
                    correct_verifications += 1
            else:
                fr_count += 1

        if correct_verifications == number_of_signatures_to_verify:
            coverage += 1

    coverage /= number_of_verifications
    return coverage, fa_count, fr_count


def generate_verification_simulation_report(feature_vector_classes,
                                            verification_lambda,
                                            report_file_path):
    verification_threshold = 70
    number_of_verifications = 1000
    max_signatures_per_signatory = 5

    append_lines_to_report(report_file_path, '\n[verification simulation]')
    append_lines_to_report(
        report_file_path, f'threshold: {verification_threshold}%')
    append_lines_to_report(
        report_file_path, f'number of simulations per signature: {number_of_verifications}')
    append_lines_to_report(report_file_path,
                           'note that each cell in the following table contains a tuple (coverage %, false acceptance count, false rejection count)')

    append_lines_to_report(report_file_path, '')

    table = [['Number of Signatories',
              'One Signature per Verification',
              'Two Signatures per Verification',
              'Three Signatures per Verification']]

    signatories_per_verification_list = [10, 20, 30, 40, 50]
    signatures_per_verification_list = [1, 2, 3]
    progress_bar = tqdm(total=len(signatories_per_verification_list)
                        * len(signatures_per_verification_list))
    for signatories_per_verification in signatories_per_verification_list:
        row = [f'{signatories_per_verification}']
        for signatures_per_verification in signatures_per_verification_list:
            coverage, fa_count, fr_count = simulate_verification(feature_vector_classes,
                                                                 verification_lambda,
                                                                 signatories_per_verification,
                                                                 max_signatures_per_signatory,
                                                                 signatures_per_verification,
                                                                 number_of_verifications,
                                                                 verification_threshold / 100)
            row.append(
                f'({round(coverage * 100, 2)}%, {fa_count}, {fr_count})')
            progress_bar.update()
        table.append(row)

    table = AsciiTable(table).table
    print(table)
    append_lines_to_report(report_file_path, table)


def euclidean_distance(x, y):
    squared_euclidean_distance = np.sum(np.square(np.subtract(x, y)), axis=-1)
    return np.sqrt(np.maximum(squared_euclidean_distance, 0.0))


def predict(embedding_net,
            siamese_net,
            input_batch1,
            input_batch2):
    distance_to_probability = interp1d([0.0, 0.6, 0.9, 1.3, 2.0],
                                       [1.0, 0.9, 0.5, 0.1, 0.0])
    siamese_output_mapper = interp1d([0.0, 0.7, 0.95, 1.0],
                                     [0.0, 0.5, 0.75, 1.0])

    embeddings1 = embedding_net.predict(input_batch1)
    embeddings2 = embedding_net.predict(input_batch2)
    siamese_output = siamese_net.predict([input_batch1, input_batch2])[:, 0]

    distances = euclidean_distance(embeddings1, embeddings2)
    triplet_probabilities = distance_to_probability(distances)
    siamese_mapped_output = siamese_output_mapper(siamese_output)

    triplet_probabilities[triplet_probabilities >= 1.0 - EPSILON] = 1.0
    siamese_mapped_output[siamese_mapped_output < 0.35] = 0.0

    combined_values = 0.5 * triplet_probabilities + 0.5 * siamese_mapped_output
    combined_values[combined_values < 0.35] = 0.0
    return combined_values


def shuffle_pairs(pairs):
    references, others, true_outputs = pairs
    indices = list(range(len(references)))
    random.shuffle(indices)

    shuffled_references = [references[indices[i]]
                           for i in range(len(references))]
    shuffled_others = [others[indices[i]] for i in range(len(references))]
    shuffled_true_outputs = [true_outputs[indices[i]]
                             for i in range(len(references))]

    return shuffled_references, shuffled_others, shuffled_true_outputs


def to_float_array(np_array):
    return np.array(np_array, np.float32)


def generate_pairs(feature_vector_classes,
                   references_per_class=14,
                   forgery_classes_per_reference=4,
                   forgeries_per_forgery_class=4):
    keys = [*feature_vector_classes]
    number_of_classes = len(keys)

    references = []
    others = []
    labels = []

    for c in range(number_of_classes - forgery_classes_per_reference):
        c = random.randint(0, len(keys) - 1)
        key = keys.pop(c)

        reference_indices = random.sample(list(range(len(feature_vector_classes[key]))),
                                          min(references_per_class, len(feature_vector_classes[key])))

        for r in reference_indices:
            reference = [key, r]
            genuines = [[key, i]
                        for i in range(len(feature_vector_classes[key]))]
            references.extend([reference] * len(genuines))
            others.extend(genuines)
            labels.extend([1] * len(genuines))

            forgery_keys = random.sample(
                keys, min(forgery_classes_per_reference, len(keys)))
            for forgery_key in forgery_keys:
                forgery_class = feature_vector_classes[forgery_key]
                forgery_indices = random.sample(list(range(len(forgery_class))),
                                                min(forgeries_per_forgery_class, len(forgery_class)))
                forgeries = [[forgery_key, f] for f in forgery_indices]

                references.extend([reference] * len(forgeries))
                others.extend(forgeries)
                labels.extend([0] * len(forgeries))

    return references, others, labels


class DataGenerator(Sequence):
    def __init__(self,
                 feature_vector_classes,
                 pairs,
                 batch_size,
                 count):
        self.batch_size = batch_size
        self.count = count
        self.feature_vector_classes = feature_vector_classes
        self.references, self.others, self.true_labels = shuffle_pairs(pairs)

    def __len__(self):
        return math.ceil(self.count / self.batch_size)

    def __getitem__(self, index):
        references, others, true_labels = [], [], []
        for i in range(index * self.batch_size,
                       min((index + 1) * self.batch_size, len(self.references))):
            reference_pair = self.references[i]
            other_pair = self.others[i]
            reference_image = self.feature_vector_classes[reference_pair[0]
                                                          ][reference_pair[1]]
            key = other_pair[0]
            other_image = self.feature_vector_classes[key][other_pair[1]]
            references.append(to_float_array(reference_image))
            others.append(to_float_array(other_image))
            true_labels.append(to_float_array(self.true_labels[i]))

        if len(references) == 0:
            return self.__getitem__(0)

        return [np.array(references), np.array(others)], np.array(true_labels)

    def on_epoch_end(self):
        self.references, self.others, self.true_labels = shuffle_pairs([self.references,
                                                                        self.others,
                                                                        self.true_labels])


def deserialize_object(file):
    with open(file, 'rb') as f:
        return pickle.load(f)


def serialize_object(obj, file):
    with open(file, 'wb') as f:
        pickle.dump(obj, f)


def get_extensions_for_type(general_type):
    for ext in mimetypes.types_map:
        if mimetypes.types_map[ext].split('/')[0] == general_type:
            yield ext.lower()


def memoize(function):
    memo = {}

    def wrapper(*args):
        if args in memo:
            return memo[args]
        else:
            rv = function(*args)
            memo[args] = rv
            return rv

    return wrapper


@memoize
def get_image_extensions():
    return tuple(get_extensions_for_type('image'))


def file_extension(path):
    return os.path.splitext(os.path.basename(path))[1].lower()


def is_image(path):
    return file_extension(path) in get_image_extensions()


def to_int(number):
    return int(round(number))


def class_from_file_name(file_name):
    return file_name[0:CLASS_NUMBER_LENGTH]


def extract_classes(iterable,
                    class_lambda,
                    item_mapper_lambda=lambda x: x):
    classes = dict()
    for item in iterable:
        c = class_lambda(item)
        if c not in classes:
            classes[c] = []
        classes[c].append(item_mapper_lambda(item))
    return classes


def extract_classes_from_file_names(paths,
                                    file_name_class_lambda=class_from_file_name):
    image_classes = dict()
    for path in paths:
        images_names = filter(lambda x: is_image(os.path.join(path, x)),
                              os.listdir(path))
        image_classes.update(extract_classes(images_names,
                                             file_name_class_lambda,
                                             lambda f: os.path.join(path, f)))
    return image_classes


def read_image_grayscale(image_path):
    return cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)


def image_classes_from_paths(image_paths):
    image_paths_classes = extract_classes_from_file_names(
        image_paths, class_from_file_name)

    image_classes = dict()
    for c in image_paths_classes:
        image_classes[c] = [read_image_grayscale(image_path)
                            for image_path in image_paths_classes[c]]
    return image_classes


def load_image_classes():
    image_classes = None
    if not os.path.isfile('image_classes.pickle'):
        image_classes = image_classes_from_paths(SIGNATURE_CLASSES_PATHS)
        serialize_object(image_classes, 'image_classes.pickle')
    if image_classes is None:
        image_classes = deserialize_object('image_classes.pickle')
    return image_classes


def threshold_otsu(image):
    return cv2.threshold(image,
                         0,
                         255,
                         cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]


def pre_process_image(image):
    return 255 - threshold_otsu(image)


def replicate_channel(image, replicas=3):
    return np.concatenate([(np.expand_dims(image, axis=-1)) for _ in range(replicas)],
                          axis=-1)


def images_to_feature_vectors(image_batch,
                              model=PRE_TRAINED_MODEL,
                              model_specific_pre_processor=PRE_TRAINED_MODEL_PRE_PROCESSOR,
                              image_pre_processor=pre_process_image):
    return model.predict(
        model_specific_pre_processor(
            np.array(
                [replicate_channel(image_pre_processor(image))
                 for image in image_batch]
            )))


class ImageBatchVectorizer:
    def __init__(self, batch_size=128):
        self.__images = []
        self.__classes = []
        self.__feature_vector_classes = dict()
        self.__batch_size = batch_size

    def __predict(self):
        if len(self.__images) == 0:
            return
        feature_vectors = images_to_feature_vectors(self.__images)
        for i in range(len(self.__classes)):
            c = self.__classes[i]
            if c not in self.__feature_vector_classes:
                self.__feature_vector_classes[c] = []
            self.__feature_vector_classes[c].append(feature_vectors[i])
        self.__images = []
        self.__classes = []

    def __predict_if_batch_size_reached(self):
        if len(self.__images) >= self.__batch_size:
            self.__predict()

    def feed_images(self, images, classes):
        for i in range(len(images)):
            self.__images.append(images[i])
            self.__classes.append(classes[i])
            self.__predict_if_batch_size_reached()

    def feed_image(self, image, image_class):
        self.__images.append(image)
        self.__classes.append(image_class)
        self.__predict_if_batch_size_reached()

    def finalize(self):
        self.__predict()

    def get_feature_vectors(self):
        return self.__feature_vector_classes


def generate_feature_vectors(image_classes, file_name):
    progress_bar = tqdm(total=len(image_classes))
    image_batch_vectorizer = ImageBatchVectorizer()
    for c in image_classes:
        batch = image_classes[c]
        image_batch_vectorizer.feed_images(batch, [c] * len(batch))
        progress_bar.update()
    image_batch_vectorizer.finalize()
    progress_bar.close()
    serialize_object(image_batch_vectorizer.get_feature_vectors(), file_name)
    return image_batch_vectorizer.get_feature_vectors()


def evaluate():
    embedding_net = load_model(TRIPLET_MODEL_FILE_NAME, compile=False)
    siamese_net = load_model(SIAMESE_MODEL_FILE_NAME, compile=False)
    dense_net = load_model(DENSE_MODEL_FILE_NAME, compile=False)

    if not os.path.isfile(TEST_FEATURE_VECTOR_CLASSES_FILE):
        image_classes = load_image_classes()
        feature_vector_classes = generate_feature_vectors(
            image_classes, TEST_FEATURE_VECTOR_CLASSES_FILE)
    else:
        feature_vector_classes = deserialize_object(
            TEST_FEATURE_VECTOR_CLASSES_FILE)

    test_pairs = generate_pairs(feature_vector_classes)
    test_data_generator = DataGenerator(feature_vector_classes,
                                        test_pairs,
                                        512,
                                        len(test_pairs[0]))

    progress_bar = tqdm(total=len(test_data_generator))

    predictions = []
    true_labels = []
    for i in range(len(test_data_generator)):
        [input_batch1, input_batch2], labels = test_data_generator[i]
        probabilities = predict(embedding_net,
                                siamese_net,
                                input_batch1,
                                input_batch2)
        true_labels.extend(labels)
        predictions.extend(probabilities)
        progress_bar.update()
    progress_bar.close()

    predictions = np.array(predictions)
    true_labels = np.array(true_labels)

    generate_report(predictions,
                    true_labels,
                    5,
                    'combined_model_report.txt',
                    'combined_model_genuine_distribution.png',
                    'combined_model_forgery_distribution.png',
                    'combined_model_mixed_distributions.png')

    generate_verification_simulation_report(feature_vector_classes,
                                            lambda x, y: predict(embedding_net,
                                                                 siamese_net,
                                                                 np.array(x),
                                                                 np.array(y)),
                                            'combined_model_report.txt')


def get_foreground_boundaries(binary_image):
    mask = (255 - binary_image) > 0

    height, width = binary_image.shape[0:2]
    mask1, mask2 = mask.any(0), mask.any(1)
    x1, x2 = mask1.argmax(), width - mask1[::-1].argmax()
    y1, y2 = mask2.argmax(), height - mask2[::-1].argmax()

    return x1, y1, x2, y2


def main1():
    output_dir = '/home/u764/Development/data/signatures-opencv'
    signatures = os.listdir(SIGNATURE_CLASSES_PATHS[0])
    signatures = [os.path.join(SIGNATURE_CLASSES_PATHS[0], x)
                  for x in signatures]
    signatures = [(x, cv2.imread(x, cv2.IMREAD_GRAYSCALE))
                  for x in signatures if is_image(x)]

    for path, signature in signatures:
        file_name = os.path.basename(path)

        signature_bin = threshold_otsu(signature)
        x1, y1, x2, y2 = get_foreground_boundaries(signature_bin)
        signature_cropped = signature[y1:y2, x1:x2]
        signature_resized = cv2.resize(
            signature_cropped, (224, 224), interpolation=cv2.INTER_CUBIC)
        signature_final = threshold_otsu(signature_resized)
        cv2.imwrite(os.path.join(output_dir, file_name.split('.')[0] + '.png'), signature_final)


def main():
    dir1 = '/home/u764/Development/data/signatures-opencv'
    dir2 = '/home/u764/Development/data/signatures-new'

    signatures1 = os.listdir(dir1)
    signatures2 = os.listdir(dir2)

    signatures1 = [os.path.join(dir1, x) for x in signatures1]
    signatures1 = [(x, cv2.imread(x, cv2.IMREAD_GRAYSCALE))
                   for x in signatures1 if is_image(x)]
    signatures1_dict = dict()
    for path, image in signatures1:
        file_name = os.path.basename(path)
        signatures1_dict[file_name] = image

    signatures2 = [os.path.join(dir2, x)for x in signatures2]
    signatures2 = [(x, cv2.imread(x, cv2.IMREAD_GRAYSCALE))
                   for x in signatures2 if is_image(x)]
    signatures2_dict = dict()
    for path, image in signatures2:
        file_name = os.path.basename(path)
        signatures2_dict[file_name] = image

    similarity_score = 0
    n = 0
    m = 0
    for key in signatures2_dict:
        image1 = signatures1_dict[key]
        image2 = signatures2_dict[key]
        if not np.all(image1 == image2):
            m += 1
            print(key)
            similarity_score += np.count_nonzero(image1 ==
                                                image2) / (224.0 * 224.0)
            n += 1

    print(similarity_score / n)
    print(m, '/', len(signatures1_dict))


if __name__ == '__main__':
    # evaluate()
    main()
