import math
import sys
import time
from typing import Callable
import matplotlib.pyplot as plt
import numpy as np
import optuna
from xgboost import XGBRegressor

EPSILON = 1e-7


def mape(y_true, y_pred):
    return np.mean((np.abs(y_true - y_pred) + EPSILON) / (np.abs(y_true) + EPSILON)) * 100.0


def mse(y_true, y_pred):
    return np.mean(np.square(y_true - y_pred))


def sequence_to_feature_vectors(sequence: np.ndarray,
                                feature_vector_length: int) -> np.ndarray:
    number_of_feature_vectors = sequence.shape[0] - feature_vector_length + 1
    feature_vectors = np.zeros((feature_vector_length, number_of_feature_vectors),
                               sequence.dtype)
    for i in range(feature_vector_length):
        feature_vectors[i, :] = sequence[i: i + number_of_feature_vectors]
    return np.transpose(feature_vectors)


def sequence_to_input_output_features(sequence: np.ndarray,
                                      input_feature_vector_length: int,
                                      output_feature_vector_length: int) -> tuple[np.ndarray, np.ndarray]:
    input_feature_vectors = sequence_to_feature_vectors(sequence[:-output_feature_vector_length],
                                                        input_feature_vector_length)
    output_feature_vectors = sequence_to_feature_vectors(sequence[input_feature_vector_length:],
                                                         output_feature_vector_length)
    return input_feature_vectors, output_feature_vectors


def moving_average(sequence: np.ndarray,
                   window_size: int,
                   keep_same_size: bool = True) -> np.ndarray:
    half_window_size = window_size // 2
    sequence_copy = np.array(sequence, np.float32)
    averages = np.convolve(sequence_copy, np.ones(window_size), 'valid') / window_size
    if keep_same_size:
        sequence_copy[half_window_size: sequence.shape[0] - half_window_size] = averages
        return sequence_copy
    return averages


def eliminate_trend_with_moving_average(time_series: np.ndarray,
                                        window_size: int,
                                        keep_same_size: bool = True):
    trend_estimate = moving_average(time_series, window_size, keep_same_size)
    if keep_same_size:
        new_time_series = time_series - trend_estimate
    else:
        half_window_size = window_size // 2
        new_time_series = time_series[half_window_size: time_series.shape[0] - half_window_size] - trend_estimate
    return new_time_series, trend_estimate


class ModelFittingArguments(object):
    pass


class SlidingWindowBasedModel(ModelFittingArguments):
    def __init__(self,
                 input_feature_vector_length,
                 output_feature_vector_length):
        super().__init__()
        self.input_feature_vector_length = input_feature_vector_length
        self.output_feature_vector_length = output_feature_vector_length

    def get_input_feature_vector_length(self):
        return self.input_feature_vector_length

    def get_output_feature_vector_length(self):
        return self.output_feature_vector_length


class IndexBasedModel(ModelFittingArguments):
    def __init__(self):
        super().__init__()


def fit_time_series_model(time_series,
                          model,
                          model_fitting_arguments):
    x_train = None
    y_train = None
    if isinstance(model_fitting_arguments, SlidingWindowBasedModel):
        x_train, y_train = sequence_to_input_output_features(time_series,
                                                             model_fitting_arguments.get_input_feature_vector_length(),
                                                             model_fitting_arguments.get_output_feature_vector_length())
    elif isinstance(model_fitting_arguments, IndexBasedModel):
        x_train = np.arange(1, time_series.shape[0] + 1).astype(np.float32)
        y_train = np.expand_dims(np.array(time_series, np.float32), axis=-1)
    model.fit(x_train, y_train)
    return model


def evaluate_time_series_model(training_series,
                               evaluation_series,
                               model,
                               model_fitting_arguments,
                               evaluation_metric):
    y_pred = None
    training_data_size = training_series.shape[0]
    evaluation_data_size = evaluation_series.shape[0]
    if isinstance(model_fitting_arguments, SlidingWindowBasedModel):
        input_feature_vector_length = model_fitting_arguments.get_input_feature_vector_length()
        y_pred = recursive_multi_step_forecast_without_trend(training_series,
                                                             0.0,
                                                             1.0,
                                                             evaluation_data_size,
                                                             model,
                                                             input_feature_vector_length)

    elif isinstance(model_fitting_arguments, IndexBasedModel):
        x_test = np.arange(training_data_size + 1,
                           training_data_size + evaluation_data_size + 1).astype(np.float32)
        y_pred = model.predict(x_test)

    return evaluation_metric(evaluation_series, y_pred.ravel())


def fit_and_evaluate_time_series_model(training_series_list: list[np.ndarray],
                                       evaluation_series_list: list[np.ndarray],
                                       model_factory: Callable,
                                       model_fitting_arguments: ModelFittingArguments,
                                       evaluation_metric) -> float:
    total_error = 0.0

    for i in range(len(training_series_list)):
        training_series = training_series_list[i]
        evaluation_series = evaluation_series_list[i]

        model = model_factory()
        fitted_model = fit_time_series_model(training_series, model, model_fitting_arguments)
        total_error += evaluate_time_series_model(training_series,
                                                  evaluation_series,
                                                  fitted_model,
                                                  model_fitting_arguments,
                                                  evaluation_metric)
    return total_error


def evaluate_time_series_model_index_based(model,
                                           training_data_size,
                                           evaluation_data):
    predictions = model(np.arange(training_data_size + 1,
                                  training_data_size + 1 + evaluation_data.shape[0]).astype(np.float32))
    return mse(evaluation_data, predictions)


def fit_constant_model(time_series: np.ndarray):
    c = np.mean(time_series)
    return lambda x: np.ones(x.shape) * c


def fit_polynomial_model(time_series: np.ndarray,
                         polynomial_degree: int):
    weights = np.polyfit(np.arange(1, time_series.shape[0] + 1).astype(np.float32),
                         time_series,
                         deg=polynomial_degree)
    return np.poly1d(weights)


def fit_linear_logarithmic_model(time_series: np.ndarray):
    weights = np.polyfit(np.log(np.arange(1, time_series.shape[0] + 1)).astype(np.float32),
                         time_series,
                         deg=1)
    linear_model = np.poly1d(weights)
    return lambda x: linear_model(np.log(x))


def fit_exponential_model(time_series: np.ndarray):
    weights = np.polyfit(np.arange(1, time_series.shape[0] + 1).astype(np.float32),
                         np.log(time_series),
                         deg=1)
    linear_model = np.poly1d(weights)
    return lambda x: np.exp(linear_model(x))


def fit_and_evaluate_constant_model(training_data: np.ndarray,
                                    evaluation_data: np.ndarray):
    constant_model = fit_constant_model(training_data)
    error = evaluate_time_series_model_index_based(constant_model, training_data.shape[0], evaluation_data)
    return constant_model, error


def fit_and_evaluate_polynomial_model(training_data: np.ndarray,
                                      evaluation_data: np.ndarray,
                                      polynomial_degree: int):
    polynomial_model = fit_polynomial_model(training_data, polynomial_degree)
    error = evaluate_time_series_model_index_based(polynomial_model, training_data.shape[0], evaluation_data)
    return polynomial_model, error


def fit_and_evaluate_linear_logarithmic_model(training_data: np.ndarray,
                                              evaluation_data: np.ndarray):
    linear_logarithmic_model = fit_linear_logarithmic_model(training_data)
    error = evaluate_time_series_model_index_based(linear_logarithmic_model, training_data.shape[0], evaluation_data)
    return linear_logarithmic_model, error


def fit_and_evaluate_exponential_model(training_data: np.ndarray,
                                       evaluation_data: np.ndarray):
    exponential_model = fit_exponential_model(training_data)
    error = evaluate_time_series_model_index_based(exponential_model, training_data.shape[0], evaluation_data)
    return exponential_model, error


def try_or_return(operation, arguments, return_value):
    try:
        return operation(*arguments)
    except:
        return return_value


def find_trend_best_fit(trend_series: np.ndarray,
                        evaluation_data_percentage: float):
    evaluation_data_size = int(math.floor(trend_series.shape[0] * evaluation_data_percentage))
    training_data = trend_series[:-evaluation_data_size]
    evaluation_data = trend_series[-evaluation_data_size:]

    constant_model_mse_pair = try_or_return(fit_and_evaluate_constant_model,
                                            (training_data, evaluation_data),
                                            (None, None))
    linear_model_mse_pair = try_or_return(fit_and_evaluate_polynomial_model,
                                          (training_data, evaluation_data, 1),
                                          (None, None))
    quadratic_model_mse_pair = try_or_return(fit_and_evaluate_polynomial_model,
                                             (training_data, evaluation_data, 2),
                                             (None, None))
    cubic_model_mse_pair = try_or_return(fit_and_evaluate_polynomial_model,
                                         (training_data, evaluation_data, 3),
                                         (None, None))
    linear_logarithmic_model_mse_pair = try_or_return(fit_and_evaluate_linear_logarithmic_model,
                                                      (training_data, evaluation_data),
                                                      (None, None))
    exponential_model_mse_pair = try_or_return(fit_and_evaluate_exponential_model,
                                               (training_data, evaluation_data),
                                               (None, None))
    models = {
        'constant': constant_model_mse_pair,
        'linear': linear_model_mse_pair,
        'quadratic': quadratic_model_mse_pair,
        'cubic': cubic_model_mse_pair,
        'linear_logarithmic': linear_logarithmic_model_mse_pair,
        'exponential': exponential_model_mse_pair
    }

    model_key_with_min_error = None
    min_error = sys.float_info.max

    for model_key in models:
        error = models[model_key][1]
        if error is not None and error < min_error:
            min_error = error
            model_key_with_min_error = model_key

    if model_key_with_min_error == 'constant':
        return fit_constant_model(trend_series), min_error
    if model_key_with_min_error == 'linear':
        return fit_polynomial_model(trend_series, 1), min_error
    if model_key_with_min_error == 'quadratic':
        return fit_polynomial_model(trend_series, 2), min_error
    if model_key_with_min_error == 'cubic':
        return fit_polynomial_model(trend_series, 3), min_error
    if model_key_with_min_error == 'linear_logarithmic':
        return fit_linear_logarithmic_model(trend_series), min_error
    if model_key_with_min_error == 'exponential':
        return fit_exponential_model(trend_series), min_error

    return lambda x: np.zeros(x.shape), sys.float_info.max


def train_and_evaluate_final_model(time_series,
                                   evaluation_data_percentage,
                                   hyper_parameters):
    xgboost_parameters = {
        'learning_rate': hyper_parameters['learning_rate'],
        'max_depth': hyper_parameters['max_depth'],
        'min_child_weight': hyper_parameters['min_child_weight'],
        'gamma': hyper_parameters['gamma'],
        'colsample_bytree': hyper_parameters['colsample_bytree'],
        'n_estimators': hyper_parameters['n_estimators'],
        'reg_alpha': hyper_parameters['reg_alpha'],
        'reg_lambda': hyper_parameters['reg_lambda']
    }

    input_feature_vector_length = hyper_parameters['input_feature_vector_length']
    model_fitting_arguments = SlidingWindowBasedModel(input_feature_vector_length, 1)

    if hyper_parameters['try_eliminate_trend']:
        best_trend_model, trend_model_error = find_trend_best_fit(time_series, evaluation_data_percentage)
        new_time_series = time_series - best_trend_model(np.arange(1, time_series.shape[0] + 1).astype(np.float32))
        mu = np.mean(new_time_series)
        sigma = np.std(new_time_series)
        normalized_time_series = (new_time_series - mu) / sigma
        model = XGBRegressor(**xgboost_parameters)
        fit_time_series_model(normalized_time_series, model, model_fitting_arguments)
        xgboost_training_error = evaluate_time_series_model(
            normalized_time_series[:input_feature_vector_length],
            normalized_time_series[input_feature_vector_length:],
            model,
            model_fitting_arguments,
            mse)
        return model, best_trend_model, mu, sigma, xgboost_training_error
    else:
        model = XGBRegressor(**xgboost_parameters)
        mu = np.mean(time_series)
        sigma = np.std(time_series)
        normalized_time_series = (time_series - mu) / sigma
        fit_time_series_model(normalized_time_series, model, model_fitting_arguments)
        xgboost_training_error = evaluate_time_series_model(
            normalized_time_series[:input_feature_vector_length],
            normalized_time_series[input_feature_vector_length:],
            model,
            model_fitting_arguments,
            mse)
        return model, mu, sigma, xgboost_training_error


def find_time_series_best_fit(time_series: np.ndarray,
                              rolling_cross_validation_percentages: list[float],
                              evaluation_data_percentage: float,
                              trials: int):
    time_series_length = time_series.shape[0]
    max_window_size = max(3, int(math.floor(time_series_length * min(rolling_cross_validation_percentages) / 2)))
    if max_window_size % 2 == 0:
        max_window_size -= 1

    training_series_list1 = []
    training_series_list2 = []
    evaluation_series_list1 = []
    evaluation_series_list2 = []

    best_trend_model, _ = find_trend_best_fit(time_series, evaluation_data_percentage)
    new_time_series = time_series - best_trend_model(np.arange(1, time_series_length + 1).astype(np.float32))
    evaluation_data_size = int(math.floor(evaluation_data_percentage * time_series_length))

    mu = np.mean(time_series)
    sigma = np.std(time_series)
    normalized_time_size = (time_series - mu) / sigma

    new_mu = np.mean(new_time_series)
    new_sigma = np.std(new_time_series)
    new_normalized_time_size = (new_time_series - new_mu) / new_sigma

    for percentage in rolling_cross_validation_percentages:
        training_data_size = int(math.floor(percentage * time_series_length))

        training_series = normalized_time_size[:training_data_size]
        evaluation_series = normalized_time_size[training_data_size: training_data_size + evaluation_data_size]
        training_series_list1.append(training_series)
        evaluation_series_list1.append(evaluation_series)

        new_training_series = new_normalized_time_size[:training_data_size]
        new_evaluation_series = new_normalized_time_size[training_data_size: training_data_size + evaluation_data_size]
        training_series_list2.append(new_training_series)
        evaluation_series_list2.append(new_evaluation_series)

    def objective(trial):
        input_feature_vector_length = trial.suggest_int('input_feature_vector_length', 1, min(31, max_window_size))
        try_eliminate_trend = trial.suggest_categorical('try_eliminate_trend', [True, False])
        xgboost_parameters = {
            'learning_rate': trial.suggest_loguniform('learning_rate', 0.001, 0.1),
            'max_depth': trial.suggest_int('max_depth', 2, 7),
            'min_child_weight': trial.suggest_int('min_child_weight', 1, 7),
            'gamma': trial.suggest_uniform('gamma', 0.1, 0.5),
            'colsample_bytree': trial.suggest_discrete_uniform('colsample_bytree', 0.1, 1.0, 0.1),
            'n_estimators': trial.suggest_int('n_estimators', 1000, 3000, 500),
            'reg_alpha': trial.suggest_loguniform('reg_alpha', 0.0001, 100),
            'reg_lambda': trial.suggest_loguniform('reg_lambda', 0.0001, 100)
        }

        model_fitting_arguments = SlidingWindowBasedModel(input_feature_vector_length, 1)
        if try_eliminate_trend:
            validation_error = fit_and_evaluate_time_series_model(training_series_list2,
                                                                  evaluation_series_list2,
                                                                  lambda: XGBRegressor(**xgboost_parameters),
                                                                  model_fitting_arguments,
                                                                  mse)
        else:
            validation_error = fit_and_evaluate_time_series_model(training_series_list1,
                                                                  evaluation_series_list1,
                                                                  lambda: XGBRegressor(**xgboost_parameters),
                                                                  model_fitting_arguments,
                                                                  mse)
        return validation_error

    study = optuna.create_study()
    study.optimize(objective, n_trials=trials)
    hyper_parameters = study.best_params

    return train_and_evaluate_final_model(time_series, evaluation_data_percentage, hyper_parameters), hyper_parameters


def recursive_multi_step_forecast_without_trend(time_series,
                                                mu,
                                                sigma,
                                                steps,
                                                model,
                                                input_feature_vector_length):
    forecast_data = time_series[-input_feature_vector_length:]
    forecast_data = (forecast_data - mu) / sigma

    multi_step_forecast = []

    for i in range(steps):
        next_prediction = float(model.predict(np.array([forecast_data]))[0])
        multi_step_forecast.append(next_prediction)
        forecast_data[:-1] = forecast_data[1:]
        forecast_data[-1] = next_prediction

    multi_step_forecast = np.array(multi_step_forecast, np.float32) * sigma + mu
    return multi_step_forecast


def recursive_multi_step_forecast_with_trend(time_series,
                                             mu,
                                             sigma,
                                             steps,
                                             model,
                                             trend_model,
                                             input_feature_vector_length):
    forecast_data = time_series[-input_feature_vector_length:]
    residual_data = forecast_data - trend_model(np.arange(time_series.shape[0] + 1 - input_feature_vector_length,
                                                          time_series.shape[0] + 1).astype(np.float32))

    residual_data = (residual_data - mu) / sigma
    multi_step_residuals_forecast = []
    multi_step_trend_forecast = trend_model(np.arange(len(time_series) + 1,
                                                      len(time_series) + 1 + steps).astype(np.float32))

    for i in range(steps):
        next_prediction = float(model.predict(np.array([residual_data]))[0])
        multi_step_residuals_forecast.append(next_prediction)
        residual_data[:-1] = residual_data[1:]
        residual_data[-1] = next_prediction

    multi_step_residuals_forecast = np.array(multi_step_residuals_forecast, np.float32)
    multi_step_residuals_forecast = multi_step_residuals_forecast * sigma + mu

    return multi_step_residuals_forecast + multi_step_trend_forecast


def generate_dummy_data():
    def generating_function(x):
        total = np.zeros(x.shape, np.float32)
        for i in range(1, 5):
            total += i * np.sin(x * math.pi / i)
        return total + 10

    x = np.arange(1, 101)
    y = generating_function(x)
    y += np.random.normal(0, 5)
    y += np.square(x)

    return x, y


def main():
    start_time = time.time()
    # x, y = generate_dummy_data()
    y = [1.0, 2.0, 3.0, 4.0, 3.0, 2.0, 1.0, 2.0, 3.0, 4.0, 3.0, 2.0, 1.0, 2.0, 3.0, 4.0, 3.0, 2.0, 1.0]

    y = np.array(y)
    x = np.arange(1, len(y) + 1)

    best_fit_result, hyper_parameters = find_time_series_best_fit(y, [0.5, 0.65, 0.8], 0.2, 20)
    print(best_fit_result)
    print(hyper_parameters)
    end_time = time.time()
    print('total duration: ', end_time - start_time)
    if len(best_fit_result) == 5:
        model, trend_model, mu, sigma, error = best_fit_result
        number_of_steps = 12
        other_ys = recursive_multi_step_forecast_with_trend(y,
                                                            mu,
                                                            sigma,
                                                            number_of_steps,
                                                            model,
                                                            trend_model,
                                                            hyper_parameters['input_feature_vector_length'])

        y -= trend_model(np.arange(1, y.shape[0] + 1).astype(np.float32))
        plt.plot(x, y, color='blue')
        other_ys -= trend_model(np.arange(y.shape[0] + 1, y.shape[0] + 1 + number_of_steps).astype(np.float32))
        plt.plot(range(y.shape[0] + 1, y.shape[0] + 1 + number_of_steps), other_ys, color='red')
        plt.show()
    else:
        model, mu, sigma, error = best_fit_result

        number_of_steps = 12
        other_ys = recursive_multi_step_forecast_without_trend(y,
                                                               mu,
                                                               sigma,
                                                               number_of_steps,
                                                               model,
                                                               hyper_parameters['input_feature_vector_length'])
        plt.plot(x, y, color='blue')
        plt.plot(range(y.shape[0] + 1, y.shape[0] + 1 + number_of_steps), other_ys, color='red')
        plt.show()


if __name__ == '__main__':
    main()
