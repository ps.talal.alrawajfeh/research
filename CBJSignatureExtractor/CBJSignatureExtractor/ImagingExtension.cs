using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using Progressoft.Imaging;

namespace CBJSignatureExtractor
{
    public unsafe class ImagingExtension
    {
        // Add to Morphology
        public static Bitmap RectangularDilation(
            Bitmap bitmap,
            (int, int) kernelShape,
            byte backgroundColor = 0,
            byte foregroundColor = 255)
        {
            BitmapData bitmapData = null;
            BitmapData dilatedData = null;
            Bitmap dilated = bitmap.Clone() as Bitmap;

            try
            {
                if (bitmap.PixelFormat != PixelFormat.Format8bppIndexed) return null;

                var height = bitmap.Height;
                var width = bitmap.Width;

                if (kernelShape.Item1 % 2 == 0
                    || kernelShape.Item2 % 2 == 0
                    || kernelShape.Item1 < 0
                    || kernelShape.Item2 < 0)
                {
                    return null;
                }

                var originY = kernelShape.Item1 / 2;
                var originX = kernelShape.Item2 / 2;

                bitmapData = bitmap.LockBits(
                    new Rectangle(0, 0, width, height),
                    ImageLockMode.ReadWrite,
                    bitmap.PixelFormat);

                dilatedData = dilated.LockBits(
                    new Rectangle(0, 0, width, height),
                    ImageLockMode.ReadWrite,
                    bitmap.PixelFormat);

                var stride = bitmapData.Stride;

                var bitmapPtr = (byte*) bitmapData.Scan0.ToPointer();
                var dilatedPtr = (byte*) dilatedData.Scan0.ToPointer();
                var offset = stride - width;

                for (var y = 0; y < height; y++)
                {
                    for (var x = 0; x < width; x++)
                    {
                        var imageX1 = Math.Max(x - originX, 0);
                        var imageY1 = Math.Max(y - originY, 0);
                        var imageX2 = Math.Min(x + originX + 1, width);
                        var imageY2 = Math.Min(y + originY + 1, height);

                        var dilationRegionPtr = bitmapPtr - (y - imageY1) * stride - (x - imageX1);
                        var dilationWidth = imageX2 - imageX1;

                        var pixel = backgroundColor;
                        for (var yk = imageY1; yk < imageY2; yk++)
                        {
                            if (pixel == foregroundColor)
                            {
                                break;
                            }

                            for (var xk = imageX1; xk < imageX2; xk++)
                            {
                                if (dilationRegionPtr[0] == foregroundColor)
                                {
                                    pixel = foregroundColor;
                                    break;
                                }

                                dilationRegionPtr++;
                            }

                            dilationRegionPtr = dilationRegionPtr - dilationWidth + stride;
                        }

                        dilatedPtr[0] = pixel;

                        bitmapPtr++;
                        dilatedPtr++;
                    }

                    bitmapPtr += offset;
                    dilatedPtr += offset;
                }

                return dilated;
            }
            catch
            {
                return null;
            }
            finally
            {
                if (bitmapData != null)
                {
                    bitmap.UnlockBits(bitmapData);
                }

                if (dilatedData != null)
                {
                    dilated.UnlockBits(dilatedData);
                }
            }
        }

        // Replace in GenericProcessing
        public static Bitmap RemoveComponents(
            Bitmap bitmap,
            List<ConnectedComponent> components,
            byte backgroundColor = 255)
        {
            BitmapData bitmapData = null;
            var output = (Bitmap) bitmap.Clone();

            try
            {
                if (components == null || bitmap.PixelFormat != PixelFormat.Format8bppIndexed)
                    return null;

                var width = bitmap.Width;
                var height = bitmap.Height;

                var lockRect = new Rectangle(0, 0, width, height);
                bitmapData = output.LockBits(
                    lockRect,
                    ImageLockMode.ReadWrite,
                    PixelFormat.Format8bppIndexed);

                var stride = bitmapData.Stride;
                var ptr = (byte*) bitmapData.Scan0.ToPointer();

                foreach (var component in components)
                {
                    if (!lockRect.Contains(component.Rectangle))
                        continue;

                    foreach (var point in component.Points)
                    {
                        ptr[point.Y * stride + point.X] = backgroundColor;
                    }
                }

                return output;
            }
            catch
            {
                return null;
            }
            finally
            {
                if (bitmapData != null)
                {
                    output.UnlockBits(bitmapData);
                }
            }
        }

        // Add to Statistics
        public static int VerticalSum(Bitmap bitmap, int x, int y1, int y2)
        {
            BitmapData bitmapData = null;
            try
            {
                if (bitmap.PixelFormat != PixelFormat.Format8bppIndexed) return 0;

                if (y2 - y1 <= 1)
                {
                    return 0;
                }

                var height = bitmap.Height;
                var width = bitmap.Width;

                bitmapData = bitmap.LockBits(
                    new Rectangle(0, 0, width, height),
                    ImageLockMode.ReadWrite,
                    bitmap.PixelFormat);

                var stride = bitmapData.Stride;

                var ptr = (byte*) bitmapData.Scan0.ToPointer();

                ptr += y1 * stride + x;

                var sum = 0;
                for (var y = y1; y < y2; y++)
                {
                    sum += ptr[0];
                    ptr += stride;
                }

                return sum;
            }
            catch
            {
                return 0;
            }
            finally
            {
                if (bitmapData != null)
                {
                    bitmap.UnlockBits(bitmapData);
                }
            }
        }

        // Add to Statistics
        public static int HorizontalSum(Bitmap bitmap, int y, int x1, int x2)
        {
            BitmapData bitmapData = null;
            try
            {
                if (bitmap.PixelFormat != PixelFormat.Format8bppIndexed) return 0;

                if (x2 - x1 <= 1)
                {
                    return 0;
                }

                var height = bitmap.Height;
                var width = bitmap.Width;

                bitmapData = bitmap.LockBits(
                    new Rectangle(0, 0, width, height),
                    ImageLockMode.ReadWrite,
                    bitmap.PixelFormat);

                var stride = bitmapData.Stride;

                var ptr = (byte*) bitmapData.Scan0.ToPointer();

                ptr += stride * y;

                var sum = 0;
                for (var x = x1 + 1; x < x2; x++)
                {
                    sum += ptr[0];
                    ptr++;
                }

                return sum;
            }
            catch
            {
                return 0;
            }
            finally
            {
                if (bitmapData != null)
                {
                    bitmap.UnlockBits(bitmapData);
                }
            }
        }

        // Add to GenericProcessing
        private static bool AreAllPixelsEqualInVerticalStrip(byte* ptr, int stride, int x, int y1, int y2,
            byte color = 0)
        {
            ptr += x;

            var allEqual = true;
            for (var y = y1; y < y2; y++)
            {
                if (ptr[0] != color)
                {
                    allEqual = false;
                    break;
                }

                ptr += stride;
            }

            return allEqual;
        }

        // Add to GenericProcessing
        private static bool AreAllPixelsEqualInHorizontalStrip(byte* ptr, int stride, int y, int x1, int x2,
            byte color = 0)
        {
            ptr += stride * y + x1;

            var allEqual = true;
            for (var x = x1; x < x2; x++)
            {
                if (ptr[0] != color)
                {
                    allEqual = false;
                    break;
                }

                ptr++;
            }

            return allEqual;
        }

        // Add to GenericProcessing
        public static Rectangle AutoInflateRectangle(Bitmap bitmap, Rectangle rectangle, byte backgroundColor = 0)
        {
            var height = bitmap.Height;
            var width = bitmap.Width;

            var (x1, y1, x2, y2) = (rectangle.Left, rectangle.Top, rectangle.Right, rectangle.Bottom);

            BitmapData bitmapData = null;

            try
            {
                if (bitmap.PixelFormat != PixelFormat.Format8bppIndexed) return new Rectangle(0, 0, 0, 0);

                bitmapData = bitmap.LockBits(
                    new Rectangle(0, 0, width, height),
                    ImageLockMode.ReadWrite,
                    bitmap.PixelFormat);

                var stride = bitmapData.Stride;
                var ptr = (byte*) bitmapData.Scan0.ToPointer();

                while (y1 > 0 &&
                       !AreAllPixelsEqualInHorizontalStrip(ptr, stride, y1 - 1, x1, x2, backgroundColor))
                {
                    y1--;
                }

                while (x1 > 0 &&
                       !AreAllPixelsEqualInVerticalStrip(ptr, stride, x1 - 1, y1, y2, backgroundColor))
                {
                    x1--;
                }

                while (y2 < height &&
                       !AreAllPixelsEqualInHorizontalStrip(ptr, stride, y2, x1, x2, backgroundColor))
                {
                    y2++;
                }

                while (x2 < width &&
                       !AreAllPixelsEqualInVerticalStrip(ptr, stride, x2, y1, y2, backgroundColor))
                {
                    x2++;
                }

                if (x1 >= x2 || y1 >= y2)
                {
                    return new Rectangle(0, 0, 0, 0);
                }

                return new Rectangle(x1, y1, x2 - x1, y2 - y1);
            }
            catch
            {
                return new Rectangle(0, 0, 0, 0);
            }
            finally
            {
                if (bitmapData != null)
                {
                    bitmap.UnlockBits(bitmapData);
                }
            }
        }

        // Replace in Statistics
        public static int[] GetVerticalHistogram(Bitmap bitmap, byte threshold, bool invertedColor = false)
        {
            BitmapData bitmapData = null;
            try
            {
                if (bitmap.PixelFormat != PixelFormat.Format8bppIndexed) return null;

                var width = bitmap.Width;
                var height = bitmap.Height;

                var histogram = new int[width];

                bitmapData = bitmap.LockBits(
                    new Rectangle(0, 0, width, height),
                    ImageLockMode.ReadWrite,
                    bitmap.PixelFormat);

                var stride = bitmapData.Stride;

                var ptr = (byte*) bitmapData.Scan0.ToPointer();
                var offset = stride - width;

                for (var y = 0; y < height; y++)
                {
                    for (var x = 0; x < width; x++)
                    {
                        if (!invertedColor && ptr[0] < threshold
                            || invertedColor && ptr[0] > threshold)
                            histogram[x]++;

                        ptr++;
                    }

                    ptr += offset;
                }

                return histogram;
            }
            catch
            {
                return null;
            }
            finally
            {
                if (bitmapData != null)
                {
                    bitmap.UnlockBits(bitmapData);
                }
            }
        }

        // Replace in GenericProcessing
        public static Rectangle GetBoundaries(Bitmap bitmap, byte backgroundColor = 255, bool invertedColor = false)
        {
            BitmapData bitmapData = null;

            try
            {
                var width = bitmap.Width;
                var height = bitmap.Height;

                bitmapData = bitmap.LockBits(
                    new Rectangle(0, 0, width, height),
                    ImageLockMode.ReadWrite,
                    PixelFormat.Format8bppIndexed);

                var offset = bitmapData.Stride - width;

                var ptr = (byte*) bitmapData.Scan0.ToPointer();
                var minX = width;
                var maxX = 0;
                var minY = height;
                var maxY = 0;

                for (var y = 0; y < height; y++)
                {
                    for (var x = 0; x < width; x++)
                    {
                        if (!invertedColor && ptr[0] < backgroundColor
                            || invertedColor && ptr[0] > backgroundColor)
                        {
                            if (x < minX)
                                minX = x;
                            if (y < minY)
                                minY = y;
                            if (x > maxX)
                                maxX = x;
                            if (y > maxY)
                                maxY = y;
                        }

                        ptr++;
                    }

                    ptr += offset;
                }

                var rectWidth = maxX - minX + 1;
                var rectHeight = maxY - minY + 1;
                return new Rectangle(minX, minY, rectWidth, rectHeight);
            }
            catch
            {
                return new Rectangle(0, 0, 0, 0);
            }
            finally
            {
                if (bitmapData != null)
                {
                    bitmap.UnlockBits(bitmapData);
                }
            }
        }
    }
}