﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using Progressoft.Imaging;

namespace CBJSignatureExtractor
{
    unsafe class SignatureExtractor
    {
        private const double Epsilon = 1E-07D;

        private static readonly (int Width, int Height) Type1Size = (730, 200);
        private static readonly (int Width, int Height) Type2Size = (450, 400);
        private static readonly (int Width, int Height) Type3Size = (580, 300);

        private static readonly Func<double, double> Type1HeightWidthRatioFuzzifier =
            TruthFuzzifier(0.275, 0.05, 0.4, 0.15);

        private static readonly Func<double, double> Type2HeightWidthRatioFuzzifier =
            TruthFuzzifier(0.88, 0.03, 0.95, 0.8);

        private static readonly Func<double, double> Type3HeightWidthRatioFuzzifier =
            TruthFuzzifier(0.52, 0.025, 0.6, 0.45);

        static Func<double, double> LinearInterpolation(
            List<double> xs,
            List<double> ys,
            double defaultValue)
        {
            return x =>
            {
                if (x - Epsilon < xs[0] || x + Epsilon > xs.Last())
                {
                    return defaultValue;
                }

                if (Math.Abs(x - xs[0]) < Epsilon)
                {
                    return ys[0];
                }

                for (var i = 0; i < xs.Count - 1; i++)
                {
                    if (xs[i] < x && x <= xs[i + 1])
                    {
                        var dy = ys[i + 1] - ys[i];
                        var dx = xs[i + 1] - xs[i];
                        return dy * (x - xs[i]) / dx + ys[i];
                    }
                }

                return ys.Last();
            };
        }

        static Func<double, double> TruthFuzzifier(
            double valueMean,
            double valueStd,
            double valueMax,
            double valueMin)
        {
            return LinearInterpolation(
                new List<double> {0.0, valueMin, valueMean - valueStd, valueMean + valueStd, valueMax, 1.0},
                new List<double> {0.0, 0.5, 0.99, 0.99, 0.5, 0.0},
                0.0);
        }

        public static ConnectedComponent LargestConnectedComponent(Bitmap bitmap)
        {
            var connectedComponents = ConnectedComponentAnalysis.GetComponents(bitmap, 255);

            ConnectedComponent largestComponent = null;
            var maxArea = 0;
            foreach (var connectedComponent in connectedComponents)
            {
                var area = connectedComponent.Rectangle.Width * connectedComponent.Rectangle.Height;

                if (area > maxArea)
                {
                    maxArea = area;
                    largestComponent = connectedComponent;
                }

                if (area == maxArea &&
                    largestComponent != null && largestComponent.Points.Count < connectedComponent.Points.Count)
                {
                    largestComponent = connectedComponent;
                }
            }

            return largestComponent;
        }

        public static int Type1ImageSignaturesSplitX(Bitmap bitmap)
        {
            var height = bitmap.Height;
            var width = bitmap.Width;

            var histogram = ImagingExtension.GetVerticalHistogram(bitmap, 127, true);
            histogram = histogram.Select(x => (int) (Math.Floor(x / 5.0) * 5.0)).ToArray();

            var minXs = new List<int>();
            var minValue = (double) height;

            for (var x = width / 4; x < 3 * width / 4; x++)
            {
                if (histogram[x] < minValue)
                {
                    minValue = histogram[x];
                    minXs = new List<int> {x};
                }
                else if (Math.Abs(histogram[x] - minValue) < Epsilon)
                {
                    minXs.Add(x);
                }
            }

            return (int) Math.Round((double) minXs.Sum() / minXs.Count);
        }

        public static int VerticalSum(byte* ptr, int stride, int x, int y1, int y2)
        {
            ptr += y1 * stride + x;

            var sum = 0;
            for (var y = y1; y < y2; y++)
            {
                sum += ptr[0];
                ptr += stride;
            }

            return sum;
        }

        public static int HorizontalSum(
            byte* ptr,
            int stride,
            int y,
            int x1,
            int x2)
        {
            ptr += stride * y;

            var sum = 0;
            for (var x = x1 + 1; x < x2; x++)
            {
                sum += ptr[0];
                ptr++;
            }

            return sum;
        }

        public static (Bitmap, int, bool) RemoveLine(
            Bitmap bitmap,
            int axis,
            int direction,
            double threshold = 0.7)
        {
            bool isLineDetected = false;

            int axisLength;
            int otherAxisLength;
            if (axis == 0)
            {
                axisLength = bitmap.Height;
                otherAxisLength = bitmap.Width;
            }
            else
            {
                axisLength = bitmap.Width;
                otherAxisLength = bitmap.Height;
            }

            if (axisLength == 0)
            {
                return (bitmap, 0, isLineDetected);
            }

            var maxValueI = -1;
            var maxValue = 0.0;

            IEnumerable<int> loopRange;
            if (axisLength < 8)
            {
                if (direction == 1)
                {
                    return (bitmap, 0, isLineDetected);
                }

                return (bitmap, axisLength, isLineDetected);
            }

            if (direction == 1)
            {
                loopRange = Enumerable.Range(0, axisLength / 4);
            }
            else if (direction == -1)
            {
                loopRange = Enumerable.Range(1, axisLength / 4 - 1).Select(x => axisLength - x);
            }
            else
            {
                throw new ArgumentException($"direction {direction} is invalid");
            }

            var denominator = otherAxisLength * 255;

            var height = bitmap.Height;
            var width = bitmap.Width;

            BitmapData bitmapData = null;

            try
            {
                bitmapData = bitmap.LockBits(
                    new Rectangle(0, 0, width, height),
                    ImageLockMode.ReadWrite,
                    bitmap.PixelFormat);

                var stride = bitmapData.Stride;
                var ptr = (byte*) bitmapData.Scan0.ToPointer();

                foreach (var i in loopRange)
                {
                    var sum = axis == 1
                        ? VerticalSum(ptr, stride, i, 0, height)
                        : HorizontalSum(ptr, stride, i, 0, width);

                    var value = (double) sum / denominator;

                    if (value > threshold && value > maxValue)
                    {
                        isLineDetected = true;
                        maxValue = value;
                        maxValueI = i;
                    }
                }

                int minValueI;
                if (maxValueI == -1)
                {
                    if (direction == 1)
                    {
                        minValueI = 0;
                    }
                    else
                    {
                        minValueI = axisLength - 1;
                    }
                }
                else
                {
                    minValueI = maxValueI;
                    var minValue = 1.0;

                    if (direction == 1)
                    {
                        loopRange = Enumerable.Range(maxValueI, axisLength / 4 - maxValueI);
                    }
                    else
                    {
                        var start = axisLength - maxValueI;
                        loopRange = Enumerable.Range(start, axisLength / 4 - start).Select(x => axisLength - x);
                    }

                    foreach (var i in loopRange)
                    {
                        var sum = axis == 1
                            ? VerticalSum(ptr, stride, i, 0, height)
                            : HorizontalSum(ptr, stride, i, 0, width);

                        var value = (double) sum / denominator;

                        if (value < minValue)
                        {
                            minValue = value;
                            minValueI = i;
                        }
                    }
                }

                if (direction == 1)
                {
                    if (axis == 1)
                    {
                        return (ImageCropping.Crop(bitmap,
                                new Rectangle(minValueI, 0, width - minValueI, height)),
                            minValueI, isLineDetected);
                    }

                    return (ImageCropping.Crop(bitmap, new Rectangle(0, minValueI, width, height - minValueI)),
                        minValueI,
                        isLineDetected);
                }

                if (axis == 1)
                {
                    return (ImageCropping.Crop(bitmap, new Rectangle(0, 0, minValueI + 1, height)), minValueI,
                        isLineDetected);
                }

                return (ImageCropping.Crop(bitmap, new Rectangle(0, 0, width, minValueI + 1)), minValueI,
                    isLineDetected);
            }
            catch
            {
                return (bitmap, 0, false);
            }
            finally
            {
                if (bitmapData != null)
                {
                    bitmap.UnlockBits(bitmapData);
                }
            }
        }

        public static (Bitmap, Rectangle) RemoveLines(
            Bitmap bitmap,
            bool allLinesMustExist = true,
            double threshold = 0.7)
        {
            var defaultRectangle = new Rectangle(0, 0, bitmap.Width, bitmap.Height);

            var (newBinaryImage1, x1, isLineDetected) = RemoveLine(bitmap, 1, 1, threshold);
            if (!isLineDetected && allLinesMustExist)
            {
                return (bitmap.Clone() as Bitmap, defaultRectangle);
            }

            int y1, x2, y2;
            Bitmap newBinaryImage2;
            (newBinaryImage2, x2, isLineDetected) = RemoveLine(newBinaryImage1, 1, -1, threshold);
            if (!isLineDetected && allLinesMustExist)
            {
                newBinaryImage1.Dispose();
                return (bitmap.Clone() as Bitmap, defaultRectangle);
            }

            Bitmap newBinaryImage3;
            (newBinaryImage3, y1, isLineDetected) = RemoveLine(newBinaryImage2, 0, 1, threshold);
            if (!isLineDetected && allLinesMustExist)
            {
                newBinaryImage1.Dispose();
                newBinaryImage2.Dispose();
                return (bitmap.Clone() as Bitmap, defaultRectangle);
            }

            Bitmap newBinaryImage4;
            (newBinaryImage4, y2, isLineDetected) = RemoveLine(newBinaryImage3, 0, -1, threshold);
            if (!isLineDetected && allLinesMustExist)
            {
                newBinaryImage1.Dispose();
                newBinaryImage2.Dispose();
                newBinaryImage3.Dispose();
                return (bitmap.Clone() as Bitmap, defaultRectangle);
            }

            return (newBinaryImage4, new Rectangle(x1, y1, x2 - x1, y2 - y1));
        }

        public static Bitmap RemoveExtremeComponents(Bitmap bitmap)
        {
            var width = bitmap.Width;
            var height = bitmap.Height;
            var area = width * height;

            var dilated = ImagingExtension.RectangularDilation(bitmap, (3, 3));
            var connectedComponents = ConnectedComponentAnalysis.GetComponents(dilated, 255);

            var removedComponents = new List<ConnectedComponent>();

            foreach (var connectedComponent in connectedComponents)
            {
                var rectangle = connectedComponent.Rectangle;

                var rectWidth = rectangle.Width;
                var rectHeight = rectangle.Height;

                var heightRatio = (double) rectHeight / height;
                var widthRatio = (double) rectWidth / width;

                if (rectWidth <= 10 && rectHeight <= 10)
                {
                    removedComponents.Add(connectedComponent);
                    continue;
                }

                if (heightRatio >= 0.85 || widthRatio >= 0.85)
                {
                    removedComponents.Add(connectedComponent);
                    continue;
                }

                if ((double) rectWidth * rectHeight / area >= 0.7)
                {
                    removedComponents.Add(connectedComponent);
                    continue;
                }

                if ((double) rectWidth / rectHeight < 0.05 || (double) rectHeight / rectWidth < 0.05)
                {
                    removedComponents.Add(connectedComponent);
                    continue;
                }

                var pixelsToImageAreaRatio = (double) connectedComponent.Points.Count / area;

                if (pixelsToImageAreaRatio >= 0.85 || pixelsToImageAreaRatio < 0.00015)
                {
                    removedComponents.Add(connectedComponent);
                }
            }

            var cleaned = ImagingExtension.RemoveComponents(dilated, removedComponents, 0);
            var result = BitWiseOperations.BitWiseAnding(cleaned, bitmap);

            dilated.Dispose();
            cleaned.Dispose();

            return result;
        }

        public static (Bitmap Result, int ExpectedSignaturesY) RemoveText(
            Bitmap bitmap,
            double threshold = 0.725)
        {
            var height = bitmap.Height;

            var dilated = ImagingExtension.RectangularDilation(bitmap, (1, 27));
            var connectedComponents = ConnectedComponentAnalysis.GetComponents(dilated, 255);
            var removedComponents = new List<ConnectedComponent>();

            var expectedSignaturesY = 0;

            foreach (var component in connectedComponents)
            {
                var rectangle = component.Rectangle;
                var rectWidth = rectangle.Width;
                var rectHeight = rectangle.Height;

                var pixelsToRectangleAreaRatio = (double) component.Points.Count / (rectWidth * rectHeight);
                if (pixelsToRectangleAreaRatio > threshold && rectWidth > rectHeight)
                {
                    var y = rectangle.Bottom;
                    if (y >= expectedSignaturesY && y < height / 2)
                    {
                        expectedSignaturesY = y;
                    }

                    removedComponents.Add(component);
                }
            }

            var cleaned = ImagingExtension.RemoveComponents(dilated, removedComponents, 0);
            var result = BitWiseOperations.BitWiseAnding(cleaned, bitmap);

            dilated.Dispose();
            cleaned.Dispose();

            return (result, expectedSignaturesY);
        }

        static Rectangle RescaleRectangle(
            Rectangle rectangle,
            (int, int) srcImageSize,
            (int, int) destinationImageSize)
        {
            var x1 = (int) Math.Floor((double) rectangle.Left / srcImageSize.Item1 * destinationImageSize.Item1);
            var y1 = (int) Math.Floor((double) rectangle.Top / srcImageSize.Item2 * destinationImageSize.Item2);
            var x2 = (int) Math.Ceiling((double) rectangle.Right / srcImageSize.Item1 * destinationImageSize.Item1);
            var y2 = (int) Math.Ceiling((double) rectangle.Bottom / srcImageSize.Item2 * destinationImageSize.Item2);

            return new Rectangle(x1, y1, x2 - x1, y2 - y1);
        }

        static Rectangle CalculateRelativeRectangle(Rectangle rectangle,
            params (int, int)[] startPoints)
        {
            var (x1, y1) = (rectangle.Left, rectangle.Top);

            foreach (var (x, y) in startPoints)
            {
                x1 += x;
                y1 += y;
            }

            return new Rectangle(x1, y1, rectangle.Width, rectangle.Height);
        }

        static bool IsEmpty(Bitmap bitmap)
        {
            var whitePixelsPercentage = GenericProcessing.GetWhitePixelsPercentage(bitmap);

            if (whitePixelsPercentage < 0.0001)
            {
                return true;
            }

            return false;
        }

        static double EuclideanDistance((int X, int Y) point1, (int X, int Y) point2)
        {
            return Math.Sqrt(Math.Pow((double) point1.X - point2.X, 2.0) + Math.Pow((double) point1.Y - point2.Y, 2.0));
        }

        static IEnumerable<(int, int)> Combinations(IEnumerable<int> items)
        {
            var list = items.ToList();

            for (var i = 0; i < list.Count - 1; i++)
            {
                for (var j = i + 1; j < list.Count; j++)
                {
                    yield return (list[i], list[j]);
                }
            }
        }

        static (int, int) Center((int X, int Y) point1, (int X, int Y) point2)
        {
            return ((point1.X + point2.X) / 2, (point1.Y + point2.Y) / 2);
        }

        static List<Bitmap> ExtractSignaturesFromType1(Bitmap bitmap)
        {
            var image = ColorDepthConversion.To8Bits(bitmap);
            var resized = GeometricTransforms.ScaleByNearestNeighbor(image, Type1Size.Width, Type1Size.Height);
            var thresh = Binarization.BinarizeImage(resized, BinarizationMethod.OtsuMethod);
            var inverted = GenericProcessing.InvertColor(thresh);

            var cleanedImage = RemoveExtremeComponents(inverted);
            if (IsEmpty(cleanedImage))
            {
                image.Dispose();
                resized.Dispose();
                thresh.Dispose();
                inverted.Dispose();
                cleanedImage.Dispose();
                return new List<Bitmap> {bitmap};
            }

            var cleanedImageCopy = cleanedImage.Clone() as Bitmap;
            var (result, expectedSignaturesY) = RemoveText(cleanedImage);
            if (!IsEmpty(result))
            {
                cleanedImage.Dispose();
                cleanedImage = result;
            }

            var width = cleanedImage.Width;
            var height = cleanedImage.Height;

            var invertedCleaned = GenericProcessing.InvertColor(cleanedImage);
            var horizontalHistogram = Statistics.GetHorizontalHistogram(invertedCleaned, 127);
            var minSum = (double) width;
            for (var y = expectedSignaturesY; y < height / 2; y++)
            {
                var sum = horizontalHistogram[y];
                if (sum < minSum)
                {
                    minSum = sum;
                    expectedSignaturesY = y;
                }
            }

            var splitX = Type1ImageSignaturesSplitX(inverted);
            var leftImage = ImageCropping.Crop(cleanedImage,
                new Rectangle(0, expectedSignaturesY, splitX, height - expectedSignaturesY));
            var rightImage = ImageCropping.Crop(cleanedImage,
                new Rectangle(splitX, expectedSignaturesY, width - splitX, height - expectedSignaturesY));

            var leftRectangle = ImagingExtension.GetBoundaries(leftImage, 0, true);
            var rightRectangle = ImagingExtension.GetBoundaries(rightImage, 0, true);

            leftRectangle.Offset(new Point(0, expectedSignaturesY));
            rightRectangle.Offset(splitX, expectedSignaturesY);

            leftRectangle = ImagingExtension.AutoInflateRectangle(cleanedImageCopy, leftRectangle);
            rightRectangle = ImagingExtension.AutoInflateRectangle(cleanedImageCopy, rightRectangle);

            List<Bitmap> signatures = new List<Bitmap>();

            foreach (var rectangle in new[] {leftRectangle, rightRectangle})
            {
                if (rectangle.Width == 0 || rectangle.Height == 0)
                {
                    continue;
                }

                var croppedImage = ImageCropping.Crop(inverted, rectangle);
                var dilated = ImagingExtension.RectangularDilation(croppedImage, (3, 3));

                var (adjusted, adjustedForLines) = RemoveLines(dilated, true, 0.9);
                var minimalBoundingRectangle = ImagingExtension.GetBoundaries(adjusted, 0, true);

                minimalBoundingRectangle = CalculateRelativeRectangle(minimalBoundingRectangle,
                    (adjustedForLines.X, adjustedForLines.Y),
                    (rectangle.X, rectangle.Y));

                var rescaledRectangle = RescaleRectangle(minimalBoundingRectangle,
                    Type1Size,
                    (bitmap.Width, bitmap.Height));

                if (rescaledRectangle.Width == 0 || rescaledRectangle.Height == 0)
                {
                    croppedImage.Dispose();
                    dilated.Dispose();
                    adjusted.Dispose();
                    continue;
                }

                signatures.Add(ImageCropping.Crop(image, new Rectangle(
                    Math.Max(rescaledRectangle.Left - 2, 0),
                    Math.Max(rescaledRectangle.Top - 2, 0),
                    Math.Min(rescaledRectangle.Width + 2, bitmap.Width),
                    Math.Min(rescaledRectangle.Height + 2, bitmap.Height))));

                croppedImage.Dispose();
                dilated.Dispose();
                adjusted.Dispose();
            }

            image.Dispose();
            resized.Dispose();
            thresh.Dispose();
            inverted.Dispose();
            cleanedImage.Dispose();
            cleanedImageCopy?.Dispose();
            invertedCleaned.Dispose();
            leftImage.Dispose();
            rightImage.Dispose();
            return signatures;
        }

        static List<Bitmap> ExtractSignaturesFromType2(Bitmap bitmap,
            bool removeText = false,
            bool resizeInput = true)
        {
            var image = ColorDepthConversion.To8Bits(bitmap);
            var resized = resizeInput
                ? GeometricTransforms.ScaleByNearestNeighbor(image, Type2Size.Width, Type2Size.Height)
                : image.Clone() as Bitmap;
            var thresh = Binarization.BinarizeImage(resized, BinarizationMethod.OtsuMethod);
            var inverted = GenericProcessing.InvertColor(thresh);

            var cleanedImage = RemoveExtremeComponents(inverted);

            if (IsEmpty(cleanedImage))
            {
                image.Dispose();
                resized?.Dispose();
                thresh.Dispose();
                inverted.Dispose();
                cleanedImage.Dispose();
                return new List<Bitmap> {bitmap};
            }

            if (removeText)
            {
                var result = RemoveText(cleanedImage).Result;
                if (!IsEmpty(result))
                {
                    cleanedImage.Dispose();
                    cleanedImage = result;
                }
            }

            var connectedComponents = ConnectedComponentAnalysis.GetComponents(cleanedImage, 255);
            var rectangles = connectedComponents.Select(component => component.Rectangle).ToList();

            while (rectangles.Count > 2)
            {
                var distancesWithIndices = new List<(double, int, int)>();

                foreach (var (r1Index, r2Index) in Combinations(Enumerable.Range(0, rectangles.Count)))
                {
                    var r1 = rectangles[r1Index];
                    var r2 = rectangles[r2Index];

                    var distance = EuclideanDistance(
                        Center((r1.Left, r1.Top), (r1.Right, r1.Bottom)),
                        Center((r2.Left, r2.Top), (r2.Right, r2.Bottom)));

                    distancesWithIndices.Add((distance, r1Index, r2Index));
                }

                distancesWithIndices.Sort((x, y) => x.Item1.CompareTo(y.Item1));

                var bestDistanceWithIndices = distancesWithIndices.First();

                var rectangle1 = rectangles[bestDistanceWithIndices.Item2];
                var rectangle2 = rectangles[bestDistanceWithIndices.Item3];

                var newRectangle = Rectangle.Union(rectangle1, rectangle2);

                rectangles.RemoveAll(rect => rect == rectangle1 || rect == rectangle2);
                rectangles.Add(newRectangle);
            }

            List<Bitmap> signatures = new List<Bitmap>();

            foreach (var rectangle in rectangles)
            {
                var rescaledRectangle = resizeInput
                    ? RescaleRectangle(rectangle,
                        Type2Size,
                        (bitmap.Width, bitmap.Height))
                    : rectangle;

                if (rescaledRectangle.Width == 0 || rescaledRectangle.Height == 0)
                {
                    continue;
                }

                signatures.Add(ImageCropping.Crop(image, new Rectangle(
                    Math.Max(rescaledRectangle.Left - 2, 0),
                    Math.Max(rescaledRectangle.Top - 2, 0),
                    Math.Min(rescaledRectangle.Width + 2, bitmap.Width),
                    Math.Min(rescaledRectangle.Height + 2, bitmap.Height))));
            }

            image.Dispose();
            resized?.Dispose();
            thresh.Dispose();
            inverted.Dispose();
            cleanedImage.Dispose();
            return signatures;
        }

        static List<Bitmap> ExtractSignaturesFromType3(Bitmap bitmap)
        {
            var image = ColorDepthConversion.To8Bits(bitmap);
            var resized = GeometricTransforms.ScaleByNearestNeighbor(image, Type3Size.Width, Type3Size.Height);
            var thresh = Binarization.BinarizeImage(resized, BinarizationMethod.OtsuMethod);
            var inverted = GenericProcessing.InvertColor(thresh);

            if (IsEmpty(inverted))
            {
                image.Dispose();
                resized.Dispose();
                thresh.Dispose();
                inverted.Dispose();
                return new List<Bitmap> {bitmap};
            }

            var dilated = ImagingExtension.RectangularDilation(inverted, (3, 3));
            var dilatedInverted = GenericProcessing.InvertColor(dilated);

            Statistics.GetVerticalAndHorizontalHistogram(dilatedInverted,
                127,
                out var verticalHistogram,
                out var horizontalHistogram);

            var height = inverted.Height;
            var width = inverted.Width;

            var maxVerticalSum = 0.0;
            var maxVerticalSumX = -1;
            for (var x = width / 3; x < width * 2 / 3; x++)
            {
                var sum = verticalHistogram[x];
                if (sum > maxVerticalSum)
                {
                    maxVerticalSum = sum;
                    maxVerticalSumX = x;
                }
            }

            var maxHorizontalSum = 0.0;
            var maxHorizontalSumY = -1;
            for (var y = height / 3; y < height * 2 / 3; y++)
            {
                var sum = horizontalHistogram[y];
                if (sum > maxHorizontalSum)
                {
                    maxHorizontalSum = sum;
                    maxHorizontalSumY = y;
                }
            }

            var rectangle1 = new Rectangle(0, 0, maxVerticalSumX, maxHorizontalSumY);
            var rectangle2 = new Rectangle(maxVerticalSumX, 0, width - maxVerticalSumX, maxHorizontalSumY);
            var rectangle3 = new Rectangle(0, maxHorizontalSumY, maxVerticalSumX, height - maxHorizontalSumY);
            var rectangle4 = new Rectangle(maxVerticalSumX, maxHorizontalSumY, width - maxVerticalSumX,
                height - maxHorizontalSumY);

            var imageRectanglePairs = new[]
            {
                (ImageCropping.Crop(dilated, rectangle1), rectangle1),
                (ImageCropping.Crop(dilated, rectangle2), rectangle2),
                (ImageCropping.Crop(dilated, rectangle3), rectangle3),
                (ImageCropping.Crop(dilated, rectangle4), rectangle4)
            };

            List<Bitmap> signatures = new List<Bitmap>();

            foreach (var (cropped, rectangle) in imageRectanglePairs)
            {
                var (adjusted, adjustedRectangle) = RemoveLines(cropped, false, 0.8);
                var minimalBoundingRectangle = ImagingExtension.GetBoundaries(adjusted, 0, true);
                var minimalCropped = ImageCropping.Crop(adjusted, minimalBoundingRectangle);
                var largestConnectedComponent = LargestConnectedComponent(minimalCropped);
                
                if (largestConnectedComponent == null)
                {
                    cropped.Dispose();
                    adjusted.Dispose();
                    minimalCropped.Dispose();
                    continue;
                }

                var bestRectangle = largestConnectedComponent.Rectangle;
                bestRectangle = ImagingExtension.AutoInflateRectangle(minimalCropped, bestRectangle);

                minimalBoundingRectangle = CalculateRelativeRectangle(bestRectangle,
                    (minimalBoundingRectangle.X, minimalBoundingRectangle.Y),
                    (adjustedRectangle.X, adjustedRectangle.Y),
                    (rectangle.X, rectangle.Y));

                var rescaledRectangle = RescaleRectangle(minimalBoundingRectangle,
                    Type3Size,
                    (bitmap.Width, bitmap.Height));

                if (rescaledRectangle.Width == 0 || rescaledRectangle.Height == 0)
                {
                    cropped.Dispose();
                    adjusted.Dispose();
                    minimalCropped.Dispose();
                    continue;
                }

                signatures.Add(ImageCropping.Crop(image, rescaledRectangle));

                cropped.Dispose();
                adjusted.Dispose();
                minimalCropped.Dispose();
            }

            image.Dispose();
            resized.Dispose();
            thresh.Dispose();
            inverted.Dispose();
            dilated.Dispose();
            dilatedInverted.Dispose();
            return signatures;
        }

        static List<Bitmap> ExtractSignaturesFromUnknownType(Bitmap bitmap)
        {
            return ExtractSignaturesFromType2(bitmap, true, false);
        }

        public static List<Bitmap> ExtractSignatures(Bitmap bitmap)
        {
            var heightWidthRatio = (double) bitmap.Height / bitmap.Width;
            var widthHeightRatio = (double) bitmap.Width / bitmap.Height;

            if (widthHeightRatio < 0.15 || heightWidthRatio < 0.15)
            {
                return new List<Bitmap> {bitmap};
            }

            if (Type1HeightWidthRatioFuzzifier(heightWidthRatio) >= 0.8)
            {
                return ExtractSignaturesFromType1(bitmap);
            }

            if (Type2HeightWidthRatioFuzzifier(heightWidthRatio) >= 0.8)
            {
                return ExtractSignaturesFromType2(bitmap);
            }

            if (Type3HeightWidthRatioFuzzifier(heightWidthRatio) >= 0.8)
            {
                return ExtractSignaturesFromType3(bitmap);
            }

            return ExtractSignaturesFromUnknownType(bitmap);
        }

        public static double SquashWithinRange(double number)
        {
            var mergerRange = (0.0, 100.0010000);
            var requiredRange = (0.0, 100.0);

            var slope = (requiredRange.Item2 - requiredRange.Item1) / (mergerRange.Item2 - mergerRange.Item1);
            return slope * (number - mergerRange.Item1) + mergerRange.Item1;
        }

        public static double MergeNumbers(double number1, double number2)
        {
            var merged = Math.Round(number1, 2) + Math.Round(number2, 2) / 10000.0;
            return SquashWithinRange(merged);
        }

        public static double UnSquash(double number)
        {
            var mergerRange = (0.0, 100.0010000);
            var requiredRange = (0.0, 100.0);

            var slope = (mergerRange.Item2 - mergerRange.Item1) / (requiredRange.Item2 - requiredRange.Item1);
            return slope * (number - requiredRange.Item1) + requiredRange.Item1;
        }

        public static (double, double) DecodeNumber(double number)
        {
            var unSquashed = UnSquash(number);

            var first = (int) (unSquashed * 100);
            var second = (unSquashed * 100 - first) * 10000;
            return (Math.Round((double) first / 100, 2), Math.Round((double) second / 100, 2));
        }

        public static void Main(string[] args)
        {
            // Console.WriteLine(MergeNumbers(99.12, 85.56));
            //
            // return;
            // var bmp = new Bitmap("/home/u764/Downloads/5");
            // ExtractSignaturesFromType3(bmp);

            //
            // var i = 0;
            // foreach (var bitmap in ExtractSignaturesFromType1(bmp))
            // {
            //     bitmap.Save($"/home/u764/Downloads/test{i}.bmp");
            //     i++;
            // }
            //
            // return;

            // result = RemoveText(result).Item1;
            // result.Save("/home/u764/Downloads/test.bmp");

            // var arr = BitmapToMatrix(bmp);
            // arr = ResizeNearestNeighbors(arr, Type1Size);
            // arr = OtsuThreshold(arr);
            // arr = Apply(arr, x => (byte) (255 - x));
            // arr = Apply(arr, x => (byte) (x / 255));
            //
            // arr = FilterOutExtremeRegions(arr);
            // arr = RemoveText(arr).Result;
            // SaveBinaryImage(arr, "/home/u764/Downloads/test3.bmp");

            /*
            var files = Directory.GetFiles("/home/u764/Downloads/cbj_images/");
            var sample = RandomSample(files, 200);
            var outPath = "/home/u764/Downloads/test/";

            var i = 1;
            foreach (var file in sample)
            {
                Bitmap bitmap;
                try
                {
                    bitmap = new Bitmap(file);
                }
                catch (Exception)
                {
                    continue;
                }

                var signatures = ExtractSignatures(bitmap);
                bitmap.Save(outPath + $"{i}.bmp");
                File.Move(outPath + $"{i}.bmp", outPath + $"{i}");

                var j = 1;
                foreach (var signature in signatures)
                {
                    if (signature == null)
                    {
                        continue;
                    }

                    signature.Save(outPath + $"{i}_{j}.bmp");
                    signature.Dispose();
                    File.Move(outPath + $"{i}_{j}.bmp", outPath + $"{i}_{j}");
                    j++;
                }

                i++;
            }

            return;
            */
            
            var bmp = new Bitmap("/home/u764/Downloads/test/34");
            // Console.WriteLine(ExtractSignatures(bmp));

            Console.WriteLine(Benchmark(100, 1000, () =>
            {
                var bitmaps = ExtractSignaturesFromType1(bmp);

                // for (var i = 0; i < bitmaps.Count; i++)
                // {
                //     bitmaps[i].Save($"/home/u764/Downloads/debug{i}.bmp");
                // }

                // SaveBinaryImage(mat, "/home/u764/Downloads/debug.bmp");
            }));

            // Console.WriteLine(Benchmark(100,
            //     1000,
            //     () => RectangularDilation(mat, (1, 27))));
            // var dilated = RectangularDilation(mat, (1, 27));
            //
        }

        static T[] RandomSample<T>(T[] array, int sampleSize)
        {
            if (sampleSize > array.Length)
            {
                throw new ArgumentException("sample size must be less than or equal to the array size");
            }

            var stepSize = array.Length / sampleSize;
            var remainder = array.Length % sampleSize;

            Random random = new Random();

            T[] result = new T[sampleSize];

            int i;
            for (i = 0; i < sampleSize - 1; i++)
            {
                var choice = random.Next(i * stepSize, (i + 1) * stepSize);
                result[i] = array[choice];
            }

            if (remainder > 0 && random.Next(0, 9) >= 5)
            {
                var choice = random.Next((i + 1) * stepSize, array.Length);
                result[i] = array[choice];
            }
            else
            {
                var choice = random.Next(i * stepSize, (i + 1) * stepSize);
                result[i] = array[choice];
            }

            return result;
        }

        static long Benchmark(int warmupIterations,
            int benchmarkIterations,
            Action routine)
        {
            for (var i = 0; i < warmupIterations; i++)
            {
                routine();
            }

            var startTime = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeMilliseconds();

            for (var i = 0; i < benchmarkIterations; i++)
            {
                routine();
            }

            var endTime = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeMilliseconds();

            return (endTime - startTime) / benchmarkIterations;
        }
    }
}