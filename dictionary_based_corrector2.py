import sys

import numpy as np
import textdistance

ENGLISH_AMOUNTS_DICTIONARY = [
    'zero',
    'one',
    'two',
    'three',
    'four',
    'five',
    'six',
    'seven',
    'eight',
    'nine',
    'ten',
    'eleven',
    'twelve',
    'thirteen',
    'fourteen',
    'fifteen',
    'sixteen',
    'seventeen',
    'eighteen',
    'nineteen',
    'twenty',
    'thirty',
    'forty',
    'fifty',
    'sixty',
    'seventy',
    'eighty',
    'ninety',
    'hundred',
    'thousand',
    'million',
    'only',
    'and'
]

ARABIC_AMOUNTS_DICTIONARY = [
    'صفر',
    'واحد',
    'اثنان',
    'اثنين',
    'ثلاثة',
    'ثلاث',
    'اربعة',
    'اربع',
    'خمسة',
    'خمس',
    'ستة',
    'ست',
    'سبعة',
    'سبع',
    'ثمانية',
    'ثمان',
    'تسعة',
    'تسع',
    'عشرة',
    'عشر',
    'احد',
    'اثنا',
    'عشرون',
    'ثلاثون',
    'اربعون',
    'خمسون',
    'ستون',
    'سبعون',
    'ثمانون',
    'تسعون',
    'مئة',
    'مئتان',
    'مئتا',
    'الف',
    'الاف',
    'الفان',
    'مليون',
    'مليونان',
    'ملايين',
    'و',
    'فقط',
    'لا',
    'غير'
]


def word_of_min_distance(text, dictionary):
    min_distance = sys.maxsize
    best_word = None
    for word in dictionary:
        distance = textdistance.damerau_levenshtein(text, word)
        if distance < min_distance:
            min_distance = distance
            best_word = word
    return best_word, min_distance


def correct_text(text, dictionary):
    max_index = len(text) + 1
    segments_distances_matrix = np.zeros((max_index, max_index), np.int64)
    best_words_matrix = []
    for i in range(max_index):
        best_words_matrix.append([])
        for j in range(max_index):
            best_words_matrix[i].append('')

    max_int64 = np.iinfo(np.int64).max
    for start in range(0, max_index):
        for end in range(0, max_index):
            if end <= start:
                segments_distances_matrix[start, end] = max_int64
                continue

            segment = text[start:end]
            best_word, min_distance = word_of_min_distance(segment, dictionary)
            segments_distances_matrix[start, end] = min_distance
            best_words_matrix[start][end] = best_word

    path_dict = dict()
    min_cost, best_start_end_pairs = recursive_function(text,
                                                        dictionary,
                                                        segments_distances_matrix,
                                                        path_dict)
    words = []
    for start, end in best_start_end_pairs:
        words.append(best_words_matrix[start][end])
    return ' '.join(words)


def recursive_function(text,
                       dictionary,
                       segments_distances_matrix,
                       path_dict,
                       start_index=0):
    max_index = len(text)
    min_cost = sys.maxsize
    best_start_end_pairs = None

    if start_index == max_index:
        return 0, []

    for start in range(start_index, max_index):
        for end in range(start + 1, max_index + 1):
            if end in path_dict:
                cost, start_end_pairs = path_dict[end]
            else:
                cost, start_end_pairs = recursive_function(text,
                                                           dictionary,
                                                           segments_distances_matrix,
                                                           path_dict,
                                                           end)
                path_dict[end] = (cost, start_end_pairs)

            min_distance = segments_distances_matrix[start, end]
            ignored_characters = start - start_index
            cost = ignored_characters + min_distance + cost
            other_cost = ignored_characters + min_distance + max_index - end

            if other_cost < min_cost:
                min_cost = cost
                best_start_end_pairs = [(start, end)]

            if cost < min_cost:
                min_cost = cost
                best_start_end_pairs = [(start, end)] + start_end_pairs

    return min_cost, best_start_end_pairs


def post_process(text, language='en'):
    if language == 'en':
        text = text.replace('-', ' ').replace('_', ' ').replace(',', ' ')
        text = ''.join([c for c in text if c in 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ '])
        text = text.lower().strip()
        dictionary = ENGLISH_AMOUNTS_DICTIONARY
    elif language == 'ar':
        text = text.replace('-', ' ').replace('،', ' ')
        text = text.replace(' و', ' و ').replace('أ', 'ا').replace('آ', 'ا').replace('إ', 'ا')
        text = ''.join([c for c in text if c in 'ا ب ت ث ج ح خ د ذ ر ز س ش ص ض ط ظ ع غ ف ق ك ل م ن ه و ي ئ ة'])
        text = text.replace('مائة', ' مئة ')
        text = text.replace('مئتان', ' مئتان ')
        text = text.strip()
        dictionary = ARABIC_AMOUNTS_DICTIONARY
    else:
        raise Exception(f'Dictionary not defined for language: {language}.')

    words = text.split(' ')
    new_words = []
    for word in words:
        if word.strip() == '':
            continue

        closest_word = None
        min_distance = sys.maxsize
        for dictionary_word in dictionary:
            distance = textdistance.damerau_levenshtein(word, dictionary_word)
            if distance < min_distance:
                min_distance = distance
                closest_word = dictionary_word

        closest_words = []
        word_copy = str(word)
        while True:
            min_segment_distance = sys.maxsize
            best_segment = None
            best_segment_match = None
            for dictionary_word in dictionary:
                start = 0
                for end in range(1, len(word_copy) + 1):
                    segment = word_copy[start:end]

                    distance = textdistance.damerau_levenshtein(segment, dictionary_word)
                    if distance < min_segment_distance or \
                            (distance == min_segment_distance and len(dictionary_word) > len(best_segment_match)):
                        min_segment_distance = distance
                        best_segment_match = dictionary_word
                        best_segment = (start, end, segment)
            if best_segment is not None:
                (start, end, segment) = best_segment
                if len(segment) <= 1 and end == len(word_copy) and min_segment_distance > 1:
                    break
                closest_words.append(best_segment_match)
                word_copy = word_copy[end:]
            else:
                break
            if end == len(word_copy):
                break

        if textdistance.damerau_levenshtein(word, ' '.join(closest_words)) < min_distance:
            new_words.extend(closest_words)
        elif closest_word is not None:
            new_words.append(closest_word)

    return ' '.join(new_words)


def main():
    print(correct_text('onertwothreeyfourx', ENGLISH_AMOUNTS_DICTIONARY))


if __name__ == '__main__':
    main()

