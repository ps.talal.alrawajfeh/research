#!/usr/bin/python3

import os
import cv2
from matplotlib import pyplot as plt
import numpy as np
 

def main():
    base_dir = '/home/u764/Development/data/tests/ours/correct/'
    files = os.listdir(base_dir)
    for f in files:
        image = cv2.imread(base_dir + f)

        avg_b = np.mean([image[:, :50, 0].mean(), image[:50, :, 0].mean(), image[:, -50:, 0].mean(), image[-50:, :, 0].mean()])
        avg_g = np.mean([image[:, :50, 1].mean(), image[:50, :, 1].mean(), image[:, -50:, 1].mean(), image[-50:, :, 1].mean()])
        avg_r = np.mean([image[:, :50, 2].mean(), image[:50, :, 2].mean(), image[:, -50:, 2].mean(), image[-50:, :, 2].mean()])
        
        avg_bgr = np.array([[avg_b, avg_g, avg_r]])

        distances = np.array(image, np.float)
        distances = np.sqrt(np.sum(np.square(distances - avg_bgr), axis=-1))
        distances = (distances - np.min(distances)) / \
            (np.max(distances) - np.min(distances)) * 255
        distances = np.array(distances, np.uint8)

        distances = cv2.threshold(
            distances, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]

        plt.imshow(distances, plt.cm.gray)
        plt.show()


if __name__ == '__main__':
    os.environ['QT_QPA_PLATFORM'] = 'wayland'
    main()
