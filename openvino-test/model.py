#!/usr/bin/python3

import tensorflow as tf


def main():
    i = tf.keras.layers.Input((10,))
    o = tf.keras.layers.Dense(1)(i)
    
    model = tf.keras.models.Model(inputs=i, outputs=o)
    tf.saved_model.save(model, 'model')


if __name__ == '__main__':
    main()

