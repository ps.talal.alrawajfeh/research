#include <inference_engine.hpp>
#include <iostream>

using namespace InferenceEngine;

// int main(int argc, char **argv)
// {
//     Core ie;
//     CNNNetwork network = ie.ReadNetwork("/home/saved_model.xml", "/home/saved_model.bin");
//     ExecutableNetwork executable_network = ie.LoadNetwork(network, "CPU");
//     for (const auto &p : network.getOutputsInfo())
//         p.second->setPrecision(Precision::FP32);
//     for (const auto &p : network.getInputsInfo())
//         p.second->setPrecision(Precision::FP32);

//     InferRequest infer_request = executable_network.CreateInferRequest();

//     InferenceEngine::BlockingDesc blk({1, 10}, {0, 1});
//     InferenceEngine::TensorDesc tDesc(InferenceEngine::Precision::FP32,
//                                       {1, 10},
//                                       blk);

//     float arr[] = {1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 1.0f, 2.0f, 3.0f, 4.0f, 5.0f};
//     infer_request.SetBlob("input_1", InferenceEngine::make_shared_blob<float>(tDesc, (float *)arr)); // infer_request accepts input blob of any size
//     infer_request.Infer();

//     Blob::Ptr output = infer_request.GetBlob("StatefulPartitionedCall/model/dense/BiasAdd/Add");

//     MemoryBlob::CPtr moutput = as<MemoryBlob>(output);
//     auto moutputHolder = moutput->rmap();
//     const float *detection = moutputHolder.as<const float *>();

//     std::cout << detection[0] << std::endl;

//     return 0;
// }


int main(int argc, char **argv)
{
    Core ie;
    CNNNetwork network = ie.ReadNetwork("/home/ChequeClearing.xml", "/home/ChequeClearing.bin");
    ExecutableNetwork executable_network = ie.LoadNetwork(network, "CPU");
    for (const auto &p : network.getOutputsInfo())
        p.second->setPrecision(Precision::FP32);
    for (const auto &p : network.getInputsInfo())
        p.second->setPrecision(Precision::FP32);

    InferRequest infer_request = executable_network.CreateInferRequest();

    InferenceEngine::BlockingDesc blk({1, 1, 200, 748}, {0, 1, 2, 3});
    InferenceEngine::TensorDesc tDesc(InferenceEngine::Precision::FP32,
                                      {1, 1, 200, 748},
                                      blk);

    float arr[149600] = {0.0f};
    infer_request.SetBlob("input_1", InferenceEngine::make_shared_blob<float>(tDesc, (float *)arr)); // infer_request accepts input blob of any size
    infer_request.Infer();

    Blob::Ptr output = infer_request.GetBlob("conv2d_15/Sigmoid");

    MemoryBlob::CPtr moutput = as<MemoryBlob>(output);
    auto moutputHolder = moutput->rmap();
    const float *detection = moutputHolder.as<const float *>();

    std::cout << detection[0] << std::endl;

    return 0;
}