#!/usr/bin/python3.6
import os

import cv2
from matplotlib import pyplot as plt
import numpy as np


def neighbours(x, y, image):
    i = image
    x1, y1, x_1, y_1 = x + 1, y - 1, x - 1, y + 1
    # print ((x,y))
    return [i[y1][x], i[y1][x1], i[y][x1], i[y_1][x1],  # P2,P3,P4,P5
            i[y_1][x], i[y_1][x_1], i[y][x_1], i[y1][x_1]]  # P6,P7,P8,P9


def transitions(neighbours):
    n = neighbours + neighbours[0:1]  # P2, ... P9, P2
    return sum((n1, n2) == (0, 1) for n1, n2 in zip(n, n[1:]))


def zhangSuen(image):
    changing1 = changing2 = [(-1, -1)]
    while changing1 or changing2:
        # Step 1
        changing1 = []
        for y in range(1, len(image) - 1):
            for x in range(1, len(image[0]) - 1):
                P2, P3, P4, P5, P6, P7, P8, P9 = n = neighbours(x, y, image)
                if (image[y][x] == 1 and  # (Condition 0)
                        P4 * P6 * P8 == 0 and  # Condition 4
                        P2 * P4 * P6 == 0 and  # Condition 3
                        transitions(n) == 1 and  # Condition 2
                        2 <= sum(n) <= 6):  # Condition 1
                    changing1.append((x, y))
        for x, y in changing1: image[y][x] = 0
        # Step 2
        changing2 = []
        for y in range(1, len(image) - 1):
            for x in range(1, len(image[0]) - 1):
                P2, P3, P4, P5, P6, P7, P8, P9 = n = neighbours(x, y, image)
                if (image[y][x] == 1 and  # (Condition 0)
                        P2 * P6 * P8 == 0 and  # Condition 4
                        P2 * P4 * P8 == 0 and  # Condition 3
                        transitions(n) == 1 and  # Condition 2
                        2 <= sum(n) <= 6):  # Condition 1
                    changing2.append((x, y))
        for x, y in changing2: image[y][x] = 0
        # print changing1
        # print changing2
    return image


def view_image(image):
    plt.imshow(image, cmap=plt.cm.gray)
    plt.show()


def binarize_skeletonize(image):
    image = binarize(image)
    image[image == 255] = 1
    zhangSuen(image)
    return image


def binarize(image):
    image = cv2.resize(image, (240, 48), interpolation=cv2.INTER_AREA)
    blur = cv2.medianBlur(image, 5)
    ret3, image = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    image = 255 - image
    return image


def main():
    out_dir = '/home/u764/Development/progressoft/en-car-reader/data/test/skeleton/'
    in_dir = '/home/u764/Development/progressoft/en-car-reader/data/test/mutaz/'

    images = os.listdir(in_dir)
    for f in images:
        image = cv2.imread(in_dir + f,
                           cv2.IMREAD_GRAYSCALE)
        image = binarize(image)
        cv2.imwrite(out_dir + f, image)


if __name__ == '__main__':
    main()
