#!/usr/bin/python3.6
import base64
import sys

import requests


def send_request():
    request_body = dict()

    with open(sys.argv[2], 'rb') as f:
        request_body['image1_b64'] = base64.b64encode(f.read())

    with open(sys.argv[3], 'rb') as f:
        request_body['image2_b64'] = base64.b64encode(f.read())

    response = requests.post(sys.argv[1], json=request_body)
    print(response.json())


if __name__ == '__main__':
    send_request()
