#!/usr/bin/python3.6

import sys

import tensorflow as tf


def convert_h5_model_to_pb(source, destination):
    h5_model = tf.keras.models.load_model(source, compile=False)
    h5_model.save(filepath=destination)


if __name__ == '__main__':
    convert_h5_model_to_pb(sys.argv[1], sys.argv[2])
