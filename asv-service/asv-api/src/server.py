import base64
import math
import sys

import cv2
import flask
import numpy as np
import tensorflow as tf
from flask import request
from scipy.interpolate import interpolate
from tensorflow.keras.applications import densenet

DENSE_NET_INPUT_DIM = 244
WHITE = 255

LIMIT_DISTANCE = 2.0
GENUINE_MOST_PROBABLE_DISTANCE = 0.415
FORGERY_MOST_PROBABLE_DISTANCE = 1.9
PIVOT_DISTANCE = 1.1
SIAMESE_CERTAINTY_THRESHOLD = 0.95
SIAMESE_MAPPED_THRESHOLD = 0.5
SIAMESE_WEIGHT = 0.4
TRIPLET_WEIGHT = 0.6
EPSILON = 1e-12

app = flask.Flask(__name__)

features_embedding_model = tf.keras.models.load_model(sys.argv[1])
siamese_model = tf.keras.models.load_model(sys.argv[2])
dense_features_model = densenet.DenseNet201(include_top=False,
                                            pooling='avg',
                                            input_shape=(DENSE_NET_INPUT_DIM, DENSE_NET_INPUT_DIM, 3))


def horizontally_centered_pad(image, padding_length, color):
    return np.pad(image,
                  ((0, 0), (padding_length, padding_length)),
                  'constant',
                  constant_values=((0, 0), (color, color)))


def vertically_centered_pad(image, padding_length, color):
    return np.pad(image,
                  ((padding_length, padding_length),
                   (0, 0)),
                  'constant',
                  constant_values=((color, color), (0, 0)))


def pad_and_resize(image):
    height_width_ratio = image.shape[0] / image.shape[1]
    if height_width_ratio > 1.0:
        height_width_difference = image.shape[0] - image.shape[1]
        padded = horizontally_centered_pad(image, int(math.ceil(height_width_difference / 2)), WHITE)
    elif height_width_ratio < 1.0:
        width_height_difference = image.shape[1] - image.shape[0]
        padded = vertically_centered_pad(image, int(math.ceil(width_height_difference / 2)), WHITE)
    else:
        padded = image

    if padded.shape[0] != DENSE_NET_INPUT_DIM:
        return cv2.resize(padded, (DENSE_NET_INPUT_DIM, DENSE_NET_INPUT_DIM), interpolation=cv2.INTER_AREA)
    return padded


def duplicate_channels(gray_scale_image):
    expanded = np.expand_dims(gray_scale_image, -1)
    return np.concatenate([expanded, expanded, expanded], -1)


def pre_process_image(image):
    image = 255 - pad_and_resize(image)
    return densenet.preprocess_input(duplicate_channels(image))


def read_image_from_base64(image_base64):
    image_binary = np.fromstring(base64.b64decode(image_base64), np.uint8)
    return cv2.imdecode(image_binary, cv2.IMREAD_GRAYSCALE)


def get_dense_features(*images):
    return dense_features_model.predict(np.array([pre_process_image(image) for image in images]))


def get_siamese_prediction(image1_dense_features, image2_dense_features):
    return siamese_model.predict([image1_dense_features.reshape((1, 1920)),
                                  image2_dense_features.reshape(1, 1920)])[0]


def get_features_embeddings(*images_dense_features):
    return features_embedding_model.predict(np.array([*images_dense_features]))


def squared_euclidean_distance(image1_features_embeddings, image2_features_embeddings):
    return np.sum(np.square(image1_features_embeddings - image2_features_embeddings))


def calculate_similarity_from_embeddings(image1_features_embeddings, image2_features_embeddings):
    distance = squared_euclidean_distance(image1_features_embeddings,
                                          image2_features_embeddings)
    if distance < EPSILON:
        return 100.0
    percentage_mapper = interpolate.interp1d(
        [0.0, GENUINE_MOST_PROBABLE_DISTANCE, PIVOT_DISTANCE, FORGERY_MOST_PROBABLE_DISTANCE, LIMIT_DISTANCE],
        [100.0, 90.0, 50.0, 10.0, 0.0],
        kind='linear')
    if distance > LIMIT_DISTANCE:
        distance = LIMIT_DISTANCE
    if distance < 0.0:
        distance = 0.0
    return percentage_mapper(distance)


@app.route('/verify', methods=['POST'])
def verify():
    body = request.get_json()
    image1 = read_image_from_base64(body['image1_b64'])
    image2 = read_image_from_base64(body['image2_b64'])

    image1_dense_features, image2_dense_features = get_dense_features(image1, image2)

    image1_features_embeddings, image2_features_embeddings = get_features_embeddings(image1_dense_features,
                                                                                     image2_dense_features)
    embeddings_similarity = calculate_similarity_from_embeddings(image1_features_embeddings,
                                                                 image2_features_embeddings)
    siamese_prediction = get_siamese_prediction(image1_dense_features, image2_dense_features)

    siamese_mapper = interpolate.interp1d([0.0, SIAMESE_CERTAINTY_THRESHOLD, 1.0],
                                          [0.0, SIAMESE_MAPPED_THRESHOLD, 1.0],
                                          kind='linear')

    siamese_result = float(siamese_mapper(siamese_prediction[0]) * 100)
    triplet_result = float(embeddings_similarity)

    combination = SIAMESE_WEIGHT * siamese_result + TRIPLET_WEIGHT * triplet_result

    print(f'siamese:{siamese_prediction[0]}')
    print(f'triplet:{embeddings_similarity / 100}')
    print(f'combination {combination}')

    if embeddings_similarity == 100.0:
        return {
            'siamese': siamese_result,
            'triplet': 100.0,
            'combination': combination
        }

    return {
        'siamese': siamese_result,
        'triplet': triplet_result,
        'combination': combination
    }


@app.route('/', methods=['GET'])
def home():
    return "Hello World!"


if __name__ == '__main__':
    app.config["DEBUG"] = False
    app.run()
