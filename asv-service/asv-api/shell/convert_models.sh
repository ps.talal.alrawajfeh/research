#!/usr/bin/zsh

PROJECT_PATH=$1

if ! [[ -d $PROJECT_PATH/resources/features_embedding ]]; then
  python3.6 ./src/h5_to_pb.py "$PROJECT_PATH"/resources/features_embedding.h5 "$PROJECT_PATH"/resources/features_embedding
fi

if ! [[ -d $PROJECT_PATH/resources/siamese ]]; then
  python3.6 ./src/h5_to_pb.py "$PROJECT_PATH"/resources/siamese.h5 "$PROJECT_PATH"/resources/siamese
fi
