#!/usr/bin/zsh

PROJECT_PATH=$1

python3.6 "$PROJECT_PATH"/src/server.py "$PROJECT_PATH"/resources/features_embedding "$PROJECT_PATH"/resources/siamese
