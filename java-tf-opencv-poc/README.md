# Java OpenCV Tensorflow Java with Docker POC

## Description
This is an exercise for deploying models using Java and Docker

## Notes

To download the proper tensorflow libraries look in this link with the same version as the maven dependency:
https://storage.googleapis.com/tensorflow/

Append the path to the library to the above link to download the library. Please note that you must only extract libtensorflow.so and libtensorflow.so.1 and libtensorflow.so.x.xx.x from the libtensorflow archive and then extract libtensorflow_jni.so and libtensorflow_framework.so and libtensorflow_framework.so.1 and libtensorflow_framework.so.x.xx.x from libtensorflow_jni archive.
