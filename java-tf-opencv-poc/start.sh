#!/bin/bash

export OPEN_CV_LIB=$(find /opt/opencv/opencv/build/lib -name "libopencv_java*")
export PATH=$PATH:/app
java -Djava.library.path=/app -Dfile.encoding=UTF-8 -classpath /app/java-tf-opencv-poc-1.0-SNAPSHOT-jar-with-dependencies.jar com.progressoft.Test /app/model.pb /app/eight.bmp
