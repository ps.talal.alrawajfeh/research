package com.progressoft;

import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.opencv.core.Mat;

import static org.opencv.imgcodecs.Imgcodecs.imread;
import static org.opencv.imgproc.Imgproc.resize;


public class ImageUtils {
    private static final Object OPEN_CV_LIB_MONITOR = new Object();
    public static final String OPEN_CV_LIB_ENV = "OPEN_CV_LIB";
    private static boolean isOpenCVLoaded = false;

    public static INDArray readImage(String path, int[] size) {
        loadOpenCVLibrary();

        Mat image = imread(path, 0);
        if (image.size(0) != size[0] || image.size(1) != size[1]) {
            Mat resizedImage = new Mat(size[0], size[1], image.type());
            resize(image,
                    resizedImage,
                    resizedImage.size(),
                    0.0D,
                    0.0D,
                    3);
            image.release();
            image = resizedImage;
        }

        double[][] arr = copyImageToArray(image);
        image.release();
        return Nd4j.create(arr).reshape(size[0], size[1], 1);
    }

    private static double[][] copyImageToArray(Mat image) {
        int cols = image.size(1);
        int rows = image.size(0);
        double[][] arr = new double[rows][cols];
        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < cols; c++) {
                arr[r][c] = image.get(r, c)[0];
            }
        }
        return arr;
    }

    public static INDArray preProcess(INDArray array) {
        return array.div(255);
    }

    private static void loadOpenCVLibrary() {
        if (!isOpenCVLoaded)
            synchronized (OPEN_CV_LIB_MONITOR) {
                if (!isOpenCVLoaded) {
                    System.load(System.getenv(OPEN_CV_LIB_ENV));
                    isOpenCVLoaded = true;
                }
            }
    }
}

