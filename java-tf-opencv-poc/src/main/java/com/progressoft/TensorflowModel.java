package com.progressoft;

import org.tensorflow.Graph;
import org.tensorflow.Session;
import org.tensorflow.Tensor;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;


public class TensorflowModel {
    private Graph modelGraph;
    private Session modelSession;

    public TensorflowModel(String modelPath) {
        byte[] protocolBuffer = loadResource(modelPath);
        this.modelGraph = new Graph();
        this.modelGraph.importGraphDef(protocolBuffer);
        this.modelSession = new Session(this.modelGraph);
    }

    public List<Tensor<?>> predict(Tensor<Float> inputTensor,
                                   String inputLayerName,
                                   String outputLayerName) {
        return this.modelSession.runner()
                .feed(inputLayerName, inputTensor)
                .fetch(outputLayerName)
                .run();
    }

    @Override
    protected void finalize() throws Throwable {
        this.modelSession.close();
        this.modelGraph.close();
        super.finalize();
    }

    private byte[] loadResource(String path) {
        byte[] resource;
        try {
            Path modelPath = Paths.get(path);
            resource = Files.readAllBytes(modelPath);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return resource;
    }
}

