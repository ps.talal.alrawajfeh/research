package com.progressoft;

import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.tensorflow.Tensor;

import java.util.List;

public class Test {
    private static final long[] INPUT_SHAPE = {1, 28, 28, 1};
    private static final int[] IMAGE_SHAPE = {28, 28};
    private static final String INPUT_LAYER_NAME = "conv2d_1_input";
    private static final String OUTPUT_LAYER_NAME = "dense_2/Softmax";

    public static void main(String[] args) {
        TensorflowModel tensorflowModel = new TensorflowModel(args[0]);
        INDArray image = ImageUtils.readImage(args[1], IMAGE_SHAPE);

        try (Tensor<Float> tensor = inputTensorFromImage(image)) {
            List<Tensor<?>> predictions = tensorflowModel.predict(tensor,
                    INPUT_LAYER_NAME,
                    OUTPUT_LAYER_NAME);

            printPredictions(predictions);
        }
    }

    private static void printPredictions(List<Tensor<?>> predictions) {
        try (Tensor<?> outputTensor = predictions.get(0)) {
            float[][] outputArray = new float[1][10];
            outputTensor.copyTo(outputArray);

            System.out.println(extractClass(outputArray));
        }
    }

    private static int extractClass(float[][] outputArray) {
        return Nd4j.argMax(Nd4j.create(outputArray)).getInt(0);
    }

    private static Tensor<Float> inputTensorFromImage(INDArray image) {
        return Tensor.create(INPUT_SHAPE,
                ImageUtils.preProcess(image)
                        .data()
                        .asNioFloat());
    }
}

