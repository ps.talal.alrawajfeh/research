from __future__ import print_function

import keras
import numpy as np
from keras import backend as K
from keras.datasets import mnist
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Dense, Dropout, Flatten
from keras.models import Sequential

BATCH_SIZE = 128
NUM_CLASSES = 10
EPOCHS = 12

IMG_ROWS, IMG_COLS = 28, 28


def get_input_shape():
    if K.image_data_format() == 'channels_first':
        return 1, IMG_ROWS, IMG_COLS
    else:
        return IMG_ROWS, IMG_COLS, 1


def pre_process(image):
    return np.reshape(image, get_input_shape()).astype(np.float32) / 255


def build_model():
    model = Sequential()

    model.add(Conv2D(32,
                     kernel_size=(3, 3),
                     activation='relu',
                     input_shape=(get_input_shape())))
    model.add(Conv2D(64,
                     (3, 3),
                     activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Flatten())

    model.add(Dense(128, activation='relu'))
    model.add(Dropout(0.5))

    model.add(Dense(NUM_CLASSES,
                    activation='softmax'))

    return model


def train(data):
    train_data, test_data = data

    x_train, y_train = train_data
    x_test, y_test = test_data

    x_train = np.array([pre_process(x) for x in x_train])
    x_test = np.array([pre_process(x) for x in x_test])

    y_train = keras.utils.to_categorical(y_train, NUM_CLASSES)
    y_test = keras.utils.to_categorical(y_test, NUM_CLASSES)

    model = build_model()

    model.compile(loss=keras.losses.categorical_crossentropy,
                  optimizer=keras.optimizers.Adadelta(),
                  metrics=['accuracy'])

    model.fit(x_train, y_train,
              batch_size=BATCH_SIZE,
              epochs=EPOCHS,
              verbose=1,
              validation_data=(x_test, y_test))

    score = model.evaluate(x_test, y_test, verbose=0)

    print('com.progressoft.Test loss:', score[0])
    print('com.progressoft.Test accuracy:', score[1])

    return model


if __name__ == '__main__':
    trained_model = train(mnist.load_data())

    trained_model.save('../resources/model.h5')
