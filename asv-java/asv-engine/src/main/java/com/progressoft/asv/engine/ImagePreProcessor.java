package com.progressoft.asv.engine;


import com.progressoft.tensor.JTensor;

import java.awt.image.BufferedImage;
import java.util.function.Function;
import java.util.stream.IntStream;

public class ImagePreProcessor implements Function<BufferedImage, JTensor<Float>> {
    private static final double RED_WEIGHT = 0.299;
    private static final double GREEN_WEIGHT = 0.587;
    private static final double BLUE_WEIGHT = 1.0 - (RED_WEIGHT + GREEN_WEIGHT);
    private static final int GRAY_SCALE_DEPTH = 256;
    private static final double FLOAT_EPSILON = 1e-5;
    private static final int INPUT_WIDTH = 144;
    private static final int INPUT_HEIGHT = 96;

    private int convertRgbToGrayScale(int r, int g, int b) {
        return (int) Math.round(r * RED_WEIGHT + g * GREEN_WEIGHT + b * BLUE_WEIGHT);
    }

    private JTensor<Integer> bufferedImageToTensor(BufferedImage image) {
        var width = image.getWidth();
        var height = image.getHeight();

        var tensor = new JTensor<>(Integer.class, new int[]{height, width});

        for (var y = 0; y < height; y++) {
            for (var x = 0; x < width; x++) {
                var pixel = image.getRGB(x, y);
                int red = (pixel >> 16) & 0xff;
                int green = (pixel >> 8) & 0xff;
                int blue = (pixel) & 0xff;
                tensor.setItem(new int[]{y, x}, convertRgbToGrayScale(red, green, blue));
            }
        }

        return tensor;
    }

    private JTensor<Integer> threshold(JTensor<Integer> tensor, Integer value) {
        return tensor.map(Integer.class, x -> x > value ? 255 : 0);
    }

    private JTensor<Integer> otsuThreshold(JTensor<Integer> tensor) {
        var shape = tensor.getShape();
        var height = shape[0];
        var width = shape[1];

        var histogram = new int[GRAY_SCALE_DEPTH];
        for (var y = 0; y < height; y++) {
            for (var x = 0; x < width; x++) {
                histogram[tensor.getItem(new int[]{y, x})]++;
            }
        }

        double scale = 1.0 / (height * width);
        double mu = IntStream.range(0, histogram.length).map(i -> histogram[i] * i).sum() * scale;

        double q1 = 0;
        var maxVal = 0;
        double maxSigma = -1;
        double mu1 = 0;

        for (int t = 0; t < GRAY_SCALE_DEPTH; t++) {
            var pt = histogram[t] * scale;
            mu1 *= q1;
            q1 += pt;
            var q2 = 1.0 - q1;

            if (Math.min(q1, q2) < FLOAT_EPSILON || Math.max(q1, q2) > 1.0 - FLOAT_EPSILON) {
                continue;
            }

            mu1 = (mu1 + t * pt) / q1;
            double mu2 = (mu - q1 * mu1) / q2;

            var sigma = q1 * q2 * (mu1 - mu2) * (mu1 - mu2);
            if (sigma > maxSigma) {
                maxSigma = sigma;
                maxVal = t;
            }
        }

        return threshold(tensor, maxVal);
    }

    private JTensor<Integer> removeWhiteBorder(JTensor<Integer> tensor) {
        var black = JTensor.compare(tensor, JTensor.singleValue(0))
                .map(Integer.class, x -> x == 0 ? 1 : 0);

        var alongY = black.reduceAlong(0,
                (x, y) -> x | y,
                1,
                false);

        var alongX = black.reduceAlong(0,
                (x, y) -> x | y,
                0,
                false);

        var shape = tensor.getShape();
        var height = shape[0];
        var width = shape[1];

        int x1 = JTensor.argMax(alongX, 0, true).getItem(new int[]{0});
        int y1 = JTensor.argMax(alongY, 0, true).getItem(new int[]{0});
        int x2 = width - JTensor.argMax(alongX.reverse(0), 0, true).getItem(new int[]{0});
        int y2 = height - JTensor.argMax(alongY.reverse(0), 0, true).getItem(new int[]{0});

        return tensor.slice(new int[][]{{y1, y2}, {x1, x2}});
    }

    private int specialFloor(double value) {
        var floor = (int) value;
        if (floor > value) {
            floor--;
        }

        return floor;
    }

    private JTensor<Integer> resizeNearestNeighbors(JTensor<Integer> tensor,
                                                    int destWidth,
                                                    int destHeight) {
        var shape = tensor.getShape();
        var srcHeight = shape[0];
        var srcWidth = shape[1];

        if (srcWidth == 0 || srcHeight == 0 || destWidth == 0 || destHeight == 0) {
            return JTensor.empty(Integer.class);
        }

        var fx = destWidth / (double) srcWidth;
        var fy = destHeight / (double) srcHeight;

        var ifx = 1.0 / fx;
        var ify = 1.0 / fy;

        var result = JTensor.zeros(Integer.class,
                new int[]{destHeight, destWidth});

        for (var y = 0; y < destHeight; y++) {
            for (var x = 0; x < destWidth; x++) {
                var srcX = specialFloor(ifx * x);
                var srcY = specialFloor(ify * y);

                srcX = Math.max(Math.min(srcX, srcWidth - 1), 0);
                srcY = Math.max(Math.min(srcY, srcHeight - 1), 0);

                result.setItem(new int[]{y, x}, tensor.getItem(new int[]{srcY, srcX}));
            }
        }

        return result;
    }

    @Override
    public JTensor<Float> apply(BufferedImage bufferedImage) {
        var resized = resizeNearestNeighbors(
                removeWhiteBorder(
                        otsuThreshold(
                                bufferedImageToTensor(bufferedImage))),
                INPUT_WIDTH,
                INPUT_HEIGHT);

        return resized
                .map(Float.class, x -> x == 0 ? 1.0f : 0.0f);
    }
}
