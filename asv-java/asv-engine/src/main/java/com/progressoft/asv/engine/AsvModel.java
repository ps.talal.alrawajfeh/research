package com.progressoft.asv.engine;

import com.progressoft.tensor.JTensor;
import org.tensorflow.Graph;
import org.tensorflow.Session;
import org.tensorflow.Tensor;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Objects;
import java.util.function.BiFunction;

public class AsvModel implements BiFunction<JTensor<Float>, JTensor<Float>, Float> {
    private static final String INPUT_LAYER_NAME = "input_1";
    private static final String OUTPUT_LAYER_NAME = "dense_3/Softmax";

    private final Graph modelGraph;
    private final Session modelSession;

    public AsvModel(String modelName) {
        this.modelGraph = new Graph();
        this.modelGraph.importGraphDef(loadResourceAsBytes(modelName));
        this.modelSession = new Session(this.modelGraph);
    }

    private float[] predict(Tensor<Float> inputTensor) {
        float[][] output = new float[1][2];

        try (Tensor<Float> outputTensor = this.modelSession.runner()
                .feed(INPUT_LAYER_NAME, inputTensor)
                .fetch(OUTPUT_LAYER_NAME)
                .run()
                .get(0)
                .expect(Float.class)) {
            outputTensor.copyTo(output);
        }

        return output[0];
    }

    @Override
    public Float apply(JTensor<Float> tensor1, JTensor<Float> tensor2) {
        var concatenated = JTensor.concatenate(tensor1.expand(2), tensor2.expand(2), 2)
                .expand(0);

        long[] inputShape = Arrays.stream(concatenated.getShape())
                .asLongStream()
                .toArray();

        try (Tensor<Float> inputTensor = Tensor.create(inputShape, concatenated.toFloatBuffer())) {
            return predict(inputTensor)[0];
        }
    }

    public void close() {
        this.modelSession.close();
        this.modelGraph.close();
    }

    private byte[] loadResourceAsBytes(String modelName) {
        try (final InputStream inputStream = Objects.requireNonNull(Thread.currentThread()
                .getContextClassLoader()
                .getResourceAsStream(modelName))) {
            return inputStream.readAllBytes();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
