import com.progressoft.asv.engine.AsvModel;
import com.progressoft.asv.engine.ImagePreProcessor;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Test {
    public static void main(String[] args) throws IOException {
        BufferedImage sig1 = ImageIO.read(new File("/home/u764/Downloads/sig2.bmp"));
        BufferedImage sig2 = ImageIO.read(new File("/home/u764/Downloads/sig3.bmp"));

        ImagePreProcessor imagePreProcessor = new ImagePreProcessor();

        AsvModel asvModel = new AsvModel("asv.pb");
        Float prediction = asvModel.apply(
                imagePreProcessor.apply(sig1),
                imagePreProcessor.apply(sig2));

        System.out.println(prediction * 100);

        asvModel.close();
    }
}
