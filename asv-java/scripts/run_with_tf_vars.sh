# start applicaiton
tf_libs="$(pwd)/tf_libs"

export TF_JNI_LIB=$tf_libs/libtensorflow_jni.so
export TF_XLA_FLAGS="--tf_xla_auto_jit=2 --tf_xla_cpu_global_jit"
export PATH=$PATH:$tf_libs

java -Djava.library.path="$tf_libs" -Xdebug -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:9090 -jar $1