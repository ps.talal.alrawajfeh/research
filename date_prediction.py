import math
import sys
from datetime import datetime, timedelta

import numpy as np
import tensorflow as tf
from tqdm import tqdm
import random
from matplotlib import pyplot as plt
import sympy as sp
from xgboost import XGBRegressor


def build_fourier_model(n, p):
    a0 = tf.Variable(tf.random.normal((1,), dtype=tf.float32), name='a0')
    f = tf.Variable(tf.random.normal((1,), dtype=tf.float32), name='f')

    parameters = [a0, f]

    for i in range(1, n + 1):
        ai = tf.Variable(tf.random.normal(
            (1,), dtype=tf.float32), name=f'a{i}')
        bi = tf.Variable(tf.random.normal(
            (1,), dtype=tf.float32), name=f'b{i}')
        parameters.extend([ai, bi])

    @tf.function
    def fourier_model(x):
        result = a0 / tf.constant(2.0)
        for j in range(1, n + 1):
            aj = parameters[2 + 2 * (j - 1)]
            bj = parameters[2 + 2 * (j - 1) + 1]
            c = f * tf.constant(j, dtype=tf.float32)
            result = result + aj * tf.math.cos(c * x) + bj * tf.math.sin(c * x)
        return result

    return fourier_model, parameters


def build_linear_model():
    a = tf.Variable(tf.random.normal((1,), dtype=tf.float32), name='a')
    b = tf.Variable(tf.random.normal((1,), dtype=tf.float32), name='b')

    @tf.function
    def linear_model(x):
        return a * x + b

    return linear_model, [a, b]


def predict_next_date_old(dates: list[datetime]):
    time_deltas = []

    for i in range(1, len(dates)):
        time_delta = dates[i] - dates[i - 1]
        time_deltas.append(time_delta.days)

    values = np.array(time_deltas, np.float32)
    print(values)
    # exit()
    mu = np.mean(values)
    values -= mu
    values_x = np.array([i for i in range(len(values))], np.float32)

    plt.plot(values_x, values + mu, color='blue')

    best_loss = sys.float_info.max
    best_model = None
    best_model_parameters = None
    best_n = -1
    best_p = -1

    n_range = 10
    p_range = 1
    progress_bar = tqdm(total=n_range * p_range)

    for n in range(1, n_range + 1):
        # for p in range(1, p_range + 1):
        p = -1
        harmonic_model, harmonic_model_parameters = build_fourier_model(
            n, p)
        linear_model, linear_parameters = build_linear_model()

        combined_parameters = harmonic_model_parameters + linear_parameters

        learning_rate = 0.001
        epochs = 1000

        x = tf.constant(values_x)
        y_true = tf.constant(values)

        for epoch in range(epochs):
            with tf.GradientTape() as tape:
                y_pred = harmonic_model(x) + linear_model(x)
                loss = tf.losses.mse(y_true, y_pred) + 0.01 * \
                    tf.reduce_sum(tf.abs(combined_parameters))

            gradients = [float(gradient.numpy().ravel())
                         for gradient in tape.gradient(loss, combined_parameters)]

            for j in range(len(gradients)):
                if gradients[j] > 6.0:
                    gradients[j] = 6.0
                if gradients[j] < -6.0:
                    gradients[j] = -6.0

            for j in range(len(combined_parameters)):
                parameter = combined_parameters[j]
                parameter_gradient = gradients[j]

                parameter.assign_sub(tf.constant(
                    np.array([parameter_gradient * learning_rate], np.float32)))

            # predictions = y_pred.numpy()
            # float(loss.numpy())

        if loss < best_loss:
            best_loss = loss
            def best_model(x): return harmonic_model(x) + linear_model(x)
            best_model_parameters = combined_parameters
            best_n = n
            best_p = p

        progress_bar.update()
    progress_bar.close()

    if best_model is not None:
        print(f'best model mse loss: {best_loss}')
        print(f'n, p = {best_n}, {best_p}')
        next_date_x = tf.constant([len(values),
                                   len(values) + 1,
                                   len(values) + 2,
                                   len(values) + 3,
                                   len(values) + 4,
                                   len(values) + 5], dtype=np.float32)
        days = np.array(
            np.round(mu + best_model(next_date_x).numpy()), np.int32)

        total = 0
        for d in days:
            total += d
            print(
                f'next predicted date: {dates[-1] + timedelta(days=int(total))}')

        plt.plot([len(values),
                  len(values) + 1,
                  len(values) + 2,
                  len(values) + 3,
                  len(values) + 4,
                  len(values) + 5], days, color='red')
    plt.show()


def find_model_coefficients_optimal(x, y, p, fourier_terms):
    n = len(x)

    i = np.arange(start=1, stop=n + 1, step=1)
    frequency = 2 * math.pi / p
    x_sum = np.sum(x)

    y_vector = [np.sum(y)]
    b_vector = [n, n * x_sum]
    for k in range(1, fourier_terms + 1):
        b_vector.append(np.sum(np.cos(k * frequency * x)))
    for k in range(1, fourier_terms + 1):
        b_vector.append(np.sum(np.sin(k * frequency * x)))

    y_vector.append(np.sum(y * x))
    a_vector = [x_sum, np.sum(np.square(x))]
    for k in range(1, fourier_terms + 1):
        a_vector.append(np.sum(np.cos(k * frequency * x) * x))
    for k in range(1, fourier_terms + 1):
        a_vector.append(np.sum(np.sin(k * frequency * x) * x))

    matrix = [b_vector, a_vector]

    for j in range(1, fourier_terms + 1):
        derivative = np.cos(j * frequency * x)
        y_vector.append(np.sum(y * derivative))
        cos_j_vector = [np.sum(np.cos(j * frequency * x)),
                        np.sum(derivative * x)]
        for k in range(1, fourier_terms + 1):
            cos_j_vector.append(np.sum(np.cos(k * frequency * x) * derivative))
        for k in range(1, fourier_terms + 1):
            cos_j_vector.append(np.sum(np.sin(k * frequency * x) * derivative))
        matrix.append(cos_j_vector)

    for j in range(1, fourier_terms + 1):
        derivative = np.sin(j * frequency * x)
        y_vector.append(np.sum(y * derivative))
        sin_j_vector = [np.sum(derivative), np.sum(derivative * x)]
        for k in range(1, fourier_terms + 1):
            sin_j_vector.append(np.sum(np.cos(k * frequency * x) * derivative))
        for k in range(1, fourier_terms + 1):
            sin_j_vector.append(np.sum(np.sin(k * frequency * x) * derivative))
        matrix.append(sin_j_vector)

    matrix = np.array(matrix, np.float32)
    y_vector = np.array(y_vector, np.float32)

    # return np.matmul(np.linalg.inv(matrix), y_vector)
    return np.linalg.lstsq(matrix, y_vector)


def predict_next_date(dates: list[datetime]):
    time_deltas = []

    for i in range(1, len(dates)):
        time_delta = dates[i] - dates[i - 1]
        time_deltas.append(time_delta.days)

    values = np.array(time_deltas, np.float32)
    print(values)

    values_x = np.array([i for i in range(len(values))], np.float32)
    plt.plot(values_x, values, color='blue')

    n = 10
    p = 2
    coefficients = find_model_coefficients_optimal(values_x, values, p, n)[0]
    print(coefficients)

    def model(x):
        result = coefficients[0] + coefficients[1] * x
        frequency = 2 * math.pi / p
        for i in range(1, n + 1):
            result += coefficients[i + 1] * math.cos(i * frequency * x)
        for i in range(1, n + 1):
            result += coefficients[n + i + 1] * math.sin(i * frequency * x)
        return result

    plt.plot(values_x, values, color='blue')
    plt.plot(values_x, [model(i) for i in values_x], color='red')
    plt.show()
    # days = int(round(model(len(values))))
    # print(dates[-1] + timedelta(days=days))


def symbolic_differentiation(n, p):
    frequency = 2 * math.pi / p

    b = sp.symbols('b')
    a = sp.symbols('a')

    cosine_coefficients = [sp.symbols(f'a{k}') for k in range(1, n + 1)]
    sine_coefficients = [sp.symbols(f'b{k}') for k in range(1, n + 1)]

    x = sp.symbols('x')

    model = b + a * x

    for k in range(1, n + 1):
        model += cosine_coefficients[k - 1] * math.cos(k * frequency * x)
    for k in range(1, n + 1):
        model += sine_coefficients[k - 1] * math.sin(k * frequency * x)

    print(sp.diff(model, b))


def predict_next_date_xgb(dates: list[datetime]):
    time_deltas = []

    for i in range(1, len(dates)):
        time_delta = dates[i] - dates[i - 1]
        time_deltas.append(time_delta.days)

    smoothed = []

    for i in range(1, len(time_deltas) - 1):
        average = 0.0
        for j in range(-1, 2):
            average += time_deltas[i + j]
        average /= 3
        smoothed.append(average)

    a, b = np.polyfit(
        list(range(1, len(time_deltas) - 1)), smoothed, 1)

    x = []
    y = []

    for i in range(len(smoothed) - 1):
        x.append(time_deltas[i + 1] - smoothed[i])
        y.append(time_deltas[i + 2] - smoothed[i + 1])

    x = np.array(x, np.float32).reshape(-1, 1)
    y = np.array(y, np.float32)

    model = XGBRegressor(learning_rate=2e-2,
                         objective='reg:squarederror', n_estimators=1000)
    model.fit(x, y)

    # predictions = model.predict(x)
    # print(predictions)

    indices = list(range(len(time_deltas)))
    plt.plot(indices, time_deltas, color='blue')
    # plt.plot(indices, predictions, color='red')
    last_x = y[-1]
    predictions = []
    for i in range(7):
        prediction = model.predict(np.array([[last_x]]))
        predictions.append(float(prediction[0]))
        last_x = float(prediction[0])
    predictions = predictions[1:]

    adjusted = []
    current_total = 0
    i = len(time_deltas)
    for prediction in predictions:
        current_total = int(round(prediction)) + a * i + b
        adjusted.append(current_total)
        print(
            f'next predicted date: {dates[-1] + timedelta(days=current_total)}')
        i += 1

    plt.plot([len(time_deltas) + i for i in range(6)],
             adjusted, color='red')
    plt.show()


def find_optimal_xgboost_model(data, evaluation_split=0.2):
    max_feature_vector_length = min(7, len(data) // 2)
    min_moving_average_window_size = 3
    max_moving_average_window_size = min(
        7, int(round(len(data) * evaluation_split)))
    if max_moving_average_window_size % 2 == 0:
        max_moving_average_window_size -= 1

    best_feature_vector_length = -1
    min_error = sys.float_info.max
    best_smoothed = None
    best_ma_window_size = -1

    progress_bar = tqdm(
        total=max_feature_vector_length * (max_moving_average_window_size - min_moving_average_window_size + 1))
    for ma_window_size in range(min_moving_average_window_size, max_moving_average_window_size + 1):
        smoothed = []

        half_ma_window_size = ma_window_size // 2
        for i in range(half_ma_window_size, len(data) - half_ma_window_size):
            average = 0.0
            for j in range(-half_ma_window_size, half_ma_window_size + 1):
                average += data[i + j]
            average /= ma_window_size
            smoothed.append(average)

        evaluation_data_size = int(round(len(smoothed) * evaluation_split))
        training_data_size = len(smoothed) - evaluation_data_size

        smoothed_training_data = smoothed[:training_data_size]
        smoothed_evaluation_data = smoothed[training_data_size:]

        end_pos = half_ma_window_size + training_data_size
        non_smoothed_training_data = data[half_ma_window_size: end_pos]
        non_smoothed_evaluation_data = data[end_pos: end_pos +
                                            evaluation_data_size]

        residual_training_data = [
            x - y for x, y in zip(non_smoothed_training_data, smoothed_training_data)]
        residual_evaluation_data = [
            x - y for x, y in zip(non_smoothed_evaluation_data, smoothed_evaluation_data)]

        for feature_vector_length in range(1, max_feature_vector_length + 1):
            if feature_vector_length >= len(residual_evaluation_data):
                progress_bar.update()
                continue

            model, error = fit_and_evaluate_model(residual_training_data,
                                                  residual_evaluation_data,
                                                  feature_vector_length)
            if error < min_error:
                best_feature_vector_length = feature_vector_length
                min_error = error
                best_smoothed = smoothed
                best_ma_window_size = ma_window_size
            progress_bar.update()
    progress_bar.close()

    half_ma_window_size = best_ma_window_size // 2
    a, b = np.polyfit(list(range(half_ma_window_size, len(
        data) - half_ma_window_size)), best_smoothed, 1)

    residual_training_data = [x - y for x, y in
                              zip(data[half_ma_window_size: len(data) - half_ma_window_size], best_smoothed)]

    x_train = []
    y_train = []
    for i in range(best_feature_vector_length, len(residual_training_data)):
        feature_vector = residual_training_data[i -
                                                best_feature_vector_length:i]
        x_train.append(feature_vector)
        y_train.append([residual_training_data[i]])

    x_train = np.array(x_train, np.float32)
    y_train = np.array(y_train, np.float32)

    model = XGBRegressor(learning_rate=2e-2,
                         objective='reg:squarederror',
                         n_estimators=1000)

    model.fit(x_train, y_train)

    smoothed_extrapolation = [
        a * (len(data) - half_ma_window_size + i) + b for i in range(half_ma_window_size)]

    smoothed_data = best_smoothed[half_ma_window_size -
                                  best_feature_vector_length:] + smoothed_extrapolation
    non_smoothed_data = data[-best_feature_vector_length:]
    residual_data = [x - y for x, y in zip(non_smoothed_data, smoothed_data)]

    next_prediction = float(model.predict(
        np.array([residual_data]))[0]) + a * len(data) + b

    return model, next_prediction


def fit_and_evaluate_model(training_data,
                           evaluation_data,
                           xgboost_parameters,
                           feature_vector_length=1):
    x_train = []
    y_train = []
    for i in range(feature_vector_length, len(training_data)):
        feature_vector = training_data[i - feature_vector_length:i]
        x_train.append(feature_vector)
        y_train.append([training_data[i]])

    x_train = np.array(x_train, np.float32)
    y_train = np.array(y_train, np.float32)

    model = XGBRegressor(**xgboost_parameters)
    model.fit(x_train, y_train)

    concatenated_evaluation_data = training_data[-feature_vector_length:] + \
        evaluation_data

    x_test = []
    y_test = []
    for i in range(feature_vector_length, len(concatenated_evaluation_data)):
        feature_vector = concatenated_evaluation_data[i -
                                                      feature_vector_length:i]
        x_test.append(feature_vector)
        y_test.append([concatenated_evaluation_data[i]])

    x_test = np.array(x_test, np.float32)
    y_test = np.array(y_test, np.float32)
    y_hat = model.predict(x_test)

    error = np.mean(np.square(y_hat.ravel() - y_test.ravel()))
    return model, error


def find_optimal_xgboost_model(data, evaluation_split=0.2):
    max_feature_vector_length = min(7, len(data) // 2)
    min_moving_average_window_size = 3
    max_moving_average_window_size = min(
        7, int(round(len(data) * evaluation_split)))
    if max_moving_average_window_size % 2 == 0:
        max_moving_average_window_size -= 1

    best_feature_vector_length = -1
    min_error = sys.float_info.max
    best_smoothed = None
    best_ma_window_size = -1
    best_xgboost_parameters = None

    xgboost_parameters_grid = {
        'learning_rate': [0.01, 0.001],
        'max_depth': [2, 5],
        'min_child_weight': [1, 3, 5],
        'gamma': [0.1, 0.2, 0.3, 0.4],
        'colsample_bytree': [0.1, 0.3, 0.5],
        'n_estimators': [2000]
    }

    total_parameter_combinations = max_feature_vector_length
    total_parameter_combinations *= (max_moving_average_window_size -
                                     min_moving_average_window_size + 1)

    total_parameter_grid_combinations = 1
    parameter_index_key = {}
    parameters_grid_indices = []
    i = 0
    for key in xgboost_parameters_grid:
        n = len(xgboost_parameters_grid[key])
        parameters_grid_indices.append(range(n))
        parameter_index_key[i] = key
        total_parameter_grid_combinations *= n
        i += 1
    parameter_combinations = itertools.product(*parameters_grid_indices)
    total_parameter_combinations *= total_parameter_grid_combinations

    for ma_window_size in range(min_moving_average_window_size, max_moving_average_window_size + 1):
        if ma_window_size % 2 == 0:
            continue

        smoothed = []

        half_ma_window_size = ma_window_size // 2
        for i in range(half_ma_window_size, len(data) - half_ma_window_size):
            average = 0.0
            for j in range(-half_ma_window_size, half_ma_window_size + 1):
                average += data[i + j]
            average /= ma_window_size
            smoothed.append(average)

        evaluation_data_size = int(round(len(smoothed) * evaluation_split))
        training_data_size = len(smoothed) - evaluation_data_size

        smoothed_training_data = smoothed[:training_data_size]
        smoothed_evaluation_data = smoothed[training_data_size:]

        end_pos = half_ma_window_size + training_data_size
        non_smoothed_training_data = data[half_ma_window_size: end_pos]
        non_smoothed_evaluation_data = data[end_pos: end_pos +
                                            evaluation_data_size]

        residual_training_data = [
            x - y for x, y in zip(non_smoothed_training_data, smoothed_training_data)]
        residual_evaluation_data = [
            x - y for x, y in zip(non_smoothed_evaluation_data, smoothed_evaluation_data)]

        for feature_vector_length in range(1, max_feature_vector_length + 1):
            if feature_vector_length >= len(residual_evaluation_data):
                continue

            for parameter_combination in parameter_combinations:
                xgboost_parameters = {}
                for i in range(len(parameter_combination)):
                    param_key = parameter_index_key[i]
                    xgboost_parameters[param_key] = xgboost_parameters_grid[param_key][parameter_combination[i]]

                model, error = fit_and_evaluate_model(residual_training_data,
                                                      residual_evaluation_data,
                                                      xgboost_parameters,
                                                      feature_vector_length)
                if error < min_error:
                    min_error = error
                    best_feature_vector_length = feature_vector_length
                    best_ma_window_size = ma_window_size
                    best_smoothed = smoothed
                    best_xgboost_parameters = xgboost_parameters

    half_ma_window_size = best_ma_window_size // 2
    a, b = np.polyfit(list(range(half_ma_window_size, len(
        data) - half_ma_window_size)), best_smoothed, 1)
    residual_training_data = [x - y for x, y in
                              zip(data[half_ma_window_size: len(data) - half_ma_window_size], best_smoothed)]

    x_train = []
    y_train = []
    for i in range(best_feature_vector_length, len(residual_training_data)):
        feature_vector = residual_training_data[i -
                                                best_feature_vector_length:i]
        x_train.append(feature_vector)
        y_train.append([residual_training_data[i]])

    x_train = np.array(x_train, np.float32)
    y_train = np.array(y_train, np.float32)

    model = XGBRegressor(**best_xgboost_parameters)
    model.fit(x_train, y_train)

    parameters = {
        'linear_trend_parameters': (a, b),
        'feature_vector_length': best_feature_vector_length,
        'ma_window_size': best_ma_window_size
    }
    parameters.update(best_xgboost_parameters)

    return model, parameters, min_error


def recursive_multi_step_forecast(data, steps, model, parameters):
    ma_window_size = parameters['ma_window_size']
    feature_vector_length = parameters['feature_vector_length']
    a, b = parameters['linear_trend_parameters']
    half_ma_window_size = ma_window_size // 2

    smoothed = []

    half_ma_window_size = ma_window_size // 2
    for i in range(half_ma_window_size, len(data) - half_ma_window_size):
        average = 0.0
        for j in range(-half_ma_window_size, half_ma_window_size + 1):
            average += data[i + j]
        average /= ma_window_size
        smoothed.append(average)

    smoothed_extrapolation = [
        a * (len(data) - half_ma_window_size + i) + b for i in range(half_ma_window_size)]

    smoothed_data = smoothed[half_ma_window_size -
                             feature_vector_length:] + smoothed_extrapolation
    non_smoothed_data = data[-feature_vector_length:]
    residual_data = [x - y for x, y in zip(non_smoothed_data, smoothed_data)]

    multi_step_residuals = []
    multi_step_trend = []

    for i in range(steps):
        next_prediction_residual = float(
            model.predict(np.array([residual_data]))[0])
        next_prediction_linear = a * (len(data) + i) + b

        multi_step_residuals.append(next_prediction_residual)
        multi_step_trend.append(next_prediction_linear)
        residual_data = residual_data[1:] + [next_prediction_residual]

    multi_step_residuals = np.array(multi_step_residuals, np.float32)
    multi_step_trend = np.array(multi_step_trend, np.float32)

    return list(multi_step_residuals + multi_step_trend)


def test_xgboost():
    start_date = datetime(2020, 1, 1)
    dates = [start_date]

    current_date = start_date
    for i in range(1, 30):
        current_date += timedelta(days=5 * i + random.randint(-2, 2))
        dates.append(current_date)

    for date in dates:
        print(date)

    time_deltas = []

    for i in range(1, len(dates)):
        time_delta = dates[i] - dates[i - 1]
        time_deltas.append(time_delta.days)

    start_time = time.time()
    model, parameters, error = find_optimal_xgboost_model(time_deltas)
    end_time = time.time()

    print(f'model error: {error}')
    print(f'training time: {end_time - start_time} seconds')

    next_predictions = recursive_multi_step_forecast(
        time_deltas, 5, model, parameters)

    current_total = 0
    for next_prediction in next_predictions:
        current_total += int(round(next_prediction))
        print('next date:', dates[-1] + timedelta(days=current_total))


if __name__ == '__main__':
    # main()
    test_xgboost()


def main():
    # dates = [datetime(2020, 9, 30),
    #          datetime(2020, 10, 28),
    #          datetime(2020, 11, 30),
    #          datetime(2020, 12, 31),
    #          datetime(2021, 2, 1),
    #          datetime(2021, 2, 27),
    #          datetime(2021, 4, 3),
    #          datetime(2021, 5, 3),
    #          datetime(2021, 5, 18),
    #          datetime(2021, 6, 16),
    #          datetime(2021, 7, 15),
    #          datetime(2021, 8, 17),
    #          datetime(2021, 9, 21),
    #          datetime(2021, 11, 6),
    #          datetime(2021, 11, 16),
    #          datetime(2021, 12, 16),
    #          datetime(2022, 1, 20),
    #          datetime(2022, 2, 16),
    #          datetime(2022, 3, 23),
    #          datetime(2022, 4, 17),
    #          datetime(2022, 5, 18)]

    # dates = [datetime(2020, 1, 15),
    #          datetime(2020, 8, 1),
    #          datetime(2020, 9, 30),
    #          datetime(2021, 1, 15),
    #          datetime(2021, 8, 1),
    #          datetime(2021, 9, 30),
    #          datetime(2022, 1, 15),
    #          datetime(2022, 8, 1),
    #          datetime(2022, 9, 30),
    #          datetime(2023, 1, 15),
    #          datetime(2023, 8, 1),
    #          datetime(2023, 9, 30),
    #          datetime(2024, 1, 15),
    #          datetime(2024, 8, 1),
    #          datetime(2024, 9, 30),
    #          datetime(2025, 1, 15),
    #          datetime(2025, 8, 1),
    #          datetime(2025, 9, 30)]

    # x = tf.constant(1)
    # x += 1

    start_date = datetime(2020, 1, 1)
    dates = [start_date]

    current_date = start_date
    for i in range(1, 21):
        current_date += timedelta(days=5 * i + random.randint(-2, 2))
        dates.append(current_date)

    for date in dates:
        print(date)

    # dates = [datetime(2020, 1, 15),
    #          datetime(2020, 1, 16),
    #          datetime(2020, 2, 16),
    #          datetime(2020, 2, 17),
    #          datetime(2020, 3, 17),
    #          datetime(2020, 3, 18),
    #          datetime(2020, 4, 19),
    #          datetime(2020, 4, 20),
    #          datetime(2020, 5, 21),
    #          datetime(2020, 5, 22)]

    # dates = [datetime(2020, 1, 1),
    #          datetime(2020, 2, 15),
    #          datetime(2020, 3, 1)]

    predict_next_date_xgb(dates)


if __name__ == '__main__':
    main()
    # symbolic_differentiation(3, 3)
