import cv2
import tensorflow as tf
from tensorflow.keras.models import load_model
import numpy as np

def remove_white_border(binary_image):
    mask = (255 - binary_image) > 0

    height, width = binary_image.shape[0:2]
    mask1, mask2 = mask.any(0), mask.any(1)
    x1, x2 = mask1.argmax(), width - mask1[::-1].argmax()
    y1, y2 = mask2.argmax(), height - mask2[::-1].argmax()

    return binary_image[y1:y2, x1:x2]


def change_image_quality(image, quality=50):
    encoded = cv2.imencode('.jpg', image, [int(cv2.IMWRITE_JPEG_QUALITY), quality])[1]
    return cv2.imdecode(encoded, cv2.IMREAD_GRAYSCALE)


def main():
    # image = cv2.imread('test.bmp', cv2.IMREAD_GRAYSCALE)
    # image = cv2.resize(image, (748, 200), interpolation=cv2.INTER_CUBIC)
    # cv2.imwrite('test-opencv.bmp', image)
    # image = 255 - remove_white_border(255 - image)
    # cv2.imwrite('test.bmp', image)
    # return
    autoencoder = load_model('autoencoder_14_0.0006.h5', compile=False)

    image = cv2.imread('test-net-core.bmp', cv2.IMREAD_GRAYSCALE)
    image = 255 - remove_white_border(255 - image)

    image = cv2.resize(image, (748, 200), interpolation=cv2.INTER_CUBIC)
    image[image > 128] = 255
    image[image <= 128] = 0

    image = np.array(255 - image, np.float32) / 255.0

    result = autoencoder.predict(np.array([image.reshape(200, 748, 1)]))[0]

    result = result.reshape(200, 748)
    result *= 255
    result = 255 - np.array(result, np.uint8)
    result = cv2.threshold(
        result, 0, 255, cv2.THRESH_OTSU + cv2.THRESH_BINARY)[1]
    
    cv2.imwrite('output-net-core.bmp', result)


if __name__ == '__main__':
    main()
