﻿using System.Diagnostics;
using Progressoft.AppUtils.Core;
using Progressoft.ML.OpenVinoSharp.Core;

namespace BenchmarkApplicationNew;

internal class Program
{
    static void Main(string[] args)
    {
        if (!File.Exists("autoencoder.xml"))
        {
            Console.WriteLine("model does not exist");
            return;
        }

        var modelId = "densenet201";
        var modelInfoResult = OpenVinoModelInfo.Create(modelId,
            File.ReadAllText("autoencoder.xml"),
            File.ReadAllBytes("autoencoder.bin"));

        if (modelInfoResult.IsFailure)
        {
            Console.WriteLine("Error creating model info");
            Console.WriteLine(modelInfoResult.ErrorException);
            return;
        }

        var modelInfo = modelInfoResult.Value;
        var registerModelResult = OpenVinoApi.RegisterModel(modelInfo, OpenVinoDevice.CPU);

        if (registerModelResult.IsFailure)
        {
            Console.WriteLine("Error registering model");
            Console.WriteLine(modelInfoResult.ErrorException);
            return;
        }


        List<OpenVinoTensor> tensors = new List<OpenVinoTensor>();

        float[][][] data = (float[][][]) ArrayUtils.CreateArray<float>(200, 748, 1);

        tensors.Add(OpenVinoTensor.Create("input_1", data).Value);

        var stopwatch = new Stopwatch();

        stopwatch.Start();

        int limit = 1000;
        for (int i = 0; i < limit; i++)
        {
            var openVinoTensorMap = OpenVinoTensorMap.Create(tensors).Value;
            var openVinoBatch = OpenVinoBatch.Create(new List<OpenVinoTensorMap>() {openVinoTensorMap});
            var inferenceResult = OpenVinoApi.Infer(modelId, openVinoBatch.Value);

            if (inferenceResult.IsFailure)
            {
                Console.WriteLine("Error inferring result");
                Console.WriteLine(modelInfoResult.ErrorException);
                return;
            }

            var output = inferenceResult.Value.BlobMaps[0];
            if (output.Tensors.Count != 1)
            {
                throw new Exception("Expected 1 inference output");
            }
        }

        stopwatch.Stop();
        var stopwatchElapsedMilliseconds = stopwatch.ElapsedMilliseconds;
        var inferenceTime = ((double) stopwatchElapsedMilliseconds) / ((double) limit);
        Console.WriteLine($"Time: {(long) inferenceTime}");
    }
}