#!/usr/bin/python3.6

from typing import Tuple, Union

import cv2
import numpy as np

TEMPLATE_SCORE_THRESHOLD = 0.55
OUT_OF_FOCUS_THRESHOLD = 90
BRIGHTNESS_THRESHOLD = 75
CONTRAST_THRESHOLD = 20


def resize(image: np.ndarray, new_size: Tuple[int, int]) -> np.ndarray:
    return cv2.resize(image, new_size, interpolation=cv2.INTER_CUBIC)


def closing_morphology(image: np.ndarray,
                       kernel_dimension_size: int = 25,
                       power: int = 3) -> np.ndarray:
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT,
                                       (kernel_dimension_size,
                                        kernel_dimension_size))
    image = cv2.dilate(image, kernel, iterations=power)
    image = cv2.erode(image, kernel, iterations=power)
    return image


def opening_morphology(image: np.ndarray,
                       kernel_dimension_size: int = 7,
                       power: int = 3) -> np.ndarray:
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT,
                                       (kernel_dimension_size,
                                        kernel_dimension_size))
    image = cv2.erode(image, kernel, iterations=power)
    image = cv2.dilate(image, kernel, iterations=power)
    return image


def dilate(image, kernel_dimension_size=3):
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT,
                                       (kernel_dimension_size,
                                        kernel_dimension_size))
    return cv2.dilate(image, kernel)


def clean_binary_image(binary_image: np.ndarray) -> np.ndarray:
    labels_count, labeled_image, stats, centroids = cv2.connectedComponentsWithStats(binary_image, 8, cv2.CV_32S)
    new_image = np.zeros_like(binary_image)
    for i in range(1, labels_count):
        if stats[:, -1][i] < 10:
            continue
        new_image += np.array(labeled_image == i, np.uint8) * 255
    binary_image = closing_morphology(new_image, 11, 1)
    return dilate(binary_image, 3)


def find_largest_contour(image: np.ndarray) -> np.ndarray:
    contours, hierarchy = cv2.findContours(image, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

    max_area = -1
    largest_contour = None
    for contour in contours:
        contour_area = cv2.moments(contour)['m00']
        if contour_area > max_area:
            max_area = contour_area
            largest_contour = contour

    return largest_contour.reshape(-1, 2)


def find_best_contour(image: np.ndarray,
                      id_card_width_height_ratio: float) -> Union[np.ndarray, None]:
    contours, hierarchy = cv2.findContours(image, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
    image_area = image.shape[0] * image.shape[1]

    best_contour = None
    max_score = 0.0
    for contour in contours:
        _, (width, height), _ = cv2.minAreaRect(contour)

        area = width * height
        if area / image_area < 0.1:
            continue

        ratio = max(width / height, height / width)
        score = 1.0 / (1.0 + abs(ratio - id_card_width_height_ratio))
        if score > max_score:
            max_score = score
            best_contour = contour

    if best_contour is None:
        return None

    return best_contour.reshape(-1, 2)


def otsu_threshold(image_gray: np.ndarray) -> np.ndarray:
    return cv2.threshold(image_gray,
                         0,
                         255,
                         cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]


def warp_perspective(image: np.ndarray,
                     original_points: list[Tuple[float, float]],
                     target_points: list[Tuple[int, int]],
                     target_width: int,
                     target_height: int) -> np.ndarray:
    original_points = np.float32(original_points)
    target_points = np.float32(target_points)

    perspective_transform_matrix = cv2.getPerspectiveTransform(original_points, target_points)

    return cv2.warpPerspective(image,
                               perspective_transform_matrix,
                               (target_width, target_height),
                               flags=cv2.INTER_CUBIC)


def image_brightness(gray_image):
    hist = np.histogram(gray_image, [i for i in range(256)])[0]
    ratios = hist / np.sum(hist)
    weighted_ratios = ratios * np.array([i for i in range(255)], np.float)
    return np.sum(weighted_ratios)


def image_contrast(gray_image):
    return np.std(gray_image)


def is_image_brightness_within_limits(image):
    gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    # print(f'image_brightness: {image_brightness(gray_image)}')
    return image_brightness(gray_image) >= BRIGHTNESS_THRESHOLD


def is_image_contrast_within_limits(image):
    gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    # print(f'image_contrast: {image_contrast(gray_image)}')
    return image_contrast(gray_image) >= CONTRAST_THRESHOLD


def is_image_out_of_focus(image):
    variance_of_laplacian = cv2.Laplacian(image, cv2.CV_64F).var()
    # print(f'variance_of_laplacian: {variance_of_laplacian}')
    return variance_of_laplacian < OUT_OF_FOCUS_THRESHOLD


def remove_noise(binary_image: np.ndarray, max_noise_size: int = 20) -> np.ndarray:
    labels_count, labeled_image, stats, centroids = cv2.connectedComponentsWithStats(binary_image, 8, cv2.CV_32S)
    new_image = np.zeros_like(binary_image)
    for i in range(1, labels_count):
        if stats[:, -1][i] <= max_noise_size:
            continue
        new_image += np.array(labeled_image == i, np.uint8) * 255
    return new_image


def text_smearing(image: np.ndarray) -> np.ndarray:
    image_hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    intensities = image_hsv[:, :, -1]

    blurred = cv2.GaussianBlur(intensities, (3, 3), 0)
    black_hat_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))
    threshold = cv2.morphologyEx(blurred, cv2.MORPH_BLACKHAT, black_hat_kernel)

    sobel_gradient = cv2.Sobel(255 - threshold, cv2.CV_64F, 1, 0, ksize=5)
    sobel_gradient = np.abs(sobel_gradient)

    min_gradient = np.min(sobel_gradient)
    max_gradient = np.max(sobel_gradient)
    sobel_gradient = (sobel_gradient - min_gradient) / (max_gradient - min_gradient)

    sobel_gradient = np.array(sobel_gradient * 255, np.uint8)
    threshold = otsu_threshold(sobel_gradient)

    closing_kernel = np.ones((7, 7), np.uint8)
    threshold = cv2.erode(cv2.dilate(threshold, closing_kernel), closing_kernel)
    threshold = remove_noise(threshold)
    threshold = cv2.dilate(threshold, np.ones((3, 3), np.uint8))
    threshold = cv2.dilate(threshold, np.ones((3, 7), np.uint8), iterations=2)
    threshold = cv2.erode(threshold, np.ones((3, 7), np.uint8), iterations=2)
    threshold = cv2.dilate(threshold, np.ones((1, 7), np.uint8), iterations=2)
    threshold = cv2.erode(threshold, np.ones((1, 7), np.uint8), iterations=2)

    return threshold
