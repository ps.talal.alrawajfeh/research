import cv2
import numpy as np
from matplotlib import pyplot as plt

COLOR_SEGMENTATION_MAXIMUM_DIMENSION = 500


def color_distance(color1, color2):
    return np.sqrt(np.sum(np.square(np.subtract(color1, color2))))


def get_connected_region(image, start_point, visits_matrix, tolerance):
    region = [start_point]
    queue = [start_point]

    while len(queue) > 0:
        parent = queue.pop(0)
        visits_matrix[parent[1], parent[0]] = 1

        for dy in range(-1, 2):
            for dx in range(-1, 2):
                x, y = parent
                x += dx
                y += dy
                if x < 0 or x >= image.shape[1]:
                    continue
                if y < 0 or y >= image.shape[0]:
                    continue
                if visits_matrix[y, x] == 1:
                    continue
                distance = color_distance(image[y, x, :], image[parent[1], parent[0], :])
                if distance < tolerance and (x, y) not in queue:
                    queue.append((x, y))
                    region.append((x, y))

    return region


def get_connected_regions(image, tolerance):
    visits_matrix = np.zeros(image.shape[:2], np.uint8)

    regions = []
    for y in range(image.shape[0]):
        for x in range(image.shape[1]):
            if visits_matrix[y, x] == 0:
                regions.append(get_connected_region(image, (x, y), visits_matrix, tolerance))

    return regions


def pre_process(image, power=2):
    output = image
    for i in range(power):
        avg = cv2.medianBlur(output, 3)
        resized = cv2.resize(avg,
                             (avg.shape[1] // 2, avg.shape[0] // 2),
                             interpolation=cv2.INTER_CUBIC)
        output = resized
    return output


def calculate_tolerance(image):
    total_distances = 0.0
    total_square_distances = 0.0
    n = 0
    for y in range(image.shape[0]):
        for x in range(image.shape[1]):
            for dx in range(-1, 2):
                for dy in range(-1, 2):
                    other_x = x + dx
                    other_y = y + dy
                    if other_x < 0 or other_x >= image.shape[1]:
                        continue
                    if other_y < 0 or other_y >= image.shape[0]:
                        continue
                    distance = color_distance(image[y, x], image[other_y, other_x])
                    total_distances += distance
                    total_square_distances += distance ** 2
                    n += 1
    avg = total_distances / n
    std = np.sqrt(total_square_distances / n - avg ** 2)
    print(avg)
    print(std)
    return avg - np.sqrt(std)


def main():
    image_paths = [
        '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Adrian Mikko/WhatsApp Image 2020-06-10 at 9.04.00 AM.jpeg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Adrian Mikko/WhatsApp Image 2020-06-10 at 9.04.00 AM (1).jpeg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Alma Pita/Shared Throught whatsapp/WhatsApp Image 2020-06-10 at 9.41.08 AM (2).jpeg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Alma Pita/Shared Throught whatsapp/WhatsApp Image 2020-06-10 at 9.41.09 AM.jpeg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Anas Abu Shaweesh/20200610_090313.jpg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Anas Abu Shaweesh/20200610_090303.jpg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Anas Abu Shaweesh/20200610_090247.jpg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Botros/20200610_102440.jpg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Botros/20200610_102433.jpg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Fayez Al Wahidi/20200610_091923.jpg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Fayez Al Wahidi/20200610_091910.jpg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Jehad/IMG_20200610_172502.jpg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Jehad/IMG_20200610_172409.jpg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Jehad/IMG_20200610_172359.jpg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/SMART ID non Qatari/Alan/IMG_20200610_110951.jpg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/SMART ID non Qatari/Alan/IMG_20200610_111001.jpg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/SMART ID non Qatari/Jack/smart id pics/20200610_151437.jpg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/SMART ID non Qatari/Jack/smart id pics/20200610_151445.jpg',
        '/home/u764/Development/data/ekyc-data/drive-download-20200830T074731Z-001/IMG_20200830_104154.jpg',
        '/home/u764/Development/data/ekyc-data/drive-download-20200830T074731Z-001/IMG_20210815_155235 (1).jpg',
        '/home/u764/Development/data/ekyc-data/test-data/id_front_14.jpg',
        '/home/u764/Development/data/ekyc-data/test-data/id_front_10.jpg',
        '/home/u764/Development/data/ekyc-data/test-data/id_front_9.jpg',
        '/home/u764/Development/data/ekyc-data/test-data/IMG_20200827_132407.jpg',
        '/home/u764/Development/data/ekyc-data/PSO ID/Amani F.jpg',
        '/home/u764/Development/data/ekyc-data/PSO ID/Amani B.jpg',
        '/home/u764/Development/data/ekyc-data/PSO ID/khaldoun B.jpg',
        '/home/u764/Development/data/ekyc-data/PSO ID/Khaldoun F.jpg',
        '/home/u764/Development/data/ekyc-data/drive-download-20210824T150016Z-001/IMG_20210824_175702_1.jpg',
        '/home/u764/Development/data/ekyc-data/drive-download-20210824T150016Z-001/IMG_20210824_175704.jpg',
        '/home/u764/Development/data/ekyc-data/drive-download-20210824T150016Z-001/IMG_20210824_175708.jpg',
        '/home/u764/Development/data/ekyc-data/drive-download-20210824T150016Z-001/IMG_20210824_175720.jpg',
        '/home/u764/Development/data/ekyc-data/drive-download-20210824T150016Z-001/IMG_20210824_175723.jpg',
        '/home/u764/Development/data/ekyc-data/drive-download-20210824T150016Z-001/IMG_20210824_175725.jpg'
    ]

    for image_path in image_paths:
        image = cv2.imread(image_path)

        image_max_dimension = max(image.shape[:2])
        resize_factor = COLOR_SEGMENTATION_MAXIMUM_DIMENSION / image_max_dimension
        image = cv2.resize(image, (0, 0), fx=resize_factor, fy=resize_factor, interpolation=cv2.INTER_CUBIC)

        output = pre_process(image)
        plt.imshow(output)
        plt.show()

        tolerance = calculate_tolerance(output)

        region_size_cutoff = int(output.shape[0] * output.shape[1] * 0.1)
        regions = get_connected_regions(output, tolerance)
        for region in regions:
            if len(region) < region_size_cutoff:
                continue
            test = np.zeros(output.shape[:2], np.uint8)
            for x, y in region:
                test[y, x] = 1

            plt.imshow(test, plt.cm.gray)
            plt.show()


if __name__ == '__main__':
    main()
