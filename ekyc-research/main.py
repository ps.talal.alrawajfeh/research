import itertools
import math
import os
import time

import cv2
import numpy as np
from matplotlib import pyplot as plt
from sklearn.cluster import MiniBatchKMeans
from sklearn.mixture import GaussianMixture
from skimage.filters import threshold_multiotsu
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
from alphashape import alphashape

SINGLE_POINT = 'point'
ORDINARY_LINE = 'ordinary'
HORIZONTAL_LINE = 'horizontal'
VERTICAL_LINE = 'vertical'

EPSILON = 1e-6

COLOR_SEGMENTATION_MAXIMUM_DIMENSION = 500
ID_CARD_FINAL_WIDTH = 750
from scipy.spatial import ConvexHull


def resize_by_factor(image, factor):
    if 1 - 1e-4 <= factor <= 1 + 1e-4:
        return image
    new_height = int(math.floor(image.shape[0] * factor))
    new_width = int(math.floor(image.shape[1] * factor))

    return cv2.resize(image, (new_width, new_height), interpolation=cv2.INTER_CUBIC)


def closing_morphology(image,
                       kernel_dimension_size=3,
                       power=3):
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,
                                       (kernel_dimension_size,
                                        kernel_dimension_size))
    image = cv2.dilate(image, kernel, iterations=power)
    image = cv2.erode(image, kernel, iterations=power)
    return image


def otsu_threshold(image_gray):
    binary_image = cv2.threshold(image_gray, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
    margin_size = 10
    margin_colors = np.concatenate([binary_image[:, :margin_size].ravel(),
                                    binary_image[:margin_size, :].ravel(),
                                    binary_image[:, -margin_size:].ravel(),
                                    binary_image[-margin_size:, :].ravel()])
    if np.mean(margin_colors) / 255.0 > 0.75:
        binary_image = 255 - binary_image
    return binary_image


def opening_morphology(image,
                       kernel_dimension_size=7,
                       power=3):
    image = cv2.erode(image, np.ones((kernel_dimension_size, kernel_dimension_size)), iterations=power)
    image = cv2.dilate(image, np.ones((kernel_dimension_size, kernel_dimension_size)), iterations=power)
    return image


def color_clustering(image):
    # image_hsv = cv2.GaussianBlur(image, (7, 7), 0)
    # image_hsv = cv2.cvtColor(image_hsv, cv2.COLOR_BGR2HSV)

    color_points = image.reshape(-1, image.shape[-1])
    k_means = MiniBatchKMeans(n_clusters=2, batch_size=int(len(color_points) * 0.0025))
    k_means.fit(color_points)
    new_image = np.array(k_means.labels_, np.uint8).reshape(image.shape[0:2])
    new_image *= 255

    return new_image  # closing_morphology(new_image, 11)


def view_histograms(image_hsv):
    hsv_h = image_hsv[:, :, 0]
    hsv_s = image_hsv[:, :, 1]
    hsv_v = image_hsv[:, :, 2]
    fig, axs = plt.subplots(3)
    axs[0].hist(hsv_h.ravel(), [i * 5 for i in range(180 // 5 + 1)])
    axs[0].title.set_text(f'H: {np.var(hsv_h)}')
    axs[1].hist(hsv_s.ravel(), [i * 5 for i in range(255 // 5 + 1)])
    axs[1].title.set_text(f'S: {np.var(hsv_s)}')
    axs[2].hist(hsv_v.ravel(), [i * 5 for i in range(255 // 5 + 1)])
    axs[2].title.set_text(f'V: {np.var(hsv_v)}')
    plt.show()


def distance(point1, point2):
    return np.sqrt(np.sum(np.square(point1 - point2)))


def compute_center(points):
    return np.array([np.mean(points[:, :1]), np.mean(points[:, 1:])])


def compute_point_score(reference_points, point):
    score = 1.0
    for reference_point in reference_points:
        score *= distance(reference_point, point)

    return score


def find_furthest_point_from(reference_points, points):
    max_point_score = -1.0
    furthest_point = None
    for p in points:
        point_score = compute_point_score(reference_points, p)

        if point_score > max_point_score:
            max_point_score = point_score
            furthest_point = p

    return furthest_point


def find_convex_polygon_vertices_from_contour(polygon_contour,
                                              polygon_center_point,
                                              number_of_vertices):
    vertices = []
    for n in range(number_of_vertices):
        vertex = find_furthest_point_from(np.array([polygon_center_point] + vertices),
                                          polygon_contour)
        vertices.append(vertex)
    return vertices


def triangle_area(point1, point2, point3):
    x1, y1 = point1
    x2, y2 = point2
    x3, y3 = point3

    return math.fabs(0.5 * (x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)))


def calculate_4_polygon_area(point1,
                             point2,
                             point3,
                             point4):
    return triangle_area(point1, point2, point3) + \
           triangle_area(point1, point4, point3)


def draw_points(image,
                points,
                color=(0, 0, 255),
                thickness=20):
    image_copy = np.array(image)
    # image_copy = cv2.cvtColor(image_copy, cv2.COLOR_GRAY2RGB)
    for pt in points:
        cv2.circle(image_copy, tuple(np.array(pt, np.int)), thickness // 2, color, -1)
    return image_copy


def adjust_angle_by_quadrant(angle, point):
    x, y = point
    if x < 0 and y > 0:
        return 180 - angle
    if x < 0 and y < 0:
        return 180 + angle
    if x > 0 and y < 0:
        return 360 - angle
    return angle


def compute_angle(origin, point):
    x1, y1 = point - origin

    if math.fabs(x1) > 1e-6:
        theta = math.fabs(math.atan(y1 / x1)) / math.pi * 180
    else:
        theta = 90

    return adjust_angle_by_quadrant(theta, (x1, y1))


def sort_points_anti_clockwise(origin, points):
    point_angle_pairs = [[point, compute_angle(point, origin)] for point in points]
    point_angle_pairs.sort(key=lambda x: x[1])
    return [point for point, _ in point_angle_pairs]


def get_perimeter(points):
    perimeter = 0
    for i in range(0, len(points)):
        if i + 1 == len(points):
            perimeter += euclidean_distance(points[-1], points[0])
        else:
            pt1 = points[i]
            pt2 = points[i + 1]
            perimeter += euclidean_distance(pt1, pt2)
    return perimeter


def find_largest_contour(image):
    contours, hierarchy = cv2.findContours(image, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

    max_area = -1
    largest_contour = None

    for contour in contours:
        width, height = cv2.minAreaRect(contour)[1]
        area = width * height

        if area > max_area:
            max_area = area
            largest_contour = contour

    return largest_contour.reshape(-1, 2)


def linearity_error(contour, segment_size=20):
    total_error = 0.0
    n = 0

    i = 0
    while i < len(contour) // segment_size:
        initial_point = contour[i * segment_size]
        terminal_point = contour[(i + 1) * segment_size - 1]
        for j in range(i * segment_size + 1, (i + 1) * segment_size):
            x1, y1 = initial_point
            x2, y2 = terminal_point
            x0, y0 = contour[j]
            distance_from_line = abs((x2 - x1) * (y1 - y0) - (x1 - x0) * (y2 - y1)) / euclidean_distance(
                initial_point, terminal_point)
            total_error += distance_from_line
            n += 1
        i += 1

    remainder = len(contour) % segment_size
    if remainder != 0:
        initial_point = contour[i * segment_size]
        terminal_point = contour[len(contour) - 1]
        for j in range(i * segment_size + 1, len(contour) - 1):
            x1, y1 = initial_point
            x2, y2 = terminal_point
            x0, y0 = contour[j]
            distance_from_line = abs((x2 - x1) * (y1 - y0) - (x1 - x0) * (y2 - y1)) / euclidean_distance(
                initial_point, terminal_point)
            total_error += distance_from_line
            n += 1

    if n == 0:
        return 0.0

    return total_error / n


def draw_contour_min_bounding_box(image,
                                  contour,
                                  color=(255, 0, 0),
                                  thickness=1):
    image_copy = np.array(image)
    image_copy = cv2.cvtColor(image_copy, cv2.COLOR_GRAY2RGB)
    cv2.drawContours(image_copy, [contour], -1, color, thickness)
    return image_copy


def get_image_score(binary_image):
    num_labels, labels, stats, centroids = cv2.connectedComponentsWithStats(binary_image, 8, cv2.CV_32S)
    if num_labels == 1:
        return 0.0

    sizes = stats[:, -1]

    max_label = 1
    max_size = sizes[1]
    for i in range(2, num_labels):
        if sizes[i] > max_size:
            max_label = i
            max_size = sizes[i]

    if max_size / (binary_image.shape[0] * binary_image.shape[1]) < 0.125:
        return 0.0

    labels = np.transpose(labels)
    coordinates = np.transpose(np.where(labels == max_label))
    convex_hull = ConvexHull(coordinates)

    score1 = max_size / convex_hull.volume

    x1 = np.min(coordinates[:, 0])
    x2 = np.max(coordinates[:, 0])
    y1 = np.min(coordinates[:, 1])
    y2 = np.max(coordinates[:, 1])

    margin_colors = np.concatenate([binary_image[:, :x1].ravel(),
                                    binary_image[:y1, :].ravel(),
                                    binary_image[:, x2:].ravel(),
                                    binary_image[y2:, :].ravel()])
    score2 = 1.0 - np.mean(margin_colors) / 255.0

    contour = find_largest_contour(binary_image)

    linearity_score = 1 / (1 + linearity_error(contour) ** 3)

    margin_size = 10
    margin_colors = np.concatenate([binary_image[:, :margin_size].ravel(),
                                    binary_image[:margin_size, :].ravel(),
                                    binary_image[:, -margin_size:].ravel(),
                                    binary_image[-margin_size:, :].ravel()])
    score3 = 1.0 - np.mean(margin_colors) / 255.0

    return round(score1 * score2 * score3 * linearity_score, 3)
    # (round(score1, 3), round(score2, 3), round(score3, 3), round(linearity_score, 3))


def extract_polygon(binary_image, convex_hull_vertices):
    mask = np.zeros((binary_image.shape[0], binary_image.shape[1]))
    cv2.fillConvexPoly(mask, convex_hull_vertices, 1)
    mask = mask.astype(np.bool)
    out = np.zeros_like(binary_image)
    out[mask] = binary_image[mask]
    return out


def euclidean_distance(point1, point2):
    return np.sqrt(np.sum(np.square(np.subtract(point2, point1))))


def dilate(image, kernel_dimension_size=3):
    return cv2.dilate(image, np.ones((kernel_dimension_size, kernel_dimension_size)))


def resize(image, new_size):
    return cv2.resize(image, new_size, interpolation=cv2.INTER_CUBIC)


def find_rectangle_vertices_from_contour(contour, center_point):
    return find_convex_polygon_vertices_from_contour(contour,
                                                     center_point,
                                                     4)


def estimate_parametric_slope_and_intercept(n, points, axis=0):
    sum1 = sum([(i + 1) * points[i][axis] for i in range(n)])
    sum2 = sum([points[i][axis] for i in range(n)])
    slope = 12 * (sum1 - sum2 * (n + 1) / 2) / (n * (n + 1) * (n - 1))
    intercept = sum2 / n - slope * (n + 1) / 2

    return slope, intercept


def parametric_linear_regression(points):
    n = len(points)

    slope_x, intercept_x = estimate_parametric_slope_and_intercept(n, points)
    slope_y, intercept_y = estimate_parametric_slope_and_intercept(n, points, 1)

    return (slope_x, intercept_x), (slope_y, intercept_y)


def parametric_line_to_non_parametric(parametric_line):
    (slope_x, intercept_x), (slope_y, intercept_y) = parametric_line

    if slope_x == 0 and slope_y == 0:
        return SINGLE_POINT, [intercept_x, intercept_y]
    if slope_x == 0:
        return VERTICAL_LINE, [intercept_x]
    if slope_y == 0:
        return HORIZONTAL_LINE, [intercept_y]

    slope = slope_y / slope_x
    return ORDINARY_LINE, [slope, intercept_y - slope * intercept_x]


def get_contour_segments(largest_contour, rectangle_vertices):
    largest_contour = largest_contour.tolist()
    rectangle_vertices = [r.tolist() for r in rectangle_vertices]
    vertex_indices = [largest_contour.index(v) for v in rectangle_vertices]
    segments = []

    for i in range(len(vertex_indices)):
        current_i = vertex_indices[i]
        next_i = vertex_indices[(i + 1) % len(vertex_indices)]

        if current_i < next_i:
            segment = largest_contour[next_i:] + largest_contour[:current_i]
        else:
            segment = largest_contour[next_i:current_i]

        segments.append(segment)

    return segments


def enhance_rectangle_vertices(largest_contour, rectangle_vertices):
    segments = get_contour_segments(largest_contour, rectangle_vertices)

    lines = [parametric_line_to_non_parametric(parametric_linear_regression(segment))
             for segment in segments]

    return np.array([find_lines_intersection_point(lines[i - 1], lines[i])
                     for i in range(len(segments))], np.int)


def within_epsilon_neighborhood(x, compared, epsilon=1e-6):
    return compared - epsilon <= x <= compared + epsilon


def find_lines_intersection_point(line1, line2):
    type1, parameters1 = line1
    type2, parameters2 = line2

    if type1 == SINGLE_POINT and type2 == SINGLE_POINT:
        if parameters1 == parameters2:
            return parameters1
        else:
            return None

    if type1 == SINGLE_POINT and type2 == VERTICAL_LINE:
        if parameters1[0] == parameters2[0]:
            return parameters1
        else:
            return None

    if type1 == SINGLE_POINT and type2 == HORIZONTAL_LINE:
        if parameters1[1] == parameters2[0]:
            return parameters1
        else:
            return None

    if type1 == SINGLE_POINT and type2 == ORDINARY_LINE:
        regressed = parameters1[0] * parameters2[0] + parameters2[1]
        if within_epsilon_neighborhood(regressed, parameters1[1]):
            return parameters1
        else:
            return None

    if type1 == VERTICAL_LINE and type2 == VERTICAL_LINE:
        if parameters1 == parameters2:
            return [parameters1[0], parameters1[0]]
        else:
            return None

    if type1 == VERTICAL_LINE and type2 == HORIZONTAL_LINE:
        return [parameters1[0], parameters2[0]]

    if type1 == VERTICAL_LINE and type2 == ORDINARY_LINE:
        return [parameters1[0], parameters1[0] * parameters2[0] + parameters2[1]]

    if type1 == HORIZONTAL_LINE and type2 == HORIZONTAL_LINE:
        if parameters1 == parameters2:
            return [parameters1[0], parameters1[0]]
        else:
            return None

    if type1 == HORIZONTAL_LINE and type2 == ORDINARY_LINE:
        return [(parameters1[0] - parameters2[1]) / parameters2[0], parameters1[0]]

    if type1 == ORDINARY_LINE and type2 == ORDINARY_LINE:
        x = (parameters2[1] - parameters1[1]) / (parameters1[0] - parameters2[0])
        return [x, parameters1[0] * x + parameters1[1]]

    return find_lines_intersection_point(line2, line1)


def warp_perspective(image,
                     original_points,
                     target_points,
                     target_width,
                     target_height):
    original_points = np.float32(original_points)
    target_points = np.float32(target_points)

    perspective_transform_matrix = cv2.getPerspectiveTransform(original_points, target_points)

    return cv2.warpPerspective(image,
                               perspective_transform_matrix,
                               (target_width, target_height),
                               flags=cv2.INTER_CUBIC)


def estimate_card_width_height_from_vertices(vertices, id_card_width_height_ratio):
    area = calculate_4_polygon_area(*vertices)
    height = math.sqrt(area / id_card_width_height_ratio)
    width = id_card_width_height_ratio * height
    return int(math.floor(width)), int(math.floor(height))


def correct_perspective(image, original_vertices, id_card_width_height_ratio):
    target_width, target_height = estimate_card_width_height_from_vertices(original_vertices,
                                                                           id_card_width_height_ratio)

    target_rectangle_vertices = [[0, 0],
                                 [target_width - 1, 0],
                                 [target_width - 1, target_height - 1],
                                 [0, target_height - 1]]

    return warp_perspective(image,
                            original_vertices,
                            target_rectangle_vertices,
                            target_width,
                            target_height)


def auto_correct_vertices_order(vertices):
    point1, point2, point3, point4 = vertices

    side1_length = euclidean_distance(point1, point2)
    corresponding_side1_length = euclidean_distance(point4, point3)

    side2_length = euclidean_distance(point2, point3)
    corresponding_side2_length = euclidean_distance(point1, point4)

    if max(side1_length, corresponding_side1_length) < min(side2_length, corresponding_side2_length):
        return [point2,
                point3,
                point4,
                point1]

    return vertices


def extract_id_card_from_largest_contour(original_image,
                                         binary_image,
                                         id_card_width_height_ratio,
                                         resize_factor):
    dilated = dilate(binary_image)
    # view_image(dilated)
    largest_contour = np.array(find_largest_contour(dilated), np.float32)
    # view_image(draw_points(dilated, largest_contour, thickness=1))
    center_point = np.array(compute_center(largest_contour), np.float32)
    rectangle_vertices = find_rectangle_vertices_from_contour(largest_contour, center_point)
    # view_image(draw_points(resized, rectangle_vertices, thickness=2))
    rectangle_vertices = sort_points_anti_clockwise(center_point, rectangle_vertices)
    # view_image(draw_points(resized, rectangle_vertices, thickness=2))
    rectangle_vertices = enhance_rectangle_vertices(largest_contour, rectangle_vertices)
    # view_image(draw_points(resized, rectangle_vertices, thickness=2))
    rectangle_vertices = auto_correct_vertices_order(rectangle_vertices)
    # view_image(draw_points(resized, rectangle_vertices, thickness=2))
    corrected_perspective = correct_perspective(original_image,
                                                [[x / resize_factor, y / resize_factor]
                                                 for x, y in rectangle_vertices],
                                                id_card_width_height_ratio)
    # view_image(corrected_perspective)
    id_card_final_height = int(math.floor(ID_CARD_FINAL_WIDTH / id_card_width_height_ratio))
    return resize(corrected_perspective, (ID_CARD_FINAL_WIDTH, id_card_final_height))


def main():
    # images = [
    #     '/home/u764/Downloads/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Adrian Mikko/WhatsApp Image 2020-06-10 at 9.04.00 AM.jpeg',
    #     '/home/u764/Downloads/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Adrian Mikko/WhatsApp Image 2020-06-10 at 9.04.00 AM (1).jpeg',
    #     '/home/u764/Downloads/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Alma Pita/Shared Throught whatsapp/WhatsApp Image 2020-06-10 at 9.41.08 AM (2).jpeg',
    #     '/home/u764/Downloads/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Alma Pita/Shared Throught whatsapp/WhatsApp Image 2020-06-10 at 9.41.09 AM.jpeg',
    #     '/home/u764/Downloads/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Anas Abu Shaweesh/20200610_090313.jpg',
    #     '/home/u764/Downloads/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Anas Abu Shaweesh/20200610_090303.jpg',
    #     '/home/u764/Downloads/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Anas Abu Shaweesh/20200610_090247.jpg',
    #     '/home/u764/Downloads/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Botros/20200610_102440.jpg',
    #     '/home/u764/Downloads/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Botros/20200610_102433.jpg',
    #     '/home/u764/Downloads/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Fayez Al Wahidi/20200610_091923.jpg',
    #     '/home/u764/Downloads/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Fayez Al Wahidi/20200610_091910.jpg',
    #     '/home/u764/Downloads/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Jehad/IMG_20200610_172502.jpg',
    #     '/home/u764/Downloads/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Jehad/IMG_20200610_172409.jpg',
    #     '/home/u764/Downloads/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Jehad/IMG_20200610_172359.jpg',
    #     '/home/u764/Downloads/ekyc-data/OneDrive_2020-08-27/QID Samples/SMART ID non Qatari/Alan/IMG_20200610_110951.jpg',
    #     '/home/u764/Downloads/ekyc-data/OneDrive_2020-08-27/QID Samples/SMART ID non Qatari/Alan/IMG_20200610_111001.jpg',
    #     '/home/u764/Downloads/ekyc-data/OneDrive_2020-08-27/QID Samples/SMART ID non Qatari/Jack/smart id pics/20200610_151437.jpg',
    #     '/home/u764/Downloads/ekyc-data/OneDrive_2020-08-27/QID Samples/SMART ID non Qatari/Jack/smart id pics/20200610_151445.jpg',
    #     '/home/u764/Downloads/ekyc-data/drive-download-20200830T074731Z-001/IMG_20200830_104154.jpg',
    #     '/home/u764/Downloads/ekyc-data/drive-download-20200830T074731Z-001/IMG_20210815_155235 (1).jpg',
    #     '/home/u764/Downloads/ekyc-data/test-data/id_front_14.jpg',
    #     '/home/u764/Downloads/ekyc-data/test-data/id_front_10.jpg',
    #     '/home/u764/Downloads/ekyc-data/test-data/id_front_9.jpg',
    #     '/home/u764/Downloads/ekyc-data/test-data/IMG_20200827_132407.jpg',
    #     '/home/u764/Downloads/ekyc-data/PSO ID/Amani F.jpg',
    #     '/home/u764/Downloads/ekyc-data/PSO ID/Amani B.jpg',
    #     '/home/u764/Downloads/ekyc-data/PSO ID/khaldoun B.jpg',
    #     '/home/u764/Downloads/ekyc-data/PSO ID/Khaldoun F.jpg',
    # ]
    images = os.listdir('/home/u764/Downloads/Training')
    # images = list(reversed(images))
    start_time = time.time()
    for image_path in images:
        original_image = cv2.imread('/home/u764/Downloads/Training/' + image_path)
        if original_image is None:
            continue
        # plt.imshow(cv2.cvtColor(id_image_bgr, cv2.COLOR_BGR2RGB))
        # plt.show()

        # id_gray = cv2.cvtColor(id_image_bgr, cv2.COLOR_BGR2GRAY)
        # plt.imshow(otsu_threshold(id_gray), plt.cm.gray)
        # plt.show()
        image_max_dimension = max(original_image.shape[:2])
        resize_factor = COLOR_SEGMENTATION_MAXIMUM_DIMENSION / image_max_dimension
        id_image_bgr = resize_by_factor(original_image,
                                        resize_factor)
        # id_image_bgr = cv2.GaussianBlur(id_image_bgr, (7, 7), 0)
        id_image_hsv = cv2.cvtColor(id_image_bgr, cv2.COLOR_BGR2HSV)

        clustered = color_clustering(id_image_hsv)
        # if get_image_score(255 - clustered) > get_image_score(clustered):
        #     clustered = 255 - clustered

        # fig, ax = plt.subplots(1, 3)

        hsv_h = id_image_hsv[:, :, 0]
        hsv_h_binary = closing_morphology(opening_morphology(otsu_threshold(hsv_h), power=1), kernel_dimension_size=11)
        h_score = get_image_score(hsv_h_binary)
        # ax[0].imshow(hsv_h_binary, plt.cm.gray)
        # ax[0].title.set_text(
        #     f'{h_score}')

        hsv_s = id_image_hsv[:, :, 1]
        hsv_s_binary = closing_morphology(opening_morphology(otsu_threshold(hsv_s), power=1), kernel_dimension_size=11)
        s_score = get_image_score(hsv_s_binary)
        # ax[1].imshow(hsv_s_binary, plt.cm.gray)
        # ax[1].title.set_text(
        #     f'{s_score}')
        #
        hsv_v = id_image_hsv[:, :, 2]
        hsv_v_binary = closing_morphology(opening_morphology(otsu_threshold(hsv_v), power=1), kernel_dimension_size=11)
        v_score = get_image_score(hsv_v_binary)
        #
        channels_with_scores = [(hsv_h_binary, h_score), (hsv_s_binary, s_score), (hsv_v_binary, v_score)]
        channels_with_scores.sort(key=lambda x: x[1])
        #

        if channels_with_scores[-1][1] >= 0.5:
            extracted = extract_id_card_from_largest_contour(original_image, channels_with_scores[-1][0], 1.575,
                                                             resize_factor)
            cv2.imwrite(f'/home/u764/Downloads/out/score_{channels_with_scores[-1][1]}_' + image_path,
                        original_image)  # channels_with_scores[-1][0])
            cv2.imwrite(f'/home/u764/Downloads/out/score_{channels_with_scores[-1][1]}' + image_path.split('.')[
                0] + '_extracted.jpg', extracted)  # channels_with_scores[-1][0])

        # ax[2].imshow(hsv_v_binary, plt.cm.gray)
        # ax[2].title.set_text(
        #     f'{v_score}')
        # plt.show()

        # id_image_hsv = id_image_hsv.astype(np.float)
        # id_image_hsv[:, :, 0] = (id_image_hsv[:, :, 0] - np.min(id_image_hsv[:, :, 0])) / np.max(id_image_hsv[:, :, 0])
        # id_image_hsv[:, :, 1] = (id_image_hsv[:, :, 1] - np.min(id_image_hsv[:, :, 1])) / np.max(id_image_hsv[:, :, 1])
        # id_image_hsv[:, :, 2] = (id_image_hsv[:, :, 2] - np.min(id_image_hsv[:, :, 2])) / np.max(id_image_hsv[:, :, 2])

        # view_histograms(id_image_hsv)

        # clustered = opening_morphology(clustered, 3, 1)
        # clustered = closing_morphology(clustered, 7, 2)

        # fig, ax = plt.subplots(1, 2)
        # ax[0].imshow(cv2.cvtColor(id_image_bgr, cv2.COLOR_BGR2RGB))
        # ax[1].imshow(clustered, plt.cm.gray)
        # plt.show()

    end_time = time.time()
    print((end_time - start_time) / len(images))


if __name__ == '__main__':
    main()
