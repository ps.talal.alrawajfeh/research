import math
import os
import time
from typing import Tuple, Union

import cv2
import numpy as np
from matplotlib import pyplot as plt
from numba import njit
from scipy.spatial import ConvexHull
from sklearn.cluster import MiniBatchKMeans
import numba as nb

COLOR_SEGMENTATION_MAXIMUM_DIMENSION = 500
ID_CARD_AREA_TO_IMAGE_AREA_MIN_RATIO = 0.125
MIN_BLACK_PIXELS_TO_AREA_RATIO = 0.1
WHITE_BORDER_PIXEL_RATIO = 0.65
THRESHOLD_CORRECTION_MARGIN_SIZE = 20
ID_CARD_FINAL_WIDTH = 750
EPSILON = 1e-6
K_MEANS_BATCH_SIZE_PERCENTAGE = 0.0025

SINGLE_POINT = 'point'
ORDINARY_LINE = 'ordinary'
HORIZONTAL_LINE = 'horizontal'
VERTICAL_LINE = 'vertical'


def resize_image(image_bgr: np.ndarray, image_max_dimension: int) -> Tuple[float, np.ndarray]:
    if image_max_dimension > COLOR_SEGMENTATION_MAXIMUM_DIMENSION:
        resize_factor = COLOR_SEGMENTATION_MAXIMUM_DIMENSION / image_max_dimension
        resized_image = cv2.resize(image_bgr,
                                   (0, 0),
                                   fx=resize_factor,
                                   fy=resize_factor,
                                   interpolation=cv2.INTER_CUBIC)
    else:
        resize_factor = 1.0
        resized_image = np.array(image_bgr)
    return resize_factor, resized_image


def otsu_threshold(image_gray: np.ndarray) -> np.ndarray:
    return cv2.threshold(image_gray,
                         0,
                         255,
                         cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]


def correct_threshold(binary_image: np.ndarray) -> np.ndarray:
    black_pixels_count = np.count_nonzero(binary_image == 0)
    if black_pixels_count < ID_CARD_AREA_TO_IMAGE_AREA_MIN_RATIO * binary_image.shape[0] * binary_image.shape[1]:
        return binary_image
    margin_size = THRESHOLD_CORRECTION_MARGIN_SIZE
    margin_colors = np.concatenate([binary_image[:, :margin_size].ravel(),
                                    binary_image[:margin_size, :].ravel(),
                                    binary_image[:, -margin_size:].ravel(),
                                    binary_image[-margin_size:, :].ravel()])

    if np.mean(margin_colors) / 255.0 > WHITE_BORDER_PIXEL_RATIO:
        return 255 - binary_image
    return binary_image


def threshold_components(image: np.ndarray) -> Tuple[np.ndarray, ...]:
    return tuple(clean_binary_image(correct_threshold(otsu_threshold(image[:, :, i]))) for i in range(image.shape[-1]))


def find_largest_contour(image: np.ndarray) -> np.ndarray:
    contours, hierarchy = cv2.findContours(image, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

    max_area = -1
    largest_contour = None
    for contour in contours:
        contour_area = cv2.moments(contour)['m00']
        if contour_area > max_area:
            max_area = contour_area
            largest_contour = contour

    return largest_contour.reshape(-1, 2)


def find_best_contour(image: np.ndarray,
                      id_card_width_height_ratio: float) -> Union[np.ndarray, None]:
    contours, hierarchy = cv2.findContours(image, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
    image_area = image.shape[0] * image.shape[1]

    best_contour = None
    max_score = 0.0
    for contour in contours:
        _, (width, height), _ = cv2.minAreaRect(contour)

        area = width * height
        if area / image_area < 0.1:
            continue

        ratio = max(width / height, height / width)
        score = 1.0 / (1.0 + abs(ratio - id_card_width_height_ratio))
        if score > max_score:
            max_score = score
            best_contour = contour

    if best_contour is None:
        return None

    return best_contour.reshape(-1, 2)


@njit
def compute_center_of_gravity(points: np.ndarray) -> np.ndarray:
    return np.array([np.mean(points[:, :1]), np.mean(points[:, 1:])])


@njit
def distance(point1: np.ndarray,
             point2: np.ndarray) -> float:
    return np.sqrt(np.sum(np.square(np.subtract(point1, point2))))


@njit
def find_furthest_point_from(point: np.ndarray,
                             points: np.ndarray) -> Tuple[np.ndarray, int]:
    max_distance = -1.0
    furthest_point = None
    furthest_point_index = -1
    for i in range(len(points)):
        p = points[i]
        point_distance = distance(point, p)
        if point_distance > max_distance:
            max_distance = point_distance
            furthest_point = p
            furthest_point_index = i
    return furthest_point, furthest_point_index


def find_convex_polygon_vertices_from_contour(polygon_contour: np.ndarray,
                                              polygon_center_point: np.ndarray,
                                              number_of_vertices: int) -> Tuple[list, list]:
    vertices = []
    vertices_i = []
    for n in range(number_of_vertices):
        vertex, vertex_i = find_furthest_point_from(np.array([polygon_center_point] + vertices),
                                                    polygon_contour)
        vertices.append(vertex)
        vertices_i.append(vertex_i)
    return vertices, vertices_i


@njit
def calculate_triangle_area(point1: np.ndarray,
                            point2: np.ndarray,
                            point3: np.ndarray) -> float:
    return np.fabs(0.5 * (point1[0] * (point2[1] - point3[1])
                          + point2[0] * (point3[1] - point1[1])
                          + point3[0] * (point1[1] - point2[1])))


def find_convex_4_polygon_vertices(points: np.ndarray) -> Tuple[list, list]:
    center = compute_center_of_gravity(points)

    vertex1, vertex1_index = find_furthest_point_from(center, points)
    vertex2, vertex2_index = find_furthest_point_from(vertex1, points)

    max_distance = -1.0
    vertex3 = None
    vertex3_index = -1
    for i in range(len(points)):
        p = points[i]
        perpendicular_distance = distance_between_point_and_line(vertex1, vertex2, p)
        if perpendicular_distance > max_distance:
            max_distance = perpendicular_distance
            vertex3 = p
            vertex3_index = i

    vertices = [vertex1, vertex2, vertex3]
    vertices_indices = [vertex1_index, vertex2_index, vertex3_index]

    vertices, vertices_indices = sort_points_anti_clockwise(center, vertices, vertices_indices)

    max_area = -1.0
    vertex4 = None
    vertex4_index = -1
    for i in range(len(points)):
        p = points[i]
        area1 = calculate_triangle_area(p, vertices[0], vertices[1])
        area2 = calculate_triangle_area(p, vertices[1], vertices[2])
        area = area1 + area2
        if area > max_area:
            max_area = area
            vertex4 = p
            vertex4_index = i

    return vertices + [vertex4], vertices_indices + [vertex4_index]


def get_contour_segments(largest_contour: np.ndarray, rectangle_vertices_indices: list) -> list[np.ndarray]:
    largest_contour_list = largest_contour.tolist()
    segments = []

    for i in range(len(rectangle_vertices_indices)):
        current_i = rectangle_vertices_indices[i]
        next_i = rectangle_vertices_indices[(i + 1) % len(rectangle_vertices_indices)]
        if current_i < next_i:
            segment = np.array(largest_contour_list[next_i:] + largest_contour_list[:current_i], np.float)
        else:
            segment = np.array(largest_contour_list[next_i:current_i], np.float)
        segments.append(segment)

    return segments


@njit
def distance_between_point_and_line(line_initial_point: np.ndarray,
                                    line_terminal_point: np.ndarray,
                                    point: np.ndarray) -> float:
    x1, y1 = line_initial_point
    x2, y2 = line_terminal_point
    x0, y0 = point
    if distance(line_initial_point, line_terminal_point) < EPSILON:
        return distance(line_initial_point, point)
    return abs((x2 - x1) * (y1 - y0) - (x1 - x0) * (y2 - y1)) / distance(
        line_initial_point, line_terminal_point)


@njit
def adjust_angle_by_quadrant(angle: float, point: tuple[int, int]) -> float:
    x, y = point
    if x < 0 and y > 0:
        return 180 - angle
    if x < 0 and y < 0:
        return 180 + angle
    if x > 0 and y < 0:
        return 360 - angle
    return angle


@njit
def compute_angle(origin: np.ndarray, point: np.ndarray) -> float:
    x1, y1 = np.subtract(point, origin)
    if math.fabs(x1) > 1e-6:
        theta = math.fabs(math.atan(y1 / x1)) / math.pi * 180
    else:
        theta = 90
    return adjust_angle_by_quadrant(theta, (x1, y1))


def sort_points_anti_clockwise(origin: np.array, points: np.array, points_indices: list) -> Tuple[list, list]:
    point_angle_pairs = [[points[i], points_indices[i], compute_angle(points[i], origin)] for i in range(len(points))]
    point_angle_pairs.sort(key=lambda x: x[2])
    return [point for point, _, _ in point_angle_pairs], [point_i for _, point_i, _ in point_angle_pairs]


def clean_binary_image(binary_image: np.ndarray) -> np.ndarray:
    labels_count, labeled_image, stats, centroids = cv2.connectedComponentsWithStats(binary_image, 8, cv2.CV_32S)
    new_image = np.zeros_like(binary_image)
    for i in range(1, labels_count):
        if stats[:, -1][i] < 10:
            continue
        new_image += np.array(labeled_image == i, np.uint8) * 255
    binary_image = closing_morphology(new_image, 11, 1)
    return dilate(binary_image, 3)


def compute_image_score_for_id_card_extraction(binary_image: np.ndarray,
                                               id_card_width_height_ratio: float) -> Tuple[float,
                                                                                           Union[np.ndarray, None]]:
    area = binary_image.shape[0] * binary_image.shape[1]
    black_pixels_count = np.count_nonzero(binary_image == 0)

    if black_pixels_count / area < MIN_BLACK_PIXELS_TO_AREA_RATIO:
        return 0.0, None

    # note: the label 0 is always preserved for labeling the background
    # note: the other foreground labels start from 1 (then 2, 3, etc.)
    labels_count, labeled_image, stats, centroids = cv2.connectedComponentsWithStats(binary_image, 8, cv2.CV_32S)
    if labels_count == 1:
        return 0.0, None

    labeled_image = np.transpose(labeled_image)
    sizes = stats[:, -1]
    max_size = sizes[1]
    max_score = 0.0
    best_coordinates = None
    best_label = 1
    best_size = sizes[1]
    for i in range(1, labels_count):
        if sizes[i] > max_size:
            max_size = sizes[i]
        coordinates = np.transpose(np.where(labeled_image == i))
        _, (width, height), _ = cv2.minAreaRect(coordinates)
        if width * height / area < 0.1:
            continue
        ratio = max(width / height, height / width)
        score = 1.0 / (1.0 + abs(ratio - id_card_width_height_ratio))
        if score > max_score:
            max_score = score
            best_coordinates = coordinates
            best_label = i
            best_size = sizes[i]

    if max_size / area < ID_CARD_AREA_TO_IMAGE_AREA_MIN_RATIO:
        return 0.0, None

    convex_hull = ConvexHull(best_coordinates)
    score1 = best_size / convex_hull.volume

    connected_component = np.array(np.transpose(labeled_image == best_label), np.uint8) * 255
    best_contour = find_largest_contour(connected_component)
    center_point = compute_center_of_gravity(best_contour)
    rectangle_vertices, rectangle_vertices_i = find_convex_4_polygon_vertices(best_contour)
    rectangle_vertices, rectangle_vertices_i = sort_points_anti_clockwise(center_point,
                                                                          rectangle_vertices,
                                                                          rectangle_vertices_i)
    rectangle_vertices = enhance_rectangle_vertices(best_contour, rectangle_vertices_i)

    segments = get_contour_segments(best_contour, rectangle_vertices_i)

    score2 = 1 / (1 + linearity_error(nb.typed.List(segments), rectangle_vertices) ** 3)
    score2 = 2 * score2 / (1 + score2)

    return 0.25 * score1 + 0.75 * score2, rectangle_vertices


@njit
def linearity_error(segments: nb.typed.List[np.ndarray], rectangle_vertices: np.ndarray) -> float:
    total_error = 0
    n = 0
    for i in range(len(rectangle_vertices)):
        current_vertex = rectangle_vertices[i]
        next_vertex = rectangle_vertices[(i + 1) % len(rectangle_vertices)]

        segment = segments[i]
        for j in range(1, len(segment) - 1):
            total_error += distance_between_point_and_line(current_vertex, next_vertex, segment[j])
            n += 1

    return total_error / n


def absolute_product_moment_correlation_coefficient(points: np.ndarray) -> float:
    xs = points[:, 0]
    ys = points[:, 1]
    s_x = np.sum(xs)
    s_y = np.sum(ys)
    s_xx = np.sum(np.square(xs))
    s_yy = np.sum(np.square(ys))
    s_xy = np.sum(xs * ys)
    n = points.shape[0]
    denominator = (n * s_xx - np.square(s_x)) * (n * s_yy - np.square(s_y))
    if denominator < EPSILON:
        return 1.0
    return np.fabs((n * s_xy - s_x * s_y) / np.sqrt(denominator))


@njit
def estimate_parametric_slope_and_intercept(n: int, points: np.ndarray, axis: int = 0) -> Tuple[float, float]:
    coefficients = np.array([i + 1 for i in range(n)])
    sum1 = np.sum(points[:, axis] * coefficients)
    sum2 = np.sum(points[:, axis])
    slope = 12 * (sum1 - sum2 * (n + 1) / 2) / (n * (n + 1) * (n - 1))
    intercept = sum2 / n - slope * (n + 1) / 2
    return slope, intercept


@njit
def parametric_linear_regression(points: np.ndarray) -> Tuple[Tuple[float, float],
                                                              Tuple[float, float]]:
    n = len(points)

    slope_x, intercept_x = estimate_parametric_slope_and_intercept(n, points)
    slope_y, intercept_y = estimate_parametric_slope_and_intercept(n, points, 1)

    return (slope_x, intercept_x), (slope_y, intercept_y)


def parametric_line_to_non_parametric(parametric_line: Tuple[Tuple[float, float],
                                                             Tuple[float, float]]) -> Tuple[str, list[float]]:
    (slope_x, intercept_x), (slope_y, intercept_y) = parametric_line

    if slope_x == 0 and slope_y == 0:
        return SINGLE_POINT, [intercept_x, intercept_y]
    if slope_x == 0:
        return VERTICAL_LINE, [intercept_x]
    if slope_y == 0:
        return HORIZONTAL_LINE, [intercept_y]

    slope = slope_y / slope_x
    return ORDINARY_LINE, [slope, intercept_y - slope * intercept_x]


def find_lines_intersection_point(line1: Tuple[str, list[float]],
                                  line2: Tuple[str, list[float]]) -> Union[list[float], None]:
    type1, parameters1 = line1
    type2, parameters2 = line2

    if type1 == SINGLE_POINT and type2 == SINGLE_POINT:
        if np.max(np.abs(np.subtract(parameters1, parameters2))) < EPSILON:
            return parameters1
        else:
            return None

    if type1 == SINGLE_POINT and type2 == VERTICAL_LINE:
        if abs(parameters1[0] - parameters2[0]) < EPSILON:
            return parameters1
        else:
            return None

    if type1 == SINGLE_POINT and type2 == HORIZONTAL_LINE:
        if abs(parameters1[1] - parameters2[0]) < EPSILON:
            return parameters1
        else:
            return None

    if type1 == SINGLE_POINT and type2 == ORDINARY_LINE:
        regressed = parameters1[0] * parameters2[0] + parameters2[1]
        if abs(regressed - parameters1[1]) < EPSILON:
            return parameters1
        else:
            return None

    if type1 == VERTICAL_LINE and type2 == VERTICAL_LINE:
        if np.max(np.abs(np.subtract(parameters1, parameters2))) < EPSILON:
            return [parameters1[0], parameters1[0]]
        else:
            return None

    if type1 == VERTICAL_LINE and type2 == HORIZONTAL_LINE:
        return [parameters1[0], parameters2[0]]

    if type1 == VERTICAL_LINE and type2 == ORDINARY_LINE:
        return [parameters1[0], parameters1[0] * parameters2[0] + parameters2[1]]

    if type1 == HORIZONTAL_LINE and type2 == HORIZONTAL_LINE:
        if parameters1 == parameters2:
            return [parameters1[0], parameters1[0]]
        else:
            return None

    if type1 == HORIZONTAL_LINE and type2 == ORDINARY_LINE:
        return [(parameters1[0] - parameters2[1]) / parameters2[0], parameters1[0]]

    if type1 == ORDINARY_LINE and type2 == ORDINARY_LINE:
        x = (parameters2[1] - parameters1[1]) / (parameters1[0] - parameters2[0])
        return [x, parameters1[0] * x + parameters1[1]]

    return find_lines_intersection_point(line2, line1)


def enhance_rectangle_vertices(largest_contour: np.ndarray, rectangle_vertices_indices: list[int]) -> np.ndarray:
    segments = get_contour_segments(largest_contour, rectangle_vertices_indices)
    lines = [parametric_line_to_non_parametric(parametric_linear_regression(segment))
             for segment in segments]
    return np.array([find_lines_intersection_point(lines[i - 1], lines[i])
                     for i in range(len(segments))], np.int)


def auto_correct_vertices_order(vertices: np.ndarray) -> np.ndarray:
    point1, point2, point3, point4 = vertices

    side1_length = distance(point1, point2)
    corresponding_side1_length = distance(point4, point3)

    side2_length = distance(point2, point3)
    corresponding_side2_length = distance(point1, point4)

    if max(side1_length, corresponding_side1_length) < min(side2_length, corresponding_side2_length):
        return np.array([point2,
                         point3,
                         point4,
                         point1])

    return vertices


def triangle_area(point1: Tuple[float, float],
                  point2: Tuple[float, float],
                  point3: Tuple[float, float]) -> float:
    x1, y1 = point1
    x2, y2 = point2
    x3, y3 = point3
    return math.fabs(0.5 * (x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)))


def calculate_4_polygon_area(point1: Tuple[float, float],
                             point2: Tuple[float, float],
                             point3: Tuple[float, float],
                             point4: Tuple[float, float]) -> float:
    return triangle_area(point1, point2, point3) + \
           triangle_area(point1, point4, point3)


def estimate_card_width_height_from_vertices(vertices: list[Tuple[float, float]],
                                             id_card_width_height_ratio: float) -> Tuple[int, int]:
    area = calculate_4_polygon_area(*vertices)
    height = math.sqrt(area / id_card_width_height_ratio)
    width = id_card_width_height_ratio * height
    return int(math.floor(width)), int(math.floor(height))


def warp_perspective(image: np.ndarray,
                     original_points: list[Tuple[float, float]],
                     target_points: list[Tuple[int, int]],
                     target_width: int,
                     target_height: int) -> np.ndarray:
    original_points = np.float32(original_points)
    target_points = np.float32(target_points)

    perspective_transform_matrix = cv2.getPerspectiveTransform(original_points, target_points)

    return cv2.warpPerspective(image,
                               perspective_transform_matrix,
                               (target_width, target_height),
                               flags=cv2.INTER_CUBIC)


def correct_perspective(image: np.ndarray,
                        original_vertices: list[Tuple[float, float]],
                        id_card_width_height_ratio: float) -> np.ndarray:
    target_width, target_height = estimate_card_width_height_from_vertices(original_vertices,
                                                                           id_card_width_height_ratio)

    target_rectangle_vertices = [(0, 0),
                                 (target_width - 1, 0),
                                 (target_width - 1, target_height - 1),
                                 (0, target_height - 1)]

    return warp_perspective(image,
                            original_vertices,
                            target_rectangle_vertices,
                            target_width,
                            target_height)


def opening_morphology(image,
                       kernel_dimension_size=7,
                       power=3):
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT,
                                       (kernel_dimension_size,
                                        kernel_dimension_size))
    image = cv2.erode(image, kernel, iterations=power)
    image = cv2.dilate(image, kernel, iterations=power)
    return image


def dilate(image, kernel_dimension_size=3):
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT,
                                       (kernel_dimension_size,
                                        kernel_dimension_size))
    return cv2.dilate(image, kernel)


def erode(image, kernel_dimension_size=3):
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT,
                                       (kernel_dimension_size,
                                        kernel_dimension_size))
    return cv2.erode(image, kernel)


def view_image(image, title=''):
    plt.imshow(image, cmap=plt.cm.gray)
    plt.title(title)
    plt.show()


def extract_id_card(image_bgr: np.ndarray, id_card_width_height_ratio: float = 1.575) -> Union[np.ndarray, None]:
    image_max_dimension = max(image_bgr.shape)
    resize_factor, resized_image = resize_image(image_bgr, image_max_dimension)
    image_hsv = cv2.cvtColor(resized_image, cv2.COLOR_BGR2HSV)
    hue_binary, saturation_binary, value_binary = threshold_components(image_hsv)

    hue_info = compute_image_score_for_id_card_extraction(hue_binary, id_card_width_height_ratio)
    saturation_info = compute_image_score_for_id_card_extraction(saturation_binary, id_card_width_height_ratio)
    value_info = compute_image_score_for_id_card_extraction(value_binary, id_card_width_height_ratio)

    # view_image(hue_binary, hue_info[0])
    # view_image(saturation_binary, saturation_info[0])
    # view_image(value_binary, value_info[0])

    components = [hue_info, saturation_info, value_info]
    components.sort(key=lambda x: x[0])

    max_score = components[-1][0]
    if max_score < EPSILON:
        return None

    rectangle_vertices = components[-1][1]

    rectangle_vertices = auto_correct_vertices_order(rectangle_vertices)
    corrected_perspective = correct_perspective(image_bgr,
                                                [(x / resize_factor, y / resize_factor)
                                                 for x, y in rectangle_vertices],
                                                id_card_width_height_ratio)

    id_card_final_height = int(math.floor(ID_CARD_FINAL_WIDTH / id_card_width_height_ratio))
    return cv2.resize(corrected_perspective, (ID_CARD_FINAL_WIDTH, id_card_final_height), cv2.INTER_CUBIC)


def closing_morphology(image,
                       kernel_dimension_size=25,
                       power=3):
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT,
                                       (kernel_dimension_size,
                                        kernel_dimension_size))
    image = cv2.dilate(image, kernel, iterations=power)
    image = cv2.erode(image, kernel, iterations=power)
    return image


def color_clustering_extraction(image_bgr: np.ndarray, id_card_width_height_ratio: float = 1.575) -> Union[np.ndarray,
                                                                                                           None]:
    image_max_dimension = max(image_bgr.shape)
    resize_factor, resized_image = resize_image(image_bgr, image_max_dimension)
    # image_hsv = cv2.cvtColor(resized_image, cv2.COLOR_BGR2HSV)
    image_hsv = resized_image

    color_points = image_hsv.reshape(-1, image_hsv.shape[-1])
    k_means = MiniBatchKMeans(n_clusters=2, batch_size=int(len(color_points) * K_MEANS_BATCH_SIZE_PERCENTAGE))
    k_means.fit(color_points)
    clustered_image = np.array(k_means.labels_, np.uint8).reshape(image_hsv.shape[0:2])
    clustered_image *= 255
    clustered_image = clean_binary_image(correct_threshold(clustered_image))
    view_image(clustered_image)
    largest_contour = find_best_contour(clustered_image, id_card_width_height_ratio)
    if largest_contour is None:
        return None

    center_point = compute_center_of_gravity(largest_contour)
    rectangle_vertices, rectangle_vertices_i = find_convex_4_polygon_vertices(largest_contour)
    rectangle_vertices, rectangle_vertices_i = sort_points_anti_clockwise(center_point,
                                                                          rectangle_vertices,
                                                                          rectangle_vertices_i)
    rectangle_vertices = enhance_rectangle_vertices(largest_contour, rectangle_vertices_i)
    rectangle_vertices = auto_correct_vertices_order(rectangle_vertices)

    corrected_perspective = correct_perspective(image_bgr,
                                                [(x / resize_factor, y / resize_factor)
                                                 for x, y in rectangle_vertices],
                                                id_card_width_height_ratio)

    id_card_final_height = int(math.floor(ID_CARD_FINAL_WIDTH / id_card_width_height_ratio))
    return cv2.resize(corrected_perspective, (ID_CARD_FINAL_WIDTH, id_card_final_height), cv2.INTER_CUBIC)


def intensity_modification(image: np.ndarray, alpha: float, beta: float) -> np.ndarray:
    new_image = np.array(image, np.float)
    new_image = new_image * alpha + beta
    new_image[new_image > 255] = 255
    new_image[new_image < 0] = 0
    new_image = np.array(new_image, np.uint8)
    return new_image


def adaptive_intensity_modification(image: np.ndarray) -> np.ndarray:
    thresh = cv2.threshold(cv2.cvtColor(image, cv2.COLOR_BGR2GRAY),
                           0,
                           255,
                           cv2.THRESH_BINARY + cv2.THRESH_OTSU)[0]
    thresh = int(thresh * 0.9)
    pivot = np.median(image[image < thresh])
    alpha = 255.0 / (255.0 - pivot)
    beta = -alpha * pivot
    return intensity_modification(image, alpha, beta)


def main():
    image_paths1 = [
        '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Adrian Mikko/WhatsApp Image 2020-06-10 at 9.04.00 AM.jpeg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Adrian Mikko/WhatsApp Image 2020-06-10 at 9.04.00 AM (1).jpeg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Alma Pita/Shared Throught whatsapp/WhatsApp Image 2020-06-10 at 9.41.09 AM.jpeg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Anas Abu Shaweesh/20200610_090313.jpg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Anas Abu Shaweesh/20200610_090303.jpg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Anas Abu Shaweesh/20200610_090247.jpg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Botros/20200610_102440.jpg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Botros/20200610_102433.jpg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Fayez Al Wahidi/20200610_091923.jpg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Fayez Al Wahidi/20200610_091910.jpg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Jehad/IMG_20200610_172502.jpg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/SMART ID non Qatari/Alan/IMG_20200610_110951.jpg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/SMART ID non Qatari/Alan/IMG_20200610_111001.jpg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/SMART ID non Qatari/Jack/smart id pics/20200610_151437.jpg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/SMART ID non Qatari/Jack/smart id pics/20200610_151445.jpg',
        '/home/u764/Development/data/ekyc-data/drive-download-20200830T074731Z-001/IMG_20200830_104154.jpg',
        '/home/u764/Development/data/ekyc-data/drive-download-20200830T074731Z-001/IMG_20210815_155235 (1).jpg',
        '/home/u764/Development/data/ekyc-data/test-data/id_front_14.jpg',
        '/home/u764/Development/data/ekyc-data/test-data/id_front_10.jpg',
        '/home/u764/Development/data/ekyc-data/test-data/id_front_9.jpg',
        '/home/u764/Development/data/ekyc-data/test-data/IMG_20200827_132407.jpg',
        '/home/u764/Development/data/ekyc-data/PSO ID/khaldoun B.jpg',
        '/home/u764/Development/data/ekyc-data/PSO ID/Khaldoun F.jpg',
        '/home/u764/Development/data/ekyc-data/drive-download-20210824T150016Z-001/IMG_20210824_175702_1.jpg',
        '/home/u764/Development/data/ekyc-data/drive-download-20210824T150016Z-001/IMG_20210824_175704.jpg',
        '/home/u764/Development/data/ekyc-data/drive-download-20210824T150016Z-001/IMG_20210824_175708.jpg',
        '/home/u764/Development/data/ekyc-data/drive-download-20210824T150016Z-001/IMG_20210824_175720.jpg',
        '/home/u764/Development/data/ekyc-data/drive-download-20210824T150016Z-001/IMG_20210824_175723.jpg',
        '/home/u764/Development/data/ekyc-data/drive-download-20210824T150016Z-001/IMG_20210824_175725.jpg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2021-08-25/QID Sample/Regular QID/WhatsApp Image 2021-08-24 at 9.40.26 AM.jpeg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2021-08-25/QID Sample/Regular QID/WhatsApp Image 2021-08-24 at 9.40.23 AM (1).jpeg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2021-08-25/QID Sample/Regular QID/WhatsApp Image 2021-08-24 at 9.40.23 AM.jpeg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2021-08-25/QID Sample/Regular QID/WhatsApp Image 2021-08-24 at 9.40.28 AM.jpeg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2021-08-25/QID Sample/Regular QID/WhatsApp Image 2021-08-24 at 9.40.22 AM.jpeg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2021-08-25/QID Sample/Smart Resident QID/WhatsApp Image 2021-08-24 at 9.40.21 AM.jpeg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2021-08-25/QID Sample/Smart Resident QID/WhatsApp Image 2021-08-24 at 9.40.20 AM (1).jpeg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2021-08-25/QID Sample/Smart Resident QID/WhatsApp Image 2021-08-24 at 9.40.19 AM (1).jpeg',
        '/home/u764/Development/data/ekyc-data/PSO ID/AhmedAmawi_back.jpg',
        '/home/u764/Development/data/ekyc-data/PSO ID/Fatema B.jpg',
        '/home/u764/Development/data/ekyc-data/PSO ID/Fatema F.jpg',
        '/home/u764/Development/data/ekyc-data/PSO ID/Jamal Al Shukri -F.jpg',
        '/home/u764/Development/data/ekyc-data/PSO ID/Jamal Al Shurki B.jpg',
        '/home/u764/Development/data/ekyc-data/PSO ID/Jawaher -B.jpg',
        '/home/u764/Development/data/ekyc-data/PSO ID/Jehad B.jpg',
        '/home/u764/Development/data/ekyc-data/PSO ID/Sara -F.jpg',
        '/home/u764/Development/data/ekyc-data/drive-download-20200830T074731Z-001/IMG_20200827_132407.jpg',
        '/home/u764/Development/data/ekyc-data/drive-download-20200830T074731Z-001/IMG_20200830_104148.jpg',
        '/home/u764/Development/data/ekyc-data/drive-download-20200830T074731Z-001/IMG_20200830_104154.jpg',
        '/home/u764/Development/data/ekyc-data/drive-download-20200830T074731Z-001/IMG_20200830_104236.jpg',
        '/home/u764/Development/data/ekyc-data/drive-download-20200830T074731Z-001/IMG_20210725_124622.jpg',
        '/home/u764/Development/data/ekyc-data/drive-download-20200830T074731Z-001/IMG_20210815_155235 (1).jpg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Adrian Mikko/WhatsApp Image 2020-06-10 at 9.03.55 AM.jpeg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Adrian Mikko/WhatsApp Image 2020-06-10 at 9.03.56 AM.jpeg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Jehad/IMG_20200610_172839.jpg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Jehad/IMG_20200610_172423.jpg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Jehad/IMG_20200610_172309.jpg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Jehad/IMG_20200610_172252.jpg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Jehad/IMG_20200610_172229.jpg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/QATARTI ID/WhatsApp Image 2020-06-10 at 12.09.49 PM (1).jpeg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/QATARTI ID/WhatsApp Image 2020-06-10 at 12.09.49 PM (2).jpeg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/QATARTI ID/WhatsApp Image 2020-06-10 at 12.09.49 PM (3).jpeg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/QATARTI ID/WhatsApp Image 2020-06-10 at 12.09.49 PM (4).jpeg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/QATARTI ID/WhatsApp Image 2020-06-10 at 12.09.49 PM (6).jpeg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/QATARTI ID/WhatsApp Image 2020-06-10 at 12.09.49 PM (7).jpeg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/SMART ID non Qatari/Alan/IMG_20200610_110642.jpg',
        '/home/u764/Development/data/ekyc-data/test-data/id_front_10.jpg',
        '/home/u764/Development/data/ekyc-data/test-data/id_front_13.jpg',
        '/home/u764/Development/data/ekyc-data/test-data/id_front_14.jpg',
        '/home/u764/Development/data/ekyc-data/test-data/IMG_20200827_132407.jpg',
        '/home/u764/Development/data/ekyc-data/test-data/300dpi full page/NON_SMART_1_CROPPED.jpg',
        '/home/u764/Development/data/ekyc-data/test-data/300dpi full page/NON_SMART_2_CROPPED.jpg',
        '/home/u764/Development/data/ekyc-data/test-data/300dpi full page/NON_SMART_3_CROPPED.jpg',
        '/home/u764/Development/data/ekyc-data/test-data/300dpi full page/SMART_1_CROPPED.jpg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/QATARTI ID/WhatsApp Image 2020-06-10 at 12.09.49 PM (5).jpeg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2021-08-25/QID Sample/Regular QID/WhatsApp Image 2021-08-24 at 9.40.26 AM (1).jpeg'
    ]

    image_paths2 = [
        '/home/u764/Development/data/ekyc-data/OneDrive_2021-08-25/QID Sample/Regular QID/WhatsApp Image 2021-08-24 at 9.40.28 AM.jpeg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2021-08-25/QID Sample/Regular QID/WhatsApp Image 2021-08-24 at 9.40.24 AM (1).jpeg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2021-08-25/QID Sample/Regular QID/WhatsApp Image 2021-08-24 at 9.40.22 AM (1).jpeg',
        '/home/u764/Development/data/ekyc-data/OneDrive_2021-08-25/QID Sample/Regular QID/WhatsApp Image 2021-08-24 at 9.40.22 AM.jpeg'
    ]

    image_paths2 = list(reversed(image_paths2))
    image_paths1 = list(reversed(image_paths1))

    image = cv2.imread(image_paths1[0])
    extract_id_card(image)

    sum_of_times = 0.0
    for image_path in image_paths1:
        image = cv2.imread(image_path)
        start_time = time.time()
        extracted = extract_id_card(image)
        end_time = time.time()
        sum_of_times += (end_time - start_time)
        if extracted is not None:
            # fig, axs = plt.subplots(1, 2)
            # axs[0].imshow(extracted)
            # axs[1].imshow(adaptive_intensity_modification(extracted))
            plt.imshow(cv2.cvtColor(extracted, cv2.COLOR_BGR2RGB))
            plt.title(os.path.basename(image_path))
            plt.show()
        else:
            print(image_path)
    print(sum_of_times / len(image_paths1))

    # sum_of_times = 0.0
    # for image_path in image_paths2:
    #     image = cv2.imread(image_path)
    #     start_time = time.time()
    #     color_clustering_extraction(image)
    #     end_time = time.time()
    #     sum_of_times += (end_time - start_time)
    #     plt.imshow(color_clustering_extraction(image))
    #     plt.show()
    # print(sum_of_times / len(image_paths2))


if __name__ == '__main__':
    main()
