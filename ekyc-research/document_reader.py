#!/usr/bin/python3.6
import cv2
import matplotlib.pyplot as plt
import numpy as np
from pytesseract import pytesseract
from shapely.geometry import Polygon

from configuration.config import get_document_pre_processor, get_document_fields
from id_card.id_card_pre_processing import compute_center, find_convex_polygon_vertices_from_contour
from imaging_utils import otsu_threshold


def read_field(field_image,
               tesseract_config):
    return pytesseract.image_to_string(field_image,
                                       config=tesseract_config)


def read_document(image,
                  country,
                  document_type,
                  apply_dewarping=True):
    pre_processor_result = get_document_pre_processor(country, document_type)(image,
                                                                              apply_dewarping)
    if pre_processor_result is None:
        return None

    pre_processed_document_image, extra_info = pre_processor_result
    # view_image(cv2.cvtColor(pre_processed_document_image, cv2.COLOR_BGR2RGB))
    return read_document_without_pre_processing(pre_processed_document_image, country, document_type, extra_info)


def read_document_without_pre_processing(pre_processed_document_image, country, document_type, extra_info):
    recognized_fields = dict()
    fields = get_document_fields(country, document_type)
    for field in fields:
        try:
            extracted_field = field['extractor'](pre_processed_document_image, extra_info)
            # view_image(extracted_field)
            pre_processed_field = field['pre_processor'](extracted_field)
            # view_image(pre_processed_field)
            recognized_fields[field['id']] = field['post_processor'](read_field(pre_processed_field,
                                                                                field['tesseract_config']))
        except Exception as ex:
            recognized_fields[field['id']] = None
            print(ex)
    return recognized_fields, pre_processed_document_image


def main():
    image = cv2.imread('/home/u764/Downloads/test.bmp', cv2.IMREAD_GRAYSCALE)
    image = 255 - otsu_threshold(image)

    coords = np.transpose(np.where(np.transpose(image == 255)))

    center = compute_center(coords)
    vertices = find_convex_polygon_vertices_from_contour(coords, center, 4)
    print(vertices)

    plt.imshow(image)
    plt.show()


if __name__ == '__main__':
    main()
