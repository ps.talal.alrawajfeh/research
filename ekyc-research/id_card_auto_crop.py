import cv2
import numpy as np
from numba import jit

ID_CARD_SIZE = (750, 476)
ACCEPTANCE_THRESHOLD = 0.7
EPSILON = 1e-6


@jit(nopython=True)
def relu(x):
    if x < 0:
        return 0
    return x


@jit(nopython=True)
def fast_ncc_search(image, template, location, search_radius, epsilon=1e-4):
    lx, ly = location
    dx, dy = search_radius

    template_height = template.shape[0]
    template_width = template.shape[1]

    lx1 = relu(lx - dx)
    ly1 = relu(ly - dy)
    lx2 = min(lx + template_width + dx, image.shape[1])
    ly2 = min(ly + template_height + dy, image.shape[0])

    if lx2 <= lx1 or ly2 <= ly1:
        return 0.0, None

    window = image[ly1:ly2, lx1:lx2]

    if window.shape[0] < template_height or window.shape[1] < template_width:
        return 0.0, None

    zero_centered1 = template - np.mean(template)
    std1 = np.std(template)

    part = window[0:template_height, 0:template_width]
    mu = np.mean(part)
    var = np.mean(np.square(part)) - mu ** 2
    n = template_width * template_height

    width_difference = window.shape[1] - template_width
    mu_var_table = np.zeros((width_difference + 1, 2))
    height_difference = window.shape[0] - template_height
    max_cc = -1
    best_coordinates = None

    for x in range(width_difference):
        if x == 0:
            mu_var_table[x][0] = mu
            mu_var_table[x][1] = var
        else:
            old_values = window[:template_height, x - 1: x]
            new_values = window[:template_height, template_width + x - 1: template_width + x]

            old_mu = mu_var_table[x - 1][0]
            old_var = mu_var_table[x - 1][1]

            new_mu = old_mu + np.sum(new_values - old_values) / n
            mu_var_table[x][0] = new_mu
            mu_var_table[x][1] = old_var + old_mu ** 2 - new_mu ** 2 + \
                                 np.sum(np.square(new_values) - np.square(old_values)) / n

        part = window[:template_height, x:x + template_width]
        zero_centered2 = part - mu_var_table[x][0]
        std2 = np.sqrt(mu_var_table[x][1])

        if std1 < epsilon or std2 < epsilon:
            cc = 0.0
        else:
            cc = np.sum(zero_centered1 * zero_centered2) / (std1 * std2 * n)

        if cc > max_cc:
            max_cc = cc
            best_coordinates = [lx1 + x, ly1 + 0]

    for y in range(1, height_difference):
        for x in range(0, width_difference):
            old_values = window[y - 1:y, x: x + template_width]
            new_values = window[template_height + y - 1: template_height + y, x: x + template_width]

            old_mu = mu_var_table[x][0]
            old_var = mu_var_table[x][1]

            new_mu = old_mu + np.sum(new_values - old_values) / n
            mu_var_table[x][0] = new_mu
            mu_var_table[x][1] = old_var + old_mu ** 2 - new_mu ** 2 + \
                                 np.sum(np.square(new_values) - np.square(old_values)) / n

            part = window[y: y + template_height, x:x + template_width]
            zero_centered2 = part - mu_var_table[x][0]
            std2 = np.sqrt(mu_var_table[x][1])

            if std1 < epsilon or std2 < epsilon:
                cc = 0.0
            else:
                cc = np.sum(zero_centered1 * zero_centered2) / (std1 * std2 * n)

            if cc > max_cc:
                max_cc = cc
                best_coordinates = [lx1 + x, ly1 + y]

    return max_cc, best_coordinates


def points_similarity(points1, points2, epsilon=1e-7):
    tensor1 = np.expand_dims(points1, 0) - np.expand_dims(points1, 1)
    tensor2 = np.expand_dims(points2, 0) - np.expand_dims(points2, 1)

    magnitude1 = np.sqrt(np.sum(np.square(tensor1), -1) + epsilon)
    magnitude2 = np.sqrt(np.sum(np.square(tensor2), -1) + epsilon)

    dot_products = np.sum(tensor1 * tensor2, -1)
    cosine_similarities = dot_products / (magnitude1 * magnitude2)

    return np.prod(cosine_similarities + np.eye(cosine_similarities.shape[0]))


def threshold(image):
    return cv2.threshold(
        image,
        0,
        255,
        cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]


def match_template(image_part,
                   resized_template,
                   matching_method=cv2.TM_CCOEFF_NORMED):
    matching_result = cv2.matchTemplate(image_part, resized_template, matching_method)
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(matching_result)
    if matching_method in [cv2.TM_SQDIFF, cv2.TM_SQDIFF_NORMED]:
        location = min_loc
        score = min_val
    else:
        location = max_loc
        score = max_val
    return location, score


def harmonic_mean(values):
    for v in values:
        if v < EPSILON:
            return 0.0
    total = 0
    for v in values:
        total += 1.0 / v
    return len(values) / total


def image_score(image, rectangle, arguments):
    rx1, ry1, rx2, ry2 = rectangle

    cropped = image[ry1:ry2, rx1:rx2]
    resized = cv2.cvtColor(cv2.resize(cropped, ID_CARD_SIZE, interpolation=cv2.INTER_CUBIC),
                           cv2.COLOR_BGR2GRAY)

    locations_vector = []
    approximate_locations_vector = []
    scores = []
    for label in arguments:
        lx, ly = label['approximate_location']
        dx, dy = label['neighborhood_search_deltas']
        approximate_locations_vector.append([lx, ly])
        template = label['label_image']

        x1 = max(0, lx - dx)
        x2 = min(resized.shape[1], lx + template.shape[1] + dx)
        y1 = max(0, ly - dy)
        y2 = min(resized.shape[0], ly + template.shape[0] + dy)

        image_part = resized[y1:y2, x1:x2]
        max_location, max_score = match_template(image_part, template)

        for i in range(0, image_part.shape[1] - template.shape[1]):
            width = template.shape[1] + i
            height = int(round(template.shape[0] / template.shape[1] * width))

            resized_template = cv2.resize(template, (width, height), interpolation=cv2.INTER_CUBIC)
            location, score = match_template(image_part, resized_template)

            if score > max_score:
                max_score = score
                max_location = location

        for i in range(1, template.shape[1] // 2):
            width = template.shape[1] - i
            height = int(round(template.shape[0] / template.shape[1] * width))

            resized_template = cv2.resize(template, (width, height), interpolation=cv2.INTER_CUBIC)
            location, score = match_template(image_part, resized_template)

            if score > max_score:
                max_score = score
                max_location = location

        locations_vector.append([max_location[0] + x1, max_location[1] + y1])
        scores.append(max_score)

    scores.append(points_similarity(locations_vector, approximate_locations_vector))
    return harmonic_mean(scores)


# rectangle = [x1, y1, x2, y2]
def auto_shoot(images, rectangle, arguments):
    rx1, ry1, rx2, ry2 = rectangle

    labels = dict()
    for label in arguments['labels']:
        labels[label] = cv2.imread(arguments['labels'][label]['label_path'], cv2.IMREAD_GRAYSCALE)
        labels[label] = cv2.threshold(labels[label], 127, 255, cv2.THRESH_BINARY)[1]

    resized_images = []
    image_labels = []
    for image in images:
        cropped = image[ry1:ry2, rx1:rx2]
        resized = cv2.cvtColor(cv2.resize(cropped, ID_CARD_SIZE, interpolation=cv2.INTER_CUBIC),
                               cv2.COLOR_BGR2GRAY)

        resized_images.append(resized)

        total_score = 1.0
        locations = dict()
        locations_vector = []
        approximate_locations_vector = []
        for label in arguments['labels']:
            label_arguments = arguments['labels'][label]
            approximate_location = label_arguments['approximate_location']
            approximate_locations_vector.append(approximate_location)

            score, location = fast_ncc_search(np.array(resized, np.float32),
                                              np.array(labels[label], np.float32),
                                              approximate_location,
                                              label_arguments['neighborhood_search_deltas'])
            locations_vector.append(location)

            locations[label] = location
            total_score = min(score, total_score)

        total_score *= points_similarity(locations_vector, approximate_locations_vector)
        image_labels.append([total_score, locations])

    max_i = None
    max_score = -1

    for i in range(len(images)):
        score = image_labels[i][0]
        if score >= ACCEPTANCE_THRESHOLD and score > max_score:
            max_score = score
            max_i = i

    if max_i is not None:
        image = resized_images[max_i]
        locations = image_labels[max_i][1]
        return image, max_score, locations

    return None


def auto_crop(image, label_locations, arguments):
    standard_center = [0.0, 0.0]
    current_center = [0.0, 0.0]
    n = 0

    for label in arguments['labels']:
        x, y = arguments['labels'][label]['approximate_location']
        standard_center[0] += x
        standard_center[1] += y

        x, y = label_locations[label]
        current_center[0] += x
        current_center[1] += y

        n += 1

    standard_center[0] /= n
    standard_center[1] /= n

    current_center[0] /= n
    current_center[1] /= n

    standard_relative_x1 = -standard_center[0]
    standard_relative_y1 = -standard_center[1]
    standard_relative_x2 = ID_CARD_SIZE[0] - standard_center[0]
    standard_relative_y2 = ID_CARD_SIZE[1] - standard_center[1]

    x1 = int(relu(current_center[0] + standard_relative_x1))
    y1 = int(relu(current_center[1] + standard_relative_y1))
    x2 = int(min(current_center[0] + standard_relative_x2, ID_CARD_SIZE[0]))
    y2 = int(min(current_center[1] + standard_relative_y2, ID_CARD_SIZE[1]))

    cropped = image[y1:y2, x1:x2]

    if x2 - x1 < ID_CARD_SIZE[0] or y2 - y1 < ID_CARD_SIZE[1]:
        cropped = cv2.resize(cropped, ID_CARD_SIZE, interpolation=cv2.INTER_CUBIC)

    return cropped
