import math

import cv2
import matplotlib.pyplot as plt
import numpy as np
import pytesseract

from clean_version import view_image
from configuration.config import get_document_pre_processor, get_document_labels
from document_reader import read_document, read_document_without_pre_processing
from id_card_auto_crop import image_score
from imaging_utils import image_brightness, image_contrast, dilate, closing_morphology, otsu_threshold


def calculate_threshold(min1, min2, mean1, mean2, std1, std2):
    weight1 = (1 + std2 / mean2) / (2 + std1 / mean1 + std2 / mean2)
    weight2 = (1 + std1 / mean1) / (2 + std1 / mean1 + std2 / mean2)
    return min1 * weight1 + min2 * weight2


def calculate_std(sum_of_squares, mean, n):
    return math.sqrt(sum_of_squares / n - mean ** 2)


def decrease_brightness(image, amount):
    result = np.array(image, np.int)
    result -= amount
    result[result < 0] = 0
    return np.array(result, np.uint8)


def update_stats(stats, value):
    stats["min"] = min(value, stats["min"])
    stats["max"] = max(value, stats["max"])
    stats["total"] += value
    stats["total_square"] += value ** 2


def initialize_stats():
    return {
        'min': 1000.0,
        'max': -1.0,
        'total': 0.0,
        'total_square': 0.0
    }


def print_statistics_and_threshold(stats1, stats2, stats1_n, stats2_n, statistic_name):
    mu1, sigma1 = calculate_stats_mean_std(stats1, stats1_n)
    print(f'n: {stats1_n}')
    print(f'min {statistic_name} for original image: {stats1["min"]}')
    print(f'max {statistic_name} for original image: {stats1["max"]}')
    print(f'mean {statistic_name} for original image: {mu1}')
    print(f'std {statistic_name} for original image: {sigma1}')
    print('\n')
    mu2, sigma2 = calculate_stats_mean_std(stats2, stats2_n)
    print(f'n: {stats2_n}')
    print(f'min {statistic_name} for last correctly read image: {stats2["min"]}')
    print(f'max {statistic_name} for last correctly read image: {stats2["max"]}')
    print(f'mean {statistic_name} for last correctly read image: {mu2}')
    print(f'std {statistic_name} for last correctly read image: {sigma2}')
    print('\n')
    threshold = calculate_threshold(stats1["min"],
                                    stats2["min"],
                                    mu1,
                                    mu2,
                                    sigma1,
                                    sigma2)
    print(f'{statistic_name}: {threshold}')


def calculate_stats_mean_std(stats, n):
    mu = stats["total"] / n
    sigma = calculate_std(stats["total_square"], mu, n)
    return mu, sigma


def decrease_contrast(image, amount):
    result = np.array(image, np.float)
    result = result * (1.0 - amount / 127.0)
    return np.array(result, np.uint8)


def main():
    images_with_info = [
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Adrian Mikko/WhatsApp Image 2020-06-10 at 9.04.00 AM (1).jpeg',
            'qat',
            'id_card.non_smart.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Alma Pita/Shared Throught whatsapp/WhatsApp Image 2020-06-10 at 9.41.09 AM.jpeg',
            'qat',
            'id_card.non_smart.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Anas Abu Shaweesh/20200610_090313.jpg',
            'qat',
            'id_card.non_smart.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Anas Abu Shaweesh/20200610_090303.jpg',
            'qat',
            'id_card.non_smart.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Anas Abu Shaweesh/20200610_090247.jpg',
            'qat',
            'id_card.non_smart.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Botros/20200610_102433.jpg',
            'qat',
            'id_card.non_smart.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Fayez Al Wahidi/20200610_091923.jpg',
            'qat',
            'id_card.non_smart.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Jehad/IMG_20200610_172502.jpg',
            'qat',
            'id_card.non_smart.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/SMART ID non Qatari/Alan/IMG_20200610_110951.jpg',
            'qat',
            'id_card.smart.resident.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/SMART ID non Qatari/Jack/smart id pics/20200610_151437.jpg',
            'qat',
            'id_card.smart.resident.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/drive-download-20200830T074731Z-001/IMG_20200830_104154.jpg',
            'jor',
            'id_card.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/test-data/id_front_14.jpg',
            'jor',
            'id_card.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/test-data/id_front_10.jpg',
            'jor',
            'id_card.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/test-data/id_front_9.jpg',
            'jor',
            'id_card.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/PSO ID/khaldoun B.jpg',
            'oma',
            'id_card.resident.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/drive-download-20210824T150016Z-001/IMG_20210824_175702_1.jpg',
            'jor',
            'id_card.front',
        ),
        (
            '/home/u764/Development/data/ekyc-data/drive-download-20210824T150016Z-001/IMG_20210824_175704.jpg',
            'jor',
            'id_card.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/drive-download-20210824T150016Z-001/IMG_20210824_175708.jpg',
            'jor',
            'id_card.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/drive-download-20210824T150016Z-001/IMG_20210824_175720.jpg',
            'jor',
            'id_card.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/drive-download-20210824T150016Z-001/IMG_20210824_175723.jpg',
            'jor',
            'id_card.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/drive-download-20210824T150016Z-001/IMG_20210824_175725.jpg',
            'jor',
            'id_card.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2021-08-25/QID Sample/Regular QID/WhatsApp Image 2021-08-24 at 9.40.26 AM.jpeg',
            'qat',
            'id_card.non_smart.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2021-08-25/QID Sample/Regular QID/WhatsApp Image 2021-08-24 at 9.40.23 AM.jpeg',
            'qat',
            'id_card.non_smart.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2021-08-25/QID Sample/Regular QID/WhatsApp Image 2021-08-24 at 9.40.28 AM.jpeg',
            'qat',
            'id_card.non_smart.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2021-08-25/QID Sample/Regular QID/WhatsApp Image 2021-08-24 at 9.40.22 AM.jpeg',
            'qat',
            'id_card.non_smart.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2021-08-25/QID Sample/Smart Resident QID/WhatsApp Image 2021-08-24 at 9.40.20 AM (1).jpeg',
            'qat',
            'id_card.smart.resident.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2021-08-25/QID Sample/Smart Resident QID/WhatsApp Image 2021-08-24 at 9.40.19 AM (1).jpeg',
            'qat',
            'id_card.smart.resident.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/PSO ID/Fatema F.jpg',
            'oma',
            'id_card.citizen.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/PSO ID/Jamal Al Shukri -F.jpg',
            'oma',
            'id_card.citizen.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/PSO ID/Sara -F.jpg',
            'oma',
            'id_card.citizen.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/drive-download-20200830T074731Z-001/IMG_20200830_104148.jpg',
            'jor',
            'id_card.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/drive-download-20200830T074731Z-001/IMG_20200830_104154.jpg',
            'jor',
            'id_card.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/drive-download-20200830T074731Z-001/IMG_20200830_104236.jpg',
            'jor',
            'id_card.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/drive-download-20200830T074731Z-001/IMG_20210725_124622.jpg',
            'jor',
            'id_card.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Adrian Mikko/WhatsApp Image 2020-06-10 at 9.03.55 AM.jpeg',
            'qat',
            'id_card.non_smart.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Adrian Mikko/WhatsApp Image 2020-06-10 at 9.03.56 AM.jpeg',
            'qat',
            'id_card.non_smart.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Jehad/IMG_20200610_172839.jpg',
            'qat',
            'id_card.non_smart.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Jehad/IMG_20200610_172423.jpg',
            'qat',
            'id_card.non_smart.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Jehad/IMG_20200610_172309.jpg',
            'qat',
            'id_card.non_smart.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Jehad/IMG_20200610_172252.jpg',
            'qat',
            'id_card.non_smart.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/Non SMART ID non Qatari/Jehad/IMG_20200610_172229.jpg',
            'qat',
            'id_card.non_smart.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/QATARTI ID/WhatsApp Image 2020-06-10 at 12.09.49 PM (1).jpeg',
            'qat',
            'id_card.smart.citizen.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/QATARTI ID/WhatsApp Image 2020-06-10 at 12.09.49 PM (2).jpeg',
            'qat',
            'id_card.smart.citizen.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/OneDrive_2020-08-27/QID Samples/QATARTI ID/WhatsApp Image 2020-06-10 at 12.09.49 PM (3).jpeg',
            'qat',
            'id_card.smart.citizen.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/test-data/id_front_10.jpg',
            'jor',
            'id_card.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/test-data/id_front_13.jpg',
            'jor',
            'id_card.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/test-data/id_front_14.jpg',
            'jor',
            'id_card.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/test-data/300dpi full page/NON_SMART_1_CROPPED.jpg',
            'qat',
            'id_card.non_smart.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/test-data/300dpi full page/NON_SMART_2_CROPPED.jpg',
            'qat',
            'id_card.non_smart.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/test-data/300dpi full page/NON_SMART_3_CROPPED.jpg',
            'qat',
            'id_card.non_smart.front'
        ),
        (
            '/home/u764/Development/data/ekyc-data/test-data/300dpi full page/SMART_1_CROPPED.jpg',
            'qat',
            'id_card.smart.resident.front'
        )
    ]

    for image_path, country, document_type in images_with_info:
        print('\n\n')
        print('#' * (len(image_path) + 6))
        print(f'## {image_path} ##')
        print('#' * (len(image_path) + 6))
        image = cv2.imread(image_path)
        if country != 'qat':
            continue
        if image.shape[0] * image.shape[1] < 8000000:
            print(image_path)
            continue
        pre_processor_result = get_document_pre_processor(country, document_type)(image,
                                                                                  True)
        if pre_processor_result is None:
            continue
        # print(pytesseract.image_to_string(pre_processor_result[0], config='-l ocrb --oem 1 --psm 1'))
        # boxes = pytesseract.image_to_boxes(pre_processor_result[0], config='-l ocrb --oem 1 --psm 1')
        # h = pre_processor_result[0].shape[0]
        # for box in boxes.splitlines():
        #     splitted = box.split(' ')
        #     print(splitted)
        #     cv2.rectangle(pre_processor_result[0], (int(splitted[1]), h - int(splitted[2])),
        #                   (int(splitted[3]), h - int(splitted[4])), (0, 255, 0), 2)
        # view_image(pre_processor_result[0])
        text_threshold(pre_processor_result[0])
        fig, axs = plt.subplots(1, 2)
        axs[0].imshow(text_threshold(pre_processor_result[0]))
        axs[1].imshow(pre_processor_result[0])
        plt.show()

    exit()

    template_score_stats1 = initialize_stats()
    template_score_stats2 = initialize_stats()

    variance_of_laplacian_stats1 = initialize_stats()
    variance_of_laplacian_stats2 = initialize_stats()

    image_brightness_stats1 = initialize_stats()
    image_brightness_stats2 = initialize_stats()

    image_contrast_stats1 = initialize_stats()
    image_contrast_stats2 = initialize_stats()

    stats1_n = 0
    stats2_n = 0

    for image_path, country, document_type in images_with_info:
        print('\n\n')
        print('#' * (len(image_path) + 6))
        print(f'## {image_path} ##')
        print('#' * (len(image_path) + 6))
        image = cv2.imread(image_path)
        result = read_document(image, country, document_type)
        if result is None:
            continue
        if country != 'qat':
            continue
        base_reading = result[0]
        print(base_reading)

        pre_processor_result = get_document_pre_processor(country, document_type)(image,
                                                                                  True)
        view_image(text_threshold(pre_processor_result[0]))

        original_pre_processed_document_image, extra_info = pre_processor_result
        # view_image(pre_processed_document_image)
        labels = get_document_labels(country, document_type)

        template_score = image_score(original_pre_processed_document_image,
                                     (0,
                                      0,
                                      original_pre_processed_document_image.shape[1],
                                      original_pre_processed_document_image.shape[0]),
                                     labels)
        variance_of_laplacian = cv2.Laplacian(original_pre_processed_document_image, cv2.CV_64F).var()
        gray_image = cv2.cvtColor(original_pre_processed_document_image, cv2.COLOR_BGR2GRAY)
        brightness = image_brightness(gray_image)
        contrast = image_contrast(gray_image)

        update_stats(template_score_stats1, template_score)
        update_stats(variance_of_laplacian_stats1, variance_of_laplacian)
        update_stats(image_brightness_stats1, brightness)
        update_stats(image_contrast_stats1, contrast)
        stats1_n += 1

        print(f'template score: {template_score}')
        print(f'variance of laplacian: {variance_of_laplacian}')
        print(f'brightness: {brightness}')
        print(f'contrast: {contrast}')
        print('--------------------------------------------------------------------------------')

        last_template_score = None
        last_variance_of_laplacian = None
        last_brightness = None
        last_contrast = None

        for i in range(0, 127, 20):
            # pre_processed_document_image = cv2.blur(pre_processed_document_image, (3, 3))
            # pre_processed_document_image = decrease_brightness(pre_processed_document_image, i)
            pre_processed_document_image = decrease_contrast(original_pre_processed_document_image, i)
            template_score = image_score(pre_processed_document_image,
                                         (0, 0, pre_processed_document_image.shape[1],
                                          pre_processed_document_image.shape[0]),
                                         labels)
            variance_of_laplacian = cv2.Laplacian(pre_processed_document_image, cv2.CV_64F).var()
            gray_image = cv2.cvtColor(pre_processed_document_image, cv2.COLOR_BGR2GRAY)
            brightness = image_brightness(gray_image)
            contrast = image_contrast(gray_image)

            print(f'template score: {template_score}')
            print(f'variance of laplacian: {variance_of_laplacian}')
            print(f'brightness: {brightness}')
            print(f'contrast: {contrast}')
            blurred_reading = read_document_without_pre_processing(pre_processed_document_image,
                                                                   country,
                                                                   document_type,
                                                                   extra_info)[0]
            differences_found = False
            for key in base_reading:
                if blurred_reading[key] != base_reading[key]:
                    print(f'difference on {i}: {base_reading[key]} -> {blurred_reading[key]}')
                    differences_found = True
            print('--------------------------------------------------------------------------------')
            # view_image(pre_processed_document_image)
            if differences_found:
                if last_variance_of_laplacian is None or last_template_score is None:
                    break

                update_stats(template_score_stats2, last_template_score)
                update_stats(variance_of_laplacian_stats2, last_variance_of_laplacian)
                update_stats(image_brightness_stats2, last_brightness)
                update_stats(image_contrast_stats2, last_contrast)
                stats2_n += 1
                break
            else:
                last_template_score = template_score
                last_variance_of_laplacian = variance_of_laplacian
                last_brightness = brightness
                last_contrast = contrast

    print('\n\n')
    print('statistics:')
    print('===========\n')
    print_statistics_and_threshold(template_score_stats1,
                                   template_score_stats2,
                                   stats1_n,
                                   stats2_n,
                                   'template score')

    print('\n\n')
    print_statistics_and_threshold(variance_of_laplacian_stats1,
                                   variance_of_laplacian_stats2,
                                   stats1_n,
                                   stats2_n,
                                   'variance of laplacian')

    print('\n\n')
    print_statistics_and_threshold(image_brightness_stats1,
                                   image_brightness_stats2,
                                   stats1_n,
                                   stats2_n,
                                   'brightness')

    print('\n\n')
    print_statistics_and_threshold(image_contrast_stats1,
                                   image_contrast_stats2,
                                   stats1_n,
                                   stats2_n,
                                   'contrast')


if __name__ == '__main__':
    main()
