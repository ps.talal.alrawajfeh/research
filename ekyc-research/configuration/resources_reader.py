#!/usr/bin/python3.6
import json
import os

import cv2

COUNTRIES_RESOURCES_PATH = './resources/countries'


class FileResource(object):
    class __FileResource:
        def __init__(self, path, lines_processor):
            with open('resources/' + path, 'r') as f:
                self.processed_file = lines_processor(f.readlines())

        def read_file(self):
            return self.processed_file

    resources = dict()

    def __new__(cls, path, lines_processor):
        if path not in FileResource.resources:
            FileResource.resources[path] = FileResource.__FileResource(path, lines_processor)
        return FileResource.resources[path]


class ImageResource(object):
    class __ImageResource:
        def __init__(self, path):
            self.image = cv2.imread('resources/' + path, cv2.IMREAD_GRAYSCALE)

        def read_image(self):
            return self.image

    resources = dict()

    def __new__(cls, path):
        if path not in ImageResource.resources:
            ImageResource.resources[path] = ImageResource.__ImageResource(path)
        return ImageResource.resources[path]


def load_countries_config():
    countries = [d for d in os.listdir(COUNTRIES_RESOURCES_PATH) if
                 os.path.isdir(COUNTRIES_RESOURCES_PATH + '/' + d)]

    countries_config = dict()

    with open(COUNTRIES_RESOURCES_PATH + '/global_config.json', 'r') as f:
        global_config = f.read()

    countries_config['global'] = json.loads(global_config)

    for country in countries:
        with open(COUNTRIES_RESOURCES_PATH + '/' + country + '/config.json', 'r') as f:
            config = f.read()
        countries_config[country] = json.loads(config)

    return countries_config
