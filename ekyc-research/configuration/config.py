#!/usr/bin/python3.6
import cv2

from id_card.id_card_pre_processing import curry_pre_process_id_card, auto_crop_text
from id_card.id_card_processing import curry_id_card_field_extractor, curry_qatari_name_field_extractor
from passport.passport_processing import passport_mrz_pre_processor, curry_mrz_field_extractor
from configuration.resources_reader import FileResource, load_countries_config

DOCUMENT_PRE_PROCESSORS = {
    'identity_with_no_extra_info': lambda image, _: (image, None),
    'id_card_pre_processor': curry_pre_process_id_card,
}

FIELD_PRE_PROCESSORS = {
    'identity': lambda img: img,
    'default_passport_mrz_pre_processor': passport_mrz_pre_processor,
    'auto_crop_text': auto_crop_text
}

FIELD_EXTRACTORS = {
    'default_passport_mrz_extractor': curry_mrz_field_extractor,
    'default_id_card_field_extractor': curry_id_card_field_extractor,
    'qatari_name_field_extractor': curry_qatari_name_field_extractor
}

FIELD_POST_PROCESSORS = {
    'remove_white_spaces': lambda lbl: lbl.replace(' ', '').replace('\n', '').replace('\t', '').strip(),
    'arabic_post_processor': lambda lbl: lbl.strip(),
    'name_post_processor': lambda lbl: lbl.strip()
}


def convert_to_tesseract_config(lines):
    config = ''
    for line in lines:
        stripped_line = line.strip()
        if stripped_line == '':
            continue
        config += ' -c ' + stripped_line
    return config


def prepare_fields(fields):
    prepared_fields = []

    for field in fields:
        field_params = dict()
        field_params['id'] = field['id']
        field_params['pre_processor'] = FIELD_PRE_PROCESSORS[field['pre_processor']]

        extractor_args = field['extractor_args']
        extractor = field['extractor']
        if len(extractor_args) == 0:
            field_params['extractor'] = FIELD_EXTRACTORS[extractor]
        else:
            field_params['extractor'] = FIELD_EXTRACTORS[extractor](**extractor_args)

        vars_config = FileResource(field['tesseract_vars_file'],
                                   convert_to_tesseract_config).read_file()
        field_params['tesseract_config'] = field['tesseract_config'] + vars_config

        field_params['post_processor'] = FIELD_POST_PROCESSORS[field['post_processor']]
        prepared_fields.append(field_params)

    return prepared_fields


def prepare_labels(labels):
    if labels is None:
        return None
    for label in labels:
        label['label_image'] = cv2.imread('./resources/' + label['label_path'], cv2.IMREAD_GRAYSCALE)
        label['label_image'] = cv2.threshold(label['label_image'], 127, 255, cv2.THRESH_BINARY)[1]
    return labels


# -----------------------------------------------------------------------------

COUNTRIES_CONFIG = load_countries_config()


def __get_document_type_key_config_pairs(type_segment, segment_dict):
    if 'pre_processor' in segment_dict:
        return [(type_segment, segment_dict)]
    document_types = []
    for key in segment_dict:
        if type_segment == '':
            new_type_segment = key
        else:
            new_type_segment = type_segment + '.' + key
        document_types.extend(__get_document_type_key_config_pairs(new_type_segment,
                                                                   segment_dict[key]))
    return document_types


def get_document_type_key_config_pairs():
    return __get_document_type_key_config_pairs('', COUNTRIES_CONFIG)


DOCUMENT_TYPE_CONFIG_PAIRS = get_document_type_key_config_pairs()

DOCUMENT_PRE_PROCESSOR = dict()

for __document_type_key, __document_config in DOCUMENT_TYPE_CONFIG_PAIRS:
    pre_processor_args = __document_config['pre_processor_args']
    pre_processor = __document_config['pre_processor']
    if len(pre_processor_args) == 0:
        DOCUMENT_PRE_PROCESSOR[__document_type_key] = DOCUMENT_PRE_PROCESSORS[pre_processor]
    else:
        DOCUMENT_PRE_PROCESSOR[__document_type_key] = DOCUMENT_PRE_PROCESSORS[pre_processor](**pre_processor_args)

DOCUMENT_FIELDS = dict()

for __document_type_key, __document_config in DOCUMENT_TYPE_CONFIG_PAIRS:
    DOCUMENT_FIELDS[__document_type_key] = prepare_fields(__document_config['fields'])

DOCUMENT_LABELS = dict()

for __document_type_key, __document_config in DOCUMENT_TYPE_CONFIG_PAIRS:
    if 'labels' not in __document_config:
        DOCUMENT_LABELS[__document_type_key] = None
    else:
        DOCUMENT_LABELS[__document_type_key] = prepare_labels(__document_config['labels'])


def list_countries():
    return [*COUNTRIES_CONFIG]


def list_document_types(country):
    document_types = []
    for document_type_key, document_config in DOCUMENT_TYPE_CONFIG_PAIRS:
        if document_type_key[:document_type_key.find('.')] == country:
            document_types.append(document_type_key[document_type_key.find('.documents.') + len('.documents.'):])
    return document_types


def get_document_type_key(country, document_type):
    document_type_key = country + '.documents.' + document_type
    if document_type_key not in DOCUMENT_PRE_PROCESSOR:
        document_type_key = 'global.documents.' + document_type
    return document_type_key


def get_document_pre_processor(country, document_type):
    return DOCUMENT_PRE_PROCESSOR[get_document_type_key(country, document_type)]


def get_document_fields(country, document_type):
    return DOCUMENT_FIELDS[get_document_type_key(country, document_type)]


def get_document_labels(country, document_type):
    return DOCUMENT_LABELS[get_document_type_key(country, document_type)]
