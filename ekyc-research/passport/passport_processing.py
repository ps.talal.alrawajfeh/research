#!/usr/bin/python3.6
import math

import cv2 as cv
import imutils as im
import numpy as np
from scipy.ndimage import interpolation as inter

SKEW_CORRECTION_DELTA = 0.5
SKEW_CORRECTION_LIMIT = 5

DETECTION_ASPECT_RATION_THRESHOLD = 4
DETECTION_COVERAGE_WIDTH_THRESHOLD = 0.5

SPECIAL_BINARIZATION_ALPHA = 1.0
SPECIAL_BINARIZATION_BETA = 1.0

IMAGE_DESTINATION_WIDTH = 600
EROSION_ITERATIONS = 4

NATIONAL_ID_LENGTH = 10

MRZ_TESSERACT_CONFIG = '-l ocrb --oem 1 --psm 1'
MRZ_TESSERACT_VARS_FILE = 'mrz_tesseract_vars'


def curry_mrz_field_extractor(special_binarization_erosion_kernel_size):
    def curried(src_image, _):
        return passport_mrz_field_extractor(src_image, _,
                                            special_binarization_erosion_kernel_size)

    return curried


def passport_mrz_field_extractor(src_image, _, special_binarization_erosion_kernel_size):
    height, width, ch = src_image.shape
    image = im.resize(src_image, width=IMAGE_DESTINATION_WIDTH)
    gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)

    rect_kernel = cv.getStructuringElement(cv.MORPH_RECT, (13, 5))
    sq_kernel = cv.getStructuringElement(cv.MORPH_RECT, (21, 21))

    # smooth the image using a 3x3 Gaussian, then apply the blackhat
    # morphological operator to find dark regions on a light background
    gray = cv.GaussianBlur(gray, (3, 3), 0)
    black_hat_image = cv.morphologyEx(gray, cv.MORPH_BLACKHAT, rect_kernel)

    # compute the Scharr gradient of the blackhat image and scale the
    # result into the range [0, 255]
    grad_x = cv.Sobel(black_hat_image, ddepth=cv.CV_32F, dx=1, dy=0, ksize=-1)
    grad_x = np.absolute(grad_x)
    (minVal, maxVal) = (np.min(grad_x), np.max(grad_x))
    grad_x = (255 * ((grad_x - minVal) / (maxVal - minVal))).astype(np.uint8)

    # apply a closing operation using the rectangular kernel to close
    # gaps in between letters -- then apply Otsu's thresholding method
    grad_x = cv.morphologyEx(grad_x, cv.MORPH_CLOSE, rect_kernel)
    thresh = cv.threshold(grad_x, 0, 255, cv.THRESH_BINARY | cv.THRESH_OTSU)[1]

    # clean speckles and small segments
    thresh = cv.erode(thresh, np.ones((5, 5)))
    thresh = cv.dilate(thresh, np.ones((5, 5)))

    # perform another closing operation, this time using the square
    # kernel to close gaps between lines of the MRZ, then perform a
    # series of erosions to break apart connected components
    thresh = cv.morphologyEx(thresh, cv.MORPH_CLOSE, sq_kernel)
    thresh = cv.erode(thresh, None, iterations=EROSION_ITERATIONS)

    # during thresholding, it's possible that border pixels were
    # included in the thresholding, so let's set 5% of the left and
    # right borders to zero
    p = int(image.shape[1] * 0.05)
    thresh[:, 0:p] = 0
    thresh[:, image.shape[1] - p:] = 0

    # find contours in the thresholded image and sort them by their
    # size
    contours = cv.findContours(thresh.copy(), cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
    contours = im.grab_contours(contours)
    contours = sorted(contours, key=cv.contourArea, reverse=True)

    roi_from_src_image = None
    found = False
    # loop over the contours
    for contour in contours:
        # compute the bounding box of the contour and use the contour to
        # compute the aspect ratio and coverage ratio of the bounding box
        # width to the width of the image
        (x, y, w, h) = cv.boundingRect(contour)
        ar = w / float(h)
        cr_width = w / float(gray.shape[1])

        if y < h / 2:
            continue

        # check to see if the aspect ratio and coverage width are within
        # acceptable criteria
        if ar > DETECTION_ASPECT_RATION_THRESHOLD and cr_width > DETECTION_COVERAGE_WIDTH_THRESHOLD:
            found = True
            # pad the bounding box since we applied erosions and now need
            # to re-grow it
            px = int((x + w) * 0.03)
            py = int((y + h) * 0.03)
            (x, y) = (x - px, y - py)
            (w, h) = (w + (px * 2), h + (py * 2))

            if x < 0:
                x = 0
            if y < 0:
                y = 0

            # extract the ROI from the image and draw a bounding box
            # surrounding the MRZ
            src_y1 = int(y / image.shape[0] * height)
            src_y2 = int((y + h) / image.shape[0] * height)
            src_x1 = int(x / image.shape[1] * width)
            src_x2 = int((x + w) / image.shape[1] * width)

            roi_from_src_image = src_image[src_y1:src_y2, src_x1:src_x2]

            break

    if found:
        binarized = special_binarization(roi_from_src_image, special_binarization_erosion_kernel_size)
        if binarized is None:
            raise Exception('cannot extract mrz field')
        return binarized
    else:
        raise Exception('cannot extract mrz field')


def calculate_geometric_mean(values, epsilon=1e-7):
    return np.exp(np.mean(np.log(values + epsilon)))


def calculate_geometric_std(geometric_mean, values, epsilon=1e-7):
    return np.exp(np.sqrt(np.mean(np.power(np.log((values + epsilon) / geometric_mean), 2))))


def special_binarization(image, erosion_kernel_size=(5, 5)):
    rgb_channels = cv.split(image)

    result_channels = []
    for channel in rgb_channels:
        eroded = cv.erode(channel, np.ones(erosion_kernel_size, np.uint8), iterations=1)
        result_channels.append(eroded)

    result = 255 - np.array(result_channels)
    result = np.array(result, np.float)
    result = np.product(result, axis=0)

    values = result.flatten()
    values /= np.max(values)
    values *= 255
    values = 255 - values
    values = values ** 2
    values /= np.max(values)
    values *= 255

    geometric_mean = calculate_geometric_mean(values)
    geometric_std = calculate_geometric_std(geometric_mean, values)

    if math.isnan(geometric_mean) or math.isnan(geometric_std) or \
            math.isinf(geometric_mean) or math.isinf(geometric_std):
        return None

    threshold = geometric_mean * SPECIAL_BINARIZATION_ALPHA + geometric_std * SPECIAL_BINARIZATION_BETA

    result /= np.max(result)
    result *= 255
    result = 255 - np.array(result, np.uint8)
    result[result > threshold] = 255
    result[result <= threshold] = 0

    return cv.cvtColor(result, cv.COLOR_GRAY2BGR)


def passport_mrz_pre_processor(src_image):
    otsu_bin_image = apply_otsu(src_image)
    corrected_skew_image = correct_skew(otsu_bin_image)
    noise_removed_image = cv.medianBlur(corrected_skew_image, 5)
    return noise_removed_image


def apply_otsu(src_image):
    gray_image = cv.cvtColor(src_image, cv.COLOR_BGR2GRAY)
    ret, otsu_bin = cv.threshold(gray_image, 0, 255, cv.THRESH_BINARY + cv.THRESH_OTSU)
    return otsu_bin


def correct_skew(src_image):
    angles = np.arange(-SKEW_CORRECTION_LIMIT, SKEW_CORRECTION_LIMIT + SKEW_CORRECTION_DELTA, SKEW_CORRECTION_DELTA)
    scores = []
    for angle in angles:
        hist, score = find_score(src_image, angle)
        scores.append(score)

    best_score = max(scores)
    best_angle = angles[scores.index(best_score)]

    # correct skew
    inverted = cv.bitwise_not(src_image)
    return cv.bitwise_not(im.rotate(inverted, best_angle))


def find_score(arr, angle):
    data = inter.rotate(arr, angle, reshape=False, order=0)
    hist = np.sum(data, axis=1)
    score = np.sum((hist[1:] - hist[:-1]) ** 2)
    return hist, score
