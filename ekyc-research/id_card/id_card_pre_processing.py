#!/usr/bin/python3.6

import math
from typing import Tuple, Union, Callable

import cv2
import numpy as np
from numba import njit
from scipy.spatial import ConvexHull
from sklearn.cluster import MiniBatchKMeans

from configuration.resources_reader import ImageResource
from imaging_utils import find_best_contour, otsu_threshold, warp_perspective, clean_binary_image, find_largest_contour, \
    TEMPLATE_SCORE_THRESHOLD

ID_CARD_FINAL_WIDTH = 750
COLOR_SEGMENTATION_MAXIMUM_DIMENSION = 500
NORMALIZED_CROSS_CORRELATION_THRESHOLD = 0.75
MIN_BLACK_PIXELS_TO_AREA_RATIO = 0.1
TEXT_CROPPING_MARGIN = 5
ID_CARD_AREA_TO_IMAGE_AREA_MIN_RATIO = 0.125
WHITE_BORDER_PIXEL_RATIO = 0.65
THRESHOLD_CORRECTION_MARGIN_SIZE = 20
K_MEANS_BATCH_SIZE_PERCENTAGE = 0.0025
EPSILON = 1e-6

SINGLE_POINT = 'point'
ORDINARY_LINE = 'ordinary'
HORIZONTAL_LINE = 'horizontal'
VERTICAL_LINE = 'vertical'


def resize_image(image_bgr: np.ndarray, image_max_dimension: int) -> Tuple[float, np.ndarray]:
    if image_max_dimension > COLOR_SEGMENTATION_MAXIMUM_DIMENSION:
        resize_factor = COLOR_SEGMENTATION_MAXIMUM_DIMENSION / image_max_dimension
        resized_image = cv2.resize(image_bgr,
                                   (0, 0),
                                   fx=resize_factor,
                                   fy=resize_factor,
                                   interpolation=cv2.INTER_CUBIC)
    else:
        resize_factor = 1.0
        resized_image = np.array(image_bgr)
    return resize_factor, resized_image


def correct_threshold(binary_image: np.ndarray) -> np.ndarray:
    black_pixels_count = np.count_nonzero(binary_image == 0)
    if black_pixels_count < ID_CARD_AREA_TO_IMAGE_AREA_MIN_RATIO * binary_image.shape[0] * binary_image.shape[1]:
        return binary_image
    margin_size = THRESHOLD_CORRECTION_MARGIN_SIZE
    margin_colors = np.concatenate([binary_image[:, :margin_size].ravel(),
                                    binary_image[:margin_size, :].ravel(),
                                    binary_image[:, -margin_size:].ravel(),
                                    binary_image[-margin_size:, :].ravel()])
    if np.mean(margin_colors) / 255.0 > WHITE_BORDER_PIXEL_RATIO:
        return 255 - binary_image
    return binary_image


def threshold_components(image: np.ndarray) -> Tuple[np.ndarray, ...]:
    return tuple(clean_binary_image(correct_threshold(otsu_threshold(image[:, :, i]))) for i in range(image.shape[-1]))


@njit
def compute_center(points: np.ndarray) -> np.ndarray:
    return np.array([np.mean(points[:, :1]), np.mean(points[:, 1:])])


@njit
def distance(point1: np.ndarray, point2: np.ndarray) -> float:
    return np.sqrt(np.sum(np.square(np.subtract(point1, point2))))


@njit
def compute_point_score(reference_points: np.ndarray, point: np.ndarray) -> float:
    score = 1.0
    for reference_point in reference_points:
        score *= distance(reference_point, point)
    return score


@njit
def find_furthest_point_from(reference_points: np.ndarray, points: np.ndarray) -> Tuple[np.ndarray, int]:
    max_point_score = -1.0
    furthest_point = None
    furthest_point_i = -1

    for i in range(len(points)):
        p = points[i]
        point_score = compute_point_score(reference_points, p)
        if point_score > max_point_score:
            max_point_score = point_score
            furthest_point = p
            furthest_point_i = i

    return furthest_point, furthest_point_i


def find_convex_polygon_vertices_from_contour(polygon_contour: np.ndarray,
                                              polygon_center_point: np.ndarray,
                                              number_of_vertices: int) -> Tuple[list, list]:
    vertices = []
    vertices_i = []
    for n in range(number_of_vertices):
        vertex, vertex_i = find_furthest_point_from(np.array([polygon_center_point] + vertices),
                                                    polygon_contour)
        vertices.append(vertex)
        vertices_i.append(vertex_i)
    return vertices, vertices_i


def find_rectangle_vertices_from_contour(contour: np.ndarray, center_point: np.ndarray) -> Tuple[list, list]:
    return find_convex_polygon_vertices_from_contour(contour,
                                                     center_point,
                                                     4)


def get_contour_segments(contour: np.ndarray, rectangle_vertices_indices: list) -> list[np.ndarray]:
    contour_list = contour.tolist()
    segments = []

    for i in range(len(rectangle_vertices_indices)):
        current_i = rectangle_vertices_indices[i]
        next_i = rectangle_vertices_indices[(i + 1) % len(rectangle_vertices_indices)]
        if current_i < next_i:
            segment = np.array(contour_list[next_i:] + contour_list[:current_i], np.float)
        else:
            segment = np.array(contour_list[next_i:current_i], np.float)
        segments.append(segment)

    return segments


@njit
def distance_between_point_and_line(line_initial_point: np.ndarray,
                                    line_terminal_point: np.ndarray,
                                    point: np.ndarray) -> float:
    x1, y1 = line_initial_point
    x2, y2 = line_terminal_point
    x0, y0 = point
    if distance(line_initial_point, line_terminal_point) < EPSILON:
        return distance(line_initial_point, point)
    return abs((x2 - x1) * (y1 - y0) - (x1 - x0) * (y2 - y1)) / distance(
        line_initial_point, line_terminal_point)


@njit
def segment_linearity_error(segment: np.ndarray) -> Tuple[float, int]:
    initial_point = segment[0]
    terminal_point = segment[-1]

    total_error = 0.0
    n = 0
    for i in range(1, len(segment) - 1):
        total_error += distance_between_point_and_line(initial_point, terminal_point, segment[i])
        n += 1

    return total_error, n


@njit
def linearity_error(points: np.ndarray, segment_size: int = 20) -> float:
    total_error = 0.0
    n = 0

    i = 0
    while i < len(points) // segment_size:
        segment = points[i * segment_size: (i + 1) * segment_size]
        segment_total_error, segment_n = segment_linearity_error(segment)
        total_error += segment_total_error
        n += segment_n
        i += 1

    remainder = len(points) % segment_size
    if remainder != 0:
        segment = points[i * segment_size:]
        segment_total_error, segment_n = segment_linearity_error(segment)
        total_error += segment_total_error
        n += segment_n

    if n == 0:
        return 0.0

    return total_error / n


@njit
def adjust_angle_by_quadrant(angle: float, point: tuple[int, int]) -> float:
    x, y = point
    if x < 0 and y > 0:
        return 180 - angle
    if x < 0 and y < 0:
        return 180 + angle
    if x > 0 and y < 0:
        return 360 - angle
    return angle


@njit
def compute_angle(origin: np.ndarray, point: np.ndarray) -> float:
    x1, y1 = np.subtract(point, origin)
    if math.fabs(x1) > 1e-6:
        theta = math.fabs(math.atan(y1 / x1)) / math.pi * 180
    else:
        theta = 90
    return adjust_angle_by_quadrant(theta, (x1, y1))


def sort_points_anti_clockwise(origin: np.array, points: np.array, points_indices: list) -> Tuple[list, list]:
    point_angle_pairs = [[points[i], points_indices[i], compute_angle(points[i], origin)] for i in range(len(points))]
    point_angle_pairs.sort(key=lambda x: x[2])
    return [point for point, _, _ in point_angle_pairs], [point_i for _, point_i, _ in point_angle_pairs]


def compute_image_score_for_id_card_extraction(binary_image: np.ndarray,
                                               id_card_width_height_ratio: float) -> Tuple[float,
                                                                                           Union[np.ndarray, None],
                                                                                           Union[list[int], None]]:
    area = binary_image.shape[0] * binary_image.shape[1]
    black_pixels_count = np.count_nonzero(binary_image == 0)

    if black_pixels_count / area < MIN_BLACK_PIXELS_TO_AREA_RATIO:
        return 0.0, None, None

    # note: the label 0 is always preserved for labeling the background
    # note: the other foreground labels start from 1 (then 2, 3, etc.)
    labels_count, labeled_image, stats, centroids = cv2.connectedComponentsWithStats(binary_image, 8, cv2.CV_32S)
    if labels_count == 1:
        return 0.0, None, None

    labeled_image = np.transpose(labeled_image)
    sizes = stats[:, -1]
    max_size = sizes[1]
    max_score = 0.0
    best_coordinates = None
    best_label = 1
    best_size = sizes[1]
    for i in range(1, labels_count):
        if sizes[i] > max_size:
            max_size = sizes[i]
        coordinates = np.transpose(np.where(labeled_image == i))
        _, (width, height), _ = cv2.minAreaRect(coordinates)
        if width * height / area < 0.1:
            continue
        ratio = max(width / height, height / width)
        score = 1.0 / (1.0 + abs(ratio - id_card_width_height_ratio))
        if score > max_score:
            max_score = score
            best_coordinates = coordinates
            best_label = i
            best_size = sizes[i]

    if max_size / area < ID_CARD_AREA_TO_IMAGE_AREA_MIN_RATIO:
        return 0.0, None, None

    convex_hull = ConvexHull(best_coordinates)
    score1 = best_size / convex_hull.volume

    connected_component = np.array(np.transpose(labeled_image == best_label), np.uint8) * 255
    best_contour = find_largest_contour(connected_component)
    center_point = compute_center(best_contour)
    rectangle_vertices, rectangle_vertices_i = find_rectangle_vertices_from_contour(best_contour, center_point)
    rectangle_vertices, rectangle_vertices_i = sort_points_anti_clockwise(center_point,
                                                                          rectangle_vertices,
                                                                          rectangle_vertices_i)
    segments = get_contour_segments(best_contour, rectangle_vertices_i)
    score2 = np.mean([1 / (1 + linearity_error(segment) ** 3) for segment in segments])

    return 0.25 * score1 + 0.75 * score2, best_contour, rectangle_vertices_i


@njit
def estimate_parametric_slope_and_intercept(n: int, points: np.ndarray, axis: int = 0) -> Tuple[float, float]:
    coefficients = np.array([i + 1 for i in range(n)])
    sum1 = np.sum(points[:, axis] * coefficients)
    sum2 = np.sum(points[:, axis])
    slope = 12 * (sum1 - sum2 * (n + 1) / 2) / (n * (n + 1) * (n - 1))
    intercept = sum2 / n - slope * (n + 1) / 2
    return slope, intercept


@njit
def parametric_linear_regression(points: np.ndarray) -> Tuple[Tuple[float, float],
                                                              Tuple[float, float]]:
    n = len(points)

    slope_x, intercept_x = estimate_parametric_slope_and_intercept(n, points)
    slope_y, intercept_y = estimate_parametric_slope_and_intercept(n, points, 1)

    return (slope_x, intercept_x), (slope_y, intercept_y)


def parametric_line_to_non_parametric(parametric_line: Tuple[Tuple[float, float],
                                                             Tuple[float, float]]) -> Tuple[str, list[float]]:
    (slope_x, intercept_x), (slope_y, intercept_y) = parametric_line

    if slope_x == 0 and slope_y == 0:
        return SINGLE_POINT, [intercept_x, intercept_y]
    if slope_x == 0:
        return VERTICAL_LINE, [intercept_x]
    if slope_y == 0:
        return HORIZONTAL_LINE, [intercept_y]

    slope = slope_y / slope_x
    return ORDINARY_LINE, [slope, intercept_y - slope * intercept_x]


def find_lines_intersection_point(line1: Tuple[str, list[float]],
                                  line2: Tuple[str, list[float]]) -> Union[list[float], None]:
    type1, parameters1 = line1
    type2, parameters2 = line2

    if type1 == SINGLE_POINT and type2 == SINGLE_POINT:
        if np.max(np.abs(np.subtract(parameters1, parameters2))) < EPSILON:
            return parameters1
        else:
            return None

    if type1 == SINGLE_POINT and type2 == VERTICAL_LINE:
        if abs(parameters1[0] - parameters2[0]) < EPSILON:
            return parameters1
        else:
            return None

    if type1 == SINGLE_POINT and type2 == HORIZONTAL_LINE:
        if abs(parameters1[1] - parameters2[0]) < EPSILON:
            return parameters1
        else:
            return None

    if type1 == SINGLE_POINT and type2 == ORDINARY_LINE:
        regressed = parameters1[0] * parameters2[0] + parameters2[1]
        if abs(regressed - parameters1[1]) < EPSILON:
            return parameters1
        else:
            return None

    if type1 == VERTICAL_LINE and type2 == VERTICAL_LINE:
        if np.max(np.abs(np.subtract(parameters1, parameters2))) < EPSILON:
            return [parameters1[0], parameters1[0]]
        else:
            return None

    if type1 == VERTICAL_LINE and type2 == HORIZONTAL_LINE:
        return [parameters1[0], parameters2[0]]

    if type1 == VERTICAL_LINE and type2 == ORDINARY_LINE:
        return [parameters1[0], parameters1[0] * parameters2[0] + parameters2[1]]

    if type1 == HORIZONTAL_LINE and type2 == HORIZONTAL_LINE:
        if parameters1 == parameters2:
            return [parameters1[0], parameters1[0]]
        else:
            return None

    if type1 == HORIZONTAL_LINE and type2 == ORDINARY_LINE:
        return [(parameters1[0] - parameters2[1]) / parameters2[0], parameters1[0]]

    if type1 == ORDINARY_LINE and type2 == ORDINARY_LINE:
        x = (parameters2[1] - parameters1[1]) / (parameters1[0] - parameters2[0])
        return [x, parameters1[0] * x + parameters1[1]]

    return find_lines_intersection_point(line2, line1)


def enhance_rectangle_vertices(contour: np.ndarray, rectangle_vertices_indices: list[int]) -> np.ndarray:
    segments = get_contour_segments(contour, rectangle_vertices_indices)
    lines = [parametric_line_to_non_parametric(parametric_linear_regression(segment))
             for segment in segments]
    return np.array([find_lines_intersection_point(lines[i - 1], lines[i])
                     for i in range(len(segments))], np.int)


def auto_correct_vertices_order(vertices: np.ndarray) -> np.ndarray:
    point1, point2, point3, point4 = vertices

    side1_length = distance(point1, point2)
    corresponding_side1_length = distance(point4, point3)

    side2_length = distance(point2, point3)
    corresponding_side2_length = distance(point1, point4)

    if max(side1_length, corresponding_side1_length) < min(side2_length, corresponding_side2_length):
        return np.array([point2,
                         point3,
                         point4,
                         point1])

    return vertices


def triangle_area(point1: Tuple[float, float],
                  point2: Tuple[float, float],
                  point3: Tuple[float, float]) -> float:
    x1, y1 = point1
    x2, y2 = point2
    x3, y3 = point3
    return math.fabs(0.5 * (x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)))


def calculate_4_polygon_area(point1: Tuple[float, float],
                             point2: Tuple[float, float],
                             point3: Tuple[float, float],
                             point4: Tuple[float, float]) -> float:
    return triangle_area(point1, point2, point3) + \
           triangle_area(point1, point4, point3)


def estimate_card_width_height_from_vertices(vertices: list[Tuple[float, float]],
                                             id_card_width_height_ratio: float) -> Tuple[int, int]:
    area = calculate_4_polygon_area(*vertices)
    height = math.sqrt(area / id_card_width_height_ratio)
    width = id_card_width_height_ratio * height
    return int(math.floor(width)), int(math.floor(height))


def correct_perspective(image: np.ndarray,
                        original_vertices: list[Tuple[float, float]],
                        id_card_width_height_ratio: float) -> np.ndarray:
    target_width, target_height = estimate_card_width_height_from_vertices(original_vertices,
                                                                           id_card_width_height_ratio)

    target_rectangle_vertices = [(0, 0),
                                 (target_width - 1, 0),
                                 (target_width - 1, target_height - 1),
                                 (0, target_height - 1)]

    return warp_perspective(image,
                            original_vertices,
                            target_rectangle_vertices,
                            target_width,
                            target_height)


def resize_id_card_to_final_dimensions(id_card_image: np.ndarray, id_card_width_height_ratio: float) -> np.ndarray:
    id_card_final_height = int(math.floor(ID_CARD_FINAL_WIDTH / id_card_width_height_ratio))
    return cv2.resize(id_card_image, (ID_CARD_FINAL_WIDTH, id_card_final_height), cv2.INTER_CUBIC)


def extract_id_card(image_bgr: np.ndarray, id_card_width_height_ratio: float = 1.575) -> Union[np.ndarray, None]:
    image_max_dimension = max(image_bgr.shape)
    resize_factor, resized_image = resize_image(image_bgr, image_max_dimension)
    image_hsv = cv2.cvtColor(resized_image, cv2.COLOR_BGR2HSV)
    hue_binary, saturation_binary, value_binary = threshold_components(image_hsv)

    hue_info = compute_image_score_for_id_card_extraction(hue_binary, id_card_width_height_ratio)
    saturation_info = compute_image_score_for_id_card_extraction(saturation_binary, id_card_width_height_ratio)
    value_info = compute_image_score_for_id_card_extraction(value_binary, id_card_width_height_ratio)

    # view_image(hue_binary, hue_info[0])
    # view_image(saturation_binary, saturation_info[0])
    # view_image(value_binary, value_info[0])

    components = [hue_info, saturation_info, value_info]
    components.sort(key=lambda x: x[0])

    max_score = components[-1][0]
    if max_score < EPSILON:
        return None

    best_contour = components[-1][1]
    rectangle_vertices_i = components[-1][2]

    rectangle_vertices = enhance_rectangle_vertices(best_contour, rectangle_vertices_i)
    rectangle_vertices = auto_correct_vertices_order(rectangle_vertices)

    corrected_perspective = correct_perspective(image_bgr,
                                                [(x / resize_factor, y / resize_factor)
                                                 for x, y in rectangle_vertices],
                                                id_card_width_height_ratio)

    return resize_id_card_to_final_dimensions(corrected_perspective, id_card_width_height_ratio)


def match_template(image: np.ndarray,
                   template: np.ndarray,
                   matching_method: int = cv2.TM_CCOEFF_NORMED) -> Tuple[Tuple[int, int], float]:
    matching_result = cv2.matchTemplate(image, template, matching_method)
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(matching_result)
    if matching_method in [cv2.TM_SQDIFF, cv2.TM_SQDIFF_NORMED]:
        location = min_loc
        score = min_val
    else:
        location = max_loc
        score = max_val
    return location, score


def find_label_location(image: np.ndarray,
                        label: np.ndarray,
                        approximate_location: Tuple[int, int],
                        neighborhood_search_deltas: Tuple[int, int]) -> Tuple[Tuple[int, int], float]:
    lx, ly = approximate_location
    dx, dy = neighborhood_search_deltas
    template = label

    x1 = max(0, lx - dx)
    x2 = min(image.shape[1], lx + template.shape[1] + dx)
    y1 = max(0, ly - dy)
    y2 = min(image.shape[0], ly + template.shape[0] + dy)

    image_part = image[y1:y2, x1:x2]
    max_location, max_score = match_template(image_part, label)

    for i in range(0, image_part.shape[1] - template.shape[1]):
        width = template.shape[1] + i
        height = int(round(template.shape[0] / template.shape[1] * width))

        resized_template = cv2.resize(template, (width, height), interpolation=cv2.INTER_CUBIC)
        location, score = match_template(image_part, resized_template)

        if score > max_score:
            max_score = score
            max_location = location

    for i in range(1, template.shape[1] // 2):
        width = template.shape[1] - i
        height = int(round(template.shape[0] / template.shape[1] * width))

        resized_template = cv2.resize(template, (width, height), interpolation=cv2.INTER_CUBIC)
        location, score = match_template(image_part, resized_template)

        if score > max_score:
            max_score = score
            max_location = location

    return (x1 + max_location[0], y1 + max_location[1]), max_score


def correct_id_card_orientation(final_resized: np.ndarray,
                                label_path: str,
                                approximate_location: Tuple[int, int],
                                neighborhood_search_deltas: Tuple[int, int],
                                template_score_threshold: float = TEMPLATE_SCORE_THRESHOLD) \
        -> Union[None, Tuple[np.ndarray, Tuple[Tuple[int, int], Tuple[int, int]]]]:
    final_resized_gray_scale = cv2.cvtColor(final_resized, cv2.COLOR_BGR2GRAY)

    label = ImageResource(label_path).read_image()
    location1, correlation1 = find_label_location(final_resized_gray_scale,
                                                  label,
                                                  approximate_location,
                                                  neighborhood_search_deltas)

    if correlation1 > NORMALIZED_CROSS_CORRELATION_THRESHOLD:
        return final_resized, (location1, (label.shape[1], label.shape[0]))

    flipped_label = cv2.flip(label, -1)
    label_approximate_location = (
        final_resized_gray_scale.shape[1] - approximate_location[0] - flipped_label.shape[1],
        final_resized_gray_scale.shape[0] - approximate_location[1] - flipped_label.shape[0])

    location2, correlation2 = find_label_location(final_resized_gray_scale,
                                                  flipped_label,
                                                  label_approximate_location,
                                                  neighborhood_search_deltas)

    location2 = (final_resized_gray_scale.shape[1] - location2[0] - flipped_label.shape[1],
                 final_resized_gray_scale.shape[0] - location2[1] - flipped_label.shape[0])

    if max(correlation1, correlation2) < template_score_threshold:
        return None

    if correlation1 > correlation2:
        return final_resized, (location1, (label.shape[1], label.shape[0]))
    else:
        return cv2.flip(final_resized, -1), (location2, (label.shape[1], label.shape[0]))


def color_clustering_extraction(image_bgr: np.ndarray, id_card_width_height_ratio: float = 1.575) -> Union[np.ndarray,
                                                                                                           None]:
    image_max_dimension = max(image_bgr.shape)
    resize_factor, resized_image = resize_image(image_bgr, image_max_dimension)
    image_hsv = cv2.cvtColor(resized_image, cv2.COLOR_BGR2HSV)

    color_points = image_hsv.reshape(-1, image_hsv.shape[-1])
    k_means = MiniBatchKMeans(n_clusters=2, batch_size=int(len(color_points) * K_MEANS_BATCH_SIZE_PERCENTAGE))
    k_means.fit(color_points)
    clustered_image = np.array(k_means.labels_, np.uint8).reshape(image_hsv.shape[0:2])
    clustered_image *= 255
    clustered_image = correct_threshold(clustered_image)
    clustered_image = clean_binary_image(clustered_image)
    # view_image(clustered_image)
    best_contour = find_best_contour(clustered_image, id_card_width_height_ratio)
    if best_contour is None:
        return None

    center_point = compute_center(best_contour)
    rectangle_vertices, rectangle_vertices_i = find_rectangle_vertices_from_contour(best_contour, center_point)
    rectangle_vertices, rectangle_vertices_i = sort_points_anti_clockwise(center_point,
                                                                          rectangle_vertices,
                                                                          rectangle_vertices_i)
    rectangle_vertices = enhance_rectangle_vertices(best_contour, rectangle_vertices_i)
    rectangle_vertices = auto_correct_vertices_order(rectangle_vertices)

    corrected_perspective = correct_perspective(image_bgr,
                                                [(x / resize_factor, y / resize_factor)
                                                 for x, y in rectangle_vertices],
                                                id_card_width_height_ratio)

    return resize_id_card_to_final_dimensions(corrected_perspective, id_card_width_height_ratio)


def pre_process_id_card(image: np.ndarray,
                        id_card_width_height_ratio: float,
                        label_info: dict,
                        apply_dewarping: bool) -> Union[Tuple[np.ndarray, Tuple[Tuple[int, int], Tuple[int, int]]],
                                                        None]:
    final_image = image
    first_method_failed = False
    if apply_dewarping:
        final_image = extract_id_card(image,
                                      id_card_width_height_ratio)
        if final_image is None:
            first_method_failed = True
            final_image = color_clustering_extraction(image,
                                                      id_card_width_height_ratio)

    if not first_method_failed:
        orientation_correction_result = correct_id_card_orientation(final_image,
                                                                    label_info['label_path'],
                                                                    tuple(label_info['approximate_location']),
                                                                    tuple(label_info['neighborhood_search_deltas']))
        if orientation_correction_result is not None:
            return orientation_correction_result
        final_image = color_clustering_extraction(image,
                                                  id_card_width_height_ratio)

    if final_image is None:
        return None

    orientation_correction_result = correct_id_card_orientation(final_image,
                                                                label_info['label_path'],
                                                                tuple(label_info['approximate_location']),
                                                                tuple(label_info['neighborhood_search_deltas']))

    if orientation_correction_result is not None:
        return orientation_correction_result

    if image.shape[1] > image.shape[0]:
        final_image = image
    else:
        final_image = cv2.rotate(image, cv2.ROTATE_90_COUNTERCLOCKWISE)
    final_image = resize_id_card_to_final_dimensions(final_image, id_card_width_height_ratio)

    return correct_id_card_orientation(final_image,
                                       label_info['label_path'],
                                       tuple(label_info['approximate_location']),
                                       tuple(label_info['neighborhood_search_deltas']),
                                       0.0)


def curry_pre_process_id_card(id_card_width_height_ratio: float,
                              label_info: dict) -> Callable[[np.ndarray, bool],
                                                            Tuple[np.ndarray,
                                                                  Tuple[Tuple[int, int], Tuple[int, int]]]]:
    def curried(image, apply_dewarping):
        return pre_process_id_card(image,
                                   id_card_width_height_ratio,
                                   label_info,
                                   apply_dewarping)

    return curried


def auto_crop_text(image: np.ndarray, threshold: int = 0, margin: int = TEXT_CROPPING_MARGIN) -> np.ndarray:
    mask = (255 - cv2.threshold(
        cv2.cvtColor(image, cv2.COLOR_BGR2HSV)[:, :, 2:3].squeeze(-1),
        0,
        255,
        cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]) > threshold

    rows, cols = image.shape[0:2]
    mask1, mask2 = mask.any(0), mask.any(1)
    col_start, col_end = mask1.argmax(), cols - mask1[::-1].argmax()
    row_start, row_end = mask2.argmax(), rows - mask2[::-1].argmax()

    row_start -= margin
    if row_start < 0:
        row_start = 0

    row_end += margin
    if row_end > rows:
        row_end = rows

    col_start -= margin
    if col_start < 0:
        col_start = 0

    col_end += margin
    if col_end >= cols:
        col_end = cols

    return image[row_start:row_end, col_start:col_end]
