#!/usr/bin/python3.6
import cv2

from id_card.id_card_pre_processing import find_label_location
from imaging_utils import text_smearing

ADD_LABEL_WIDTH = '+w'
SUBTRACT_LABEL_WIDTH = '-w'
ADD_LABEL_HEIGHT = '+h'
SUBTRACT_LABEL_HEIGHT = '-h'


def translate_relative_x_coordinate_code(relative_x_coordinate, field_label_size):
    if len(relative_x_coordinate) == 2:
        if relative_x_coordinate[1] == ADD_LABEL_WIDTH:
            return field_label_size[0]
        elif relative_x_coordinate[1] == SUBTRACT_LABEL_WIDTH:
            return -field_label_size[0]
        else:
            raise ValueError(f'relative code unknown: {relative_x_coordinate[1]}')
    return 0


def translate_relative_x_coordinate(relative_x_coordinate,
                                    field_label_location,
                                    field_label_size):
    x = field_label_location[0]
    x += relative_x_coordinate[0]
    x += translate_relative_x_coordinate_code(relative_x_coordinate, field_label_size)
    return x


def translate_relative_y_coordinate_code(relative_y_coordinate, field_label_size):
    if len(relative_y_coordinate) == 2:
        if relative_y_coordinate[1] == ADD_LABEL_HEIGHT:
            return field_label_size[1]
        elif relative_y_coordinate[1] == SUBTRACT_LABEL_HEIGHT:
            return -field_label_size[1]
        else:
            raise ValueError(f'relative code unknown: {relative_y_coordinate[1]}')
    return 0


def translate_relative_y_coordinate(relative_y_coordinate,
                                    field_label_location,
                                    field_label_size):
    y = field_label_location[1]
    y += relative_y_coordinate[0]
    y += translate_relative_y_coordinate_code(relative_y_coordinate, field_label_size)
    return y


def translate_relative_coordinates(relative_coordinates,
                                   field_label_location,
                                   field_label_size):
    x = translate_relative_x_coordinate(relative_coordinates[0],
                                        field_label_location,
                                        field_label_size)
    y = translate_relative_y_coordinate(relative_coordinates[1],
                                        field_label_location,
                                        field_label_size)
    return x, y


def id_card_field_extractor(document_image,
                            extra_info,
                            field_relative_location,
                            field_relative_size):
    label_location, label_size = extra_info

    x1, y1 = translate_relative_coordinates(field_relative_location,
                                            label_location,
                                            label_size)

    x2, y2 = translate_relative_coordinates(field_relative_size,
                                            label_location,
                                            label_size)
    return document_image[y1:y2, x1:x2]


def qatari_name_field_extractor(document_image,
                                _,
                                name_label,
                                name_label_approximate_location,
                                name_label_neighborhood_search_deltas,
                                field_relative_location,
                                field_relative_size):
    (x, y), score = find_label_location(cv2.cvtColor(document_image, cv2.COLOR_BGR2GRAY),
                                        name_label,
                                        name_label_approximate_location,
                                        name_label_neighborhood_search_deltas)
    x1 = max(0, x + name_label.shape[1] + field_relative_location[0])
    y1 = y + field_relative_location[1]
    x2 = min(document_image.shape[1], x1 + field_relative_size[0])
    y2 = min(document_image.shape[0], y1 + field_relative_size[1])
    return document_image[y1:y2, x1:x2]


def curry_id_card_field_extractor(field_relative_location,
                                  field_relative_size):
    def curried(document_image, extra_info):
        return id_card_field_extractor(document_image,
                                       extra_info,
                                       field_relative_location,
                                       field_relative_size)

    return curried


def curry_qatari_name_field_extractor(name_label,
                                      name_label_approximate_location,
                                      name_label_neighborhood_search_deltas,
                                      field_relative_location,
                                      field_relative_size):
    name_label_image = cv2.imread('./resources/' + name_label, cv2.IMREAD_GRAYSCALE)

    def curried(document_image, extra_info):
        return qatari_name_field_extractor(document_image,
                                           extra_info,
                                           name_label_image,
                                           name_label_approximate_location,
                                           name_label_neighborhood_search_deltas,
                                           field_relative_location,
                                           field_relative_size)

    return curried


def smearing_based_id_card_field_extractor(document_image: np.ndarray,
                                           extra_info: dict,
                                           field_relative_location: list,
                                           field_relative_size: list) -> np.ndarray:
    label_location, label_size = extra_info

    x1, y1 = translate_relative_coordinates(field_relative_location,
                                            label_location,
                                            label_size)

    x2, y2 = translate_relative_coordinates(field_relative_size,
                                            label_location,
                                            label_size)

    gray_image = cv2.cvtColor(document_image, cv2.COLOR_BGR2GRAY)
    smeared = text_smearing(gray_image[y1:y2, x1:x2])
    mask, rectangle = get_connected_component_with_largest_intersection(smeared, [x1, y1, x2 - x1, y2 - y1])

    x, y, width, height = rectangle
    return document_image[y: y + height, x:x + width]
