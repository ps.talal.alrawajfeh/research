# Support Database Issues Full-Text Search & Clustering

## Environment Setup

First, make sure you have python installed of version at least `3.6`. Next install the requirements using the command:

```bash
python3 -m pip install -r --upgrade requirements.txt
```

Now run the script `download_nltk_data.py` in this folder using the command:

```bash
python3 download_nltk_data.py
```

To use the Full-Text search script make sure you have an elastic search node running or just use the following commands:

```bash
docker pull elasticsearch:7.14.0
docker run -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" elasticsearch:7.14.0
```

To configure the support database url and the elastic search url, edit the `config.json` file inside this folder:

```
"support_db_url": "postgresql://sys:sys@localhost:5432/supportdb20210805",
"elastic_search_host": "http://localhost:9200"
```

## Issues Search

To be able to use the issues search API, you must first make sure that the issues are indexed on elasticsearch. If the issues are not indexed run the script `index_issues.py` using the command:

```bash
python3 index_issues.py
```

## Issues Clustering

To be able to use the issues clustering API, you must first make sure that the issues are clustered. If the issues are not clustered run the script `cluster_issues.py` using the command:

```bash
python3 cluster_issues.py
```

## APIs

To start the server, run the command:

```bash
python3 app.py
```

To see the documentation of the APIs, use one of the following urls: `/docs` or `/redoc`.