#!/usr/bin/python3

import json
import string

import numpy as np
import sqlalchemy as db
from scipy.spatial import distance
from sklearn.cluster import MiniBatchKMeans
from sklearn.decomposition import TruncatedSVD
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import Normalizer
from tqdm import tqdm

from commons.utils import combine_into_one_document, query_table_as_df, prepare_projects, prepare_lookups, \
    normalize_text, deserialize_object, serialize_object

with open('config.json', 'r') as config_file:
    config = json.load(config_file)

SUPPORT_DB_URL = config['support_db_url']
CLUSTERING_TOP_NUMBER_OF_ISSUES_TO_DISPLAY = config['clustering_top_number_of_issues_to_display']

MAX_NUMBER_OF_KEYWORDS = 128
MIN_DOCUMENT_FREQUENCY = 2
MAX_DOCUMENT_FREQUENCY = 0.5
CLUSTERING_MIN_ISSUE_COUNT = 100
ISSUES_FEATURES_FACTOR = 0.2
ISSUES_CLUSTERS_FACTOR = 0.1
FINAL_FEATURE_VECTOR_SIZE = 128
CLUSTERING_BATCH_SIZE = 100
MIN_FEATURES_COUNT = 130


def load_jira_issues(engine, connection):
    print('retrieving issues...')
    jira_issues_metadata = db.MetaData()
    jira_issues = db.Table('jiraissue',
                           jira_issues_metadata,
                           autoload=True,
                           autoload_with=engine)
    return query_table_as_df(connection, jira_issues, True)


def load_projects(engine, connection):
    projects_metadata = db.MetaData()
    projects = db.Table('project',
                        projects_metadata,
                        autoload=True,
                        autoload_with=engine)
    print('retrieving projects...')
    projects_df = query_table_as_df(connection, projects)
    projects_dict = prepare_projects(projects_df)
    return projects_dict


def load_lookups(engine, connection):
    lookups_metadata = db.MetaData()
    lookups = db.Table('lookups',
                       lookups_metadata,
                       autoload=True,
                       autoload_with=engine)
    print('retrieving lookups...')
    lookups_df = query_table_as_df(connection, lookups)
    countries_dict, products_dict, customers_dict = prepare_lookups(lookups_df)
    return countries_dict, customers_dict, products_dict


def get_product(project_key, products_dict, countries_dict):
    if project_key[:2] in countries_dict:
        product_key = project_key[2:4]
        if product_key in products_dict:
            product = products_dict[product_key]
        elif project_key[2:] in products_dict:
            product_key = project_key[2:]
            product = products_dict[product_key]
        else:
            product_key = ''
            product = ''
    elif project_key in products_dict:
        product_key = project_key
        product = products_dict[product_key]
    else:
        product_key = ''
        product = ''

    return product_key, product


def generate_embeddings_with_tfidf():
    engine = db.create_engine(SUPPORT_DB_URL)
    connection = engine.connect()

    jira_issues_df = load_jira_issues(engine, connection)
    projects_dict = load_projects(engine, connection)
    countries_dict, _, products_dict = load_lookups(engine, connection)

    ascii_characters = set(string.printable)
    issues_dict = dict()

    print('preparing issues...')
    progress_bar = tqdm(total=len(jira_issues_df))
    for i in range(len(jira_issues_df)):
        issue = jira_issues_df.iloc[i]
        progress_bar.update()
        if issue['summary'] is None and issue['description'] is None:
            continue

        project_id = int(issue['project'])
        project = projects_dict[project_id]
        project_key = str(project['key']).strip().lower()
        product_key, _ = get_product(project_key, products_dict, countries_dict)

        if product_key.strip() == '':
            continue

        document = combine_into_one_document([issue['summary'], issue['description']])
        document = ''.join(filter(lambda x: x in ascii_characters, document))
        text = ' '.join(normalize_text(document))

        if product_key not in issues_dict:
            issues_dict[product_key] = {
                'issue_ids': [int(issue['id'])],
                'documents': [text]
            }
        else:
            issues_dict[product_key]['issue_ids'].append(int(issue['id']))
            issues_dict[product_key]['documents'].append(text)
    progress_bar.close()

    print('generating feature vectors...')
    product_keys_to_remove = []
    for product_key in issues_dict:
        issues_count = len(issues_dict[product_key]['issue_ids'])
        if issues_count < CLUSTERING_MIN_ISSUE_COUNT:
            product_keys_to_remove.append(product_key)
            continue
        max_features = int(issues_count * ISSUES_FEATURES_FACTOR)
        if max_features < MIN_FEATURES_COUNT:
            max_features = MIN_FEATURES_COUNT
        vectorizer = TfidfVectorizer(max_df=MAX_DOCUMENT_FREQUENCY,
                                     max_features=max_features,
                                     min_df=MIN_DOCUMENT_FREQUENCY,
                                     token_pattern=r'(?u)\b[A-Za-z]+\b',
                                     stop_words='english',
                                     use_idf=True)
        documents = issues_dict[product_key]['documents']
        if len(documents) < MIN_DOCUMENT_FREQUENCY:
            product_keys_to_remove.append(product_key)
            continue
        try:
            issue_vectors = vectorizer.fit_transform(documents)
        except:
            product_keys_to_remove.append(product_key)
            continue

        svd = TruncatedSVD(FINAL_FEATURE_VECTOR_SIZE)
        normalizer = Normalizer(copy=False)
        lsa = make_pipeline(svd, normalizer)
        issue_vectors = lsa.fit_transform(issue_vectors)

        issues_dict[product_key]['issue_vectors'] = issue_vectors
        issues_dict[product_key]['issues_vectorizer'] = vectorizer
        issues_dict[product_key]['issues_svd'] = svd

    for key in product_keys_to_remove:
        issues_dict.pop(key)

    serialize_object(issues_dict, 'issues_dict.pickle')


def cluster_issues():
    issues_dict = deserialize_object('issues_dict.pickle')
    for k in issues_dict:
        print(f'clustering issues with product key: {k}...')
        issue_vectors = np.array(issues_dict[k]['issue_vectors'])
        clustering_model = MiniBatchKMeans(n_clusters=int(len(issue_vectors) * ISSUES_CLUSTERS_FACTOR),
                                           batch_size=CLUSTERING_BATCH_SIZE,
                                           verbose=True)
        issue_labels = clustering_model.fit(issue_vectors)
        issues_dict[k]['clustering_model'] = clustering_model
        issues_dict[k]['issue_labels'] = issue_labels
    serialize_object(issues_dict, 'issues_dict.pickle')


def annotate_clusters():
    issues_dict = deserialize_object('issues_dict.pickle')

    for product_key in issues_dict:
        print(f'annotating cluster of issues with product key: {product_key}...')
        issue_ids = issues_dict[product_key]['issue_ids']
        issue_labels = issues_dict[product_key]['issue_labels'].labels_
        issue_vectors = issues_dict[product_key]['issue_vectors']

        cluster_dict = dict()
        progress_bar = tqdm(total=len(issue_labels))
        for i in range(len(issue_labels)):
            label = issue_labels[i]
            issue_vector = issue_vectors[i]
            if label not in cluster_dict:
                cluster_dict[label] = {'issues': [(issue_ids[i], issue_vector)]}
            else:
                cluster_dict[label]['issues'].append((issue_ids[i], issue_vector))
            progress_bar.update()
        progress_bar.close()

        vectorizer = issues_dict[product_key]['issues_vectorizer']
        svd = issues_dict[product_key]['issues_svd']
        clustering_model = issues_dict[product_key]['clustering_model']

        centroids = clustering_model.cluster_centers_
        original_space_centroids = svd.inverse_transform(centroids)
        order_centroids = original_space_centroids.argsort()[:, ::-1]
        terms = vectorizer.get_feature_names()

        for i in range(len(order_centroids)):
            if i not in cluster_dict:
                continue
            cluster_keywords = []
            for ind in order_centroids[i, :MAX_NUMBER_OF_KEYWORDS]:
                cluster_keywords.append(terms[ind])
            cluster_dict[i]['keywords'] = cluster_keywords

        for label in cluster_dict:
            centroid = centroids[label]
            issues = cluster_dict[label]['issues']
            issues.sort(key=lambda x: distance.euclidean(centroid, x[1]))

        issues_dict[product_key]['cluster_dict'] = cluster_dict

    serialize_object(issues_dict, 'issues_dict.pickle')


if __name__ == '__main__':
    generate_embeddings_with_tfidf()
    cluster_issues()
    annotate_clusters()
