#!/usr/bin/python3
import pickle
import string

import pandas as pd
import sqlalchemy as db
from nltk import word_tokenize
from nltk.corpus import stopwords


def serialize_object(obj, file):
    with open(file, 'wb') as f:
        pickle.dump(obj, f)


def deserialize_object(file):
    with open(file, 'rb') as f:
        return pickle.load(f)


def is_excluded_word(word):
    return word in stopwords.words('english') or word in string.punctuation


def normalize_text(text):
    tokens = word_tokenize(text.lower())
    words = []

    for word in tokens:
        if is_excluded_word(word):
            continue
        words.append(word)

    return words


def pre_process(text):
    return '' if text is None else text


def combine_into_one_document(strings):
    return '\n'.join([pre_process(s) for s in strings])


def query_table_as_df(connection, table, sort_by_created_date=False):
    if sort_by_created_date:
        query = db.select(table).order_by(table.c.created.asc())
    else:
        query = db.select(table)
    result_proxy = connection.execute(query)
    result_set = result_proxy.fetchall()
    df = pd.DataFrame(result_set)
    df.columns = result_set[0].keys()
    return df


def prepare_projects(projects_df):
    projects_dict = dict()
    for i in range(len(projects_df)):
        project = projects_df.iloc[i]
        projects_dict[int(project['id'])] = {
            'id': int(project['id']),
            'name': str(project['pname']).strip().lower(),
            'key': str(project['pkey']).strip().lower()
        }
    return projects_dict


def prepare_lookups(lookups_df):
    countries_dict = dict()
    products_dict = dict()
    customers_dict = dict()
    for i in range(len(lookups_df)):
        lookup = lookups_df.iloc[i]
        lookup_type = str(lookup['type']).strip().lower()
        lookup_dict = {'key': str(lookup['key']).strip().lower(),
                       'value': str(lookup['value']).strip().lower()}
        if lookup_type == 'countries':
            countries_dict[lookup_dict['key']] = lookup_dict['value']
        if lookup_type == 'products':
            products_dict[lookup_dict['key']] = lookup_dict['value']
        if lookup_type == 'customers':
            customers_dict[lookup_dict['key']] = lookup_dict['value']
    return countries_dict, products_dict, customers_dict
