#!/usr/bin/python3

import json
import warnings

import sqlalchemy as db
from elasticsearch import Elasticsearch, helpers
from tqdm import tqdm

from commons.utils import query_table_as_df, combine_into_one_document, prepare_projects, prepare_lookups

warnings.simplefilter('ignore')

with open('config.json', 'r') as config_file:
    config = json.load(config_file)

SUPPORT_DB_URL = config['support_db_url']
ELASTIC_SEARCH_HOST = config['elastic_search_host']
ISSUES_BULK_SIZE = 100


def parse_project_key(project_key, countries_dict, customers_dict, products_dict):
    country_key = ''
    country = ''
    product_key = ''
    product = ''
    customer_key = ''
    customer = ''

    if project_key[:2] in countries_dict:
        country_key = project_key[:2]
        country = countries_dict[country_key]
        product_key = project_key[2:4]
        if product_key in products_dict:
            product = products_dict[product_key]
            customer_key = project_key[4:]
            if customer_key in customers_dict:
                customer = customers_dict[customer_key]
            else:
                customer_key = ''
                customer = ''
        elif project_key[2:] in products_dict:
            product_key = project_key[2:]
            product = products_dict[product_key]
            customer_key = ''
            customer = ''
        else:
            product_key = ''
            product = ''
            customer_key = project_key[2:]
            if customer_key in customers_dict:
                customer = customers_dict[customer_key]
            else:
                customer_key = ''
                customer = ''
    elif project_key in customers_dict:
        customer_key = project_key
        customer = customers_dict[customer_key]
    elif project_key in products_dict:
        product_key = project_key
        product = products_dict[product_key]

    return country, country_key, customer, customer_key, product, product_key


def document_from_issue(issue,
                        projects_dict,
                        countries_dict,
                        products_dict,
                        customers_dict):
    issue_id = int(issue['id'])

    project_id = int(issue['project'])
    project = projects_dict[project_id]
    project_key = str(project['key']).strip().lower()

    country, country_key, customer, customer_key, product, product_key = parse_project_key(project_key,
                                                                                           countries_dict,
                                                                                           customers_dict,
                                                                                           products_dict)

    return {
        'text': combine_into_one_document([str(issue['summary']),
                                           str(issue['description'])]),
        'project_name': str(project['name']).strip().lower(),
        'project_key': project_key,
        'country_key': country_key,
        'country': country,
        'product_key': product_key,
        'product': product,
        'customer_key': customer_key,
        'customer': customer,
        'issue_id': issue_id
    }


def index_issues(jira_issues_df,
                 projects_dict,
                 countries_dict,
                 products_dict,
                 customers_dict):
    es = Elasticsearch([ELASTIC_SEARCH_HOST])
    bulks = len(jira_issues_df) // ISSUES_BULK_SIZE
    remainder = len(jira_issues_df) % ISSUES_BULK_SIZE
    if remainder != 0:
        progress_bar = tqdm(total=bulks + 1)
    else:
        progress_bar = tqdm(total=bulks)
    for i in range(bulks):
        documents = []
        for j in range(ISSUES_BULK_SIZE):
            row = jira_issues_df.iloc[i * ISSUES_BULK_SIZE + j]
            if row['summary'] is None and row['description'] is None:
                continue
            documents.append(document_from_issue(row,
                                                 projects_dict,
                                                 countries_dict,
                                                 products_dict,
                                                 customers_dict))
        helpers.bulk(es, documents, index='jira-issues')
        progress_bar.update()
    documents = []
    for j in range(remainder):
        row = jira_issues_df.iloc[bulks * ISSUES_BULK_SIZE + j]
        if row['summary'] is None and row['description'] is None:
            continue
        documents.append(document_from_issue(row,
                                             projects_dict,
                                             countries_dict,
                                             products_dict,
                                             customers_dict))
    helpers.bulk(es, documents, index='jira-issues')
    progress_bar.update()
    progress_bar.close()


def retrieve_data():
    engine = db.create_engine(SUPPORT_DB_URL)
    connection = engine.connect()

    jira_issues_metadata = db.MetaData()
    jira_issues = db.Table('jiraissue',
                           jira_issues_metadata,
                           autoload=True,
                           autoload_with=engine)

    projects_metadata = db.MetaData()
    projects = db.Table('project',
                        projects_metadata,
                        autoload=True,
                        autoload_with=engine)

    lookups_metadata = db.MetaData()
    lookups = db.Table('lookups',
                       lookups_metadata,
                       autoload=True,
                       autoload_with=engine)

    print('retrieving issues...')
    jira_issues_df = query_table_as_df(connection, jira_issues, True)

    print('retrieving projects...')
    projects_df = query_table_as_df(connection, projects)
    projects_dict = prepare_projects(projects_df)

    print('retrieving lookups...')
    lookups_df = query_table_as_df(connection, lookups)
    countries_dict, products_dict, customers_dict = prepare_lookups(lookups_df)

    print('indexing issues...')
    index_issues(jira_issues_df,
                 projects_dict,
                 countries_dict,
                 products_dict,
                 customers_dict)


if __name__ == '__main__':
    retrieve_data()
