#!/usr/bin/python3

import json

from fastapi import APIRouter, Form

from commons.utils import deserialize_object

with open('config.json', 'r') as config_file:
    config = json.load(config_file)

CLUSTERING_TOP_NUMBER_OF_ISSUES_TO_DISPLAY = config['clustering_top_number_of_issues_to_display']
issues_dict = deserialize_object('issues_dict.pickle')

router = APIRouter()


@router.get('/list-info', tags=['clustering'])
def list_info():
    response = {}

    for product_key in issues_dict:
        response[product_key] = {
            'number_of_issues': len(issues_dict[product_key]['issue_ids']),
            'number_of_clusters': len(issues_dict[product_key]['cluster_dict'])
        }

    return response


@router.post('/top-issues', tags=['clustering'])
def top_issues(product_key: str = Form(...)):
    cluster_dict = issues_dict[product_key]['cluster_dict']
    cluster_keys = [*cluster_dict]
    cluster_keys.sort(key=lambda k: len(cluster_dict[k]['issues']))
    cluster_keys = reversed(cluster_keys)

    response = {}
    for key in cluster_keys:
        key = int(key)
        response[key] = {'keywords': cluster_dict[key]['keywords'], 'issues': []}
        for issue_id, _ in cluster_dict[key]['issues'][:CLUSTERING_TOP_NUMBER_OF_ISSUES_TO_DISPLAY]:
            response[key]['issues'].append({'issue_id': int(issue_id)})

    return response


@router.post('/all-issues', tags=['clustering'])
def all_issues(product_key: str = Form(...),
               cluster_key: str = Form(...)):
    cluster_dict = issues_dict[product_key]['cluster_dict']

    key = int(cluster_key)
    response = {'keywords': cluster_dict[key]['keywords'], 'issues': []}
    for issue_id, _ in cluster_dict[key]['issues'][:CLUSTERING_TOP_NUMBER_OF_ISSUES_TO_DISPLAY]:
        response['issues'].append({'issue_id': int(issue_id)})

    return response
