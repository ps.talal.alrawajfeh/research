#!/usr/bin/python3

import json
from typing import Optional

from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search, Q
from fastapi import APIRouter, Form

from commons.utils import normalize_text

router = APIRouter()

with open('config.json', 'r') as config_file:
    config = json.load(config_file)

SUPPORT_DB_URL = config['support_db_url']
ELASTIC_SEARCH_HOST = config['elastic_search_host']
MAX_NUMBER_OF_ISSUES_TO_DISPLAY = config['search_max_number_of_issues_to_display']

es = Elasticsearch([ELASTIC_SEARCH_HOST])


@router.post('/search', tags=['search'])
def search(query_text: str = Form(...),
           country_key: Optional[str] = Form(None),
           product_key: Optional[str] = Form(None),
           customer_key: Optional[str] = Form(None)):
    normalized_text = ' '.join(normalize_text(query_text))

    query = Search(using=es)

    queries = []
    if country_key != '' and country_key is not None:
        queries.append(Q('term', country_key=country_key.strip()))
    if product_key != '' and product_key is not None:
        queries.append(Q('term', product_key=product_key.strip()))
    if customer_key != '' and customer_key is not None:
        queries.append(Q('term', customer_key=customer_key.strip()))
    query = query.query('bool', filter=queries)

    query1 = query.query('match', text=normalized_text)
    results1 = query1[:MAX_NUMBER_OF_ISSUES_TO_DISPLAY].execute()

    query2 = query.query('fuzzy', text=normalized_text)
    results2 = query2[:MAX_NUMBER_OF_ISSUES_TO_DISPLAY].execute()

    if len(results1) == 0 and len(results2) == 0:
        return {'error': 'could not find any relevant issue matching your query.'}

    if len(results1) != 0:
        results = results1
        score = results[0].meta.score
    else:
        results = []
        score = 0.0

    if len(results2) != 0 and results2[0].meta.score > score:
        results = results2

    issues_ids = []
    for result in results:
        result_dict = result.to_dict()
        issues_ids.append(result_dict['issue_id'])

    return {'issues': issues_ids}
