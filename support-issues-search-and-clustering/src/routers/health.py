#!/usr/bin/python3

from fastapi import APIRouter

router = APIRouter()


@router.get('/health', tags=['health'])
def health():
    return {'status': 'up'}
