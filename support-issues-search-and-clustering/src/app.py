#!/usr/bin/python3

import os

import uvicorn
from fastapi import FastAPI

from routers import health, issues_search, issues_clustering

app = FastAPI()

app.include_router(health.router)
app.include_router(issues_search.router)
app.include_router(issues_clustering.router)

if __name__ == '__main__':
    port = int(os.getenv('APP_PORT', 5000))
    uvicorn.run(app, host='0.0.0.0', port=port)
