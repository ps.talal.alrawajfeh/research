import cv2
import numpy as np
import math
from matplotlib import pyplot as plt
from scipy import interpolate


def main():
    with open('genuine_statistical_results.csv', 'r') as f:
        lines = f.readlines()

    values = []
    for l in lines:
        val = int(l.strip().split(',')[2])
        values.append(val)

    print(f'min: {np.min(values)}')
    print(f'max: {np.max(values)}')
    print(f'mean: {np.mean(values)}')
    print(f'std: {np.std(values)}')

    unique_values, counts = np.unique(values, return_counts=True)
    mode = unique_values[np.argmax(counts)]

    print(f'mode: {mode}')
    print(f'median: {np.median(values)}')

    plt.hist(np.array(values), list(range(101)))
    plt.show()

    mapping = interpolate.interp1d([np.min(values), mode, 100], [70, mode, 100])
    plt.xticks(list(range(0, 101, 5)))
    plt.hist(mapping(np.array(values)), list(range(101)))
    plt.show()


if __name__ == "__main__":
    main()
