from tensorflow.keras.applications import efficientnet
import numpy as np


def get_input_size_and_feature_vector_length(model_code, model_constructor):
    if model_code == 'b0':
        input_size = (224, 224, 3)
    elif model_code == 'b1':
        input_size = (240, 240, 3)
    elif model_code == 'b2':
        input_size = (260, 260, 3)
    elif model_code == 'b3':
        input_size = (300, 300, 3)
    elif model_code == 'b4':
        input_size = (380, 380, 3)
    elif model_code == 'b5':
        input_size = (456, 456, 3)
    elif model_code == 'b6':
        input_size = (528, 528, 3)
    elif model_code == 'b7':
        input_size = (600, 600, 3)
    else:
        input_size = None

    model = model_constructor(include_top=False,
                              weights='imagenet',
                              input_shape=input_size,
                              pooling='avg')
    feature_vector = model.predict(np.zeros((1,) + input_size))[0]

    return input_size, len(feature_vector)


def main():
    input_size, feature_vector_length = get_input_size_and_feature_vector_length(f'b4', efficientnet.EfficientNetB4)
    print(f'input size: {input_size} --> feature vector length: {feature_vector_length}')


if __name__ == '__main__':
    main()
