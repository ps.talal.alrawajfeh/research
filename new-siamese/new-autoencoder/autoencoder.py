import math
import os
import pickle
import random
import shutil
from enum import Enum

import cv2
import numpy as np
from keras.layers import ReLU
from tensorflow.keras import Model
from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.keras.layers import Input, Conv2D, BatchNormalization, MaxPooling2D, UpSampling2D
from tensorflow.keras.losses import binary_crossentropy
from tensorflow.keras.models import load_model
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.utils import Sequence

INPUT_SIZE = (748, 200)
INPUT_SHAPE = (INPUT_SIZE[1], INPUT_SIZE[0], 1)

INPUT_IMAGES_PATH = 'C:\\Users\\USER\\Desktop\\AutoEncoder\\Data\\Input'
OUTPUT_IMAGES_PATH = 'C:\\Users\\USER\\Desktop\\AutoEncoder\\Data\\Output'
TRAINING_IMAGES_PERCENTAGE = 0.7
VALIDATION_TEST_IMAGES_PERCENTAGE = 0.15

MODEL_FILE_NAME = 'autoencoder'


def conv_batch_norm_relu_layer(input_layer, filters, kernel=(3, 3), strides=None):
    if strides is None:
        conv = Conv2D(filters,
                      kernel,
                      kernel_initializer='he_normal',
                      use_bias=False,
                      padding='same')(input_layer)
    else:
        conv = Conv2D(filters,
                      kernel,
                      strides=strides,
                      use_bias=False,
                      kernel_initializer='he_normal',
                      padding='same')(input_layer)
    conv = BatchNormalization()(conv)
    conv = ReLU()(conv)
    return conv


def stacked_conv(input_layer, filters, kernel=(3, 3)):
    conv = conv_batch_norm_relu_layer(input_layer, filters, kernel)
    return conv_batch_norm_relu_layer(conv, filters, kernel)


def build_model(input_shape=INPUT_SHAPE):
    input_layer = Input(shape=input_shape)
    batch_norm = BatchNormalization()(input_layer)

    filters = [32, 64, 128, 256]

    conv1 = stacked_conv(batch_norm, filters[0])
    pool1 = MaxPooling2D(pool_size=(2, 2))(conv1)
    conv2 = stacked_conv(pool1, filters[1])
    pool2 = MaxPooling2D(pool_size=(2, 2))(conv2)
    conv3 = stacked_conv(pool2, filters[2])
    pool3 = MaxPooling2D(pool_size=(2, 2))(conv3)

    conv4 = stacked_conv(pool3, filters[3])

    up1 = UpSampling2D((2, 2))(conv4)
    conv5 = stacked_conv(up1, filters[2])
    up2 = UpSampling2D((2, 2))(conv5)
    conv6 = stacked_conv(up2, filters[1])
    up3 = UpSampling2D((2, 2))(conv6)
    conv7 = stacked_conv(up3, filters[0])

    prediction = Conv2D(1,
                        (1, 1),
                        kernel_initializer='he_normal',
                        use_bias=True,
                        padding='same',
                        activation='sigmoid')(conv7)

    return Model(inputs=input_layer,
                 outputs=prediction)


def threshold_otsu(image):
    return cv2.threshold(image,
                         0,
                         255,
                         cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]


def pre_process(image_tensor: np.ndarray) -> np.ndarray:
    image_tensor = threshold_otsu(image_tensor)
    image_tensor = 255.0 - np.array(image_tensor, np.float32)

    return (image_tensor / 255.0).reshape(INPUT_SHAPE)


class DataGenerator(Sequence):
    def __init__(self,
                 input_image_paths,
                 output_image_paths,
                 batch_size):
        self.count = len(input_image_paths)
        self.input_image_paths = input_image_paths
        self.output_image_paths = output_image_paths
        self.batch_size = batch_size

    def __getitem__(self, index):
        input_image_paths = self.input_image_paths[index * self.batch_size: (index + 1) * self.batch_size]
        input_images = [pre_process(cv2.imread(f, cv2.IMREAD_GRAYSCALE)) for f in input_image_paths]

        output_image_paths = self.output_image_paths[index * self.batch_size: (index + 1) * self.batch_size]
        output_images = [pre_process(cv2.imread(f, cv2.IMREAD_GRAYSCALE)) for f in output_image_paths]

        return np.array(input_images), np.array(output_images)

    def __len__(self):
        return math.ceil(self.count / self.batch_size)


def load_image_paths():
    input_image_paths = os.listdir(INPUT_IMAGES_PATH)
    output_image_paths = os.listdir(OUTPUT_IMAGES_PATH)

    input_image_paths = filter(lambda x: x.endswith('.jpg'), input_image_paths)
    output_image_paths = filter(lambda x: x.endswith('.jpg'), output_image_paths)

    input_image_paths = [os.path.join(INPUT_IMAGES_PATH, f) for f in input_image_paths]
    output_image_paths = [os.path.join(OUTPUT_IMAGES_PATH, f) for f in output_image_paths]

    return input_image_paths, output_image_paths


def train_validation_test_split(input_images, output_images):
    indices = list(range(len(input_images)))
    random.shuffle(indices)

    input_images = [input_images[i] for i in indices]
    output_images = [output_images[i] for i in indices]

    train_images_count = int(len(input_images) * TRAINING_IMAGES_PERCENTAGE)
    validation_test_images_count = int(len(input_images) * VALIDATION_TEST_IMAGES_PERCENTAGE)

    return ((input_images[:train_images_count],
             output_images[:train_images_count]),
            (input_images[train_images_count:train_images_count + validation_test_images_count],
             output_images[train_images_count:train_images_count + validation_test_images_count]),
            (input_images[train_images_count + validation_test_images_count:],
             output_images[train_images_count + validation_test_images_count:]))


def cache_object(obj, file):
    with open(file, 'wb') as f:
        pickle.dump(obj, f)


def load_cached(file):
    with open(file, 'rb') as f:
        return pickle.load(f)


class ModelLoadingMode(Enum):
    MinimumMetricValue = 0
    MaximumMetricValue = 1


def rename_best_model(mode=ModelLoadingMode.MinimumMetricValue):
    all_models = list(filter(lambda x: x[-3:] == '.h5', os.listdir(os.curdir)))
    all_metrics = []

    for model in all_models:
        i1 = model.rfind('_')
        i2 = model.rfind('.')

        if i1 == -1 or i2 == -1:
            all_metrics.append(None)
            continue

        metric = model[i1 + 1: i2]

        try:
            all_metrics.append(float(metric))
        except ValueError:
            all_metrics.append(None)

    model_metric_pairs = [[all_models[i], all_metrics[i]] for i in range(len(all_models)) if all_metrics[i] is not None]
    if mode == ModelLoadingMode.MaximumMetricValue:
        best_index = np.array(model_metric_pairs)[:, 1:2].flatten().argmax()
    else:
        best_index = np.array(model_metric_pairs)[:, 1:2].flatten().argmin()

    shutil.copy(model_metric_pairs[best_index][0], MODEL_FILE_NAME + '.h5')


def train_model():
    if not os.path.isfile('autoencoder_last.h5'):
        autoencoder_model = build_model()
    else:
        autoencoder_model = load_model('autoencoder_last.h5', compile=False)

    autoencoder_model.compile(loss=binary_crossentropy, optimizer=Adam())

    if os.path.isfile('x_train.pickle') and os.path.isfile('y_train.pickle') and \
            os.path.isfile('x_val.pickle') and os.path.isfile('y_val.pickle') and \
            os.path.isfile('x_test.pickle') and os.path.isfile('y_test.pickle'):
        x_train = load_cached('x_train.pickle')
        y_train = load_cached('y_train.pickle')
        x_val = load_cached('x_val.pickle')
        y_val = load_cached('y_val.pickle')
    else:
        input_image_paths, output_image_paths = load_image_paths()
        (x_train, y_train), (x_val, y_val), (x_test, y_test) = train_validation_test_split(
            input_image_paths, output_image_paths)
        cache_object(x_train, 'x_train.pickle')
        cache_object(y_train, 'y_train.pickle')
        cache_object(x_val, 'x_val.pickle')
        cache_object(y_val, 'y_val.pickle')
        cache_object(x_test, 'x_test.pickle')
        cache_object(y_test, 'y_test.pickle')

    train_data_generator = DataGenerator(x_train, y_train, 2)
    val_data_generator = DataGenerator(x_val, y_val, 2)

    model_best_checkpoint_callback = ModelCheckpoint(
        filepath=MODEL_FILE_NAME + '_{epoch:02d}_{val_loss:0.4f}.h5',
        save_best_only=False)

    model_last_checkpoint_callback = ModelCheckpoint(
        filepath=MODEL_FILE_NAME + '_last.h5',
        save_best_only=False)

    history = autoencoder_model.fit(train_data_generator,
                                    epochs=20,
                                    validation_data=val_data_generator,
                                    callbacks=[model_last_checkpoint_callback,
                                               model_best_checkpoint_callback])

    cache_object(history, MODEL_FILE_NAME + '_history.pickle')
    rename_best_model()


def evaluate_model():
    autoencoder_model = load_model(MODEL_FILE_NAME + '.h5', compile=False)

    autoencoder_model.compile(loss=binary_crossentropy, optimizer=Adam)

    x_test = load_cached('x_test.pickle')
    y_test = load_cached('y_test.pickle')

    test_data_generator = DataGenerator(x_test, y_test, 2)

    results = autoencoder_model.evaluate(test_data_generator)

    cache_object(results, 'evaluation_results.pickle')
    print(results)


if __name__ == '__main__':
    train_model()
    evaluate_model()
