using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using Progressoft.Imaging.Common;

namespace FeatureVectorsUpdator
{
    class Program
    {
        private static Random _randomizer = new Random();
        private static readonly byte[] _emptyData = new byte[] { 240, 62, 30, 103, 184, 185, 37, 76, 130, 206, 120, 239, 1, 247, 26, 117 };

        public static string Encode(byte[] data)
        {
            return Convert.ToBase64String(data);

        }

        public static byte[] Decode(string data)
        {
            return Convert.FromBase64String(data);
        }
        public static string Decrypt(string data)
        {
            return ASCIIEncoding.UTF8.GetString(Decrypt(Decode(data)));
        }

        public static byte[] Decrypt(byte[] data)
        {
            int keyIndex = BitConverter.ToInt32(data, 0);
            int keyLength = BitConverter.ToInt32(data, 4);
            byte[] cipherWithoutKeyIndexAndKey = new byte[data.Length - 8];
            Array.Copy(data, 8, cipherWithoutKeyIndexAndKey, 0, cipherWithoutKeyIndexAndKey.Length);

            Split(cipherWithoutKeyIndexAndKey, keyIndex, out byte[] firstEnc, out byte[] secondEncAndKey);
            Split(secondEncAndKey, keyLength, out byte[] key, out byte[] secondEnc);
            byte[] decData = XOr(firstEnc.Concat(secondEnc).ToArray(), key);
            return _emptyData.SequenceEqual(decData) ? new byte[0] : decData;
        }

        public static string EncryptThenWrap(byte[] data)
        {
            return Encode(Encrypt(data));
        }

        public static byte[] DeWrapThenDecrypt(string data)
        {
            return Decrypt(Decode(data));
        }

        public static string Encrypt(string data)
        {
            return Encode(Encrypt(ASCIIEncoding.UTF8.GetBytes(data)));
        }

        public static byte[] Encrypt(byte[] data)
        {
            byte[] data2Enc = data.Length == 0 ? _emptyData : data;

            byte[] randKey = GenerateRandomKey();
            byte[] destination = XOr(data2Enc, randKey);
            int keyIndex = _randomizer.Next(0, destination.Length);
            Split(destination, keyIndex, out byte[] firstDest, out byte[] secondDest);
            // key-index, key-length, first-part, injected-key, second-part
            return BitConverter.GetBytes(keyIndex).Concat(BitConverter.GetBytes(randKey.Length)).Concat(firstDest).Concat(randKey).Concat(secondDest).ToArray();
        }

        private static byte[] XOr(byte[] source, byte[] key)
        {
            int sourceLength = source.Length;
            int keyLength = key.Length;
            byte[] destination = new byte[sourceLength];
            int j = 0;
            for (int i = 0; i < sourceLength; i++)
            {
                destination[i] = (byte)(source[i] ^ key[j++]);
                if (j >= keyLength)
                    j = 0;
            }

            return destination;
        }

        private static byte[] GenerateRandomKey()
        {
            byte[] randomKey = new byte[_randomizer.Next(25, 125)];
            _randomizer.NextBytes(randomKey);
            return randomKey;
        }

        private static void Split(byte[] array, int atIndexExcluded, out byte[] first, out byte[] second)
        {
            first = array.Take(atIndexExcluded).ToArray();
            second = array.Skip(atIndexExcluded).ToArray();
        }

        public static byte[] Compress(byte[] data)
        {
            return Ionic.Zlib.GZipStream.CompressBuffer(data);
        }

        public static byte[] Uncompress(byte[] data)
        {
            return Ionic.Zlib.GZipStream.UncompressBuffer(data); ;
        }

        public static byte[] ConvertFloatArrayToByteArray(float[] array)
        {
            var byteArray = new byte[array.Length * 4];
            Buffer.BlockCopy(array, 0, byteArray, 0, byteArray.Length);
            return byteArray;
        }

        public static float[] ConvertByteArrayToFloatArray(byte[] array)
        {
            var floatArray2 = new float[array.Length / 4];
            Buffer.BlockCopy(array, 0, floatArray2, 0, array.Length);
            return floatArray2;
        }

        static void Main(string[] args)
        {
            try
            {
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                builder.DataSource = "DESKTOP-NIOI2R4\\SQLEXPRESS";
                builder.UserID = "sa";
                builder.Password = "sa";
                builder.InitialCatalog = "ARBK-ASV-TEST";

                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    Console.WriteLine("\nQuery data example:");
                    Console.WriteLine("=========================================\n");
                    connection.Open();

                    var featureVectorMap = new Dictionary<byte[], byte[]>();

                    String sql = "SELECT * FROM dbo.ASV_COSIGNEE;";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                byte[] buffer = new byte[12];
                                reader.GetBytes(0, 0, buffer, 0, 12);
                                Console.WriteLine("Creating new feature vector for: " + BitConverter.ToString(buffer).Replace("-", ""));
                                var sqlBytes = reader.GetSqlBytes(3);
                                var image = sqlBytes.Buffer.ToArray();
                                var bitmap = BitmapUtils.ConvertByteBufferToBitmap(Decrypt(image)); //.Save("E:\\test\\" + BitConverter.ToUInt32(buffer, 0).ToString() + ".bmp");
                                var featureVector = FeatureVector.PreProcess(bitmap);
                                featureVectorMap.Add(buffer, Compress(ConvertFloatArrayToByteArray(featureVector)));
                            }
                        }
                    }

                    foreach (var kvp in featureVectorMap)

                    {
                        sql = "UPDATE dbo.ASV_COSIGNEE set CSGN_SIG_FEATURES=@FeatureVector WHERE CSGN_PK=@PrimaryKey;";
                        using (SqlCommand command = new SqlCommand(sql, connection))
                        {

                            Console.WriteLine("Updating feature vector for: " + BitConverter.ToString(kvp.Key).Replace("-", ""));
                            command.Parameters.AddWithValue("@PrimaryKey", kvp.Key);
                            command.Parameters.AddWithValue("@FeatureVector", kvp.Value);
                            command.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }

            Console.ReadLine();
        }
    }
}
