import math
import os
import random

import cv2
import matplotlib.pyplot as plt
import numpy as np


def threshold(image):
    return cv2.threshold(
        image,
        0,
        255,
        cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]


def read_image(image_path):
    return cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)


def remove_white_border(binary_image):
    mask = (255 - binary_image) > 0

    height, width = binary_image.shape[0:2]
    mask1, mask2 = mask.any(0), mask.any(1)
    x1, x2 = mask1.argmax(), width - mask1[::-1].argmax()
    y1, y2 = mask2.argmax(), height - mask2[::-1].argmax()

    return binary_image[y1:y2, x1:x2]


def pre_process_image(image):
    return remove_white_border(threshold(image))


def euclidean_distance(point1, point2):
    return math.sqrt(math.pow(point1[0] - point2[0], 2.0)
                     + math.pow(point1[1] - point2[1], 2.0))


def get_rectangle_coordinates(origin, radius1, radius2, theta, start_angle):
    relative_points = np.array([
        [math.cos(start_angle) * radius1, -math.sin(start_angle) * radius1],
        [math.cos(start_angle) * radius2, -math.sin(start_angle) * radius2],
        [math.cos(start_angle + theta) * radius1, -math.sin(start_angle + theta) * radius1],
        [math.cos(start_angle + theta) * radius2, -math.sin(start_angle + theta) * radius2],
    ])

    points = np.array(origin) + relative_points

    x_min = np.min(points[:, :1])
    x_max = np.max(points[:, :1])
    y_min = np.min(points[:, 1:])
    y_max = np.max(points[:, 1:])

    return int(math.floor(x_min)), int(math.floor(y_min)), int(math.ceil(x_max)), int(math.ceil(y_max))


def calculate_angle(origin, x, y):
    if x == origin[0]:
        if y < origin[1]:
            alpha = math.pi / 2
        elif y > origin[1]:
            alpha = 3 * math.pi / 2
        else:
            alpha = 0
    else:
        alpha = math.atan(abs((y - origin[1]) / (x - origin[0])))
        if x < origin[0] and y <= origin[1]:
            alpha = math.pi - alpha
        elif x < origin[0] and y >= origin[1]:
            alpha = math.pi + alpha
        elif x > origin[0] and y >= origin[1]:
            alpha = 2 * math.pi - alpha
    return alpha


def get_sector_points(origin, radius1, radius2, theta, start_angle):
    start_angle = start_angle % (2 * math.pi)

    x1, y1, x2, y2 = get_rectangle_coordinates(origin, radius1, radius2, theta, start_angle)
    sector_points = []

    for x in range(x1, x2 + 1):
        for y in range(y1, y2 + 1):
            r = euclidean_distance((x, y), origin)
            alpha = calculate_angle(origin, x, y)

            if radius1 <= r <= radius2 and start_angle <= alpha <= start_angle + theta:
                sector_points.append([x, y, r, alpha])

    if len(sector_points) == 0:
        sector_points = [[int(math.floor(origin[0] + math.cos(start_angle) * radius1)),
                          int(math.floor(origin[1] + -math.sin(start_angle) * radius1)),
                          radius1,
                          start_angle]]

    return sector_points


def find_radius(image_size, origin):
    return max([
        euclidean_distance((0, 0), origin),
        euclidean_distance((image_size[0], 0), origin),
        euclidean_distance((0, image_size[1]), origin),
        euclidean_distance(image_size, origin)
    ])


def pre_process(image):
    pre_processed = 255 - pre_process_image(image)
    return pre_processed // 255


def foreground_points(image):
    points = []
    for y in range(image.shape[0]):
        for x in range(image.shape[1]):
            if image[y, x] == 1:
                points.append((x, y))
    return points


def center_of_mass(points):
    return [np.mean(points[:, :1]), np.mean(points[:, 1:])]


def circular_descriptor(image, angle_sectors, radius_sectors):
    image = pre_process(image)
    origin = center_of_mass(np.array(foreground_points(image)))

    theta = 2 * math.pi / angle_sectors
    radius = find_radius((image.shape[1], image.shape[0]), origin)
    delta_radius = radius / radius_sectors

    descriptor = np.zeros((angle_sectors, radius_sectors, 3), np.float)

    current_angle = 0
    for angle_sector in range(angle_sectors):
        r1 = 0
        for radius_sector in range(radius_sectors):
            r2 = r1 + delta_radius

            points = get_sector_points(origin, r1, r2, theta, current_angle)

            ratio = 0
            mean_r = 0
            mean_a = 0
            n = 0
            for (x, y, r, a) in points:
                if 0 <= y < image.shape[0] and 0 <= x < image.shape[1] and image[y, x] == 1:
                    ratio += 1
                    mean_r += (r - r1)
                    mean_a += (a - current_angle)
                n += 1

            ratio /= len(points)
            mean_r /= (len(points) * delta_radius)
            mean_a /= (len(points) * theta)

            descriptor[angle_sector, radius_sector, 0] = ratio
            descriptor[angle_sector, radius_sector, 1] = mean_r
            descriptor[angle_sector, radius_sector, 2] = mean_a

            r1 += delta_radius
        current_angle += theta

    return descriptor


def calculate_sectors(image):
    image = pre_process(image)
    origin = center_of_mass(np.array(foreground_points(image)))
    radius = find_radius((image.shape[1], image.shape[0]), origin)
    # radius = image.shape[1] / 2
    return radius / 10, 2 * math.pi * radius


def compare_descriptors_euclidean_distance(descriptor1, descriptor2):
    correlations = []

    for i in range(descriptor1.shape[0]):
        correlations.append(
            1.0 - np.sqrt(np.sum(np.square(descriptor1 - descriptor2))) / np.sqrt(np.prod(descriptor1.shape)))

        descriptor1 = np.concatenate([descriptor1[1:], descriptor1[:1]], axis=0)

    return max(correlations)


def compare_descriptors_dot_product1(descriptor1, descriptor2):
    correlations = []

    magnitude1 = np.sqrt(np.sum(np.square(descriptor1)))
    magnitude2 = np.sqrt(np.sum(np.square(descriptor2)))

    if magnitude1 == 0.0 and magnitude2 > 0.0 or magnitude2 == 0 and magnitude1 > 0.0:
        return 0.0

    if magnitude1 == 0.0 and magnitude2 == 0.0:
        return 1.0

    for i in range(descriptor1.shape[0]):
        correlations.append((np.sum(descriptor1 * descriptor2) + 1e-7) /
                            (magnitude1 * magnitude2 + 1e-7))

        descriptor1 = np.concatenate([descriptor1[1:], descriptor1[:1]], axis=0)

    return max(correlations)


def compare_descriptors_cross_correlation1(descriptor1, descriptor2):
    correlations = []
    mean1 = np.mean(descriptor1)
    mean2 = np.mean(descriptor2)
    centered1 = descriptor1 - mean1
    centered2 = descriptor2 - mean2
    std1 = np.std(descriptor1)
    std2 = np.std(descriptor2)
    denominator = std1 * std2 * np.prod(descriptor1.shape)

    if std1 == 0.0 and std2 == 0.0:
        if abs(mean1 - mean2) < 1e-5:
            return 1.0
        else:
            return 0.0

    if std1 == 0.0 and std2 > 0.0:
        return 0.0

    if std2 == 0.0 and std1 > 0.0:
        return 0.0

    for i in range(descriptor1.shape[0]):
        correlations.append(np.sum(centered1 * centered2) / denominator)
        centered1 = np.concatenate([centered1[1:], centered1[:1]], axis=0)

    return max(correlations)


def compare_descriptors_cross_correlation2(descriptor1, descriptor2):
    correlations = []
    for i in range(descriptor1.shape[0]):
        correlations.append(compare_descriptors_cross_correlation2_sub(descriptor1, descriptor2))
        descriptor1 = np.concatenate([descriptor1[1:], descriptor1[:1]], axis=0)
    return max(correlations)


def compare_descriptors_cross_correlation2_sub(descriptor1, descriptor2):
    mean1 = np.mean(descriptor1, axis=1)
    mean2 = np.mean(descriptor2, axis=1)
    centered1 = descriptor1 - np.expand_dims(mean1, axis=1)
    centered2 = descriptor2 - np.expand_dims(mean2, axis=1)
    std1 = np.std(descriptor1, axis=1)
    std2 = np.std(descriptor2, axis=1)

    mask1 = np.logical_or(np.logical_and(std1 > 0.0, std2 == 0.0),
                          np.logical_and(std1 == 0.0, std2 > 0.0))
    mask1 = 1.0 - np.float32(mask1)

    mask2 = np.logical_and(np.logical_and(std1 == 0.0, std2 == 0.0),
                           mean1 != mean2)
    mask2 = 1.0 - np.float32(mask2)

    mask3 = np.logical_and(np.logical_and(std1 == 0.0, std2 == 0.0),
                           mean1 == mean2)
    mask3 = np.float32(mask3)

    correlation = (np.sum(centered1 * centered2, axis=1) + 1e-7) / (std1 * std2 * centered1.shape[1] + 1e-7)
    correlation *= mask1
    correlation *= mask2
    correlation = correlation * (1.0 - mask3) + mask3
    correlation = np.mean(correlation, axis=0)

    return np.mean(correlation)


#
# def main1():
#     base_dir = '/home/u764/Downloads/signatures/'
#
#     correlations = []
#     files = os.listdir(base_dir)
#     # choice = random.choice(files)
#     # print(choice)
#
#     file_classes = extract_classes_from_file_names([base_dir])
#     # original = read_image(base_dir + file_classes.pop(random.choice()))
#     original = read_image(random.choice(file_classes.pop(random.choice(list(file_classes.keys())))))
#
#     for c in random.sample(file_classes.keys(), 100):
#         # f = random.choice(file_classes[c])
#         # original = read_image(f)
#         # image = rotate(original, 12)
#         # image = read_image(f)
#         original = read_image(file_classes[c][0])
#         for i in range(1, len(file_classes[c])):
#             image = read_image(file_classes[c][i])
#
#             radius_sectors1, angle_sectors1 = calculate_sectors(original)
#             radius_sectors2, angle_sectors2 = calculate_sectors(image)
#
#             radius_sectors = int(math.ceil((radius_sectors1 + radius_sectors2) / 2))
#             angle_sectors = int(math.ceil((angle_sectors1 + angle_sectors2) / 2))
#
#             descriptor1 = circular_descriptor(image, angle_sectors, radius_sectors)
#             descriptor2 = circular_descriptor(original, angle_sectors, radius_sectors)
#
#             correlation = compare_descriptors_cross_correlation1(descriptor1, descriptor2)
#
#             # if correlation >= 0.95:
#             #     print(file_classes[c][0], file_classes[c][i])
#             #     fig, (axis1, axis2) = plt.subplots(2, 1)
#             #     axis1.imshow(original, plt.cm.gray)
#             #     axis2.imshow(image, plt.cm.gray)
#             #     plt.show()
#
#             correlations.append(correlation)
#             print(angle_sectors, radius_sectors, correlations[-1])
#
#     print('--------------------------')
#     print(f'number of items: {len(correlations)}')
#     print(f'means: {np.mean(correlations)}')
#     print(f'std: {np.std(correlations)}')
#     print(f'max: {max(correlations)}')
#     print(f'min: {min(correlations)}')
#     correlations = np.array(correlations)
#     print(f'> 0.80: {len(correlations[correlations > 0.80])}')
#     print(f'> 0.85: {len(correlations[correlations > 0.85])}')
#     print(f'> 0.90: {len(correlations[correlations > 0.90])}')
#     print(f'> 0.95: {len(correlations[correlations > 0.95])}')


def main2():
    image1 = 255 - read_image(
        '/home/u764/Development/progressoft/docreader/src/resources/countries/qat/non_smart_occupation_label.bmp')
    # image2 = read_image('/home/u764/Downloads/nationality.bmp')
    image2 = read_image('/home/u764/Downloads/occupation.bmp')
    image2 = 255 - cv2.threshold(image2, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]

    sift = cv2.SIFT_create()
    keypoints1, descriptors1 = sift.detectAndCompute(image1, None)
    keypoints2, descriptors2 = sift.detectAndCompute(image2, None)
    bf = cv2.BFMatcher(cv2.NORM_L1, crossCheck=True)

    matches = bf.match(descriptors1, descriptors2)
    matches = sorted(matches, key=lambda x: x.distance)

    print(descriptors1.shape)
    print(descriptors2.shape)
    exit()
    # image1 = cv2.resize(image1, (image1.shape[1], image1.shape[1]), interpolation=cv2.INTER_NEAREST)
    # image2 = cv2.resize(image2, (image2.shape[1], image2.shape[1]), interpolation=cv2.INTER_NEAREST)

    # cv2.imwrite('/home/u764/Downloads/0a43deb20_1_rotated.bmp', image2)
    # plt.imshow(image2)
    # plt.show()
    radius_sectors1, angle_sectors1 = calculate_sectors(image1)
    radius_sectors2, angle_sectors2 = calculate_sectors(image2)

    radius_sectors = int(math.ceil((radius_sectors1 + radius_sectors2) / 2))
    angle_sectors = int(math.ceil((angle_sectors1 + angle_sectors2) / 2))

    desc1 = circular_descriptor(image1, angle_sectors, radius_sectors)
    desc2 = circular_descriptor(image2, angle_sectors, radius_sectors)

    print(compare_descriptors_cross_correlation1(desc1, desc2))


if __name__ == '__main__':
    main2()
