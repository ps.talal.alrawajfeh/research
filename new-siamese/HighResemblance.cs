namespace ASVModelPreProcessing
{
    public class HighResemblance
    {
        private const double ComparisonEpsilon = 1e-7;

        public static double EuclideanDistance(
            double[] point1,
            double[] point2)
        {
            return Math.Sqrt(Math.Pow(point1[0] - point2[0], 2.0)
                             + Math.Pow(point1[1] - point2[1], 2.0));
        }

        public static (double, double, double, double) RectangleCoordinates(
            double[] origin,
            double radius1,
            double radius2,
            double theta,
            double startAngle)
        {
            var point1 = (
                origin[0] + Math.Cos(startAngle) * radius1,
                origin[1] - Math.Sin(startAngle) * radius1);
            var point2 = (
                origin[0] + Math.Cos(startAngle) * radius2,
                origin[1] - Math.Sin(startAngle) * radius2);
            var point3 = (
                origin[0] + Math.Cos(startAngle + theta) * radius1,
                origin[1] - Math.Sin(startAngle + theta) * radius1);
            var point4 = (
                origin[0] + Math.Cos(startAngle + theta) * radius2,
                origin[1] + -Math.Sin(startAngle + theta) * radius2);

            var xs = new[] {point1.Item1, point2.Item1, point3.Item1, point4.Item1};
            var ys = new[] {point1.Item2, point2.Item2, point3.Item2, point4.Item2};

            return (xs.Min(), ys.Min(), xs.Max(), ys.Max());
        }

        public static double CalculateAngle(
            double[] origin,
            double[] point)
        {
            var x = point[0];
            var y = point[1];

            double alpha;

            if (Math.Abs(x - origin[0]) < ComparisonEpsilon)
            {
                if (y < origin[1])
                {
                    alpha = Math.PI / 2;
                }

                else if (y > origin[1])
                {
                    alpha = 3 * Math.PI / 2;
                }
                else
                {
                    alpha = 0;
                }
            }
            else
            {
                alpha = Math.Atan(Math.Abs((y - origin[1]) / (x - origin[0])));

                if (x < origin[0] && y < origin[1])
                {
                    alpha = Math.PI - alpha;
                }
                else if (x < origin[0] && y > origin[1])
                {
                    alpha = Math.PI + alpha;
                }
                else if (x > origin[0] && y > origin[1])
                {
                    alpha = 2 * Math.PI - alpha;
                }
            }

            return alpha;
        }


        public static List<(int[], double[])> GetSectorPoints(
            double[] origin,
            double radius1,
            double radius2,
            double theta,
            double startAngle)
        {
            double angle = startAngle % (2 * Math.PI);

            var (x1, y1, x2, y2) = RectangleCoordinates(origin, radius1, radius2, theta, angle);

            var startX = (int) Math.Floor(x1);
            var endX = (int) Math.Ceiling(x2) + 1;
            var startY = (int) Math.Floor(y1);
            var endY = (int) Math.Ceiling(y2) + 1;

            List<(int[], double[])> sectorPoints = new List<(int[], double[])>();

            for (var x = startX; x < endX; x++)
            {
                for (var y = startY; y < endY; y++)
                {
                    var radius = EuclideanDistance(origin, new double[] {x, y});
                    var alpha = CalculateAngle(origin, new double[] {x, y});

                    if (radius1 <= radius &&
                        radius <= radius2 &&
                        angle <= alpha &&
                        alpha <= angle + theta)
                    {
                        sectorPoints.Add((
                            new[] {x, y},
                            new[] {radius, alpha}));
                    }
                }
            }

            if (sectorPoints.Count == 0)
            {
                var x = (int) Math.Floor(origin[0] + Math.Cos(angle) * radius1);
                var y = (int) Math.Floor(origin[1] - Math.Sin(angle) * radius1);
                sectorPoints.Add((new[] {x, y}, new[] {radius1, angle}));
            }

            return sectorPoints;
        }

        public static double FindRadius(
            double[] origin,
            double[] imageSize)
        {
            return new[]
            {
                EuclideanDistance(new[] {0.0, 0.0}, origin),
                EuclideanDistance(new[] {imageSize[0], 0.0}, origin),
                EuclideanDistance(new[] {0.0, imageSize[1]}, origin),
                EuclideanDistance(new[] {imageSize[0], imageSize[1]}, origin)
            }.Max();
        }

        public static double[] CenterOfMass(
            float[] image,
            int height,
            int width)
        {
            double meanX = 0.0;
            double meanY = 0.0;
            double count = 0.0;

            for (var y = 0; y < height; y++)
            {
                for (var x = 0; x < width; x++)
                {
                    var pixelValue = image[y * width + x];
                    if (pixelValue > 0.5)
                    {
                        meanX += x;
                        meanY += y;
                        count += 1.0;
                    }
                }
            }

            meanX /= count;
            meanY /= count;

            return new[] {meanX, meanY};
        }

        public static double[][][] PolarMeshDescriptor(
            float[] image,
            int height,
            int width,
            double[] origin,
            double radius,
            int angleSectors,
            int radiusSectors)
        {
            var theta = 2 * Math.PI / angleSectors;
            var deltaRadius = radius / radiusSectors;

            double[][][] descriptor = new double[angleSectors][][];
            for (var i = 0; i < angleSectors; i++)
            {
                descriptor[i] = new double[radiusSectors][];
                for (var j = 0; j < radiusSectors; j++)
                {
                    descriptor[i][j] = new double[3];
                }
            }

            var currentAngle = 0.0;

            for (var angleSector = 0; angleSector < angleSectors; angleSector++)
            {
                double r1 = 0.0;

                for (var radiusSector = 0; radiusSector < radiusSectors; radiusSector++)
                {
                    double r2 = r1 + deltaRadius;

                    var points = GetSectorPoints(origin, r1, r2, theta, currentAngle);

                    var ratio = 0.0;
                    var meanR = 0.0;
                    var meanA = 0.0;

                    foreach (var point in points)
                    {
                        var x = point.Item1[0];
                        var y = point.Item1[1];

                        var r = point.Item2[0];
                        var a = point.Item2[1];

                        if (0 <= y &&
                            y < height &&
                            0 <= x &&
                            x < width &&
                            image[y * width + x] > 0.5)
                        {
                            ratio += 1.0;
                            meanR += r - r1;
                            meanA += a - currentAngle;
                        }
                    }

                    ratio /= points.Count;
                    meanR /= points.Count * deltaRadius;
                    meanA /= points.Count * theta;

                    descriptor[angleSector][radiusSector][0] = ratio;
                    descriptor[angleSector][radiusSector][1] = meanR;
                    descriptor[angleSector][radiusSector][2] = meanA;

                    r1 += deltaRadius;
                }

                currentAngle += theta;
            }

            return descriptor;
        }

        public static (double, double) CalculateSectors(double radius)
        {
            return (2 * Math.PI * radius / 20.0, radius / 10.0);
        }

        public static double CompareDescriptors(
            double[][][] descriptor1,
            double[][][] descriptor2)
        {
            List<double> correlations = new List<double>();

            var mean1 = 0.0;
            var mean2 = 0.0;

            var angleSectors = descriptor1.Length;
            var radiusSectors = descriptor1[0].Length;

            for (var i = 0; i < angleSectors; i++)
            {
                for (var j = 0; j < radiusSectors; j++)
                {
                    mean1 += descriptor1[i][j][0];
                    mean1 += descriptor1[i][j][1];
                    mean1 += descriptor1[i][j][2];

                    mean2 += descriptor2[i][j][0];
                    mean2 += descriptor2[i][j][1];
                    mean2 += descriptor2[i][j][2];
                }
            }

            mean1 /= angleSectors * radiusSectors * 3;
            mean2 /= angleSectors * radiusSectors * 3;

            var dotProduct11 = 0.0;
            var dotProduct22 = 0.0;

            for (var i = 0; i < angleSectors; i++)
            {
                for (var j = 0; j < radiusSectors; j++)
                {
                    for (var k = 0; k < 3; k++)
                    {
                        var zeroCentered1 = descriptor1[i][j][k] - mean1;
                        var zeroCentered2 = descriptor2[i][j][k] - mean2;

                        dotProduct11 += zeroCentered1 * zeroCentered1;
                        dotProduct22 += zeroCentered2 * zeroCentered2;
                    }
                }
            }

            if (dotProduct11 < ComparisonEpsilon && dotProduct22 < ComparisonEpsilon)
            {
                if (Math.Abs(mean1 - mean1) < ComparisonEpsilon)
                {
                    return 1.0;
                }

                return 0.0;
            }

            if (dotProduct11 < ComparisonEpsilon && dotProduct22 > 0.0)
            {
                return 0.0;
            }

            if (dotProduct22 < ComparisonEpsilon && dotProduct11 > 0.0)
            {
                return 0.0;
            }


            for (int n = 0; n < angleSectors; n++)
            {
                var dotProduct12 = 0.0;

                for (var i = 0; i < angleSectors; i++)
                {
                    for (var j = 0; j < radiusSectors; j++)
                    {
                        for (var k = 0; k < 3; k++)
                        {
                            var zeroCentered1 = descriptor1[i][j][k] - mean1;
                            var zeroCentered2 = descriptor2[i][j][k] - mean2;

                            dotProduct12 += zeroCentered1 * zeroCentered2;
                        }
                    }
                }

                var correlation = dotProduct12 / Math.Sqrt(dotProduct11 * dotProduct22);
                correlations.Add(correlation);

                for (var i = 0; i < angleSectors - 1; i++)
                {
                    var temp = descriptor1[i];
                    descriptor1[i] = descriptor1[i + 1];
                    descriptor1[i + 1] = temp;
                }
            }

            return correlations.Max();
        }

        public static double ImageSimilarity(
            float[] image1,
            int height1,
            int width1,
            float[] image2,
            int height2,
            int width2)
        {
            var origin1 = CenterOfMass(image1, height1, width1);
            var radius1 = FindRadius(origin1, new double[] {width1, height1});

            var origin2 = CenterOfMass(image2, height2, width2);
            var radius2 = FindRadius(origin2, new double[] {width2, height2});

            var (angleSectors1, radiusSectors1) = CalculateSectors(radius1);
            var (angleSectors2, radiusSectors2) = CalculateSectors(radius2);

            var angleSectors = (int) Math.Ceiling((angleSectors1 + angleSectors2) / 2);
            var radiusSectors = (int) Math.Ceiling((radiusSectors1 + radiusSectors2) / 2);

            var descriptor1 = PolarMeshDescriptor(
                image1,
                height1,
                width1,
                origin1,
                radius1,
                angleSectors,
                radiusSectors);

            var descriptor2 = PolarMeshDescriptor(
                image2,
                height2,
                width2,
                origin2,
                radius2,
                angleSectors,
                radiusSectors);

            return CompareDescriptors(descriptor1, descriptor2);
        }

        static (float[], int, int) PreProcess(Bitmap bitmap)
        {
            int height = bitmap.Height;
            int width = bitmap.Width;

            var (cropped, newHeight, newWidth) = Program.RemoveWhiteBorder(
                Program.OtsuThreshold(Program.BitmapToMatrix(bitmap), height, width),
                height, width);

            float[] result = new float[cropped.Length];

            for (int i = 0; i < result.Length; i++)
            {
                if (cropped[i] == 255)
                {
                    result[i] = 0.0F;
                }
                else
                {
                    result[i] = 1.0F;
                }
            }

            return (result, newHeight, newWidth);
        }
    }
}