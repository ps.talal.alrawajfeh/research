import time

from openvino.inference_engine import IECore
import cv2
import numpy as np

INPUT_SIZE = (144, 96)
INPUT_SHAPE = (96, 144, 2)


def threshold(image):
    return cv2.threshold(
        image,
        0,
        255,
        cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]


def remove_white_border(binary_image):
    mask = (255 - binary_image) > 0

    height, width = binary_image.shape[0:2]
    mask1, mask2 = mask.any(0), mask.any(1)
    x1, x2 = mask1.argmax(), width - mask1[::-1].argmax()
    y1, y2 = mask2.argmax(), height - mask2[::-1].argmax()

    return binary_image[y1:y2, x1:x2]


def pre_process_image(image):
    return remove_white_border(threshold(image))


def pre_process_for_input(image):
    if image.shape[0] == INPUT_SIZE[1] and image.shape[1] == INPUT_SIZE[0]:
        resized = image
    else:
        resized = cv2.resize(image, INPUT_SIZE, interpolation=cv2.INTER_NEAREST)

    inverted = 255 - resized
    normalized = inverted / 255.0

    return normalized.reshape((INPUT_SIZE[1], INPUT_SIZE[0], 1))


def main():
    image1 = cv2.imread('/home/u764/Downloads/signatures/______001_01.jpg', cv2.IMREAD_GRAYSCALE)
    image2 = cv2.imread('/home/u764/Downloads/signatures/______001_02.jpg', cv2.IMREAD_GRAYSCALE)

    image1 = pre_process_for_input(pre_process_image(image1))
    image2 = pre_process_for_input(pre_process_image(image2))

    concatenated_input = np.concatenate([image1,
                                         image2], axis=-1).reshape(1, 2, 96, 144)

    ie = IECore()
    net = ie.read_network(
        model='/home/u764/Development/progressoft/research/new-siamese/models/siamese_files/full_data/siamese.xml')

    input_blob = next(iter(net.input_info))
    out_blob = next(iter(net.outputs))

    exec_net = ie.load_network(network=net, device_name='CPU')

    start_time = time.time()
    for i in range(10000):
        res = exec_net.infer(inputs={input_blob: concatenated_input})
        res = res[out_blob]
    end_time = time.time()
    print(f'average inference takes: {(end_time - start_time) / 10000}')


if __name__ == '__main__':
    main()
