import math
import pickle
import numpy as np
import cv2
import tqdm
from tensorflow.keras.models import load_model

## latest ##

INPUT_SIZE = (144, 96)
INPUT_SHAPE = (96, 144, 1)

PRE_TRAINED_MODEL = load_model('../embedding_net.h5', compile=False)

TRAIN_IMAGE_CLASSES_FILE = 'train_image_classes.pickle'
VALIDATION_IMAGE_CLASSES_FILE = 'validation_image_classes.pickle'
TEST_IMAGE_CLASSES_FILE = 'test_image_classes.pickle'
SPECIAL_FORGERY_IMAGE_CLASSES_FILE = 'special_forgery_image_classes.pickle'

ORIGINAL_TRAIN_IMAGE_CLASSES_FILE = '../train_image_classes.pickle'
ORIGINAL_VALIDATION_IMAGE_CLASSES_FILE = '../validation_image_classes.pickle'
ORIGINAL_TEST_IMAGE_CLASSES_FILE = '../test_image_classes.pickle'
ORIGINAL_SPECIAL_FORGERY_IMAGE_CLASSES_FILE = '../special_forgery_image_classes.pickle'

BATCH_SIZE = 1024


def cache_object(obj, file):
    with open(file, 'wb') as f:
        pickle.dump(obj, f)


def load_cached(file):
    with open(file, 'rb') as f:
        return pickle.load(f)


def images_to_feature_vectors(image_batch,
                              model=PRE_TRAINED_MODEL):
    return model.predict(np.array(image_batch, np.float32))


class ImageBatchVectorizer:
    def __init__(self, batch_size=BATCH_SIZE):
        self.images = []
        self.image_class = []
        self.batch_size = batch_size
        self.image_feature_vectors = dict()

    def __predict(self):
        while len(self.images) >= self.batch_size:
            feature_vectors = images_to_feature_vectors(self.images[:self.batch_size])
            self.images = self.images[self.batch_size:]
            image_classes = self.image_class[:self.batch_size]
            self.image_class = self.image_class[self.batch_size:]

            for i in range(len(image_classes)):
                f = image_classes[i]
                feature_vector = feature_vectors[i]
                if f not in self.image_feature_vectors:
                    self.image_feature_vectors[f] = []
                self.image_feature_vectors[f].append(feature_vector)

    def feed(self, single_image, image_class):
        self.images.append(single_image)
        self.image_class.append(image_class)
        self.__predict()

    def feed_all(self, images, image_classes):
        self.images.extend(images)
        self.image_class.extend(image_classes)
        self.__predict()

    def finalize(self):
        feature_vectors = images_to_feature_vectors(self.images)

        for i in range(len(self.image_class)):
            f = self.image_class[i]
            feature_vector = feature_vectors[i]
            if f not in self.image_feature_vectors:
                self.image_feature_vectors[f] = []
            self.image_feature_vectors[f].append(feature_vector)

        self.images = []
        self.image_class = []

    def get_image_feature_vectors(self):
        return self.image_feature_vectors


def generate_original_image_classes_embeddings(image_classes):
    keys = [*image_classes]

    image_batch_vectorizer = ImageBatchVectorizer()

    progress_bar = tqdm.tqdm(total=len(keys))
    for i in range(len(keys)):
        image_class = image_classes[keys[i]]
        image_batch_vectorizer.feed_all(image_class, [keys[i]] * len(image_class))
        progress_bar.update()

    image_batch_vectorizer.finalize()
    progress_bar.close()

    return image_batch_vectorizer.get_image_feature_vectors()


def main():
    cache_object(generate_original_image_classes_embeddings(load_cached(ORIGINAL_TRAIN_IMAGE_CLASSES_FILE)),
                 TRAIN_IMAGE_CLASSES_FILE)

    cache_object(generate_original_image_classes_embeddings(load_cached(ORIGINAL_VALIDATION_IMAGE_CLASSES_FILE)),
                 VALIDATION_IMAGE_CLASSES_FILE)

    cache_object(generate_original_image_classes_embeddings(load_cached(ORIGINAL_TEST_IMAGE_CLASSES_FILE)),
                 TEST_IMAGE_CLASSES_FILE)

    cache_object(generate_original_image_classes_embeddings(load_cached(ORIGINAL_SPECIAL_FORGERY_IMAGE_CLASSES_FILE)),
                 SPECIAL_FORGERY_IMAGE_CLASSES_FILE)


if __name__ == '__main__':
    main()
