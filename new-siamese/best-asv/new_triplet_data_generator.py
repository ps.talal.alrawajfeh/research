import math
import mimetypes
import os
import pickle
import random

from keras.engine.saving import load_model
import numpy as np
import cv2
from tqdm import tqdm

CLASS_LENGTH = 9
IMAGES_PER_CLASS_THRESHOLD = 2
IMAGE_PATHS = ['C:\\Users\\USER\\Development\\Data\\ASV\\Raw\\OriginalSignatures',
               'C:\\Users\\USER\\Development\\Data\\ASV\\Raw\\CABHBTF']

GENERATED_FEATURE_VECTORS_PATH = './data/feature_vectors'
SPECIAL_GENERATED_FEATURE_VECTORS_PATH = './data/special_feature_vectors'

TRAIN_CLASSES_PERCENTAGE = 0.7
VALIDATION_CLASSES_PERCENTAGE = 0.15
TEST_CLASSES_PERCENTAGE = 0.15

AUGMENT_DATA = True
GENERATE_SPECIAL_FORGERIES = True
VARIATIONS_PER_IMAGE = 4

SPECIAL_FORGERY_VARIATIONS = 1
SPECIAL_FORGERY_CLASSES = 7
SPECIAL_FORGERY_PER_CLASS = 1

SPECKLES_PROBABILITY = 1e-3
WHITE_SPECKLES_PROBABILITY = 1e-3
MIN_ROTATION_ANGLE = 1
MAX_ROTATION_ANGLE = 5
MIN_QUALITY = 50
MAX_QUALITY = 95
QUALITY_RANDOMIZATION_STEP_SIZE = 5

INPUT_SIZE = (144, 96)
INPUT_SHAPE = (96, 144, 1)

PRE_TRAINED_MODEL = load_model('embedding_net.h5', compile=False)


def get_extensions_for_type(general_type):
    for ext in mimetypes.types_map:
        if mimetypes.types_map[ext].split('/')[0] == general_type:
            yield ext.lower()


def memoize(function):
    memo = {}

    def wrapper(*args):
        if args in memo:
            return memo[args]
        else:
            return_value = function(*args)
            memo[args] = return_value
            return return_value

    return wrapper


def cache_object(obj, file):
    with open(file, 'wb') as f:
        pickle.dump(obj, f)


def load_cached(file):
    with open(file, 'rb') as f:
        return pickle.load(f)


def file_exists(file):
    return os.path.isfile(file)


@memoize
def get_image_extensions():
    return tuple(get_extensions_for_type('image'))


def file_extension(path):
    return os.path.splitext(os.path.basename(path))[1].lower()


def is_image(path):
    return file_extension(path) in get_image_extensions()


def class_from_file_name(file_name):
    return file_name[0:CLASS_LENGTH]


def extract_classes(iterable,
                    class_lambda,
                    item_mapper_lambda=lambda x: x):
    classes = dict()
    for item in iterable:
        c = class_lambda(item)
        if c not in classes:
            classes[c] = []
        classes[c].append(item_mapper_lambda(item))
    return classes


def extract_classes_from_file_names(paths,
                                    file_name_class_lambda=class_from_file_name):
    image_classes = dict()
    for path in paths:
        image_classes.update(extract_classes(filter(is_image, os.listdir(path)),
                                             file_name_class_lambda,
                                             lambda f: os.path.join(path, f)))
    return image_classes


def threshold(image):
    return cv2.threshold(
        image,
        0,
        255,
        cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]


def remove_white_border(binary_image):
    mask = (255 - binary_image) > 0

    height, width = binary_image.shape[0:2]
    mask1, mask2 = mask.any(0), mask.any(1)
    x1, x2 = mask1.argmax(), width - mask1[::-1].argmax()
    y1, y2 = mask2.argmax(), height - mask2[::-1].argmax()

    return binary_image[y1:y2, x1:x2]


def read_image(image_path):
    return cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)


def to_byte_array(np_array):
    return np.array(np_array, np.bool)


def to_8_bit_array(np_array):
    return np.array(np_array, np.uint8)


def to_int(number):
    return int(math.ceil(number))


def random_bool():
    return random.random() >= 0.5


def rotate(image, angle):
    (h, w) = image.shape
    center_x, center_y = w / 2, h / 2

    rotation_matrix = cv2.getRotationMatrix2D((center_x, center_y),
                                              angle,
                                              1.0)
    cos = np.abs(rotation_matrix[0, 0])
    sin = np.abs(rotation_matrix[0, 1])

    new_w = h * sin + w * cos
    new_h = h * cos + w * sin

    rotation_matrix[0, 2] += new_w / 2 - center_x
    rotation_matrix[1, 2] += new_h / 2 - center_y

    return cv2.warpAffine(image,
                          rotation_matrix,
                          (to_int(new_w), to_int(new_h)),
                          borderValue=(255),
                          flags=cv2.INTER_CUBIC)


def change_image_quality(image, quality=50):
    encoded = cv2.imencode('.jpg', image, [int(cv2.IMWRITE_JPEG_QUALITY), quality])[1]
    return cv2.imdecode(encoded, cv2.IMREAD_GRAYSCALE)


def random_coordinate(image):
    random_flat_coord = random.randint(0, image.size - 1)
    return random_flat_coord // image.shape[1], random_flat_coord % image.shape[1]


def speckle(image, prob, color=0):
    coords = [random_coordinate(image) for _ in range(to_int(image.size * prob))]

    image_copy = np.array(image)
    for coord in coords:
        image_copy[coord[0], coord[1]] = color

    return image_copy


def generate_variation(image):
    variation = image

    if random_bool():
        variation = change_image_quality(image, random.randrange(MIN_QUALITY,
                                                                 MAX_QUALITY + 1,
                                                                 QUALITY_RANDOMIZATION_STEP_SIZE))

    if random_bool():
        variation = rotate(variation, random.randint(MIN_ROTATION_ANGLE,
                                                     MAX_ROTATION_ANGLE))

    if random_bool():
        variation = random.choice([
            lambda img: cv2.dilate(img, np.ones((2, 2))),
            lambda img: cv2.erode(img, np.ones((2, 2)))
        ])(variation)

    if random_bool():
        variation = speckle(variation,
                            SPECKLES_PROBABILITY)

    if random_bool():
        variation = speckle(variation,
                            WHITE_SPECKLES_PROBABILITY,
                            255)

    return variation


def pad_to_height(image, height):
    difference = height - image.shape[0]
    top_padding = random.randint(0, difference)
    return np.pad(image,
                  ((top_padding, difference - top_padding),
                   (0, 0)),
                  'constant',
                  constant_values=((255, 255), (0, 0)))


def concatenate_images_in_random_order(image1, image2):
    if image1.shape[0] < image2.shape[0]:
        image1, image2 = image2, image1

    images = [image1, pad_to_height(image2, image1.shape[0])]
    random.shuffle(images)

    return np.concatenate(images, axis=1)


def generate_special_forgery_class(genuine_image_class,
                                   forgery_image_classes):
    special_forgeries = []

    for image in genuine_image_class:
        if random_bool():
            special_forgery = cv2.flip(image, 0)
        else:
            special_forgery = cv2.rotate(image, cv2.ROTATE_180)
        random_image = np.ones(image.shape, np.uint8) * 255
        special_forgeries.extend([special_forgery,
                                  generate_variation(special_forgery),
                                  speckle(random_image, SPECKLES_PROBABILITY)])

    for c in forgery_image_classes:
        forgeries = random.sample(forgery_image_classes[c], SPECIAL_FORGERY_PER_CLASS)
        for forgery in forgeries:
            special_forgery = concatenate_images_in_random_order(forgery, random.choice(genuine_image_class))
            special_forgeries.extend([special_forgery, generate_variation(special_forgery)])

    return special_forgeries


def replicate_channel(image, replicas=3):
    return np.concatenate([(np.expand_dims(image, axis=-1)) for _ in range(replicas)],
                          axis=-1)


def pre_process_image(image):
    return remove_white_border(threshold(image))


def pre_process_for_input(image):
    if image.shape[0] == INPUT_SIZE[1] and image.shape[1] == INPUT_SIZE[0]:
        resized = image
    else:
        resized = cv2.resize(image, INPUT_SIZE, interpolation=cv2.INTER_NEAREST)

    inverted = 255 - resized
    normalized = inverted / 255.0

    return normalized.reshape((INPUT_SIZE[1], INPUT_SIZE[0], 1))


def images_to_feature_vectors(image_batch,
                              model=PRE_TRAINED_MODEL,
                              model_specific_pre_processor=pre_process_for_input,
                              image_pre_processors=pre_process_image):
    return model.predict(
        np.array(
            [model_specific_pre_processor(image_pre_processors(image))
             for image in image_batch],
            np.float32
        ))


class ImageBatchVectorizer:
    def __init__(self, batch_size=1024):
        self.images = []
        self.files = []
        self.batch_size = batch_size

    def __predict(self):
        while len(self.images) >= self.batch_size:
            feature_vectors = images_to_feature_vectors(self.images[:self.batch_size])
            self.images = self.images[self.batch_size:]
            files = self.files[:self.batch_size]
            self.files = self.files[self.batch_size:]

            for i in range(len(files)):
                cache_object(feature_vectors[i], files[i])

    def feed(self, single_image, output_file):
        self.images.append(single_image)
        self.files.append(output_file)
        self.__predict()

    def feed_all(self, images, output_files):
        self.images.extend(images)
        self.files.extend(output_files)
        self.__predict()

    def finalize(self):
        feature_vectors = images_to_feature_vectors(self.images)

        for i in range(len(self.files)):
            cache_object(feature_vectors[i], self.files[i])

        self.images = []
        self.files = []


def generate_feature_vectors(image_paths,
                             augment_data=AUGMENT_DATA,
                             variations_per_image=VARIATIONS_PER_IMAGE,
                             generated_feature_vectors_path=GENERATED_FEATURE_VECTORS_PATH):
    if not os.path.isdir(generated_feature_vectors_path):
        os.makedirs(generated_feature_vectors_path)

    file_classes = extract_classes_from_file_names(image_paths)

    progress_bar = tqdm(total=len(file_classes))
    batch_vectorizer = ImageBatchVectorizer()

    for c in file_classes:
        for f in file_classes[c]:
            image = read_image(f)
            file_name = os.path.basename(f).split('.')[0]
            batch_vectorizer.feed(image, os.path.join(generated_feature_vectors_path, f'{file_name}.pickle'))
            if augment_data:
                variations = [generate_variation(image) for _ in range(variations_per_image)]
                file_paths = [os.path.join(generated_feature_vectors_path, f'{file_name}_v{i}.pickle')
                              for i in range(len(variations))]
                batch_vectorizer.feed_all(variations, file_paths)
        progress_bar.update()

    batch_vectorizer.finalize()
    progress_bar.close()


def generate_special_forgery_image_classes(image_paths,
                                           generated_feature_vectors_path=SPECIAL_GENERATED_FEATURE_VECTORS_PATH):
    if not os.path.isdir(generated_feature_vectors_path):
        os.makedirs(generated_feature_vectors_path)

    file_classes = extract_classes_from_file_names(image_paths)
    image_classes = dict()

    for c in file_classes:
        image_classes[c] = [read_image(f) for f in file_classes[c]]

    keys = [*file_classes]
    random.shuffle(keys)

    progress_bar = tqdm(total=len(keys))
    batch_vectorizer = ImageBatchVectorizer()

    for _ in range(len(keys)):
        reference_class = keys.pop(random.randint(0, len(keys) - 1))

        forgery_classes = dict()
        for c in random.sample(keys, min(SPECIAL_FORGERY_CLASSES, len(keys))):
            forgery_classes[c] = image_classes[c]

        images = generate_special_forgery_class(
            image_classes[reference_class],
            forgery_classes)

        file_paths = [os.path.join(generated_feature_vectors_path, f's{reference_class}_{i}.pickle')
                      for i in range(len(images))]

        batch_vectorizer.feed_all(images, file_paths)
        progress_bar.update()

    batch_vectorizer.finalize()
    progress_bar.close()


def run_pipeline():
    if not os.path.isdir(GENERATED_FEATURE_VECTORS_PATH):
        generate_feature_vectors(IMAGE_PATHS)

    if not os.path.isdir(SPECIAL_GENERATED_FEATURE_VECTORS_PATH) and GENERATE_SPECIAL_FORGERIES:
        generate_special_forgery_image_classes(IMAGE_PATHS)


if __name__ == '__main__':
    run_pipeline()
