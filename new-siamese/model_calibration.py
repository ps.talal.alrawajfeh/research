import pickle
import numpy as np
from matplotlib import pyplot as plt
from scipy import interpolate

EPSILON = 1e-5


def load_cached(file):
    with open(file, 'rb') as f:
        return pickle.load(f)


def main():
    labels_file = '/home/u764/Downloads/labels-predictions/labels.pickle'
    predictions_file = '/home/u764/Downloads/labels-predictions/predictions.pickle'

    labels = np.array(load_cached(labels_file), np.int)
    predictions = np.array(load_cached(predictions_file), np.float)

    genuine_predictions = predictions[labels == 0]
    forgery_predictions = predictions[labels == 1]

    genuine_predictions = np.sort(genuine_predictions)
    forgery_predictions = np.sort(forgery_predictions)

    current_threshold = 0
    current_frr = 0
    current_far = forgery_predictions.shape[0]

    threshold_array = [0.0]
    frr_array = [0.0]
    far_array = [1.0]

    genuine_i = 0
    forgery_i = 0

    while genuine_i < genuine_predictions.shape[0] and forgery_i < forgery_predictions.shape[0]:
        genuine_count = 0
        forgery_count = 0

        while genuine_i < genuine_predictions.shape[0]:
            if abs(genuine_predictions[genuine_i] - current_threshold) < EPSILON:
                genuine_i += 1
                genuine_count += 1
            else:
                break

        while forgery_i < forgery_predictions.shape[0]:
            if abs(forgery_predictions[forgery_i] - current_threshold) < EPSILON:
                forgery_i += 1
                forgery_count += 1
            else:
                break

        if genuine_i < genuine_predictions.shape[0] and forgery_i < forgery_predictions.shape[0]:
            current_threshold = min(genuine_predictions[genuine_i],
                                    forgery_predictions[forgery_i])
        elif genuine_i < genuine_predictions.shape[0]:
            current_threshold = genuine_predictions[genuine_i]
        else:
            current_threshold = forgery_predictions[forgery_i]

        current_frr += genuine_count
        current_far -= forgery_count

        threshold_array.append(current_threshold)
        frr_array.append(current_frr / genuine_predictions.shape[0])
        far_array.append(current_far / forgery_predictions.shape[0])

    mapping_start = 0.7

    i = 0
    while i < len(frr_array):
        if frr_array[i] >= mapping_start:
            break
        i += 1

    xs = [0.0, threshold_array[i]]
    ys = [0.0, mapping_start]

    mapped_value = mapping_start + 0.10

    while i < len(frr_array):
        if frr_array[i] >= mapped_value:
            xs.append(threshold_array[i])
            ys.append(mapped_value)
            mapped_value += 0.1
            if mapped_value > 0.95:
                break
        i += 1

    xs.append(1.0)
    ys.append(1.0)

    mapper = interpolate.interp1d(xs, ys)

    print(xs)
    print(ys)
    print('-----------------------------------------\n\n\n')

    print('thresh', 'frr', 'far')
    print('---------------')

    print('0.0', '0.0', '100.0')

    mapped_value = 0.1
    i = 0
    while i < len(frr_array):
        value = mapper(threshold_array[i])
        if value >= mapped_value:
            print(f'{round(float(value * 100), 2)}',
                  f'{round(float(frr_array[i] * 100), 2)}',
                  f'{round(float(far_array[i] * 100), 2)}')
            mapped_value += 0.1
        i += 1

    print('100.0', '100.0', '0.0')

    plt.plot(mapper(threshold_array), frr_array, color='blue')
    plt.plot(mapper(threshold_array), far_array, color='red')
    plt.show()


if __name__ == '__main__':
    main()
