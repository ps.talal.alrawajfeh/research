import math
import mimetypes
import os
import pickle
import random
import shutil
from enum import Enum

import cv2
import keras.backend as K
import numpy as np
from keras import Model
from keras.callbacks import LambdaCallback, ModelCheckpoint
from keras.engine.saving import load_model
from keras.layers import Input, Conv2D, BatchNormalization, MaxPooling2D, GlobalAveragePooling2D, \
    Dense, ReLU, Concatenate, Add, Dropout, Lambda, Layer
from keras.optimizers import Adam, SGD
from keras.utils import Sequence
from matplotlib import pyplot as plt
from sklearn.metrics import accuracy_score, confusion_matrix, classification_report
from tqdm import tqdm
from keras.losses import binary_crossentropy
import tensorflow as tf

CLASS_LENGTH = 9
IMAGES_PER_CLASS_THRESHOLD = 2

GENERATED_FEATURE_VECTORS_PATH = './data/feature_vectors'
SPECIAL_GENERATED_FEATURE_VECTORS_PATH = './data/special_feature_vectors'
IMAGE_CLASSES_FILE = 'image_classes.pickle'
SPECIAL_FORGERY_IMAGE_CLASSES_FILE = 'special_forgery_image_classes.pickle'
GENERATE_SPECIAL_FORGERIES = True

MODEL_FILE_NAME = 'siamese'
EVALUATION_REPORT_FILE = 'report.txt'
TRAIN_IMAGE_CLASSES_FILE = 'train_image_classes.pickle'
VALIDATION_IMAGE_CLASSES_FILE = 'validation_image_classes.pickle'
TEST_IMAGE_CLASSES_FILE = 'test_image_classes.pickle'
TRAIN_PAIRS_FILE = 'train_pairs.pickle'
VALIDATION_PAIRS_FILE = 'validation_pairs.pickle'
TEST_PAIRS_FILE = 'test_pairs.pickle'

GENUINE_LABEL = [1, 0]
FORGERY_LABEL = [0, 1]

# average number of genuines per class = VARIATIONS_PER_IMAGE * 3 + 2
# lower bound of genunines per class = VARIATIONS_PER_IMAGE * 2 + 2
REFERENCES_PER_CLASS = 10
FORGERY_CLASSES_PER_REFERENCE = 20
FORGERIES_PER_FORGERY_CLASS = 10

TRAIN_CLASSES_PERCENTAGE = 0.7
VALIDATION_CLASSES_PERCENTAGE = 0.15
TEST_CLASSES_PERCENTAGE = 0.15

INPUT_SHAPE = (1792 + 1920,)
HIDDEN_LAYER_SIZE = 1024
OUTPUT_LAYER_SIZE = 2

LEAKY_RELU_ALPHA = 0.05
LABEL_SMOOTHING_ALPHA = 0.1
LEARNING_RATE = 1e-1
CONFIDENCE_PENALTY = 0.1
EPSILON = 1e-7

TRAINING_BATCH_SIZE = 128
EVALUATION_BATCH_SIZE = 1024
EPOCHS = 50

REPORT_GENUINE_THRESHOLD = 0.5
REPORT_HISTOGRAM_BIN_SIZE = 5


class TrainingMode(Enum):
    TrainWithSplittingClasses = 1
    TrainWithoutSplittingClasses = 2


TRAINING_MODE = TrainingMode.TrainWithSplittingClasses


def cache_object(obj, file):
    with open(file, 'wb') as f:
        pickle.dump(obj, f)


def load_cached(file):
    with open(file, 'rb') as f:
        return pickle.load(f)


def file_exists(file):
    return os.path.isfile(file)


def file_extension(path):
    return os.path.splitext(os.path.basename(path))[1].lower()


def class_from_file_name(file_name):
    return file_name[0:CLASS_LENGTH]


def extract_classes(iterable,
                    class_lambda,
                    item_mapper_lambda=lambda x: x):
    classes = dict()
    for item in iterable:
        c = class_lambda(item)
        if c not in classes:
            classes[c] = []
        classes[c].append(item_mapper_lambda(item))
    return classes


def extract_classes_from_file_names(paths,
                                    file_name_class_lambda=class_from_file_name):
    image_classes = dict()
    for path in paths:
        image_classes.update(extract_classes(os.listdir(path),
                                             file_name_class_lambda,
                                             lambda f: os.path.join(path, f)))
    return image_classes


def image_classes_from_paths(image_paths, file_name_class_lambda):
    file_classes = extract_classes_from_file_names(image_paths, file_name_class_lambda)
    image_classes = dict()
    for c in file_classes:
        image_classes[c] = [load_cached(f) for f in file_classes[c]]
    return image_classes


def generate_pairs(image_classes,
                   special_forgery_image_classes):
    keys = [*image_classes]
    number_of_classes = len(keys)

    references = []
    others = []
    labels = []

    for c in range(number_of_classes - FORGERY_CLASSES_PER_REFERENCE):
        c = random.randint(0, len(keys) - 1)
        key = keys.pop(c)

        reference_indices = random.sample(list(range(len(image_classes[key]))),
                                          min(REFERENCES_PER_CLASS, len(image_classes[key])))

        for r in reference_indices:
            reference = [key, r]
            genuines = [[key, i] for i in range(len(image_classes[key]))]
            references.extend([reference] * len(genuines))
            others.extend(genuines)
            labels.extend([GENUINE_LABEL] * len(genuines))

            forgery_keys = random.sample(keys, min(FORGERY_CLASSES_PER_REFERENCE, len(keys)))
            for forgery_key in forgery_keys:
                forgery_class = image_classes[forgery_key]
                forgery_indices = random.sample(list(range(len(forgery_class))),
                                                min(FORGERIES_PER_FORGERY_CLASS, len(forgery_class)))
                forgeries = [[forgery_key, f] for f in forgery_indices]

                references.extend([reference] * len(forgeries))
                others.extend(forgeries)
                labels.extend([FORGERY_LABEL] * len(forgeries))

            if special_forgery_image_classes is not None:
                forgery_key = 's' + key
                forgery_class = special_forgery_image_classes[forgery_key]
                forgeries = [[forgery_key, f] for f in list(range(len(forgery_class)))]

                references.extend([reference] * len(forgeries))
                others.extend(forgeries)
                labels.extend([FORGERY_LABEL] * len(forgeries))

    return references, others, labels


def class_train_validation_test_split(image_classes):
    keys = [*image_classes]

    keys_indices = list(range(len(keys)))
    random.shuffle(keys_indices)

    classes = (dict(), dict(), dict())
    percentages = [TRAIN_CLASSES_PERCENTAGE,
                   VALIDATION_CLASSES_PERCENTAGE,
                   TEST_CLASSES_PERCENTAGE]

    for i in range(3):
        chosen_key_indices = keys_indices[:int(len(image_classes) * percentages[i])]
        for j in range(len(chosen_key_indices)):
            key = keys[chosen_key_indices[j]]
            classes[i][key] = image_classes[key]
        keys_indices = keys_indices[len(chosen_key_indices):]

    return classes


def shuffle_pairs(pairs):
    references, others, true_outputs = pairs
    indices = list(range(len(references)))
    random.shuffle(indices)

    shuffled_references = [references[indices[i]] for i in range(len(references))]
    shuffled_others = [others[indices[i]] for i in range(len(references))]
    shuffled_true_outputs = [true_outputs[indices[i]] for i in range(len(references))]

    return shuffled_references, shuffled_others, shuffled_true_outputs


def to_float_array(np_array):
    return np.array(np_array, np.float)


class DataGenerator(Sequence):
    def __init__(self,
                 image_classes,
                 pairs,
                 special_forgery_image_classes,
                 batch_size,
                 count):
        self.batch_size = batch_size
        self.count = count
        self.image_classes = image_classes
        self.references, self.others, self.true_labels = shuffle_pairs(pairs)
        self.special_forgery_image_classes = special_forgery_image_classes

    def __len__(self):
        return math.ceil(self.count / self.batch_size)

    def __getitem__(self, index):
        references, others, true_labels = [], [], []
        for i in range(index * self.batch_size,
                       min((index + 1) * self.batch_size, len(self.references))):
            reference_pair = self.references[i]
            other_pair = self.others[i]
            reference_image = self.image_classes[reference_pair[0]][reference_pair[1]]
            key = other_pair[0]
            if key in self.image_classes:
                other_image = self.image_classes[key][other_pair[1]]
            else:
                other_image = self.special_forgery_image_classes[key][other_pair[1]]
            references.append(to_float_array(reference_image))
            others.append(to_float_array(other_image))
            true_labels.append(to_float_array(self.true_labels[i]))

        if len(references) == 0:
            return self.__getitem__(0)

        return [np.array(references), np.array(others)], np.array(true_labels)

    def on_epoch_end(self):
        self.references, self.others, self.true_labels = shuffle_pairs([self.references,
                                                                        self.others,
                                                                        self.true_labels])


def leaky_relu6(negative_slope=LEAKY_RELU_ALPHA):
    return ReLU(max_value=6, negative_slope=negative_slope)


def absolute_difference(tensors):
    x, y = tensors
    return K.abs(K.sum(K.stack([x, -y], axis=1), axis=1))


def element_wise_product(tensors):
    x, y = tensors
    return K.prod(K.stack([x, -y], axis=1), axis=1)


def element_wise_addition(tensors):
    x, y = tensors
    return K.sum(K.stack([x, y], axis=1), axis=1)


class Hadamard(Layer):

    def __init__(self):
        super(Hadamard, self).__init__()

    def build(self, input_shape):
        self.w = self.add_weight(shape=(1,) + input_shape[1:],
                                 initializer='random_normal',
                                 trainable=True)

    def call(self, x):
        return x * self.w


def build_siamese_model(features_shape):
    # if os.path.isfile(MODEL_FILE_NAME + '.h5'):
    #     return load_model(MODEL_FILE_NAME + '.h5', compile=False)

    input1 = Input(shape=features_shape)
    input2 = Input(shape=features_shape)

    merged1 = Lambda(absolute_difference)([input1, input2])
    merged2 = Lambda(element_wise_product)([input1, input2])
    merged3 = Lambda(element_wise_addition)([input1, input2])
    concatenated = Lambda(lambda x: K.stack(x, axis=1))([merged1, merged2, merged3])
    hadamard = Hadamard()(concatenated)
    embedding = Lambda(lambda x: K.sum(x, axis=1))(hadamard)
    embedding = Dropout(0.5)(embedding)
    embedding = BatchNormalization()(embedding)

    fc1 = Dense(HIDDEN_LAYER_SIZE,
                kernel_initializer='he_normal',
                use_bias=False)(embedding)
    fc1 = Dropout(0.5)(fc1)
    fc1 = BatchNormalization()(fc1)
    fc1 = leaky_relu6()(fc1)

    concat_skip_connection = Concatenate()([embedding, fc1])

    fc2 = Dense(HIDDEN_LAYER_SIZE,
                kernel_initializer='he_normal',
                use_bias=False)(concat_skip_connection)
    fc2 = Dropout(0.5)(fc2)
    fc2 = BatchNormalization()(fc2)
    fc2 = leaky_relu6()(fc2)

    concat_skip_connection = Concatenate()([concat_skip_connection, fc2])

    prediction = Dense(OUTPUT_LAYER_SIZE,
                       activation='sigmoid',
                       kernel_initializer='glorot_normal',
                       use_bias=False)(concat_skip_connection)

    model = Model(inputs=[input1, input2], outputs=prediction)

    if os.path.isfile(MODEL_FILE_NAME + '_last.h5'):
        model.load_weights(MODEL_FILE_NAME + '_last.h5')

    return model


def binary_accuracy(y_true, y_pred, thresh=REPORT_GENUINE_THRESHOLD):
    return K.mean(K.equal(y_true > thresh, y_pred > thresh))


def entropy(prob_batch):
    log_prob_batch = tf.math.log(prob_batch + EPSILON)
    entropy_batch = -1.0 * tf.reduce_sum(prob_batch * log_prob_batch, axis=-1)
    return tf.reduce_mean(entropy_batch)


def confidence_regularized_binary_crossentropy(y_true,
                                               y_pred,
                                               label_smoothing_alpha=LABEL_SMOOTHING_ALPHA,
                                               confidence_penalty=CONFIDENCE_PENALTY):
    return binary_crossentropy(y_true,
                               y_pred,
                               label_smoothing=label_smoothing_alpha) - \
           confidence_penalty * entropy(y_pred)


def append_lines_to_report(lines):
    with open(EVALUATION_REPORT_FILE, 'a') as f:
        f.writelines(lines)


def epoch_metrics_log(epoch, logs):
    line = f'epoch: {epoch}'
    line += ' - '
    line += f'loss: {logs["loss"]}'
    line += ' - '
    line += f'binary accuracy: {logs["binary_accuracy"]}'
    line += ' - '
    line += f'validation loss: {logs["val_loss"]}'
    line += ' - '
    line += f'validation binary accuracy: {logs["val_binary_accuracy"]}'
    return line


class ModelLoadingMode(Enum):
    MinimumValue = 0
    MaximumValue = 1


def rename_best_model(mode=ModelLoadingMode.MaximumValue):
    all_models = list(filter(lambda x: x[-3:] == '.h5',
                             os.listdir(os.curdir)))
    values = []

    for model in all_models:
        value_start_index = model.rfind('_')
        value_end_index = model.rfind('.')

        if value_start_index == -1 or value_end_index == -1:
            values.append(None)
            continue

        value = model[value_start_index + 1: value_end_index]

        try:
            values.append(float(value))
        except ValueError:
            values.append(None)

    model_value_pairs = [[all_models[i], values[i]]
                         for i in range(len(all_models)) if values[i] is not None]

    model_value_pairs.sort(key=lambda x: x[1])
    if mode == ModelLoadingMode.MaximumValue:
        best_index = -1
    else:
        best_index = 0

    shutil.copy(model_value_pairs[best_index][0], MODEL_FILE_NAME + '.h5')


def train(train_image_classes,
          train_pairs,
          validation_image_classes,
          validation_pairs,
          special_forgery_image_classes):
    last_checkpoint_file = MODEL_FILE_NAME + '_last.h5'
    # if os.path.isfile(last_checkpoint_file):
    #     model = load_model(last_checkpoint_file, compile=False)
    # else:
    model = build_siamese_model(INPUT_SHAPE)
    model.summary()

    model.compile(loss=confidence_regularized_binary_crossentropy,
                  optimizer=Adam(1e-3),
                  metrics=[binary_accuracy])

    train_data_generator = DataGenerator(train_image_classes,
                                         train_pairs,
                                         special_forgery_image_classes,
                                         TRAINING_BATCH_SIZE,
                                         len(train_pairs[0]))

    validation_data_generator = DataGenerator(validation_image_classes,
                                              validation_pairs,
                                              special_forgery_image_classes,
                                              EVALUATION_BATCH_SIZE,
                                              len(validation_pairs[0]))

    append_lines_to_report('[training log]\n\n')

    update_report_callback = LambdaCallback(
        on_epoch_end=lambda epoch, logs: append_lines_to_report(
            epoch_metrics_log(epoch, logs) + '\n'
        ))

    best_checkpoint_callback = ModelCheckpoint(
        filepath=MODEL_FILE_NAME + '_{epoch:02d}_{val_binary_accuracy:0.4f}.h5',
        monitor='val_binary_accuracy',
        mode='max',
        save_best_only=True)

    last_checkpoint_callback = ModelCheckpoint(
        filepath=last_checkpoint_file,
        save_best_only=False)

    model.fit_generator(generator=train_data_generator,
                        steps_per_epoch=math.ceil(len(train_pairs[0]) / TRAINING_BATCH_SIZE),
                        validation_data=validation_data_generator,
                        validation_steps=math.ceil(len(validation_pairs[0]) / EVALUATION_BATCH_SIZE),
                        epochs=EPOCHS,
                        callbacks=[update_report_callback,
                                   best_checkpoint_callback,
                                   last_checkpoint_callback])

    rename_best_model()


def write_classification_report(all_true_labels, all_predicted_labels):
    accuracy = accuracy_score(all_true_labels, all_predicted_labels)
    append_lines_to_report(f'\ntest accuracy: {round(accuracy * 100, 2)}%\n')

    append_lines_to_report('\nconfusion matrix:\n')
    matrix = confusion_matrix(all_true_labels, all_predicted_labels)
    append_lines_to_report(f'{matrix}\n')

    frr = round((matrix[0, 1] / np.sum(matrix[0])) * 100, 2)
    far = round((matrix[1, 0] / np.sum(matrix[1])) * 100, 2)
    append_lines_to_report(f'\nFRR: {frr}%\n')
    append_lines_to_report(f'FAR: {far}%\n')

    append_lines_to_report('\nclassification report:\n')
    append_lines_to_report(f'{classification_report(all_true_labels, all_predicted_labels)}\n')


def draw_distributions(all_true_labels,
                       all_predicted_outputs,
                       genuine_threshold=REPORT_GENUINE_THRESHOLD,
                       bin_size=REPORT_HISTOGRAM_BIN_SIZE):
    bins = [bin_size * i / 100 for i in range(1, 100 // bin_size)]

    true_labels = 1 - np.array(all_true_labels)
    predicted_outputs = np.array(all_predicted_outputs)

    genuine_outputs = predicted_outputs[true_labels > genuine_threshold]
    forgery_outputs = predicted_outputs[true_labels <= genuine_threshold]

    plt.hist(genuine_outputs, bins, color='blue', label='genuine')
    plt.legend(loc='best')
    plt.savefig('genuine_distribution.png', dpi=200)
    plt.close('all')

    plt.hist(forgery_outputs, bins, color='red', label='forgery')
    plt.legend(loc='best')
    plt.savefig('forgery_distribution.png', dpi=200)
    plt.close('all')

    plt.hist(genuine_outputs, bins, color='blue', label='genuine')
    plt.hist(forgery_outputs, bins, color='red', label='forgery')
    plt.legend(loc='best')
    plt.savefig('distributions.png', dpi=200)
    plt.close('all')


def evaluate(model,
             test_image_classes,
             test_pairs,
             special_forgery_image_classes):
    model.summary()

    model.compile(loss=confidence_regularized_binary_crossentropy,
                  optimizer=Adam(LEARNING_RATE),
                  metrics=[binary_accuracy])

    test_data_generator = DataGenerator(test_image_classes,
                                        test_pairs,
                                        special_forgery_image_classes,
                                        EVALUATION_BATCH_SIZE,
                                        len(test_pairs[0]))

    append_lines_to_report('\n\n[evaluation log]\n')

    results = model.evaluate_generator(generator=test_data_generator,
                                       steps=math.ceil(len(test_pairs[0]) / EVALUATION_BATCH_SIZE))

    append_lines_to_report(f'test loss: {results[0]} - test accuracy: {results[1]}\n')

    all_true_labels, all_predicted_labels, all_predicted_outputs = [], [], []
    batches = len(test_data_generator)
    progress_bar = tqdm(total=batches)
    for i in range(batches):
        input_channels, true_labels = test_data_generator[i]
        predicted_outputs = model.predict(input_channels)
        all_true_labels.extend(list(np.argmax(true_labels, axis=1)))
        all_predicted_labels.extend(list(np.argmax(predicted_outputs, axis=1)))
        all_predicted_outputs.extend(list(predicted_outputs[:, 0]))
        progress_bar.update()
    progress_bar.close()

    write_classification_report(all_true_labels, all_predicted_labels)
    draw_distributions(all_true_labels, all_predicted_outputs)


def load_or_supply_and_cache(cache_file, supplier_lambda):
    if file_exists(cache_file):
        return load_cached(cache_file)
    result = supplier_lambda()
    cache_object(result, cache_file)
    return result


def train_model(train_image_classes,
                validation_image_classes,
                special_forgery_image_classes):
    train_pairs = load_or_supply_and_cache(TRAIN_PAIRS_FILE,
                                           lambda: generate_pairs(train_image_classes,
                                                                  special_forgery_image_classes))
    validation_pairs = load_or_supply_and_cache(VALIDATION_PAIRS_FILE,
                                                lambda: generate_pairs(validation_image_classes,
                                                                       special_forgery_image_classes))

    train(train_image_classes,
          train_pairs,
          validation_image_classes,
          validation_pairs,
          special_forgery_image_classes)


def evaluate_model(test_image_classes,
                   special_forgery_image_classes):
    test_pairs = load_or_supply_and_cache(TEST_PAIRS_FILE,
                                          lambda: generate_pairs(test_image_classes,
                                                                 special_forgery_image_classes))

    # model = load_model(MODEL_FILE_NAME + '.h5', compile=False)
    model = build_siamese_model(INPUT_SHAPE)
    model.load_weights(MODEL_FILE_NAME + '.h5')

    evaluate(model,
             test_image_classes,
             test_pairs,
             special_forgery_image_classes)


def run_pipeline():
    image_classes = None
    train_image_classes = None
    validation_image_classes = None
    test_image_classes = None
    special_forgery_image_classes = None

    if not file_exists(IMAGE_CLASSES_FILE):
        image_classes = image_classes_from_paths([GENERATED_FEATURE_VECTORS_PATH],
                                                 lambda f: f[0:CLASS_LENGTH])
        cache_object(image_classes, IMAGE_CLASSES_FILE)

    if not file_exists(SPECIAL_FORGERY_IMAGE_CLASSES_FILE) and GENERATE_SPECIAL_FORGERIES:
        special_forgery_image_classes = image_classes_from_paths([SPECIAL_GENERATED_FEATURE_VECTORS_PATH],
                                                                 lambda f: f[0:CLASS_LENGTH + 1])
        cache_object(special_forgery_image_classes, SPECIAL_FORGERY_IMAGE_CLASSES_FILE)

    if not file_exists(TRAIN_IMAGE_CLASSES_FILE) or \
            not file_exists(VALIDATION_IMAGE_CLASSES_FILE) or \
            not file_exists(TEST_IMAGE_CLASSES_FILE):
        if image_classes is None:
            image_classes = load_cached(IMAGE_CLASSES_FILE)
        train_image_classes, validation_image_classes, test_image_classes = class_train_validation_test_split(
            image_classes)

        if TRAINING_MODE == TrainingMode.TrainWithoutSplittingClasses:
            train_image_classes = image_classes

        cache_object(train_image_classes, TRAIN_IMAGE_CLASSES_FILE)
        cache_object(validation_image_classes, VALIDATION_IMAGE_CLASSES_FILE)
        cache_object(test_image_classes, TEST_IMAGE_CLASSES_FILE)

    if not file_exists(MODEL_FILE_NAME + '.h5'):
        if train_image_classes is None or \
                validation_image_classes is None:
            train_image_classes = load_cached(TRAIN_IMAGE_CLASSES_FILE)
            validation_image_classes = load_cached(VALIDATION_IMAGE_CLASSES_FILE)
        if GENERATE_SPECIAL_FORGERIES and special_forgery_image_classes is None:
            special_forgery_image_classes = load_cached(SPECIAL_FORGERY_IMAGE_CLASSES_FILE)
        train_model(train_image_classes,
                    validation_image_classes,
                    special_forgery_image_classes)

    if file_exists(MODEL_FILE_NAME + '.h5'):
        if test_image_classes is None:
            test_image_classes = load_cached(TEST_IMAGE_CLASSES_FILE)
        if GENERATE_SPECIAL_FORGERIES and special_forgery_image_classes is None:
            special_forgery_image_classes = load_cached(SPECIAL_FORGERY_IMAGE_CLASSES_FILE)
        evaluate_model(test_image_classes, special_forgery_image_classes)


if __name__ == '__main__':
    run_pipeline()
