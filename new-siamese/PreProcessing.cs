using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace PreProcessing
{
    class Program
    {
        private const int InputWidth = 144;
        private const int InputHeight = 96;

        private const double RedWeight = 0.299;
        private const double GreenWeight = 0.587;
        private const double BlueWeight = 1.0 - (RedWeight + GreenWeight);

        private const double FloatEpsilon = 1e-5;

        private const int GrayScaleDepth = 256;

        public static int ConvertRgbToGrayScale(int r, int g, int b)
        {
            return (int) Math.Round(r * RedWeight + g * GreenWeight + b * BlueWeight);
        }

        public static int[] BitmapToMatrix(Bitmap bitmap)
        {
            var width = bitmap.Width;
            var height = bitmap.Height;

            var matrix = new int[height * width];

            for (var y = 0; y < height; y++)
            {
                for (var x = 0; x < width; x++)
                {
                    var pixel = bitmap.GetPixel(x, y);
                    matrix[y * width + x] = ConvertRgbToGrayScale(pixel.R, pixel.G, pixel.B);
                }
            }

            return matrix;
        }

        public static Bitmap MatrixToBitmap(int[] matrix, int height, int width)
        {
            Bitmap bitmap = new Bitmap(width, height, PixelFormat.Format24bppRgb);

            for (var y = 0; y < height; y++)
            {
                for (var x = 0; x < width; x++)
                {
                    var pixel = matrix[y * width + x];
                    bitmap.SetPixel(x, y, Color.FromArgb(pixel, pixel, pixel));
                }
            }

            return bitmap;
        }

        public static int[] Threshold(int[] matrix, int value)
        {
            return matrix.Select(x => x > value ? 255 : 0).ToArray();
        }

        public static Bitmap OpenCvOtsu(Bitmap inputImage)
        {
            try
            {
                if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed) return null;

                var outputImage = (Bitmap) inputImage.Clone();
                int height = outputImage.Height;
                int width = outputImage.Width;

                var outputImageData = outputImage.LockBits(new Rectangle(0, 0, width, height),
                    ImageLockMode.ReadWrite,
                    outputImage.PixelFormat);

                int stride = outputImageData.Stride;
                int offset = stride - outputImage.Width;
                var scan0 = outputImageData.Scan0;
                byte* outputImageDataPtr = (byte*) (void*) scan0;

                var histogram = new int[256];
                for (var y = 0; y < height; y++)
                {
                    for (var x = 0; x < width; x++)
                    {
                        int i = outputImageDataPtr![0];
                        histogram[i]++;
                        outputImageDataPtr++;
                    }

                    outputImageDataPtr += offset;
                }

                double scale = 1.0 / (height * width);
                double mu = histogram.Select((x, i) => x * i).Sum() * scale;

                double q1 = 0;
                var maxVal = 0;
                double maxSigma = -1;
                double mu1 = 0;

                for (int t = 0; t < 256; t++)
                {
                    var pt = histogram[t] * scale;
                    mu1 *= q1;
                    q1 += pt;
                    var q2 = 1.0 - q1;

                    if (Math.Min(q1, q2) < FloatEpsilon || Math.Max(q1, q2) > 1.0 - FloatEpsilon)
                    {
                        continue;
                    }

                    mu1 = (mu1 + t * pt) / q1;
                    double mu2 = (mu - q1 * mu1) / q2;

                    var sigma = q1 * q2 * (mu1 - mu2) * (mu1 - mu2);
                    if (sigma > maxSigma)
                    {
                        maxSigma = sigma;
                        maxVal = t;
                    }
                }

                outputImageDataPtr = (byte*) (void*) scan0;
                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        outputImageDataPtr![0] = outputImageDataPtr[0] > maxVal ? (byte) 255 : (byte) 0;
                        outputImageDataPtr++;
                    }

                    outputImageDataPtr += offset;
                }

                outputImage.UnlockBits(outputImageData);
                return outputImage;
            }
            catch
            {
                return null;
            }
        }

        public static int[] OtsuThreshold(int[] matrix, int height, int width)
        {
            var histogram = new int[GrayScaleDepth];
            for (var y = 0; y < height; y++)
            {
                for (var x = 0; x < width; x++)
                {
                    histogram[matrix[y * width + x]]++;
                }
            }

            double scale = 1.0 / (height * width);
            double mu = histogram.Select((x, i) => x * i).Sum() * scale;

            double q1 = 0;
            var maxVal = 0;
            double maxSigma = -1;
            double mu1 = 0;

            for (int t = 0; t < GrayScaleDepth; t++)
            {
                var pt = histogram[t] * scale;
                mu1 *= q1;
                q1 += pt;
                var q2 = 1.0 - q1;

                if (Math.Min(q1, q2) < FloatEpsilon || Math.Max(q1, q2) > 1.0 - FloatEpsilon)
                {
                    continue;
                }

                mu1 = (mu1 + t * pt) / q1;
                double mu2 = (mu - q1 * mu1) / q2;

                var sigma = q1 * q2 * (mu1 - mu2) * (mu1 - mu2);
                if (sigma > maxSigma)
                {
                    maxSigma = sigma;
                    maxVal = t;
                }
            }

            return Threshold(matrix, maxVal);
        }

        public static (int[], int, int) RemoveWhiteBorder(int[] matrix, int height, int width)
        {
            int x1 = 0, y1 = 0, x2 = width - 1, y2 = height - 1;

            while (y1 < height &&
                   Enumerable.Range(0, width)
                       .Select(x => matrix[y1 * width + x])
                       .All(x => x == 255))
            {
                y1++;
            }

            while (x1 < width &&
                   Enumerable.Range(0, height)
                       .Select(y => matrix[y * width + x1])
                       .All(y => y == 255))
            {
                x1++;
            }

            while (y2 >= 0 &&
                   Enumerable.Range(0, width)
                       .Select(x => matrix[y2 * width + x])
                       .All(x => x == 255))
            {
                y2--;
            }

            while (x2 >= 0 &&
                   Enumerable.Range(0, height)
                       .Select(y => matrix[y * width + x2])
                       .All(y => y == 255))
            {
                x2--;
            }

            if (x1 >= x2 || y1 >= y2)
            {
                return (new int[0], 0, 0);
            }

            var newHeight = y2 - y1 + 1;
            var newWidth = x2 - x1 + 1;
            var cropped = new int[newHeight * newWidth];

            for (var y = 0; y < newHeight; y++)
            {
                for (var x = 0; x < newWidth; x++)
                {
                    cropped[y * newWidth + x] = matrix[(y1 + y) * width + x1 + x];
                }
            }

            return (cropped, newHeight, newWidth);
        }

        public static int SpecialFloor(double value)
        {
            var floor = (int) value;
            if (floor > value)
            {
                floor--;
            }

            return floor;
        }

        public static int[] ResizeNearestNeighbors(int[] srcMatrix,
            int srcWidth, int srcHeight,
            int destWidth, int destHeight)
        {
            if (srcWidth == 0 || srcHeight == 0 || destWidth == 0 || destHeight == 0)
            {
                return new int[0];
            }

            var fx = destWidth / (double) srcWidth;
            var fy = destHeight / (double) srcHeight;

            var ifx = 1.0 / fx;
            var ify = 1.0 / fy;

            var destMatrix = new int[destHeight * destWidth];

            for (var y = 0; y < destHeight; y++)
            {
                for (var x = 0; x < destWidth; x++)
                {
                    var srcX = SpecialFloor(ifx * x);
                    var srcY = SpecialFloor(ify * y);

                    srcX = Math.Max(Math.Min(srcX, srcWidth - 1), 0);
                    srcY = Math.Max(Math.Min(srcY, srcHeight - 1), 0);

                    destMatrix[y * destWidth + x] = srcMatrix[srcY * srcWidth + srcX];
                }
            }

            return destMatrix;
        }

        public static float[] ToNeuralNetworkInput(int[] matrix)
        {
            var result = new float[InputHeight * InputWidth];

            for (int i = 0; i < InputHeight * InputWidth; i++)
            {
                if (matrix[i] == 0)
                {
                    result[i] = 1.0f;
                }
                else
                {
                    result[i] = 0.0f;
                }
            }

            return result;
        }

        public static float[] PreProcess(Bitmap bitmap)
        {
            int height = bitmap.Height;
            int width = bitmap.Width;

            var (cropped, newHeight, newWidth) = RemoveWhiteBorder(
                OtsuThreshold(BitmapToMatrix(bitmap), height, width),
                height, width);

            var resized = ResizeNearestNeighbors(cropped,
                newWidth, newHeight,
                InputWidth, InputHeight);

            return ToNeuralNetworkInput(resized);
        }
    }
}
