import math

import cv2
import numpy as np
import os
from tqdm import tqdm


def print_statistics(array):
    without_outliers = np.array(array, np.float32)
    without_outliers.sort()
    begin = int(math.ceil(len(without_outliers) * 0.05))
    end = int(math.floor(len(without_outliers) * 0.95))
    without_outliers = without_outliers[begin:end]

    print(f'mean: {np.mean(without_outliers)}')
    print(f'median: {np.median(without_outliers)}')
    print(f'std: {np.std(without_outliers)}')
    print(f'min: {np.min(without_outliers)}')
    print(f'max: {np.max(without_outliers)}')


def main():
    base_dir = '/home/u764/Downloads/signatures'
    files = os.listdir(base_dir)

    widths = []
    heights = []
    areas = []
    ratios = []

    progress_bar = tqdm(total=len(files))
    for f in files:
        image = cv2.imread(os.path.join(base_dir, f))

        widths.append(image.shape[1])
        heights.append(image.shape[0])
        areas.append(widths[-1] * heights[-1])
        ratios.append(heights[-1] / widths[-1])
        progress_bar.update()
    progress_bar.close()

    print('~~ widths ~~')
    print_statistics(widths)
    print('~~~~~~~~~~~~\n')

    print('~~ heights ~~')
    print_statistics(heights)
    print('~~~~~~~~~~~~~\n')

    print('~~ areas ~~')
    print_statistics(areas)
    print('~~~~~~~~~~~\n')

    print('~~ ratios ~~')
    print_statistics(ratios)
    print('~~~~~~~~~~~~\n')


if __name__ == '__main__':
    main()

# ~~ widths ~~
# mean: 269.7056579589844
# median: 233.0
# std: 107.10079956054688
# min: 113.0
# max: 581.0
# ~~~~~~~~~~~~
#
# ~~ heights ~~
# mean: 127.5453872680664
# median: 113.0
# std: 58.4321403503418
# min: 45.0
# max: 262.0
# ~~~~~~~~~~~~~
#
# ~~ areas ~~
# mean: 36868.5234375
# median: 28829.0
# std: 26803.390625
# min: 6642.0
# max: 131461.0
# ~~~~~~~~~~~
#
# ~~ ratios ~~
# mean: 0.5047498345375061
# median: 0.447826087474823
# std: 0.2251010537147522
# min: 0.19230769574642181
# max: 1.0
# ~~~~~~~~~~~~
