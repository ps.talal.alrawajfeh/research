import os
import random

import cv2
import numpy as np
from keras.engine.saving import load_model
from matplotlib import pyplot as plt

from new_siamese import rotate

INPUT_SIZE = (144, 96)
INPUT_SHAPE = (96, 144, 2)


def threshold(image):
    return cv2.threshold(
        image,
        0,
        255,
        cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]


def remove_white_border(binary_image):
    mask = (255 - binary_image) > 0

    height, width = binary_image.shape[0:2]
    mask1, mask2 = mask.any(0), mask.any(1)
    x1, x2 = mask1.argmax(), width - mask1[::-1].argmax()
    y1, y2 = mask2.argmax(), height - mask2[::-1].argmax()

    return binary_image[y1:y2, x1:x2]


def pre_process_image(image):
    return remove_white_border(threshold(image))


def pre_process_for_input(image):
    if image.shape[0] == INPUT_SIZE[1] and image.shape[1] == INPUT_SIZE[0]:
        resized = image
    else:
        resized = cv2.resize(image, INPUT_SIZE, interpolation=cv2.INTER_NEAREST)

    inverted = 255 - resized
    normalized = inverted / 255.0

    return normalized.reshape((INPUT_SIZE[1], INPUT_SIZE[0], 1))


def read_image(image_path):
    return cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)


def concatenate_signatures(signature1, signature2):
    if signature1.shape[0] >= signature2.shape[0]:
        larger_signature = signature1
        smaller_signature = signature2
    else:
        larger_signature = signature2
        smaller_signature = signature1

    height_difference = larger_signature.shape[0] - smaller_signature.shape[0]

    top_pad = random.randint(0, height_difference)
    bottom_pad = height_difference - top_pad

    smaller_signature = np.pad(smaller_signature,
                               ((top_pad, bottom_pad), (0, 0)),
                               mode='constant',
                               constant_values=((255, 255), (255, 255)))

    signatures = [larger_signature, smaller_signature]
    random.shuffle(signatures)

    return np.concatenate(signatures, -1)


def view_image(image):
    plt.imshow(image, plt.cm.gray)
    plt.show()


def feed_forward(signature1, signature2):
    signature1_pre_processed = pre_process_for_input(pre_process_image(signature1))
    signature2_pre_processed = pre_process_for_input(pre_process_image(signature2))

    concatenated_input = np.concatenate([signature1_pre_processed,
                                         signature2_pre_processed], axis=-1)

    siamese = load_model('models/siamese.h5', compile=False)

    prediction = siamese.predict(np.array([concatenated_input]))[0]

    prob = int(prediction[0] * 100) / 100
    if prediction[0] > 0.5:
        print(f'{prediction[0]} -> signatures match')
    else:
        print(f'{prediction[0]} -> signatures do not match')


def main():
    # feed_forward(read_image('sig1.jpg'),
    #              read_image('sig1.jpg'))

    i = 0
    sigs = os.listdir('/home/u764/Downloads/signatures')
    for sig in sigs:
        im1 = read_image('/home/u764/Downloads/python/' + sig + '.bmp')
        im2 = read_image('/home/u764/Downloads/csharp/' + sig + '.bmp')
        if not np.all(im1 == im2):
            print(f'Found One')
            i += 1
        else:
            print('good')
            pass
        # im = pre_process_image(read_image('/home/u764/Downloads/signatures/' + sig))
        # im = cv2.resize(im, INPUT_SIZE, interpolation=cv2.INTER_NEAREST)
        # cv2.imwrite('/home/u764/Downloads/python/' + sig + '.bmp', im)
    print(i)


def main2():
    image = read_image('/home/u764/Downloads/signatures/firma0195-14.bmp')
    print(cv2.threshold(
        image,
        0,
        255,
        cv2.THRESH_BINARY + cv2.THRESH_OTSU)[0])


def main3():
    feed_forward(read_image('/home/u764/Downloads/sig3.bmp'),
                 read_image('/home/u764/Downloads/sig2.bmp'))


if __name__ == '__main__':
    main3()
