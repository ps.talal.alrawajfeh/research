import math
import mimetypes
import os
import pickle
import random
import shutil
from enum import Enum

import cv2
import numpy as np
from tensorflow.keras import Model
from tensorflow.keras.callbacks import LambdaCallback, ModelCheckpoint, LearningRateScheduler
from tensorflow.keras.models import load_model
from tensorflow.keras.layers import Input, Conv2D, BatchNormalization, MaxPooling2D, GlobalAveragePooling2D, \
    Dense, ReLU, Concatenate, Add, Dropout, Layer, Lambda
from tensorflow.keras.optimizers import Adam, SGD
from tensorflow.keras.utils import Sequence
from matplotlib import pyplot as plt
from sklearn.metrics import accuracy_score, confusion_matrix, classification_report
from tqdm import tqdm
from tensorflow.keras.losses import binary_crossentropy
import tensorflow as tf
from alt_model_checkpoint.tensorflow import AltModelCheckpoint

## latest ##

CLASS_LENGTH = 9
IMAGES_PER_CLASS_THRESHOLD = 2
IMAGE_PATHS = ['/home/user/Development/data/trainingSignatures',
               '/home/user/Development/data/CABHBTF']

MODEL_FILE_NAME = 'embedding_siamese'
SIAMESE_MODEL_FILE_NAME = 'siamese'
EMBEDDING_NET_MODEL_FILE_NAME = 'embedding_net'
EVALUATION_REPORT_FILE = 'report.txt'
IMAGE_CLASSES_FILE = 'image_classes.pickle'
SPECIAL_FORGERY_IMAGE_CLASSES_FILE = 'special_forgery_image_classes.pickle'
TRAIN_IMAGE_CLASSES_FILE = 'train_image_classes.pickle'
VALIDATION_IMAGE_CLASSES_FILE = 'validation_image_classes.pickle'
TEST_IMAGE_CLASSES_FILE = 'test_image_classes.pickle'
TRAIN_PAIRS_FILE = 'train_pairs.pickle'
VALIDATION_PAIRS_FILE = 'validation_pairs.pickle'
TEST_PAIRS_FILE = 'test_pairs.pickle'

GENUINE_LABEL = [1, 0]
FORGERY_LABEL = [0, 1]

# average number of genuines per class = VARIATIONS_PER_IMAGE * 3 + 2
# lower bound of genunines per class = VARIATIONS_PER_IMAGE * 2 + 2
REFERENCES_PER_CLASS = 10
FORGERY_CLASSES_PER_REFERENCE = 14
FORGERIES_PER_FORGERY_CLASS = 6

TRAIN_CLASSES_PERCENTAGE = 0.7
VALIDATION_CLASSES_PERCENTAGE = 0.15
TEST_CLASSES_PERCENTAGE = 0.15

INPUT_SIZE = (144, 96)
INPUT_SHAPE = (96, 144, 1)

LEAKY_RELU_ALPHA = 0.05
INPUT_DROPOUT_PERCENTAGE = 0.05
EMBEDDING_MODEL_FILTERS = (30, 60, 100, 150, 200)
EMBEDDING_VECTOR_SIZE = 200
LABEL_SMOOTHING_ALPHA = 0.1
LEARNING_RATE = 1e-1
CONFIDENCE_PENALTY = 0.1
EPSILON = 1e-7
INITIAL_LEARNING_RATE = 1e-1
FINAL_LEARNING_RATE = 1e-3

TRAINING_BATCH_SIZE = 64
EVALUATION_BATCH_SIZE = 256
EPOCHS = 12

REPORT_GENUINE_THRESHOLD = 0.5
REPORT_HISTOGRAM_BIN_SIZE = 5

AUGMENT_DATA = True
GENERATE_SPECIAL_FORGERIES = True
VARIATIONS_PER_IMAGE = 4

SPECIAL_FORGERY_VARIATIONS = 1
SPECIAL_FORGERY_CLASSES = FORGERY_CLASSES_PER_REFERENCE // 2
SPECIAL_FORGERY_PER_CLASS = 1

SPECKLES_PROBABILITY = 1e-3
MIN_ROTATION_ANGLE = 1
MAX_ROTATION_ANGLE = 5
MIN_QUALITY = 50
MAX_QUALITY = 95
QUALITY_RANDOMIZATION_STEP_SIZE = 5


class TrainingMode(Enum):
    TrainWithSplittingClasses = 1
    TrainWithoutSplittingClasses = 2


TRAINING_MODE = TrainingMode.TrainWithSplittingClasses


def get_extensions_for_type(general_type):
    for ext in mimetypes.types_map:
        if mimetypes.types_map[ext].split('/')[0] == general_type:
            yield ext.lower()


def memoize(function):
    memo = {}

    def wrapper(*args):
        if args in memo:
            return memo[args]
        else:
            return_value = function(*args)
            memo[args] = return_value
            return return_value

    return wrapper


def cache_object(obj, file):
    with open(file, 'wb') as f:
        pickle.dump(obj, f)


def load_cached(file):
    with open(file, 'rb') as f:
        return pickle.load(f)


def file_exists(file):
    return os.path.isfile(file)


@memoize
def get_image_extensions():
    return tuple(get_extensions_for_type('image'))


def file_extension(path):
    return os.path.splitext(os.path.basename(path))[1].lower()


def is_image(path):
    return file_extension(path) in get_image_extensions()


def class_from_file_name(file_name):
    return file_name[0:CLASS_LENGTH]


def extract_classes(iterable,
                    class_lambda,
                    item_mapper_lambda=lambda x: x):
    classes = dict()
    for item in iterable:
        c = class_lambda(item)
        if c not in classes:
            classes[c] = []
        classes[c].append(item_mapper_lambda(item))
    return classes


def extract_classes_from_file_names(paths,
                                    file_name_class_lambda=class_from_file_name):
    image_classes = dict()
    for path in paths:
        image_classes.update(extract_classes(filter(is_image, os.listdir(path)),
                                             file_name_class_lambda,
                                             lambda f: os.path.join(path, f)))
    return image_classes


def threshold(image):
    return cv2.threshold(
        image,
        0,
        255,
        cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]


def remove_white_border(binary_image):
    mask = (255 - binary_image) > 0

    height, width = binary_image.shape[0:2]
    mask1, mask2 = mask.any(0), mask.any(1)
    x1, x2 = mask1.argmax(), width - mask1[::-1].argmax()
    y1, y2 = mask2.argmax(), height - mask2[::-1].argmax()

    return binary_image[y1:y2, x1:x2]


def read_image(image_path):
    return cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)


def pre_process_image(image):
    return remove_white_border(threshold(image))


def to_byte_array(np_array):
    return np.array(np_array, np.bool)


def to_8_bit_array(np_array):
    return np.array(np_array, np.uint8)


def pre_process_for_input(image):
    if image.shape[0] == INPUT_SIZE[1] and image.shape[1] == INPUT_SIZE[0]:
        resized = image
    else:
        resized = cv2.resize(image, INPUT_SIZE, interpolation=cv2.INTER_NEAREST)

    inverted = 255 - resized
    normalized = inverted / 255.0

    return to_byte_array(normalized.reshape((INPUT_SIZE[1], INPUT_SIZE[0], 1)))


def to_int(number):
    return int(math.ceil(number))


def random_bool():
    return random.random() >= 0.5


def rotate(image, angle):
    (h, w) = image.shape
    center_x, center_y = w / 2, h / 2

    rotation_matrix = cv2.getRotationMatrix2D((center_x, center_y),
                                              angle,
                                              1.0)
    cos = np.abs(rotation_matrix[0, 0])
    sin = np.abs(rotation_matrix[0, 1])

    new_w = h * sin + w * cos
    new_h = h * cos + w * sin

    rotation_matrix[0, 2] += new_w / 2 - center_x
    rotation_matrix[1, 2] += new_h / 2 - center_y

    return cv2.warpAffine(image,
                          rotation_matrix,
                          (to_int(new_w), to_int(new_h)),
                          borderValue=(255),
                          flags=cv2.INTER_CUBIC)


def change_image_quality(image, quality=50):
    encoded = cv2.imencode('.jpg', image, [int(cv2.IMWRITE_JPEG_QUALITY), quality])[1]
    return cv2.imdecode(encoded, cv2.IMREAD_GRAYSCALE)


def random_coordinate(image):
    random_flat_coord = random.randint(0, image.size - 1)
    return random_flat_coord // image.shape[1], random_flat_coord % image.shape[1]


def speckle(image, prob, color=0):
    coords = [random_coordinate(image) for _ in range(to_int(image.size * prob))]

    image_copy = np.array(image)
    for coord in coords:
        image_copy[coord[0], coord[1]] = color

    return image_copy


def generate_variation(image):
    variation = image

    if random_bool():
        variation = change_image_quality(image, random.randrange(MIN_QUALITY,
                                                                 MAX_QUALITY + 1,
                                                                 QUALITY_RANDOMIZATION_STEP_SIZE))

    if random_bool():
        variation = rotate(variation, random.randint(MIN_ROTATION_ANGLE,
                                                     MAX_ROTATION_ANGLE))

    if random_bool():
        variation = random.choice([
            lambda img: cv2.dilate(img, np.ones((2, 2))),
            lambda img: cv2.erode(img, np.ones((2, 2)))
        ])(variation)

    if random_bool():
        variation = speckle(variation,
                            SPECKLES_PROBABILITY)

    return variation


def pad_to_height(image, height):
    difference = height - image.shape[0]
    top_padding = random.randint(0, difference)
    return np.pad(image,
                  ((top_padding, difference - top_padding),
                   (0, 0)),
                  'constant',
                  constant_values=((255, 255), (0, 0)))


def concatenate_images_in_random_order(image1, image2):
    if image1.shape[0] < image2.shape[0]:
        image1, image2 = image2, image1

    images = [image1, pad_to_height(image2, image1.shape[0])]
    random.shuffle(images)

    return np.concatenate(images, axis=1)


def generate_special_forgery_class(genuine_image_class,
                                   forgery_image_classes):
    special_forgeries = []

    for image in genuine_image_class:
        if random_bool():
            special_forgery = cv2.flip(image, 0)
        else:
            special_forgery = cv2.rotate(image, cv2.ROTATE_180)
        random_image = np.ones(image.shape, np.uint8) * 255
        special_forgeries.extend([special_forgery,
                                  generate_variation(special_forgery),
                                  speckle(random_image, SPECKLES_PROBABILITY)])

    for c in forgery_image_classes:
        forgeries = random.sample(forgery_image_classes[c], SPECIAL_FORGERY_PER_CLASS)
        for forgery in forgeries:
            special_forgery = concatenate_images_in_random_order(forgery, random.choice(genuine_image_class))
            special_forgeries.extend([special_forgery, generate_variation(special_forgery)])

    return special_forgeries


def view_images(images, title=''):
    if len(images) == 1:
        plt.imshow(images[0], plt.cm.gray)
    else:
        figure, axis = plt.subplots(len(images), 1)
        for i in range(len(images)):
            axis[i].imshow(images[i], plt.cm.gray)
    plt.title(title)
    plt.show()


def image_classes_from_paths(image_paths,
                             augment_data=AUGMENT_DATA,
                             variations_per_image=VARIATIONS_PER_IMAGE):
    file_classes = extract_classes_from_file_names(image_paths)
    image_classes = dict()
    for c in file_classes:
        image_classes[c] = []
        for f in file_classes[c]:
            image = read_image(f)
            image_classes[c].append(pre_process_for_input(pre_process_image(image)))
            if augment_data:
                variations = [generate_variation(image) for _ in range(variations_per_image)]
                variations = [pre_process_for_input(pre_process_image(v)) for v in variations]
                image_classes[c].extend(variations)
    return image_classes


def generate_special_forgery_image_classes(image_paths):
    file_classes = extract_classes_from_file_names(image_paths)
    image_classes = dict()

    for c in file_classes:
        image_classes[c] = [read_image(f) for f in file_classes[c]]

    special_forgery_image_classes = dict()

    keys = [*file_classes]
    random.shuffle(keys)

    for _ in range(len(keys)):
        reference_class = keys.pop(random.randint(0, len(keys) - 1))

        forgery_classes = dict()
        for c in random.sample(keys, min(SPECIAL_FORGERY_CLASSES, len(keys))):
            forgery_classes[c] = image_classes[c]

        special_forgery_image_classes['s' + reference_class] = [
            pre_process_for_input(pre_process_image(image))
            for image in generate_special_forgery_class(image_classes[reference_class],
                                                        forgery_classes)]

    return special_forgery_image_classes


def generate_pairs(image_classes,
                   special_forgery_image_classes):
    keys = [*image_classes]
    number_of_classes = len(keys)

    references = []
    others = []
    labels = []

    for c in range(number_of_classes - FORGERY_CLASSES_PER_REFERENCE):
        c = random.randint(0, len(keys) - 1)
        key = keys.pop(c)

        reference_indices = random.sample(list(range(len(image_classes[key]))),
                                          min(REFERENCES_PER_CLASS, len(image_classes[key])))

        for r in reference_indices:
            reference = [key, r]
            genuines = [[key, i] for i in range(len(image_classes[key]))]
            references.extend([reference] * len(genuines))
            others.extend(genuines)
            labels.extend([GENUINE_LABEL] * len(genuines))

            forgery_keys = random.sample(keys, min(FORGERY_CLASSES_PER_REFERENCE, len(keys)))
            for forgery_key in forgery_keys:
                forgery_class = image_classes[forgery_key]
                forgery_indices = random.sample(list(range(len(forgery_class))),
                                                min(FORGERIES_PER_FORGERY_CLASS, len(forgery_class)))
                forgeries = [[forgery_key, f] for f in forgery_indices]

                references.extend([reference] * len(forgeries))
                others.extend(forgeries)
                labels.extend([FORGERY_LABEL] * len(forgeries))

            if special_forgery_image_classes is not None:
                forgery_key = 's' + key
                forgery_class = special_forgery_image_classes[forgery_key]
                forgeries = [[forgery_key, f] for f in list(range(len(forgery_class)))]

                references.extend([reference] * len(forgeries))
                others.extend(forgeries)
                labels.extend([FORGERY_LABEL] * len(forgeries))

    return references, others, labels


def class_train_validation_test_split(image_classes):
    keys = [*image_classes]

    keys_indices = list(range(len(keys)))
    random.shuffle(keys_indices)

    classes = (dict(), dict(), dict())
    percentages = [TRAIN_CLASSES_PERCENTAGE,
                   VALIDATION_CLASSES_PERCENTAGE,
                   TEST_CLASSES_PERCENTAGE]

    for i in range(3):
        chosen_key_indices = keys_indices[:int(len(image_classes) * percentages[i])]
        for j in range(len(chosen_key_indices)):
            key = keys[chosen_key_indices[j]]
            classes[i][key] = image_classes[key]
        keys_indices = keys_indices[len(chosen_key_indices):]

    return classes


def shuffle_pairs(pairs):
    references, others, true_outputs = pairs
    indices = list(range(len(references)))
    random.shuffle(indices)

    shuffled_references = [references[indices[i]] for i in range(len(references))]
    shuffled_others = [others[indices[i]] for i in range(len(references))]
    shuffled_true_outputs = [true_outputs[indices[i]] for i in range(len(references))]

    return shuffled_references, shuffled_others, shuffled_true_outputs


def to_float_array(np_array):
    return np.array(np_array, np.float)


class DataGenerator(Sequence):
    def __init__(self,
                 image_classes,
                 pairs,
                 special_forgery_image_classes,
                 batch_size,
                 count):
        self.batch_size = batch_size
        self.count = count
        self.image_classes = image_classes
        self.references, self.others, self.true_labels = shuffle_pairs(pairs)
        self.special_forgery_image_classes = special_forgery_image_classes

    def __len__(self):
        return math.ceil(self.count / self.batch_size)

    def __getitem__(self, index):
        references, others, true_labels = [], [], []
        for i in range(index * self.batch_size,
                       min((index + 1) * self.batch_size, len(self.references))):
            reference_pair = self.references[i]
            other_pair = self.others[i]
            reference_image = self.image_classes[reference_pair[0]][reference_pair[1]]
            key = other_pair[0]
            if key in self.image_classes:
                other_image = self.image_classes[key][other_pair[1]]
            else:
                other_image = self.special_forgery_image_classes[key][other_pair[1]]
            references.append(to_float_array(reference_image))
            others.append(to_float_array(other_image))
            true_labels.append(to_float_array(self.true_labels[i]))

        if len(references) == 0:
            return self.__getitem__(0)

        return [np.array(references), np.array(others)], np.array(true_labels)

    def on_epoch_end(self):
        self.references, self.others, self.true_labels = shuffle_pairs([self.references,
                                                                        self.others,
                                                                        self.true_labels])


def leaky_relu6(negative_slope=LEAKY_RELU_ALPHA):
    return ReLU(max_value=6, negative_slope=negative_slope)


def normalized_convolution_block(filters, kernel_size, input_layer, apply_activation=True):
    conv = Conv2D(filters,
                  (kernel_size, kernel_size),
                  padding='same',
                  use_bias=False,
                  kernel_initializer='he_normal')(input_layer)
    batch_norm = BatchNormalization()(conv)
    if not apply_activation:
        return batch_norm
    return leaky_relu6()(batch_norm)


def conv_residual_skip_connection(filters, last_layer, input_layer):
    skip_connection = normalized_convolution_block(filters, 1, input_layer, apply_activation=False)
    skip_connection = Add()([last_layer, skip_connection])
    return skip_connection


def stacked_conv_block(filters, kernel_size, input_layer, convolutions=2):
    convolution = input_layer
    for _ in range(convolutions - 1):
        convolution = normalized_convolution_block(filters, kernel_size, convolution)
    convolution = normalized_convolution_block(filters, kernel_size, convolution, apply_activation=False)

    skip_connection = conv_residual_skip_connection(filters, convolution, input_layer)
    return leaky_relu6()(skip_connection)


def embedding_net(input_shape=INPUT_SHAPE, filters=EMBEDDING_MODEL_FILTERS):
    input_layer = Input(shape=input_shape)

    output = BatchNormalization()(input_layer)
    output = Dropout(INPUT_DROPOUT_PERCENTAGE)(output)
    for i in range(len(filters) - 1):
        block = stacked_conv_block(filters[i], 3, output)
        output = MaxPooling2D(pool_size=(2, 2))(block)
    conv1 = normalized_convolution_block(filters[-1], 3, output)
    conv2 = normalized_convolution_block(filters[-1], 1, conv1)
    embedding = GlobalAveragePooling2D()(conv2)
    embedding = BatchNormalization()(embedding)

    return Model(inputs=[input_layer], outputs=[embedding])


def siamese_net(input_shape=(EMBEDDING_VECTOR_SIZE,)):
    embedding1 = Input(input_shape)
    embedding2 = Input(input_shape)

    merged1 = Lambda(absolute_difference)([embedding1, embedding2])
    merged2 = Lambda(element_wise_product)([embedding1, embedding2])
    concatenated = Concatenate()([merged1, merged2])
    embedding = BatchNormalization()(concatenated)

    fc1 = Dense(EMBEDDING_VECTOR_SIZE,
                use_bias=False,
                kernel_initializer='he_normal')(embedding)
    fc1 = BatchNormalization()(fc1)
    fc1 = leaky_relu6()(fc1)

    concat_skip_connection = Concatenate()([embedding, fc1])

    fc2 = Dense(EMBEDDING_VECTOR_SIZE,
                use_bias=False,
                kernel_initializer='he_normal')(concat_skip_connection)
    fc2 = BatchNormalization()(fc2)
    fc2 = leaky_relu6()(fc2)

    fc3 = Dense(2,
                kernel_initializer='glorot_normal',
                activation='sigmoid')(fc2)

    return Model(inputs=[embedding1, embedding2], outputs=[fc3])


def absolute_difference(tensors):
    x, y = tensors
    return tf.abs(tf.reduce_sum(tf.stack([x, -y], axis=1), axis=1))


def element_wise_product(tensors):
    x, y = tensors
    return tf.reduce_prod(tf.stack([x, -y], axis=1), axis=1)


def build_model(input_shape=INPUT_SHAPE, embedding_model=None, siamese_model=None):
    input1 = Input(shape=input_shape)
    input2 = Input(shape=input_shape)

    if embedding_model is None:
        embedding_model = embedding_net(input_shape)
    embedding1 = embedding_model(input1)
    embedding2 = embedding_model(input2)

    if siamese_model is None:
        siamese_model = siamese_net()
    prediction = siamese_model([embedding1, embedding2])

    return Model(inputs=[input1, input2], outputs=[prediction]), embedding_model, siamese_model


def binary_accuracy(y_true, y_pred, thresh=REPORT_GENUINE_THRESHOLD):
    return tf.reduce_mean(tf.cast(tf.equal(y_true > thresh, y_pred > thresh), tf.float32))


def entropy(prob_batch):
    log_prob_batch = tf.math.log(prob_batch + EPSILON)
    entropy_batch = tf.reduce_sum(prob_batch * log_prob_batch, axis=-1)
    return -1.0 * tf.reduce_mean(entropy_batch)


def confidence_regularized_binary_crossentropy(y_true,
                                               y_pred,
                                               label_smoothing_alpha=LABEL_SMOOTHING_ALPHA,
                                               confidence_penalty=CONFIDENCE_PENALTY):
    return binary_crossentropy(y_true,
                               y_pred,
                               label_smoothing=label_smoothing_alpha) - \
           confidence_penalty * entropy(y_pred)


def append_lines_to_report(lines):
    with open(EVALUATION_REPORT_FILE, 'a') as f:
        f.writelines(lines)


def epoch_metrics_log(epoch, logs):
    line = f'epoch: {epoch}'
    line += ' - '
    line += f'loss: {logs["loss"]}'
    line += ' - '
    line += f'binary accuracy: {logs["binary_accuracy"]}'
    line += ' - '
    line += f'validation loss: {logs["val_loss"]}'
    line += ' - '
    line += f'validation binary accuracy: {logs["val_binary_accuracy"]}'
    return line


class ModelLoadingMode(Enum):
    MinimumValue = 0
    MaximumValue = 1


def rename_best_model(mode=ModelLoadingMode.MaximumValue):
    all_models = list(filter(lambda x: x[-3:] == '.h5' and x[:len(MODEL_FILE_NAME)] == MODEL_FILE_NAME,
                             os.listdir(os.curdir)))
    values = []

    for model in all_models:
        value_start_index = model.rfind('_')
        value_end_index = model.rfind('.')

        if value_start_index == -1 or value_end_index == -1:
            values.append(None)
            continue

        value = model[value_start_index + 1: value_end_index]

        try:
            values.append(float(value))
        except ValueError:
            values.append(None)

    model_value_pairs = [[all_models[i], values[i]]
                         for i in range(len(all_models)) if values[i] is not None]

    model_value_pairs.sort(key=lambda x: x[1])
    if mode == ModelLoadingMode.MaximumValue:
        best_index = -1
    else:
        best_index = 0

    shutil.copy(model_value_pairs[best_index][0], MODEL_FILE_NAME + '.h5')


def train(train_image_classes,
          train_pairs,
          validation_image_classes,
          validation_pairs,
          special_forgery_image_classes):
    siamese_last_checkpoint_file = SIAMESE_MODEL_FILE_NAME + '_last.h5'
    embedding_net_last_checkpoint_file = EMBEDDING_NET_MODEL_FILE_NAME + '_last.h5'
    if os.path.isfile(siamese_last_checkpoint_file) and os.path.isfile(embedding_net_last_checkpoint_file):
        embedding_model = load_model(embedding_net_last_checkpoint_file, compile=False)
        siamese_model = load_model(siamese_last_checkpoint_file, compile=False)
        model, embedding_model, siamese_model = build_model(embedding_model=embedding_model,
                                                            siamese_model=siamese_model)
    else:
        model, embedding_model, siamese_model = build_model()

    embedding_model.summary()
    siamese_model.summary()
    model.summary()

    model.compile(loss=confidence_regularized_binary_crossentropy,
                  optimizer=SGD(lr=LEARNING_RATE,
                                decay=0.0,
                                momentum=0.9,
                                nesterov=True),
                  metrics=[binary_accuracy])

    train_data_generator = DataGenerator(train_image_classes,
                                         train_pairs,
                                         special_forgery_image_classes,
                                         TRAINING_BATCH_SIZE,
                                         len(train_pairs[0]))

    validation_data_generator = DataGenerator(validation_image_classes,
                                              validation_pairs,
                                              special_forgery_image_classes,
                                              EVALUATION_BATCH_SIZE,
                                              len(validation_pairs[0]))

    append_lines_to_report('[training log]\n\n')

    update_report_callback = LambdaCallback(
        on_epoch_end=lambda epoch, logs: append_lines_to_report(
            epoch_metrics_log(epoch, logs) + '\n'
        ))

    model_best_checkpoint_callback = ModelCheckpoint(
        filepath=MODEL_FILE_NAME + '_{epoch:02d}_{val_binary_accuracy:0.4f}.h5',
        monitor='val_binary_accuracy',
        mode='max',
        save_best_only=True)

    model_last_checkpoint_callback = ModelCheckpoint(
        filepath=MODEL_FILE_NAME + '_last.h5',
        save_best_only=False)

    siamese_best_checkpoint_callback = AltModelCheckpoint(
        filepath=SIAMESE_MODEL_FILE_NAME + '_{epoch:02d}_{val_binary_accuracy:0.4f}.h5',
        alternate_model=siamese_model,
        monitor='val_binary_accuracy',
        mode='max',
        save_best_only=True)

    siamese_last_checkpoint_callback = AltModelCheckpoint(
        alternate_model=siamese_model,
        filepath=siamese_last_checkpoint_file,
        save_best_only=False)

    embedding_net_best_checkpoint_callback = AltModelCheckpoint(
        filepath=EMBEDDING_NET_MODEL_FILE_NAME + '_{epoch:02d}_{val_binary_accuracy:0.4f}.h5',
        alternate_model=embedding_model,
        monitor='val_binary_accuracy',
        mode='max',
        save_best_only=True)

    embedding_net_last_checkpoint_callback = AltModelCheckpoint(
        alternate_model=embedding_model,
        filepath=embedding_net_last_checkpoint_file,
        save_best_only=False)

    def scheduler(epoch, lr):
        rate = math.pow(FINAL_LEARNING_RATE / INITIAL_LEARNING_RATE, 1 / EPOCHS)
        lr = INITIAL_LEARNING_RATE * math.pow(rate, epoch)
        append_lines_to_report(f'epoch: {epoch} - lr: {lr}')
        return lr

    lr_scheduler = LearningRateScheduler(scheduler)

    model.fit_generator(generator=train_data_generator,
                        steps_per_epoch=math.ceil(len(train_pairs[0]) / TRAINING_BATCH_SIZE),
                        validation_data=validation_data_generator,
                        validation_steps=math.ceil(len(validation_pairs[0]) / EVALUATION_BATCH_SIZE),
                        epochs=EPOCHS,
                        callbacks=[update_report_callback,
                                   model_best_checkpoint_callback,
                                   model_last_checkpoint_callback,
                                   siamese_best_checkpoint_callback,
                                   siamese_last_checkpoint_callback,
                                   embedding_net_best_checkpoint_callback,
                                   embedding_net_last_checkpoint_callback,
                                   lr_scheduler])

    rename_best_model()


def write_classification_report(all_true_labels, all_predicted_labels):
    accuracy = accuracy_score(all_true_labels, all_predicted_labels)
    append_lines_to_report(f'\ntest accuracy: {round(accuracy * 100, 2)}%\n')

    append_lines_to_report('\nconfusion matrix:\n')
    matrix = confusion_matrix(all_true_labels, all_predicted_labels)
    append_lines_to_report(f'{matrix}\n')

    frr = round((matrix[0, 1] / np.sum(matrix[0])) * 100, 2)
    far = round((matrix[1, 0] / np.sum(matrix[1])) * 100, 2)
    append_lines_to_report(f'\nFRR: {frr}%\n')
    append_lines_to_report(f'FAR: {far}%\n')

    append_lines_to_report('\nclassification report:\n')
    append_lines_to_report(f'{classification_report(all_true_labels, all_predicted_labels)}\n')


def draw_distributions(all_true_labels,
                       all_predicted_outputs,
                       genuine_threshold=REPORT_GENUINE_THRESHOLD,
                       bin_size=REPORT_HISTOGRAM_BIN_SIZE):
    bins = [bin_size * i / 100 for i in range(1, 100 // bin_size + 1)]

    true_labels = 1 - np.array(all_true_labels)
    predicted_outputs = np.array(all_predicted_outputs)

    genuine_outputs = predicted_outputs[true_labels > genuine_threshold]
    forgery_outputs = predicted_outputs[true_labels <= genuine_threshold]

    plt.hist(genuine_outputs, bins, color='blue', label='genuine')
    plt.legend(loc='best')
    plt.savefig('genuine_distribution.png', dpi=200)
    plt.close('all')

    plt.hist(forgery_outputs, bins, color='red', label='forgery')
    plt.legend(loc='best')
    plt.savefig('forgery_distribution.png', dpi=200)
    plt.close('all')

    plt.hist(genuine_outputs, bins, color='blue', label='genuine')
    plt.hist(forgery_outputs, bins, color='red', label='forgery')
    plt.legend(loc='best')
    plt.savefig('distributions.png', dpi=200)
    plt.close('all')


def evaluate(model,
             test_image_classes,
             test_pairs,
             special_forgery_image_classes):
    model.summary()

    model.compile(loss=confidence_regularized_binary_crossentropy,
                  optimizer=SGD(lr=LEARNING_RATE,
                                decay=0.0,
                                momentum=0.9,
                                nesterov=True),
                  metrics=[binary_accuracy])

    test_data_generator = DataGenerator(test_image_classes,
                                        test_pairs,
                                        special_forgery_image_classes,
                                        EVALUATION_BATCH_SIZE,
                                        len(test_pairs[0]))

    append_lines_to_report('\n\n[evaluation log]\n')

    results = model.evaluate_generator(generator=test_data_generator,
                                       steps=math.ceil(len(test_pairs[0]) / EVALUATION_BATCH_SIZE))

    append_lines_to_report(f'test loss: {results[0]} - test accuracy: {results[1]}\n')

    all_true_labels, all_predicted_labels, all_predicted_outputs = [], [], []
    batches = len(test_data_generator)
    progress_bar = tqdm(total=batches)
    for i in range(batches):
        input_channels, true_labels = test_data_generator[i]
        predicted_outputs = model.predict(input_channels)
        all_true_labels.extend(list(np.argmax(true_labels, axis=1)))
        all_predicted_labels.extend(list(np.argmax(predicted_outputs, axis=1)))
        all_predicted_outputs.extend(list(predicted_outputs[:, 0]))
        progress_bar.update()
    progress_bar.close()

    write_classification_report(all_true_labels, all_predicted_labels)
    draw_distributions(all_true_labels, all_predicted_outputs)


def load_or_supply_and_cache(cache_file, supplier_lambda):
    if file_exists(cache_file):
        return load_cached(cache_file)
    result = supplier_lambda()
    cache_object(result, cache_file)
    return result


def train_model(train_image_classes,
                validation_image_classes,
                special_forgery_image_classes):
    train_pairs = load_or_supply_and_cache(TRAIN_PAIRS_FILE,
                                           lambda: generate_pairs(train_image_classes,
                                                                  special_forgery_image_classes))
    validation_pairs = load_or_supply_and_cache(VALIDATION_PAIRS_FILE,
                                                lambda: generate_pairs(validation_image_classes,
                                                                       special_forgery_image_classes))

    train(train_image_classes,
          train_pairs,
          validation_image_classes,
          validation_pairs,
          special_forgery_image_classes)


def evaluate_model(test_image_classes,
                   special_forgery_image_classes):
    test_pairs = load_or_supply_and_cache(TEST_PAIRS_FILE,
                                          lambda: generate_pairs(test_image_classes,
                                                                 special_forgery_image_classes))

    model = build_model()[0]
    model.load_weights(MODEL_FILE_NAME + '.h5')
    # model = load_model(MODEL_FILE_NAME + '.h5', compile=False)

    evaluate(model,
             test_image_classes,
             test_pairs,
             special_forgery_image_classes)


def calculate_mean_images_per_class():
    train_image_classes = load_cached(TRAIN_IMAGE_CLASSES_FILE)
    validation_image_classes = load_cached(VALIDATION_IMAGE_CLASSES_FILE)
    test_image_classes = load_cached(TEST_IMAGE_CLASSES_FILE)

    total = 0
    for c in train_image_classes:
        total += len(train_image_classes[c])
    for c in validation_image_classes:
        total += len(validation_image_classes[c])
    for c in test_image_classes:
        total += len(test_image_classes[c])

    total /= (len(train_image_classes) + len(validation_image_classes) + len(test_image_classes))
    print(f"average: {total}")


def run_pipeline():
    image_classes = None
    train_image_classes = None
    validation_image_classes = None
    test_image_classes = None
    special_forgery_image_classes = None

    if not file_exists(IMAGE_CLASSES_FILE):
        image_classes = image_classes_from_paths(IMAGE_PATHS)
        cache_object(image_classes, IMAGE_CLASSES_FILE)

    if not file_exists(SPECIAL_FORGERY_IMAGE_CLASSES_FILE) and GENERATE_SPECIAL_FORGERIES:
        special_forgery_image_classes = generate_special_forgery_image_classes(IMAGE_PATHS)
        cache_object(special_forgery_image_classes, SPECIAL_FORGERY_IMAGE_CLASSES_FILE)

    if not file_exists(TRAIN_IMAGE_CLASSES_FILE) or \
            not file_exists(VALIDATION_IMAGE_CLASSES_FILE) or \
            not file_exists(TEST_IMAGE_CLASSES_FILE):
        if image_classes is None:
            image_classes = load_cached(IMAGE_CLASSES_FILE)
        train_image_classes, validation_image_classes, test_image_classes = class_train_validation_test_split(
            image_classes)

        if TRAINING_MODE == TrainingMode.TrainWithoutSplittingClasses:
            train_image_classes = image_classes

        cache_object(train_image_classes, TRAIN_IMAGE_CLASSES_FILE)
        cache_object(validation_image_classes, VALIDATION_IMAGE_CLASSES_FILE)
        cache_object(test_image_classes, TEST_IMAGE_CLASSES_FILE)

    if not file_exists(MODEL_FILE_NAME + '.h5'):
        if train_image_classes is None or \
                validation_image_classes is None:
            train_image_classes = load_cached(TRAIN_IMAGE_CLASSES_FILE)
            validation_image_classes = load_cached(VALIDATION_IMAGE_CLASSES_FILE)
        if GENERATE_SPECIAL_FORGERIES and special_forgery_image_classes is None:
            special_forgery_image_classes = load_cached(SPECIAL_FORGERY_IMAGE_CLASSES_FILE)
        train_model(train_image_classes,
                    validation_image_classes,
                    special_forgery_image_classes)

    if file_exists(MODEL_FILE_NAME + '.h5'):
        if test_image_classes is None:
            test_image_classes = load_cached(TEST_IMAGE_CLASSES_FILE)
        if GENERATE_SPECIAL_FORGERIES and special_forgery_image_classes is None:
            special_forgery_image_classes = load_cached(SPECIAL_FORGERY_IMAGE_CLASSES_FILE)
        evaluate_model(test_image_classes, special_forgery_image_classes)


if __name__ == '__main__':
    run_pipeline()
