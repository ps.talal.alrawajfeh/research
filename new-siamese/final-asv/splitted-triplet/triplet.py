import math
import os
import pickle
import random
import shutil
from enum import Enum

import numpy as np
from alt_model_checkpoint.tensorflow import AltModelCheckpoint
from tensorflow.keras import Model
from tensorflow.keras.callbacks import LambdaCallback, ModelCheckpoint, LearningRateScheduler
from tensorflow.keras.models import load_model
from tensorflow.keras.layers import Input, BatchNormalization, Dense, ReLU, Lambda, concatenate
from tensorflow.keras.optimizers import Adam, SGD
from tensorflow.keras.utils import Sequence
from matplotlib import pyplot as plt
from scipy import interpolate
import tensorflow as tf

## latest ##

CLASS_LENGTH = 9
IMAGES_PER_CLASS_THRESHOLD = 2

MODEL_FILE_NAME = 'embedding_net'
EMBEDDING_NET_FILE_NAME = 'embedding_net'
EVALUATION_REPORT_FILE = 'report.txt'
TRAIN_IMAGE_CLASSES_FILE = 'train_image_classes.pickle'
VALIDATION_IMAGE_CLASSES_FILE = 'validation_image_classes.pickle'
TEST_IMAGE_CLASSES_FILE = 'test_image_classes.pickle'
SPECIAL_FORGERY_IMAGE_CLASSES_FILE = 'special_forgery_image_classes.pickle'

TRAIN_TRIPLETS_FILE = 'train_triplets.pickle'
VALIDATION_TRIPLETS_FILE = 'validation_triplets.pickle'
TEST_TRIPLETS_FILE = 'test_triplets.pickle'

FORGERIES_PER_CLASS = 10
FORGERY_CLASSES_PER_REFERENCE = 40
USE_ALL_ANCHORS = False
NUMBER_OF_ANCHORS = 10

TRAIN_CLASSES_PERCENTAGE = 0.7
VALIDATION_CLASSES_PERCENTAGE = 0.15
TEST_CLASSES_PERCENTAGE = 0.15

INPUT_SHAPE = (200,)
EMBEDDING_LAYER_SIZE = 200
HIDDEN_LAYER_SIZE = 200
NUMBER_OF_HIDDEN_LAYERS = 2

MARGIN = 0.2
LEAKY_RELU_ALPHA = 0.05
LABEL_SMOOTHING_ALPHA = 0.1
INITIAL_LEARNING_RATE = 1e-1
FINAL_LEARNING_RATE = 1e-3
LEARNING_RATE = 1e-1
CONFIDENCE_PENALTY = 0.1
EPSILON = 1e-7

TRAINING_BATCH_SIZE = 128
EVALUATION_BATCH_SIZE = 1024
EPOCHS = 12

REPORT_GENUINE_THRESHOLD = 0.5
REPORT_HISTOGRAM_BIN_SIZE = 0.05

LIMIT_DISTANCE = 2.0
DISTANCE_MULTIPLIER = 0.01

GENERATE_SPECIAL_FORGERY_TRIPLETS = True


class TensorType(Enum):
    TF_TENSOR = 1
    NP_TENSOR = 2


def euclidean_distance(x, y, tensor_type=TensorType.TF_TENSOR):
    if tensor_type == TensorType.TF_TENSOR:
        return tf.reduce_sum(tf.square(x - y), axis=-1)
    elif tensor_type == TensorType.NP_TENSOR:
        return np.sqrt(np.sum(np.square(x - y), axis=-1))
    raise Exception(f'not implemented for tensor_type={tensor_type}')


DISTANCE_FUNCTION = euclidean_distance


class TrainingMode(Enum):
    TrainWithSplittingClasses = 1
    TrainWithoutSplittingClasses = 2


TRAINING_MODE = TrainingMode.TrainWithoutSplittingClasses


def leaky_relu6(negative_slope=LEAKY_RELU_ALPHA):
    return ReLU(max_value=6, negative_slope=negative_slope)


def embedding_network(features_shape=INPUT_SHAPE):
    features = Input(features_shape)
    embedding = BatchNormalization()(features)

    for _ in range(NUMBER_OF_HIDDEN_LAYERS):
        embedding = Dense(HIDDEN_LAYER_SIZE,
                          kernel_initializer='he_normal',
                          # kernel_regularizer=l2(1e-4),
                          use_bias=False)(embedding)
        embedding = BatchNormalization()(embedding)
        embedding = leaky_relu6()(embedding)

    embedding = Dense(EMBEDDING_LAYER_SIZE,
                      kernel_initializer='he_normal',
                      # kernel_regularizer=l2(1e-4),
                      use_bias=False)(embedding)
    embedding = leaky_relu6()(embedding)
    embedding = Lambda(lambda x: tf.nn.l2_normalize(x, axis=-1))(embedding)

    return Model(inputs=features, outputs=embedding)


def training_wrapped_model(features_shape=INPUT_SHAPE,
                           embedding_net=embedding_network()):
    anchor = Input(features_shape)
    positive = Input(features_shape)
    negative = Input(features_shape)

    anchor_embedding = embedding_net(anchor)
    positive_embedding = embedding_net(positive)
    negative_embedding = embedding_net(negative)

    merged_vector = concatenate(
        [anchor_embedding,
         positive_embedding,
         negative_embedding], axis=-1)

    return Model(inputs=[anchor, positive, negative], outputs=merged_vector)


def squared_euclidean_distance(x, y):
    return tf.reduce_sum(tf.square(x - y), axis=-1)


def triplet_loss(_, y_pred, margin=MARGIN):
    total_length = y_pred.shape.as_list()[-1]

    anchor = y_pred[:, 0:int(total_length * 1 / 3)]
    positive = y_pred[:, int(total_length * 1 / 3):int(total_length * 2 / 3)]
    negative = y_pred[:, int(total_length * 2 / 3):int(total_length * 3 / 3)]

    positive_dist = squared_euclidean_distance(anchor, positive)
    negative_dist = squared_euclidean_distance(anchor, negative)

    return tf.reduce_mean(tf.maximum(positive_dist - negative_dist + margin, 0.0))


def accuracy(_, y_pred, margin=MARGIN):
    anchor = y_pred[:, 0:EMBEDDING_LAYER_SIZE]
    positive = y_pred[:, EMBEDDING_LAYER_SIZE: EMBEDDING_LAYER_SIZE * 2]
    negative = y_pred[:, EMBEDDING_LAYER_SIZE * 2: EMBEDDING_LAYER_SIZE * 3]

    positive_dist = squared_euclidean_distance(anchor, positive)
    negative_dist = squared_euclidean_distance(anchor, negative)

    return tf.reduce_mean(tf.cast(positive_dist + margin < negative_dist, tf.dtypes.float32))


def cache_object(obj, file):
    with open(file, 'wb') as f:
        pickle.dump(obj, f)


def load_cached(file):
    with open(file, 'rb') as f:
        return pickle.load(f)


def file_exists(file):
    return os.path.isfile(file)


class MinimumRecurrenceRateRandomSamplerWithReplacement:
    def __init__(self, array) -> None:
        self.array = array
        self.left_choices = array.copy()

    def next(self):
        if len(self.left_choices) == 0:
            self.left_choices = self.array.copy()
        i = random.randint(0, len(self.left_choices) - 1)
        return self.left_choices.pop(i)


# generates lists of anchors, positives, and negatives that represent triplets to train the triplet siamese model
def generate_triplets(classes,
                      special_forgery_classes,
                      use_all_anchors=USE_ALL_ANCHORS,
                      number_of_anchors=NUMBER_OF_ANCHORS,
                      forgery_classes_per_reference=FORGERY_CLASSES_PER_REFERENCE,
                      forgeries_per_class=FORGERIES_PER_CLASS):
    keys = [*classes]
    number_of_classes = len(keys)

    anchors = []
    positives = []
    negatives = []

    for c in range(number_of_classes):
        key = keys[c]
        keys_except_key = [k for k in keys if k != key]

        if use_all_anchors:
            number_of_anchors = len(classes[key])
        else:
            number_of_anchors = min(number_of_anchors, len(classes[key]))

        anchor_indices_sampler = MinimumRecurrenceRateRandomSamplerWithReplacement(list(range(len(classes[key]))))

        for r in range(number_of_anchors):
            anchor_index = anchor_indices_sampler.next()
            reference = [key, anchor_index]
            genuine_indices = list(range(len(classes[key])))
            genuine_indices.pop(anchor_index)
            genuine_indices_sampler = MinimumRecurrenceRateRandomSamplerWithReplacement(genuine_indices)
            forgery_keys = random.sample(keys_except_key, min(forgery_classes_per_reference, len(keys_except_key)))

            for forgery_key in forgery_keys:
                forgery_class = classes[forgery_key]
                forgery_indices_sampler = MinimumRecurrenceRateRandomSamplerWithReplacement(
                    list(range(len(forgery_class))))
                forgeries = [[forgery_key, forgery_indices_sampler.next()] for _ in
                             range(min(forgeries_per_class, len(forgery_class)))]

                anchors.extend([reference] * len(forgeries))
                positives.extend([[key, genuine_indices_sampler.next()] for _ in range(len(forgeries))])
                negatives.extend(forgeries)

        if GENERATE_SPECIAL_FORGERY_TRIPLETS:
            forgery_key = 's' + key
            forgery_class = special_forgery_classes[forgery_key]
            forgeries = [[forgery_key, f] for f in list(range(len(forgery_class)))]
            anchors.extend([[key, anchor_indices_sampler.next()] for _ in range(len(forgeries))])
            genuine_indices = list(range(len(classes[key])))
            genuine_indices_sampler = MinimumRecurrenceRateRandomSamplerWithReplacement(genuine_indices)
            positives.extend([[key, genuine_indices_sampler.next()] for _ in range(len(forgeries))])
            negatives.extend(forgeries)

    return anchors, positives, negatives


def shuffle_triplets(triplets):
    anchors, positives, negatives = triplets
    indices = list(range(len(anchors)))
    random.shuffle(indices)

    shuffled_anchors = [anchors[indices[i]] for i in range(len(anchors))]
    shuffled_positives = [positives[indices[i]] for i in range(len(anchors))]
    shuffled_true_negatives = [negatives[indices[i]] for i in range(len(anchors))]

    return shuffled_anchors, shuffled_positives, shuffled_true_negatives


class DataGenerator(Sequence):
    def __init__(self,
                 image_classes,
                 triplets,
                 special_forgery_classes,
                 batch_size,
                 count):
        self.batch_size = batch_size
        self.count = count
        self.image_classes = image_classes
        self.special_forgery_classes = special_forgery_classes
        self.anchors, self.positives, self.negatives = shuffle_triplets(triplets)
        self.true_output = np.array([[0] for _ in range(batch_size)])

    def __len__(self):
        return math.ceil(self.count / self.batch_size)

    def __getitem__(self, index):
        anchors, positives, negatives = [], [], []
        for i in range(index * self.batch_size, min((index + 1) * self.batch_size, len(self.anchors))):
            anchor_pair = self.anchors[i]
            positive_pair = self.positives[i]
            negative_pair = self.negatives[i]
            anchors.append(self.image_classes[anchor_pair[0]][anchor_pair[1]])
            positives.append(self.image_classes[positive_pair[0]][positive_pair[1]])

            key = negative_pair[0]
            if key in self.image_classes:
                negatives.append(self.image_classes[key][negative_pair[1]])
            else:
                negatives.append(self.special_forgery_classes[key][negative_pair[1]])

        if len(anchors) == 0:
            return self.__getitem__(0)

        true_output = self.true_output
        if len(anchors) != self.batch_size:
            true_output = np.array([[0] for _ in range(len(anchors))])

        return [np.array(anchors),
                np.array(positives),
                np.array(negatives)], true_output

    def on_epoch_end(self):
        self.anchors, self.positives, self.negatives = shuffle_triplets([self.anchors, self.positives, self.negatives])


def class_train_validation_test_split(image_classes):
    keys = [*image_classes]

    keys_indices = list(range(len(keys)))
    random.shuffle(keys_indices)

    classes = (dict(), dict(), dict())
    percentages = [TRAIN_CLASSES_PERCENTAGE,
                   VALIDATION_CLASSES_PERCENTAGE,
                   TEST_CLASSES_PERCENTAGE]

    for i in range(3):
        chosen_key_indices = keys_indices[:int(len(image_classes) * percentages[i])]
        for j in range(len(chosen_key_indices)):
            key = keys[chosen_key_indices[j]]
            classes[i][key] = image_classes[key]
        keys_indices = keys_indices[len(chosen_key_indices):]

    return classes


def append_lines_to_report(lines):
    with open(EVALUATION_REPORT_FILE, 'a') as f:
        f.writelines(lines)


def epoch_metrics_log(epoch, logs):
    line = f'epoch: {epoch}'
    line += ' - '
    line += f'loss: {logs["loss"]}'
    line += ' - '
    line += f'binary accuracy: {logs["accuracy"]}'
    line += ' - '
    line += f'validation loss: {logs["val_loss"]}'
    line += ' - '
    line += f'validation binary accuracy: {logs["val_accuracy"]}'
    return line


class ModelLoadingMode(Enum):
    MinimumValue = 0
    MaximumValue = 1


def rename_best_model(mode=ModelLoadingMode.MaximumValue):
    all_models = list(filter(lambda x: x[-3:] == '.h5',
                             os.listdir(os.curdir)))
    values = []

    for model in all_models:
        value_start_index = model.rfind('_')
        value_end_index = model.rfind('.')

        if value_start_index == -1 or value_end_index == -1:
            values.append(None)
            continue

        value = model[value_start_index + 1: value_end_index]

        try:
            values.append(float(value))
        except ValueError:
            values.append(None)

    model_value_pairs = [[all_models[i], values[i]]
                         for i in range(len(all_models)) if values[i] is not None]

    model_value_pairs.sort(key=lambda x: x[1])
    if mode == ModelLoadingMode.MaximumValue:
        best_index = -1
    else:
        best_index = 0

    shutil.copy(model_value_pairs[best_index][0], MODEL_FILE_NAME + '.h5')


def train(train_image_classes,
          train_triplets,
          validation_image_classes,
          validation_triplets,
          special_forgery_image_classes):
    last_checkpoint_file = MODEL_FILE_NAME + '_last.h5'
    # if os.path.isfile(last_checkpoint_file):
    #     model = load_model(last_checkpoint_file, compile=False)
    # else:
    embedding_net = embedding_network()
    model = training_wrapped_model(embedding_net=embedding_net)

    model.summary()

    model.compile(loss=triplet_loss,
                  optimizer=SGD(lr=LEARNING_RATE,
                                decay=0.0,
                                momentum=0.9,
                                nesterov=True),
                  metrics=[accuracy])

    train_data_generator = DataGenerator(train_image_classes,
                                         train_triplets,
                                         special_forgery_image_classes,
                                         TRAINING_BATCH_SIZE,
                                         len(train_triplets[0]))

    validation_data_generator = DataGenerator(validation_image_classes,
                                              validation_triplets,
                                              special_forgery_image_classes,
                                              EVALUATION_BATCH_SIZE,
                                              len(validation_triplets[0]))

    append_lines_to_report('[training log]\n\n')

    update_report_callback = LambdaCallback(
        on_epoch_end=lambda epoch, logs: append_lines_to_report(
            epoch_metrics_log(epoch, logs) + '\n'
        ))

    best_checkpoint_callback = ModelCheckpoint(
        filepath=MODEL_FILE_NAME + '_{epoch:02d}_{val_accuracy:0.4f}.h5',
        monitor='val_accuracy',
        mode='max',
        save_best_only=True)

    en_last_model_checkpoint = AltModelCheckpoint(EMBEDDING_NET_FILE_NAME + '_checkpoint_last.h5',
                                                  embedding_net)
    en_best_model_checkpoint = AltModelCheckpoint(
        EMBEDDING_NET_FILE_NAME + '_checkpoint_{epoch:02d}_{val_accuracy:0.4f}.h5',
        embedding_net,
        monitor='val_accuracy',
        save_best_only=True,
        mode='max')

    last_checkpoint_callback = ModelCheckpoint(
        filepath=last_checkpoint_file,
        save_best_only=False)

    def scheduler(epoch, lr):
        rate = math.pow(FINAL_LEARNING_RATE / INITIAL_LEARNING_RATE, 1 / EPOCHS)
        lr = INITIAL_LEARNING_RATE * math.pow(rate, epoch)
        append_lines_to_report(f'epoch: {epoch} - lr: {lr}\n')
        return lr

    lr_scheduler = LearningRateScheduler(scheduler)

    model.fit_generator(generator=train_data_generator,
                        steps_per_epoch=math.ceil(len(train_triplets[0]) / TRAINING_BATCH_SIZE),
                        validation_data=validation_data_generator,
                        validation_steps=math.ceil(len(validation_triplets[0]) / EVALUATION_BATCH_SIZE),
                        epochs=EPOCHS,
                        callbacks=[update_report_callback,
                                   best_checkpoint_callback,
                                   last_checkpoint_callback,
                                   en_last_model_checkpoint,
                                   en_best_model_checkpoint,
                                   lr_scheduler])

    rename_best_model()


def load_or_supply_and_cache(cache_file, supplier_lambda):
    if file_exists(cache_file):
        return load_cached(cache_file)
    result = supplier_lambda()
    cache_object(result, cache_file)
    return result


def train_model(train_image_classes,
                validation_image_classes,
                special_forgery_image_classes):
    train_pairs = load_or_supply_and_cache(TRAIN_TRIPLETS_FILE,
                                           lambda: generate_triplets(train_image_classes,
                                                                     special_forgery_image_classes))
    validation_pairs = load_or_supply_and_cache(VALIDATION_TRIPLETS_FILE,
                                                lambda: generate_triplets(validation_image_classes,
                                                                          special_forgery_image_classes))

    train(train_image_classes,
          train_pairs,
          validation_image_classes,
          validation_pairs,
          special_forgery_image_classes)


def evaluate_model(test_image_classes,
                   special_forgery_classes,
                   test_triplets):
    #embedding_net = load_model(EMBEDDING_NET_FILE_NAME + '.h5', compile=False)
    embedding_net = embedding_network()
    embedding_net.load_weights(EMBEDDING_NET_FILE_NAME + '.h5')
    model = training_wrapped_model(features_shape=INPUT_SHAPE,
                                   embedding_net=embedding_net)
    model.summary()

    model.compile(loss=triplet_loss,
                  optimizer=SGD(lr=LEARNING_RATE,
                                decay=0.0,
                                momentum=0.9,
                                nesterov=True),
                  metrics=[accuracy])

    test_data_generator = DataGenerator(test_image_classes,
                                        test_triplets,
                                        special_forgery_classes,
                                        EVALUATION_BATCH_SIZE,
                                        len(test_triplets[0]))

    append_lines_to_report('======= evaluation results =======\n')

    results = model.evaluate_generator(generator=test_data_generator,
                                       steps=math.ceil(len(test_triplets[0]) / EVALUATION_BATCH_SIZE),
                                       use_multiprocessing=False)

    append_lines_to_report(f'test loss: {results[0]} - test accuracy: {results[1]}\n\n')

    return embedding_net


def draw_distributions(all_forgery_distances, all_genuine_distances):
    bins = [DISTANCE_MULTIPLIER * i for i in range(int(LIMIT_DISTANCE / DISTANCE_MULTIPLIER) + 1)]

    plt.hist(all_genuine_distances, bins, color='blue', label='genuine')
    plt.legend(loc='best')
    plt.savefig('genuine_distribution.png', dpi=200)
    plt.close('all')

    plt.hist(all_forgery_distances, bins, color='red', label='forgery')
    plt.legend(loc='best')
    plt.savefig('forgery_distribution.png', dpi=200)
    plt.close('all')

    plt.hist(all_genuine_distances, bins, color='blue', label='genuine')
    plt.hist(all_forgery_distances, bins, color='red', label='forgery')
    plt.legend(loc='best')
    plt.savefig('distributions.png', dpi=200)
    plt.close('all')


def write_similarities_report(all_forgery_distances, all_genuine_distances):
    genuine_distances = np.array(all_genuine_distances)
    forgery_distances = np.array(all_forgery_distances)

    bins = [DISTANCE_MULTIPLIER * i for i in range(int(LIMIT_DISTANCE / DISTANCE_MULTIPLIER) + 1)]
    genuine_distances_hist, _ = np.histogram(genuine_distances, bins)
    forgery_distances_hist, _ = np.histogram(forgery_distances, bins)

    max_genuine_hist = np.argmax(genuine_distances_hist[len(genuine_distances_hist) // 4:])
    max_forgery_hist = np.argmax(forgery_distances_hist)
    equal_on_distance = (max_genuine_hist + np.argmin(np.abs(genuine_distances_hist[max_genuine_hist:max_forgery_hist] -
                                                             forgery_distances_hist[
                                                             max_genuine_hist:max_forgery_hist]))) / 100
    append_lines_to_report(f'equal on distance: {equal_on_distance}\n')

    mapper = interpolate.interp1d([0.0, equal_on_distance, 2.0],
                                  [1.0, 0.5, 0.0])

    genuine_distances[genuine_distances < 0] = 0
    genuine_distances[genuine_distances > 2.0] = 2.0

    forgery_distances[forgery_distances < 0] = 0
    forgery_distances[forgery_distances > 2.0] = 2.0

    genuine_similarities = mapper(genuine_distances)
    forgery_similarities = mapper(forgery_distances)
    # in every bin the counts are done as follows:
    # x >= left_inclusive and x < right_exclusive but the last
    bins = [i * REPORT_HISTOGRAM_BIN_SIZE
            for i in range(int(1.0 / REPORT_HISTOGRAM_BIN_SIZE) + 1)]

    genuine_hist, _ = np.histogram(genuine_similarities, bins)
    forgery_hist, _ = np.histogram(forgery_similarities, bins)

    current_frr = 0
    current_far = len(forgery_similarities)
    thresholds = [0.0]
    frr_array = [0.0]
    far_array = [100.0]

    for i in range(1, len(bins) - 1):
        current_frr += genuine_hist[i]
        current_far -= forgery_hist[i]

        thresholds.append(bins[i] * 100)
        frr_array.append(current_frr / len(genuine_similarities) * 100)
        far_array.append(current_far / len(forgery_similarities) * 100)

    append_lines_to_report(['\nthresh\t\tfrr\t\tfar\n'])
    append_lines_to_report(['~~~~~~~~~~~~~~~~~~~~~\n'])

    for i in range(len(thresholds)):
        append_lines_to_report(f'{round(thresholds[i], 2)}\t\t{round(frr_array[i], 2)}\t\t{round(far_array[i], 2)}\n')


def evaluate_model_and_generate_reports(test_image_classes,
                                        special_forgery_classes,
                                        test_triplets,
                                        distance_function=lambda x, y: DISTANCE_FUNCTION(x, y, TensorType.NP_TENSOR)):
    embedding_net = evaluate_model(test_image_classes,
                                   special_forgery_classes,
                                   test_triplets)

    test_data_generator = DataGenerator(test_image_classes,
                                        test_triplets,
                                        special_forgery_classes,
                                        EVALUATION_BATCH_SIZE,
                                        len(test_triplets[0]))

    all_genuine_distances = []
    all_forgery_distances = []

    for i in range(len(test_data_generator)):
        [anchors, positives, negatives], _ = test_data_generator[i]

        anchor_embeddings = embedding_net.predict(anchors)
        positive_embeddings = embedding_net.predict(positives)
        negative_embeddings = embedding_net.predict(negatives)

        genuine_batch = distance_function(anchor_embeddings, positive_embeddings)
        forgery_batch = distance_function(anchor_embeddings, negative_embeddings)

        all_genuine_distances.extend(list(genuine_batch))
        all_forgery_distances.extend(list(forgery_batch))

    draw_distributions(all_forgery_distances, all_genuine_distances)
    write_similarities_report(all_forgery_distances, all_genuine_distances)


def prepare_data():
    train_image_classes = load_cached(TRAIN_IMAGE_CLASSES_FILE)
    validation_image_classes = load_cached(VALIDATION_IMAGE_CLASSES_FILE)
    test_image_classes = load_cached(TEST_IMAGE_CLASSES_FILE)
    special_forgery_classes = load_cached(SPECIAL_FORGERY_IMAGE_CLASSES_FILE)

    train_triplets = generate_triplets(train_image_classes, special_forgery_classes)
    validation_triplets = generate_triplets(validation_image_classes, special_forgery_classes)
    test_triplets = generate_triplets(test_image_classes, special_forgery_classes)

    del train_image_classes
    del validation_image_classes
    del test_image_classes

    cache_object(train_triplets, TRAIN_TRIPLETS_FILE)
    cache_object(validation_triplets, VALIDATION_TRIPLETS_FILE)
    cache_object(test_triplets, TEST_TRIPLETS_FILE)

    del special_forgery_classes
    del train_triplets
    del validation_triplets
    del test_triplets


def run_pipeline():
    if not os.path.isfile(TRAIN_TRIPLETS_FILE) or \
            not os.path.isfile(VALIDATION_TRIPLETS_FILE) or \
            not os.path.isfile(TEST_TRIPLETS_FILE):
        prepare_data()

    # ------------------------------- train model -------------------------------- #
    if not os.path.isfile(os.path.join(os.path.curdir, MODEL_FILE_NAME + '.h5')):
        train_triplets = load_cached(TRAIN_TRIPLETS_FILE)
        validation_triplets = load_cached(VALIDATION_TRIPLETS_FILE)

        train_image_classes = load_cached(TRAIN_IMAGE_CLASSES_FILE)
        validation_image_classes = load_cached(VALIDATION_IMAGE_CLASSES_FILE)

        special_forgery_classes = load_cached(SPECIAL_FORGERY_IMAGE_CLASSES_FILE)
        train(train_image_classes,
              train_triplets,
              validation_image_classes,
              validation_triplets,
              special_forgery_classes)

        del train_triplets
        del validation_triplets

    # ---------------- evaluate model on the test feature vectors ---------------- #
    special_forgery_classes = load_cached(SPECIAL_FORGERY_IMAGE_CLASSES_FILE)
    evaluate_model_and_generate_reports(load_cached(TEST_IMAGE_CLASSES_FILE),
                                        special_forgery_classes,
                                        load_cached(TEST_TRIPLETS_FILE))


if __name__ == '__main__':
    run_pipeline()
