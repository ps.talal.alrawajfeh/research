import os

import tensorflow as tf
import tensorflow.python.keras.backend as K
from keras import Model, Input
from keras.engine.saving import load_model
from keras.layers import ReLU, Lambda, Dense, BatchNormalization
from tensorflow.python.framework.graph_util import convert_variables_to_constants

LEAKY_RELU_ALPHA = 0.05
INPUT_SHAPE = (200,)
EMBEDDING_LAYER_SIZE = 200
HIDDEN_LAYER_SIZE = 200
NUMBER_OF_HIDDEN_LAYERS = 2


def freeze_session(session, keep_var_names=None, output_names=None, clear_devices=True):
    graph = session.graph
    with graph.as_default():
        freeze_var_names = list(set(v.op.name for v in tf.global_variables()).difference(keep_var_names or []))
        output_names = output_names or []
        output_names += [v.op.name for v in tf.global_variables()]
        input_graph_def = graph.as_graph_def()
        if clear_devices:
            for node in input_graph_def.node:
                node.device = ''
        frozen_graph = convert_variables_to_constants(session,
                                                      input_graph_def,
                                                      output_names,
                                                      freeze_var_names)
        return frozen_graph


def leaky_relu6(negative_slope=LEAKY_RELU_ALPHA):
    return ReLU(max_value=6, negative_slope=negative_slope)


def embedding_network(features_shape=INPUT_SHAPE):
    features = Input(features_shape)
    embedding = BatchNormalization()(features)

    for _ in range(NUMBER_OF_HIDDEN_LAYERS):
        embedding = Dense(HIDDEN_LAYER_SIZE,
                          kernel_initializer='he_normal',
                          # kernel_regularizer=l2(1e-4),
                          use_bias=False)(embedding)
        embedding = BatchNormalization()(embedding)
        embedding = leaky_relu6()(embedding)

    embedding = Dense(EMBEDDING_LAYER_SIZE,
                      kernel_initializer='he_normal',
                      # kernel_regularizer=l2(1e-4),
                      use_bias=False)(embedding)
    embedding = leaky_relu6()(embedding)
    embedding = Lambda(lambda x: tf.nn.l2_normalize(x, axis=-1))(embedding)

    return Model(inputs=features, outputs=embedding)


if __name__ == '__main__':
    input_path = '/home/u764/Development/progressoft/research/new-siamese/final-asv/full-triplet/embedding_net_checkpoint_11_0.9998.h5'
    output_path = '/home/u764/Development/progressoft/research/new-siamese/final-asv/full-triplet/triplet_net.pb'

    K.set_learning_phase(0)
    # model = load_model(input_path, compile=False)
    model = embedding_network()
    model.load_weights(input_path)

    tf.train.write_graph(freeze_session(K.get_session(),
                                        output_names=[out.op.name for out in model.outputs]),
                         os.path.dirname(output_path),
                         os.path.basename(output_path),
                         as_text=False)
