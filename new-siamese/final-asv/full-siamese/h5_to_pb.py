import os

import tensorflow as tf
import tensorflow.python.keras.backend as K
from keras import Input, Model
from keras.layers import BatchNormalization, Dropout, ReLU, Conv2D, Add, MaxPooling2D, GlobalAveragePooling2D, Lambda, \
    Concatenate, Dense
from tensorflow.python.framework.graph_util import convert_variables_to_constants

INPUT_SHAPE = (96, 144, 1)
EMBEDDING_MODEL_FILTERS = (30, 60, 100, 150, 200)
EMBEDDING_VECTOR_SIZE = 200
INPUT_DROPOUT_PERCENTAGE = 0.05
LEAKY_RELU_ALPHA = 0.05


def freeze_session(session, keep_var_names=None, output_names=None, clear_devices=True):
    graph = session.graph
    with graph.as_default():
        freeze_var_names = list(set(v.op.name for v in tf.global_variables()).difference(keep_var_names or []))
        output_names = output_names or []
        output_names += [v.op.name for v in tf.global_variables()]
        input_graph_def = graph.as_graph_def()
        if clear_devices:
            for node in input_graph_def.node:
                node.device = ''
        frozen_graph = convert_variables_to_constants(session,
                                                      input_graph_def,
                                                      output_names,
                                                      freeze_var_names)
        return frozen_graph


def leaky_relu6(negative_slope=LEAKY_RELU_ALPHA):
    return ReLU(max_value=6, negative_slope=negative_slope)


def normalized_convolution_block(filters, kernel_size, input_layer, apply_activation=True):
    conv = Conv2D(filters,
                  (kernel_size, kernel_size),
                  padding='same',
                  use_bias=False,
                  kernel_initializer='he_normal')(input_layer)
    batch_norm = BatchNormalization()(conv)
    if not apply_activation:
        return batch_norm
    return leaky_relu6()(batch_norm)


def conv_residual_skip_connection(filters, last_layer, input_layer):
    skip_connection = normalized_convolution_block(filters, 1, input_layer, apply_activation=False)
    skip_connection = Add()([last_layer, skip_connection])
    return skip_connection


def stacked_conv_block(filters, kernel_size, input_layer, convolutions=2):
    convolution = input_layer
    for _ in range(convolutions - 1):
        convolution = normalized_convolution_block(filters, kernel_size, convolution)
    convolution = normalized_convolution_block(filters, kernel_size, convolution, apply_activation=False)

    skip_connection = conv_residual_skip_connection(filters, convolution, input_layer)
    return leaky_relu6()(skip_connection)


def embedding_net(input_shape=INPUT_SHAPE, filters=EMBEDDING_MODEL_FILTERS):
    input_layer = Input(shape=input_shape)

    output = BatchNormalization()(input_layer)
    output = Dropout(INPUT_DROPOUT_PERCENTAGE)(output)
    for i in range(len(filters) - 1):
        block = stacked_conv_block(filters[i], 3, output)
        output = MaxPooling2D(pool_size=(2, 2))(block)
    conv1 = normalized_convolution_block(filters[-1], 3, output)
    conv2 = normalized_convolution_block(filters[-1], 1, conv1)
    embedding = GlobalAveragePooling2D()(conv2)
    embedding = BatchNormalization()(embedding)

    return Model(inputs=[input_layer], outputs=[embedding])


def absolute_difference(tensors):
    x, y = tensors
    return tf.abs(tf.reduce_sum(tf.stack([x, -y], axis=1), axis=1))


def element_wise_product(tensors):
    x, y = tensors
    return tf.reduce_prod(tf.stack([x, -y], axis=1), axis=1)


def siamese_net(input_shape=(EMBEDDING_VECTOR_SIZE,)):
    embedding1 = Input(input_shape)
    embedding2 = Input(input_shape)

    merged1 = Lambda(absolute_difference)([embedding1, embedding2])
    merged2 = Lambda(element_wise_product)([embedding1, embedding2])
    concatenated = Concatenate()([merged1, merged2])
    embedding = BatchNormalization()(concatenated)

    fc1 = Dense(EMBEDDING_VECTOR_SIZE,
                use_bias=False,
                kernel_initializer='he_normal')(embedding)
    fc1 = BatchNormalization()(fc1)
    fc1 = leaky_relu6()(fc1)

    concat_skip_connection = Concatenate()([embedding, fc1])

    fc2 = Dense(EMBEDDING_VECTOR_SIZE,
                use_bias=False,
                kernel_initializer='he_normal')(concat_skip_connection)
    fc2 = BatchNormalization()(fc2)
    fc2 = leaky_relu6()(fc2)

    fc3 = Dense(2,
                kernel_initializer='glorot_normal',
                activation='sigmoid')(fc2)

    return Model(inputs=[embedding1, embedding2], outputs=[fc3])


if __name__ == '__main__':
    input_path = '/home/u764/Development/progressoft/research/new-siamese/final-asv/full-siamese/siamese_06_0.9997.h5'
    output_path = '/home/u764/Development/progressoft/research/new-siamese/final-asv/full-siamese/siamese.pb'

    K.set_learning_phase(0)
    # model = load_model(input_path, compile=False)
    model = siamese_net()
    model.load_weights(input_path)

    tf.train.write_graph(freeze_session(K.get_session(),
                                        output_names=[out.op.name for out in model.outputs]),
                         os.path.dirname(output_path),
                         os.path.basename(output_path),
                         as_text=False)
