import gzip
import itertools
import mimetypes
import multiprocessing
import os
import pickle
import random
from concurrent.futures import ThreadPoolExecutor
from multiprocessing import Lock

import cv2
import numpy as np
from PIL import Image, ImageDraw, ImageFont
from scipy.interpolate import interp1d
from tqdm import tqdm

GREY_BACKGROUNDS_DIR = '/home/u764/Downloads/cheques'
SIGNATURES_DIR = '/home/u764/Development/data/signatures'

INPUT_IMAGES_PATH = './input'
OUTPUT_IMAGES_PATH = './output'
DATASET_SIZE = 1000
INPUT_SHAPE = (350, 710)
SIGNATURE_AREA_SHAPE = (224, 416)
MAX_SIGNATURE_RECT_INTERSECTION = 0.25


def to_int(number):
    return int(round(number))


def get_extensions_for_type(general_type):
    for ext in mimetypes.types_map:
        if mimetypes.types_map[ext].split('/')[0] == general_type:
            yield ext.lower()


def memoize(function):
    memo = {}

    def wrapper(*args):
        if args in memo:
            return memo[args]
        else:
            rv = function(*args)
            memo[args] = rv
            return rv

    return wrapper


@memoize
def get_image_extensions():
    return tuple(get_extensions_for_type('image'))


def file_extension(path):
    return os.path.splitext(os.path.basename(path))[1].lower()


def is_image(path):
    return file_extension(path) in get_image_extensions()


def read_image_grayscale(image_path):
    return cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)


def threshold_otsu(image):
    return cv2.threshold(image,
                         0,
                         255,
                         cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]


def correct_image_values(image):
    image[image > 255] = 255
    image[image < 0] = 0


def change_brightness(image, delta):
    result = image.astype(np.int32)
    result += delta
    correct_image_values(result)
    return result.astype(np.uint8)


def change_contrast(image, coefficient):
    result = image.astype(np.float64)
    result *= coefficient
    correct_image_values(result)
    return result.astype(np.uint8)


def rotate(image, angle):
    (height, width) = image.shape[:2]
    center_x, center_y = width / 2, height / 2
    rotation_matrix = cv2.getRotationMatrix2D((center_x, center_y),
                                              angle,
                                              1.0)
    cos = np.abs(rotation_matrix[0, 0])
    sin = np.abs(rotation_matrix[0, 1])
    new_width = height * sin + width * cos
    new_height = height * cos + width * sin
    rotation_matrix[0, 2] += new_width / 2 - center_x
    rotation_matrix[1, 2] += new_height / 2 - center_y
    return cv2.warpAffine(image,
                          rotation_matrix,
                          (to_int(new_width), to_int(new_height)),
                          borderValue=(255, 255, 255),
                          flags=cv2.INTER_CUBIC)


def compress_image(image, quality):
    encoded = cv2.imencode('.jpg', image, [int(cv2.IMWRITE_JPEG_QUALITY), quality])[1]
    return cv2.imdecode(encoded, cv2.IMREAD_GRAYSCALE)


def random_coordinate(image):
    return random.randint(0, image.shape[0] - 1), random.randint(0, image.shape[1] - 1)


def speckle_image(image, fraction, color=0):
    coordinates = [random_coordinate(image)
                   for _ in range(to_int(image.size * fraction))]
    image_copy = np.array(image)
    for coord in coordinates:
        image_copy[coord[0], coord[1]] = color
    return image_copy


def crop_foreground(binary_image):
    mask = (255 - binary_image) > 0

    height, width = binary_image.shape[0:2]
    mask1, mask2 = mask.any(0), mask.any(1)
    x1, x2 = mask1.argmax(), width - mask1[::-1].argmax()
    y1, y2 = mask2.argmax(), height - mask2[::-1].argmax()

    return binary_image[y1:y2, x1:x2]


def crop_foreground_non_binary(image, threshold=None):
    if threshold is None:
        binary_image = threshold_otsu(image)
    else:
        binary_image = np.array(image)
        binary_image[binary_image < threshold] = 0
        binary_image[binary_image >= threshold] = 255

    mask = (255 - binary_image) > 0

    height, width = binary_image.shape[0:2]
    mask1, mask2 = mask.any(0), mask.any(1)
    x1, x2 = mask1.argmax(), width - mask1[::-1].argmax()
    y1, y2 = mask2.argmax(), height - mask2[::-1].argmax()

    return image[y1:y2, x1:x2]


def random_bool():
    return random.choice([0, 1]) == 0


def random_blur(image):
    if random_bool():
        return image

    blur_lambdas = [lambda img: cv2.blur(img, (3, 3)),
                    lambda img: cv2.medianBlur(img, 3),
                    lambda img: cv2.GaussianBlur(img, (3, 3), 0)]

    return random.choice(blur_lambdas)(image)


def random_add_gaussian_noise(image, mean=0, variance=1, interval=None):
    if random_bool():
        return image
    width, height = image.shape
    gaussian = np.random.normal(mean,
                                variance,
                                (width, height)).astype(np.float64)
    if interval is not None:
        gaussian[gaussian < interval[0]] = interval[0]
        gaussian[gaussian > interval[1]] = interval[1]
    gaussian = image + gaussian
    gaussian[gaussian > 255] = 255
    gaussian[gaussian < 0] = 0
    return gaussian.astype(np.uint8)


def randomize_foreground(image,
                         min_color,
                         max_color):
    binary = np.array(image)
    binary[binary < 255] = 0
    mask = (binary == 255)
    result = np.random.randint(min_color, max_color, image.shape).astype(np.uint8)
    result[mask] = 255
    return result


def insert_foreground(image,
                      foreground,
                      position):
    x, y = position
    image_part = image[y:y + foreground.shape[0], x:x + foreground.shape[1]]
    foreground_mask = np.array(foreground < 255, np.uint8)
    result = (1 - foreground_mask) * image_part + foreground_mask * foreground
    image[y:y + foreground.shape[0], x:x + foreground.shape[1]] = result


def image_weighted_average(image1, image2, alpha):
    result = alpha * image1.astype(np.float64) + (1 - alpha) * image2.astype(np.float64)
    correct_image_values(result)
    return result.astype(np.uint8)


def blend_foreground_on_background(image,
                                   foreground,
                                   position,
                                   alpha=0.5):
    x, y = position
    width, height = foreground.shape[1], foreground.shape[0]
    background_part = image[y:y + height, x:x + width]
    background_mean = np.mean(background_part)
    mask1 = np.array(foreground == 0, np.uint8)

    mask2 = (foreground > 0) & (foreground < 255)
    non_zero_foreground = foreground[mask2]
    min_non_zero_foreground = np.min(non_zero_foreground)
    interpolator = interp1d([min_non_zero_foreground, np.max(non_zero_foreground)],
                            [min_non_zero_foreground, background_mean])

    foreground[mask2] = interpolator(non_zero_foreground)

    mask2 = np.array(mask2, np.uint8)
    mask3 = np.array(foreground == 255, np.uint8)

    blended = image_weighted_average(mask2 * background_part, mask2 * foreground, alpha)
    result = mask3 * background_part + blended + mask1 * foreground
    image[y:y + height, x:x + width] = result


def change_foreground_color_and_blend_foreground_on_background(image,
                                                               foreground,
                                                               position,
                                                               new_foreground_color,
                                                               alpha=0.3):
    x, y = position
    width, height = foreground.shape[1], foreground.shape[0]
    background_part = image[y:y + height, x:x + width]
    background_mean = np.mean(background_part)
    mask1 = np.array(foreground == 0, np.uint8)

    mask2 = (foreground > 0) & (foreground < 255)
    if np.any(mask2):
        non_zero_foreground = foreground[mask2]
        min_non_zero_foreground = np.min(non_zero_foreground)
        new_min_non_zero_foreground = new_foreground_color + min_non_zero_foreground
        if new_min_non_zero_foreground > 255:
            new_min_non_zero_foreground = 255

        interpolator = interp1d([min_non_zero_foreground, np.max(non_zero_foreground)],
                                [new_min_non_zero_foreground, background_mean])

        foreground[mask2] = interpolator(non_zero_foreground)

        mask2 = np.array(mask2, np.uint8)
        blended = image_weighted_average(mask2 * background_part, mask2 * foreground, alpha)
    else:
        blended = np.zeros(foreground.shape, np.uint8)

    mask3 = np.array(foreground == 255, np.uint8)
    result = mask3 * background_part + blended + mask1 * new_foreground_color
    image[y:y + height, x:x + width] = result


class InfiniteSampler:
    def __init__(self, items):
        self.items = items
        self.indices = self._get_random_indices()

    def next(self):
        if len(self.indices) == 0:
            self.indices = self._get_random_indices()
        index = self.indices.pop()
        return self.items[index]

    def _get_random_indices(self):
        indices = list(range(len(self.items)))
        random.shuffle(indices)
        return indices


def read_lines(file_path):
    with open(file_path, 'r') as f:
        return list(filter(lambda x: x != '', [line.strip() for line in f.readlines()]))


def alter_image_brightness_and_contrast_randomly(image,
                                                 delta_range=(-25, 25),
                                                 coefficient_range=(0.75, 1.25)):
    if random.choice([0, 1]) == 0:
        return image
    choice = random.choice([0, 1, 2, 3])
    if choice == 0:
        return change_brightness(image,
                                 random.randint(delta_range[0],
                                                delta_range[1]))
    if choice == 1:
        return change_contrast(image,
                               random.uniform(coefficient_range[0],
                                              coefficient_range[1]))
    if choice == 2:
        return change_contrast(change_brightness(image,
                                                 random.randint(delta_range[0],
                                                                delta_range[1])),
                               random.uniform(coefficient_range[0],
                                              coefficient_range[1]))
    return change_brightness(change_contrast(image,
                                             random.uniform(coefficient_range[0],
                                                            coefficient_range[1])),
                             random.randint(delta_range[0],
                                            delta_range[1]))


def alter_signature(image,
                    width_range,
                    height_range,
                    angle_range=(-60.0, 60.0)):
    if random_bool():
        rotated = rotate(image, random.uniform(angle_range[0], angle_range[1]))
    else:
        rotated = image

    width = random.randint(width_range[0], width_range[1])
    height = random.randint(height_range[0], height_range[1])
    return cv2.resize(image, (width, height), interpolation=cv2.INTER_CUBIC)


def insert_random_signatures(signatures_foreground, signature_sampler):
    number_of_signatures = random.choice([1, 2, 3])
    if number_of_signatures == 1:
        signature = signature_sampler.next()
        signature = alter_signature(signature,
                                    width_range=(
                                        to_int(SIGNATURE_AREA_SHAPE[1] * 0.25), to_int(SIGNATURE_AREA_SHAPE[1] * 0.75)),
                                    height_range=(
                                        to_int(SIGNATURE_AREA_SHAPE[0] * 0.25), to_int(SIGNATURE_AREA_SHAPE[0] * 0.75)))
        width = signature.shape[1]
        height = signature.shape[0]
        x = random.randint(0, SIGNATURE_AREA_SHAPE[1] - width)
        y = random.randint(0, SIGNATURE_AREA_SHAPE[0] - height)
        signatures_foreground[y:y + height, x: x + width] = signature
        return (np.array(signatures_foreground),
                np.ones(SIGNATURE_AREA_SHAPE, np.uint8) * 255,
                np.ones(SIGNATURE_AREA_SHAPE, np.uint8) * 255)
    elif number_of_signatures == 2:
        cell_width = SIGNATURE_AREA_SHAPE[1] / 2
        cell_height = SIGNATURE_AREA_SHAPE[0]
        min_width = to_int(cell_width / 2)
        max_width = to_int(cell_width * (1.0 + MAX_SIGNATURE_RECT_INTERSECTION))
        min_height = to_int(cell_height / 2)
        max_height = to_int(cell_height)

        signature_providers = [lambda: signature_sampler.next(), lambda: signature_sampler.next()]
        indices = list(range(len(signature_providers)))
        signature_indices = random.choice(list(itertools.combinations(indices, random.randint(0, len(indices) - 1))))
        for index in signature_indices:
            signature_providers[index] = lambda: np.ones((min_width, min_height), np.uint8) * 255

        signature1 = signature_providers[0]()
        signature2 = signature_providers[1]()

        signature1 = alter_signature(signature1,
                                     width_range=(min_width, max_width),
                                     height_range=(min_height, max_height))

        signature2 = alter_signature(signature2,
                                     width_range=(min_width, max_width),
                                     height_range=(min_height, max_height))

        width1 = signature1.shape[1]
        height1 = signature1.shape[0]
        width2 = signature2.shape[1]
        height2 = signature2.shape[0]

        x1 = random.randint(0, max_width - width1)
        y1 = random.randint(0, max_height - height1)
        x2 = random.randint(SIGNATURE_AREA_SHAPE[1] - max_width, SIGNATURE_AREA_SHAPE[1] - width2)
        y2 = random.randint(SIGNATURE_AREA_SHAPE[0] - max_height, SIGNATURE_AREA_SHAPE[0] - height2)

        signatures_foreground[y1:y1 + height1, x1:x1 + width1] = signature1
        signatures_foreground[y2:y2 + height2, x2:x2 + width2] &= signature2

        signature1_output = np.ones(SIGNATURE_AREA_SHAPE, np.uint8) * 255
        signature1_output[y1:y1 + height1, x1:x1 + width1] = signature1
        signature2_output = np.ones(SIGNATURE_AREA_SHAPE, np.uint8) * 255
        signature2_output[y2:y2 + height2, x2:x2 + width2] = signature2

        result = [signature1_output,
                  signature2_output,
                  None]
        for index in signature_indices:
            result[index] = None
        result = [s for s in result if s is not None]
        while len(result) < 3:
            result.append(np.ones(SIGNATURE_AREA_SHAPE, np.uint8) * 255)
        return result
    else:
        cell_width = SIGNATURE_AREA_SHAPE[1] / 3
        cell_height = SIGNATURE_AREA_SHAPE[0]
        min_width = to_int(cell_width / 2)
        max_width = to_int(cell_width * (1.0 + MAX_SIGNATURE_RECT_INTERSECTION))
        min_height = to_int(cell_height / 2)
        max_height = to_int(cell_height)

        signature_providers = [lambda: signature_sampler.next(),
                               lambda: signature_sampler.next(),
                               lambda: signature_sampler.next()]

        indices = list(range(len(signature_providers)))
        signature_indices = random.choice(list(itertools.combinations(indices, random.randint(0, len(indices) - 1))))
        for index in signature_indices:
            signature_providers[index] = lambda: np.ones((min_width, min_height), np.uint8) * 255

        signature1 = signature_providers[0]()
        signature2 = signature_providers[1]()
        signature3 = signature_providers[2]()

        signature1 = alter_signature(signature1,
                                     width_range=(min_width, max_width),
                                     height_range=(min_height, max_height))

        signature2 = alter_signature(signature2,
                                     width_range=(min_width, max_width),
                                     height_range=(min_height, max_height))

        signature3 = alter_signature(signature3,
                                     width_range=(min_width, max_width),
                                     height_range=(min_height, max_height))

        width1 = signature1.shape[1]
        height1 = signature1.shape[0]
        width2 = signature2.shape[1]
        height2 = signature2.shape[0]
        width3 = signature3.shape[1]
        height3 = signature3.shape[0]

        x1 = random.randint(0, max_width - width1)
        y1 = random.randint(0, max_height - height1)
        min_x2 = to_int((SIGNATURE_AREA_SHAPE[1] - max_width) / 2)
        x2 = random.randint(min_x2, min_x2 + max_width - width2)
        y2 = random.randint(0, max_height - height2)
        x3 = random.randint(SIGNATURE_AREA_SHAPE[1] - max_width, SIGNATURE_AREA_SHAPE[1] - width3)
        y3 = random.randint(0, max_height - height3)

        signatures_foreground[y1:y1 + height1, x1:x1 + width1] = signature1
        signatures_foreground[y2:y2 + height2, x2:x2 + width2] &= signature2
        signatures_foreground[y3:y3 + height3, x3:x3 + width3] &= signature3

        signature1_output = np.ones(SIGNATURE_AREA_SHAPE, np.uint8) * 255
        signature1_output[y1:y1 + height1, x1:x1 + width1] = signature1
        signature2_output = np.ones(SIGNATURE_AREA_SHAPE, np.uint8) * 255
        signature2_output[y2:y2 + height2, x2:x2 + width2] = signature2
        signature3_output = np.ones(SIGNATURE_AREA_SHAPE, np.uint8) * 255
        signature3_output[y3:y3 + height3, x3:x3 + width3] = signature3

        result = [signature1_output,
                  signature2_output,
                  signature3_output]
        for index in signature_indices:
            result[index] = None
        result = [s for s in result if s is not None]
        while len(result) < 3:
            result.append(np.ones(SIGNATURE_AREA_SHAPE, np.uint8) * 255)
        return result


def generate_sample(grey_background_sampler,
                    signature_sampler):
    background = grey_background_sampler.next()
    background = cv2.resize(background, (INPUT_SHAPE[1], INPUT_SHAPE[0]), interpolation=cv2.INTER_CUBIC)
    background = alter_image_brightness_and_contrast_randomly(background)

    signatures_foreground = np.ones(SIGNATURE_AREA_SHAPE, np.uint8) * 255
    output_channels = insert_random_signatures(signatures_foreground, signature_sampler)
    output_channels = [threshold_otsu(c) for c in output_channels]
    all_signatures_channel = np.ones(SIGNATURE_AREA_SHAPE, np.uint8) * 255
    for output_channel in output_channels:
        all_signatures_channel &= output_channel
    output_channels.append(all_signatures_channel)

    background_mean = np.mean(background)
    background_std = np.std(background)
    color_upper_limit = max(min(to_int(background_mean - background_std), to_int(background_mean - 5)), 0)
    color_lower_limit = random.randint(0, max(min(to_int(background_mean - 2 * background_std),
                                                  to_int(background_mean - 10)), 0))

    signatures_x = INPUT_SHAPE[1] - SIGNATURE_AREA_SHAPE[1]
    signatures_y = INPUT_SHAPE[0] - SIGNATURE_AREA_SHAPE[0]

    foreground_color_type = random.choice([1, 2, 3])
    if foreground_color_type == 1:
        signatures_foreground = change_brightness(threshold_otsu(signatures_foreground),
                                                  random.randint(color_lower_limit, color_upper_limit))

        insert_foreground(background,
                          signatures_foreground,
                          (signatures_x, signatures_y))
    elif foreground_color_type == 2:
        change_foreground_color_and_blend_foreground_on_background(background,
                                                                   signatures_foreground,
                                                                   (signatures_x, signatures_y),
                                                                   random.randint(color_lower_limit,
                                                                                  color_upper_limit))
    else:
        signatures_foreground = randomize_foreground(signatures_foreground, color_lower_limit, color_upper_limit)
        insert_foreground(background,
                          signatures_foreground,
                          (signatures_x, signatures_y))

    if random_bool():
        if random_bool():
            background = random_add_gaussian_noise(background, 0, random.randint(10, 20), (-20, 20))
        else:
            background = speckle_image(background, 0.01, random.randint(0, 255))

        if random_bool():
            background = random_blur(background)
        else:
            background = compress_image(background, random.randint(50, 100))

    return background, output_channels


def main():
    if not os.path.isdir(INPUT_IMAGES_PATH):
        os.makedirs(INPUT_IMAGES_PATH)

    if not os.path.isdir(OUTPUT_IMAGES_PATH):
        os.makedirs(OUTPUT_IMAGES_PATH)

    grey_backgrounds_paths = [os.path.join(GREY_BACKGROUNDS_DIR, f) for f in os.listdir(GREY_BACKGROUNDS_DIR)]
    signatures_paths = [os.path.join(SIGNATURES_DIR, f) for f in os.listdir(SIGNATURES_DIR)]

    grey_backgrounds = [read_image_grayscale(p) for p in grey_backgrounds_paths if is_image(p)]
    signatures = [read_image_grayscale(p) for p in signatures_paths if is_image(p)]

    grey_background_sampler = InfiniteSampler(grey_backgrounds)
    signature_sampler = InfiniteSampler(signatures)

    progress_bar = tqdm(total=DATASET_SIZE)
    for i in range(DATASET_SIZE):
        background, output_channels = generate_sample(grey_background_sampler,
                                                      signature_sampler)

        cv2.imwrite(os.path.join(INPUT_IMAGES_PATH, f'{i}.png'), background)
        # with gzip.open(os.path.join(OUTPUT_IMAGES_PATH, f'{i}.dat'), 'wb') as f:
        #     pickle.dump([255 - c for c in output_channels], f)
        progress_bar.update()

    progress_bar.close()


if __name__ == '__main__':
    main()
