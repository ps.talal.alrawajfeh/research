import gzip
import math
import os
import pickle
import random
from enum import Enum

import cv2
import matplotlib
import numpy as np
import tensorflow as tf

keras = tf.keras
from keras.callbacks import ModelCheckpoint
from keras.layers import Input, Conv2D, BatchNormalization, Conv2DTranspose, ReLU, Flatten, Dense, Concatenate, Permute, \
    Reshape, Multiply, Subtract, Lambda, Softmax
from keras.losses import binary_crossentropy
from keras.models import Model
from keras.models import load_model
from keras.optimizers import Adam
from keras.utils import Sequence
from matplotlib import pyplot as plt

INPUT_IMAGES_PATH = './input'
OUTPUT_IMAGES_PATH = './output'
INPUT_SIZE = (416, 224)
INPUT_SHAPE = (INPUT_SIZE[1], INPUT_SIZE[0], 1)
TRAINING_IMAGES_PERCENTAGE = 0.7
VALIDATION_TEST_IMAGES_PERCENTAGE = 0.15
EPSILON = 1e-8
NUMBER_OF_OUTPUT_FILTERS = 1

MODEL_FILE_NAME = 'model'

TRAINING_BATCH_SIZE = 16
VALIDATION_BATCH_SIZE = 16


def conv_batch_norm_relu_layer(input_layer, filters, kernel=(3, 3), strides=None):
    if strides is None:
        conv = Conv2D(filters,
                      kernel,
                      kernel_initializer='he_normal',
                      use_bias=False,
                      padding='same')(input_layer)
    else:
        conv = Conv2D(filters,
                      kernel,
                      strides=strides,
                      use_bias=False,
                      kernel_initializer='he_normal',
                      padding='same')(input_layer)
    conv = BatchNormalization()(conv)
    conv = ReLU()(conv)
    return conv


def stacked_conv(input_layer, filters, kernel=(3, 3)):
    conv = conv_batch_norm_relu_layer(input_layer, filters, kernel)
    return conv_batch_norm_relu_layer(conv, filters, kernel)


def deconv_batch_norm_relu_layer(input_layer, filters, kernel=(3, 3)):
    conv = Conv2DTranspose(filters,
                           kernel,
                           strides=(2, 2),
                           kernel_initializer='he_normal',
                           use_bias=False,
                           padding='same')(input_layer)
    conv = BatchNormalization()(conv)
    conv = ReLU()(conv)
    return conv


def location_attention(input_layer, input_size):
    condensed = tf.keras.layers.Lambda(lambda x: tf.reduce_mean(x, axis=-1, keepdims=True))(input_layer)
    average_flat = tf.keras.layers.Flatten()(condensed)
    units = np.prod(input_size)
    dense = tf.keras.layers.Dense(units,
                                  kernel_initializer='he_normal',
                                  use_bias=False)(average_flat)
    dense = tf.keras.layers.BatchNormalization()(dense)
    dense = tf.keras.layers.ReLU()(dense)
    dense = tf.keras.layers.Dense(units,
                                  kernel_initializer='he_normal',
                                  use_bias=False)(dense)
    dense = tf.keras.layers.BatchNormalization()(dense)
    dense = tf.keras.layers.Activation(tf.nn.sigmoid)(dense)
    return tf.keras.layers.Reshape((input_size[0], input_size[1], 1))(dense)


def attention_module(input_layer, input_size):
    attention = location_attention(input_layer, input_size)
    return tf.keras.layers.Multiply()([input_layer, attention])


def u_net():
    filters = [16, 32, 64, 128, 256, 512]
    # encoder
    input_layer = Input(shape=INPUT_SHAPE, name='encoder_input')
    batch_norm = BatchNormalization()(input_layer)

    convolutions = []
    encoder = batch_norm
    for i in range(len(filters) - 1):
        conv = stacked_conv(encoder, filters[i])
        encoder = conv_batch_norm_relu_layer(conv, filters[i], (3, 3), strides=(2, 2))
        convolutions.append(conv)

    # bottleneck
    conv = stacked_conv(encoder, filters[-1], (3, 3))
    convolutions.append(conv)
    bottleneck_model = Model(inputs=input_layer, outputs=convolutions)

    input_image = Input(shape=INPUT_SHAPE, name='input_image')
    template_image = Input(shape=INPUT_SHAPE, name='template_image')

    input_convolutions = bottleneck_model(input_image)
    template_convolutions = bottleneck_model(template_image)

    input_features = input_convolutions[-1]
    template_features = template_convolutions[-1]

    reshaped_input_features = Reshape((-1, filters[-1]))(input_features)
    reshaped_template_features = Reshape((-1, filters[-1]))(template_features)
    query = Reshape((-1, 1, filters[-1]))(reshaped_template_features)
    key = Reshape((1, -1, filters[-1]))(reshaped_input_features)
    euclidean_distance = Lambda(lambda x: tf.reduce_sum(tf.square(x[0] - x[1]), axis=-1))([query, key])
    # similarity = Lambda(lambda x: tf.exp(-x))(euclidean_distance)
    n = len(filters) - 1
    reshaped = Reshape((-1, INPUT_SHAPE[0] // 2 ** n, INPUT_SHAPE[1] // 2 ** n))(euclidean_distance)
    transposed = Permute((2, 3, 1))(reshaped)
    condensed = tf.keras.layers.Lambda(lambda x: tf.reduce_sum(x, axis=-1, keepdims=True))(transposed)
    # inverted = 1 - condensed
    value = Multiply()([condensed, input_features])

    # decoder
    decoder = value
    for i in range(len(filters) - 2, -1, -1):
        up = tf.keras.layers.Concatenate(axis=-1)(
            [deconv_batch_norm_relu_layer(decoder, filters[i], (3, 3)), input_convolutions[i]])
        decoder = stacked_conv(up, filters[i], (3, 3))

    prediction = Conv2D(NUMBER_OF_OUTPUT_FILTERS,
                        (1, 1),
                        kernel_initializer='he_normal',
                        use_bias=True,
                        padding='same',
                        activation='sigmoid')(decoder)

    return Model(inputs=[input_image, template_image], outputs=prediction)


def pre_process(image_tensor: np.ndarray) -> np.ndarray:
    image_tensor = np.array(image_tensor, np.float32)
    return (image_tensor / 255.0).reshape(INPUT_SHAPE)


def read_pickle_compressed(file_path):
    with gzip.open(file_path, 'rb') as f:
        return pickle.load(f)


def read_compressed_image(file_path):
    with gzip.open(file_path, 'rb') as f:
        content = f.read()
    content_array = np.asarray(bytearray(content), dtype=np.uint8)
    return cv2.imdecode(content_array, cv2.IMREAD_GRAYSCALE)


class DataGenerator(Sequence):
    def __init__(self,
                 input_image_paths,
                 output_image_paths,
                 batch_size):
        self.count = len(input_image_paths)
        self.input_image_paths = input_image_paths
        self.output_image_paths = output_image_paths
        self.batch_size = batch_size

    def __getitem__(self, index):
        input_image_paths = self.input_image_paths[index * self.batch_size: (index + 1) * self.batch_size]
        input_images = [read_pickle_compressed(f) for f in input_image_paths]
        input_images_1 = [pre_process(images[0]) for images in input_images]
        input_images_2 = [pre_process(images[1]) for images in input_images]
        output_image_paths = self.output_image_paths[index * self.batch_size: (index + 1) * self.batch_size]
        output_images = [pre_process(read_pickle_compressed(f)) for f in output_image_paths]
        return [np.array(input_images_1), np.array(input_images_2)], np.array(output_images)

    def __len__(self):
        return int(math.ceil(self.count / self.batch_size))


def load_image_paths():
    input_image_paths = os.listdir(INPUT_IMAGES_PATH)
    output_image_paths = os.listdir(OUTPUT_IMAGES_PATH)

    input_image_paths = list(filter(lambda x: x.endswith('.dat'), input_image_paths))
    output_image_paths = list(filter(lambda x: x.endswith('.dat'), output_image_paths))

    corresponding_image_paths = []
    for f in input_image_paths:
        number = f.split('.')[0].strip()
        corresponding_image_paths.append(os.path.join(OUTPUT_IMAGES_PATH, f'{number}.dat'))

    input_image_paths = [os.path.join(INPUT_IMAGES_PATH, f) for f in input_image_paths]

    return input_image_paths, corresponding_image_paths


def train_validation_test_split(input_images, output_images):
    indices = list(range(len(input_images)))
    random.shuffle(indices)

    input_images = [input_images[i] for i in indices]
    output_images = [output_images[i] for i in indices]

    train_images_count = int(len(input_images) * TRAINING_IMAGES_PERCENTAGE)
    validation_test_images_count = int(
        len(input_images) * VALIDATION_TEST_IMAGES_PERCENTAGE)

    return ((input_images[:train_images_count],
             output_images[:train_images_count]),
            (input_images[train_images_count:train_images_count + validation_test_images_count],
             output_images[train_images_count:train_images_count + validation_test_images_count]),
            (input_images[train_images_count + validation_test_images_count:],
             output_images[train_images_count + validation_test_images_count:]))


def cache_object(obj, file):
    with open(file, 'wb') as f:
        pickle.dump(obj, f)


def load_cached(file):
    with open(file, 'rb') as f:
        return pickle.load(f)


class ModelLoadingMode(Enum):
    MinimumMetricValue = 0
    MaximumMetricValue = 1


def train_model():
    model = u_net()
    model.summary()

    model.compile(loss=binary_crossentropy, optimizer=Adam())

    if os.path.isfile('x_train.pickle') and os.path.isfile('y_train.pickle') and \
            os.path.isfile('x_val.pickle') and os.path.isfile('y_val.pickle') and \
            os.path.isfile('x_test.pickle') and os.path.isfile('y_test.pickle'):
        x_train = load_cached('x_train.pickle')
        y_train = load_cached('y_train.pickle')
        x_val = load_cached('x_val.pickle')
        y_val = load_cached('y_val.pickle')
    else:
        input_image_paths, output_image_paths = load_image_paths()
        (x_train, y_train), (x_val, y_val), (x_test, y_test) = train_validation_test_split(
            input_image_paths, output_image_paths)
        cache_object(x_train, 'x_train.pickle')
        cache_object(y_train, 'y_train.pickle')
        cache_object(x_val, 'x_val.pickle')
        cache_object(y_val, 'y_val.pickle')
        cache_object(x_test, 'x_test.pickle')
        cache_object(y_test, 'y_test.pickle')

    train_data_generator = DataGenerator(x_train, y_train, TRAINING_BATCH_SIZE)
    val_data_generator = DataGenerator(x_val, y_val, VALIDATION_BATCH_SIZE)

    model_last_checkpoint_callback = ModelCheckpoint(
        filepath=MODEL_FILE_NAME +
                 '_{epoch:02d}_{loss:0.4f}_{val_loss:0.4f}.h5',
        save_best_only=False
    )

    model_best_checkpoint_callback = ModelCheckpoint(
        filepath=MODEL_FILE_NAME + '.h5',
        save_best_only=True,
        monitor='val_loss',
        mode='min'
    )

    history = model.fit(train_data_generator,
                        epochs=50,
                        validation_data=val_data_generator,
                        callbacks=[model_last_checkpoint_callback,
                                   model_best_checkpoint_callback])

    cache_object(history, MODEL_FILE_NAME + '_history.pickle')


def evaluate_model():
    model = load_model(MODEL_FILE_NAME + '.h5', compile=False)
    model.compile(loss=binary_crossentropy, optimizer=Adam())

    x_test = load_cached('x_test.pickle')
    y_test = load_cached('y_test.pickle')

    test_data_generator = DataGenerator(x_test, y_test, 2)

    results = model.evaluate(test_data_generator)

    cache_object(results, 'evaluation_results.pickle')
    print(results)


def remove_white_border(binary_image):
    mask = (255 - binary_image) > 0

    height, width = binary_image.shape[0:2]
    mask1, mask2 = mask.any(0), mask.any(1)
    x1, x2 = mask1.argmax(), width - mask1[::-1].argmax()
    y1, y2 = mask2.argmax(), height - mask2[::-1].argmax()

    return binary_image[y1:y2, x1:x2]


def post_process(output_image):
    output_image = output_image.reshape((INPUT_SHAPE[0], INPUT_SHAPE[1]))
    output_image = output_image * 255
    output_image[output_image > 255] = 255
    output_image[output_image < 0] = 0
    output_image = np.array(output_image, np.uint8)
    output_image = 255 - output_image
    # return output_image
    output_image = cv2.threshold(
        output_image, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
    # return remove_white_border(output_image)
    return output_image


def post_process_without_binarization(output_image):
    output_image = output_image.reshape((INPUT_SHAPE[0], INPUT_SHAPE[1]))
    output_image = output_image * 255
    output_image[output_image > 255] = 255
    output_image[output_image < 0] = 0
    output_image = np.array(output_image, np.uint8)
    output_image = 255 - output_image
    return remove_white_border(output_image)


def threshold_otsu(image):
    return cv2.threshold(image,
                         0,
                         255,
                         cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]


def feed_forward(image_path):
    matplotlib.use('TkAgg')
    model = load_model('/home/u764/Downloads/model_04_0.0016_0.0168.h5', compile=False)
    model.compile(loss=binary_crossentropy, optimizer=Adam())

    images = read_pickle_compressed(image_path)
    input_image = images[0]
    template_image = images[1]
    input_tensor = [np.array([pre_process(input_image)]), np.array([pre_process(template_image)])]

    output_image = model.predict(input_tensor)[0]
    fig, axs = plt.subplots(3, 1)
    axs[0].imshow(255 - input_image, plt.cm.gray)
    axs[1].imshow(255 - template_image, plt.cm.gray)
    axs[2].imshow(post_process(output_image), plt.cm.gray)
    plt.show()


if __name__ == '__main__':
    feed_forward("./input/11.dat")
    # train_model()
