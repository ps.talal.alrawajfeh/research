import math
import os
import pickle
import random
from enum import Enum

import cv2
import numpy as np
import tensorflow as tf
from matplotlib import pyplot as plt

keras = tf.keras
from keras.callbacks import ModelCheckpoint
from keras.layers import Input, Conv2D, BatchNormalization, Conv2DTranspose, ReLU, Flatten, Dense, Multiply
from keras.losses import binary_crossentropy
from keras.models import Model
from keras.models import load_model
from keras.optimizers import Adam
from keras.utils import Sequence
from tqdm import tqdm

SIGNATURES_PATH = '/home/user/Development/data/signatures'
INPUT_IMAGES_PATH = './input'
OUTPUT_IMAGES_PATH = './output'
INPUT_SIZE = (416, 224)
INPUT_SHAPE = (INPUT_SIZE[1], INPUT_SIZE[0], 1)
TRAINING_IMAGES_PERCENTAGE = 0.7
VALIDATION_TEST_IMAGES_PERCENTAGE = 0.15
EPSILON = 1e-8
NUMBER_OF_TRAINING_SAMPLES = 10000

MODEL_FILE_NAME = 'model'

TRAINING_BATCH_SIZE = 2
VALIDATION_BATCH_SIZE = 2


def conv_batch_norm_relu_layer(input_layer, filters, kernel=(3, 3), strides=None):
    if strides is None:
        conv = Conv2D(filters,
                      kernel,
                      kernel_initializer='he_normal',
                      use_bias=False,
                      padding='same')(input_layer)
    else:
        conv = Conv2D(filters,
                      kernel,
                      strides=strides,
                      use_bias=False,
                      kernel_initializer='he_normal',
                      padding='same')(input_layer)
    conv = BatchNormalization()(conv)
    conv = ReLU()(conv)
    return conv


def stacked_conv(input_layer, filters, kernel=(3, 3)):
    conv = conv_batch_norm_relu_layer(input_layer, filters, kernel)
    return conv_batch_norm_relu_layer(conv, filters, kernel)


def deconv_batch_norm_relu_layer(input_layer, filters, kernel=(3, 3)):
    conv = Conv2DTranspose(filters,
                           kernel,
                           strides=(2, 2),
                           kernel_initializer='he_normal',
                           use_bias=False,
                           padding='same')(input_layer)
    conv = BatchNormalization()(conv)
    conv = ReLU()(conv)
    return conv


def spatial_attention(input_layer):
    average_pool = tf.keras.layers.Lambda(lambda x: tf.reduce_mean(x, axis=-1, keepdims=True))(input_layer)
    max_pool = tf.keras.layers.Lambda(lambda x: tf.reduce_max(x, axis=-1, keepdims=True))(input_layer)

    concatenated = tf.keras.layers.Concatenate(axis=-1)([average_pool, max_pool])
    concatenated_pooled = tf.keras.layers.AveragePooling2D(pool_size=(2, 2))(concatenated)
    flat = tf.keras.layers.Flatten()(concatenated_pooled)

    attention = tf.keras.layers.Dense(flat.shape[-1],
                                      use_bias=False,
                                      kernel_initializer='he_normal')(flat)

    channels_shape = (-1, input_layer.shape[1] // 2, input_layer.shape[2] // 2, 2)
    attention = tf.keras.layers.Lambda(lambda x: tf.reshape(x, channels_shape))(attention)
    attention = tf.keras.layers.UpSampling2D(size=(2, 2))(attention)

    attention1 = tf.keras.layers.Lambda(lambda x: x[:, :, :, :1])(attention)
    attention2 = tf.keras.layers.Lambda(lambda x: x[:, :, :, 1:])(attention)

    attention1 = tf.keras.layers.Conv2D(input_layer.shape[-1] // 2,
                                        kernel_size=(1, 1),
                                        kernel_initializer='he_normal',
                                        use_bias=False,
                                        activation='sigmoid')(attention1)

    attention2 = tf.keras.layers.Conv2D(input_layer.shape[-1] // 2,
                                        kernel_size=(1, 1),
                                        kernel_initializer='he_normal',
                                        use_bias=False,
                                        activation='sigmoid')(attention2)

    attention = tf.keras.layers.Concatenate(axis=-1)([attention1, attention2])

    return tf.keras.layers.Lambda(lambda x: x[0] * x[1])([attention, input_layer])


#
# def autoencoder(filters=(32, 64, 128, 256),
#                 kernels=(5, 3, 3, 3)):
#     reductions = list(map(lambda x: x - 1, kernels))
#     padding1 = sum(map(lambda i: reductions[i] * 2 ** i, range(len(kernels))))
#     padding2 = int(round(sum(map(lambda i: reductions[i] * 2 ** i / 8, range(len(kernels) - 1)))))
#
#     # encoder
#     input_layer = Input(shape=INPUT_SHAPE)
#
#     zero_padding = tf.keras.layers.ZeroPadding2D(padding=(padding1, padding1))(input_layer)
#     batch_norm = BatchNormalization()(zero_padding)
#
#     conv = batch_norm
#     for layer_index in range(len(filters) - 1):
#         conv = stacked_conv(conv, filters[layer_index], (kernels[layer_index], kernels[layer_index]))
#         conv = conv_batch_norm_relu_layer(conv, filters[layer_index], (2, 2), strides=(2, 2))
#
#     # bottleneck
#     conv = stacked_conv(conv, filters[-1], (kernels[-1], kernels[-1]))
#     conv = spatial_attention(conv)
#
#     # decoder
#     conv = tf.keras.layers.ZeroPadding2D(padding=(padding2, padding2))(conv)
#
#     for layer_index in range(len(filters) - 1, 0, -1):
#         conv = deconv_batch_norm_relu_layer(conv, filters[layer_index], (2, 2))
#         conv = stacked_conv(conv, filters[layer_index - 1], (kernels[layer_index - 1], kernels[layer_index - 1]))
#
#     # output
#     prediction = Conv2D(filters=2,
#                         kernel_size=(1, 1),
#                         kernel_initializer='he_normal',
#                         use_bias=True,
#                         activation='sigmoid')(conv)
#     return Model(inputs=input_layer, outputs=prediction)


# def autoencoder(filters=(16, 32, 64, 128),
#                 kernels=(5, 3, 3, 3)):
#     # encoder
#     input_layer = Input(shape=INPUT_SHAPE)
#     batch_norm = BatchNormalization()(input_layer)

#     conv = batch_norm
#     for layer_index in range(len(filters) - 1):
#         conv = stacked_conv(conv, filters[layer_index], (kernels[layer_index], kernels[layer_index]))
#         conv = conv_batch_norm_relu_layer(conv, filters[layer_index], (2, 2), strides=(2, 2))

#     # bottleneck
#     conv = stacked_conv(conv, filters[-1], (kernels[-1], kernels[-1]))
#     flattened = Flatten()(conv)
#     dense = Dense(25 * 52 * 128,
#                   use_bias=None,
#                   kernel_initializer='he_normal',
#                   activation='softmax')(flattened)
#     conv = Multiply()([conv, dense])

#     # decoder
#     for layer_index in range(len(filters) - 1, 0, -1):
#         conv = deconv_batch_norm_relu_layer(conv, filters[layer_index], (2, 2))
#         conv = stacked_conv(conv, filters[layer_index - 1], (kernels[layer_index - 1], kernels[layer_index - 1]))

#     # output
#     prediction = Conv2D(filters=2,
#                         kernel_size=(1, 1),
#                         kernel_initializer='he_normal',
#                         use_bias=True,
#                         activation='sigmoid')(conv)
#     return Model(inputs=input_layer, outputs=prediction)

def location_attention(input_layer, input_size):
    average = tf.keras.layers.Lambda(lambda x: tf.reduce_mean(x, axis=-1, keepdims=True))(input_layer)
    average_flat = tf.keras.layers.Flatten()(average)
    units = np.prod(input_size)
    dense = tf.keras.layers.Dense(units,
                                  kernel_initializer='he_normal',
                                  use_bias=False)(average_flat)
    dense = tf.keras.layers.BatchNormalization()(dense)
    dense = tf.keras.layers.ReLU()(dense)
    dense = tf.keras.layers.Dense(units,
                                  kernel_initializer='he_normal',
                                  use_bias=False)(dense)
    dense = tf.keras.layers.BatchNormalization()(dense)
    dense = tf.keras.layers.Activation(tf.nn.sigmoid)(dense)
    return tf.keras.layers.Reshape((input_size[0], input_size[1], 1))(dense)


def attention_module(input_layer, input_size):
    attention1 = location_attention(input_layer, input_size)
    attention2 = location_attention(input_layer, input_size)
    input_layer_1 = tf.keras.layers.Multiply()([input_layer, attention1])
    input_layer_2 = tf.keras.layers.Multiply()([input_layer, attention2])
    return tf.keras.layers.Concatenate(axis=-1)([input_layer_1, input_layer_2])


def u_net():
    filters = [32, 64, 128, 256]
    # encoder
    input_layer = Input(shape=INPUT_SHAPE)
    batch_norm = BatchNormalization()(input_layer)

    conv1 = stacked_conv(batch_norm, filters[0])
    pool1 = conv_batch_norm_relu_layer(
        conv1, filters[0], (3, 3), strides=(2, 2))
    conv2 = stacked_conv(pool1, filters[1])
    pool2 = conv_batch_norm_relu_layer(
        conv2, filters[1], (3, 3), strides=(2, 2))
    conv3 = stacked_conv(pool2, filters[2])
    pool3 = conv_batch_norm_relu_layer(
        conv3, filters[2], (3, 3), strides=(2, 2))
    # bottleneck
    conv4 = stacked_conv(pool3, filters[3], (3, 3))
    n = len(filters) - 1
    conv4 = attention_module(conv4, (INPUT_SHAPE[0] // 2 ** n, INPUT_SHAPE[1] // 2 ** n))

    # decoder
    up1 = tf.keras.layers.Concatenate(axis=-1)([deconv_batch_norm_relu_layer(
        conv4, filters[3], (3, 3)), conv3])
    conv5 = stacked_conv(up1, filters[2], (3, 3))
    # n -= 1
    # conv5 = attention_module(conv5, (INPUT_SHAPE[0] // 2 ** n, INPUT_SHAPE[1] // 2 ** n))

    up2 = tf.keras.layers.Concatenate(axis=-1)([deconv_batch_norm_relu_layer(
        conv5, filters[2], (3, 3)), conv2])
    conv6 = stacked_conv(up2, filters[1], (3, 3))
    # n -= 1
    # conv6 = attention_module(conv6, (INPUT_SHAPE[0] // 2 ** n, INPUT_SHAPE[1] // 2 ** n))

    up3 = tf.keras.layers.Concatenate(axis=-1)([deconv_batch_norm_relu_layer(
        conv6, filters[1], (3, 3)), conv1])
    conv7 = stacked_conv(up3, filters[0], (3, 3))
    # n -= 1
    # conv7 = attention_module(conv7, (INPUT_SHAPE[0], INPUT_SHAPE[1]))

    # output
    prediction = Conv2D(2,
                        (1, 1),
                        kernel_initializer='he_normal',
                        use_bias=True,
                        padding='same',
                        activation='sigmoid')(conv7)
    return Model(inputs=input_layer, outputs=prediction)


def pre_process(image_tensor: np.ndarray) -> np.ndarray:
    image_tensor = np.array(image_tensor, np.float32)
    return (image_tensor / 255.0).reshape(INPUT_SHAPE)


class DataGenerator(Sequence):
    def __init__(self,
                 input_image_paths,
                 output_image_paths,
                 batch_size):
        self.count = len(input_image_paths)
        self.input_image_paths = input_image_paths
        self.output_image_paths = output_image_paths
        self.batch_size = batch_size

    def __getitem__(self, index):
        input_image_paths = self.input_image_paths[index * self.batch_size: (index + 1) * self.batch_size]
        input_images = [pre_process(cv2.imread(f, cv2.IMREAD_GRAYSCALE))
                        for f in input_image_paths]

        output_image_paths = self.output_image_paths[index * self.batch_size: (index + 1) * self.batch_size]
        output_images = [np.concatenate([pre_process(cv2.imread(f[0], cv2.IMREAD_GRAYSCALE)),
                                         pre_process(cv2.imread(f[1], cv2.IMREAD_GRAYSCALE))],
                                        axis=-1)
                         for f in output_image_paths]

        return np.array(input_images), np.array(output_images)

    def __len__(self):
        return int(math.ceil(self.count / self.batch_size))


def load_image_paths():
    input_image_paths = os.listdir(INPUT_IMAGES_PATH)
    output_image_paths = os.listdir(OUTPUT_IMAGES_PATH)

    input_image_paths = list(filter(lambda x: x.endswith('.png'), input_image_paths))
    output_image_paths = list(filter(lambda x: x.endswith('.png'), output_image_paths))

    corresponding_image_paths = []
    for f in input_image_paths:
        number = f.split('.')[0].strip()

        if f'{number}_1.png' not in output_image_paths or f'{number}_2.png' not in output_image_paths:
            raise ValueError('Invalid generated files.')

        corresponding_image_paths.append((os.path.join(OUTPUT_IMAGES_PATH, f'{number}_1.png'),
                                          os.path.join(OUTPUT_IMAGES_PATH, f'{number}_2.png')))

    input_image_paths = [os.path.join(INPUT_IMAGES_PATH, f) for f in input_image_paths]

    return input_image_paths, corresponding_image_paths


def train_validation_test_split(input_images, output_images):
    indices = list(range(len(input_images)))
    random.shuffle(indices)

    input_images = [input_images[i] for i in indices]
    output_images = [output_images[i] for i in indices]

    train_images_count = int(len(input_images) * TRAINING_IMAGES_PERCENTAGE)
    validation_test_images_count = int(
        len(input_images) * VALIDATION_TEST_IMAGES_PERCENTAGE)

    return ((input_images[:train_images_count],
             output_images[:train_images_count]),
            (input_images[train_images_count:train_images_count + validation_test_images_count],
             output_images[train_images_count:train_images_count + validation_test_images_count]),
            (input_images[train_images_count + validation_test_images_count:],
             output_images[train_images_count + validation_test_images_count:]))


def cache_object(obj, file):
    with open(file, 'wb') as f:
        pickle.dump(obj, f)


def load_cached(file):
    with open(file, 'rb') as f:
        return pickle.load(f)


class ModelLoadingMode(Enum):
    MinimumMetricValue = 0
    MaximumMetricValue = 1


def resize_image(image, width, height):
    new_width = int(math.floor(width))
    new_height = int(math.floor(height))
    return cv2.resize(image, (new_width, new_height), interpolation=cv2.INTER_AREA)


def resize_image_up_to(image, max_width, max_height):
    if image.shape[0] / max_height > image.shape[1] / max_width:
        width_to_height_ratio = image.shape[1] / image.shape[0]
        new_height = max_height
        new_width = new_height * width_to_height_ratio
        return resize_image(image, new_width, new_height)

    height_to_width_ratio = image.shape[0] / image.shape[1]
    new_width = max_width
    new_height = new_width * height_to_width_ratio
    return resize_image(image, new_width, new_height)


def generate_images():
    signatures = os.listdir(SIGNATURES_PATH)
    signatures = [cv2.imread(os.path.join(SIGNATURES_PATH, s), cv2.IMREAD_GRAYSCALE) for s in signatures]

    if not os.path.isdir(INPUT_IMAGES_PATH):
        os.makedirs(INPUT_IMAGES_PATH)

    if not os.path.isdir(OUTPUT_IMAGES_PATH):
        os.makedirs(OUTPUT_IMAGES_PATH)

    image_width = INPUT_SIZE[0]
    image_height = INPUT_SIZE[1]
    image_half_width = image_width // 2

    progress_bar = tqdm(total=NUMBER_OF_TRAINING_SAMPLES)
    for i in range(NUMBER_OF_TRAINING_SAMPLES):
        signature1 = random.choice(signatures)
        signature2 = random.choice(signatures)

        signature1 = resize_image_up_to(signature1,
                                        random.randint(image_width // 4, image_half_width),
                                        image_height)
        signature2 = resize_image_up_to(signature2,
                                        random.randint(image_width // 4, image_half_width),
                                        image_height)

        signature1 = 255 - cv2.threshold(signature1, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
        signature2 = 255 - cv2.threshold(signature2, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]

        max_intersection_width = int(round(image_half_width * 0.2))
        signature1_x = random.randint(0, max_intersection_width)
        signature1_y = random.randint(0, image_height - signature1.shape[0])

        signature2_x = image_half_width + random.randint(-max_intersection_width,
                                                         image_half_width - signature2.shape[1])
        signature2_y = random.randint(0, image_height - signature2.shape[0])

        input_image = np.zeros((image_height, image_width), np.uint8)
        output_image1 = np.zeros((image_height, image_width), np.uint8)
        output_image2 = np.zeros((image_height, image_width), np.uint8)

        input_image[signature1_y: signature1_y + signature1.shape[0],
        signature1_x: signature1_x + signature1.shape[1]] = signature1

        output_image1[signature1_y: signature1_y + signature1.shape[0],
        signature1_x: signature1_x + signature1.shape[1]] = signature1

        input_image[signature2_y: signature2_y + signature2.shape[0],
        signature2_x: signature2_x + signature2.shape[1]] |= signature2

        output_image2[signature2_y: signature2_y + signature2.shape[0],
        signature2_x: signature2_x + signature2.shape[1]] |= signature2

        cv2.imwrite(os.path.join(INPUT_IMAGES_PATH, f'{i}.png'), input_image)
        cv2.imwrite(os.path.join(OUTPUT_IMAGES_PATH, f'{i}_1.png'), output_image1)
        cv2.imwrite(os.path.join(OUTPUT_IMAGES_PATH, f'{i}_2.png'), output_image2)
        progress_bar.update()
    progress_bar.close()


def train_model():
    model = u_net()
    model.summary()

    model.compile(loss=binary_crossentropy, optimizer=Adam())

    if os.path.isfile('x_train.pickle') and os.path.isfile('y_train.pickle') and \
            os.path.isfile('x_val.pickle') and os.path.isfile('y_val.pickle') and \
            os.path.isfile('x_test.pickle') and os.path.isfile('y_test.pickle'):
        x_train = load_cached('x_train.pickle')
        y_train = load_cached('y_train.pickle')
        x_val = load_cached('x_val.pickle')
        y_val = load_cached('y_val.pickle')
    else:
        input_image_paths, output_image_paths = load_image_paths()
        (x_train, y_train), (x_val, y_val), (x_test, y_test) = train_validation_test_split(
            input_image_paths, output_image_paths)
        cache_object(x_train, 'x_train.pickle')
        cache_object(y_train, 'y_train.pickle')
        cache_object(x_val, 'x_val.pickle')
        cache_object(y_val, 'y_val.pickle')
        cache_object(x_test, 'x_test.pickle')
        cache_object(y_test, 'y_test.pickle')

    train_data_generator = DataGenerator(x_train, y_train, TRAINING_BATCH_SIZE)
    val_data_generator = DataGenerator(x_val, y_val, VALIDATION_BATCH_SIZE)

    model_last_checkpoint_callback = ModelCheckpoint(
        filepath=MODEL_FILE_NAME + '_{epoch:02d}_{loss:0.4f}_{val_loss:0.4f}.h5',
        save_best_only=False
    )

    model_best_checkpoint_callback = ModelCheckpoint(
        filepath=MODEL_FILE_NAME + '.h5',
        save_best_only=True,
        monitor='val_loss',
        mode='min'
    )

    history = model.fit(train_data_generator,
                        epochs=50,
                        validation_data=val_data_generator,
                        callbacks=[model_last_checkpoint_callback,
                                   model_best_checkpoint_callback])

    cache_object(history, MODEL_FILE_NAME + '_history.pickle')


def evaluate_model():
    model = load_model(MODEL_FILE_NAME + '.h5', compile=False)
    model.compile(loss=binary_crossentropy, optimizer=Adam())

    x_test = load_cached('x_test.pickle')
    y_test = load_cached('y_test.pickle')

    test_data_generator = DataGenerator(x_test, y_test, 2)

    results = model.evaluate(test_data_generator)

    cache_object(results, 'evaluation_results.pickle')
    print(results)


def remove_white_border(binary_image):
    mask = (255 - binary_image) > 0

    height, width = binary_image.shape[0:2]
    mask1, mask2 = mask.any(0), mask.any(1)
    x1, x2 = mask1.argmax(), width - mask1[::-1].argmax()
    y1, y2 = mask2.argmax(), height - mask2[::-1].argmax()

    return binary_image[y1:y2, x1:x2]


def post_process(output_image):
    output_image = output_image * 255
    output_image[output_image > 255] = 255
    output_image[output_image < 0] = 0
    output_image = np.array(output_image, np.uint8)
    output_image = 255 - output_image
    return output_image
    # output_image = cv2.threshold(output_image, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
    # return remove_white_border(output_image)


def test_model():
    model = load_model('model_27_0.0088_0.0128.h5', compile=False)
    model.compile(loss=binary_crossentropy, optimizer=Adam())
    model.summary()

    # x_test = load_cached('x_test.pickle')
    # y_test = load_cached('y_test.pickle')
    #
    # test_data_generator = DataGenerator(x_test, y_test, 1)

    # input_tensor, _ = test_data_generator[5]

    original_input_image = cv2.imread('/home/u764/Downloads/Untitled.png', cv2.IMREAD_GRAYSCALE)
    original_input_image = cv2.resize(original_input_image, INPUT_SIZE, interpolation=cv2.INTER_AREA)
    input_image = 255 - cv2.threshold(original_input_image, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
    input_image = np.array(input_image, np.float32) / 255.0
    input_image = input_image.reshape(INPUT_SHAPE)
    input_tensor = np.array([input_image])

    output_tensor = model.predict(input_tensor)
    output_tensor = output_tensor.reshape(2, INPUT_SHAPE[0], INPUT_SHAPE[1])

    image1 = output_tensor[0]
    image2 = output_tensor[1]

    # input_image = input_tensor.reshape(INPUT_SHAPE[0], INPUT_SHAPE[1])

    fig, axs = plt.subplots(3, 1)

    axs[0].imshow(original_input_image, plt.cm.gray)
    axs[1].imshow(post_process(image1), plt.cm.gray)
    axs[2].imshow(post_process(image2), plt.cm.gray)
    plt.show()


# def test_model():
#     model = load_model(MODEL_FILE_NAME + '.h5', compile=False)
#     model.compile(loss=binary_crossentropy, optimizer=Adam())
#
#     x_test = load_cached('x_test.pickle')
#     y_test = load_cached('y_test.pickle')
#
#     test_data_generator = DataGenerator(x_test, y_test, 1)
#
#     input_tensor, _ = test_data_generator[5]
#
#     output_tensor = model.predict(input_tensor)
#     output_tensor = output_tensor.reshape(2, INPUT_SHAPE[0], INPUT_SHAPE[1])
#
#     image1 = output_tensor[0]
#     image2 = output_tensor[1]
#
#     input_image = input_tensor.reshape(INPUT_SHAPE[0], INPUT_SHAPE[1])
#
#     fig, axs = plt.subplots(3, 1)
#
#     axs[0].imshow(post_process(input_image), plt.cm.gray)
#     axs[1].imshow(post_process(image1), plt.cm.gray)
#     axs[2].imshow(post_process(image2), plt.cm.gray)
#     plt.show()


if __name__ == '__main__':
    # generate_images()
    train_model()
    # evaluate_model()
    # test_model()

