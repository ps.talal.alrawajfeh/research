import gzip
import itertools
import mimetypes
import multiprocessing
import os
import pickle
import random
from concurrent.futures import ThreadPoolExecutor
from multiprocessing import Lock

import cv2
import numpy as np
from PIL import Image, ImageDraw, ImageFont
from scipy.interpolate import interp1d
from tqdm import tqdm

ENGLISH_FONTS_DIR = '/home/u764/Development/data/english-fonts'
ARABIC_FONTS_DIR = '/home/u764/Development/data/arabic-fonts'
MICR_FONTS_DIR = ''
GREY_BACKGROUNDS_DIRS = ['/home/u764/Development/data/BBK-Cropped-Refined']
BINARY_BACKGROUNDS_DIRS = ['/home/u764/Development/data/CAB-Cleaned-Cropped', '/home/u764/Development/data/ABP-Cleaned-Cropped']
SIGNATURES_DIR = '/home/u764/Development/data/signatures'
ENGLISH_HANDWRITTEN_WORDS_DIR = '/home/u764/Development/data/words-dataset/filtered-english-words'
ARABIC_HANDWRITTEN_WORDS_DIR = '/home/u764/Development/data/words-dataset/filtered-arabic-words'
ENGLISH_TEXTS_FILE = './english_text.txt'
ARABIC_TEXTS_FILE = './arabic_text.txt'

INPUT_IMAGES_PATH = './input'
OUTPUT_IMAGES_PATH = './output'
DATASET_SIZE = 10000
INPUT_SHAPE = (224, 416)
MAX_SIGNATURE_RECT_INTERSECTION = 0.25


def to_int(number):
    return int(round(number))


def get_extensions_for_type(general_type):
    for ext in mimetypes.types_map:
        if mimetypes.types_map[ext].split('/')[0] == general_type:
            yield ext.lower()


def memoize(function):
    memo = {}

    def wrapper(*args):
        if args in memo:
            return memo[args]
        else:
            rv = function(*args)
            memo[args] = rv
            return rv

    return wrapper


@memoize
def get_image_extensions():
    return tuple(get_extensions_for_type('image'))


def file_extension(path):
    return os.path.splitext(os.path.basename(path))[1].lower()


def is_image(path):
    return file_extension(path) in get_image_extensions()


def read_image_grayscale(image_path):
    return cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)


def threshold_otsu(image):
    return cv2.threshold(image,
                         0,
                         255,
                         cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]


def otsu_threshold_value(image):
    return cv2.threshold(image,
                         0,
                         255,
                         cv2.THRESH_BINARY + cv2.THRESH_OTSU)[0]


def correct_image_values(image):
    image[image > 255] = 255
    image[image < 0] = 0


def change_brightness(image, delta):
    result = image.astype(np.int32)
    result += delta
    correct_image_values(result)
    return result.astype(np.uint8)


def change_contrast(image, coefficient):
    result = image.astype(np.float64)
    result *= coefficient
    correct_image_values(result)
    return result.astype(np.uint8)


def rotate(image, angle):
    (height, width) = image.shape[:2]
    center_x, center_y = width / 2, height / 2
    rotation_matrix = cv2.getRotationMatrix2D((center_x, center_y),
                                              angle,
                                              1.0)
    cos = np.abs(rotation_matrix[0, 0])
    sin = np.abs(rotation_matrix[0, 1])
    new_width = height * sin + width * cos
    new_height = height * cos + width * sin
    rotation_matrix[0, 2] += new_width / 2 - center_x
    rotation_matrix[1, 2] += new_height / 2 - center_y
    return cv2.warpAffine(image,
                          rotation_matrix,
                          (to_int(new_width), to_int(new_height)),
                          borderValue=(255, 255, 255),
                          flags=cv2.INTER_CUBIC)


def compress_image(image, quality):
    encoded = cv2.imencode('.jpg', image, [int(cv2.IMWRITE_JPEG_QUALITY), quality])[1]
    return cv2.imdecode(encoded, cv2.IMREAD_GRAYSCALE)


def random_coordinate(image):
    return random.randint(0, image.shape[0] - 1), random.randint(0, image.shape[1] - 1)


def speckle_image(image, fraction, color=0):
    coordinates = [random_coordinate(image)
                   for _ in range(to_int(image.size * fraction))]
    image_copy = np.array(image)
    for coord in coordinates:
        image_copy[coord[0], coord[1]] = color
    return image_copy


def crop_foreground(binary_image):
    mask = (255 - binary_image) > 0

    height, width = binary_image.shape[0:2]
    mask1, mask2 = mask.any(0), mask.any(1)
    x1, x2 = mask1.argmax(), width - mask1[::-1].argmax()
    y1, y2 = mask2.argmax(), height - mask2[::-1].argmax()

    return binary_image[y1:y2, x1:x2]


def crop_foreground_non_binary(image, threshold=None):
    if threshold is None:
        binary_image = threshold_otsu(image)
    else:
        binary_image = np.array(image)
        binary_image[binary_image < threshold] = 0
        binary_image[binary_image >= threshold] = 255

    mask = (255 - binary_image) > 0

    height, width = binary_image.shape[0:2]
    mask1, mask2 = mask.any(0), mask.any(1)
    x1, x2 = mask1.argmax(), width - mask1[::-1].argmax()
    y1, y2 = mask2.argmax(), height - mask2[::-1].argmax()

    return image[y1:y2, x1:x2]


def random_bool():
    return random.choice([0, 1]) == 0


def random_blur(image):
    if random_bool():
        return image

    blur_lambdas = [lambda img: cv2.blur(img, (3, 3)),
                    lambda img: cv2.medianBlur(img, 3),
                    lambda img: cv2.GaussianBlur(img, (3, 3), 0)]

    return random.choice(blur_lambdas)(image)


def random_add_gaussian_noise(image, mean=0, variance=1, interval=None):
    if random_bool():
        return image
    width, height = image.shape
    gaussian = np.random.normal(mean,
                                variance,
                                (width, height)).astype(np.float64)
    if interval is not None:
        gaussian[gaussian < interval[0]] = interval[0]
        gaussian[gaussian > interval[1]] = interval[1]
    gaussian = image + gaussian
    gaussian[gaussian > 255] = 255
    gaussian[gaussian < 0] = 0
    return gaussian.astype(np.uint8)


def print_text_cropped(text,
                       font_path,
                       desired_font_size,
                       foreground_color,
                       background_size_upper_bound):
    width_upper_bound, height_upper_bound = background_size_upper_bound

    width, height, font = None, None, None
    font_size = desired_font_size
    while font_size > 0:
        font = ImageFont.truetype(font_path, font_size)
        width, height = font.getsize(text)
        if height <= height_upper_bound and width <= width_upper_bound:
            break
        font_size -= 1

    if font_size <= 0:
        raise Exception('font size must not be zero or negative')

    image = np.ones((height, width), np.uint8) * 255
    image = Image.fromarray(image, 'L')
    draw = ImageDraw.Draw(image)
    draw.text((0, 0), text, fill=foreground_color, font=font)

    return crop_foreground_non_binary(np.array(image, np.uint8), threshold=255)


def randomize_foreground(image,
                         min_color,
                         max_color):
    binary = threshold_otsu(image)
    mask = (binary == 255)
    result = np.random.randint(min_color, max_color, image.shape).astype(np.uint8)
    result[mask] = 255
    return result


def insert_foreground(image,
                      foreground,
                      position):
    x, y = position
    image_part = image[y:y + foreground.shape[0], x:x + foreground.shape[1]]
    foreground_mask = np.array(foreground < 255, np.uint8)
    result = (1 - foreground_mask) * image_part + foreground_mask * foreground
    image[y:y + foreground.shape[0], x:x + foreground.shape[1]] = result


def image_weighted_average(image1, image2, alpha):
    result = alpha * image1.astype(np.float64) + (1 - alpha) * image2.astype(np.float64)
    correct_image_values(result)
    return result.astype(np.uint8)


def extract_foreground(image):
    result = np.array(image)
    binary = threshold_otsu(result)
    binary = cv2.erode(binary, np.ones((3, 3), np.uint8))
    mask = (binary == 255)
    result[mask] = 255
    return result


def blend_foreground_on_background(image,
                                   foreground,
                                   position,
                                   alpha=0.5):
    x, y = position
    width, height = foreground.shape[1], foreground.shape[0]
    background_part = image[y:y + height, x:x + width]
    background_mean = np.mean(background_part)
    mask1 = np.array(foreground == 0, np.uint8)

    mask2 = (foreground > 0) & (foreground < 255)
    non_zero_foreground = foreground[mask2]
    min_non_zero_foreground = np.min(non_zero_foreground)
    interpolator = interp1d([min_non_zero_foreground, np.max(non_zero_foreground)],
                            [min_non_zero_foreground, background_mean])

    foreground[mask2] = interpolator(non_zero_foreground)

    mask2 = np.array(mask2, np.uint8)
    mask3 = np.array(foreground == 255, np.uint8)

    blended = image_weighted_average(mask2 * background_part, mask2 * foreground, alpha)
    result = mask3 * background_part + blended + mask1 * foreground
    image[y:y + height, x:x + width] = result


def change_foreground_color_and_blend_foreground_on_background(image,
                                                               foreground,
                                                               position,
                                                               new_foreground_color,
                                                               alpha=0.3):
    x, y = position
    width, height = foreground.shape[1], foreground.shape[0]
    background_part = image[y:y + height, x:x + width]
    background_mean = np.mean(background_part)
    mask1 = np.array(foreground == 0, np.uint8)

    mask2 = (foreground > 0) & (foreground < 255)
    if np.any(mask2):
        non_zero_foreground = foreground[mask2]
        min_non_zero_foreground = np.min(non_zero_foreground)
        new_min_non_zero_foreground = new_foreground_color + min_non_zero_foreground
        if new_min_non_zero_foreground > 255:
            new_min_non_zero_foreground = 255

        interpolator = interp1d([min_non_zero_foreground, np.max(non_zero_foreground)],
                                [new_min_non_zero_foreground, background_mean])

        foreground[mask2] = interpolator(non_zero_foreground)

        mask2 = np.array(mask2, np.uint8)
        blended = image_weighted_average(mask2 * background_part, mask2 * foreground, alpha)
    else:
        blended = np.zeros(foreground.shape, np.uint8)

    mask3 = np.array(foreground == 255, np.uint8)
    result = mask3 * background_part + blended + mask1 * new_foreground_color
    image[y:y + height, x:x + width] = result


class InfiniteSampler:
    def __init__(self, items):
        self.items = items
        self.indices = self._get_random_indices()

    def next(self):
        if len(self.indices) == 0:
            self.indices = self._get_random_indices()
        index = self.indices.pop()
        return self.items[index]

    def _get_random_indices(self):
        indices = list(range(len(self.items)))
        random.shuffle(indices)
        return indices


def read_lines(file_path):
    with open(file_path, 'r') as f:
        return list(filter(lambda x: x != '', [line.strip() for line in f.readlines()]))


def alter_image_brightness_and_contrast_randomly(image,
                                                 delta_range=(-25, 25),
                                                 coefficient_range=(0.75, 1.25)):
    if random.choice([0, 1]) == 0:
        return image
    choice = random.choice([0, 1, 2, 3])
    if choice == 0:
        return change_brightness(image,
                                 random.randint(delta_range[0],
                                                delta_range[1]))
    if choice == 1:
        return change_contrast(image,
                               random.uniform(coefficient_range[0],
                                              coefficient_range[1]))
    if choice == 2:
        return change_contrast(change_brightness(image,
                                                 random.randint(delta_range[0],
                                                                delta_range[1])),
                               random.uniform(coefficient_range[0],
                                              coefficient_range[1]))
    return change_brightness(change_contrast(image,
                                             random.uniform(coefficient_range[0],
                                                            coefficient_range[1])),
                             random.randint(delta_range[0],
                                            delta_range[1]))


def alter_signature(image,
                    width_range,
                    height_range,
                    angle_range=(-60.0, 60.0)):
    if random_bool():
        rotated = rotate(image, random.uniform(angle_range[0], angle_range[1]))
    else:
        rotated = image

    width = random.randint(width_range[0], width_range[1])
    height = random.randint(height_range[0], height_range[1])
    return cv2.resize(rotated, (width, height), interpolation=cv2.INTER_CUBIC)


#
# def insert_random_signatures(signatures_foreground, signature_sampler):
#     number_of_signatures = random.choice([1, 2, 3, 4])
#     if number_of_signatures == 1:
#         signature = signature_sampler.next()
#         signature = alter_signature(signature,
#                                     width_range=(to_int(INPUT_SHAPE[1] * 0.25), to_int(INPUT_SHAPE[1] * 0.75)),
#                                     height_range=(to_int(INPUT_SHAPE[0] * 0.25), to_int(INPUT_SHAPE[0] * 0.75)))
#         width = signature.shape[1]
#         height = signature.shape[0]
#         x = random.randint(0, INPUT_SHAPE[1] - width)
#         y = random.randint(0, INPUT_SHAPE[0] - height)
#         signatures_foreground[y:y + height, x: x + width] = signature
#         return (np.array(signatures_foreground),
#                 np.ones(INPUT_SHAPE, np.uint8) * 255,
#                 np.ones(INPUT_SHAPE, np.uint8) * 255,
#                 np.ones(INPUT_SHAPE, np.uint8) * 255)
#     elif number_of_signatures == 2:
#         # configuration 1 is a 1 x 2 grid and configuration 2 is a 2 x 1 grid
#         configuration = random.choice([1, 2])
#         signature1 = signature_sampler.next()
#         signature2 = signature_sampler.next()
#
#         if configuration == 1:
#             cell_width = INPUT_SHAPE[1] / 2
#             cell_height = INPUT_SHAPE[0]
#             min_width = to_int(cell_width / 2)
#             max_width = to_int(cell_width * (1.0 + MAX_SIGNATURE_RECT_INTERSECTION))
#             min_height = to_int(cell_height / 2)
#             max_height = to_int(cell_height)
#         else:
#             cell_width = INPUT_SHAPE[1]
#             cell_height = INPUT_SHAPE[0] / 2
#             min_width = to_int(cell_width / 2)
#             max_width = to_int(cell_width)
#             min_height = to_int(cell_height / 2)
#             max_height = to_int(cell_height * (1.0 + MAX_SIGNATURE_RECT_INTERSECTION))
#
#         signature1 = alter_signature(signature1,
#                                      width_range=(min_width, max_width),
#                                      height_range=(min_height, max_height))
#
#         signature2 = alter_signature(signature2,
#                                      width_range=(min_width, max_width),
#                                      height_range=(min_height, max_height))
#
#         width1 = signature1.shape[1]
#         height1 = signature1.shape[0]
#         width2 = signature2.shape[1]
#         height2 = signature2.shape[0]
#
#         x1 = random.randint(0, max_width - width1)
#         y1 = random.randint(0, max_height - height1)
#         x2 = random.randint(INPUT_SHAPE[1] - max_width, INPUT_SHAPE[1] - width2)
#         y2 = random.randint(INPUT_SHAPE[0] - max_height, INPUT_SHAPE[0] - height2)
#
#         signatures_foreground[y1:y1 + height1, x1:x1 + width1] = signature1
#         signatures_foreground[y2:y2 + height2, x2:x2 + width2] &= signature2
#
#         signature1_output = np.ones(INPUT_SHAPE, np.uint8) * 255
#         signature1_output[y1:y1 + height1, x1:x1 + width1] = signature1
#         signature2_output = np.ones(INPUT_SHAPE, np.uint8) * 255
#         signature2_output[y2:y2 + height2, x2:x2 + width2] = signature2
#
#         return (signature1_output,
#                 signature2_output,
#                 np.ones(INPUT_SHAPE, np.uint8) * 255,
#                 np.ones(INPUT_SHAPE, np.uint8) * 255)
#     elif number_of_signatures == 3:
#         # configuration 1 is a 1 x 3 grid and configuration 2 is a 2 x 2 grid
#         configuration = random.choice([1, 2])
#         signature1 = signature_sampler.next()
#         signature2 = signature_sampler.next()
#         signature3 = signature_sampler.next()
#
#         if configuration == 1:
#             cell_width = INPUT_SHAPE[1] / 3
#             cell_height = INPUT_SHAPE[0]
#             min_width = to_int(cell_width / 2)
#             max_width = to_int(cell_width * (1.0 + MAX_SIGNATURE_RECT_INTERSECTION))
#             min_height = to_int(cell_height / 2)
#             max_height = to_int(cell_height)
#         else:
#             cell_width = INPUT_SHAPE[1] / 2
#             cell_height = INPUT_SHAPE[0] / 2
#             min_width = to_int(cell_width / 2)
#             max_width = to_int(cell_width * (1.0 + MAX_SIGNATURE_RECT_INTERSECTION))
#             min_height = to_int(cell_height / 2)
#             max_height = to_int(cell_height * (1.0 + MAX_SIGNATURE_RECT_INTERSECTION))
#
#         signature1 = alter_signature(signature1,
#                                      width_range=(min_width, max_width),
#                                      height_range=(min_height, max_height))
#
#         signature2 = alter_signature(signature2,
#                                      width_range=(min_width, max_width),
#                                      height_range=(min_height, max_height))
#
#         signature3 = alter_signature(signature3,
#                                      width_range=(min_width, max_width),
#                                      height_range=(min_height, max_height))
#
#         width1 = signature1.shape[1]
#         height1 = signature1.shape[0]
#         width2 = signature2.shape[1]
#         height2 = signature2.shape[0]
#         width3 = signature3.shape[1]
#         height3 = signature3.shape[0]
#
#         x1 = random.randint(0, max_width - width1)
#         y1 = random.randint(0, max_height - height1)
#
#         if configuration == 1:
#             min_x2 = to_int((INPUT_SHAPE[1] - max_width) / 2)
#             x2 = random.randint(min_x2, min_x2 + max_width - width2)
#             y2 = random.randint(0, max_height - height2)
#             x3 = random.randint(INPUT_SHAPE[1] - max_width, INPUT_SHAPE[1] - width3)
#             y3 = random.randint(0, max_height - height3)
#         else:
#             x2 = random.randint(INPUT_SHAPE[1] - max_width, INPUT_SHAPE[1] - width2)
#             y2 = random.randint(0, max_height - height2)
#             x3 = random.randint(0, INPUT_SHAPE[1] - width3)
#             y3 = random.randint(INPUT_SHAPE[0] - max_height, INPUT_SHAPE[0] - height3)
#
#         signatures_foreground[y1:y1 + height1, x1:x1 + width1] = signature1
#         signatures_foreground[y2:y2 + height2, x2:x2 + width2] &= signature2
#         signatures_foreground[y3:y3 + height3, x3:x3 + width3] &= signature3
#
#         signature1_output = np.ones(INPUT_SHAPE, np.uint8) * 255
#         signature1_output[y1:y1 + height1, x1:x1 + width1] = signature1
#         signature2_output = np.ones(INPUT_SHAPE, np.uint8) * 255
#         signature2_output[y2:y2 + height2, x2:x2 + width2] = signature2
#         signature3_output = np.ones(INPUT_SHAPE, np.uint8) * 255
#         signature3_output[y3:y3 + height3, x3:x3 + width3] = signature3
#
#         return (signature1_output,
#                 signature2_output,
#                 signature3_output,
#                 np.ones(INPUT_SHAPE, np.uint8) * 255)
#     else:
#         signature1 = signature_sampler.next()
#         signature2 = signature_sampler.next()
#         signature3 = signature_sampler.next()
#         signature4 = signature_sampler.next()
#
#         cell_width = INPUT_SHAPE[1] / 2
#         cell_height = INPUT_SHAPE[0] / 2
#         min_width = to_int(cell_width / 2)
#         max_width = to_int(cell_width * (1.0 + MAX_SIGNATURE_RECT_INTERSECTION))
#         min_height = to_int(cell_height / 2)
#         max_height = to_int(cell_height * (1.0 + MAX_SIGNATURE_RECT_INTERSECTION))
#
#         signature1 = alter_signature(signature1,
#                                      width_range=(min_width, max_width),
#                                      height_range=(min_height, max_height))
#
#         signature2 = alter_signature(signature2,
#                                      width_range=(min_width, max_width),
#                                      height_range=(min_height, max_height))
#
#         signature3 = alter_signature(signature3,
#                                      width_range=(min_width, max_width),
#                                      height_range=(min_height, max_height))
#
#         signature4 = alter_signature(signature4,
#                                      width_range=(min_width, max_width),
#                                      height_range=(min_height, max_height))
#
#         width1 = signature1.shape[1]
#         height1 = signature1.shape[0]
#         width2 = signature2.shape[1]
#         height2 = signature2.shape[0]
#         width3 = signature3.shape[1]
#         height3 = signature3.shape[0]
#         width4 = signature4.shape[1]
#         height4 = signature4.shape[0]
#
#         x1 = random.randint(0, max_width - width1)
#         y1 = random.randint(0, max_height - height1)
#         x2 = random.randint(INPUT_SHAPE[1] - max_width, INPUT_SHAPE[1] - width2)
#         y2 = random.randint(0, max_height - height2)
#         x3 = random.randint(0, max_width - width3)
#         y3 = random.randint(INPUT_SHAPE[0] - max_height, INPUT_SHAPE[0] - height3)
#         x4 = random.randint(INPUT_SHAPE[1] - max_width, INPUT_SHAPE[1] - width4)
#         y4 = random.randint(INPUT_SHAPE[0] - max_height, INPUT_SHAPE[0] - height4)
#
#         signatures_foreground[y1:y1 + height1, x1:x1 + width1] = signature1
#         signatures_foreground[y2:y2 + height2, x2:x2 + width2] &= signature2
#         signatures_foreground[y3:y3 + height3, x3:x3 + width3] &= signature3
#         signatures_foreground[y4:y4 + height4, x4:x4 + width4] &= signature4
#
#         signature1_output = np.ones(INPUT_SHAPE, np.uint8) * 255
#         signature1_output[y1:y1 + height1, x1:x1 + width1] = signature1
#         signature2_output = np.ones(INPUT_SHAPE, np.uint8) * 255
#         signature2_output[y2:y2 + height2, x2:x2 + width2] = signature2
#         signature3_output = np.ones(INPUT_SHAPE, np.uint8) * 255
#         signature3_output[y3:y3 + height3, x3:x3 + width3] = signature3
#         signature4_output = np.ones(INPUT_SHAPE, np.uint8) * 255
#         signature4_output[y4:y4 + height4, x4:x4 + width4] = signature4
#
#         return (signature1_output,
#                 signature2_output,
#                 signature3_output,
#                 signature4_output)


def insert_random_signatures(signatures_foreground, signature_sampler):
    number_of_signatures = random.choice([1, 2, 3])
    if number_of_signatures == 1:
        signature = signature_sampler.next()
        signature = alter_signature(signature,
                                    width_range=(to_int(INPUT_SHAPE[1] * 0.25), to_int(INPUT_SHAPE[1] * 0.75)),
                                    height_range=(to_int(INPUT_SHAPE[0] * 0.25), to_int(INPUT_SHAPE[0] * 0.75)))
        width = signature.shape[1]
        height = signature.shape[0]
        x = random.randint(0, INPUT_SHAPE[1] - width)
        y = random.randint(0, INPUT_SHAPE[0] - height)
        signatures_foreground[y:y + height, x: x + width] = signature
        return (np.array(signatures_foreground),
                np.ones(INPUT_SHAPE, np.uint8) * 255,
                np.ones(INPUT_SHAPE, np.uint8) * 255)
    elif number_of_signatures == 2:
        cell_width = INPUT_SHAPE[1] / 2
        cell_height = INPUT_SHAPE[0]
        min_width = to_int(cell_width / 2)
        max_width = to_int(cell_width * (1.0 + MAX_SIGNATURE_RECT_INTERSECTION))
        min_height = to_int(cell_height / 2)
        max_height = to_int(cell_height)

        signature_providers = [lambda: signature_sampler.next(), lambda: signature_sampler.next()]
        indices = list(range(len(signature_providers)))
        signature_indices = random.choice(list(itertools.combinations(indices, random.randint(0, len(indices) - 1))))
        for index in signature_indices:
            signature_providers[index] = lambda: np.ones((min_width, min_height), np.uint8) * 255

        signature1 = signature_providers[0]()
        signature2 = signature_providers[1]()

        signature1 = alter_signature(signature1,
                                     width_range=(min_width, max_width),
                                     height_range=(min_height, max_height))

        signature2 = alter_signature(signature2,
                                     width_range=(min_width, max_width),
                                     height_range=(min_height, max_height))

        width1 = signature1.shape[1]
        height1 = signature1.shape[0]
        width2 = signature2.shape[1]
        height2 = signature2.shape[0]

        x1 = random.randint(0, max_width - width1)
        y1 = random.randint(0, max_height - height1)
        x2 = random.randint(INPUT_SHAPE[1] - max_width, INPUT_SHAPE[1] - width2)
        y2 = random.randint(INPUT_SHAPE[0] - max_height, INPUT_SHAPE[0] - height2)

        signatures_foreground[y1:y1 + height1, x1:x1 + width1] = signature1
        signatures_foreground[y2:y2 + height2, x2:x2 + width2] &= signature2

        signature1_output = np.ones(INPUT_SHAPE, np.uint8) * 255
        signature1_output[y1:y1 + height1, x1:x1 + width1] = signature1
        signature2_output = np.ones(INPUT_SHAPE, np.uint8) * 255
        signature2_output[y2:y2 + height2, x2:x2 + width2] = signature2

        result = [signature1_output,
                  signature2_output,
                  None]
        for index in signature_indices:
            result[index] = None
        result = [s for s in result if s is not None]
        while len(result) < 3:
            result.append(np.ones(INPUT_SHAPE, np.uint8) * 255)
        return result
    else:
        cell_width = INPUT_SHAPE[1] / 3
        cell_height = INPUT_SHAPE[0]
        min_width = to_int(cell_width / 2)
        max_width = to_int(cell_width * (1.0 + MAX_SIGNATURE_RECT_INTERSECTION))
        min_height = to_int(cell_height / 2)
        max_height = to_int(cell_height)

        signature_providers = [lambda: signature_sampler.next(),
                               lambda: signature_sampler.next(),
                               lambda: signature_sampler.next()]

        indices = list(range(len(signature_providers)))
        signature_indices = random.choice(list(itertools.combinations(indices, random.randint(0, len(indices) - 1))))
        for index in signature_indices:
            signature_providers[index] = lambda: np.ones((min_width, min_height), np.uint8) * 255

        signature1 = signature_providers[0]()
        signature2 = signature_providers[1]()
        signature3 = signature_providers[2]()

        signature1 = alter_signature(signature1,
                                     width_range=(min_width, max_width),
                                     height_range=(min_height, max_height))

        signature2 = alter_signature(signature2,
                                     width_range=(min_width, max_width),
                                     height_range=(min_height, max_height))

        signature3 = alter_signature(signature3,
                                     width_range=(min_width, max_width),
                                     height_range=(min_height, max_height))

        width1 = signature1.shape[1]
        height1 = signature1.shape[0]
        width2 = signature2.shape[1]
        height2 = signature2.shape[0]
        width3 = signature3.shape[1]
        height3 = signature3.shape[0]

        x1 = random.randint(0, max_width - width1)
        y1 = random.randint(0, max_height - height1)
        min_x2 = to_int((INPUT_SHAPE[1] - max_width) / 2)
        x2 = random.randint(min_x2, min_x2 + max_width - width2)
        y2 = random.randint(0, max_height - height2)
        x3 = random.randint(INPUT_SHAPE[1] - max_width, INPUT_SHAPE[1] - width3)
        y3 = random.randint(0, max_height - height3)

        signatures_foreground[y1:y1 + height1, x1:x1 + width1] = signature1
        signatures_foreground[y2:y2 + height2, x2:x2 + width2] &= signature2
        signatures_foreground[y3:y3 + height3, x3:x3 + width3] &= signature3

        signature1_output = np.ones(INPUT_SHAPE, np.uint8) * 255
        signature1_output[y1:y1 + height1, x1:x1 + width1] = signature1
        signature2_output = np.ones(INPUT_SHAPE, np.uint8) * 255
        signature2_output[y2:y2 + height2, x2:x2 + width2] = signature2
        signature3_output = np.ones(INPUT_SHAPE, np.uint8) * 255
        signature3_output[y3:y3 + height3, x3:x3 + width3] = signature3

        result = [signature1_output,
                  signature2_output,
                  signature3_output]
        for index in signature_indices:
            result[index] = None
        result = [s for s in result if s is not None]
        while len(result) < 3:
            result.append(np.ones(INPUT_SHAPE, np.uint8) * 255)
        return result


def draw_random_lines(background,
                      color_lower_limit,
                      color_upper_limit,
                      horizontal_lines_count_range=(1, 2),
                      vertical_lines_count_range=(1, 2),
                      dx_range=(-3, 3),
                      dy_range=(-3, 3),
                      x_margin_range=(20, -20),
                      y_margin_range=(20, -20)):
    lines = []

    horizontal_lines = random.randint(horizontal_lines_count_range[0], horizontal_lines_count_range[1])
    for _ in range(horizontal_lines):
        x1 = random.randint(0, x_margin_range[0])
        y1 = random.randint(y_margin_range[0], INPUT_SHAPE[0] + y_margin_range[1])
        x2 = random.randint(INPUT_SHAPE[1] + x_margin_range[1], INPUT_SHAPE[1])
        y2 = random.randint(y1 + dy_range[0], y1 + dy_range[1])
        lines.append((x1, y1, x2, y2))

    vertical_lines = random.randint(vertical_lines_count_range[0], vertical_lines_count_range[1])
    for _ in range(vertical_lines):
        x1 = random.randint(x_margin_range[0], INPUT_SHAPE[1] + x_margin_range[1])
        y1 = random.randint(0, y_margin_range[0])
        x2 = random.randint(x1 + dx_range[0], x1 + dx_range[1])
        y2 = random.randint(INPUT_SHAPE[0] + y_margin_range[1], INPUT_SHAPE[0])
        lines.append((x1, y1, x2, y2))

    for x1, y1, x2, y2 in lines:
        cv2.line(background, (x1, y1), (x2, y2),
                 random.randint(color_lower_limit, color_upper_limit),
                 random.randint(1, 2),
                 random.choice([cv2.LINE_4,
                                cv2.LINE_8,
                                cv2.LINE_AA]))


def resize_crop_randomly(background, max_resize_factor=1.5):
    if random_bool():
        resize_factor = random.uniform(INPUT_SHAPE[0] / background.shape[0], max_resize_factor)
        background = cv2.resize(background,
                                None,
                                fx=resize_factor,
                                fy=resize_factor,
                                interpolation=cv2.INTER_CUBIC)
    x = random.randint(0, max(0, background.shape[1] - INPUT_SHAPE[1]))
    y = random.randint(0, max(0, background.shape[0] - INPUT_SHAPE[0]))
    background = background[y: y + INPUT_SHAPE[0], x:x + INPUT_SHAPE[1]]
    return background


class SignatureHandWrittenTextSampler:
    def __init__(self,
                 signature_sampler,
                 english_handwritten_words_sampler,
                 arabic_handwritten_words_sampler):
        self.signature_sampler = signature_sampler
        self.english_handwritten_words_sampler = english_handwritten_words_sampler
        self.arabic_handwritten_words_sampler = arabic_handwritten_words_sampler

    def next(self):
        if random.choice([1, 2, 3]) < 3:
            return self.signature_sampler.next()
        if random_bool():
            return self.english_handwritten_words_sampler.next()
        return self.arabic_handwritten_words_sampler.next()


def generate_sample(grey_backgrounds_samplers,
                    binary_backgrounds_sampler,
                    signature_sampler,
                    english_handwritten_words_sampler,
                    arabic_handwritten_words_sampler,
                    english_font_path_sampler,
                    arabic_font_path_sampler,
                    english_text_sampler,
                    arabic_text_sampler):
    background_type = random.choice([1, 2, 3, 4])
    if background_type in [1, 2]:
        background = random.choice(grey_backgrounds_samplers).next()
        background = resize_crop_randomly(background)
    elif background_type == 3:
        background = random.choice(binary_backgrounds_sampler).next()
        background = resize_crop_randomly(background)
        background = threshold_otsu(background)
        if random_bool():
            color_split = random.randint(100, 255)
            foreground_mask = (background == 0)
            background_mask = ~foreground_mask
            color1 = random.randint(color_split, 255)
            color2 = random.randint(color1, 255)
            random_background = np.random.uniform(color1, color2, background.shape).astype(np.uint8)
            color1 = random.randint(0, color_split)
            color2 = random.randint(color1, color_split)
            random_foreground = np.random.uniform(color1, color2, background.shape).astype(np.uint8)
            foreground_mask = np.array(foreground_mask, np.uint8)
            background_mask = np.array(background_mask, np.uint8)
            background = foreground_mask * random_foreground + background_mask * random_background
    else:
        if random_bool():
            background = np.ones(INPUT_SHAPE, np.uint8) * 255
        else:
            min_color = random.randint(100, 255)
            background = np.random.uniform(min_color, random.randint(min_color, 255), INPUT_SHAPE).astype(np.uint8)

    background = cv2.resize(background, (INPUT_SHAPE[1], INPUT_SHAPE[0]), interpolation=cv2.INTER_CUBIC)
    background = alter_image_brightness_and_contrast_randomly(background)

    signatures_foreground = np.ones(INPUT_SHAPE, np.uint8) * 255
    signature_handwritten_text_sampler = SignatureHandWrittenTextSampler(signature_sampler,
                                                                         english_handwritten_words_sampler,
                                                                         arabic_handwritten_words_sampler)
    output_channels = insert_random_signatures(signatures_foreground,
                                               signature_handwritten_text_sampler)
    output_channels = [threshold_otsu(c) for c in output_channels]
    all_signatures_channel = np.ones(INPUT_SHAPE, np.uint8) * 255
    for output_channel in output_channels:
        all_signatures_channel &= output_channel
    output_channels.append(all_signatures_channel)

    # language = random.choice([1, 2])
    # if language == 1:  # english
    #     text = english_text_sampler.next()
    #     font_path = english_font_path_sampler.next()
    # else:  # arabic
    #     text = arabic_text_sampler.next()
    #     font_path = arabic_font_path_sampler.next()
    # printed_text = print_text_cropped(text,
    #                                   font_path,
    #                                   random.randint(11, 20),
    #                                   0,
    #                                   (INPUT_SHAPE[1] // 2, INPUT_SHAPE[0] // 2))
    # if random_bool():
    #     printed_text = rotate(printed_text, random.uniform(-3, 3))

    background_mean = np.mean(background)
    background_std = np.std(background)
    color_upper_limit = max(min(to_int(background_mean - background_std), to_int(background_mean - 5)), 0)
    color_lower_limit = random.randint(0, max(min(to_int(background_mean - 2 * background_std),
                                                  to_int(background_mean - 10)), 0))

    draw_lines_before_signatures_and_text = random_bool()
    if draw_lines_before_signatures_and_text and random_bool():
        draw_random_lines(background, color_lower_limit, color_upper_limit)

    foreground_color_type = random.choice([1, 2, 3])
    if foreground_color_type == 1:
        signatures_foreground = change_brightness(threshold_otsu(signatures_foreground),
                                                  random.randint(color_lower_limit, color_upper_limit))
        # printed_text = change_brightness(threshold_otsu(printed_text),
        #                                  random.randint(color_lower_limit, color_upper_limit))

        # insert_foreground(background,
        #                   printed_text,
        #                   (random.randint(0, INPUT_SHAPE[1] - printed_text.shape[1]),
        #                    random.randint(0, INPUT_SHAPE[0] - printed_text.shape[0])))

        insert_foreground(background,
                          signatures_foreground,
                          (0, 0))
    elif foreground_color_type == 2:
        # change_foreground_color_and_blend_foreground_on_background(background,
        #                                                            printed_text,
        #                                                            (random.randint(0, INPUT_SHAPE[1] -
        #                                                                            printed_text.shape[1]),
        #                                                             random.randint(0, INPUT_SHAPE[0] -
        #                                                                            printed_text.shape[0])),
        #                                                            random.randint(color_lower_limit,
        #                                                                           color_upper_limit))
        signatures_foreground = extract_foreground(signatures_foreground)
        change_foreground_color_and_blend_foreground_on_background(background,
                                                                   signatures_foreground,
                                                                   (0, 0),
                                                                   random.randint(color_lower_limit,
                                                                                  color_upper_limit))
    else:
        signatures_foreground = randomize_foreground(signatures_foreground, color_lower_limit, color_upper_limit)
        # printed_text = randomize_foreground(printed_text, color_lower_limit, color_upper_limit)

        # insert_foreground(background,
        #                   printed_text,
        #                   (random.randint(0, INPUT_SHAPE[1] - printed_text.shape[1]),
        #                    random.randint(0, INPUT_SHAPE[0] - printed_text.shape[0])))

        insert_foreground(background,
                          signatures_foreground,
                          (0, 0))

    if not draw_lines_before_signatures_and_text and random_bool():
        draw_random_lines(background, color_lower_limit, color_upper_limit)

    if random_bool():
        if random_bool():
            background = random_add_gaussian_noise(background, 0, random.randint(10, 20), (-20, 20))
        else:
            background = speckle_image(background, 0.01, random.randint(0, 255))

        if random_bool():
            background = random_blur(background)
        else:
            background = compress_image(background, random.randint(50, 100))

    return background, output_channels


def main():
    if not os.path.isdir(INPUT_IMAGES_PATH):
        os.makedirs(INPUT_IMAGES_PATH)

    if not os.path.isdir(OUTPUT_IMAGES_PATH):
        os.makedirs(OUTPUT_IMAGES_PATH)

    grey_backgrounds_samplers = []
    for grey_backgrounds_dir in GREY_BACKGROUNDS_DIRS:
        grey_backgrounds_paths = [os.path.join(grey_backgrounds_dir, f) for f in os.listdir(grey_backgrounds_dir)]
        grey_backgrounds = [read_image_grayscale(p) for p in grey_backgrounds_paths if is_image(p)]
        grey_backgrounds_samplers.append(InfiniteSampler(grey_backgrounds))

    binary_backgrounds_samplers = []
    for binary_backgrounds_dir in BINARY_BACKGROUNDS_DIRS:
        binary_backgrounds_paths = [os.path.join(binary_backgrounds_dir, f) for f in os.listdir(binary_backgrounds_dir)]
        binary_backgrounds = [read_image_grayscale(p) for p in binary_backgrounds_paths if is_image(p)]
        binary_backgrounds_samplers.append(InfiniteSampler(binary_backgrounds))

    signatures_paths = [os.path.join(SIGNATURES_DIR, f) for f in os.listdir(SIGNATURES_DIR)]
    english_fonts_paths = [os.path.join(ENGLISH_FONTS_DIR, f) for f in os.listdir(ENGLISH_FONTS_DIR)]
    arabic_fonts_paths = [os.path.join(ARABIC_FONTS_DIR, f) for f in os.listdir(ARABIC_FONTS_DIR)]
    english_handwritten_words_paths = [os.path.join(ENGLISH_HANDWRITTEN_WORDS_DIR, f)
                                       for f in os.listdir(ENGLISH_HANDWRITTEN_WORDS_DIR)]
    arabic_handwritten_words_paths = [os.path.join(ARABIC_HANDWRITTEN_WORDS_DIR, f)
                                      for f in os.listdir(ARABIC_HANDWRITTEN_WORDS_DIR)]
    # micr_fonts_paths = [os.path.join(MICR_FONTS_DIR, f) for f in os.listdir(MICR_FONTS_DIR)]
    english_texts = read_lines(ENGLISH_TEXTS_FILE)
    arabic_texts = read_lines(ARABIC_TEXTS_FILE)

    signatures = [read_image_grayscale(p) for p in signatures_paths if is_image(p)]
    english_handwritten_words = [read_image_grayscale(p) for p in english_handwritten_words_paths]
    arabic_handwritten_words = [read_image_grayscale(p) for p in arabic_handwritten_words_paths]
    signature_sampler = InfiniteSampler(signatures)
    english_handwritten_words_sampler = InfiniteSampler(english_handwritten_words)
    arabic_handwritten_words_sampler = InfiniteSampler(arabic_handwritten_words)
    english_font_path_sampler = InfiniteSampler(english_fonts_paths)
    arabic_font_path_sampler = InfiniteSampler(arabic_fonts_paths)
    english_text_sampler = InfiniteSampler(english_texts)
    arabic_text_sampler = InfiniteSampler(arabic_texts)

    progress_bar = tqdm(total=DATASET_SIZE)
    # for i in range(DATASET_SIZE):
    #     background, output_channels = generate_sample(grey_background_sampler,
    #                                                   binary_background_sampler,
    #                                                   signature_sampler,
    #                                                   english_font_path_sampler,
    #                                                   arabic_font_path_sampler,
    #                                                   english_text_sampler,
    #                                                   arabic_text_sampler)
    #
    #     _, buffer = cv2.imencode('.png', 255 - background)
    #     with gzip.open(os.path.join(INPUT_IMAGES_PATH, f'{i}.dat'), 'wb') as f:
    #         f.write(buffer)
    #     with gzip.open(os.path.join(OUTPUT_IMAGES_PATH, f'{i}.dat'), 'wb') as f:
    #         pickle.dump([255 - c for c in output_channels], f)
    #     progress_bar.update()

    parallel_data_generation(DATASET_SIZE,
                             1,
                             progress_bar,
                             generate_sample,
                             grey_backgrounds_samplers,
                             binary_backgrounds_samplers,
                             signature_sampler,
                             english_handwritten_words_sampler,
                             arabic_handwritten_words_sampler,
                             english_font_path_sampler,
                             arabic_font_path_sampler,
                             english_text_sampler,
                             arabic_text_sampler)

    progress_bar.close()


def parallel_data_generation(output_data_count,
                             start_index,
                             progress_bar,
                             data_generation_function,
                             *function_args):
    progress_lock = Lock()
    indices = list(range(start_index, start_index + output_data_count))
    random.shuffle(indices)

    def wrapped_data_generation_function(*args):
        input_image, output_channels = data_generation_function(*args)

        with progress_lock:
            current_index = indices.pop()

        # _, buffer = cv2.imencode('.png', 255 - input_image)
        # with gzip.open(os.path.join(INPUT_IMAGES_PATH, f'{current_index}.dat'), 'wb') as f:
        #     f.write(buffer)
        # with gzip.open(os.path.join(OUTPUT_IMAGES_PATH, f'{current_index}.dat'), 'wb') as f:
        #     pickle.dump([255 - c for c in output_channels], f)
        cv2.imwrite(os.path.join(INPUT_IMAGES_PATH, f'{current_index}.png'), input_image)
        c_i = 1
        for c in output_channels:
            cv2.imwrite(os.path.join(OUTPUT_IMAGES_PATH, f'{current_index}_{c_i}.png'), c)
            c_i += 1

        with progress_lock:
            progress_bar.update()

    while len(indices) > 0:
        with ThreadPoolExecutor(max_workers=multiprocessing.cpu_count() * 2) as executor:
            for _ in range(len(indices)):
                executor.submit(wrapped_data_generation_function,
                                *function_args)


def clean_example():
    image = read_image_grayscale(ENGLISH_HANDWRITTEN_WORDS_DIR + '/0.png')
    result = extract_foreground(image)

    from matplotlib import pyplot as plt
    import matplotlib
    matplotlib.use('TkAgg')

    plt.imshow(result, plt.cm.gray)
    plt.show()


if __name__ == '__main__':
    main()
    # clean_example()
