import io
import sys

import numpy as np
import pandas as pd
from causallearn.graph.Dag import Dag
from causallearn.graph.GraphNode import GraphNode
from causallearn.search.FCMBased import lingam
from causallearn.utils.GraphUtils import GraphUtils
from matplotlib import pyplot as plt, image as mpimg
from sklearn import tree
from sklearn.ensemble import RandomForestClassifier

sys.path.append("")
from causallearn.search.ScoreBased.GES import ges


def main1():
    data_frame = pd.read_csv('diabetes.csv')

    # print(data_frame)
    # print(data_frame.to_numpy().shape)
    # print(data_frame.to_numpy())
    # print(data_frame.columns)
    # exit()
    arr = data_frame.to_numpy()
    res_map = ges(arr, score_func='local_score_BIC', maxP=None, parameters=None)

    # visualize network
    columns = data_frame.columns
    pyd = GraphUtils.to_pydot(res_map['G'], labels=columns)
    tmp_png = pyd.create_png(f="png")
    fp = io.BytesIO(tmp_png)
    img = mpimg.imread(fp, format='png')
    plt.axis('off')
    plt.imshow(img)
    plt.show()

    adj_mat = np.ones((len(columns), len(columns)), np.int32) * -1
    adj_mat[:-1, -1] = 1

    model = lingam.DirectLiNGAM(prior_knowledge=adj_mat)
    model.fit(data_frame)

    nodes = []
    for i in range(len(columns)):
        node = GraphNode(columns[i])
        nodes.append(node)

    graph = Dag(nodes)
    for i in range(len(columns)):
        for j in range(len(columns)):
            if model.adjacency_matrix_[i, j] != 0:
                graph.add_directed_edge(nodes[i], nodes[j])
    pyd = GraphUtils.to_pydot(graph)
    tmp_png = pyd.create_png(f="png")
    fp = io.BytesIO(tmp_png)
    img = mpimg.imread(fp, format='png')
    plt.axis('off')
    plt.imshow(img)
    plt.show()


def main2():
    data_frame = pd.read_csv('diabetes.csv')
    arr = data_frame.to_numpy()
    clf = RandomForestClassifier(random_state=42, n_estimators=50, n_jobs=-1)
    train = arr[:, :-1]
    clf.fit(train, arr[:, -1:].ravel())
    predictions = clf.predict(train)
    cls_t = tree.DecisionTreeClassifier(max_depth=4)
    cls_t.fit(train, predictions)
    fig = plt.figure(figsize=(16, 8))
    vis = tree.plot_tree(cls_t, feature_names=data_frame.columns, class_names=['Non-Diabetic', 'Diabetic'], max_depth=4,
                         fontsize=9, proportion=True, filled=True, rounded=True)
    plt.show()


if __name__ == '__main__':
    main2()
