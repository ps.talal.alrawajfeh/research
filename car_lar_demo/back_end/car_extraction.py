#!/usr/bin/python3.6

import os

import cv2
import ps_amount_extractor
from matplotlib import pyplot as plt


def extract_car(image):
    image_data = []
    for row in image:
        image_data.extend(row)

    native_data_array = ps_amount_extractor.UCharArray(len(image_data))
    for i in range(len(image_data)):
        native_data_array[i] = int(image_data[i])

    result = ps_amount_extractor.detectAmount(
        native_data_array, image.shape[0], image.shape[1])

    return result.y, result.y + result.height, result.x, result.x + result.width


def main():
    image = cv2.imread(
        '/home/u764/Desktop/demo/test_image.bmp', cv2.IMREAD_GRAYSCALE)

    dims = extract_car(image)
    plt.imshow(image[dims[0]:dims[1], dims[2]:dims[3]], cmap=plt.cm.gray)
    plt.show()


if __name__ == '__main__':
    main()
