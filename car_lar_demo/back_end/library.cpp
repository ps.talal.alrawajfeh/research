#include "library.h"

template<typename T>
T min(T t1, T t2) {
    if (t1 <= t2) {
        return t1;
    }
    return t2;
}

template<typename T>
T max(T t1, T t2) {
    if (t1 >= t2) {
        return t1;
    }
    return t2;
}

template<typename T>
T max(T t1, T t2, T t3) {
    return max(t1, max(t2, t3));
}

template<typename T>
T max(T t1, T t2, T t3, T t4) {
    return max(t1, max(t2, max(t3, t4)));
}

cv::Mat binarize(const cv::Mat &image) {
    cv::Mat thresh(image.rows, image.cols, CV_8UC1);
    adaptiveThreshold(image, thresh, 255,
                      cv::ADAPTIVE_THRESH_MEAN_C,
                      cv::THRESH_BINARY,
                      11,
                      15);
    thresh = 255 - thresh;
    return thresh;
}

cv::Mat removeSmallLines(const cv::Mat &image, int exclusiveUpperLimit, int direction) {
    cv::Mat copy = image.clone();

    int firstLimit;
    int secondLimit;

    if (direction == HORIZONTAL_DIRECTION) {
        firstLimit = copy.rows;
        secondLimit = copy.cols;
    } else {
        firstLimit = copy.cols;
        secondLimit = copy.rows;
    }

    unsigned char value;

    for (int l = 0; l < firstLimit; l++) {
        for (int start = 0; start < secondLimit; start++) {
            if (direction == HORIZONTAL_DIRECTION) {
                value = copy.at<unsigned char>(l, start);
            } else {
                value = copy.at<unsigned char>(start, l);
            }

            if (value > 0) {
                int end = start + 1;
                for (; end < secondLimit; end++) {
                    if (direction == HORIZONTAL_DIRECTION) {
                        value = copy.at<unsigned char>(l, end);
                    } else {
                        value = copy.at<unsigned char>(end, l);
                    }

                    if (value == 0) {
                        break;
                    }
                }

                if (end - start < exclusiveUpperLimit) {
                    if (direction == HORIZONTAL_DIRECTION) {
                        for (int i = start; i < end; i++) {
                            copy.at<unsigned char>(l, i) = 0;
                        }
                    } else {
                        for (int i = start; i < end; i++) {
                            copy.at<unsigned char>(i, l) = 0;
                        }
                    }
                }

                start = end;
            }
        }
    }

    return copy;
}

cv::Mat maxPooling(const cv::Mat &image) {
    int rows_remainder = image.rows % 2;
    int cols_remainder = image.cols % 2;
    int rows = image.rows / 2;
    int cols = image.cols / 2;

    cv::Mat result(rows + rows_remainder,
                   cols + cols_remainder,
                   CV_8UC1);

    int row;
    for (row = 0; row < rows; row++) {
        for (int col = 0; col < cols; col++) {
            unsigned char value = max(image.at<unsigned char>(row * 2, col * 2),
                                      image.at<unsigned char>(row * 2, col * 2 + 1),
                                      image.at<unsigned char>(row * 2 + 1, col * 2),
                                      image.at<unsigned char>(row * 2 + 1, col * 2 + 1));


            result.at<unsigned char>(row, col) = value;
        }

        if (cols_remainder != 0) {
            result.at<unsigned char>(row, cols) = max(image.at<unsigned char>(row * 2, cols * 2),
                                                      image.at<unsigned char>(row * 2, cols * 2 + 1));
        }
    }

    if (rows_remainder != 0) {
        row = rows;
        for (int col = 0; col < cols; col++) {
            unsigned char value = max(image.at<unsigned char>(row * 2, col * 2),
                                      image.at<unsigned char>(row * 2, col * 2 + 1));


            result.at<unsigned char>(row, col) = value;
        }

        if (cols_remainder != 0) {
            result.at<unsigned char>(row, cols) = max(image.at<unsigned char>(row * 2, cols * 2),
                                                      image.at<unsigned char>(row * 2, cols * 2 + 1));
        }
    }

    return result;
}

std::vector<std::pair<int, int>> nonZeroPixels(const cv::Mat &image) {
    std::vector<std::pair<int, int>> pixels;

    for (int row = 0; row < image.rows; row++) {
        for (int col = 0; col < image.cols; col++) {
            if (image.at<unsigned char>(row, col) != 0) {
                pixels.emplace_back(std::pair<int, int>(col, row));
            }
        }
    }

    return pixels;
}

bool withinBounds(const cv::Mat &image, int x, int y) {
    return (x >= 0 && x < image.cols) &&
           (y >= 0 && y < image.rows);
}

bool skipPixel(const cv::Mat &connectedVisited,
               const cv::Mat &disconnectedVisited,
               const std::deque<std::pair<int, int>> &connectedQueue,
               const std::deque<std::pair<int, int>> &disconnectedQueue,
               const int &x,
               const int &y) {
    return connectedVisited.at<unsigned char>(y, x) != 0 ||
           disconnectedVisited.at<unsigned char>(y, x) != 0 ||
           std::find(connectedQueue.begin(),
                     connectedQueue.end(),
                     std::pair<int, int>(x, y)) != connectedQueue.end() ||
           std::find(disconnectedQueue.begin(),
                     disconnectedQueue.end(),
                     std::pair<int, int>(x, y)) != disconnectedQueue.end();
}

std::vector<cv::Rect> detectPartiallyConnectedComponents(const cv::Mat &image) {
    std::vector<cv::Rect> components;

    std::vector<std::pair<int, int>> pixels = nonZeroPixels(image);

    cv::Mat connectedVisited = cv::Mat::zeros(image.rows, image.cols, CV_8UC1);
    cv::Mat disconnectedVisited = cv::Mat::zeros(image.rows, image.cols, CV_8UC1);

    for (auto pixel : pixels) {
        int startX = pixel.first;
        int startY = pixel.second;

        if (connectedVisited.at<unsigned char>(startY, startX) != 0) {
            continue;
        }

        int cX1 = image.cols;
        int cX2 = 0;
        int cY1 = image.rows;
        int cY2 = 0;

        std::deque<std::pair<int, int>> connectedQueue;
        connectedQueue.emplace_back(std::pair<int, int>(startX, startY));

        std::deque<std::pair<int, int>> disconnectedQueue;

        while (!connectedQueue.empty()) {
            std::pair<int, int> pair = connectedQueue.front();
            connectedQueue.pop_front();

            int parentX = pair.first;
            int parentY = pair.second;

            connectedVisited.at<unsigned char>(parentY, parentX) = 1;

            if (parentX < cX1)
                cX1 = parentX;
            if (parentX > cX2)
                cX2 = parentX;
            if (parentY < cY1)
                cY1 = parentY;
            if (parentY > cY2)
                cY2 = parentY;

            for (int xDelta = 0; xDelta <= 1; xDelta++) {
                int x = parentX + xDelta;
                for (int yDelta = 0; yDelta <= 1; yDelta++) {
                    int y = parentY + yDelta;

                    const std::deque<std::pair<int, int>>
                    ::iterator &iterator = std::find(disconnectedQueue.begin(),
                                                     disconnectedQueue.end(),
                                                     std::pair<int, int>(x, y));

                    if (iterator != disconnectedQueue.end()) {
                        int foundX = iterator->first;
                        int foundY = iterator->second;
                        disconnectedQueue.erase(iterator);
                        connectedQueue.emplace_back(std::pair<int, int>(foundX, foundY));
                    }

                    if (withinBounds(image, x, y)) {
                        if (skipPixel(connectedVisited,
                                      disconnectedVisited,
                                      connectedQueue,
                                      disconnectedQueue,
                                      x,
                                      y)) {
                            continue;
                        }


                        if (image.at<unsigned char>(y, x) != 0) {
                            connectedQueue.emplace_back(std::pair<int, int>(x, y));
                        }
                    }
                }
            }

            if (parentX < image.cols - 1 &&
                parentY < image.rows - 1 &&
                image.at<unsigned char>(parentY + 1, parentX) == 0 &&
                image.at<unsigned char>(parentY + 1, parentX + 1) == 0) {

                for (int xDelta = -MAX_GAP_LENGTH; xDelta <= MAX_GAP_LENGTH; xDelta++) {
                    int x = parentX + xDelta;
                    for (int yDelta = -MAX_GAP_LENGTH; yDelta <= MAX_GAP_LENGTH; yDelta++) {
                        int y = parentY + yDelta;

                        if (withinBounds(image, x, y)) {
                            if (skipPixel(connectedVisited,
                                          disconnectedVisited,
                                          connectedQueue,
                                          disconnectedQueue,
                                          x,
                                          y)) {
                                continue;
                            }

                            if (image.at<unsigned char>(y, x) != 0) {
                                disconnectedQueue.emplace_back(std::pair<int, int>(x, y));
                            }
                        }
                    }
                }
            }
        }

        bool anyDisconnected = !disconnectedQueue.empty();

        while (!disconnectedQueue.empty()) {
            std::pair<int, int> pair = disconnectedQueue.front();
            disconnectedQueue.pop_front();

            int parentX = pair.first;
            int parentY = pair.second;

            disconnectedVisited.at<unsigned char>(parentY, parentX) = 1;

            if (parentX < cX1)
                cX1 = parentX;
            if (parentX > cX2)
                cX2 = parentX;
            if (parentY < cY1)
                cY1 = parentY;
            if (parentY > cY2)
                cY2 = parentY;

            for (int xDelta = 0; xDelta <= 1; xDelta++) {
                int x = parentX + xDelta;
                for (int yDelta = 0; yDelta <= 1; yDelta++) {
                    int y = parentY + yDelta;

                    if (withinBounds(image, x, y)) {
                        if (skipPixel(connectedVisited,
                                      disconnectedVisited,
                                      connectedQueue,
                                      disconnectedQueue,
                                      x,
                                      y)) {
                            continue;
                        }

                        if (image.at<unsigned char>(y, x) != 0) {
                            disconnectedQueue.emplace_back(std::pair<int, int>(x, y));
                        }
                    }
                }
            }
        }

        components.emplace_back(cv::Rect(cX1, cY1, cX2 - cX1 + 1, cY2 - cY1 + 1));

        if (anyDisconnected) {
            disconnectedVisited.release();
            disconnectedVisited = cv::Mat::zeros(image.rows, image.cols, CV_8UC1);
        }
    }

    return components;
}

std::vector<cv::Rect> detectLines(const cv::Mat &image, const cv::Rect &rect, int direction, int minExclusiveLength) {
    cv::Mat subImage = image(rect);
    std::vector<cv::Rect> lines;

    int firstLimit;
    int secondLimit;

    if (direction == HORIZONTAL_DIRECTION) {
        firstLimit = subImage.rows;
        secondLimit = subImage.cols;
    } else {
        firstLimit = subImage.cols;
        secondLimit = subImage.rows;
    }

    for (int l = 0; l < firstLimit; l++) {
        for (int start = 0; start < secondLimit; start++) {
            unsigned char value;

            if (direction == HORIZONTAL_DIRECTION) {
                value = subImage.at<unsigned char>(l, start);
            } else {
                value = subImage.at<unsigned char>(start, l);
            }

            if (value != 0) {
                int gapCounter = 0;
                int gapStart = -1;
                int i;
                for (i = start + 1; i < secondLimit; i++) {
                    if (direction == HORIZONTAL_DIRECTION) {
                        value = subImage.at<unsigned char>(l, i);
                    } else {
                        value = subImage.at<unsigned char>(i, l);
                    }

                    if (value == 0) {
                        gapStart = gapStart == -1 ? i : gapStart;
                        gapCounter++;
                    } else {
                        gapStart = -1;
                        gapCounter = 0;
                    }

                    if (gapCounter > MAX_GAP_LENGTH) {
                        break;
                    }
                }

                int end;
                if (gapStart == -1) {
                    end = i;
                } else {
                    end = gapStart;
                }

                if (end - start > minExclusiveLength) {
                    if (direction == HORIZONTAL_DIRECTION) {
                        lines.emplace_back(cv::Rect(start, l, end - start, 1));
                    } else {
                        lines.emplace_back(cv::Rect(l, start, 1, end - start));
                    }
                }

                start = i;
            }
        }
    }

    return lines;
}

std::pair<cv::Rect, double> findBestRectangle(const cv::Rect &component,
                                              const std::vector<cv::Rect> &componentHorizontalLines,
                                              const std::vector<cv::Rect> &componentVerticalLines) {
    std::vector<bool> HorizontalLinesPermutationMarker(componentHorizontalLines.size());
    std::vector<bool> VerticalLinesPermutationMarker(componentVerticalLines.size());

    std::fill(HorizontalLinesPermutationMarker.end() - 2, HorizontalLinesPermutationMarker.end(), true);
    std::fill(VerticalLinesPermutationMarker.end() - 2, VerticalLinesPermutationMarker.end(), true);

    double maxScore = 0;
    cv::Rect bestRect(0, 0, 0, 0);

    do {
        cv::Rect hLine1(0, 0, 0, 0);
        cv::Rect hLine2(0, 0, 0, 0);
        unsigned long i;
        for (i = 0; i < componentHorizontalLines.size(); i++) {
            if (HorizontalLinesPermutationMarker[i]) {
                hLine1 = componentHorizontalLines[i];
                break;
            }
        }
        for (i = i + 1; i < componentHorizontalLines.size(); i++) {
            if (HorizontalLinesPermutationMarker[i]) {
                hLine2 = componentHorizontalLines[i];
                break;
            }
        }

        if (abs(hLine1.y - hLine2.y) < MIN_VERTICAL_SPACING / 4) {
            continue;
        }

        do {
            cv::Rect vLine1(0, 0, 0, 0);
            cv::Rect vLine2(0, 0, 0, 0);
            unsigned long j;
            for (j = 0; j < componentVerticalLines.size(); j++) {
                if (VerticalLinesPermutationMarker[j]) {
                    vLine1 = componentVerticalLines[j];
                    break;
                }
            }
            for (j = j + 1; j < componentVerticalLines.size(); j++) {
                if (VerticalLinesPermutationMarker[j]) {
                    vLine2 = componentVerticalLines[j];
                    break;
                }
            }

            if (abs(vLine1.x - vLine2.x) < MIN_HORIZONTAL_SPACING / 4) {
                continue;
            }

            double score = 0;

            score += 1 - ((double) abs(vLine1.y - vLine2.y)) / component.height;
            score += 1 - ((double) abs(vLine1.height - vLine2.height)) / component.height;

            score += 1 - ((double) abs(hLine1.x - hLine2.x)) / component.width;
            score += 1 - ((double) abs(hLine1.width - hLine2.width)) / component.width;

            int rY1 = min(hLine1.y, hLine2.y);
            int rY2 = max(hLine1.y, hLine2.y);
            int rX1 = min(vLine1.x, vLine2.x);
            int rX2 = max(vLine1.x, vLine2.x);

            score += 1 - ((double) abs(rY1 - vLine1.y)) / component.height;
            score += 1 - ((double) abs(rY1 - vLine2.y)) / component.height;
            score += 1 - ((double) abs(rY2 - rY1 + 1 - vLine1.height)) / component.height;
            score += 1 - ((double) abs(rY2 - rY1 + 1 - vLine2.height)) / component.height;

            score += 1 - ((double) abs(rX1 - hLine1.x)) / component.width;
            score += 1 - ((double) abs(rX1 - hLine2.x)) / component.width;
            score += 1 - ((double) abs(rX2 - rX1 + 1 - hLine1.width)) / component.width;
            score += 1 - ((double) abs(rX2 - rX1 + 1 - hLine2.width)) / component.width;

            if (maxScore < score) {
                maxScore = score;

                bestRect.x = component.x + rX1;
                bestRect.width = rX2 - rX1 + 1;
                bestRect.y = component.y + rY1;
                bestRect.height = rY2 - rY1 + 1;
            }
        } while (std::next_permutation(VerticalLinesPermutationMarker.begin(),
                                       VerticalLinesPermutationMarker.end()));
    } while (std::next_permutation(HorizontalLinesPermutationMarker.begin(),
                                   HorizontalLinesPermutationMarker.end()));


    return std::pair<cv::Rect, double>(bestRect, maxScore);
}

unsigned long pixelSum(const cv::Mat &image) {
    unsigned long sum = 0;
    for (int row = 0; row < image.rows; row++) {
        for (int col = 0; col < image.cols; col++) {
            sum += (unsigned long) image.at<unsigned char>(row, col);
        }
    }
    return sum;
}

cv::Rect detectRectangle(const cv::Mat &image) {
    cv::Rect rect(image.cols / 2,
                  image.rows / 4,
                  image.cols / 2,
                  image.rows / 2);

    cv::Mat cropped = image(rect).clone();

    cv::Mat thresh = binarize(cropped);
    cv::Mat horizontalLines = removeSmallLines(thresh,
                                               MIN_LINE_LENGTH,
                                               HORIZONTAL_DIRECTION);

    cv::Mat verticalLines = removeSmallLines(thresh,
                                             MIN_LINE_LENGTH,
                                             VERTICAL_DIRECTION);

    cv::Mat clean = horizontalLines + verticalLines;
    cv::Mat pooled1 = maxPooling(clean);
    cv::Mat pooled2 = maxPooling(pooled1);

    std::vector<cv::Rect> components = detectPartiallyConnectedComponents(pooled2);

    cv::Rect bestRect(0, 0, 0, 0);
    double maxScore = 0;

    for (auto component : components) {
        if (component.width < MIN_HORIZONTAL_SPACING / 4 || component.height < MIN_VERTICAL_SPACING / 4) {
            continue;
        }

        std::vector<cv::Rect> componentHorizontalLines = detectLines(pooled2,
                                                                     component,
                                                                     HORIZONTAL_DIRECTION,
                                                                     MIN_LINE_LENGTH / 4);

        std::vector<cv::Rect> componentVerticalLines = detectLines(pooled2,
                                                                   component,
                                                                   VERTICAL_DIRECTION,
                                                                   MIN_LINE_LENGTH / 4);

        if (componentHorizontalLines.size() < 2 || componentVerticalLines.size() < 2) {
            continue;
        }

        std::pair<cv::Rect, double> bestRectWithScore = findBestRectangle(component, componentHorizontalLines,
                                                                          componentVerticalLines);

        if (maxScore < bestRectWithScore.second) {
            maxScore = bestRectWithScore.second;
            bestRect = bestRectWithScore.first;
        } else if (maxScore == bestRectWithScore.second) {
            if (bestRectWithScore.first.width * bestRectWithScore.first.height < bestRect.width * bestRect.height) {
                bestRect = bestRectWithScore.first;
            }
        }
    }

    bestRect.x *= 4;
    bestRect.width *= 4;

    bestRect.y *= 4;
    bestRect.height *= 4;

    cv::Mat part = clean(bestRect);

    int x1;
    for (x1 = 0; x1 < part.cols; x1++) {
        cv::Rect strip(x1, 0, 1, part.rows);
        if (pixelSum(part(strip)) > 0) {
            break;
        }
    }
    bestRect.x += x1;

    int y1;
    for (y1 = 0; y1 < part.rows; y1++) {
        cv::Rect strip(0, y1, part.cols, 1);
        if (pixelSum(part(strip)) > 0) {
            break;
        }
    }
    bestRect.y += y1;

    int x2;
    for (x2 = part.cols - 1; x2 >= 0; x2--) {
        cv::Rect strip(x2, 0, 1, part.rows);
        if (pixelSum(part(strip)) > 0) {
            break;
        }
    }
    bestRect.width = x2 - x1 + 1;

    int y2;
    for (y2 = part.rows - 1; y2 >= 0; y2--) {
        cv::Rect strip(0, y2, part.cols, 1);
        if (pixelSum(part(strip)) > 0) {
            break;
        }
    }
    bestRect.height = y2 - y1 + 1;

    bestRect.x += image.cols / 2;
    bestRect.y += image.rows / 4;
    return bestRect;
}

Rect detectAmount(unsigned char *imageData, int rows, int cols) {
    cv::Mat img(rows, cols, CV_8UC1, (void *) imageData, 0);

    Rect region;
    try {
        const cv::Rect &rect = detectRectangle(img);

        region.x = rect.x;
        region.y = rect.y;
        region.width = rect.width;
        region.height = rect.height;
    } catch(std::exception& ex) {
        region.x = 0;
        region.y = 0;
        region.width = 0;
        region.height = 0;
    }

    return region;
}