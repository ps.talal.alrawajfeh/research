from cv2 import cv2
from PIL import Image
from flask import Flask, jsonify
import base64
import numpy as np
from flask import request
from flask_cors import CORS
from io import BytesIO
import json

from car_extraction import extract_car
from lar_extraction import extract_lar

app = Flask(__name__)
CORS(app)


def base64ToImage(base64_string):
    image_data = base64.b64decode(base64_string)
    image = Image.open(BytesIO(image_data))
    return np.array(image, np.uint8)


@app.route("/")
def hello():
    return "Hello World!"


@app.route('/get-boxes', methods=["POST"])
def getCarLarBoxes():
    image = base64ToImage(request.data)
    y1, y2, x1, x2 = extract_car(image)
    (y11, y12, x11, x12), (y21, y22, x21, x22) = extract_lar(image)

    return jsonify(
        {
            "car_box": [int(y1), int(y2), int(x1), int(x2)],
            "lar_line1": [int(y11), int(y12), int(x11), int(x12)],
            "lar_line2": [int(y21), int(y22), int(x21), int(x22)]
        }
    )


if __name__ == "__main__":
    app.run()
