#!/usr/bin/python3.6
import math
import os
from itertools import combinations

import numpy as np
from cv2 import cv2
from matplotlib import pyplot as plt
from scipy.interpolate import interp1d


class FuzzyTruthValue:
    def __init__(self, x):
        if x > 1 or x < 0:
            raise ValueError(
                f'a fuzzy truth value should be between 0 and 1, given: {x}')
        self.x = x

    def __invert__(self):
        return FuzzyTruthValue(1 - self.x)

    def __and__(self, other):
        return FuzzyTruthValue(self.x * other.x)

    def __or__(self, other):
        return FuzzyTruthValue(self.x + other.x - self.x * other.x)

    def __xor__(self, other):
        return FuzzyTruthValue(self.x * (1 - other.x) + (1 - self.x) * other.x
                               - self.x * (1 - self.x) * other.x * (1 - other.x))

    def get(self):
        return self.x


def binarize(image, beta=1.25):
    result, binarized = cv2.threshold(image,
                                      np.mean(image) + beta * np.std(image),
                                      255,
                                      cv2.THRESH_BINARY)
    return binarized


def remove_small_lines(image, exclusive_limit):
    result = np.zeros(image.shape, np.uint8)

    for i in range(result.shape[0]):
        row = image[i]
        start = 0
        while start < result.shape[1]:
            end = start + np.argmin(row[start:])

            if end - start >= exclusive_limit:
                result[i, start:end] = 1

            start = end + 1

    return result


def max_pooling(image):
    rows = image.shape[0] // 2
    cols = image.shape[1] // 2
    rem_row = image.shape[0] % 2
    rem_col = image.shape[1] % 2

    result = np.zeros((rows + rem_row, cols + rem_col))

    for y in range(rows):
        for x in range(cols):
            result[y, x] = np.max(image[2 * y:2 * (y + 1), 2 * x:2 * (x + 1)])

    if rem_row > 0:
        for x in range(cols):
            result[-1, x] = np.max(image[-1:, 2 * x:2 * (x + 1)])

    if rem_col > 0:
        for y in range(rows):
            result[y, -1] = np.max(image[2 * y:2 * (y + 1), -1:])

    if rem_row > 0 and rem_col > 0:
        result[-1, -1] = image[-1, -1]

    return result


def sharpen_lines(image, filter_width=21):
    matrix1 = np.ones((3, filter_width))
    matrix1[0:1, :] *= -1
    matrix1 /= filter_width

    matrix2 = np.ones((3, filter_width))
    matrix2[2:3, :] *= -1
    matrix2 /= filter_width

    filtered1 = cv2.filter2D(image, -1, matrix1)
    filtered2 = cv2.filter2D(image, -1, matrix2)

    averaged = 0.5 * \
        filtered1[:-1, :].astype(np.float) + 0.5 * \
        filtered2[1:, :].astype(np.float)
    contrast_adjusted = averaged * 1.5
    contrast_adjusted[contrast_adjusted > 255] = 255

    return contrast_adjusted.astype(np.uint8)


def detect_lines(image):
    lines = []

    i = 0
    while i < image.shape[0]:
        row = image[i]
        if row[np.argmax(row)] == 0:
            i += 1
            continue

        j = i + 1
        while j < min(i + 3, image.shape[0]):
            row = image[j]
            if row[np.argmax(row)] == 0:
                break
            j += 1

        lines.append((i, j - 1))
        i += 1

    return lines


def sign(x):
    if x == 0:
        return 0
    return x / abs(x)


def calculate_line_truth_value_and_endpoints(section):
    background_width = section.shape[1]
    length_fuzzifier = np.zeros(background_width, np.uint8)

    for row in section:
        length_fuzzifier = length_fuzzifier | row

    # all following fuzzifications yield a truth value between 0 and 1
    # fuzzification of linearity
    line_truth = FuzzyTruthValue(
        np.sum(length_fuzzifier) / (255 * background_width))

    # fuzzification of the concept of empty space
    start_line = np.argmax(length_fuzzifier)
    end_line = len(length_fuzzifier) - np.argmax(length_fuzzifier[::-1])
    line_part = length_fuzzifier[start_line:end_line]
    line_length = end_line - start_line
    if line_length > 2:
        empty_truth = FuzzyTruthValue(
            len(line_part[line_part == 0]) / (line_length - 2))
    else:
        empty_truth = FuzzyTruthValue(0)

    # fuzzification of straightness
    line_straightness_truth = 0
    start = 0
    for row in section:
        start = start + np.argmax(row[start:])
        end = start + len(row[start:]) - np.argmax(row[start:][::-1])
        if end - start == 0:
            line_straightness_truth /= 2
            continue
        line_straightness_truth += np.sum(row[start:end]) / 255
        if end == background_width:
            break
        start = end
    line_straightness_truth = FuzzyTruthValue(
        line_straightness_truth / background_width)

    # fuzzification of "long enough" which is around the half of the background width
    length_fuzzifier = interp1d(
        [0, background_width / 2, background_width], [0, 0.8, 1.0])
    line_long_truth = FuzzyTruthValue(length_fuzzifier(line_length))

    return line_truth & ~empty_truth & line_straightness_truth & line_long_truth, start_line, end_line


def view_image(image, title=''):
    plt.suptitle(title)
    plt.imshow(image, cmap=plt.cm.gray)
    plt.show()


def view_images(images, title=''):
    f, axs = plt.subplots(len(images))
    axs[0].set_title(title)
    for i in range(len(images)):
        axs[i].imshow(images[i], cmap=plt.cm.gray)
    plt.show()


def extract_lar(image):
    image = 255 - image
    sharpened = sharpen_lines(image)
    binarized = binarize(sharpened)

    start_y = math.floor(image.shape[0] * 0.23)
    end_y = math.ceil(image.shape[0] * 0.72)

    region_of_interest = binarized[start_y:end_y, :]
    horizontal_lines = remove_small_lines(np.copy(region_of_interest), 60)

    pooled1 = max_pooling(horizontal_lines)
    pooled2 = (max_pooling(pooled1) * 255).astype(np.uint8)

    line_truth_values = []
    lines = detect_lines(pooled2)
    for y1, y2 in lines:
        part = pooled2[y1: y2 + 1, :]
        overall_truth, x1, x2 = calculate_line_truth_value_and_endpoints(part)

        if y2 - y1 == 2:
            line_truth_values.append((overall_truth, y1 + 1, x1, x2))
        else:
            line_truth_values.append((overall_truth, y1, x1, x2))

    max_truth = 0
    max_truth_lines = []
    for line1, line2, line3 in combinations(line_truth_values, 3):
        overall_truth = line1[0] & line2[0] & line3[0]

        vertically_sorted = [line1, line2, line3]
        vertically_sorted.sort(key=lambda x: x[1])
        line1, line2, line3 = vertically_sorted

        distance1 = line2[1] - line1[1]
        distance2 = line3[1] - line2[1]

        if distance1 > 25 or distance2 > 25:
            continue

        if distance1 < 5 or distance2 < 5:
            continue

        # fuzzification of the concept of equally distant lines
        equally_distant_truth = FuzzyTruthValue(
            2 * math.sqrt(distance1 * distance2) / (distance1 + distance2))

        overall_truth &= equally_distant_truth

        if overall_truth.get() > max_truth:
            max_truth = overall_truth.get()
            max_truth_lines = [line1[1:], line2[1:], line3[1:]]

    line1 = np.array(max_truth_lines[0]) * 4
    line2 = np.array(max_truth_lines[1]) * 4
    line3 = np.array(max_truth_lines[2]) * 4

    distance = max(line2[0] - line1[0], line3[0] - line2[0])
    x1 = min(line1[1], line2[1], line3[1])
    x2 = max(line2[2], line3[2])
    y11 = int(line2[0] - distance + 10)
    y21 = int(line2[0] + distance * 1 / 4)
    y12 = int(line3[0] - distance + 10)
    y22 = int(line3[0] + distance * 1 / 4)

    region1 = (start_y + y11, start_y + y21, x1 - 10, x2 + 10)
    region2 = (start_y + y12, start_y + y22, x1 - 10, x2 + 10)

    return region1, region2
