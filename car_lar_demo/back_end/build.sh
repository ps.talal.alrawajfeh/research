#!/bin/bash

if [[ -d build ]]
then
  rm -rf build
fi

cmake -H. -Bbuild
cmake --build build -- -j3

cp -f ./build/_ps_amount_extractor.so ./_ps_amount_extractor.so
cp -f ./build/ps_amount_extractor.py ./ps_amount_extractor.py