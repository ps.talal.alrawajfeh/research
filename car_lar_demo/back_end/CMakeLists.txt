cmake_minimum_required(VERSION 3.15)
project(ps_amount_extractor)

set(CMAKE_CXX_STANDARD 14)
set(OpenCV_DIR /installation/OpenCV-master/lib/cmake/opencv4)

find_package(OpenCV REQUIRED)
find_package(SWIG REQUIRED)
include(${SWIG_USE_FILE})

find_package(PythonLibs)
include_directories(${PYTHON_INCLUDE_PATH})
include_directories(${CMAKE_CURRENT_SOURCE_DIR})

set_property(SOURCE library.i PROPERTY CPLUSPLUS ON)

swig_add_module(ps_amount_extractor python library.i library.cpp library.h)
swig_link_libraries(ps_amount_extractor ${OpenCV_LIBS})
swig_link_libraries(ps_amount_extractor ${PYTHON_LIBRARIES})




