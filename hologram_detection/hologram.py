import itertools
import math
import os

import cv2
import numpy as np
from matplotlib import pyplot as plt
from scipy.spatial import ConvexHull

FLANN_INDEX_KDTREE = 1

MIN_MATCH_COUNT = 1

DEBUG_MODE = True


def view_image(image):
    plt.imshow(image, plt.cm.gray)
    plt.show()


def find_key_points(image):
    orb = cv2.ORB_create(scaleFactor=2,
                         nlevels=3)
    key_points = orb.detect(image, None)
    return orb.compute(image, key_points)


def detect_match(template_image,
                 image,
                 key_points1,
                 key_points2,
                 descriptors1,
                 descriptors2):
    # matcher = cv2.FlannBasedMatcher(dict(algorithm=1, trees=5),
    #                                 dict(checks=50))
    bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)

    # matches = matcher.knnMatch(descriptors1, descriptors2, k=2)
    matches = bf.match(descriptors1, descriptors2)
    matches = sorted(matches, key=lambda x: x.distance)
    good_matches = matches
    # good_matches = []
    # for m, n in matches:
    #     if m.distance < 0.7 * n.distance:
    #         good_matches.append(m)

    if len(good_matches) > MIN_MATCH_COUNT:
        src_pts = np.float32([key_points1[m.queryIdx].pt for m in good_matches]).reshape(-1, 1, 2)
        dst_pts = np.float32([key_points2[m.trainIdx].pt for m in good_matches]).reshape(-1, 1, 2)
        M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 5.0)
        if M is None:
            return
        matches_mask = mask.ravel().tolist()
        h, w, _ = template_image.shape
        pts = np.float32([[0, 0], [0, h - 1], [w - 1, h - 1], [w - 1, 0]]).reshape(-1, 1, 2)
        dst = cv2.perspectiveTransform(pts, M)
        draw_image = cv2.polylines(np.array(image), [np.int32(dst)], True, (0, 255, 0), 3, cv2.LINE_AA)

        draw_params = dict(matchColor=(0, 255, 0),
                           singlePointColor=None,
                           matchesMask=matches_mask,
                           flags=2)
        draw_image = cv2.drawMatches(template_image, key_points1, draw_image, key_points2, good_matches, None,
                                     **draw_params)

        return draw_image
    return None


def adaptive_canny_edge_detection(image, beta_low=1.0, beta_high=1.0):
    inverted = 255 - image

    mu = np.mean(inverted)
    sigma = np.std(inverted)

    canny_low_threshold = int(max(mu / 127 * beta_low * sigma, 0.0))
    canny_high_threshold = int(min(mu + beta_high * sigma, 255.0))

    return cv2.Canny(image, canny_low_threshold, canny_high_threshold)


def get_harris_corners(image):
    image_copy = cv2.cvtColor(np.array(image), cv2.COLOR_GRAY2BGR)
    corner_harris = cv2.cornerHarris(np.array(image, np.float32), 2, 3, 0.04)
    max_corner = corner_harris.max()
    corners = []
    for y in range(corner_harris.shape[0]):
        for x in range(corner_harris.shape[1]):
            if corner_harris[y, x] > 0.01 * max_corner:
                corners.append((x, y))
                if DEBUG_MODE:
                    cv2.circle(image_copy, (x, y), 2, (0, 255, 0), 1)
    if DEBUG_MODE:
        view_image(image_copy)
    return corners


def get_good_features_to_track(image):
    features_to_track = cv2.goodFeaturesToTrack(image, 25, 0.01, 10)
    features_to_track = np.int0(features_to_track)
    corners = []

    image_copy = cv2.cvtColor(np.array(image), cv2.COLOR_GRAY2BGR)
    for i in features_to_track:
        x, y = i.ravel()
        corners.append((x, y))
        cv2.circle(image_copy, (x, y), 3, 255, -1)

    if DEBUG_MODE:
        view_image(image_copy)
    return corners


def get_canny_points(image):
    canny = adaptive_canny_edge_detection(image, 1.2)

    points = []
    for y in range(image.shape[0]):
        for x in range(image.shape[1]):
            if canny[y, x] > 0:
                points.append((x, y))
    return points


def top_hat_morphology(image):
    tp = cv2.morphologyEx(image, cv2.MORPH_TOPHAT, np.ones((5, 5)))
    tp[tp > 70] = 255
    tp[tp <= 70] = 0
    return tp


def euclidean_distance(point1, point2):
    return np.sqrt(np.sum(np.square(np.array(point1, np.float32) -
                                    np.array(point2, np.float32))))


def find_furthest_point_from(reference_points, points):
    def compute_point_score(point):
        score = 1.0
        for reference_point in reference_points:
            score *= euclidean_distance(reference_point, point)

        return score

    max_point_score = -1
    furthest_point = None
    for p in points:
        point_score = compute_point_score(p)

        if point_score > max_point_score:
            max_point_score = point_score
            furthest_point = p

    return furthest_point


def find_convex_polygon_vertices_from_contour(polygon_contour,
                                              polygon_center_point,
                                              number_of_vertices):
    vertices = []
    for n in range(number_of_vertices):
        vertex = find_furthest_point_from([polygon_center_point] + vertices,
                                          polygon_contour)
        vertices.append(vertex)
    return vertices


def find_rectangle_vertices_from_contour(contour, center_point):
    return find_convex_polygon_vertices_from_contour(contour,
                                                     center_point,
                                                     4)


def get_connected_region(image, start_point, visits_matrix):
    if image[start_point[1], start_point[0]] == 0:
        return []

    region = [start_point]
    queue = [start_point]

    while len(queue) > 0:
        parent = queue.pop(0)
        visits_matrix[parent[1], parent[0]] = 1

        for dy in range(-1, 2):
            for dx in range(-1, 2):
                x, y = parent
                x += dx
                y += dy
                if x < 0 or x >= image.shape[1]:
                    continue
                if y < 0 or y >= image.shape[0]:
                    continue
                if visits_matrix[y, x] == 1:
                    continue
                if image[y, x] == 255 and (x, y) not in queue:
                    queue.append((x, y))
                    region.append((x, y))

    return region


def get_connected_regions(image):
    visits_matrix = np.zeros(image.shape, np.uint8)

    regions = []
    for y in range(image.shape[0]):
        for x in range(image.shape[1]):
            if image[y, x] == 255 and visits_matrix[y, x] == 0:
                regions.append(get_connected_region(image, (x, y), visits_matrix))

    return regions


def main():
    template_image = cv2.imread('template.jpg', cv2.IMREAD_GRAYSCALE)
    template_image = cv2.resize(template_image, (0, 0), fx=0.5, fy=0.5, interpolation=cv2.INTER_AREA)
    key_points1, descriptors1 = find_key_points(template_image)
    descriptors1 = np.array(descriptors1, np.float32)

    cv2.imwrite(f'temp_key_points.jpg',
                cv2.drawKeypoints(template_image, key_points1, None, color=(0, 255, 0), flags=0))

    video_capture = cv2.VideoCapture('test3.mp4')

    if not os.path.isdir('test'):
        os.makedirs('test')

    i = 0
    while video_capture.isOpened():
        ret, frame = video_capture.read()
        if frame is None:
            break

        image = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        image = cv2.resize(image, (0, 0), fx=0.5, fy=0.5)
        key_points2, descriptors2 = find_key_points(image)
        descriptors2 = np.array(descriptors2, np.float32)

        view_image(adaptive_canny_edge_detection(image))
        # canny = get_canny_points(image)
        corners2 = get_good_features_to_track(image)
        orb_points = [k.pt for k in key_points2]

        new_image = np.zeros(image.shape)

        features = set().union(set()).union(set(orb_points))
        features = [(int(x), int(y)) for x, y in features]

        # for y in range(new_image.shape[0]):
        #     for x in range(new_image.shape[1]):
        #         if (x, y) in features:
        #             new_image[y, x] = 255
        # new_image = cv2.erode(new_image, np.ones((2, 2)))
        # new_image = cv2.dilate(new_image, np.ones((3, 3)))
        #
        # features = [(x, y)
        #             for x, y in itertools.product(range(0, new_image.shape[1]), range(0, new_image.shape[0]))
        #             if new_image[y, x] == 255]
        #
        # tp = top_hat_morphology(image)
        # tp = cv2.dilate(tp, np.ones((3, 3)), iterations=3)
        # mu = int(np.mean(image))
        # tp[tp == 255] = mu
        #
        # image_copy = np.array(image)
        # image_copy[tp > 0] = 0
        # image_copy += tp
        #
        # view_image(image_copy)
        # features.extend([(x, y)
        #                  for x, y in itertools.product(range(0, tp.shape[1]), range(0, tp.shape[0]))
        #                  if tp[y, x] == 255])

        points = np.array(list(features))
        center_point = (int(np.mean(points[:, 0])), int(np.mean(points[:, 1])))

        hull = ConvexHull(points)

        img = np.array(image)
        img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
        hull_points = []
        for vertex in hull.vertices:
            point = (np.int0(points[vertex, 0]), np.int0(points[vertex, 1]))
            hull_points.append(point)
            # cv2.circle(img, point, 2, (0, 255, 0), 1)

        rect = find_rectangle_vertices_from_contour(hull_points, center_point)
        for pt in rect:
            cv2.circle(img, pt, 2, (0, 255, 0), 1)
        for pt in features:
            cv2.circle(img, pt, 2, (0, 255, 0), 1)

        # cv2.circle(img, center_point, 2, (0, 255, 0), 1)

        # top_line = None
        # for p1, p2 in itertools.combinations(hull_points, 2):
        #     slope = (p2[1] - p1[1]) / (p2[0] - p1[0])
        #     line_function = lambda x: slope * (x - p1[0]) + p1[1]
        #
        #     is_top_line = True
        #     for x, y in hull_points:
        #         if line_function(x) < y:
        #             is_top_line = False
        #             break
        #
        #     if is_top_line:
        #         top_line = (p1, p2)
        #         break
        #
        # if top_line is not None:
        #     cv2.line(img, top_line[0], top_line[1], (255, 0, 0), 1)

        view_image(img)

        if DEBUG_MODE:
            view_image(new_image)
        # cv2.imwrite(f'test/frame_key_points_{i}.jpg',
        #             cv2.drawKeypoints(image, key_points2, None, color=(0, 255, 0), flags=0))

        cv2.imwrite(f'test/frame_{i}.jpg', img)

        i += 1

    video_capture.release()


def main2():
    video_capture = cv2.VideoCapture('test3.mp4')

    i = 0
    while video_capture.isOpened():
        ret, frame = video_capture.read()
        if frame is None:
            break

        image = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        image = cv2.resize(image, (0, 0), fx=0.5, fy=0.5)
        image = adaptive_canny_edge_detection(image)

        cv2.imwrite(f'./test/{i}.jpg', image)
        i += 1


def main3():
    video_capture = cv2.VideoCapture('test3.mp4')

    template = cv2.imread('temp2.jpg', cv2.IMREAD_GRAYSCALE)
    key_points1, descriptors1 = find_key_points(template)
    # descriptors1 = np.array(descriptors1, np.float32)

    i = 0
    while video_capture.isOpened():
        ret, frame = video_capture.read()
        if frame is None:
            break

        image = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        key_points2, descriptors2 = find_key_points(image)
        # descriptors2 = np.array(descriptors2, np.float32)

        detect_match(template, image, key_points1, key_points2, descriptors1, descriptors2)
        cv2.imwrite(f'./test/{i}.jpg', image)
        i += 1


def main4():
    gray = cv2.imread(
        '/home/u764/Downloads/WhatsApp Image 2020-06-10 at 9.04.01 AM.jpeg',
        cv2.IMREAD_GRAYSCALE)
    view_image(gray)
    detect_key_points(gray)

    gray = cv2.imread(
        '/home/u764/Downloads/OneDrive_2020-08-18/QID Samples/Non SMART ID non Qatari/Botros/20200610_102336.jpg',
        cv2.IMREAD_GRAYSCALE)
    view_image(gray)
    detect_key_points(gray)


def detect_key_points(gray):
    gray = cv2.resize(gray, (0, 0), fx=0.5, fy=0.5, interpolation=cv2.INTER_AREA)
    gray = cv2.GaussianBlur(gray, (3, 3), 0)
    rect_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (13, 5))
    sq_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (13, 5))
    # view_image(cv2.erode(cv2.dilate(gray, rect_kernel), rect_kernel))
    black_hat_image = cv2.morphologyEx(gray,
                                       cv2.MORPH_BLACKHAT,
                                       rect_kernel)
    # view_image(black_hat_image)
    grad_x = cv2.Sobel(black_hat_image, ddepth=cv2.CV_32F, dx=1, dy=0, ksize=-1)
    grad_x = np.absolute(grad_x)
    (minVal, maxVal) = (np.min(grad_x), np.max(grad_x))
    grad_x = (255 * ((grad_x - minVal) / (maxVal - minVal))).astype(np.uint8)
    grad_x = cv2.morphologyEx(grad_x, cv2.MORPH_CLOSE, rect_kernel)
    thresh = cv2.threshold(grad_x, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
    thresh = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, sq_kernel)
    thresh = cv2.erode(thresh, None, iterations=4)
    thresh = cv2.dilate(thresh, np.ones((3, 3)), iterations=5)
    view_image(thresh)
    thresh[thresh < 128] = 0
    thresh[thresh >= 128] = 255
    regions = get_connected_regions(thresh)
    new_image = np.zeros(thresh.shape, np.uint8)
    for region in regions:
        region_matrix = np.array(region)
        region_x = int(np.mean(region_matrix[:, :1]))
        region_y = int(np.mean(region_matrix[:, 1:]))
        new_image[region_y, region_x] = 255
    new_image = cv2.dilate(new_image, np.ones((15, 15)), iterations=1)
    view_image(new_image)


def main5():
    video_capture = cv2.VideoCapture(
        'test.mp4'
        # '/home/u764/Downloads/OneDrive_2020-08-18/QID Samples/Non SMART ID non Qatari/Adrian Mikko/WhatsApp Video 2020-06-10 at 9.08.13 AM.mp4'
    )
    previous_image = None

    mean_hue = []
    max_hue = []

    i = 0
    while video_capture.isOpened():
        ret, frame = video_capture.read()
        if frame is None:
            break

        # image = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        image = cv2.resize(frame, (0, 0), fx=0.5, fy=0.5)

        if previous_image is not None:
            difference = cv2.absdiff(image, previous_image)
            difference = cv2.cvtColor(difference, cv2.COLOR_BGR2GRAY)
            difference[difference >= 15] = 255
            difference[difference < 15] = 0
            view_image(difference)
            multiplier = 5
            # hist = np.histogram(difference.ravel(), [multiplier * i for i in range(0, 255 // multiplier + 1)])

            # hue_values = []
            # for y in range(difference.shape[0]):
            #     for x in range(difference.shape[1]):
            #         if difference[y, x][1] > mean_s:

        i += 1
        previous_image = image

    plt.plot(list(range(1, i)), mean_hue)
    plt.savefig('test_mean.png', dpi=200)
    plt.close('all')

    plt.plot(list(range(1, i)), max_hue)
    plt.savefig('test_max.png', dpi=200)


def find_largest_contour(image):
    contours, hierarchy = cv2.findContours(image, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

    max_area = -1
    largest_contour = None

    for contour in contours:
        width, height = cv2.minAreaRect(contour)[1]
        area = width * height

        if area > max_area:
            max_area = area
            largest_contour = contour

    return largest_contour.reshape(-1, 2), max_area


def get_feature_contours(image):
    image = cv2.morphologyEx(image,
                             cv2.MORPH_BLACKHAT,
                             np.ones((5, 13)))
    image = cv2.threshold(image, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
    for i in range(10):
        image = cv2.erode(image, np.ones((3, 3)))
        image = cv2.dilate(image, np.ones((3, 3)))
    image = cv2.dilate(image, np.ones((5, 13)), iterations=3)
    regions = get_connected_regions(image)
    contour_areas = []
    for region in regions:
        region_image = np.zeros(image.shape, np.uint8)
        for x, y in region:
            region_image[y, x] = 255

        contour_areas.append(find_largest_contour(region_image))
    contour_areas.sort(key=lambda ca: ca[1])
    q1 = np.quantile([a for _, a in contour_areas], 0.3)

    new_image = np.zeros(image.shape, np.uint8)

    for contour, area in contour_areas:
        if area <= q1:
            continue
        for x, y in contour:
            new_image[y, x] = 255

    return new_image


def get_feature_points(image):
    image = cv2.morphologyEx(image,
                             cv2.MORPH_BLACKHAT,
                             np.ones((5, 13)))
    image = cv2.threshold(image, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
    for i in range(10):
        image = cv2.erode(image, np.ones((3, 3)))
        image = cv2.dilate(image, np.ones((3, 3)))
    image = cv2.dilate(image, np.ones((5, 13)), iterations=3)
    regions = get_connected_regions(image)
    contour_areas = []
    for region in regions:
        region_image = np.zeros(image.shape, np.uint8)
        for x, y in region:
            region_image[y, x] = 255

        contour_areas.append(find_largest_contour(region_image))
    contour_areas.sort(key=lambda ca: ca[1])
    q1 = np.quantile([a for _, a in contour_areas], 0.3)

    feature_points = []
    for contour, area in contour_areas:
        contour = cv2.approxPolyDP(contour, 0.4, True).reshape(-1, 2)
        if area <= q1:
            continue

        feature_points.extend([(int(x), int(y)) for x, y in contour])

    return feature_points


def main6():
    image1 = cv2.imread(
        'template.jpg',
        cv2.IMREAD_GRAYSCALE)
    image2 = cv2.imread(
        '/home/u764/Downloads/OneDrive_2020-08-18/QID Samples/Non SMART ID non Qatari/Adrian Mikko/WhatsApp Image 2020-06-10 at 9.04.00 AM (1).jpeg',
        cv2.IMREAD_GRAYSCALE)

    points1 = get_feature_points(image1)
    points1 = [cv2.KeyPoint(x, y, 100) for x, y in points1]
    points2 = get_feature_points(image2)
    points2 = [cv2.KeyPoint(x, y, 100) for x, y in points2]

    orb = cv2.ORB_create(scaleFactor=2, nlevels=3)
    kp1, descriptors1 = orb.compute(cv2.imread('template.jpg'), points1)
    kp2, descriptors2 = orb.compute(cv2.imread(
        '/home/u764/Downloads/OneDrive_2020-08-18/QID Samples/Non SMART ID non Qatari/Adrian Mikko/WhatsApp Image 2020-06-10 at 9.04.00 AM (1).jpeg'),
        points2)

    detect_match(image1,
                 image2,
                 kp1,
                 kp2,
                 descriptors1,
                 descriptors2)


def main7():
    image1 = cv2.imread(
        'template.jpg',
        cv2.IMREAD_GRAYSCALE)

    temp = cv2.imread('template.jpg')

    points1 = get_feature_points(image1)
    points1 = [cv2.KeyPoint(x, y, 5) for x, y in points1]

    i = 0

    video_capture = cv2.VideoCapture('test.mp4')

    while video_capture.isOpened():
        ret, frame = video_capture.read()
        if frame is None:
            break

        image2 = np.array(frame, np.uint8)
        image2_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        points2 = get_feature_points(image2_gray)
        points2 = [cv2.KeyPoint(x, y, 5) for x, y in points2]

        orb = cv2.ORB_create(scaleFactor=2, nlevels=3)

        kp1, descriptors1 = orb.compute(temp, points1)
        kp2, descriptors2 = orb.compute(image2, points2)
        print(descriptors1.shape)
        print(descriptors2.shape)
        match = detect_match(temp, image2, kp1, kp2, descriptors1, descriptors2)
        if match is not None:
            cv2.imwrite(f'./test/frame_{i}.jpg', match)

        i += 1


def adjust_angle_by_quadrant(angle, point):
    x, y = point
    if x < 0 and y > 0:
        return 180 - angle
    if x < 0 and y < 0:
        return 180 + angle
    if x > 0 and y < 0:
        return 360 - angle
    return angle


def compute_angle(origin, point):
    x1, y1 = point - origin

    if math.fabs(x1) > 1e-6:
        theta = math.fabs(math.atan(y1 / x1)) / math.pi * 180
    else:
        theta = 90

    return adjust_angle_by_quadrant(theta, (x1, y1))


def sort_points_anti_clockwise(origin, points):
    point_angle_pairs = [[point, compute_angle(point, origin)] for point in points]
    point_angle_pairs.sort(key=lambda x: x[1])
    return [point for point, _ in point_angle_pairs]


def auto_correct_vertices_order(vertices):
    point1, point2, point3, point4 = vertices

    side1_length = euclidean_distance(point1, point2)
    corresponding_side1_length = euclidean_distance(point4, point3)

    side2_length = euclidean_distance(point2, point3)
    corresponding_side2_length = euclidean_distance(point1, point4)

    if max(side1_length, corresponding_side1_length) < min(side2_length, corresponding_side2_length):
        return [point2,
                point3,
                point4,
                point1]

    return vertices


def triangle_area(point1, point2, point3):
    x1, y1 = point1
    x2, y2 = point2
    x3, y3 = point3

    return math.fabs(0.5 * (x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)))


def calculate_4_polygon_area(point1,
                             point2,
                             point3,
                             point4):
    return triangle_area(point1, point2, point3) + \
           triangle_area(point1, point4, point3)


def estimate_card_width_height_from_vertices(vertices, id_card_width_height_ratio):
    area = calculate_4_polygon_area(*vertices)
    height = math.sqrt(area / id_card_width_height_ratio)
    width = id_card_width_height_ratio * height
    return int(math.floor(width)), int(math.floor(height))


def warp_perspective(image,
                     original_points,
                     target_points,
                     target_width,
                     target_height):
    original_points = np.float32(original_points)
    target_points = np.float32(target_points)

    perspective_transform_matrix = cv2.getPerspectiveTransform(original_points, target_points)

    return cv2.warpPerspective(image,
                               perspective_transform_matrix,
                               (target_width, target_height),
                               flags=cv2.INTER_NEAREST)


def correct_perspective(image, original_vertices, id_card_width_height_ratio):
    target_width, target_height = estimate_card_width_height_from_vertices(original_vertices,
                                                                           id_card_width_height_ratio)

    target_rectangle_vertices = [[0, 0],
                                 [target_width - 1, 0],
                                 [target_width - 1, target_height - 1],
                                 [0, target_height - 1]]

    return warp_perspective(image,
                            original_vertices,
                            target_rectangle_vertices,
                            target_width,
                            target_height)


def get_best_regions(original_image):
    image = cv2.morphologyEx(original_image,
                             cv2.MORPH_BLACKHAT,
                             np.ones((7, 7)))
    image = cv2.threshold(image, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]

    image = cv2.erode(image, np.ones((2, 2)))
    image = cv2.dilate(image, np.ones((2, 2)))
    image = cv2.dilate(image, np.ones((3, 3)), iterations=2)
    image = cv2.dilate(image, np.ones((2, 2)))

    view_image(image)
    regions = get_connected_regions(image)
    contour_areas = []
    slopes = []
    for region in regions:
        np_region = np.array(region, np.int32)
        slopes.append(np.polyfit(np_region[:, 0], np_region[:, 1], 1)[0])
        region_image = np.zeros(image.shape, np.uint8)
        for x, y in region:
            region_image[y, x] = 255
        contour_areas.append(find_largest_contour(region_image))
    contour_areas.sort(key=lambda ca: ca[1])

    print(np.mean(slopes))

    q1 = np.quantile([a for _, a in contour_areas], 0.05)
    all_points = []
    for i in range(len(contour_areas)):
        _, area = contour_areas[i]
        if area <= q1:
            continue
        all_points.extend(regions[i])
    all_points = np.array(all_points, np.int32)

    x = np.mean(all_points[:, 0])
    y = np.mean(all_points[:, 1])
    vertices = find_convex_polygon_vertices_from_contour(all_points, (x, y), 4)
    pts = sort_points_anti_clockwise([x, y], vertices)
    pts = auto_correct_vertices_order(pts)
    corrected_perspective = correct_perspective(original_image,
                                                pts,
                                                1.575)

    return corrected_perspective


def main8():
    video_capture = cv2.VideoCapture(
        '/home/u764/Downloads/OneDrive_2020-08-18/QID Samples/Non SMART ID non Qatari/Adrian Mikko/WhatsApp Video 2020-06-10 at 9.08.13 AM.mp4')

    i = 0
    while video_capture.isOpened():
        ret, frame = video_capture.read()
        if frame is None:
            break

        image = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        cv2.imwrite(f'./test/frame_{i}.jpg', get_best_regions(image))
        i += 1


def detect_key_points_special(image):
    # apply black-hat morphology
    image = cv2.morphologyEx(image,
                             cv2.MORPH_BLACKHAT,
                             np.ones((7, 7)))
    # view_image(image)
    # apply OTSU threshold
    image = cv2.threshold(image, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
    # open morphology on varying kernel sizes
    image = cv2.erode(image, np.ones((2, 2)))
    image = cv2.dilate(image, np.ones((2, 2)))
    image = cv2.erode(image, np.ones((3, 3)))
    image = cv2.dilate(image, np.ones((3, 3)))
    # simple thresholding
    image[image >= 128] = 255
    image[image < 128] = 0
    # get feature points
    points = [(x, y)
              for x, y in itertools.product(range(image.shape[1]), range(image.shape[0]))
              if image[y, x] == 255]
    key_points = [cv2.KeyPoint(x, y, 1) for x, y in points]
    return key_points


def main9():
    template = cv2.imread('mikko_template.jpeg')
    video_capture = cv2.VideoCapture('test.mp4')

    template_gray = cv2.cvtColor(template, cv2.COLOR_BGR2GRAY)

    template_key_points = detect_key_points_special(template_gray)
    orb = cv2.ORB_create(scaleFactor=2, nlevels=3)
    template_key_points, template_descriptors = orb.compute(template, template_key_points)
    i = 0
    while video_capture.isOpened():
        ret, frame = video_capture.read()
        if frame is None:
            break

        image = frame
        image_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        image_key_points = detect_key_points_special(image_gray)
        orb = cv2.ORB_create(scaleFactor=2, nlevels=3)
        image_key_points, image_descriptors = orb.compute(image, image_key_points)

        matched = detect_match(template,
                               image,
                               template_key_points,
                               image_key_points,
                               template_descriptors,
                               image_descriptors)
        cv2.imwrite(f'./test2/{i}.jpg', matched)
        i += 1

    print(template_key_points)


def get_shape_descriptors(image):
    image = cv2.morphologyEx(image,
                             cv2.MORPH_BLACKHAT,
                             np.ones((7, 7)))

    # apply OTSU threshold
    image = cv2.threshold(image, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]

    # open morphology on varying kernel sizes
    image = cv2.erode(image, np.ones((2, 2)))
    image = cv2.dilate(image, np.ones((2, 2)))
    image = cv2.erode(image, np.ones((3, 3)))
    image = cv2.dilate(image, np.ones((3, 3)))
    # view_image(image)

    swelled = cv2.dilate(image, np.ones((5, 5)), iterations=3)
    view_image(swelled)
    # simple thresholding
    swelled[swelled >= 128] = 255
    swelled[swelled < 128] = 0

    regions = get_connected_regions(swelled)

    info = []
    multiplier = 0.05
    for region in regions:
        np_region = np.array(region, np.int32)

        mu_x = np.mean(np_region[:, 0])
        mu_y = np.mean(np_region[:, 1])

        distances = []
        for (x, y) in region:
            distances.append(euclidean_distance((mu_x, mu_y), (x, y)))

        distances = np.array(distances)
        distances /= np.max(distances)

        histogram = np.histogram(distances, bins=[multiplier * i for i in range(0, int(100 / multiplier) + 1)])[0] / (
                100 / multiplier)

        x0 = np.min(np_region[:, 0])
        y0 = np.min(np_region[:, 1])
        x1 = np.max(np_region[:, 0]) + 1
        y1 = np.max(np_region[:, 1]) + 1

        info.append([image[y0:y1, x0:x1], histogram, abs(y1 - y0) * abs(x1 - x0), (x0, y0, x1, y1)])

    info.sort(key=lambda x: x[2])

    min_size = np.quantile([x[2] for x in info], multiplier)
    max_size = np.quantile([x[2] for x in info], 0.95)
    info = list(filter(lambda x: min_size <= x[2] <= max_size, info))

    return info


def main10():
    template = cv2.imread('mikko_template.jpeg', cv2.IMREAD_GRAYSCALE)
    image = cv2.imread(
        '/home/u764/Downloads/OneDrive_2020-08-18/QID Samples/Non SMART ID non Qatari/Adrian Mikko/WhatsApp Image 2020-06-10 at 9.04.00 AM (1).jpeg',
        cv2.IMREAD_GRAYSCALE)

    info1 = get_shape_descriptors(template)
    info2 = get_shape_descriptors(image)

    matches = []

    for i1 in info1:
        best_match = None
        min_dist = 99999999.0
        for i2 in info2:
            dist = euclidean_distance(i1[1], i2[1])
            print(dist)
            if dist < min_dist:
                min_dist = dist
                best_match = i2

        if best_match is not None:
            matches.append((i1, best_match, min_dist))

    matches.sort(key=lambda x: x[2])
    print(len(matches))
    for m in matches:
        print(m[2])
        view_image(m[0][0])
        view_image(m[1][0])


def main11():
    original_image = cv2.imread(
        '/home/u764/Downloads/test.jpeg',
        cv2.IMREAD_GRAYSCALE)

    image = cv2.morphologyEx(original_image,
                             cv2.MORPH_BLACKHAT,
                             np.ones((7, 7)))

    # apply OTSU threshold
    image = cv2.threshold(image, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]

    # open morphology on varying kernel sizes
    image = cv2.erode(image, np.ones((2, 2)))
    image = cv2.dilate(image, np.ones((2, 2)))
    image = cv2.erode(image, np.ones((3, 3)))
    image = cv2.dilate(image, np.ones((3, 3)))
    # view_image(image)

    swelled = cv2.dilate(image, np.ones((5, 5)), iterations=3)
    view_image(swelled)
    # simple thresholding
    swelled[swelled >= 128] = 255
    swelled[swelled < 128] = 0

    regions = get_connected_regions(swelled)

    sizes = [len(x) for x in regions]
    regions.sort(key=lambda x: len(x))
    mu = np.mean(sizes)
    sigma = np.std(sizes)

    regions = list(filter(lambda x: len(x) >= mu - sigma * 0.75, regions))

    points = []
    for region in regions:
        points.extend(region)
    points = np.array(points)

    hull = ConvexHull(points)
    hull_points = []
    for vertex in hull.vertices:
        point = (np.int0(points[vertex, 0]), np.int0(points[vertex, 1]))
        hull_points.append(point)

    # approx = cv2.approxPolyDP(np.array(hull_points), 100, True).reshape(-1, 2)
    approx = hull_points
    print(len(approx))
    img = cv2.cvtColor(original_image, cv2.COLOR_GRAY2BGR)

    for point in approx:
        cv2.circle(img, tuple(point), 10, [0, 255, 0], 5)

    view_image(img)

    # new_image = np.zeros(image.shape, np.uint8)
    #
    # for region in regions:
    #     size = len(region)
    #
    #     if size < mu - sigma * 0.5:
    #         continue
    #
    #     for x, y in region:
    #         new_image[y, x] = 255
    #
    # view_image(new_image)


# def main12():
#     template = cv2.imread('/home/u764/Downloads/test.jpeg')
#     template_gray = cv2.cvtColor(template, cv2.COLOR_BGR2GRAY)
#     template_gray = cv2.resize(template_gray, (0, 0), fx=0.5, fy=0.5, interpolation=cv2.INTER_AREA)
#
#     # image = cv2.imread(
#     #     '/home/u764/Downloads/OneDrive_2020-08-18/QID Samples/Non SMART ID non Qatari/Adrian Mikko/WhatsApp Image 2020-06-10 at 9.04.00 AM (1).jpeg')
#     # image_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
#
#     temp_kp, temp_desc = sift.computeKeypointsAndDescriptors(template_gray)
#
#     video_capture = cv2.VideoCapture('test.mp4')
#
#     while video_capture.isOpened():
#         ret, frame = video_capture.read()
#         if frame is None:
#             break
#
#         image = cv2.rotate(frame, cv2.ROTATE_90_CLOCKWISE)
#         image_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
#         image_gray = cv2.resize(image_gray, (0, 0), fx=0.5, fy=0.5, interpolation=cv2.INTER_AREA)
#         image_kp, image_desc = sift.computeKeypointsAndDescriptors(image_gray)
#         view_image(detect_match(template, image, temp_kp, image_kp, temp_desc, image_desc))
#

if __name__ == '__main__':
    # main()
    # main2()
    # main3()
    # main4()
    # main5()
    # main7()
    # main8()
    # main9()
    # main10()
    main11()
    # main12()

# base
# def main():
#     video_capture = cv2.VideoCapture('test.mp4')
#
#     while video_capture.isOpened():
#         ret, frame = video_capture.read()
#         if frame is None:
#             break
#
#         image = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
#         image = cv2.resize(image, (0, 0), fx=0.5, fy=0.5)
#
#         view_image(image)
