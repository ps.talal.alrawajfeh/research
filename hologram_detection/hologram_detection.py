import math

import cv2
import numpy as np
from matplotlib import pyplot as plt

SINGLE_POINT = 'point'
ORDINARY_LINE = 'ordinary'
HORIZONTAL_LINE = 'horizontal'
VERTICAL_LINE = 'vertical'


def find_largest_contour(image):
    contours, hierarchy = cv2.findContours(image, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

    max_area = -1
    largest_contour = None

    for contour in contours:
        width, height = cv2.minAreaRect(contour)[1]
        area = width * height

        if area > max_area:
            max_area = area
            largest_contour = contour

    return largest_contour.reshape(-1, 2)


def compute_center(points):
    return np.array([np.mean(points[:, :1]), np.mean(points[:, 1:])])


def euclidean_distance(point1, point2):
    return np.sqrt(np.sum(np.square(np.array(point1, np.float32) -
                                    np.array(point2, np.float32))))


def find_furthest_point_from(reference_points, points):
    def compute_point_score(point):
        score = 1.0
        for reference_point in reference_points:
            score *= euclidean_distance(reference_point, point)

        return score

    max_point_score = -1
    furthest_point = None
    for p in points:
        point_score = compute_point_score(p)

        if point_score > max_point_score:
            max_point_score = point_score
            furthest_point = p

    return furthest_point


def find_convex_polygon_vertices_from_contour(polygon_contour,
                                              polygon_center_point,
                                              number_of_vertices):
    vertices = []
    for n in range(number_of_vertices):
        vertex = find_furthest_point_from([polygon_center_point] + vertices,
                                          polygon_contour)
        vertices.append(vertex)
    return vertices


def find_rectangle_vertices_from_contour(contour, center_point):
    return find_convex_polygon_vertices_from_contour(contour,
                                                     center_point,
                                                     4)


def adjust_angle_by_quadrant(angle, point):
    x, y = point
    if x < 0 and y > 0:
        return 180 - angle
    if x < 0 and y < 0:
        return 180 + angle
    if x > 0 and y < 0:
        return 360 - angle
    return angle


def compute_angle(origin, point):
    x1, y1 = point - origin

    if math.fabs(x1) > 1e-6:
        theta = math.fabs(math.atan(y1 / x1)) / math.pi * 180
    else:
        theta = 90

    return adjust_angle_by_quadrant(theta, (x1, y1))


def sort_points_anti_clockwise(origin, points):
    point_angle_pairs = [[point, compute_angle(point, origin)] for point in points]
    point_angle_pairs.sort(key=lambda x: x[1])
    return [point for point, _ in point_angle_pairs]


def get_contour_segments(largest_contour, rectangle_vertices):
    largest_contour = largest_contour.tolist()
    rectangle_vertices = [r.tolist() for r in rectangle_vertices]
    vertex_indices = [largest_contour.index(v) for v in rectangle_vertices]
    segments = []

    for i in range(len(vertex_indices)):
        current_i = vertex_indices[i]
        next_i = vertex_indices[(i + 1) % len(vertex_indices)]

        if current_i < next_i:
            segment = largest_contour[next_i:] + largest_contour[:current_i]
        else:
            segment = largest_contour[next_i:current_i]

        segments.append(segment)

    return segments


def estimate_parametric_slope_and_intercept(n, points, axis=0):
    sum1 = sum([(i + 1) * points[i][axis] for i in range(n)])
    sum2 = sum([points[i][axis] for i in range(n)])
    slope = 12 * (sum1 - sum2 * (n + 1) / 2) / (n * (n + 1) * (n - 1))
    intercept = sum2 / n - slope * (n + 1) / 2

    return slope, intercept


def parametric_linear_regression(points):
    n = len(points)

    slope_x, intercept_x = estimate_parametric_slope_and_intercept(n, points)
    slope_y, intercept_y = estimate_parametric_slope_and_intercept(n, points, 1)

    return (slope_x, intercept_x), (slope_y, intercept_y)


def parametric_line_to_non_parametric(parametric_line):
    (slope_x, intercept_x), (slope_y, intercept_y) = parametric_line

    if slope_x == 0 and slope_y == 0:
        return SINGLE_POINT, [intercept_x, intercept_y]
    if slope_x == 0:
        return VERTICAL_LINE, [intercept_x]
    if slope_y == 0:
        return HORIZONTAL_LINE, [intercept_y]

    slope = slope_y / slope_x
    return ORDINARY_LINE, [slope, intercept_y - slope * intercept_x]


def within_epsilon_neighborhood(x, compared, epsilon=1e-6):
    return compared - epsilon <= x <= compared + epsilon


def find_lines_intersection_point(line1, line2):
    type1, parameters1 = line1
    type2, parameters2 = line2

    if type1 == SINGLE_POINT and type2 == SINGLE_POINT:
        if parameters1 == parameters2:
            return parameters1
        else:
            return None

    if type1 == SINGLE_POINT and type2 == VERTICAL_LINE:
        if parameters1[0] == parameters2[0]:
            return parameters1
        else:
            return None

    if type1 == SINGLE_POINT and type2 == HORIZONTAL_LINE:
        if parameters1[1] == parameters2[0]:
            return parameters1
        else:
            return None

    if type1 == SINGLE_POINT and type2 == ORDINARY_LINE:
        regressed = parameters1[0] * parameters2[0] + parameters2[1]
        if within_epsilon_neighborhood(regressed, parameters1[1]):
            return parameters1
        else:
            return None

    if type1 == VERTICAL_LINE and type2 == VERTICAL_LINE:
        if parameters1 == parameters2:
            return [parameters1[0], parameters1[0]]
        else:
            return None

    if type1 == VERTICAL_LINE and type2 == HORIZONTAL_LINE:
        return [parameters1[0], parameters2[0]]

    if type1 == VERTICAL_LINE and type2 == ORDINARY_LINE:
        return [parameters1[0], parameters1[0] * parameters2[0] + parameters2[1]]

    if type1 == HORIZONTAL_LINE and type2 == HORIZONTAL_LINE:
        if parameters1 == parameters2:
            return [parameters1[0], parameters1[0]]
        else:
            return None

    if type1 == HORIZONTAL_LINE and type2 == ORDINARY_LINE:
        return [(parameters1[0] - parameters2[1]) / parameters2[0], parameters1[0]]

    if type1 == ORDINARY_LINE and type2 == ORDINARY_LINE:
        x = (parameters2[1] - parameters1[1]) / (parameters1[0] - parameters2[0])
        return [x, parameters1[0] * x + parameters1[1]]

    return find_lines_intersection_point(line2, line1)


def enhance_rectangle_vertices(largest_contour, rectangle_vertices):
    segments = get_contour_segments(largest_contour, rectangle_vertices)

    lines = [
        parametric_line_to_non_parametric(
            parametric_linear_regression(segment)
        )
        for segment in segments
    ]

    return np.array([find_lines_intersection_point(lines[i - 1], lines[i])
                     for i in range(len(segments))], np.int), lines


def auto_correct_vertices_order(vertices):
    point1, point2, point3, point4 = vertices

    side1_length = euclidean_distance(point1, point2)
    corresponding_side1_length = euclidean_distance(point4, point3)

    side2_length = euclidean_distance(point2, point3)
    corresponding_side2_length = euclidean_distance(point1, point4)

    if max(side1_length, corresponding_side1_length) < min(side2_length, corresponding_side2_length):
        return [point2,
                point3,
                point4,
                point1]

    return vertices


def view_image(image):
    plt.imshow(image, plt.cm.gray)
    plt.show()


def triangle_area(point1, point2, point3):
    x1, y1 = point1
    x2, y2 = point2
    x3, y3 = point3

    return math.fabs(0.5 * (x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)))


def calculate_4_polygon_area(point1,
                             point2,
                             point3,
                             point4):
    return triangle_area(point1, point2, point3) + \
           triangle_area(point1, point4, point3)


def estimate_card_width_height_from_vertices(vertices, id_card_width_height_ratio):
    area = calculate_4_polygon_area(*vertices)
    height = math.sqrt(area / id_card_width_height_ratio)
    width = id_card_width_height_ratio * height
    return int(math.floor(width)), int(math.floor(height))


def warp_perspective(image,
                     original_points,
                     target_points,
                     target_width,
                     target_height):
    original_points = np.float32(original_points)
    target_points = np.float32(target_points)

    perspective_transform_matrix = cv2.getPerspectiveTransform(original_points, target_points)

    return cv2.warpPerspective(image,
                               perspective_transform_matrix,
                               (target_width, target_height),
                               flags=cv2.INTER_NEAREST)


def correct_perspective(image, original_vertices, id_card_width_height_ratio):
    target_width, target_height = estimate_card_width_height_from_vertices(original_vertices,
                                                                           id_card_width_height_ratio)

    target_rectangle_vertices = [[0, 0],
                                 [target_width - 1, 0],
                                 [target_width - 1, target_height - 1],
                                 [0, target_height - 1]]

    return warp_perspective(image,
                            original_vertices,
                            target_rectangle_vertices,
                            target_width,
                            target_height)


def main():
    image = cv2.imread('/home/u764/Downloads/WhatsApp Image 2020-06-10 at 9.04.00 AM (1).jpeg', cv2.IMREAD_COLOR)
    image_copy = np.array(image, np.uint8)
    image_resized = cv2.resize(image, (0, 0), fx=0.5, fy=0.5)
    image_resized = cv2.GaussianBlur(image_resized, (7, 7), 0)

    image_hsv = cv2.cvtColor(image_resized, cv2.COLOR_BGR2HSV)

    image_hsv[(image_hsv[:, :, 0] < 80) |
              (image_hsv[:, :, 0] > 130) |
              (image_hsv[:, :, 1] < 10) |
              (image_hsv[:, :, 1] > 135) |
              (image_hsv[:, :, 2] < 100)] = 0

    image_gray = np.max(image_hsv, -1)
    image_gray[image_gray > 0] = 255
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (15, 15))
    image_gray = cv2.dilate(image_gray, kernel, iterations=2)
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (15, 15))
    image_gray = cv2.erode(image_gray, kernel, iterations=2)
    # view_image(image_gray)

    largest_contour = find_largest_contour(image_gray)

    for vertex in largest_contour:
        cv2.circle(image_copy, (vertex[0] * 2, vertex[1] * 2), 5, [0, 255, 0], -1)

    center_point = compute_center(largest_contour)
    rectangle_vertices = find_rectangle_vertices_from_contour(largest_contour, center_point)
    rectangle_vertices = sort_points_anti_clockwise(center_point, rectangle_vertices)
    rectangle_vertices, lines = enhance_rectangle_vertices(largest_contour, rectangle_vertices)

    for line in lines:
        x = image_resized.shape[1] - 1
        x *= 2
        cv2.line(image_copy, (0, int(line[1][1]) * 2),
                 (x, int(x * line[1][0] + line[1][1] * 2)),
                 [255, 0, 0], 3)

    rectangle_vertices = auto_correct_vertices_order(rectangle_vertices)

    for vertex in rectangle_vertices:
        cv2.circle(image_copy, (vertex[0] * 2, vertex[1] * 2), 15, [0, 0, 255], -1)

    corrected_perspective = correct_perspective(image,
                                                [[x / 0.5, y / 0.5]
                                                 for x, y in rectangle_vertices],
                                                1.575)
    view_image(image_copy)
    view_image(corrected_perspective)


if __name__ == '__main__':
    # import time
    #
    # start = time.time()
    # for i in range(100):
    #     main()
    # end = time.time()
    #
    # print(end - start)
    main()
