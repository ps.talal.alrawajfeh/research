from datetime import datetime
import random
from calendar import monthrange
from sklearn.cluster import MiniBatchKMeans, KMeans

import numpy as np
from tabulate import tabulate


class PositiveGaussianDistribution:
    def __init__(self, mu, sigma):
        self.mu = mu
        self.sigma = sigma

    def sample(self):
        x = random.gauss(self.mu, self.sigma)
        if x < 0:
            return 0
        return x


def main():
    day_map = {}
    week_map = {}
    previous_weekday = datetime(day=1, month=1, year=2022).weekday()
    current_week = 1
    current_day = 1
    for month in range(1, 13):
        max_days = monthrange(year=2022, month=month)[1]
        for day in range(1, max_days + 1):
            current_date = datetime(day=day, month=month, year=2022)
            day_map[(day, month, 2022)] = current_day
            current_day += 1
            current_weekday = current_date.weekday()
            if current_weekday < previous_weekday:
                current_week += 1
            week_map[(day, month, 2022)] = current_week
            previous_weekday = current_weekday
    weeks = set()
    days = set()
    for v in week_map.values():
        weeks.add(v)
    for v in day_map.values():
        days.add(v)

    number_of_customers = 1000
    max_number_of_transactions = 1000
    customer_ids = list(range(number_of_customers))

    customer_ids_transactions = [random.randint(1, max_number_of_transactions) for _ in range(len(customer_ids))]
    customer_ids_amount_distributions = []
    for _ in range(len(customer_ids)):
        amount_range = random.choice([1, 2, 3, 4])
        mu = random.randint(1, 10 ** amount_range)
        customer_ids_amount_distributions.append(PositiveGaussianDistribution(mu, random.randint(0, mu)))

    data = []
    for customer_id in customer_ids:
        for _ in range(customer_ids_transactions[customer_id]):
            month = random.randint(1, 12)
            max_days = monthrange(year=2022, month=month)[1]
            day = random.randint(1, max_days)
            transaction_date = datetime(day=day, month=month, year=2022)
            transaction_amount = max(1.0, customer_ids_amount_distributions[customer_id].sample())
            data.append([customer_id, transaction_date, transaction_amount])

    # total number of transactions in past 3 months
    aggregations1 = {}
    for customer_id in customer_ids:
        aggregations1[customer_id] = 0

    for row in data:
        customer_id = row[0]
        transaction_date = row[1]
        if transaction_date.year < 2022 or transaction_date.month < 10:
            continue
        aggregations1[customer_id] += 1

    # total number of transactions in past 2 months
    aggregations2 = {}
    for customer_id in customer_ids:
        aggregations2[customer_id] = 0

    for row in data:
        customer_id = row[0]
        transaction_date = row[1]
        if transaction_date.year < 2022 or transaction_date.month < 11:
            continue
        aggregations2[customer_id] += 1

    # total number of transactions in past month
    aggregations3 = {}
    for customer_id in customer_ids:
        aggregations3[customer_id] = 0

    for row in data:
        customer_id = row[0]
        transaction_date = row[1]
        if transaction_date.year < 2022 or transaction_date.month < 11:
            continue
        aggregations3[customer_id] += 1

    # average monthly number of transactions
    aggregations4 = {}
    for customer_id in customer_ids:
        aggregations4[customer_id] = {}
        for i in range(1, 13):
            aggregations4[customer_id][i] = 0

    for row in data:
        customer_id = row[0]
        transaction_date = row[1]
        aggregations4[customer_id][transaction_date.month] += 1

    for customer_id in aggregations4:
        aggregations4[customer_id] = sum(aggregations4[customer_id].values()) / len(aggregations4[customer_id])

    # average weekly number of transactions
    aggregations5 = {}
    for customer_id in customer_ids:
        aggregations5[customer_id] = {}
        for i in range(1, len(weeks) + 1):
            aggregations5[customer_id][i] = 0

    for row in data:
        customer_id = row[0]
        transaction_date = row[1]
        week = week_map[(transaction_date.day, transaction_date.month, transaction_date.year)]
        aggregations5[customer_id][week] += 1

    for customer_id in aggregations5:
        aggregations5[customer_id] = sum(aggregations5[customer_id].values()) / len(aggregations5[customer_id])

    # average daily number of transactions
    aggregations6 = {}
    for customer_id in customer_ids:
        aggregations6[customer_id] = {}
        for i in range(1, len(days) + 1):
            aggregations6[customer_id][i] = 0

    for row in data:
        customer_id = row[0]
        transaction_date = row[1]
        day = day_map[(transaction_date.day, transaction_date.month, transaction_date.year)]
        aggregations6[customer_id][day] += 1

    for customer_id in aggregations6:
        aggregations6[customer_id] = sum(aggregations6[customer_id].values()) / len(aggregations6[customer_id])

    # sum of amounts in past 3 months
    aggregations7 = {}
    for customer_id in customer_ids:
        aggregations7[customer_id] = 0

    for row in data:
        customer_id = row[0]
        transaction_date = row[1]
        if transaction_date.year < 2022 or transaction_date.month < 10:
            continue
        aggregations7[customer_id] += row[2]

    # sum of amounts in past 2 months
    aggregations8 = {}
    for customer_id in customer_ids:
        aggregations8[customer_id] = 0

    for row in data:
        customer_id = row[0]
        transaction_date = row[1]
        if transaction_date.year < 2022 or transaction_date.month < 11:
            continue
        aggregations8[customer_id] += row[2]

    # sum of amounts if past month
    aggregations9 = {}
    for customer_id in customer_ids:
        aggregations9[customer_id] = 0

    for row in data:
        customer_id = row[0]
        transaction_date = row[1]
        if transaction_date.year < 2022 or transaction_date.month < 11:
            continue
        aggregations9[customer_id] += row[2]

    # average monthly sum of amounts
    aggregations10 = {}
    for customer_id in customer_ids:
        aggregations10[customer_id] = {}
        for i in range(1, 13):
            aggregations10[customer_id][i] = 0

    for row in data:
        customer_id = row[0]
        transaction_date = row[1]
        aggregations10[customer_id][transaction_date.month] += row[2]

    for customer_id in aggregations10:
        aggregations10[customer_id] = sum(aggregations10[customer_id].values()) / len(aggregations10[customer_id])

    # average weekly sum of amounts
    aggregations11 = {}
    for customer_id in customer_ids:
        aggregations11[customer_id] = {}
        for i in range(1, len(weeks) + 1):
            aggregations11[customer_id][i] = 0

    for row in data:
        customer_id = row[0]
        transaction_date = row[1]
        week = week_map[(transaction_date.day, transaction_date.month, transaction_date.year)]
        aggregations11[customer_id][week] += row[2]

    for customer_id in aggregations11:
        aggregations11[customer_id] = sum(aggregations11[customer_id].values()) / len(aggregations11[customer_id])

    # average daily sum of amounts
    aggregations12 = {}
    for customer_id in customer_ids:
        aggregations12[customer_id] = {}
        for i in range(1, len(days) + 1):
            aggregations12[customer_id][i] = 0

    for row in data:
        customer_id = row[0]
        transaction_date = row[1]
        day = day_map[(transaction_date.day, transaction_date.month, transaction_date.year)]
        aggregations12[customer_id][day] += row[2]

    for customer_id in aggregations12:
        aggregations12[customer_id] = sum(aggregations12[customer_id].values()) / len(aggregations12[customer_id])

    customers = []
    for customer_id in customer_ids:
        customer_row = []
        for aggregation in [aggregations1,
                            aggregations2,
                            aggregations3,
                            aggregations4,
                            aggregations5,
                            aggregations6,
                            aggregations7,
                            aggregations8,
                            aggregations9,
                            aggregations10,
                            aggregations11,
                            aggregations12]:
            customer_row.append(aggregation[customer_id])
        customers.append(customer_row)

    kmeans = KMeans(n_clusters=4,
                    random_state=0)
    customers = np.array(customers, np.float32)
    kmeans.fit(customers)
    labels = kmeans.predict(customers)

    clusters = dict()
    for i in range(4):
        clusters[i] = []

    for i in range(len(customer_ids)):
        clusters[labels[i]].append(customers[i])

    for cluster_id in clusters:
        print(f'cluster id: {cluster_id}')
        cluster = clusters[cluster_id]

        cluster = np.array(cluster, np.float32)
        row1 = ["min value"]
        for i in range(cluster.shape[-1]):
            min_value = np.min(cluster[:, i])
            row1.append(min_value)

        row2 = ["max value"]
        for i in range(cluster.shape[-1]):
            max_value = np.max(cluster[:, i])
            row2.append(max_value)

        row3 = ["average value"]
        for i in range(cluster.shape[-1]):
            avg_value = np.mean(cluster[:, i])
            row3.append(avg_value)

        print(tabulate([row1, row2, row3],
                       headers=['count past 3 months', 'count past 2 months', 'count past month',
                                'average count monthly',
                                'average count weekly', 'average count daily', 'sum past 3 months', 'sum past 2 months',
                                'sum past month', 'average sum monthly', 'average sum weekly', 'average sum daily']))


if __name__ == '__main__':
    main()
