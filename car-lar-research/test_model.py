#!/usr/bin/python3.6

import math
import os
import random

import cv2
import editdistance
import keras.backend as K
import numpy as np
from keras import Model
from keras.engine.saving import load_model
from keras.utils import plot_model
from matplotlib import pyplot as plt
from tqdm import tqdm

from image_utils import load_all_image_paths
from settings import SETTINGS
import tensorflow as tf

# =============================================================================


MAX_POOL_SIZE = 2
MAX_COMMAS_COUNT = (SETTINGS['integral_part_max_length'] - 1) // 3

# the additional 1 is for the decimal point
MAX_LABEL_LENGTH = SETTINGS['integral_part_max_length'] + \
                   SETTINGS['decimal_part_max_length'] + MAX_COMMAS_COUNT + 1

CHARACTERS = '0123456789/'
BLANK_CLASS = len(CHARACTERS)


# =============================================================================


def label_to_classes(label):
    return [CHARACTERS.index(l) for l in label]


def get_input_shape():
    if K.image_data_format() == 'channels_first':
        input_shape = (1, SETTINGS['input_image_height'],
                       SETTINGS['input_image_width'])
    else:
        input_shape = (SETTINGS['input_image_height'],
                       SETTINGS['input_image_width'], 1)
    return input_shape


def pre_process(image):
    test = image.astype(np.float).reshape(get_input_shape()) / 255.0
    print(test[0, 0, 0])
    return test


def post_process_label(label):
    post_processed_label = [c for c in label]
    while len(post_processed_label) > 0 and post_processed_label[0] == '-':
        post_processed_label.pop(0)
    while len(post_processed_label) > 0 and post_processed_label[len(post_processed_label) - 1] == '-':
        post_processed_label.pop(len(post_processed_label) - 1)
    label = ''.join(post_processed_label).replace('-', '.')
    return label


def predict_batch(model, images, greedy=False):
    model_without_softmax = Model(input=model.input,
                                  output=model.get_layer('logits').output)

    input_tensor = np.array([pre_process(image) for image in images])
    predictions = model_without_softmax.predict(input_tensor)[:, 2:, :]

    time_steps = 30 - 2

    decoded_batch = []
    probabilities = []
    for prediction in predictions:
        if greedy:
            decoded, neg_sum_logits = tf.nn.ctc_greedy_decoder(prediction.reshape((time_steps, 1, len(CHARACTERS) + 1)),
                                                               [time_steps])

            prob = tf.reduce_prod(tf.math.reduce_max(
                tf.nn.softmax(prediction), -1, keepdims=True)).numpy()
            probabilities.append(prob)
        else:
            decoded, log_prob = tf.nn.ctc_beam_search_decoder(prediction.reshape((time_steps, 1, len(CHARACTERS) + 1)),
                                                              [time_steps])
            probabilities.append(np.exp(log_prob[0]))

        decoded_batch.append(tf.sparse.to_dense(decoded[0]).numpy()[0])

    labels = []
    for seq in decoded_batch:
        labels.append(post_process_label(
            ''.join([CHARACTERS[c] for c in seq])))

    return labels, probabilities


def pad_horizontally_centered(image, padding_length, color):
    return np.pad(image,
                  ((0, 0), (padding_length, padding_length)),
                  'constant',
                  constant_values=((0, 0), (color, color)))


def pad_vertically_centered(image, padding_length, color):
    return np.pad(image,
                  ((padding_length, padding_length),
                   (0, 0)),
                  'constant',
                  constant_values=((color, color), (0, 0)))


def resize_enhance(image):
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY).astype(np.float)
    image += (165 - (np.mean(image) + np.median(image)) / 2)
    image[image < 0] = 0
    image[image > 255] = 255
    image = image.astype(np.uint8)

    ratio = image.shape[0] / image.shape[1]
    color = (np.max(image) + np.mean(image)) // 2
    if ratio > 40 / 240:
        width_pad_ratio = ratio / (40 / 240)
        width_diff = width_pad_ratio * image.shape[1] - image.shape[1]
        width_pad = int(math.ceil(width_diff / 2))

        image = np.pad(image, ((0, 0), (width_pad, width_pad)), 'constant',
                       constant_values=((0, 0), (color, color)))
    elif ratio < 40 / 240:
        height_pad_ratio = (40 / 240) / ratio
        height_diff = height_pad_ratio * image.shape[0] - image.shape[0]
        height_pad = int(math.ceil(height_diff / 2))

        image = np.pad(image, ((height_pad, height_pad), (0, 0)), 'constant',
                       constant_values=((color, color), (0, 0)))

    image = cv2.resize(image, (240, 40), interpolation=cv2.INTER_CUBIC)

    return image


def evaluate_model(model, batch_size=64):
    # files = load_all_image_paths('/home/u764/Development/progressoft/en-car-reader/data/test/mutaz')
    files = load_all_image_paths(
        '/home/u764/Development/progressoft/asv-datasets/output')
    files = list(filter(lambda f: len(f[0:f.index('_')]) != 0, files))

    distances_mean = 0
    distance_ratios_mean = 0
    amount_overall_accuracy = 0

    print('evaluating model on test samples...')
    iterations = math.ceil(len(files) / batch_size)
    progress_bar = tqdm(total=iterations)
    count = 0
    all_probabilities = []

    for i in range(iterations):
        batch = files[i * batch_size: (i + 1) * batch_size]

        images = [255 - resize_enhance(cv2.imread(p)) for p in batch]
        labels = [os.path.basename(
            f)[0:os.path.basename(f).index('_')] for f in batch]

        predictions, probabilities = predict_batch(model, images)

        for j in range(len(predictions)):
            predicted = predictions[j]
            label = post_process_label(labels[j])

            edit_distance = editdistance.eval(predicted, label)
            distances_mean += edit_distance
            distance_ratios_mean += edit_distance * 1.0 / len(label)

            if label.replace(',', '') == predicted.replace(',', ''):
                amount_overall_accuracy += 1
                all_probabilities.append([probabilities[j], True])
            else:
                all_probabilities.append([probabilities[j], False])
                # plt.title(label)
                # plt.suptitle(predicted)
                # plt.imshow(images[j], cmap=plt.cm.gray)
                # plt.show()
            count += 1
        progress_bar.update()

    progress_bar.close()

    distances_mean /= len(files)
    distance_ratios_mean /= len(files)
    amount_overall_accuracy /= len(files)
    print('\n\nevaluation results:')
    print(f'=> average distances:       {distances_mean}')
    print(f'=> average distance ratios: {distance_ratios_mean}')

    print(f'\n\nmodel accuracy:           {100 - distance_ratios_mean * 100}')
    print(f'amount overall accuracy:    {amount_overall_accuracy * 100}')

    bins = [i * 5 for i in range(100 // 5 + 1)]
    plt.hist(np.array([x[0] for x in all_probabilities],
                      np.float) * 100, bins, color='blue', label='forgery')
    plt.show()

    with open('results.txt', 'a') as f:
        for x, y in all_probabilities:
            f.write(str(x) + '\t' + str(y) + '\n')

    return distances_mean, distance_ratios_mean, amount_overall_accuracy


def test_model(model, test_image):
    img = 255 - cv2.imread(test_image, cv2.IMREAD_GRAYSCALE)
    plt.imshow(img, cmap=plt.cm.gray)
    plt.show()

    # img =  resize_enhance(img)
    name = os.path.basename(test_image)

    print("\n\n")
    # print(f"expected: {name[0:name.index('_')]}")
    print(f"actual:   {predict_batch(model, [img])}")


if __name__ == '__main__':
    loaded_model = load_model('/home/u764/Development/progressoft/car-lar/model/date.h5')
    # plot_model(loaded_model, to_file='icr.png', show_shapes=True)

    test_model(
        loaded_model, '/home/u764/Desktop/debug/c38df009-cb69-4020-aec7-8f8c99b1db1a-date-input.bmp')

    # evaluate_model(loaded_model)
