#!/usr/bin/python3
import os

import cv2
import numpy as np
import math

from image_utils import load_all_image_paths


def process_image(image):
    image = cv2.imread(image, cv2.IMREAD_GRAYSCALE).astype(np.uint8)
    color = (np.max(image) + np.mean(image)) // 2

    ratio = image.shape[0] / image.shape[1]

    if ratio > 40 / 240:
        width_pad_ratio = ratio / (40 / 240)
        width_diff = width_pad_ratio * image.shape[1] - image.shape[1]
        width_pad = int(math.ceil(width_diff / 2))

        image = np.pad(image, ((0, 0), (width_pad, width_pad)), 'constant',
                       constant_values=((0, 0), (color, color)))
    elif ratio < 40 / 240:
        height_pad_ratio = (40 / 240) / ratio
        height_diff = height_pad_ratio * image.shape[0] - image.shape[0]
        height_pad = int(math.ceil(height_diff / 2))

        image = np.pad(image, ((height_pad, height_pad), (0, 0)), 'constant',
                       constant_values=((color, color), (0, 0)))

    return 255 - cv2.resize(image, (240, 40), interpolation=cv2.INTER_AREA)


def main():
    # paths = load_all_image_paths('/home/u764/Downloads/cleaned-train')
    # dest_dir = '/home/u764/Downloads/train/car'

    paths = load_all_image_paths('/home/u764/Downloads/clean-validation')
    dest_dir = '/home/u764/Downloads/validation/car'

    if not os.path.isdir(dest_dir):
        os.makedirs(dest_dir)

    count = 0
    labels = []

    for p in paths:
        image = process_image(p)
        label = os.path.basename(p)
        print(label)
        label = label[0:label.index('_')]
        labels.append(str(count).encode() + b'\0' + label.encode() + b'\n')

        cv2.imwrite(os.path.join(dest_dir, str(count) + '.jpg'), image)
        count += 1

    labels_file = os.path.join(dest_dir, 'labels.dat')
    with open(labels_file, 'ab') as f:
        for l in labels:
            f.write(l)


if __name__ == '__main__':
    main()
