if __name__ == '__main__':
    pairs = []

    with open('/home/u764/Development/progressoft/research/car-lar-research/results.txt', 'r') as f:
        line = f.readline()

        while line != '' and line is not None:
            columns = line.split('\t')
            prob = float(columns[0][1:len(columns[0]) - 1]) * 100
            is_correct = columns[1][0:len(columns[1]) - 1] == 'True'

            pairs.append([prob, is_correct])
            line = f.readline()

    threshold = 99
    while threshold < 100.01:
        acc = 0
        far = 0
        frr = 0
        total_above_threshold = 0
        total_below_threshold = 0

        for prob, is_correct in pairs:
            if prob >= threshold:
                total_above_threshold += 1
            else:
                total_below_threshold += 1
            if prob >= threshold and is_correct:
                acc += 1
            if prob >= threshold and not is_correct:
                far += 1
            if prob < threshold and is_correct:
                frr += 1

        if frr == 0 and total_below_threshold == 0:
            frr = 0
            total_below_threshold = 1

        if far == 0 and total_above_threshold == 0:
            far = 0
            total_above_threshold = 1

        print(
            f'threshold: {threshold}, overall_accuracy: {acc / (total_above_threshold + total_below_threshold)}, FAR: {far / total_above_threshold}, FRR: {frr / total_below_threshold}')
        threshold += 0.01

    # print(pairs)
