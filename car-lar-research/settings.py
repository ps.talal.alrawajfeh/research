#!/usr/bin/python3.6

import json


def read_settings_file(settings_file_path):
    with open(settings_file_path, 'r') as f:
        settings = f.read()
    return json.loads(settings)


SETTINGS = read_settings_file('./settings.json')
