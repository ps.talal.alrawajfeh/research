#!/usr/bin/env bash

7z a car-lar.7z \
  ../car \
  ../character-generator \
  ../concerns \
  ../data \
  ../lar \
  ../model \
  ../clean_workspace.bat \
  ../clean_workspace.sh \
  ../requirements.txt
