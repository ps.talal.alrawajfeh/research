#!/usr/bin/python3

from concerns.settings import SETTINGS
import random
from PIL import Image, ImageDraw, ImageFont
import numpy as np
import cv2
from matplotlib import pyplot as plt


def random_sign():
    return (-1) ** random.randint(0, 9)


def random_bool():
    return True if random_sign() == 1 else False


def change_image_intensity(image, intensity_delta):
    image = image.astype(np.int32)
    image[image > 0] += intensity_delta
    image[image > 255] = 255
    image[image < 0] = 0
    return image.astype(np.uint8)


def generate_printed_sequence(font,
                              enable_enclosings=True):
    sequence_label = ''
    left_enclosings_str = ''
    right_enclosings_str = ''

    add_decimal = random_bool()
    if add_decimal:
        decimals = random.randint(SETTINGS['decimal_part_min_length'], SETTINGS['decimal_part_max_length'])
    else:
        decimals = 0

    integral_length = random.randint(SETTINGS['integral_part_min_length'], SETTINGS['integral_part_max_length'])
    possible_commas = (integral_length - 1) // 3
    commas = random.randint(0, possible_commas)

    add_enclosings = random_bool()
    if add_enclosings:
        left_enclosings = random.randint(SETTINGS['printed_min_left_enclosings_length'],
                                         SETTINGS['printed_max_left_enclosings_length'])
        right_enclosings = random.randint(SETTINGS['printed_min_right_enclosings_length'],
                                          SETTINGS['printed_max_right_enclosings_length'])
    else:
        left_enclosings = 0
        right_enclosings = 0

    comma_indices = random.sample(list(range(possible_commas)), commas)
    for i in range(integral_length):
        if 0 < i < integral_length - 1 and \
                commas > 0 and \
                (integral_length - i) % 3 == 0 and \
                (integral_length - i) // 3 - 1 in comma_indices:
            sequence_label += ','
        sequence_label += str(random.randint(0, 9))

    for i in range(decimals):
        if i == 0:
            sequence_label += '.'
        sequence_label += str(random.randint(0, 9))

    if enable_enclosings:
        for _ in range(left_enclosings):
            left_enclosings_str += random.choice(['=', '=/', '#', '*', '-', '-/'])

    if enable_enclosings:
        for _ in range(right_enclosings):
            right_enclosings_str += random.choice(['=', '/=', '#', '*', '-', '/-'])

    template = cv2.imread('/home/u764/Development/progressoft/en-car-reader/data/templates/10.jpg',
                          cv2.IMREAD_GRAYSCALE)
    template = Image.fromarray(template, 'L')

    draw = ImageDraw.Draw(template)
    sequence_width, sequence_height = draw.textsize(sequence_label, font=font)

    if enable_enclosings and left_enclosings > 0:
        left_enclosings_width, left_enclosings_height = draw.textsize(left_enclosings_str, font=font)
        left_spacing = random.randint(SETTINGS['printed_min_left_spacing'],
                                      SETTINGS['printed_max_left_spacing'])
    else:
        left_enclosings_width, left_enclosings_height = 0, 0
        left_spacing = 0

    if enable_enclosings and right_enclosings > 0:
        right_enclosings_width, right_enclosings_height = draw.textsize(right_enclosings_str, font=font)
        right_spacing = random.randint(SETTINGS['printed_min_right_spacing'],
                                       SETTINGS['printed_max_right_spacing'])
    else:
        right_enclosings_width, right_enclosings_height = 0, 0
        right_spacing = 0

    total_width = left_enclosings_width + left_spacing + sequence_width + right_spacing + right_enclosings_width
    total_height = np.max([left_enclosings_height, sequence_height, right_enclosings_height])

    turn = 0
    while total_width > SETTINGS['car_input_image_width']:
        if enable_enclosings:
            if turn == 0 and left_enclosings > 0:
                left_enclosings_str = left_enclosings_str[0:len(left_enclosings_str) - 1]
            elif turn == 1 and right_enclosings > 0:
                right_enclosings_str = right_enclosings_str[0:len(right_enclosings_str) - 1]
            else:
                sequence_label = sequence_label[0:len(sequence_label) - 1]
        else:
            sequence_label = sequence_label[0:len(sequence_label) - 1]

        sequence_width, sequence_height = draw.textsize(sequence_label, font=font)

        if left_enclosings > 0 and enable_enclosings:
            left_enclosings_width, left_enclosings_height = draw.textsize(left_enclosings_str, font=font)
        if right_enclosings > 0 and enable_enclosings:
            right_enclosings_width, right_enclosings_height = draw.textsize(right_enclosings_str, font=font)

        total_width = left_enclosings_width + left_spacing + sequence_width + right_spacing + right_enclosings_width

        turn += 1
        if turn == 3:
            turn = 0

    start_x = (SETTINGS['car_input_image_width'] - total_width) // 2
    start_y = (SETTINGS['car_input_image_height'] - total_height) // 2

    start_x += random_sign() * random.randint(0, start_x)
    start_y += random_sign() * random.randint(0, start_y)

    text_intensity_delta = random.randint(0, 10)
    all_intensity_delta = random_sign() * random.randint(0, 50)

    if enable_enclosings and left_enclosings > 0:
        draw.text((start_x, start_y), left_enclosings_str, fill=text_intensity_delta, font=font)
    start_x += left_enclosings_width + left_spacing

    draw.text((start_x, start_y), sequence_label, fill=text_intensity_delta, font=font)

    start_x += sequence_width
    if enable_enclosings and right_enclosings > 0:
        start_x += right_spacing
        draw.text((start_x, start_y), right_enclosings_str, fill=text_intensity_delta, font=font)

    template = np.asarray(template)
    template = change_image_intensity(template, all_intensity_delta)

    return sequence_label, template


def main():
    font_path = "../data/car_handwritten_fonts/20.ttf"
    font = ImageFont.truetype(font_path,
                              SETTINGS['car_printed_min_font_size'])
    label, sequence = generate_printed_sequence(font=font)

    f, axs = plt.subplots(3, 1)

    axs[0].set_title(label)
    axs[0].imshow(sequence, cmap=plt.cm.gray)

    font = ImageFont.truetype(font_path,
                              (SETTINGS['printed_max_font_size'] + SETTINGS['printed_min_font_size']) // 2)
    label, sequence = generate_printed_sequence(font=font)

    axs[1].set_title(label)
    axs[1].imshow(sequence, cmap=plt.cm.gray)

    font = ImageFont.truetype(font_path,
                              SETTINGS['printed_max_font_size'])
    label, sequence = generate_printed_sequence(font=font)

    axs[2].set_title(label)
    axs[2].imshow(sequence, cmap=plt.cm.gray)

    plt.show()


if __name__ == '__main__':
    main()
