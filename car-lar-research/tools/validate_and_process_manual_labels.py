#!/usr/bin/python3.6
import math
import os

import cv2
import editdistance
import numpy as np

from concerns.settings import SETTINGS

WHITE_SPACE_CHARS = [' ', '\t', '\r', '\n']


def normalize_word(word):
    return word.replace('-', ' ').replace(',', '')


def extract_normalized_words(label):
    words = label.split(' ')
    cleaned_words = []
    for word in words:
        if word.strip() == '':
            continue
        for component in word.replace(',', '').split('-'):
            cleaned_words.append(component.lower())
    return cleaned_words


def label_to_classes(label):
    cleaned_words = extract_normalized_words(label)

    classes = []
    for word in cleaned_words:
        try:
            classes.append(SETTINGS['lar_classes'].index(word))
        except ValueError:
            min_distance = 1000
            min_i = -1
            for i in range(len(SETTINGS['lar_classes'])):
                c = SETTINGS['lar_classes'][i]
                distance = editdistance.eval(word, c)
                if distance < min_distance:
                    min_distance = distance
                    min_i = i
            classes.append(min_i)

    return classes


def label_from_classes(label):
    return ' '.join([SETTINGS['lar_classes'][i] for i in label_to_classes(label)])


def adjust_image(image):
    ratio = image.shape[0] / image.shape[1]
    color = (np.max(image) + np.mean(image)) // 2

    input_image_width = SETTINGS['lar_input_image_width']
    input_image_height = SETTINGS['lar_input_image_height']

    desired_ratio = input_image_height / input_image_width

    if ratio > input_image_height / input_image_width:
        width_pad_ratio = ratio / desired_ratio
        width_diff = width_pad_ratio * image.shape[1] - image.shape[1]
        width_pad = int(math.ceil(width_diff / 2))

        image = np.pad(image,
                       ((0, 0), (width_pad, width_pad)),
                       'constant',
                       constant_values=((0, 0), (color, color)))
    elif ratio < input_image_height / input_image_width:
        height_pad_ratio = desired_ratio / ratio
        height_diff = height_pad_ratio * image.shape[0] - image.shape[0]
        height_pad = int(math.ceil(height_diff / 2))

        image = np.pad(image,
                       ((height_pad, height_pad), (0, 0)),
                       'constant',
                       constant_values=((color, color), (0, 0)))

    image = cv2.resize(image,
                       (input_image_width, input_image_height),
                       interpolation=cv2.INTER_CUBIC)

    return 255 - image


def main():
    samples_dir = '/home/u764/Downloads/lar'
    labels_file = '/home/u764/Downloads/labels.txt'
    output_dir = '/home/u764/Development/progressoft/car-lar/data/test/lar'

    image_ids = []

    samples = os.listdir(samples_dir)
    for s in samples:
        image_id = s.split(os.path.extsep)[0]
        image_ids.append(image_id)

    label_ids = []
    labels = []

    with open(labels_file, 'r') as f:
        l = f.readline()
        while l != '' and l is not None:
            if l.strip() == '':
                l = f.readline()
                continue
            i = 0
            while i < len(l):
                if l[i] in WHITE_SPACE_CHARS:
                    break
                i += 1
            label_ids.append(l[:i].strip())
            labels.append(label_from_classes(l[i:].strip()))
            l = f.readline()

    images_not_in_labels = []
    labels_not_in_images = []

    for image_id in image_ids:
        if image_id not in label_ids:
            images_not_in_labels.append(image_id)

    for label_id in label_ids:
        if label_id not in image_ids:
            labels_not_in_images.append(label_id)

    is_everything_ok = True

    if len(images_not_in_labels) != 0:
        print('images that are not labeled: ' + str(images_not_in_labels))
        is_everything_ok = False

    if len(labels_not_in_images) != 0:
        print('labels that have no corresponding images: ' + str(labels_not_in_images))
        is_everything_ok = False

    if is_everything_ok:
        if not os.path.isdir(output_dir):
            os.makedirs(output_dir)

        for i in range(len(image_ids)):
            image = adjust_image(cv2.imread(samples_dir + '/' + samples[i], cv2.IMREAD_GRAYSCALE))
            cv2.imwrite(output_dir + '/' + samples[i], image)

        with open(output_dir + '/labels.dat', 'wb') as f:
            for i in range(len(labels)):
                label_id = label_ids[i]
                label = labels[i]

                f.write(label_id.encode() + b'\0' + label.encode() + b'\n')


if __name__ == '__main__':
    main()
