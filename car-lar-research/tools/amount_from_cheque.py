#!/usr/bin/python3.6

import math
import os
import sys

import cv2
import keras.backend as K
import numpy as np
import ps_amount_extractor
from keras.engine.saving import load_model

from concerns.generation_utils import load_all_image_paths, file_extension
from concerns.settings import SETTINGS
from tqdm import tqdm

CHARACTERS = '0123456789.,-'
BLANK_CLASS = len(CHARACTERS)


def extract_amount_box(image):
    image_data = []
    for row in image:
        image_data.extend(row)

    native_data_array = ps_amount_extractor.UCharArray(len(image_data))
    for i in range(len(image_data)):
        native_data_array[i] = int(image_data[i])

    result = ps_amount_extractor.detectAmount(native_data_array, image.shape[0], image.shape[1])

    if result.x <= 0 or \
            result.width <= 0 or \
            result.y <= 0 or \
            result.height <= 0:
        return None

    return image[result.y:result.y + result.height,
           result.x:result.x + result.width]


def get_input_shape():
    if K.image_data_format() == 'channels_first':
        input_shape = (1, SETTINGS['car_input_image_height'], SETTINGS['car_input_image_width'])
    else:
        input_shape = (SETTINGS['car_input_image_height'], SETTINGS['car_input_image_width'], 1)
    return input_shape


def pre_process(image):
    return image.astype(np.float).reshape(get_input_shape()) / 255.0


def predict_amount(model, amount_box, enable_post_processing=True):
    predictions = model.predict(np.array([pre_process(amount_box)]))

    prediction = predictions[0]
    label = ''

    current_character_class = -1
    for i in range(len(prediction)):
        character_class = np.argmax(prediction[i])
        if current_character_class == -1:
            current_character_class = character_class
        elif current_character_class != character_class:
            if current_character_class != BLANK_CLASS:
                label += CHARACTERS[current_character_class]
            current_character_class = character_class

    if current_character_class != BLANK_CLASS:
        label += CHARACTERS[current_character_class]

    if enable_post_processing:
        post_processed_label = [c for c in label]
        while len(post_processed_label) > 0 and post_processed_label[0] == '-':
            post_processed_label.pop(0)

        while len(post_processed_label) > 0 and post_processed_label[len(post_processed_label) - 1] == '-':
            post_processed_label.pop(len(post_processed_label) - 1)
        label = ''.join(post_processed_label).replace('-', '.')

    return label


def pad_image(image):
    ratio = image.shape[0] / image.shape[1]
    expected_ratio = SETTINGS['car_input_image_height'] / SETTINGS['car_input_image_width']

    if ratio == expected_ratio:
        return image

    color = (np.max(image) + np.mean(image)) // 2
    if ratio > expected_ratio:
        width_difference = ratio / expected_ratio * image.shape[1] - image.shape[1]
        horizontal_pad_length = int(math.ceil(width_difference / 2))

        image = np.pad(image,
                       ((0, 0), (horizontal_pad_length, horizontal_pad_length)),
                       'constant',
                       constant_values=((0, 0), (color, color)))
    elif ratio < expected_ratio:
        height_difference = expected_ratio / ratio * image.shape[0] - image.shape[0]
        vertical_pad_length = int(math.ceil(height_difference / 2))

        image = np.pad(image,
                       ((vertical_pad_length, vertical_pad_length), (0, 0)),
                       'constant',
                       constant_values=((color, color), (0, 0)))

    return image


def read_courtesy_amount(model, cheque_image_path, enable_post_processing=True):
    image = cv2.imread(cheque_image_path, cv2.IMREAD_GRAYSCALE)

    try:
        amount_box = extract_amount_box(image)
    except Exception:
        return None, None

    if amount_box is None:
        return None, None

    amount_box = pad_image(amount_box)
    amount_box = 255 - cv2.resize(amount_box,
                                  (SETTINGS['car_input_image_width'], SETTINGS['car_input_image_height']),
                                  interpolation=cv2.INTER_AREA)

    return predict_amount(model, amount_box, enable_post_processing), amount_box


def main():
    if len(sys.argv) < 2:
        return

    loaded_model = load_model('car.h5')
    path = sys.argv[1]

    if os.path.isdir(path):
        if len(sys.argv) != 3:
            return

        paths = load_all_image_paths(path)
        progress_bar = tqdm(total=len(paths))
        labels_counter = dict()

        for image_path in paths:
            amount, amount_box = read_courtesy_amount(loaded_model, image_path, enable_post_processing=False)
            if amount is not None:
                if amount in labels_counter:
                    labels_counter[amount] += 1
                else:
                    labels_counter[amount] = 1

                cv2.imwrite(os.path.join(sys.argv[2], amount) + \
                            '_' + str(labels_counter[amount]) + \
                            file_extension(image_path),
                            amount_box)

            progress_bar.update()
        progress_bar.close()
    else:
        if len(sys.argv) != 2:
            return

        print('\n\n\nModel Prediction')
        print('================')
        amount, img = read_courtesy_amount(loaded_model, path)
        from matplotlib import  pyplot as plt
        plt.imshow(img, plt.cm.gray)
        plt.show()
        print(amount)


if __name__ == '__main__':
    main()
