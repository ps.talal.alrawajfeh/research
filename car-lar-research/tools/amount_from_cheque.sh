#!/bin/bash

if [[ ! -d ./ps-amount-extractor/build ]]
then
  cd ps-amount-extractor || exit
  chmod a+rwx build.sh
  ./build.sh
  cd ..
fi

if [[ -d out ]]
then
  rm -rf ./out
fi
mkdir out

cp ./ps-amount-extractor/build/_ps_amount_extractor.so ./out/_ps_amount_extractor.so
cp ./ps-amount-extractor/build/ps_amount_extractor.py ./out/ps_amount_extractor.py

cp -r ../car ./out/
cp -r ../lar ./out/
cp -r ../concerns ./out/

cp amount_from_cheque.py ./out/amount_from_cheque.py
cp ../car.h5 ./out/car.h5

cd out || exit
python3 amount_from_cheque.py "$@"
