#!/usr/bin/python3.6

import mimetypes
import os

import cv2
import ps_amount_extractor
from matplotlib import pyplot as plt


def get_extensions_for_type(general_type):
    for ext in mimetypes.types_map:
        if mimetypes.types_map[ext].split('/')[0] == general_type:
            yield ext.lower()


# Returns all image extensions, e.g. '.jpg'.
def get_image_extensions():
    return tuple(get_extensions_for_type('image'))


def file_extension(path):
    return os.path.splitext(os.path.basename(path))[1].lower()


def is_image(path):
    return os.path.isfile(path) and file_extension(path) in get_image_extensions()


def load_all_image_paths(base_path):
    image_extensions = get_image_extensions()
    paths = []
    for current_path, sub_dirs, sub_files in os.walk(base_path):
        for f in sub_files:
            path = os.path.join(current_path, f)
            if not os.path.isfile(path) or file_extension(path) not in image_extensions:
                continue
            paths.append(path)
    return paths


def main():
    base_dir = "/home/u764/Development/progressoft/asv-datasets/qnb_cheques/all"
    files = load_all_image_paths(base_dir)
    count = 0

    for f in files:
        image = cv2.imread(f, cv2.IMREAD_GRAYSCALE)

        image_data = []
        for row in image:
            image_data.extend(row)

        native_data_array = ps_amount_extractor.UCharArray(len(image_data))
        for i in range(len(image_data)):
            native_data_array[i] = int(image_data[i])

        result = ps_amount_extractor.detectAmount(native_data_array, image.shape[0], image.shape[1])
        amount_box = image[result.y:result.y + result.height,
                     result.x:result.x + result.width]

        count += 1
        plt.title(str(count))
        plt.imshow(amount_box, cmap=plt.cm.gray)
        plt.show()


if __name__ == '__main__':
    main()
