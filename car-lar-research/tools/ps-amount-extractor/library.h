#ifndef PS_AMOUNT_EXTRACTOR_LIBRARY_H
#define PS_AMOUNT_EXTRACTOR_LIBRARY_H

#define OPENCV_TRAITS_ENABLE_DEPRECATED

#include <opencv2/opencv.hpp>
#include <opencv2/imgproc.hpp>
#include <algorithm>
#include <chrono>
#include <ctime>
#include <iostream>
#include <vector>

#define HORIZONTAL_DIRECTION 0
#define VERTICAL_DIRECTION 1

#define MIN_LINE_LENGTH 20
#define MAX_GAP_LENGTH 2
#define MIN_VERTICAL_SPACING  24
#define MIN_HORIZONTAL_SPACING 170

typedef struct _Rect {
    int x;
    int y;
    int width;
    int height;
} Rect;

Rect detectAmount(unsigned char* imageData, int rows, int cols);

#endif //PS_AMOUNT_EXTRACTOR_LIBRARY_H