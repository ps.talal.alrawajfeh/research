#!/usr/bin/python3.6
import itertools
import math
import os

import numpy as np
from cv2 import cv2
from matplotlib import pyplot as plt

H = 40
W = 960

EXCLUSIVE_LIMIT = 10
MIN_LINE_LENGTH = 50


def view_image(image, title=''):
    plt.suptitle(title)
    plt.imshow(image, cmap=plt.cm.gray)
    plt.show()


def remove_small_lines(image, exclusive_limit):
    result = np.zeros(image.shape, np.uint8)

    for i in range(result.shape[0]):
        row = image[i]
        start = 0
        while start < result.shape[1]:
            end = start + np.argmin(row[start:])

            if end - start >= exclusive_limit:
                result[i, start:end] = 1

            start = end + 1

    return result


def max_pooling(image):
    rows = image.shape[0] // 2
    cols = image.shape[1] // 2
    rem_row = image.shape[0] % 2
    rem_col = image.shape[1] % 2

    result = np.zeros((rows + rem_row, cols + rem_col))

    for y in range(rows):
        for x in range(cols):
            result[y, x] = np.max(image[2 * y:2 * (y + 1), 2 * x:2 * (x + 1)])

    if rem_row > 0:
        for x in range(cols):
            result[-1, x] = np.max(image[-1:, 2 * x:2 * (x + 1)])

    if rem_col > 0:
        for y in range(rows):
            result[y, -1] = np.max(image[2 * y:2 * (y + 1), -1:])

    if rem_row > 0 and rem_col > 0:
        result[-1, -1] = image[-1, -1]

    return result


def extract_lar(image):
    cropped = image[image.shape[0] // 4: image.shape[0] * 3 // 4, :image.shape[1] * 3 // 4]

    binarized = 255 - cv2.adaptiveThreshold(cropped,
                                            255,
                                            cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
                                            cv2.THRESH_BINARY,
                                            11,
                                            15)

    binarized = (binarized / 255.0).astype(np.uint8)

    horizontal_lines = remove_small_lines(binarized, EXCLUSIVE_LIMIT)

    pooled = max_pooling(horizontal_lines)
    pooled = max_pooling(pooled)

    lines = []
    i = 0
    while i < len(pooled):
        row1 = np.array(pooled[i])

        line_start1 = np.argmax(row1)
        if row1[line_start1] == 0:
            i += 1
            continue

        line_end1 = len(row1) - np.argmax(row1[::-1])

        if i < len(pooled) - 1:
            row2 = np.array(pooled[i + 1])
            line_start2 = np.argmax(row2)
            if row2[line_start2] != 0:
                line_end2 = len(row2) - np.argmax(row2[::-1])
                if line_start1 <= line_start2 <= line_end1 or line_start1 <= line_end2 <= line_end1:
                    line_start1 = min(line_start1, line_start2)
                    line_end1 = max(line_end1, line_end2)

        if line_end1 - line_start1 >= MIN_LINE_LENGTH:
            lines.append((i, line_start1, line_end1, line_end1 - line_start1))

        i += 1

    combinations = itertools.combinations(lines, 2)
    max_score = 0
    best_two_lines = None
    for combination in combinations:
        first, second = combination

        # the lines nearer to the center have better score
        score1 = (4 * first[0] / pooled.shape[0] - 4 * first[0] ** 2 / pooled.shape[0] ** 2 +
                  4 * second[0] / pooled.shape[0] - 4 * second[0] ** 2 / pooled.shape[0] ** 2) / 2

        # longer lines are desired most
        score2 = (first[3] / pooled.shape[1] + second[3] / pooled.shape[1]) / 2

        # lines to the left are desired most
        score3 = ((pooled.shape[1] - first[1]) / pooled.shape[1] + (pooled.shape[1] - second[1]) / pooled.shape[1]) / 2

        # lines that have more space between them are better but to an extent
        dist = abs(first[0] - second[0])
        if dist < pooled.shape[0] / 5:
            score4 = 0
        else:
            score4 = 1 - (dist - pooled.shape[0] / 5) / pooled.shape[0]

        score = score1 + score2 + score3 + score4

        # print(f'{combination} --> [{score1}, {score2}, {score3}, {score4}] --> {score}')
        if score > max_score:
            max_score = score
            best_two_lines = combination

    # print(max_score)
    # view_image(pooled)

    final_lines = []
    for line in best_two_lines:
        y = line[0] * 4
        x1 = line[1] * 4
        x2 = line[2] * 4

        y += image.shape[0] // 4
        final_lines.append((y, x1, x2))

    first_part = image[final_lines[0][0] - 30: final_lines[0][0] + 10, final_lines[0][1] - 10: final_lines[0][2] + 10]
    second_part = image[final_lines[1][0] - 30: final_lines[1][0] + 10, final_lines[1][1] - 10: final_lines[1][2] + 10]

    return np.concatenate([first_part, second_part], -1)


def resize_enhance(image):
    image = image.astype(np.float)
    image += (165 - (np.mean(image) + np.median(image)) / 2)
    image[image < 0] = 0
    image[image > 255] = 255
    image = image.astype(np.uint8)

    ratio = image.shape[0] / image.shape[1]
    color = (np.max(image) + np.mean(image)) // 2
    if ratio > H / W:
        width_pad_ratio = ratio / (H / W)
        width_diff = width_pad_ratio * image.shape[1] - image.shape[1]
        width_pad = int(math.ceil(width_diff / 2))

        image = np.pad(image, ((0, 0), (width_pad, width_pad)), 'constant',
                       constant_values=((0, 0), (color, color)))
    elif ratio < H / W:
        height_pad_ratio = (H / W) / ratio
        height_diff = height_pad_ratio * image.shape[0] - image.shape[0]
        height_pad = int(math.ceil(height_diff / 2))

        image = np.pad(image, ((height_pad, height_pad), (0, 0)), 'constant',
                       constant_values=((color, color), (0, 0)))

    image = cv2.resize(image, (W, H), interpolation=cv2.INTER_AREA)

    return image


if __name__ == '__main__':
    from_dir = '/home/u764/Development/progressoft/asv-datasets/dump3'
    to_dir = '/home/u764/Downloads/test'

    if not os.path.isdir(to_dir):
        os.makedirs(to_dir)

    from_files = list(filter(lambda f: f[-3:] == 'jpg', os.listdir(from_dir)))

    # _, _, base_model, _ = build_model()

    file_content = ''
    count = 1
    for f in from_files:
        try:
            img = cv2.imread(from_dir + '/' + f, cv2.IMREAD_GRAYSCALE)
            # img = resize_enhance(extract_lar(img))
            # pred, prob = predict_batch(base_model, [255 - img])

            cv2.imwrite(to_dir + '/' + str(count) + '.jpg', extract_lar(img))
            # file_content += f'{count}\t{pred[0]}\n'
        except:
            pass

        count += 1

    with open(to_dir + '/labels.txt', 'w') as f:
        f.write(file_content)

    # '/home/u764/Development/progressoft/asv-datasets/dump3/0C825D260E064073B214CF01546F8958.jpg'
    # '/home/u764/Development/progressoft/asv-datasets/dump3/0C519F8AA11k54C58AF1B136F0A667F91.jpg'
