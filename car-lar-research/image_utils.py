#!/usr/bin/python3.6

import mimetypes
import os


def get_extensions_for_type(general_type):
    for ext in mimetypes.types_map:
        if mimetypes.types_map[ext].split('/')[0] == general_type:
            yield ext.lower()


# Returns all image extensions, e.g. '.jpg'.
def get_image_extensions():
    return tuple(get_extensions_for_type('image'))


def file_extension(path):
    return os.path.splitext(os.path.basename(path))[1].lower()


def is_image(path):
    return os.path.isfile(path) and file_extension(path) in get_image_extensions()


def load_all_image_paths(base_path):
    image_extensions = get_image_extensions()
    paths = []
    for current_path, sub_dirs, sub_files in os.walk(base_path):
        for f in sub_files:
            path = os.path.join(current_path, f)
            if not os.path.isfile(path) or file_extension(path) not in image_extensions:
                continue
            paths.append(path)
    return paths
