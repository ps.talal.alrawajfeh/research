#!/usr/bin/python3.6

import cv2
from matplotlib import pyplot as plt
import os
import tensorflow as tf
import keras.backend as K
import numpy as np
from keras import Model
from keras.engine.saving import load_model
from tqdm import tqdm
import math
import mimetypes


BASE_DIR = '/home/u764/Development/progressoft/asv-datasets/dump3'
H = 40
W = 240
DATE_MODEL_PATH = '/home/u764/Desktop/date.h5'
CHARACTERS = '0123456789/'
BLANK_CLASS = len(CHARACTERS)
OUTPUT_PATH = '/home/u764/Desktop/extracted'


def get_extensions_for_type(general_type):
    for ext in mimetypes.types_map:
        if mimetypes.types_map[ext].split('/')[0] == general_type:
            yield ext.lower()


# Returns all image extensions, e.g. '.jpg'.
def get_image_extensions():
    return tuple(get_extensions_for_type('image'))


def file_extension(path):
    return os.path.splitext(os.path.basename(path))[1].lower()


def is_image(path):
    return os.path.isfile(path) and file_extension(path) in get_image_extensions()


def view_image(image):
    plt.imshow(image, plt.cm.gray)
    plt.show()


def get_input_shape():
    if K.image_data_format() == 'channels_first':
        input_shape = (1, H, W)
    else:
        input_shape = (H, W, 1)
    return input_shape


def pre_process(image):
    return image.astype(np.float).reshape(get_input_shape()) / 255.0


def predict_batch(model, images, greedy=False):
    model_without_softmax = Model(input=model.input,
                                  output=model.get_layer('logits').output)

    input_tensor = np.array([pre_process(image) for image in images])
    predictions = model_without_softmax.predict(input_tensor)[:, 2:, :]

    time_steps = 30 - 2

    decoded_batch = []
    probabilities = []
    for prediction in predictions:
        if greedy:
            decoded, neg_sum_logits = tf.nn.ctc_greedy_decoder(prediction.reshape((time_steps, 1, len(CHARACTERS) + 1)),
                                                               [time_steps])

            prob = tf.reduce_prod(tf.math.reduce_max(
                tf.nn.softmax(prediction), -1, keepdims=True)).numpy()
            probabilities.append(prob)
        else:
            decoded, log_prob = tf.nn.ctc_beam_search_decoder(prediction.reshape((time_steps, 1, len(CHARACTERS) + 1)),
                                                              [time_steps])
            probabilities.append(np.exp(log_prob[0]))

        decoded_batch.append(tf.sparse.to_dense(decoded[0]).numpy()[0])

    labels = []
    for seq in decoded_batch:
        labels.append(''.join([CHARACTERS[c] for c in seq]))

    return labels, probabilities


def binarize(image):
    return 255 - cv2.threshold(image,
                               np.mean(image) - 1.25 * np.std(image),
                               255,
                               cv2.THRESH_BINARY)[1]


def resize_enhance(image):
    ratio = image.shape[0] / image.shape[1]
    color = (np.max(image) + np.mean(image)) // 2
    if ratio > H / W:
        width_pad_ratio = ratio / (H / W)
        width_diff = width_pad_ratio * image.shape[1] - image.shape[1]
        width_pad = int(math.ceil(width_diff / 2))

        image = np.pad(image,
                       ((0, 0), (width_pad, width_pad)),
                       'constant',
                       constant_values=((0, 0), (color, color)))
    elif ratio < H / W:
        height_pad_ratio = (H / W) / ratio
        height_diff = height_pad_ratio * image.shape[0] - image.shape[0]
        height_pad = int(math.ceil(height_diff / 2))

        image = np.pad(image,
                       ((height_pad, height_pad), (0, 0)),
                       'constant',
                       constant_values=((color, color), (0, 0)))

    image = cv2.resize(image, (W, H), interpolation=cv2.INTER_AREA)

    return image


def find_best_date_rect(image):
    binarized = binarize(image)

    best_y = 0
    best_x = 0
    max_pixels = -1

    if image.shape[0] <= H and image.shape[1] > W:
        y = 0
        for x in range(0, image.shape[1] - W + 1):
            rect = binarized[y:y + H, x:x + W]
            pixels = np.sum(rect)
            if pixels > max_pixels:
                max_pixels = pixels
                best_y = y
                best_x = x
    elif image.shape[0] > H and image.shape[1] <= W:
        x = 0
        for y in range(0, image.shape[0] - H + 1):
            rect = binarized[y:y + H, x:x + W]
            pixels = np.sum(rect)
            if pixels > max_pixels:
                max_pixels = pixels
                best_y = y
                best_x = x
    elif image.shape[0] > H and image.shape[1] > W:
        for y in range(0, image.shape[0] - H + 1):
            for x in range(0, image.shape[1] - W + 1):
                rect = binarized[y:y + H, x:x + W]
                pixels = np.sum(rect)
                if pixels > max_pixels:
                    max_pixels = pixels
                    best_y = y
                    best_x = x
        
    best_rect = image[best_y: best_y + H, best_x:best_x + W]
    return best_rect


def main():
    model = load_model(DATE_MODEL_PATH)

    if not os.path.isdir(OUTPUT_PATH):
        os.makedirs(OUTPUT_PATH)

    image_name_count = dict()

    files = os.listdir(BASE_DIR)
    for f in files:
        if not is_image(os.path.join(BASE_DIR, f)):
            continue

        image = cv2.imread(os.path.join(BASE_DIR, f), cv2.IMREAD_GRAYSCALE)

        x1 = image.shape[1] - image.shape[1] // 3
        x2 = image.shape[1]
        y1 = 0
        y2 = image.shape[0] // 4

        cropped = image[y1 + 3:y2 - 3, x1 + 3:x2 - 3]
        date_rect = find_best_date_rect(cropped)

        labels, probs = predict_batch(model, [255 - resize_enhance(date_rect)])

        image_name = labels[0].replace('/', '-')

        if not image_name in image_name_count:
            image_name_count[image_name] = 0

        image_name_count[image_name] += 1

        cv2.imwrite(os.path.join(
            OUTPUT_PATH, image_name + f'_{image_name_count[image_name]}.jpg'), date_rect)


if __name__ == "__main__":
    main()
