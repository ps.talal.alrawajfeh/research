#!/usr/bin/python3.6

import os
import sys
from concurrent.futures import ThreadPoolExecutor
from multiprocessing import Lock

import numpy as np
import cv2
from tqdm import tqdm


def bytes_to_ints(byte_arr):
    return [int(byte_arr[i]) for i in range(len(byte_arr))]


def bytes_to_unsigned_int(byte_arr):
    base = 1
    i = len(byte_arr) - 1
    result = 0
    while i >= 0:
        result += byte_arr[i] * base
        base *= 256
        i -= 1
    return result


def extract_character(images_bin,
                      labels_bin,
                      images_ptr,
                      labels_ptr,
                      file_names,
                      file_names_lock,
                      image_width,
                      image_height,
                      image_size,
                      output_path,
                      progress,
                      progress_lock):
    label = bytes_to_ints(labels_bin[labels_ptr: labels_ptr + 1])[0]

    image = bytes_to_ints(images_bin[images_ptr: images_ptr + image_size])
    image = np.array(image, np.uint8) \
        .reshape(image_height, image_width) \
        .swapaxes(0, 1)

    with file_names_lock:
        if label in file_names:
            file_names[label] += 1
        else:
            file_names[label] = 0

    cv2.imwrite(os.path.join(output_path, f'{label}_{file_names[label]}.jpg'), image)

    with progress_lock:
        progress.update()


def main():
    if len(sys.argv) < 4:
        exit()

    labels_file = sys.argv[1]
    images_file = sys.argv[2]
    output_path = sys.argv[3]

    with open(labels_file, 'rb') as file:
        labels_bin = file.read()

    with open(images_file, 'rb') as file:
        images_bin = file.read()

    labels_ptr = 0
    images_ptr = 0

    # skip magic number
    labels_ptr += 4
    images_ptr += 4

    items_bin = bytes_to_ints(labels_bin[labels_ptr: labels_ptr + 4])
    items = bytes_to_unsigned_int(items_bin)

    labels_ptr += 4
    images_ptr += 4

    image_height_bin = bytes_to_ints(images_bin[images_ptr:images_ptr + 4])
    image_height = bytes_to_unsigned_int(image_height_bin)
    images_ptr += 4

    image_width_bin = bytes_to_ints(images_bin[images_ptr:images_ptr + 4])
    image_width = bytes_to_unsigned_int(image_width_bin)
    images_ptr += 4

    file_names = dict()
    file_names_lock = Lock()

    print('extracting characters...\n')

    progress = tqdm(total=items)
    progress_lock = Lock()

    image_size = image_height * image_width

    with ThreadPoolExecutor(max_workers=4) as executor:
        for i in range(items):
            executor.submit(extract_character(images_bin,
                                              labels_bin,
                                              images_ptr,
                                              labels_ptr,
                                              file_names,
                                              file_names_lock,
                                              image_width,
                                              image_height,
                                              image_size,
                                              output_path,
                                              progress,
                                              progress_lock))

            labels_ptr += 1
            images_ptr += image_size

    progress.close()


if __name__ == '__main__':
    main()
