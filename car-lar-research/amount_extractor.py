#!/usr/bin/python3.6
import os
import random

import cv2
import numpy as np
from matplotlib import pyplot as plt

from image_utils import is_image

MIN_LINE_LENGTH = 20
MIN_VERTICAL_SPACING = 24
MIN_HORIZONTAL_SPACING = 170

MAX_VERTICAL_SPACING = 50
MAX_HORIZONTAL_SPACING = 280

MAX_GAP_LENGTH = 2


def view_image(image, title=''):
    plt.suptitle(title)
    plt.imshow(image, cmap=plt.cm.gray)
    plt.show()


def view_images(images, title=''):
    f, axs = plt.subplots(len(images))
    axs[0].set_title(title)
    for i in range(len(images)):
        axs[i].imshow(images[i], cmap=plt.cm.gray)
    plt.show()


def horizontal_edge_kernel(size):
    k = []
    for i in range(size // 2):
        k.append([0] * size)
    k.append([1 / size] * size)
    for i in range(size // 2):
        k.append([0] * size)
    return np.array(k, np.float32)


def vertical_edge_kernel(size):
    return horizontal_edge_kernel(size).T


def remove_connected_pixels_per_row(image, threshold):
    for row in image:
        i = 0
        while i < len(row):
            if row[i] > 0:
                j = i + 1
                while j < len(row):
                    if row[j] == 0:
                        break
                    j += 1
                if j - i < threshold:
                    row[i:j] = 0
                i = j
            i += 1


def max_pooling(image):
    new_image = []
    y = 0
    while y < image.shape[0]:
        x = 0
        row = []
        while x < image.shape[1]:
            row.append(np.max(image[y: y + 2,
                              x: x + 2]))
            x += 2
        new_image.append(row)
        y += 2
    return np.array(new_image, np.uint8)


def first_non_zero_pixel(image):
    for y in range(len(image)):
        for x in range(len(image[0])):
            if image[y, x] > 0:
                return x, y
    return None


def safe_get_pixel(image, x, y):
    if x < 0 or x >= image.shape[1] or \
            y < 0 or y >= image.shape[0]:
        return 0
    else:
        return image[y, x]


def detect_horizontal_lines(image, max_gap_length, min_line_length):
    lines = []
    for y in range(len(image)):
        row = image[y]
        start = 0
        while start < len(row):
            if row[start] > 0:
                gap_counter = 0
                gap_start = -1
                end = start + 1
                while end < len(row):
                    if row[end] == 0:
                        gap_start = end if gap_start == -1 else gap_start
                        gap_counter += 1
                    else:
                        gap_start = -1
                        gap_counter = 0
                    if gap_counter == max_gap_length + 1:
                        break
                    end += 1

                if gap_start == -1:
                    to = end
                else:
                    to = gap_start

                line_length = to - start
                if line_length >= min_line_length:
                    lines.append((start, y, to, y))

                start = end
            start += 1
    return lines


def detect_vertical_lines(image, max_gap_length, min_line_length):
    temp = image.T
    lines = detect_horizontal_lines(temp, max_gap_length, min_line_length)
    new_lines = []
    for line in lines:
        y1, x1, y2, x2 = line
        new_lines.append((x1, y1, x2, y2))
    return new_lines


def refine_rectangle(image, rectangle, threshold_prob=0):
    x1, y1, x2, y2 = rectangle

    max_prob = threshold_prob
    max_prob_i = x1
    for i in range(x1, x2 - MIN_HORIZONTAL_SPACING):
        prob = np.mean(image[y1:y2, i:i + MAX_GAP_LENGTH]) / 255
        if prob > max_prob:
            max_prob = prob
            max_prob_i = i
    x1 = max_prob_i

    max_prob = threshold_prob
    max_prob_i = y1
    for i in range(y1, y2 - MIN_VERTICAL_SPACING):
        prob = np.mean(image[i:i + MAX_GAP_LENGTH, x1:x2]) / 255
        if prob > max_prob:
            max_prob = prob
            max_prob_i = i
    y1 = max_prob_i

    max_prob = threshold_prob
    max_prob_i = x2
    i = x2
    while i > x1 + MIN_HORIZONTAL_SPACING:
        prob = np.mean(image[y1:y2, i:i + MAX_GAP_LENGTH]) / 255
        if prob > max_prob:
            max_prob = prob
            max_prob_i = i
        i -= 1
    x2 = max_prob_i

    max_prob = threshold_prob
    max_prob_i = y2
    i = y2
    while i > y1 + MIN_VERTICAL_SPACING:
        prob = np.mean(image[i:i + MAX_GAP_LENGTH, x1:x2]) / 255
        if prob > max_prob:
            max_prob = prob
            max_prob_i = i
        i -= 1
    y2 = max_prob_i

    return x1, y1, x2, y2


def locate_exact_amount_area_x(vertical_edges):
    height = vertical_edges.shape[0]
    width = vertical_edges.shape[1]

    i = width - 1
    min_pixels = width * height
    min_pixels_i = i
    while i > width // 2:
        pixels = vertical_edges[0:height, 0:i].sum() / 255
        if min_pixels > pixels:
            min_pixels = pixels
            min_pixels_i = i
        i -= 1
    x2 = min_pixels_i

    min_pixels = width * height
    min_pixels_i = 0
    for i in range(0, x2 // 2):
        pixels = vertical_edges[0:height, i:width].sum() / 255
        if min_pixels > pixels:
            min_pixels = pixels
            min_pixels_i = i
    x1 = min_pixels_i

    return x1, x2


def locate_exact_amount_area_y(horizontal_edges):
    height = horizontal_edges.shape[0]
    width = horizontal_edges.shape[1]

    min_pixels = width * height
    min_pixels_i = 0
    for i in range(0, height // 2):
        pixels = horizontal_edges[i:height, 0:width].sum() / 255
        if min_pixels > pixels:
            min_pixels = pixels
            min_pixels_i = i
    y1 = min_pixels_i

    i = height - 1
    min_pixels = width * height
    min_pixels_i = i
    while i > (y1 + height) // 2:
        pixels = horizontal_edges[0:i, 0:width].sum() / 255
        if min_pixels > pixels:
            min_pixels = pixels
            min_pixels_i = i
        i -= 1
    y2 = min_pixels_i

    return y1, y2


def main():
    base_path = '/home/u764/Downloads/datasets/dump3'
    count = 0

    files = os.listdir(base_path)
    random.shuffle(files)

    for f in files:
        path = os.path.join(base_path, f)
        if not is_image(path):
            continue

        original_image = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
        cropped_image = original_image[original_image.shape[0] // 4: 3 * original_image.shape[0] // 4,
                        original_image.shape[1] // 2: original_image.shape[1]]

        binarized_image = 255 - cv2.adaptiveThreshold(cropped_image,
                                                      255,
                                                      cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
                                                      cv2.THRESH_BINARY,
                                                      11,
                                                      15)

        # horizontal lines
        kernel_size = 3
        horizontal_edges = np.copy(binarized_image)
        # horizontal_edges = cv2.filter2D(binarized_image,
        #                                 -1,
        #                                 horizontal_edge_kernel(kernel_size))
        # horizontal_edges[horizontal_edges < 255] = 0
        # horizontal_edges = horizontal_edges.astype(np.uint8)
        remove_connected_pixels_per_row(horizontal_edges, MIN_LINE_LENGTH)
        horizontal_edges_pooled = max_pooling(horizontal_edges)
        horizontal_edges_pooled = max_pooling(horizontal_edges_pooled)

        # vertical lines
        vertical_edges = np.copy(binarized_image)
        # vertical_edges = cv2.filter2D(binarized_image,
        #                               -1,
        #                               vertical_edge_kernel(kernel_size))
        # vertical_edges[vertical_edges < 255] = 0
        vertical_edges = vertical_edges.astype(np.uint8).T
        remove_connected_pixels_per_row(vertical_edges, MIN_LINE_LENGTH)
        vertical_edges = vertical_edges.T
        vertical_edges_pooled = max_pooling(vertical_edges)
        vertical_edges_pooled = max_pooling(vertical_edges_pooled)
        view_image(horizontal_edges + vertical_edges)
        max_pooling_count = 2
        max_pooling_reduction_factor = 2 ** max_pooling_count

        image = horizontal_edges_pooled + vertical_edges_pooled
        contours = detect_contours_optimized(image)

        expected_amount_box = None
        max_lines = 0

        for rect in contours:
            x1, y1, x2, y2 = rect
            w = x2 - x1
            h = y2 - y1

            if h < MIN_VERTICAL_SPACING // max_pooling_reduction_factor:
                continue
            if w < MIN_HORIZONTAL_SPACING // max_pooling_reduction_factor:
                continue

            horizontal_lines = len(detect_horizontal_lines(horizontal_edges_pooled[y1:y2, x1:x2],
                                                           MAX_GAP_LENGTH,
                                                           MIN_HORIZONTAL_SPACING // max_pooling_reduction_factor))
            vertical_lines = len(detect_vertical_lines(vertical_edges_pooled[y1:y2, x1:x2],
                                                       MAX_GAP_LENGTH,
                                                       MIN_VERTICAL_SPACING // max_pooling_reduction_factor))

            lines = horizontal_lines + vertical_lines
            if max_lines < lines:
                max_lines = lines
                expected_amount_box = rect

        if expected_amount_box is not None:
            x1, y1, x2, y2 = expected_amount_box

            x1 *= max_pooling_reduction_factor
            y1 *= max_pooling_reduction_factor
            x2 *= max_pooling_reduction_factor
            y2 *= max_pooling_reduction_factor

            x1 -= max_pooling_count
            y1 -= max_pooling_count
            x2 += max_pooling_count
            y2 += max_pooling_count

            x1, y1, x2, y2 = refine_rectangle(horizontal_edges + vertical_edges, (x1, y1, x2, y2))

            remove_connected_pixels_per_row(horizontal_edges, MIN_HORIZONTAL_SPACING // 2)

            vertical_edges = vertical_edges.astype(np.uint8).T
            remove_connected_pixels_per_row(vertical_edges, 3 * MIN_VERTICAL_SPACING // 4)
            vertical_edges = vertical_edges.T

            ax1, ax2, = locate_exact_amount_area_x(
                vertical_edges[y1 - MAX_GAP_LENGTH:y2 + MAX_GAP_LENGTH + max_pooling_count,
                x1 - MAX_GAP_LENGTH:x2 + MAX_GAP_LENGTH + max_pooling_count])

            ay1, ay2, = locate_exact_amount_area_y(
                horizontal_edges[y1 - MAX_GAP_LENGTH:y2 + MAX_GAP_LENGTH + max_pooling_count,
                x1 - MAX_GAP_LENGTH:x2 + MAX_GAP_LENGTH + max_pooling_count])

            amount_box = cropped_image[y1 - MAX_GAP_LENGTH:y2 + MAX_GAP_LENGTH + max_pooling_count,
                         x1 - MAX_GAP_LENGTH:x2 + MAX_GAP_LENGTH + max_pooling_count]

            view_images([cropped_image, amount_box[ay1:ay2, ax1:ax2]])

            # view_images([cropped_image, binarized_image, horizontal_edges + vertical_edges, amount_box,
            #              horizontal_edges_pooled + vertical_edges_pooled],
            #             f + '/' + str(count))
        else:
            pass
            # view_images([cropped_image, binarized_image, horizontal_edges + vertical_edges,
            #              horizontal_edges_pooled + vertical_edges_pooled],
            #             f + '/' + str(count))
        count += 1


def non_zero_pixels(image):
    pixels = []
    for y in range(image.shape[0]):
        for x in range(image.shape[1]):
            if image[y, x] != 0:
                pixels.append((x, y))
    return pixels


def within_bounds(image, x, y):
    return 0 <= y < image.shape[0] and 0 <= x < image.shape[1]


def detect_contours_optimized(image):
    contours = []

    pixels = non_zero_pixels(image)
    connected_visited = np.zeros(image.shape)
    disconnected_visited = np.zeros(image.shape)

    pixels_ptr = 0
    while pixels_ptr < len(pixels):
        start_x, start_y = pixels[pixels_ptr]

        if connected_visited[start_y, start_x] != 0:
            pixels_ptr += 1
            continue

        contour_x1 = image.shape[1]
        contour_x2 = 0
        contour_y1 = image.shape[0]
        contour_y2 = 0

        connected_queue = set()
        connected_queue.add((start_x, start_y))

        disconnected_queue = set()

        while len(connected_queue) > 0:
            parent_x, parent_y = connected_queue.pop()
            connected_visited[parent_y, parent_x] = 1

            if parent_x < contour_x1:
                contour_x1 = parent_x
            if parent_x > contour_x2:
                contour_x2 = parent_x
            if parent_y < contour_y1:
                contour_y1 = parent_y
            if parent_y > contour_y2:
                contour_y2 = parent_y

            for x_delta in [0, 1]:
                x = parent_x + x_delta
                for y_delta in [0, 1]:
                    y = parent_y + y_delta

                    if (x, y) in disconnected_queue:
                        disconnected_queue.remove((x, y))
                        connected_queue.add((x, y))

                    if within_bounds(image, x, y) and (connected_visited[y, x] != 0 or
                                                       disconnected_visited[y, x] != 0 or
                                                       (x, y) in connected_queue):
                        continue

                    if safe_get_pixel(image, x, y) > 0:
                        connected_queue.add((x, y))

            if parent_x < image.shape[1] - 1 and parent_y < image.shape[0] - 1 and \
                    image[parent_y + 1, parent_x] == 0 and image[parent_y + 1, parent_x + 1] == 0:
                for x_delta in range(-MAX_GAP_LENGTH, MAX_GAP_LENGTH + 1):
                    x = parent_x + x_delta
                    for y_delta in range(-MAX_GAP_LENGTH, MAX_GAP_LENGTH + 1):
                        y = parent_y + y_delta

                        if within_bounds(image, x, y) and (connected_visited[y, x] != 0 or
                                                           disconnected_visited[y, x] != 0 or
                                                           (x, y) in connected_queue or
                                                           (x, y) in disconnected_queue):
                            continue

                        if safe_get_pixel(image, x, y) != 0:
                            disconnected_queue.add((x, y))

        any_disconnected = False
        if len(disconnected_queue) > 0:
            any_disconnected = True

        while len(disconnected_queue) > 0:
            parent_x, parent_y = disconnected_queue.pop()
            disconnected_visited[parent_y, parent_x] = 1

            if parent_x < contour_x1:
                contour_x1 = parent_x
            if parent_x > contour_x2:
                contour_x2 = parent_x
            if parent_y < contour_y1:
                contour_y1 = parent_y
            if parent_y > contour_y2:
                contour_y2 = parent_y

            for x_delta in [0, 1]:
                x = parent_x + x_delta
                for y_delta in [0, 1]:
                    y = parent_y + y_delta

                    if within_bounds(image, x, y) and (connected_visited[y, x] != 0 or
                                                       disconnected_visited[y, x] != 0 or
                                                       (x, y) in connected_queue or
                                                       (x, y) in disconnected_queue):
                        continue

                    if safe_get_pixel(image, x, y) != 0:
                        disconnected_queue.add((x, y))

        contours.append((contour_x1, contour_y1, contour_x2 + 1, contour_y2 + 1))

        if any_disconnected:
            del disconnected_visited
            disconnected_visited = np.zeros(image.shape)

        pixels_ptr += 1

    return contours


if __name__ == '__main__':
    main()
