#!/usr/bin/python3.6

import os
import sys
import cv2
import numpy as np
from tqdm import tqdm
import math
import random


def two_means_clustering(inputs, centroids=None):
    random.shuffle(inputs)

    max_input = int(np.max(inputs))
    min_input = int(np.min(inputs))

    if centroids is None:
        centroids = [min_input, max_input]

    centroid1 = centroids[0]
    centroid2 = centroids[1]

    alpha1 = 0.9
    beta1 = 1 - alpha1

    alpha2 = 0.2
    beta2 = 1 - alpha2

    for i in range(len(inputs)):
        x = inputs[i]
        dist1 = abs(centroid1 - x)
        dist2 = abs(centroid2 - x)

        if dist2 <= dist1:
            centroid2 = alpha2 * centroid2 + beta2 * x
        else:
            centroid1 = alpha1 * centroid1 + beta1 * x

    return [centroid1, centroid2]


def adaptive_threshold(image):
    pixels = np.copy(image).reshape((image.shape[0] * image.shape[1]))
    centroids = two_means_clustering(pixels)
    return sum(centroids) / 2


def binarize(image, thresh=None):
    if thresh is None:
        thresh = adaptive_threshold(image)
    image[image < thresh] = 0
    return image
