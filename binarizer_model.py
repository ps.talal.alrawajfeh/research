#!/usr/bin/python3.6

import cv2
import numpy as np
import random
import numba
import math
from numba import jit
import keras.backend as K
import keras
from keras.utils import plot_model
from matplotlib import pyplot as plt


IMAGE_SHAPE = (128, 128)
THRESHOLD_RANGE = (60, 190)
LINE_MIN_LENGTH = int(128 * 3 / 4)
TRAIN_SAMPLES = 10000
VALIDATION_SAMPLES = 2000
TEST_SAMPLES = 2000


def view_image(image):
    plt.imshow(image, plt.cm.gray)
    plt.show()


@jit(nopython=True)
def random_point():
    return (random.randint(0, IMAGE_SHAPE[1]), random.randint(0, IMAGE_SHAPE[0]))


@jit(nopython=True)
def distance(point1, point2):
    return math.sqrt((point1[0] - point2[0]) ** 2 + (point1[1] - point2[1]) ** 2)


@jit(nopython=True)
def two_random_points(min_distance=LINE_MIN_LENGTH):
    while True:
        point1 = random_point()
        point2 = random_point()

        if distance(point1, point2) >= LINE_MIN_LENGTH:
            break

    return (point1, point2)


def generate_image_binarized_pair():
    threshold = random.randint(THRESHOLD_RANGE[0],
                               THRESHOLD_RANGE[1])

    original = np.random.uniform(low=threshold + 10,
                                 high=255,
                                 size=IMAGE_SHAPE)

    point1, point2 = two_random_points()

    cv2.line(original,
             point1,
             point2, random.randint(0, threshold - 10))

    binarized = np.array(original)
    binarized[binarized < threshold] = 0
    binarized[binarized >= threshold] = 255

    return original, binarized


def generate_data_set(count):
    dataset_x = []
    dataset_y = []

    for _ in range(count):
        x, y = generate_image_binarized_pair()
        dataset_x.append(x)
        dataset_y.append(y)

    return (dataset_x, dataset_y)


def convert_to_pre_processed_tensors(data_set):
    x, y = data_set
    x = [pre_process(image) for image in x]
    y = [pre_process(image) for image in y]

    return (np.array(x), np.array(y))


def generate_train_validation_test_data_set(train_samples=TRAIN_SAMPLES,
                                            validation_samples=VALIDATION_SAMPLES,
                                            test_samples=TEST_SAMPLES):
    return (convert_to_pre_processed_tensors(generate_data_set(train_samples)),
            convert_to_pre_processed_tensors(
                generate_data_set(validation_samples)),
            convert_to_pre_processed_tensors(generate_data_set(test_samples)))


def get_input_shape():
    if K.image_data_format() == 'channels_first':
        input_shape = (1, IMAGE_SHAPE[0], IMAGE_SHAPE[1])
    else:
        input_shape = (IMAGE_SHAPE[0], IMAGE_SHAPE[1], 1)
    return input_shape


def pre_process(image):
    return (255 - image).astype(np.float).reshape(get_input_shape()) / 255.0


def binarizer():
    input_image = keras.layers.Input(shape=get_input_shape(),
                                     name='input_image')

    # conv blocks 1 - 4
    encoder_conv_input = input_image
    for i in range(4):
        base = 16 // (i + 1)
        encoder_conv = keras.layers.Conv2D(base,
                                           (base + 1, base + 1),
                                           padding='same',
                                           kernel_initializer='he_normal',
                                           name=f'encoder_conv{i}')(encoder_conv_input)
        encoder_conv_relu = keras.layers.ReLU(
            name=f'encoder_conv{i}_relu')(encoder_conv)
        encoder_conv_pool = keras.layers.MaxPooling2D(pool_size=(2, 2),
                                                      name=f'encoder_conv{i}_pool')(encoder_conv_relu)
        encoder_conv_input = encoder_conv_pool

    # conv block 5
    encoder_conv5 = keras.layers.Conv2D(1,
                                        (1, 1),
                                        padding='same',
                                        kernel_initializer='he_normal',
                                        name='encoder_conv5')(encoder_conv_pool)
    encoder_conv5_relu = keras.layers.ReLU(
        name='encoder_conv5_relu')(encoder_conv5)

    # upsample block
    up_sampled = keras.layers.UpSampling2D(size=(16, 16),
                                           interpolation='nearest',
                                           name='up_sampled')(encoder_conv5_relu)

    # difference between output and input
    difference = keras.layers.Lambda(lambda x: x[0] - x[1],
                                     name='difference')([input_image, up_sampled])

    # final output layer
    output_image = keras.layers.Conv2D(1,
                                       (1, 1),
                                       activation='sigmoid',
                                       padding='same',
                                       kernel_initializer='he_normal',
                                       name='output_conv')(difference)

    return keras.Model(inputs=[input_image], outputs=output_image)


def post_process(image):
    return 255 - (image.reshape(IMAGE_SHAPE) * 255.0).astype(np.uint8)


def train():
    model = binarizer()
    # model = keras.engine.saving.load_model('binarizer.h5', compile=False)

    model.summary()
    # plot_model(model,
    #            to_file='binarizer',
    #            show_shapes=True)

    model.compile(loss=keras.losses.binary_crossentropy,
                  optimizer=keras.optimizers.SGD(0.1, 0.99, True))

    (train_x, train_y), (validation_x,
                         validation_y), _ = generate_train_validation_test_data_set()

    model.fit(x=train_x,
              y=train_y,
              batch_size=64,
              epochs=30,
              validation_data=(validation_x, validation_y))

    model.save('binarizer.h5')


def predict():
    model = keras.engine.saving.load_model('binarizer.h5', compile=False)

    image, binarized = generate_image_binarized_pair()

    view_image(image)

    view_image(post_process(model.predict(np.array([pre_process(image)]))[0]))


def main():
    train()
    # predict()


if __name__ == "__main__":
    main()
