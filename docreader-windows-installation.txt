install python3.9+
install visual studio
install cmake (make sure to check on add variables to path)
install tesseract ocr: https://digi.bib.uni-mannheim.de/tesseract/
set environment variables (PATH: C:\Program Files\Tesseract-OCR, TESSDATA_PREFIX: C:\Program Files\Tesseract-OCR\tessdata)
install tesserocr from wheel: https://github.com/simonflueckiger/tesserocr-windows_build/releases
install requirements
add the files in resources/*.traineddata to the tessdata directory
