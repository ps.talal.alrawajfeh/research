package com.progressoft;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Size;

import java.util.ArrayList;
import java.util.List;

import static org.opencv.core.Core.*;
import static org.opencv.core.Core.REDUCE_MIN;
import static org.opencv.core.CvType.CV_64F;
import static org.opencv.core.CvType.CV_8UC1;
import static org.opencv.imgproc.Imgproc.*;
import static org.opencv.imgproc.Imgproc.MORPH_RECT;

public class Utils {

    public static Mat edgeDetection(Mat image) {
        Mat sobelX = new Mat();
        Sobel(image, sobelX, CV_64F, 1, 0, 7);

        Mat sobelY = new Mat();
        Sobel(image, sobelY, CV_64F, 0, 1, 7);

        Mat sobelXSquared = new Mat();
        multiply(sobelX, sobelX, sobelXSquared);

        Mat sobelYSquared = new Mat();
        multiply(sobelY, sobelY, sobelYSquared);

        Mat squaredGradients = new Mat();
        add(sobelXSquared, sobelYSquared, squaredGradients);

        Mat gradients = new Mat();
        sqrt(squaredGradients, gradients);

        List<Mat> channels = new ArrayList<>();
        Core.split(gradients, channels);

        Mat maxGradients = channels.get(0);
        for (Mat channel : channels) {
            max(maxGradients, channel, maxGradients);
        }

        Mat maxGradientMat = new Mat();
        Mat minGradientMat = new Mat();
        reduce(maxGradients, maxGradientMat, 0, REDUCE_MAX);
        reduce(maxGradients, minGradientMat, 0, REDUCE_MIN);

        double maxGradient = maxGradientMat.get(0, 0)[0];
        double minGradient = minGradientMat.get(0, 0)[0];

        for (int y = 0; y < maxGradients.rows(); y++) {
            for (int x = 0; x < maxGradients.cols(); x++) {
                double newValue = (maxGradients.get(y, x)[0] - minGradient) / (maxGradient - minGradient) * 255.0;
                if (newValue > 255) {
                    newValue = 255;
                }
                if (newValue < 0) {
                    newValue = 0;
                }
                maxGradients.put(y, x, newValue);
            }
        }

        Mat gradientsMat = new Mat();
        maxGradients.convertTo(gradientsMat, CV_8UC1);

        return gradientsMat;
    }


    public static Mat doubleThreshold(Mat image, int lowThreshold, int highThreshold) {
        Mat threshold = new Mat();
        image.copyTo(threshold);

        for (int y = 0; y < threshold.rows(); y++) {
            for (int x = 0; x < threshold.cols(); x++) {
                double value = threshold.get(y, x)[0];
                if (value >= highThreshold) {
                    value = 0;
                }
                if (value >= lowThreshold) {
                    value = 255;
                }
                if (value < lowThreshold) {
                    value = 0;
                }
                threshold.put(y, x, value);
            }
        }

        return threshold;
    }

    public static Mat performEdgeHysteresis(Mat strongEdges, Mat weakEdges) {
        Mat mask = new Mat();
        bitwise_or(weakEdges, strongEdges, mask);
        Mat result = new Mat();
        strongEdges.copyTo(result);

        int area = strongEdges.rows() * strongEdges.cols();

        while (true) {
            Mat newStrongEdges = new Mat();
            dilate(result, newStrongEdges, getStructuringElement(MORPH_RECT, new Size(3, 3)));
            bitwise_and(newStrongEdges, mask, newStrongEdges);
            Mat comparisonResult = new Mat();
            compare(newStrongEdges, result, comparisonResult, CMP_EQ);
            if (countNonZero(comparisonResult) == area) {
                break;
            }
            result = newStrongEdges;
        }

        return result;
    }
}
