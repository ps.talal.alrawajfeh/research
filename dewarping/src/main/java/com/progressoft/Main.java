package com.progressoft;


import org.opencv.core.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import static org.opencv.imgcodecs.Imgcodecs.imread;
import static org.opencv.imgcodecs.Imgcodecs.imwrite;
import static org.opencv.imgproc.Imgproc.*;

public class Main {
    static {
        nu.pattern.OpenCV.loadShared();
        System.loadLibrary(org.opencv.core.Core.NATIVE_LIBRARY_NAME);
    }

    private static double DEFAULT_MIN_WIDTH_TO_HEIGHT_RATIO = 1.5;
    private static double DEFAULT_MAX_WIDTH_TO_HEIGHT_RATIO = 2.5;
    private static double DEFAULT_MIN_AREA_PERCENTAGE = 0.1;
    private static double DEFAULT_ARC_LENGTH_PERCENTAGE = 0.001;
    private static double DEFAULT_EXTRACTION_MAX_SIDE_LENGTH = 750.0;

    public static MatOfPoint findBestContour(Mat binaryImage,
                                             double minAreaPercentage,
                                             double minWidthToHeightRatio,
                                             double maxWidthToHeightRatio) {
        List<MatOfPoint> contours = new ArrayList<>();
        Mat hierarchy = new Mat();
        findContours(binaryImage, contours, hierarchy, RETR_CCOMP, CHAIN_APPROX_NONE);

        double maxArea = -1;
        int bestContourIndex = -1;

        if (contours.size() == 0) {
            return null;
        }

        double minArea = minAreaPercentage * binaryImage.width() * binaryImage.height();

        for (int i = 0; i < contours.size(); i++) {
            MatOfPoint contour = contours.get(i);

            double areaOfContour = contourArea(contour);
            RotatedRect rotatedRect = minAreaRect(new MatOfPoint2f(contour.toArray()));
            double rotatedRectWidth = rotatedRect.size.width;
            double rotatedRectHeight = rotatedRect.size.height;

            if (rotatedRectHeight > rotatedRectWidth) {
                var temp = rotatedRectWidth;
                rotatedRectWidth = rotatedRectHeight;
                rotatedRectHeight = temp;
            }

            double widthToHeightRatio = rotatedRectWidth / rotatedRectHeight;
            double area = rotatedRectWidth * rotatedRectHeight;
            if (widthToHeightRatio >= minWidthToHeightRatio &&
                    widthToHeightRatio <= maxWidthToHeightRatio &&
                    area >= minArea &&
                    areaOfContour >= minArea &&
                    area > maxArea) {
                maxArea = area;
                bestContourIndex = i;
            }
        }

        if (bestContourIndex == -1) {
            return null;
        }

        return contours.get(bestContourIndex);
    }

    public static double triangleArea(Point point1, Point point2, Point point3) {
        return Math.abs(0.5 * (point1.x * (point2.y - point3.y) +
                point2.x * (point3.y - point1.y) +
                point3.x * (point1.y - point2.y)));
    }

    public static double computeAngle(Point origin, Point point) {
        double x = point.x - origin.x;
        double y = origin.y - point.y;
        double theta;
        if (Math.abs(x) > 1e-7) {
            theta = Math.abs(Math.atan(y / x)) / Math.PI * 180;
        } else {
            theta = 90.0;
        }

        if (x < 0 && y > 0) {
            return 180 - theta;
        } else if (x < 0 && y < 0) {
            return 180 + theta;
        } else if (x > 0 && y < 0) {
            return 360 - theta;
        }
        return theta;
    }

    public static double polygonArea(Point point1, Point point2, Point point3, Point point4) {
        double originX = (point1.x + point2.x + point3.x + point4.x) / 4.0;
        double originY = (point1.y + point2.y + point3.y + point4.y) / 4.0;

        Point origin = new Point(originX, originY);
        List<Point> points = Arrays.asList(point1, point2, point3, point4);
        points.sort(Comparator.comparingDouble(p -> computeAngle(origin, p)));

        double area1 = triangleArea(points.get(0), points.get(1), points.get(2));
        double area2 = triangleArea(points.get(0), points.get(2), points.get(3));
        return area1 + area2;
    }

    public static List<Point> approximateContourPolygon(MatOfPoint contour,
                                                        double arcLengthPercentage) {
        MatOfPoint2f curve = new MatOfPoint2f(contour.toArray());
        double epsilon = arcLengthPercentage * arcLength(curve, true);
        MatOfPoint2f approximatePolygon = new MatOfPoint2f();
        approxPolyDP(curve, approximatePolygon, epsilon, true);

        List<Point> convexHullPoints = new ArrayList<>();
        MatOfInt hull = new MatOfInt();
        Point[] curvePoints = curve.toArray();
        convexHull(new MatOfPoint(curvePoints), hull);
        List<Integer> hullPointsIndices = hull.toList();
        for (Integer index : hullPointsIndices) {
            convexHullPoints.add(curvePoints[index]);
        }

        if (convexHullPoints.size() < 4) {
            return null;
        }

        double centerX = 0.0;
        double centerY = 0.0;
        for (Point point : convexHullPoints) {
            centerX += point.x;
            centerY += point.y;
        }
        centerX /= convexHullPoints.size();
        centerY /= convexHullPoints.size();

        Point point1 = new Point(0, 0);
        double maxDistance = 0.0;

        for (Point point : convexHullPoints) {
            double distance = Math.sqrt(Math.pow(centerX - point.x, 2) + Math.pow(centerY - point.y, 2));
            if (distance > maxDistance) {
                point1 = point;
                maxDistance = distance;
            }
        }

        Point point2 = new Point(0, 0);
        maxDistance = 0.0;
        for (Point point : convexHullPoints) {
            double distance = Math.sqrt(Math.pow(point1.x - point.x, 2) + Math.pow(point1.y - point.y, 2));
            if (distance > maxDistance) {
                point2 = point;
                maxDistance = distance;
            }
        }

        Point point3 = new Point(0, 0);
        double denominator = Math.sqrt(Math.pow(point1.x - point2.x, 2) + Math.pow(point1.y - point2.y, 2));
        maxDistance = 0.0;
        for (Point point : convexHullPoints) {
            double distanceBetweenPointAndLine = 0.0;
            if (denominator < 1e-7) {
                distanceBetweenPointAndLine = Math.sqrt(Math.pow(point1.x - point.x, 2) + Math.pow(point1.y - point.y, 2));
            } else {
                distanceBetweenPointAndLine = Math.abs((point2.x - point1.x) * (point1.y - point.y) - (point1.x - point.x) * (point2.y - point1.y)) / denominator;
            }
            if (distanceBetweenPointAndLine > maxDistance) {
                point3 = point;
                maxDistance = distanceBetweenPointAndLine;
            }
        }

        Point point4 = new Point(0, 0);
        double maxArea = 0.0;
        for (Point point : convexHullPoints) {
            double area = polygonArea(point1, point2, point3, point);
            if (area > maxArea) {
                point4 = point;
                maxArea = area;
            }
        }

        return Arrays.asList(point1, point2, point3, point4);
    }

    private static List<Point> getFourPolygonPoints(Mat image) {
        Mat closingMorphology = new Mat();
        Mat kernel = getStructuringElement(MORPH_RECT, new Size(7, 7));
        morphologyEx(image, closingMorphology, MORPH_CLOSE, kernel);

        Mat blurred = new Mat();
        GaussianBlur(closingMorphology, blurred, new Size(7, 7), 0.0);

        int maxSideLength = Math.max(image.cols(), image.rows());
        double resizeFactor = DEFAULT_EXTRACTION_MAX_SIDE_LENGTH / maxSideLength;
        Mat resized = new Mat();
        resize(blurred, resized, new Size(), resizeFactor, resizeFactor, INTER_CUBIC);

        Mat luvImage = new Mat();
        cvtColor(resized, luvImage, COLOR_BGR2Luv);

        MatOfPoint contour = null;
        boolean isFound = false;

        for (int highThreshold = 200; highThreshold >= 50; highThreshold -= 50) {
            int lowThresholdStart;
            if ((highThreshold - 20) % 20 == 0) {
                lowThresholdStart = highThreshold - 10;
            } else {
                lowThresholdStart = highThreshold - 20;
            }
            for (int lowThreshold = lowThresholdStart; lowThreshold >= 10; lowThreshold -= 20) {
                Mat canny = new Mat();
                Canny(luvImage, canny, lowThreshold, highThreshold);

                MatOfPoint bestContour = findBestContour(canny,
                        DEFAULT_MIN_AREA_PERCENTAGE,
                        DEFAULT_MIN_WIDTH_TO_HEIGHT_RATIO,
                        DEFAULT_MAX_WIDTH_TO_HEIGHT_RATIO);
                if (bestContour != null) {
                    contour = bestContour;
                    isFound = true;
                    break;
                }
            }
            if (isFound) {
                break;
            }

            Mat canny = new Mat();
            Canny(luvImage, canny, 4, highThreshold);
            MatOfPoint bestContour = findBestContour(canny,
                    DEFAULT_MIN_AREA_PERCENTAGE,
                    DEFAULT_MIN_WIDTH_TO_HEIGHT_RATIO,
                    DEFAULT_MAX_WIDTH_TO_HEIGHT_RATIO);

            if (bestContour != null) {
                contour = bestContour;
                isFound = true;
                break;
            }
        }

        List<Point> finalPoints = null;
        if (isFound) {
            List<Point> points = approximateContourPolygon(contour, DEFAULT_ARC_LENGTH_PERCENTAGE);
            if (points != null) {
                for (Point point : points) {
                    point.x /= resizeFactor;
                    point.y /= resizeFactor;
                }
                finalPoints = points;
            }
        }

        return finalPoints;
    }

    public static void main(String[] args) throws IOException {
        AtomicLong averageTime = new AtomicLong();
        AtomicLong n = new AtomicLong();
        try (var stream = Files.list(Paths.get("/home/u764/Downloads/fonts-downloads/images"))) {
            stream.filter(file -> !Files.isDirectory(file))
                    .forEach(file -> {
                        var image = imread(file.toString());

                        var start = System.currentTimeMillis();
                        List<Point> finalPoints = getFourPolygonPoints(image);

                        averageTime.set(averageTime.get() + System.currentTimeMillis() - start);
                        n.set(n.get() + 1);

                        if (finalPoints != null) {
                            for (Point point : finalPoints) {
                                circle(image, point, (int) (image.width() * 0.005), new Scalar(255), -1);
                            }

//                            drawContours(luvImage, List.of(contour), 0, new Scalar(0));
                            imwrite("/home/u764/Downloads/fonts-downloads/images-out/" + file.getFileName(),
                                    image);
                        }
                    });

            System.out.println((double) averageTime.get() / (double) n.get());

        }
    }
}
