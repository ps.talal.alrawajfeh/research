using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Progressoft.AppUtils.Core.Logging;
using Progressoft.ML.ImageDenoisingNet.Core.Documents;
using Progressoft.ML.SignatureFeaturesNet.Core;

namespace Progressoft.ASV.Service
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Environment.SetEnvironmentVariable("PROGRESSSOFT_HOME",
                "C:\\Users\\USER\\Desktop\\git\\progressoft-asv-core\\src\\Progressoft.ASV.Service\\ML.Models");
            
            var image = new Bitmap(
                "C:\\Users\\USER\\Downloads\\0A088E4EFB9D4F7895231617BAC051C0");
            
            var options = DocDenoisingNetOptions.CreateTensorflow("ChequeClearing_enc.pb",
                "input_1",
                "conv2d_15/Sigmoid",
                new Size(748, 200), 200, 200,
                null).Value;
            
            bool enableMultiThreading = true;
            int numberOfThreads = 50;
            int totalIterations = 1000;

            // initalize model before benchmark
            var docDenoisingNet = new DocDenoisingNet(options);
                        
            var output = docDenoisingNet.Predict(new List<DocDenoisingNetInput>()
            {
                DocDenoisingNetInput.Create(image, new Rectangle(0, 0, image.Width, image.Height), true).Value
            });

            Semaphore semaphore = new Semaphore(initialCount: numberOfThreads, maximumCount: numberOfThreads);
            int successful = 0;
            Object lockObj = new Object();

            var stopwatch = new Stopwatch();

            stopwatch.Start();

            if (!enableMultiThreading)
            {
                for (int i = 0; i < totalIterations; i++)
                {
                    try {
                        output = docDenoisingNet.Predict(new List<DocDenoisingNetInput>()
                        {
                            DocDenoisingNetInput.Create(image, new Rectangle(0, 0, image.Width, image.Height), true).Value
                        });

                        if (output.IsFailure)
                        {
                            Console.WriteLine("Failed");
                        }

                        if (output.IsSuccess)
                        {
                            Console.WriteLine("Success");
                            successful++;
                        }
                    } catch {

                    }
                }
            }
            else
            {                
                List<Task> tasks = new List<Task>();
                for (int i = 0; i < totalIterations; i++)
                {
                    var task = Task.Factory.StartNew(() =>
                    {
                        semaphore.WaitOne();
                        
                        try {
                            output = docDenoisingNet.Predict(new List<DocDenoisingNetInput>()
                            {
                                DocDenoisingNetInput.Create(image, new Rectangle(0, 0, image.Width, image.Height), true)
                                    .Value
                            });
                            
                            if (output.IsFailure)
                            {
                                Console.WriteLine("Failed");
                            }

                            if (output.IsSuccess)
                            {
                                Console.WriteLine("Success");
                                lock (lockObj) {
                                    successful++;
                                }
                            }
                        } catch {

                        }

                        semaphore.Release();
                    });
                    tasks.Add(task);
                }

                Task.WaitAll(tasks.ToArray());
            }

            stopwatch.Stop();
            Console.WriteLine((double) stopwatch.ElapsedMilliseconds / (double) successful);
            
            return;
            LoggerFactory.Instance.Initialize();
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });
    }
}
