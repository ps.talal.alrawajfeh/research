import cv2
import os
import base64
from datetime import datetime
import requests
import json
import dill
from tqdm import tqdm


ACCOUNTS_PATH = '/home/u764/Downloads/ATB/accounts'
MAX_RETRIES = 3


def image_to_base64(image, extension='png'):
    _, buffer = cv2.imencode(f'.{extension}', image)
    return base64.b64encode(buffer).decode()


def main():
    accounts = os.listdir(ACCOUNTS_PATH)

    cheques_with_problems = []

    with open('request_template.json', 'r') as f:
        request_template = f.read()

    i = accounts.index('0201103892101')
    accounts = accounts[i:]

    count = 0
    for account in accounts:
        cheques_path = os.path.join(ACCOUNTS_PATH, account)
        count += len(os.listdir(cheques_path))

    progress_bar = tqdm(total=count)
    for account in accounts:
        cheques_path = os.path.join(ACCOUNTS_PATH, account)
        cheques = os.listdir(cheques_path)

        for cheque in cheques:
            cheque_path = os.path.join(cheques_path, cheque)
            cheque_image = image_to_base64(cv2.imread(cheque_path))

            new_request = request_template.replace(
                '${ACCOUNT_NUMBER}', account.strip())
            new_request = new_request.replace(
                '${DATE}', datetime.now().isoformat())
            new_request = new_request.replace('${IMAGE_FILE_NAME}', cheque)
            new_request = new_request.replace('${IMAGE_BASE64}', cheque_image)

            response = requests.post('http://localhost:9090/asv/api/v1/DocumentVerification/VerifyDocument',
                                     json=json.loads(new_request))
            status_code = response.status_code
            retry_count = 0
            while status_code != 200 and retry_count < MAX_RETRIES:
                response = requests.post('http://localhost:9090/asv/api/v1/DocumentVerification/VerifyDocument',
                                         json=json.loads(new_request))
                status_code = response.status_code
                retry_count += 1

            if status_code != 200:
                response_body = response.json()
                cheques_with_problems.append(
                    (account, cheque, status_code, response_body))
                print(f'cheque with problem detected:')
                print(f'    - account number: {account}')
                print(f'    - cheque file name: {cheque}')
                print(f'    - response status: {status_code}')
                print(f'    - response body: {response_body}')

            progress_bar.update()
    progress_bar.close()

    print(f'\n{len(cheques_with_problems)} cheques with problems detected')
    if len(cheques_with_problems) > 0:
        with open('cheques_with_problems.dill', 'wb') as f:
            dill.dump(cheques_with_problems, f)


if __name__ == '__main__':
    main()
