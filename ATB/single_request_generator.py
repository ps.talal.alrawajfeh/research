import cv2
import os
import base64
from datetime import datetime
import requests
import json
import dill
from tqdm import tqdm


ACCOUNTS_PATH = '/home/u764/Downloads/ATB/accounts'


def image_to_base64(image, extension='png'):
    _, buffer = cv2.imencode(f'.{extension}', image)
    return base64.b64encode(buffer).decode()


def main(account, cheque):
    with open('request_template.json', 'r') as f:
        request_template = f.read()

    cheques_path = os.path.join(ACCOUNTS_PATH, account)
    cheque_path = os.path.join(cheques_path, cheque)
    cheque_image = image_to_base64(cv2.imread(cheque_path))

    new_request = request_template.replace(
        '${ACCOUNT_NUMBER}', account.strip())
    new_request = new_request.replace(
        '${DATE}', datetime.now().isoformat())
    new_request = new_request.replace('${IMAGE_FILE_NAME}', cheque)
    new_request = new_request.replace('${IMAGE_BASE64}', cheque_image)

    response = requests.post('http://localhost:9090/asv/api/v1/DocumentVerification/VerifyDocument',
                                json=json.loads(new_request))

    status_code = response.status_code
    if status_code != 200:
        response_body = response.json()
        print(f'cheque with problem detected:')
        print(f'    - account number: {account}')
        print(f'    - cheque file name: {cheque}')
        print(f'    - response status: {status_code}')
        print(f'    - response body: {response_body}')


if __name__ == '__main__':
    main('0201103562806', '00250250100102011035628068507R_fullCheque.bmp')
