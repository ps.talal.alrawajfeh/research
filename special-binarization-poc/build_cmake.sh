#!/usr/bin/bash

cmake -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON -DCMAKE_BUILD_TYPE=Release ./build
cmake --build ./build --config Release --target all -j 10 --