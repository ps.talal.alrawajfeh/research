#!/usr/bin/python3

import cv2
import numpy as np


def main():
    image = cv2.imread('/home/u764/Downloads/arbk-palestine/cheques-73/2BB12D61A48240F4B815746E245B9DDB_10971_30000261.JPG',
                       cv2.IMREAD_GRAYSCALE)
    original_image = image
    print(image.shape)
    with open('./test-image.csv', 'w') as f:
        for y in range(image.shape[0]):
            for x in range(image.shape[1]):
                f.write(f'{image[y, x]}')
                if x < image.shape[1] - 1:
                    f.write(',')
            if y < image.shape[0] - 1:
                f.write('\n')

    with open('./test-result.csv', 'r') as f:
        csv = f.read()

    lines = csv.split('\n')
    matrix = [list(map(lambda x: int(x), line.split(','))) for line in lines]

    image = np.array(matrix, np.uint8)
    print(image.shape)
    cv2.imwrite('test-result.bmp', image)

    # print(np.count_nonzero(cv2.dilate(original_image, np.ones((7, 5), np.uint8)) != image))
    print(np.count_nonzero(cv2.GaussianBlur(original_image, (3, 3), 0) != image))

if __name__ == '__main__':
    main()
