#include <memory>
#include <vector>
#include <iostream>
#include <algorithm>
#include <fstream>
#include <string>
#include <ctime>
#include <chrono>
#include <immintrin.h>
#include <math.h>
#include <ctime>
#include <chrono>
#include <cstdlib>

using std::chrono::duration_cast;
using std::chrono::milliseconds;
using std::chrono::seconds;
using std::chrono::system_clock;
using namespace std::chrono;

#if defined(_WIN32)
#include <intrin.h>
#define cpuid(info, x) __cpuidex(info, x, 0)
#else
#include <cpuid.h>
void cpuid(int info[4], int InfoType)
{
    __cpuid_count(InfoType, 0, info[0], info[1], info[2], info[3]);
}
#endif

#if defined(_MSC_VER)
#define INTERFACE_API extern "C" __declspec(dllexport)
#elif defined(__GNUC__)
#define INTERFACE_API extern "C" __attribute__((visibility("default")))
#else
#define INTERFACE_API extern "C"
#endif

#define uint8 uint8_t
#define EPSILON 1e-5

static bool IS_AVX_SUPPORT_CHECKED = false;
static bool IS_AVX_SUPPORTED;

bool isAVXSupported()
{
    if (!IS_AVX_SUPPORT_CHECKED)
    {
        bool supported = false;
        int info[4];
        cpuid(info, 0);
        int nIds = info[0];

        if (nIds >= 0x00000001)
        {
            cpuid(info, 0x00000001);
            supported = ((_xgetbv(0) & 0x6) == 0x6) &&
                        ((info[2] & ((int)1 << 28)) != 0) &&
                        (info[2] & (1 << 27) || false);
        }

        IS_AVX_SUPPORT_CHECKED = true;
        IS_AVX_SUPPORTED = supported;

        if (supported)
        {
            std::cout << "NativeImaging: AVX2 is supported by both the Operating System and the CPU." << std::endl;
        }
        else
        {
            std::cout << "NativeImaging: AVX2 is not supported, this means that some performance-critical operations will not run optimally." << std::endl;
        }
    }
    return IS_AVX_SUPPORTED;
}

inline int specialFloor(const double value)
{
    const int floorValue = (int)value;
    return floorValue > value ? floorValue - 1 : floorValue;
}

inline int specialCeil(const double value)
{
    const int ceilValue = (int)std::ceil(value);
    return ceilValue < value ? ceilValue + 1 : ceilValue;
}

inline uint8 convertDoubleToByte(const double value)
{
    const double v = std::min(255.0, std::max(0.0, value));
    const int intV = static_cast<int>(v);
    const double diffV = v - intV;
    return static_cast<uint8>(static_cast<unsigned int>(diffV > 0.5 || diffV == 0.5 && (intV & 1) != 0
                                                            ? intV + 1
                                                            : intV));
}

inline uint8 convertFloatToByte(const float value)
{
    const float v = std::min(255.0f, std::max(0.0f, value));
    const int intV = static_cast<int>(v);
    const float diffV = v - intV;
    return static_cast<uint8>(static_cast<unsigned int>(diffV > 0.5 || diffV == 0.5 && (intV & 1) != 0
                                                            ? intV + 1
                                                            : intV));
}

template <int kernelSize>
std::shared_ptr<uint8> filter(uint8 const *inputImage,
                              const int width,
                              const int height,
                              const int stride,
                              const float kernel[kernelSize * kernelSize],
                              const float weight,
                              const float bias)
{
    std::shared_ptr<uint8> outputImage(new uint8[stride * height]);
    const int kernelRadius = kernelSize / 2;

    const int rightMargin = width - kernelRadius;
    const int bottomMargin = height - kernelRadius;

    uint8 const *pInputImage = inputImage;
    uint8 *pOutputImage = outputImage.get();

    const int offset = stride - width;
    uint8 const *pTmpInputImage = pInputImage;
    uint8 *pTmpOutputImage = pOutputImage;

    std::shared_ptr<float> inputImageCopy(new float[stride * height]);
    float *pInputImageCopy = inputImageCopy.get();

    for (int y = 0; y < height; y++)
    {
        for (int x = 0; x < width; x++)
        {
            *pInputImageCopy = static_cast<float>(*pTmpInputImage);
            pTmpInputImage++;
            pInputImageCopy++;
        }
        pTmpInputImage += offset;
        pInputImageCopy += offset;
    }

    pInputImageCopy = inputImageCopy.get();
    for (int y = 0; y < height; y++)
    {
        for (int x = 0; x < width; x++)
        {
            if (x >= kernelRadius && x < rightMargin && y >= kernelRadius && y < bottomMargin)
            {
                pInputImageCopy++;
                pTmpOutputImage++;
                continue;
            }
            *pTmpOutputImage = convertFloatToByte(*pInputImageCopy * weight + bias);
            pInputImageCopy++;
            pTmpOutputImage++;
        }
        pInputImageCopy += offset;
        pTmpOutputImage += offset;
    }

    float const *pKernel = &kernel[0];
    float const *pTmpKernel;
    int offsetYKernel = -kernelRadius * stride;

    pInputImageCopy = inputImageCopy.get();
    pTmpOutputImage = pOutputImage + kernelRadius * stride;
    pInputImageCopy = pInputImageCopy + kernelRadius * stride;
    for (int y = kernelRadius; y < bottomMargin; y++)
    {
        pTmpOutputImage += kernelRadius;
        pInputImageCopy += kernelRadius;
        for (int x = kernelRadius; x < rightMargin; x++)
        {
            float pixelValue = 0.0f;
            int iY = offsetYKernel;
            int iX;
            pTmpKernel = pKernel;
            for (int i = -kernelRadius; i <= kernelRadius; i++)
            {
                iX = -kernelRadius;
                for (int j = -kernelRadius; j <= kernelRadius; j++)
                {
                    pixelValue += *(pInputImageCopy + iY + iX) * *pTmpKernel;

                    pTmpKernel++;
                    iX++;
                }
                iY += stride;
            }
            *pTmpOutputImage = convertFloatToByte(pixelValue * weight + bias);

            pInputImageCopy++;
            pTmpOutputImage++;
        }
        pTmpOutputImage += kernelRadius + offset;
        pInputImageCopy += kernelRadius + offset;
    }

    return outputImage;
}

std::shared_ptr<uint8> gaussianBlur3x3(uint8 const *inputImage,
                                       const int width,
                                       const int height,
                                       const int stride)
{
    const float gaussianKernel[9] = {
        1.0f,
        2.0f,
        1.0f,
        2.0f,
        4.0f,
        2.0f,
        1.0f,
        2.0f,
        1.0f,
    };

    const float weight = 1.0f / 16.0f;

    return filter<3>(inputImage, width, height, stride, gaussianKernel, weight, 0.0);
}

template <int kernelWidth, int kernelHeight>
std::shared_ptr<uint8> dilate(uint8 const *inputImage,
                              const int width,
                              const int height,
                              const int stride)
{
    std::shared_ptr<uint8> outputImage(new uint8[stride * height]);

    uint8 const *inputPtr = inputImage;
    uint8 *outputPtr = outputImage.get();

    const int offset = stride - width;
    const int radiusX = kernelWidth / 2;
    const int radiusY = kernelHeight / 2;

    uint8 const *pTmpInputPtr = inputPtr;
    uint8 *pTmpOutputPtr = outputPtr;

    for (int y = 0; y < height; y++)
    {
        for (int x = 0; x < width; x++)
        {
            if (x >= radiusX && x < width - radiusX && y >= radiusY && y < height - radiusY)
            {
                pTmpInputPtr++;
                pTmpOutputPtr++;
                continue;
            }
            *pTmpOutputPtr = *pTmpInputPtr;
            pTmpInputPtr++;
            pTmpOutputPtr++;
        }
        pTmpInputPtr += offset;
        pTmpOutputPtr += offset;
    }

    int offsetYKernel = -radiusY * stride;
    inputPtr += stride * radiusY;
    outputPtr += stride * radiusY;
    for (int y = radiusY; y < height - radiusY; y++)
    {
        inputPtr += radiusX;
        outputPtr += radiusX;
        for (int x = radiusX; x < width - radiusX; x++)
        {
            uint8 pixelValue = static_cast<uint8>(0);
            int iY = offsetYKernel;
            int iX;
            for (int dy = -radiusY; dy <= radiusY; dy++)
            {
                iX = -radiusX;
                for (int dx = -radiusX; dx <= radiusX; dx++)
                {
                    pixelValue = std::max<uint8>(pixelValue, *(inputPtr + iY + iX));
                    iX++;
                }
                iY += stride;
            }

            *outputPtr = pixelValue;

            outputPtr++;
            inputPtr++;
        }
        outputPtr += offset + radiusX;
        inputPtr += offset + radiusX;
    }

    return outputImage;
}

std::shared_ptr<uint8> subtractAndInvert(uint8 const *inputImage1,
                                         uint8 const *inputImage2,
                                         const int width,
                                         const int height,
                                         const int stride)
{
    std::shared_ptr<uint8> outputImage(new uint8[stride * height]);

    const int offset = stride - width;
    uint8 const *ptr1 = inputImage1;
    uint8 const *ptr2 = inputImage2;
    uint8 *outputPtr = outputImage.get();

    for (int y = 0; y < height; y++)
    {
        for (int x = 0; x < width; x++)
        {
            *outputPtr = *ptr1 < *ptr2 ? static_cast<uint8>(0) : static_cast<uint8>(255 - static_cast<int>(*ptr1) + static_cast<int>(*ptr2));
            ptr1++;
            ptr2++;
            outputPtr++;
        }
        ptr1 += offset;
        ptr2 += offset;
        outputPtr += offset;
    }

    return outputImage;
}

uint8 otsuThreshold(uint8 const *inputImage,
                    const int width,
                    const int height,
                    const int stride)
{
    const int offset = stride - width;
    uint8 const *imagePtr = inputImage;

    int histogram[256];
    for (int i = 0; i < 256; i++)
    {
        histogram[i] = 0;
    }

    for (int y = 0; y < height; y++)
    {
        for (int x = 0; x < width; x++)
        {
            int i = imagePtr[0];
            histogram[i]++;
            imagePtr++;
        }

        imagePtr += offset;
    }

    double scale = 1.0 / (height * width);
    double mu = 0.0;

    int *pHistogram = &histogram[0];
    for (int i = 0; i < 256; i++)
    {
        mu += static_cast<double>(*pHistogram) * static_cast<double>(i);
        pHistogram++;
    }
    mu *= scale;

    double q1 = 0;
    int maxVal = 0;
    double maxSigma = -1;
    double mu1 = 0;

    for (int t = 0; t < 256; t++)
    {
        double pt = static_cast<double>(histogram[t]) * scale;
        mu1 *= q1;
        q1 += pt;
        double q2 = 1.0 - q1;

        if (std::min(q1, q2) < EPSILON || std::max(q1, q2) > 1.0 - EPSILON)
        {
            continue;
        }

        mu1 = (mu1 + t * pt) / q1;
        double mu2 = (mu - q1 * mu1) / q2;
        double sigma = q1 * q2 * (mu1 - mu2) * (mu1 - mu2);

        if (sigma > maxSigma)
        {
            maxSigma = sigma;
            maxVal = t;
        }
    }

    return maxVal;
}

std::shared_ptr<uint8> backgroundAdaptiveThreshold(uint8 const *inputImage,
                                                   const int width,
                                                   const int height,
                                                   const int stride,
                                                   const double alpha)
{
    auto image = gaussianBlur3x3(inputImage, width, height, stride);
    auto dilation = dilate<5, 7>(image.get(), width, height, stride);
    auto subtracted = subtractAndInvert(dilation.get(), image.get(), width, height, stride);

    auto threshold = otsuThreshold(image.get(), width, height, stride);
    auto subtractedThreshold = otsuThreshold(subtracted.get(), width, height, stride);

    int strictThreshold = static_cast<int>(static_cast<double>(threshold) * alpha);

    std::shared_ptr<uint8> outputImage(new uint8[stride * height]);

    uint8 *imagePtr = image.get();
    uint8 *outputPtr = outputImage.get();
    uint8 *subtractedPtr = subtracted.get();

    const int offset = stride - width;

    for (int y = 0; y < height; y++)
    {
        for (int x = 0; x < width; x++)
        {
            if (*subtractedPtr <= subtractedThreshold && *imagePtr < threshold)
            {
                *outputPtr = 0;
            }
            else if (*imagePtr <= strictThreshold)
            {
                *outputPtr = 0;
            }
            else
            {
                *outputPtr = 255;
            }

            imagePtr++;
            outputPtr++;
            subtractedPtr++;
        }

        imagePtr += offset;
        outputPtr += offset;
        subtractedPtr += offset;
    }

    return outputImage;
}

INTERFACE_API void backgroundAdaptiveThresholdApi(uint8 const *inputImage,
                                                  const int width,
                                                  const int height,
                                                  const int stride,
                                                  const double alpha,
                                                  uint8 *outputImage,
                                                  const int outputImageStride)
{
    auto result = backgroundAdaptiveThreshold(inputImage, width, height, stride, alpha);
    auto imagePtr = result.get();

    auto ptr1 = imagePtr;
    auto ptr2 = outputImage;
    const int inputOffset = stride - width;
    const int outputOffset = outputImageStride - width;

    for (int y = 0; y < height; y++)
    {
        for (int x = 0; x < width; x++)
        {
            *ptr2 = *ptr1;
            ptr1++;
            ptr2++;
        }
        ptr1 += inputOffset;
        ptr2 += outputOffset;
    }
}

inline double interpolateCubic(const double x)
{
    const double a = -0.75;
    const double fx = x >= 0 ? x : -1.0 * x;

    return fx >= 0 && fx <= 1
               ? ((a + 2) * fx - (a + 3)) * fx * fx + 1
           : fx > 1 && fx <= 2
               ? ((a * fx - 5 * a) * fx + 8 * a) * fx - 4 * a
               : 0.0;
}

std::shared_ptr<uint8> resizeBiCubicInterpolation(uint8 const *image,
                                                  const int dstWidth,
                                                  const int dstHeight,
                                                  const int srcWidth,
                                                  const int srcHeight,
                                                  const int srcStride,
                                                  const int srcChannels)
{
    const int channels = srcChannels;

    std::shared_ptr<uint8> result(new uint8[dstWidth * dstHeight * channels]);
    std::shared_ptr<double> imageCopy(new double[srcWidth * srcHeight * channels]);

    double *imageCopyPtr = imageCopy.get();
    uint8 const *imagePtr = image;
    const int imageOffset = srcStride - srcWidth * channels;

    for (int y = 0; y < srcHeight; y++)
    {
        for (int x = 0; x < srcWidth * channels; x++)
        {
            *imageCopyPtr = static_cast<double>(*imagePtr);

            imageCopyPtr++;
            imagePtr++;
        }
        imagePtr += imageOffset;
    }

    const double scaleX = (double)dstWidth / (double)srcWidth;
    const double scaleY = (double)dstHeight / (double)srcHeight;

    uint8 *resultPtr = result.get();
    uint8 *tmpResultPtr = resultPtr;

    const double invScaleX = 1.0 / scaleX;
    const double invScaleY = 1.0 / scaleY;

    std::shared_ptr<double> hKernelCoefficients(new double[dstWidth * 4]);
    std::shared_ptr<int> hKernelX(new int[dstWidth]);
    std::shared_ptr<double> vKernelCoefficients(new double[dstHeight * 4]);
    std::shared_ptr<int> vKernelY(new int[dstWidth]);

    int dY, dX, c;
    double sX, sY;
    double dX1, dX2, dX3, dX4,
        dY1, dY2, dY3, dY4;
    int x1, x2, x3, x4,
        y1, y2, y3, y4;
    bool bX1, bX2, bX3, bX4,
        bY1, bY2, bY3, bY4;
    int iX1, iX2, iX3, iX4,
        iY1, iY2, iY3, iY4;
    double kX1, kX2, kX3, kX4,
        kY1, kY2, kY3, kY4;

    bool b11, b12, b13, b14,
        b21, b22, b23, b24,
        b31, b32, b33, b34,
        b41, b42, b43, b44;

    double *ptr11, *ptr12, *ptr13, *ptr14,
        *ptr21, *ptr22, *ptr23, *ptr24,
        *ptr31, *ptr32, *ptr33, *ptr34,
        *ptr41, *ptr42, *ptr43, *ptr44;

    double k11, k12, k13, k14,
        k21, k22, k23, k24,
        k31, k32, k33, k34,
        k41, k42, k43, k44;

    int sXStart, sYStart;
    int resultOffset = 0;
    double v;

    double *pVKernelCoefficients = vKernelCoefficients.get();
    int *pVKernelY = vKernelY.get();
    double *pHKernelCoefficients = hKernelCoefficients.get();
    int *pHKernelX = hKernelX.get();

    double *pTmpVKernelCoefficients = pVKernelCoefficients;
    int *pTmpVKernelY = pVKernelY;
    double *pTmpHKernelCoefficients = pHKernelCoefficients;
    int *pTmpHKernelX = pHKernelX;

    for (int x = 0; x < dstWidth; x++)
    {
        sX = (x + 0.5) * invScaleX - 0.5;
        int sXFloor = specialFloor(sX);

        dX1 = 1 + sX - sXFloor;
        dX2 = sX - sXFloor;
        dX3 = sXFloor + 1 - sX;
        dX4 = sXFloor + 2 - sX;

        *pTmpHKernelCoefficients = interpolateCubic(dX1);
        pTmpHKernelCoefficients++;
        *pTmpHKernelCoefficients = interpolateCubic(dX2);
        pTmpHKernelCoefficients++;
        *pTmpHKernelCoefficients = interpolateCubic(dX3);
        pTmpHKernelCoefficients++;
        *pTmpHKernelCoefficients = interpolateCubic(dX4);
        pTmpHKernelCoefficients++;

        *pTmpHKernelX = sXFloor - 1;
        pTmpHKernelX++;
    }

    for (int y = 0; y < dstHeight; y++)
    {
        sY = (y + 0.5) * invScaleY - 0.5;
        int sYFloor = specialFloor(sY);

        dY1 = 1 + sY - sYFloor;
        dY2 = sY - sYFloor;
        dY3 = sYFloor + 1 - sY;
        dY4 = sYFloor + 2 - sY;

        *pTmpVKernelCoefficients = interpolateCubic(dY1);
        pTmpVKernelCoefficients++;
        *pTmpVKernelCoefficients = interpolateCubic(dY2);
        pTmpVKernelCoefficients++;
        *pTmpVKernelCoefficients = interpolateCubic(dY3);
        pTmpVKernelCoefficients++;
        *pTmpVKernelCoefficients = interpolateCubic(dY4);
        pTmpVKernelCoefficients++;

        *pTmpVKernelY = sYFloor - 1;
        pTmpVKernelY++;
    }

    pTmpVKernelCoefficients = pVKernelCoefficients;
    pTmpVKernelY = pVKernelY;
    imageCopyPtr = imageCopy.get();

    for (dY = 0; dY < dstHeight; dY++)
    {
        kY1 = *pTmpVKernelCoefficients;
        pTmpVKernelCoefficients++;
        kY2 = *pTmpVKernelCoefficients;
        pTmpVKernelCoefficients++;
        kY3 = *pTmpVKernelCoefficients;
        pTmpVKernelCoefficients++;
        kY4 = *pTmpVKernelCoefficients;
        pTmpVKernelCoefficients++;

        sYStart = *pTmpVKernelY;
        pTmpVKernelY++;

        y1 = sYStart;
        y2 = sYStart + 1;
        y3 = sYStart + 2;
        y4 = sYStart + 3;

        iY1 = y1 * srcWidth * channels;
        iY2 = y2 * srcWidth * channels;
        iY3 = y3 * srcWidth * channels;
        iY4 = y4 * srcWidth * channels;

        bY1 = y1 < 0 || y1 >= srcHeight;
        bY2 = y2 < 0 || y2 >= srcHeight;
        bY3 = y3 < 0 || y3 >= srcHeight;
        bY4 = y4 < 0 || y4 >= srcHeight;

        pTmpHKernelCoefficients = pHKernelCoefficients;
        pTmpHKernelX = pHKernelX;

        for (dX = 0; dX < dstWidth; dX++)
        {
            kX1 = *pTmpHKernelCoefficients;
            pTmpHKernelCoefficients++;
            kX2 = *pTmpHKernelCoefficients;
            pTmpHKernelCoefficients++;
            kX3 = *pTmpHKernelCoefficients;
            pTmpHKernelCoefficients++;
            kX4 = *pTmpHKernelCoefficients;
            pTmpHKernelCoefficients++;

            sXStart = *pTmpHKernelX;
            pTmpHKernelX++;

            x1 = sXStart;
            x2 = sXStart + 1;
            x3 = sXStart + 2;
            x4 = sXStart + 3;

            iX1 = x1 * channels;
            iX2 = x2 * channels;
            iX3 = x3 * channels;
            iX4 = x4 * channels;

            bX1 = x1 < 0 || x1 >= srcWidth;
            bX2 = x2 < 0 || x2 >= srcWidth;
            bX3 = x3 < 0 || x3 >= srcWidth;
            bX4 = x4 < 0 || x4 >= srcWidth;

            k11 = kY1 * kX1;
            k21 = kY2 * kX1;
            k31 = kY3 * kX1;
            k41 = kY4 * kX1;
            k12 = kY1 * kX2;
            k22 = kY2 * kX2;
            k32 = kY3 * kX2;
            k42 = kY4 * kX2;
            k13 = kY1 * kX3;
            k23 = kY2 * kX3;
            k33 = kY3 * kX3;
            k43 = kY4 * kX3;
            k14 = kY1 * kX4;
            k24 = kY2 * kX4;
            k34 = kY3 * kX4;
            k44 = kY4 * kX4;

            b11 = bY1 || bX1;
            b21 = bY2 || bX1;
            b31 = bY3 || bX1;
            b41 = bY4 || bX1;
            b12 = bY1 || bX2;
            b22 = bY2 || bX2;
            b32 = bY3 || bX2;
            b42 = bY4 || bX2;
            b13 = bY1 || bX3;
            b23 = bY2 || bX3;
            b33 = bY3 || bX3;
            b43 = bY4 || bX3;
            b14 = bY1 || bX4;
            b24 = bY2 || bX4;
            b34 = bY3 || bX4;
            b44 = bY4 || bX4;

            ptr11 = imageCopyPtr + iY1 + iX1;
            ptr21 = imageCopyPtr + iY2 + iX1;
            ptr31 = imageCopyPtr + iY3 + iX1;
            ptr41 = imageCopyPtr + iY4 + iX1;
            ptr12 = imageCopyPtr + iY1 + iX2;
            ptr22 = imageCopyPtr + iY2 + iX2;
            ptr32 = imageCopyPtr + iY3 + iX2;
            ptr42 = imageCopyPtr + iY4 + iX2;
            ptr13 = imageCopyPtr + iY1 + iX3;
            ptr23 = imageCopyPtr + iY2 + iX3;
            ptr33 = imageCopyPtr + iY3 + iX3;
            ptr43 = imageCopyPtr + iY4 + iX3;
            ptr14 = imageCopyPtr + iY1 + iX4;
            ptr24 = imageCopyPtr + iY2 + iX4;
            ptr34 = imageCopyPtr + iY3 + iX4;
            ptr44 = imageCopyPtr + iY4 + iX4;

            for (c = 0; c < channels; c++)
            {
                v = (b11 ? 0.0 : k11 * *(ptr11 + c)) +
                    (b21 ? 0.0 : k21 * *(ptr21 + c)) +
                    (b31 ? 0.0 : k31 * *(ptr31 + c)) +
                    (b41 ? 0.0 : k41 * *(ptr41 + c)) +
                    (b12 ? 0.0 : k12 * *(ptr12 + c)) +
                    (b22 ? 0.0 : k22 * *(ptr22 + c)) +
                    (b32 ? 0.0 : k32 * *(ptr32 + c)) +
                    (b42 ? 0.0 : k42 * *(ptr42 + c)) +
                    (b13 ? 0.0 : k13 * *(ptr13 + c)) +
                    (b23 ? 0.0 : k23 * *(ptr23 + c)) +
                    (b33 ? 0.0 : k33 * *(ptr33 + c)) +
                    (b43 ? 0.0 : k43 * *(ptr43 + c)) +
                    (b14 ? 0.0 : k14 * *(ptr14 + c)) +
                    (b24 ? 0.0 : k24 * *(ptr24 + c)) +
                    (b34 ? 0.0 : k34 * *(ptr34 + c)) +
                    (b44 ? 0.0 : k44 * *(ptr44 + c));

                *tmpResultPtr = convertDoubleToByte(v);
                tmpResultPtr++;
            }
        }

        tmpResultPtr += resultOffset;
    }

    return result;
}

std::shared_ptr<uint8> resizeBiCubicInterpolationAVX(uint8 const *image,
                                                     const int dstWidth,
                                                     const int dstHeight,
                                                     const int srcWidth,
                                                     const int srcHeight,
                                                     const int srcStride,
                                                     const int srcChannels)
{
    const int channels = srcChannels;

    std::shared_ptr<uint8> result(new uint8[dstWidth * dstHeight * channels]);
    std::vector<std::shared_ptr<double>> splittedChannels;
    std::vector<double *> pSplittedChannels;

    for (int c = 0; c < channels; c++)
    {
        std::shared_ptr<double> imageChannel(new double[srcWidth * srcHeight]);
        splittedChannels.push_back(imageChannel);
        pSplittedChannels.push_back(imageChannel.get());

        uint8 const *tempPtr1 = image;
        double *tempPtr2 = pSplittedChannels[c];
        int iY = 0;

        for (int y = 0; y < srcHeight; y++)
        {
            tempPtr1 = image + iY + c;

            for (int x = 0; x < srcWidth; x++)
            {
                *tempPtr2 = static_cast<double>(*tempPtr1);

                tempPtr2++;
                tempPtr1 += channels;
            }

            iY += srcStride;
        }
    }

    const double scaleX = (double)dstWidth / (double)srcWidth;
    const double scaleY = (double)dstHeight / (double)srcHeight;

    uint8 *resultPtr = result.get();
    uint8 *tmpResultPtr = resultPtr;

    const double invScaleX = 1.0 / scaleX;
    const double invScaleY = 1.0 / scaleY;

    std::shared_ptr<double> hKernelCoefficients(new double[dstWidth * 4]);
    std::shared_ptr<int> hKernelX(new int[dstWidth]);
    std::shared_ptr<double> vKernelCoefficients(new double[dstHeight * 4]);
    std::shared_ptr<int> vKernelY(new int[dstWidth]);

    const double betaX = 0.5 * invScaleX - 0.5;
    const double betaY = 0.5 * invScaleY - 0.5;

    const int xMin = specialCeil((1.0 - betaX) / invScaleX);
    const int xMax = specialFloor((srcWidth - betaX - 2) / invScaleX);

    const int yMin = specialCeil((1.0 - betaY) / invScaleY);
    const int yMax = specialFloor((srcHeight - betaY - 2) / invScaleY);

    int dY, dX, c;
    double sX, sY;
    double dX1, dX2, dX3, dX4,
        dY1, dY2, dY3, dY4;
    double kY1, kY2, kY3, kY4;
    int x1, x2, x3, x4,
        y1, y2, y3, y4;
    bool bX1, bX2, bX3, bX4,
        bY1, bY2, bY3, bY4;
    int iY1, iY2, iY3, iY4;

    bool b11, b12, b13, b14,
        b21, b22, b23, b24,
        b31, b32, b33, b34,
        b41, b42, b43, b44;

    double *ptr11, *ptr12, *ptr13, *ptr14,
        *ptr21, *ptr22, *ptr23, *ptr24,
        *ptr31, *ptr32, *ptr33, *ptr34,
        *ptr41, *ptr42, *ptr43, *ptr44;

    int sXStart, sYStart;
    int resultOffset = 0;

    double *pVKernelCoefficients = vKernelCoefficients.get();
    int *pVKernelY = vKernelY.get();
    double *pHKernelCoefficients = hKernelCoefficients.get();
    int *pHKernelX = hKernelX.get();

    double *pTmpVKernelCoefficients = pVKernelCoefficients;
    int *pTmpVKernelY = pVKernelY;
    double *pTmpHKernelCoefficients = pHKernelCoefficients;
    int *pTmpHKernelX = pHKernelX;

    for (int x = 0; x < dstWidth; x++)
    {
        sX = (x + 0.5) * invScaleX - 0.5;
        int sXFloor = specialFloor(sX);

        dX1 = 1 + sX - sXFloor;
        dX2 = sX - sXFloor;
        dX3 = sXFloor + 1 - sX;
        dX4 = sXFloor + 2 - sX;

        *pTmpHKernelCoefficients = interpolateCubic(dX1);
        pTmpHKernelCoefficients++;
        *pTmpHKernelCoefficients = interpolateCubic(dX2);
        pTmpHKernelCoefficients++;
        *pTmpHKernelCoefficients = interpolateCubic(dX3);
        pTmpHKernelCoefficients++;
        *pTmpHKernelCoefficients = interpolateCubic(dX4);
        pTmpHKernelCoefficients++;

        *pTmpHKernelX = sXFloor - 1;
        pTmpHKernelX++;
    }

    for (int y = 0; y < dstHeight; y++)
    {
        sY = (y + 0.5) * invScaleY - 0.5;
        int sYFloor = specialFloor(sY);

        dY1 = 1 + sY - sYFloor;
        dY2 = sY - sYFloor;
        dY3 = sYFloor + 1 - sY;
        dY4 = sYFloor + 2 - sY;

        *pTmpVKernelCoefficients = interpolateCubic(dY1);
        pTmpVKernelCoefficients++;
        *pTmpVKernelCoefficients = interpolateCubic(dY2);
        pTmpVKernelCoefficients++;
        *pTmpVKernelCoefficients = interpolateCubic(dY3);
        pTmpVKernelCoefficients++;
        *pTmpVKernelCoefficients = interpolateCubic(dY4);
        pTmpVKernelCoefficients++;

        *pTmpVKernelY = sYFloor - 1;
        pTmpVKernelY++;
    }

    // first loop

    pTmpVKernelCoefficients = pVKernelCoefficients;
    pTmpVKernelY = pVKernelY;

    for (dY = 0; dY < dstHeight; dY++)
    {
        kY1 = *pTmpVKernelCoefficients;
        pTmpVKernelCoefficients++;
        kY2 = *pTmpVKernelCoefficients;
        pTmpVKernelCoefficients++;
        kY3 = *pTmpVKernelCoefficients;
        pTmpVKernelCoefficients++;
        kY4 = *pTmpVKernelCoefficients;
        pTmpVKernelCoefficients++;

        sYStart = *pTmpVKernelY;
        pTmpVKernelY++;

        y1 = sYStart;
        y2 = sYStart + 1;
        y3 = sYStart + 2;
        y4 = sYStart + 3;

        iY1 = y1 * srcWidth;
        iY2 = y2 * srcWidth;
        iY3 = y3 * srcWidth;
        iY4 = y4 * srcWidth;

        bY1 = y1 < 0 || y1 >= srcHeight;
        bY2 = y2 < 0 || y2 >= srcHeight;
        bY3 = y3 < 0 || y3 >= srcHeight;
        bY4 = y4 < 0 || y4 >= srcHeight;

        pTmpHKernelCoefficients = pHKernelCoefficients;
        pTmpHKernelX = pHKernelX;

        __m256d vectorKY1 = _mm256_set_pd(kY1, kY1, kY1, kY1);
        __m256d vectorKY2 = _mm256_set_pd(kY2, kY2, kY2, kY2);
        __m256d vectorKY3 = _mm256_set_pd(kY3, kY3, kY3, kY3);
        __m256d vectorKY4 = _mm256_set_pd(kY4, kY4, kY4, kY4);

        for (dX = 0; dX < dstWidth; dX++)
        {
            if (dX >= xMin && dX < xMax && dY >= yMin && dY < yMax)
            {
                pTmpHKernelCoefficients += 4;
                pTmpHKernelX++;
                tmpResultPtr += channels;
                continue;
            }

            sXStart = *pTmpHKernelX;
            pTmpHKernelX++;

            x1 = sXStart;
            x2 = sXStart + 1;
            x3 = sXStart + 2;
            x4 = sXStart + 3;

            bX1 = x1 < 0 || x1 >= srcWidth;
            bX2 = x2 < 0 || x2 >= srcWidth;
            bX3 = x3 < 0 || x3 >= srcWidth;
            bX4 = x4 < 0 || x4 >= srcWidth;

            b11 = bY1 || bX1;
            b21 = bY2 || bX1;
            b31 = bY3 || bX1;
            b41 = bY4 || bX1;
            b12 = bY1 || bX2;
            b22 = bY2 || bX2;
            b32 = bY3 || bX2;
            b42 = bY4 || bX2;
            b13 = bY1 || bX3;
            b23 = bY2 || bX3;
            b33 = bY3 || bX3;
            b43 = bY4 || bX3;
            b14 = bY1 || bX4;
            b24 = bY2 || bX4;
            b34 = bY3 || bX4;
            b44 = bY4 || bX4;

            __m256d vectorKX = _mm256_loadu_pd(pTmpHKernelCoefficients);
            __m256d kernel1 = _mm256_mul_pd(vectorKX, vectorKY1);
            __m256d kernel2 = _mm256_mul_pd(vectorKX, vectorKY2);
            __m256d kernel3 = _mm256_mul_pd(vectorKX, vectorKY3);
            __m256d kernel4 = _mm256_mul_pd(vectorKX, vectorKY4);

            for (c = 0; c < channels; c++)
            {
                auto ptr = pSplittedChannels[c];

                ptr11 = ptr + iY1 + x1;
                ptr21 = ptr + iY2 + x1;
                ptr31 = ptr + iY3 + x1;
                ptr41 = ptr + iY4 + x1;
                ptr12 = ptr + iY1 + x2;
                ptr22 = ptr + iY2 + x2;
                ptr32 = ptr + iY3 + x2;
                ptr42 = ptr + iY4 + x2;
                ptr13 = ptr + iY1 + x3;
                ptr23 = ptr + iY2 + x3;
                ptr33 = ptr + iY3 + x3;
                ptr43 = ptr + iY4 + x3;
                ptr14 = ptr + iY1 + x4;
                ptr24 = ptr + iY2 + x4;
                ptr34 = ptr + iY3 + x4;
                ptr44 = ptr + iY4 + x4;

                __m256d dotProducts = _mm256_add_pd(_mm256_add_pd(_mm256_mul_pd(_mm256_set_pd(b14 ? 0.0 : *ptr14,
                                                                                              b13 ? 0.0 : *ptr13,
                                                                                              b12 ? 0.0 : *ptr12,
                                                                                              b11 ? 0.0 : *ptr11),
                                                                                kernel1),
                                                                  _mm256_mul_pd(_mm256_set_pd(b24 ? 0.0 : *ptr24,
                                                                                              b23 ? 0.0 : *ptr23,
                                                                                              b22 ? 0.0 : *ptr22,
                                                                                              b21 ? 0.0 : *ptr21),
                                                                                kernel2)),
                                                    _mm256_add_pd(_mm256_mul_pd(_mm256_set_pd(b34 ? 0.0 : *ptr34,
                                                                                              b33 ? 0.0 : *ptr33,
                                                                                              b32 ? 0.0 : *ptr32,
                                                                                              b31 ? 0.0 : *ptr31),
                                                                                kernel3),
                                                                  _mm256_mul_pd(_mm256_set_pd(b44 ? 0.0 : *ptr44,
                                                                                              b43 ? 0.0 : *ptr43,
                                                                                              b42 ? 0.0 : *ptr42,
                                                                                              b41 ? 0.0 : *ptr41),
                                                                                kernel4)));

                dotProducts = _mm256_hadd_pd(dotProducts, dotProducts);
                *tmpResultPtr = convertDoubleToByte(_mm_cvtsd_f64(_mm_add_pd(_mm256_extractf128_pd(dotProducts, 1),
                                                                             _mm256_castpd256_pd128(dotProducts))));

                tmpResultPtr++;
            }

            pTmpHKernelCoefficients += 4;
        }

        tmpResultPtr += resultOffset;
    }

    // second loop

    pTmpVKernelCoefficients = pVKernelCoefficients + yMin * 4;
    pTmpVKernelY = pVKernelY + yMin;

    int resultOffsetY = yMin * dstWidth * channels;
    int resultOffsetX = xMin * channels;
    int offset;
    resultOffset = (dstWidth - xMax) * channels;

    tmpResultPtr = resultPtr + resultOffsetY;

    int ptrY1 = srcWidth;
    int ptrY2 = srcWidth * 2;
    int ptrY3 = srcWidth * 3;

    for (dY = yMin; dY < yMax; dY++)
    {
        kY1 = *pTmpVKernelCoefficients;
        pTmpVKernelCoefficients++;
        kY2 = *pTmpVKernelCoefficients;
        pTmpVKernelCoefficients++;
        kY3 = *pTmpVKernelCoefficients;
        pTmpVKernelCoefficients++;
        kY4 = *pTmpVKernelCoefficients;
        pTmpVKernelCoefficients++;

        sYStart = *pTmpVKernelY * srcWidth;
        pTmpVKernelY++;

        pTmpHKernelCoefficients = pHKernelCoefficients + xMin * 4;
        pTmpHKernelX = pHKernelX + xMin;

        tmpResultPtr += resultOffsetX;

        __m256d vectorKY1 = _mm256_set_pd(kY1, kY1, kY1, kY1);
        __m256d vectorKY2 = _mm256_set_pd(kY2, kY2, kY2, kY2);
        __m256d vectorKY3 = _mm256_set_pd(kY3, kY3, kY3, kY3);
        __m256d vectorKY4 = _mm256_set_pd(kY4, kY4, kY4, kY4);

        for (dX = xMin; dX < xMax; dX++)
        {
            sXStart = *pTmpHKernelX;
            pTmpHKernelX++;
            offset = sYStart + sXStart;

            __m256d vectorKX = _mm256_loadu_pd(pTmpHKernelCoefficients);
            __m256d kernel1 = _mm256_mul_pd(vectorKX, vectorKY1);
            __m256d kernel2 = _mm256_mul_pd(vectorKX, vectorKY2);
            __m256d kernel3 = _mm256_mul_pd(vectorKX, vectorKY3);
            __m256d kernel4 = _mm256_mul_pd(vectorKX, vectorKY4);

            for (c = 0; c < channels; c++)
            {
                auto ptr = pSplittedChannels[c];
                ptr += offset;

                __m256d dotProducts = _mm256_add_pd(_mm256_add_pd(_mm256_mul_pd(_mm256_loadu_pd(ptr),
                                                                                kernel1),
                                                                  _mm256_mul_pd(_mm256_loadu_pd(ptr + ptrY1),
                                                                                kernel2)),
                                                    _mm256_add_pd(_mm256_mul_pd(_mm256_loadu_pd(ptr + ptrY2),
                                                                                kernel3),
                                                                  _mm256_mul_pd(_mm256_loadu_pd(ptr + ptrY3),
                                                                                kernel4)));

                dotProducts = _mm256_hadd_pd(dotProducts, dotProducts);
                *tmpResultPtr = convertDoubleToByte(_mm_cvtsd_f64(_mm_add_pd(_mm256_extractf128_pd(dotProducts, 1),
                                                                             _mm256_castpd256_pd128(dotProducts))));

                tmpResultPtr++;
            }

            pTmpHKernelCoefficients += 4;
        }

        tmpResultPtr += resultOffset;
    }

    return result;
}

INTERFACE_API void resizeBiCubicInterpolationApi(uint8 const *image,
                                                 const int dstWidth,
                                                 const int dstHeight,
                                                 const int srcWidth,
                                                 const int srcHeight,
                                                 const int srcStride,
                                                 const int srcChannels,
                                                 uint8 *outputImagePtr,
                                                 const int outputImageStride)
{
    std::shared_ptr<uint8> result;

    if (isAVXSupported())
    {
        result = resizeBiCubicInterpolationAVX(image, dstWidth, dstHeight, srcWidth, srcHeight, srcStride, srcChannels);
    }
    else
    {
        result = resizeBiCubicInterpolation(image, dstWidth, dstHeight, srcWidth, srcHeight, srcStride, srcChannels);
    }

    auto imagePtr = result.get();

    auto ptr1 = imagePtr;
    auto ptr2 = outputImagePtr;
    const int offset = outputImageStride - dstWidth * srcChannels;

    for (int y = 0; y < dstHeight; y++)
    {
        for (int x = 0; x < dstWidth * srcChannels; x++)
        {
            *ptr2 = *ptr1;
            ptr1++;
            ptr2++;
        }
        ptr2 += offset;
    }
}

void saveImage(const char *fileName, uint8 *outputImage, int dstWidth, int dstHeight, int channels)
{
    std::fstream file;
    file.open("test-result.csv", std::ios::out);

    for (int y = 0; y < dstHeight; y++)
    {
        for (int x = 0; x < dstWidth; x++)
        {
            file << static_cast<int>(outputImage[y * dstWidth * channels + x * channels]);

            if (x < dstWidth - 1)
            {
                file << ",";
            }
        }
        if (y < dstHeight - 1)
        {
            file << "\n";
        }
    }
}

// for testing purposes only
int main(int argc, char **argv)
{
    std::fstream file;
    file.open("test-image.csv", std::ios::in);

    std::vector<std::vector<int>> matrix;

    int width = -1;
    int height = 0;

    std::string line;
    while (std::getline(file, line))
    {
        int startPos = 0;

        std::vector<int> row;

        int currentWidth = 0;
        int i;
        for (i = 0; i < line.length(); i++)
        {
            if (line[i] == ',')
            {
                if (startPos == i)
                {
                    row.push_back(0);
                }
                else
                {
                    row.push_back(std::stoi(line.substr(startPos, i)));
                }
                startPos = i + 1;
                currentWidth++;
            }
        }

        row.push_back(std::stoi(line.substr(startPos, i)));
        currentWidth++;

        if (width == -1)
        {
            width = currentWidth;
        }
        else
        {
            if (width != currentWidth)
            {
                std::cout << "invalid file" << std::endl;
                file.close();
                return 0;
            }
        }

        matrix.push_back(row);
        height++;
    }
    file.close();

    std::cout << width << std::endl;
    std::cout << height << std::endl;

    uint8 *image = new uint8[height * width];

    for (int y = 0; y < height; y++)
    {
        for (int x = 0; x < width; x++)
        {
            image[y * width + x] = matrix[y][x];
        }
    }

    // std::shared_ptr<uint8> inputImage(image);

    // uint8 *image = new uint8[height * width * 3];

    // for (int y = 0; y < height; y++)
    // {
    //     for (int x = 0; x < width; x++)
    //     {
    //         for (int c = 0; c < 3; c++)
    //         {
    //             image[y * width * 3 + x * 3 + c] = matrix[y][x];
    //         }
    //     }
    // }

    // int width = 1000;
    // int height = 667;
    // int dstWidth = width * 2;
    // int dstHeight = height * 2;
    // uint8 *image = new uint8[width * height * 3];

    // uint8 *imagePtr = image;
    // for (int i = 0; i < width * height * 2; i++)
    // {
    //     *imagePtr = static_cast<uint8>(rand() % 256);
    //     imagePtr++;
    // }

    auto start = high_resolution_clock::now();

    // for (int i = 0; i < 1000; i++)
    // {

    // auto result = gaussianBlur3x3(image, width, height, width);
    // auto result = erode<5, 7>(image, width, height, width);
    auto result = backgroundAdaptiveThreshold(image, width, height, width, 0.75);
    // }

    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<milliseconds>(stop - start);
    // std::cout << ((double)duration.count()) / 1000.0 << std::endl;

    uint8 *outputImage = result.get();
    // saveImage("test-result.csv", outputImage, dstWidth, dstHeight, 3);
    saveImage("test-result.csv", outputImage, width, height, 1);

    delete[] image;
}
