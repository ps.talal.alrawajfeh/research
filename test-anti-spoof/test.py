import cv2
from matplotlib import pyplot as plt
import matplotlib
import numpy as np
import os


def rescale(array):
    rescaled = array.astype(np.float32)

    max_value = np.max(rescaled)
    min_value = np.min(rescaled)
    rescaled = (rescaled - min_value) / (max_value - min_value)

    rescaled *= 255
    rescaled[rescaled > 255] = 255
    rescaled[rescaled < 0] = 0

    return rescaled.astype(np.uint8)


def distances_from_black(image):
    distances = image.astype(np.float32) - np.array([[0.0,
                                                      0.0,
                                                      0.0]],
                                                    np.float32)
    distances = np.square(distances)
    distances = np.sum(distances, axis=-1)
    distances = np.sqrt(distances)
    distances = rescale(distances)

    return distances


def main():
    max_dimension = 1500
    matplotlib.use('TkAgg')

    for i in range(62):
        path = os.path.join('/home/u764/Development/data/ekyc-data/all_passports', f'{i}.jpg')
        if not os.path.isfile(path):
            continue

        image = cv2.imread(path, cv2.IMREAD_COLOR)

        if max(image.shape[0], image.shape[1]) > max_dimension:
            factor = max_dimension / max(image.shape[0], image.shape[1])
            image = cv2.resize(image, None, fx=factor, fy=factor, interpolation=cv2.INTER_CUBIC)

        distances = distances_from_black(image)
        # distances = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        smoothed = cv2.GaussianBlur(distances, (5, 5), 0)

        blackhat = cv2.morphologyEx(smoothed,
                                    cv2.MORPH_BLACKHAT,
                                    np.ones((11, 11), np.float32))
        blackhat = rescale(blackhat)

        binary = cv2.threshold(
            blackhat, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]

        plt.imshow(binary, plt.cm.gray)
        plt.show()


if __name__ == '__main__':
    main()
