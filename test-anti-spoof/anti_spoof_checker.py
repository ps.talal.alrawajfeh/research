import math

import onnxruntime
import numpy as np
import cv2

import dlib
import matplotlib
from matplotlib import pyplot as plt
import sys

FACE_DETECTOR = dlib.get_frontal_face_detector()


def locate_faces(face_image):
    face_locations, confidences, _ = FACE_DETECTOR.run(face_image, 1, 0.0)

    locations = []
    for i in range(len(face_locations)):
        face_location = face_locations[i]
        x = face_location.left()
        y = face_location.top()
        w = face_location.width()
        h = face_location.height()
        locations.append({
            'rectangle': [x, y, w, h],
            'score': confidences[i]
        })

    return locations


def crop_faces(image):
    locations = locate_faces(image)
    if len(locations) == 0:
        return []
    faces = []
    for location in locations:
        x, y, w, h = location['rectangle']
        faces.append(image[y:y + h, x:x + w])
    return faces


def pre_process_face(face, norm_mean, norm_std):
    resized = cv2.resize(face, (128, 128), interpolation=cv2.INTER_CUBIC)
    resized = cv2.cvtColor(resized, cv2.COLOR_BGR2RGB)
    pre_processed = (resized.astype(np.float32) -
                     norm_mean * 255) / (norm_std * 255)
    pre_processed = np.transpose(pre_processed, (2, 0, 1)).astype(np.float32)
    return pre_processed


def feed_forward_onnx_model(onnx_session, pre_processed_image):
    output = onnx_session.run(None,
                              {'actual_input_1': np.expand_dims(pre_processed_image, axis=0)})[0]
    return output


def main():
    matplotlib.use('TkAgg')

    norm_mean = np.array([0.5931, 0.4690, 0.4229], np.float32)
    norm_std = np.array([0.2471, 0.2214, 0.2157], np.float32)

    sess = onnxruntime.InferenceSession('anti-spoof-mn3.onnx')

    image = cv2.imread(sys.argv[1])
    results = []
    for face in crop_faces(image):
        pre_processed = pre_process_face(face, norm_mean, norm_std)
        output = feed_forward_onnx_model(sess, pre_processed)
        spoof_confidence = round(output[0][1] * 100, 2)
        real_confidence = round(output[0][0] * 100, 2)

        results.append((cv2.cvtColor(face, cv2.COLOR_BGR2RGB),
                       spoof_confidence, real_confidence))

    if len(results) > 1:
        fig, axs = plt.subplots(1, len(results))

        for i in range(len(results)):
            axs[i].imshow(results[i][0])
            axs[i].set_title(
                f'Spoof: {results[i][1]}%, Real: {results[i][2]}%')

        plt.show()
    elif len(results) == 1:
        plt.imshow(results[0][0])
        plt.title(f'Spoof: {results[0][1]}%, Real: {results[0][2]}%')
        plt.show()
    else:
        print('No face detected.')


if __name__ == '__main__':
    main()
