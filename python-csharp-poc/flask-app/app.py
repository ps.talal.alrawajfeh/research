from typing import Union

import numpy as np
from fastapi import FastAPI
from pydantic import BaseModel
from forecasting import FourierSeriesForecastingModel

app = FastAPI()


@app.get("/health")
def health():
    return {"status": "alive"}


class RequestBody(BaseModel):
    values: list[float]
    steps: int


@app.post("/forecast")
def forecast(request_body: RequestBody):
    model = FourierSeriesForecastingModel()
    model.fit(
        np.arange(len(request_body.values)).astype(np.float32),
        np.array(request_body.values, np.float32),
    )
    xs = np.arange(
        len(request_body.values), len(request_body.values) + request_body.steps
    ).astype(np.float32)
    return list(model.predict(xs))
