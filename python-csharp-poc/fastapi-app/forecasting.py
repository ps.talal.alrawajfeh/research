import numpy as np
import math

from scipy.linalg import lstsq

def mse(y_true: np.ndarray, y_pred: np.ndarray) -> float:
    return float(np.mean(np.square(y_true - y_pred)))


def dot_product(array1: np.ndarray, array2: np.ndarray) -> np.ndarray:
    return np.sum(array1 * array2)


def sample_autocovariance(time_series: np.ndarray, lag: int) -> float:
    h = abs(lag)
    average = np.mean(time_series)
    sum_of_products = dot_product(
        time_series[h:] - average, time_series[: time_series.shape[0] - h] - average
    )
    return sum_of_products / time_series.shape[0]


def sample_autocorrelation(time_series: np.ndarray, lag: int) -> float:
    return sample_autocovariance(time_series, lag) / sample_autocovariance(
        time_series, 0
    )


def find_seasonality_period(time_series):
    count = time_series.shape[0]
    indices = np.arange(count)
    trend_weights = np.polyfit(indices, time_series, deg=1)
    trend_function = np.poly1d(trend_weights)

    trend = trend_function(indices)
    de_trended_time_series = time_series - trend
    max_period = min(365, int(math.floor(count / 2)))

    acf_values = [
        sample_autocorrelation(de_trended_time_series, i) for i in range(max_period + 1)
    ]
    acf_values = np.array(acf_values)
    acf_values *= np.power(
        np.arange(1, len(acf_values) + 1).astype(np.float64) / len(acf_values), 1 / 4
    )

    if np.argmax(acf_values[1:]) + 1 == len(acf_values) - 1:
        best_period = len(acf_values) - 1
    else:
        new_acf_values = [0]
        for i in range(1, len(acf_values) - 1):
            if acf_values[i] > max(acf_values[i - 1], acf_values[i + 1]):
                new_acf_values.append(acf_values[i])
            else:
                if acf_values[i] < 0:
                    new_acf_values.append(acf_values[i])
                else:
                    new_acf_values.append(-acf_values[i])

        best_period = np.argmax(new_acf_values)

    if best_period == 0:
        best_period = np.argmax(acf_values[1:]) + 1

    return best_period, max_period


def fit_fourier_series_with_linear_trend(series, period_tolerance=0):
    best_period, max_period = find_seasonality_period(series)

    start_period = int(math.floor(best_period * (1 - period_tolerance)))
    end_period = int(math.ceil(best_period * (1 + period_tolerance)))

    indices = np.arange(series.shape[0]).astype(np.float64)
    indices_2_pi = 2 * math.pi * indices

    min_error = None
    best_coefficients = None
    best_period = None
    best_number_of_coefficients = None

    for period in range(start_period, end_period + 1):
        frequencies = np.expand_dims(indices_2_pi / period, axis=0)
        for number_of_coefficients in range(1, period + 1):
            angles = frequencies * np.expand_dims(
                np.arange(number_of_coefficients).astype(np.float64), axis=-1
            )
            design_matrix = np.concatenate(
                [
                    [np.ones(series.shape[0], np.float64), indices.copy()],
                    np.sin(angles),
                    np.cos(angles),
                ],
                axis=0,
            ).transpose()

            try:
                coefficients, res, _, _ = lstsq(design_matrix, series)
                error = np.mean(
                    np.square(np.sum(design_matrix * coefficients, axis=-1) - series)
                )
                if min_error is None or min_error > error:
                    min_error = error
                    best_coefficients = coefficients
                    best_period = period
                    best_number_of_coefficients = number_of_coefficients
            except:
                pass

    return min_error, best_period, best_number_of_coefficients, best_coefficients


class FourierSeriesForecastingModel:
    def __init__(self, period_tolerance=0):
        self.period_tolerance = period_tolerance
        self.period = None
        self.number_of_coefficients = None
        self.coefficients = None
        self.start_index = None

    def fit(self, x, y):
        error, best_period, best_number_of_coefficients, best_coefficients = (
            fit_fourier_series_with_linear_trend(y)
        )
        self.start_index = np.min(x)
        self.period = best_period
        self.number_of_coefficients = best_number_of_coefficients
        self.coefficients = best_coefficients
        return error

    def predict(self, x):
        shifted_x = x - self.start_index
        shifted_x_2_pi = 2 * math.pi * shifted_x
        frequencies = np.expand_dims(shifted_x_2_pi / self.period, axis=0)
        angles = frequencies * np.expand_dims(
            np.arange(self.number_of_coefficients).astype(np.float64), axis=-1
        )
        design_matrix = np.concatenate(
            [
                [np.ones(x.shape[0], np.float64), shifted_x.copy()],
                np.sin(angles),
                np.cos(angles),
            ],
            axis=0,
        ).transpose()
        return np.sum(design_matrix * self.coefficients, axis=-1)
