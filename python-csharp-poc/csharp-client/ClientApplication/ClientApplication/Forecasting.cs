using System.Text;
using System.Text.Json;

namespace ClientApplication;

public class Forecasting
{
    private readonly PythonServerManager _pythonServerManager;

    public Forecasting(PythonServerManager pythonServerManager)
    {
        _pythonServerManager = pythonServerManager;
    }

    public List<float>? Forecast(float[] values, int steps)
    {
        using StringContent requestBody = new(
            JsonSerializer.Serialize(new
            {
                values = values,
                steps = steps
            }),
            Encoding.UTF8,
            "application/json");

        using HttpResponseMessage response = _pythonServerManager.GetHttpClient().PostAsync(
            "forecast",
            requestBody).Result;

        return JsonSerializer.Deserialize<List<float>>(response.Content.ReadAsStringAsync().Result);
    }
}