﻿using ClientApplication;

class Program
{
    static void Main(string[] args)
    {
        PythonServerManager pythonServerManager = new PythonServerManager("localhost",
            8000,
            "app",
            "app",
            "/home/u764/Development/progressoft/research/python-csharp-poc/fastapi-app",
            true,
            Console.WriteLine);

        var forecasting = new Forecasting(pythonServerManager);

        var forecastedValues = forecasting.Forecast(
            new[] { 1.0f, 2.0f, 3.0f, 2.0f, 1.0f, 2.0f, 3.0f, 2.0f, 1.0f },
            10)!;

        foreach (var val in forecastedValues)
        {
            Console.WriteLine(val);
        }

        pythonServerManager.StopServer();
    }
}