using System.Diagnostics;
using System.Net;

namespace ClientApplication;

public class PythonServerManager
{
    private static readonly object StartServerLock = new();

    private const int ServerAliveCheckWaitMillis = 1000;

    private readonly string _host;
    private readonly int _port;
    private readonly string _pythonModuleName;
    private readonly string _pythonApiGlobalVariable;
    private readonly string _serverPath;
    private bool _serverStarted;
    private bool _keepServerAlive;
    private Process _serverProcess = null;
    private Thread _serverThread = null;
    private Thread _keepServerAliveThread = null;
    private HttpClient _httpClient = null;
    private HttpClient _httpsClient = null;
    private readonly Action<string?> _logConsumer;

    public PythonServerManager(
        string host,
        int port,
        string pythonModuleName,
        string pythonApiGlobalVariable,
        string serverPath,
        bool keepServerAlive,
        Action<string?> logConsumer)
    {
        _host = host;
        _port = port;
        _pythonModuleName = pythonModuleName;
        _serverPath = serverPath;
        _logConsumer = logConsumer;
        _pythonApiGlobalVariable = pythonApiGlobalVariable;

        StartServerProcessAndThread();
        _keepServerAlive = keepServerAlive;
        if (_keepServerAlive)
        {
            _keepServerAliveThread = new Thread(KeepServerAliveLoop);
            _keepServerAliveThread.Start();
        }
    }

    public HttpClient GetHttpClient(bool isHttps = false)
    {
        if (isHttps && _httpsClient == null)
        {
            _httpsClient = new()
            {
                BaseAddress = new Uri($"https://{_host}:{_port}"),
            };
        }

        if (!isHttps && _httpClient == null)
        {
            _httpClient = new()
            {
                BaseAddress = new Uri($"http://{_host}:{_port}"),
            };
        }

        return isHttps ? _httpsClient : _httpClient;
    }

    public bool IsServerAlive()
    {
        try
        {
            var httpResponseMessage = GetHttpClient().GetAsync("health").Result;
            return httpResponseMessage.StatusCode == HttpStatusCode.OK;
        }
        catch (Exception)
        {
            return false;
        }
    }

    public void StopServer()
    {
        if (_serverStarted)
        {
            StopKeepServerAliveThread();
            StopServerProcessAndThread();
        }
    }

    private void StartServerProcessAndThread(bool isBackground = false)
    {
        lock (StartServerLock)
        {
            if (!_serverStarted)
            {
                _serverThread = new Thread(StartPythonServer)
                {
                    IsBackground = isBackground
                };
                _serverThread.Start();
                EnsureServerStarted();
                _serverStarted = true;
            }
        }
    }

    private void StartPythonServer()
    {
        var startCommand = $"\"uvicorn {_pythonModuleName}:{_pythonApiGlobalVariable} --host {_host} --port {_port}\"";

        Environment.SetEnvironmentVariable("PYTHONPATH", _serverPath);

        string shell = "";
        string commandArgument = "";
        if (OperatingSystem.IsWindows())
        {
            shell = "cmd.exe";
            commandArgument = "/c";
        }
        else if (OperatingSystem.IsLinux())
        {
            shell = "/usr/bin/bash";
            commandArgument = "-c";
        }
        else
        {
            throw new Exception("Current operating system is not supported.");
        }

        ProcessStartInfo startInfo = new ProcessStartInfo
        {
            FileName = shell,
            Arguments = $"{commandArgument} {startCommand}",
            UseShellExecute = false,
            RedirectStandardOutput = true,
            CreateNoWindow = true
        };

        _serverProcess = new Process();
        _serverProcess.StartInfo = startInfo;
        _serverProcess.Start();

        while (!_serverProcess.StandardOutput.EndOfStream)
        {
            string? line = _serverProcess.StandardOutput.ReadLine();
            _logConsumer(line);
        }
    }

    private void KeepServerAliveLoop()
    {
        while (true)
        {
            if (!_keepServerAlive)
                break;

            if (!IsServerAlive())
            {
                StopServerProcessAndThread();
                StartServerProcessAndThread();
            }

            Thread.Sleep(ServerAliveCheckWaitMillis);
        }
    }

    private void EnsureServerStarted()
    {
        while (!IsServerAlive())
        {
        }
    }

    private void StopServerProcessAndThread()
    {
        lock (StartServerLock)
        {
            try
            {
                _serverProcess.Kill();
            }
            catch (Exception)
            {
            }

            try
            {
                _serverProcess.Dispose();
            }
            catch (Exception)
            {
            }

            try
            {
                _serverThread.Join();
            }
            catch (Exception)
            {
            }

            _serverProcess = null;
            _serverThread = null;
            _serverStarted = false;
        }
    }

    private void StopKeepServerAliveThread()
    {
        if (_keepServerAlive)
        {
            _keepServerAlive = false;

            try
            {
                _keepServerAliveThread.Join();
            }
            catch (Exception)
            {
            }

            _keepServerAliveThread = null;
        }
    }
}