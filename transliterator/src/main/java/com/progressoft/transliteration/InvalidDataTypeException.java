package com.progressoft.transliteration;

public class InvalidDataTypeException extends RuntimeException {
    public InvalidDataTypeException(String s) {
        super(s);
    }

    public InvalidDataTypeException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
