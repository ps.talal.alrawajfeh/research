package com.progressoft.transliteration;

import java.util.Map;

public interface Transliteration {
    String transliterate(String string);

    String transliterateWord(String word);
}
