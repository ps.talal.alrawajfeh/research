package com.progressoft.transliteration;

public class JsonParsingException extends RuntimeException {
    public JsonParsingException(String s) {
        super(s);
    }

    public JsonParsingException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
