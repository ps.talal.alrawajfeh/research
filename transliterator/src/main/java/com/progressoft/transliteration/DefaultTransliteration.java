package com.progressoft.transliteration;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.IntStream;

public class DefaultTransliteration implements Transliteration {
    private Map<String, ArrayList<String>> characterMap = new HashMap<>();
    private Map<String, String> wordsMap = new HashMap<>();

    public DefaultTransliteration(String jsonFileName) {
        this(jsonFileName, null);
    }

    public DefaultTransliteration(String jsonCharactersFileName, String jsonPredefinedWordsFileName) {
        fillCharacterMap(parseJsonFile(Paths.get(getResource(jsonCharactersFileName).getPath())).toMap());
        //------- same scenario for words
        if (Objects.nonNull(jsonPredefinedWordsFileName)) {
            fillWordsMap(parseJsonFile(Paths.get(getResource(jsonPredefinedWordsFileName).getPath())).toMap());
        }
    }

    private URL getResource(String resourceName) {
        URL resource = Thread.currentThread().getContextClassLoader().getResource(resourceName);
        if (Objects.isNull(resource)) {
            throw new IllegalArgumentException("the json file of name " + resourceName + " could not be found");
        }
        return resource;
    }

    public DefaultTransliteration(Path jsonFilePath) {
        this(jsonFilePath, null);
    }

    public DefaultTransliteration(Path jsonFilePath, Path jsonPredefinedPath) {
        ensurePathExists(jsonFilePath);
        fillCharacterMap(parseJsonFile(jsonFilePath).toMap());
        if (Objects.nonNull(jsonPredefinedPath)) {
            ensurePathExists(jsonPredefinedPath);
            fillWordsMap(parseJsonFile(jsonPredefinedPath).toMap());
        }
    }

    private void ensurePathExists(Path path) {
        if (!Files.exists(path)) {
            throw new IllegalArgumentException("the json file of path " + path + " could not be found");
        }
    }

    private JSONObject parseJsonFile(Path path) {
        try {
            String jsonContent = String.join("", Files.readAllLines(path));
            return new JSONObject(jsonContent);
        } catch (IOException | JSONException e) {
            throw new JsonParsingException("an error occurred while parsing the json file", e);
        }
    }

    private void fillCharacterMap(Map<String, Object> characterMap) {
        characterMap.forEach((key, value) -> {
            if (value instanceof ArrayList) {
                ensureArrayContainsStringsOnly((ArrayList) value);
                this.characterMap.put(key, (ArrayList<String>) value);
            } else {
                throw new InvalidDataTypeException("json attribute value must be an array of strings");
            }
        });
    }

    private void fillWordsMap(Map<String, Object> wordsMap) {
        wordsMap.forEach((key, value) -> {
            if (value instanceof String) {
                this.wordsMap.put(key, (String) value);
            } else {
                throw new InvalidDataTypeException("json attribute value must be string");
            }
        });
    }

    private void ensureArrayContainsStringsOnly(ArrayList list) {
        list.forEach(item -> {
            if (!(item instanceof String)) {
                throw new InvalidDataTypeException("json attribute value must be an array of strings");
            }
        });
    }

    @Override
    public String transliterateWord(String word) {
        if (wordsMap.containsKey(word))
            return wordsMap.get(word);
        final StringBuilder result = new StringBuilder();
        IntStream.range(0, word.length()).forEach(i -> {
            String character = Character.toString(word.charAt(i));
            if (!characterMap.containsKey(character)) {
                result.append(character);
            } else {

                result.append(getCharacterMapping(word, character, i));
            }
        });
        return result.toString();
    }

    private String getCharacterMapping(String word, String character, int index) {
        ArrayList<String> characterMappings = characterMap.get(character);
        if (characterMappings.size() != 1 && characterMappings.size() != 3)
            throw new TransliterationException("incorrect json array length, each array must contain either one or three strings");
        if (characterMappings.size() > 1) {
            if (index == 0) {
                return characterMappings.get(0);
            } else if (index < word.length() - 1) {
                return characterMappings.get(1);
            } else {
                return characterMappings.get(2);
            }
        } else {
            return characterMappings.get(0);
        }
    }

    @Override
    public String transliterate(String string) {
        StringBuilder result = new StringBuilder();
        int start = 0;
        int end = 0;
        for (; end < string.length(); end++) {
            if (Character.isWhitespace(string.charAt(end))) {
                result.append(transliterateWord(string.substring(start, end))); //append word after transliterate
                result.append(string.charAt(end)); // append white space
                start = end + 1;
            }
        }
        result.append(transliterateWord(string.substring(start, end)));
        return result.toString();
    }

    public static void main(String[] args) {
        DefaultTransliteration t = new DefaultTransliteration(Paths.get("/home/u764/Downloads/transliterator/src/main/resources/ar_en_character_map.json"),
                Paths.get("/home/u764/Downloads/transliterator/src/main/resources/ar_en_word_map.json"));

        System.out.println(t.transliterate("شارع طلال"));

    }
}
