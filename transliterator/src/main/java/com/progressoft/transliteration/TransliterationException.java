package com.progressoft.transliteration;

public class TransliterationException extends RuntimeException {
    public TransliterationException(String s) {
        super(s);
    }

    public TransliterationException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
