package com.progressoft.transliteration;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class TransliterationTests {
    @Test()
    void whenConstructingTransliteration_givenNonExistingJsonFileName_thenShouldThrowIllegalArgumentException() {
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> new DefaultTransliteration("dummy"));
        Assertions.assertEquals("the json file of name dummy could not be found", exception.getMessage());
    }

    @Test()
    void whenConstructingTransliteration_givenNonExistingJsonFilePath_thenShouldThrowIllegalArgumentException() {
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> new DefaultTransliteration(Paths.get("./dummy/dummy")));
        Assertions.assertEquals("the json file of path ./dummy/dummy could not be found", exception.getMessage());
    }

    @Test
    void whenConstructingTransliteration_givenExistingJsonFileName_thenShouldSucceed() {
        Assertions.assertDoesNotThrow(() -> new DefaultTransliteration("ar_en_character_map.json"));
    }

    @Test
    void whenConstructingTransliteration_givenExistingJsonFilePath_thenShouldSucceed() {
        Assertions.assertDoesNotThrow(() -> new DefaultTransliteration(Paths.get("./src/test/resources/ArEnCharacterMapTest.json")));
    }


    @Test
    void whenConstructingTransliteration_givenInvalidJsonFile_thenShouldThrowJsonParingException() {
        JsonParsingException exception = Assertions.assertThrows(JsonParsingException.class, () -> new DefaultTransliteration("invalidJsonTest.json"));
        Assertions.assertEquals("an error occurred while parsing the json file", exception.getMessage());
    }


    @Test
    void whenConstructingTransliteration_givenJsonFileWithAttributeValuesOfTypeOtherThanArrayOfStrings_thenShouldThrowJsonParingException() {
        InvalidDataTypeException exception1 = Assertions.assertThrows(InvalidDataTypeException.class, () -> new DefaultTransliteration("invalidDataTypeTest1.json"));
        Assertions.assertEquals("json attribute value must be an array of strings", exception1.getMessage());
        InvalidDataTypeException exception2 = Assertions.assertThrows(InvalidDataTypeException.class, () -> new DefaultTransliteration("invalidDataTypeTest2.json"));
        Assertions.assertEquals("json attribute value must be an array of strings", exception2.getMessage());
    }

    @Test
    void whenTransliteratingWord_givenJsonFileWithEmptyJson_thenShouldReturnTheSameWord() {
        String wordTest = "mutaz";
        DefaultTransliteration transliteration = new DefaultTransliteration("emptyJsonTest.json");
        Assertions.assertEquals(wordTest, transliteration.transliterateWord(wordTest));
    }


    @Test
    void whenTransliteratingWord_givenJsonFileWithCharacterMappings_thenShouldReturnEachCharacterReplacedWithTheCorrespondingMappedCharacter() {
        String wordTest = "باب";
        DefaultTransliteration transliteration = new DefaultTransliteration("simpleTransliterationTest.json");
        String result = transliteration.transliterateWord(wordTest);
        Assertions.assertEquals("bab", result);
    }


    @Test
    void whenTransliteratingWord_givenMultipleCharacterMappings_thenEachCharacterMappingShouldCorrespondToCharacterPositionInWord() {
        String beginningOfWord = "عمر";
        String middleOfWord = "سعد";
        String endOfWord = "شارع";

        DefaultTransliteration transliteration = new DefaultTransliteration("simpleTransliterationTest.json");

        Assertions.assertEquals("oمر", transliteration.transliterateWord(beginningOfWord));
        Assertions.assertEquals("سaد", transliteration.transliterateWord(middleOfWord));
        Assertions.assertEquals("شaرea", transliteration.transliterateWord(endOfWord));
    }


    @Test
    void whenTransliteratingWord_givenIncorrectNumberOfCharacterMappings_thenShouldThrowTransliterationException() {
        DefaultTransliteration transliteration = new DefaultTransliteration("simpleTransliterationTest.json");
        TransliterationException exception = Assertions.assertThrows(TransliterationException.class, () -> transliteration.transliterateWord("فارغ"));
        Assertions.assertEquals("incorrect json array length, each array must contain either one or three strings", exception.getMessage());
        exception = Assertions.assertThrows(TransliterationException.class, () -> transliteration.transliterateWord("قاعدة"));
        Assertions.assertEquals("incorrect json array length, each array must contain either one or three strings", exception.getMessage());
    }

    @Test
    void whenTransliteratingString_givenOneWord_thenShouldReturnTransliteratedWord() {
        DefaultTransliteration transliteration = new DefaultTransliteration("ar_en_character_map.json");
        Assertions.assertEquals("majd", transliteration.transliterate("ماجد"));
    }

    @Test
    void whenTransliteratingString_givenTwoWordsWithSpaceInBetween_thenShouldReturnTwoTransliteratedWordsWithSpaceInBetween() {
        DefaultTransliteration transliteration = new DefaultTransliteration("ar_en_character_map.json");
        Assertions.assertEquals("yazn samay", transliteration.transliterate("يزن سامي"));
    }


    @Test
    void whenTransliteratingString_givenTabInString_thenShouldTransliterateWordsPreservingTab() {
        DefaultTransliteration transliteration = new DefaultTransliteration("ar_en_character_map.json");
        Assertions.assertEquals("aazeezay almshtrk \t yarja iaaadah shhn khtk", transliteration.transliterate("عزيزي المشترك \t يرجى إعادة شحن خطك"));
    }

    @Test
    void whenTransliteratingString_givenNewLineInString_thenShouldTransliterateWordsPreservingNewLine() {
        DefaultTransliteration transliteration = new DefaultTransliteration("ar_en_character_map.json");
        Assertions.assertEquals("aazeezay almshtrk \n yarja iaaadah shhn khtk", transliteration.transliterate("عزيزي المشترك \n يرجى إعادة شحن خطك"));
    }

    @Test
    void whenTransliteratingString_givenSpacesAtTheBeginningOfTheString_thenShouldTransliterateWordsPreservingInitialSpaces() {
        DefaultTransliteration transliteration = new DefaultTransliteration("ar_en_character_map.json");
        Assertions.assertEquals("  hello from test", transliteration.transliterate("  hello from test"));
    }

    @Test
    void whenTransliteratingString_givenSpacesAtTheEndOfTheString_thenShouldTransliterateWordsPreservingTrailingSpaces() {
        DefaultTransliteration transliteration = new DefaultTransliteration("ar_en_character_map.json");
        Assertions.assertEquals("hello      ", transliteration.transliterate("hello      "));
    }


    @Test
    void whenTransliteratingString_givenPredefinedWordMap_thenShouldReplaceWordsExistingInPredefinedWordMapAndTransliterateAllOtherWords() {
        String string1 = "شارع ابو الطل";
        String string2 = "شارع عمر";

        DefaultTransliteration transliteration = new DefaultTransliteration("ArEnCharacterMapTest.json","ArEnWordMapTest.json");
        String result1 = transliteration.transliterate(string1);
        String result2 = transliteration.transliterate(string2);

        Assertions.assertEquals("Share'a abu altl", result1);
        Assertions.assertEquals("Share'a Omar", result2);
    }
}
