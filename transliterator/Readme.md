# Transliteration

## Description

The transliteration tool converts characters from one language to characters to another using a json mapping.

### Example
The string `"شارع"` could be converted to the string `"sharaa"` via this mapping:
```json
{
    "ش": ["sh"],
    "ا": ["a"],
    "ر": ["r"],
    "غ": ["aa"]
}
```

## Usage

To transliterate a word, first you need to define your character mappings in a json file.
There are three rules for writing your character mappings:

1. Json keys should contain the characters from the **source** language and the values should always be arrays of strings containing characters of the **destination** language.
2. If a character from the source language always maps to exactly one combination of characters of the destination language,
then the array should contain only one string.
3. If a character is mapped differently according to its position in the word (at the beginning, at the middle, and at the end),
then the array should contain three strings each corresponding to the position of the character in a word respectively.
For example:


```json
{
    "و": ["wa", "oo", "aw"]
}
```

According to the above json, if the character `و` came at a beginning of a word, then it will be mapped to `wa`.
If it came in the middle of a word then it will be mapped to `oo`, and if it came as the last character in a word then it will be mapped to `aw`.

After the json file is done, construct an instance of `DefaultTransliteration` with either the path of the json file or the name of the file if it exists under `classpath`.

```java
new DefaultTransliteration(Paths.get("/path/to/json"));
```

To transliterate a string call the `transliterate` method.

```java
new DefaultTransliteration(Paths.get("/path/to/json")).transliterate("شارع الأردن");
```

If there are some special cases that cannot be converted using the character mappings, you can use the other constructor by giving it another json file with words of the source language mapped to the destination language.
For example:

```json
{
  "شارع": "Share'a",
  "عمر": "Omar",
  "عماد": "Emad"
}
```


```java
new DefaultTransliteration(Paths.get("/path/to/character/map/json"), Pahts.get("/path/to/word/map/json")).transliterate("شارع الأردن");
```

Similarly if the two json files exist under `classpath` you can use the file names instead.