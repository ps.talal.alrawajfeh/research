public partial class Form1 : Form
{
private const double FloatEpsilon = 1e-5;

string imagePath = string.Empty;
private OpenCvSharp.Point[] mCornerPoints = null;
private OpenCvSharp.Point mPointTopLeft;
private OpenCvSharp.Point mPointTopRight;
private OpenCvSharp.Point mPointBottomLeft;
private OpenCvSharp.Point mPointBottomRight;
private OpenCvSharp.Point mCenterOfMass;
private Mat mOriginalPciture = null;
private const double ratioH2W = 3.5f / 7.5f;

private Bitmap cornerLocation = null;

public static int Floor(double value)
{
    var floor = (int)value;
    return floor > value ? floor - 1 : floor;
}

public static double InterpolateCubic(double x)
{
    const double a = -0.75;

    var fx = Math.Abs(x);

    return fx >= 0 && fx <= 1
        ? ((a + 2) * fx - (a + 3)) * fx * fx + 1
        : fx > 1 && fx <= 2
            ? ((a * fx - 5 * a) * fx + 8 * a) * fx - 4 * a
            : 0.0;
}

public static unsafe void UpdateBackgroundColor(ref Bitmap inputBitmap, Color color)
{
    int width = inputBitmap.Width;
    int height = inputBitmap.Height;
    BitmapData outBmpData = inputBitmap.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, inputBitmap.PixelFormat);
    byte* pStride = (byte*)outBmpData.Scan0.ToPointer();
    int stride = outBmpData.Stride;
    byte[] strideLine = Enumerable.Range(0, stride).Select(_ => color.R).ToArray();
    for (int y = 0; y < height; y++, pStride += stride)
        Marshal.Copy(strideLine, 0, new IntPtr(pStride), stride);
    inputBitmap.UnlockBits(outBmpData);
}

public static ColorPalette CreateGrayPalette()
{
    ColorPalette colorPalette = Create(PixelFormat.Format8bppIndexed);
    for (int i = 0; i < 256; i++)
        colorPalette.Entries[i] = Color.FromArgb(255, i, i, i);
    return colorPalette;
}

private static ColorPalette Create(PixelFormat pixelFormat)
{
    Bitmap bitmap = new Bitmap(1, 1, pixelFormat);
    ColorPalette palette = bitmap.Palette;
    bitmap.Dispose();
    return palette;
}

public static unsafe Bitmap ResizeBiCubicInterpolation(Bitmap image, int dstWidth, int dstHeight)
{
    Bitmap result = null;
    BitmapData resultData = null;
    BitmapData imageData = null;

    try
    {
        int srcWidth = image.Width;
        int srcHeight = image.Height;

        result = BitmapFactory.CreateImage(dstWidth, dstHeight, image.PixelFormat);

        imageData = image.LockBits(new Rectangle(0, 0, srcWidth, srcHeight),
            ImageLockMode.ReadWrite,
            image.PixelFormat);
        resultData = result.LockBits(new Rectangle(0, 0, dstWidth, dstHeight),
            ImageLockMode.ReadWrite,
            result.PixelFormat);

        int channels = Math.Max(1, Image.GetPixelFormatSize(imageData.PixelFormat) / 8);

        double scaleX = (double) dstWidth / (double) srcWidth;
        double scaleY = (double) dstHeight / (double) srcHeight;

        byte* imagePtr = (byte*) (void*) imageData.Scan0;
        byte* resultPtr = (byte*) (void*) resultData.Scan0;

        double invScaleX = 1.0 / scaleX;
        double invScaleY = 1.0 / scaleY;

        int imageStride = imageData.Stride;
        int resultStride = resultData.Stride;
        int resultOffset = resultStride - dstWidth * channels;

        double[] hKernelCoefficients = new double[dstWidth * 4];
        int[] hKernelX = new int[dstWidth];

        double[] vKernelCoefficients = new double[dstHeight * 4];
        int[] vKernelY = new int[dstHeight];

        int dY, dX, c;
        double sX, sY;
        double dX1, dX2, dX3, dX4, dY1, dY2, dY3, dY4;
        double kX1, kX2, kX3, kX4, kY1, kY2, kY3, kY4;
        int iX1, iX2, iX3, iX4, iY1, iY2, iY3, iY4;
        int x1, x2, x3, x4, y1, y2, y3, y4;

        double k11,
            k12,
            k13,
            k14,
            k21,
            k22,
            k23,
            k24,
            k31,
            k32,
            k33,
            k34,
            k41,
            k42,
            k43,
            k44;

        byte* ptr11,
            ptr12,
            ptr13,
            ptr14,
            ptr21,
            ptr22,
            ptr23,
            ptr24,
            ptr31,
            ptr32,
            ptr33,
            ptr34,
            ptr41,
            ptr42,
            ptr43,
            ptr44;

        double v, diffV;
        int intV;
        int sXStart, sYStart;

        fixed (double* pVKernelCoefficients = &vKernelCoefficients[0])
        {
            fixed (int* pVKernelY = &vKernelY[0])
            {
                fixed (double* pHKernelCoefficients = &hKernelCoefficients[0])
                {
                    fixed (int* pHKernelX = &hKernelX[0])
                    {
                        double* pTmpVKernelCoefficients = pVKernelCoefficients;
                        int* pTmpVKernelY = pVKernelY;

                        double* pTmpHKernelCoefficients = pHKernelCoefficients;
                        int* pTmpHKernelX = pHKernelX;

                        for (int x = 0; x < dstWidth; x++)
                        {
                            sX = (x + 0.5) * invScaleX - 0.5;
                            var sXFloor = Floor(sX);

                            dX1 = 1 + sX - sXFloor;
                            dX2 = sX - sXFloor;
                            dX3 = sXFloor + 1 - sX;
                            dX4 = sXFloor + 2 - sX;

                            *pTmpHKernelCoefficients = InterpolateCubic(dX1);
                            pTmpHKernelCoefficients++;
                            *pTmpHKernelCoefficients = InterpolateCubic(dX2);
                            pTmpHKernelCoefficients++;
                            *pTmpHKernelCoefficients = InterpolateCubic(dX3);
                            pTmpHKernelCoefficients++;
                            *pTmpHKernelCoefficients = InterpolateCubic(dX4);
                            pTmpHKernelCoefficients++;

                            *pTmpHKernelX = sXFloor - 1;
                            pTmpHKernelX++;
                        }

                        for (int y = 0; y < dstHeight; y++)
                        {
                            sY = (y + 0.5) * invScaleY - 0.5;
                            var sYFloor = Floor(sY);

                            dY1 = 1 + sY - sYFloor;
                            dY2 = sY - sYFloor;
                            dY3 = sYFloor + 1 - sY;
                            dY4 = sYFloor + 2 - sY;

                            *pTmpVKernelCoefficients = InterpolateCubic(dY1);
                            pTmpVKernelCoefficients++;
                            *pTmpVKernelCoefficients = InterpolateCubic(dY2);
                            pTmpVKernelCoefficients++;
                            *pTmpVKernelCoefficients = InterpolateCubic(dY3);
                            pTmpVKernelCoefficients++;
                            *pTmpVKernelCoefficients = InterpolateCubic(dY4);
                            pTmpVKernelCoefficients++;

                            *pTmpVKernelY = sYFloor - 1;
                            pTmpVKernelY++;
                        }

                        pTmpVKernelCoefficients = pVKernelCoefficients;
                        pTmpVKernelY = pVKernelY;

                        for (dY = 0; dY < dstHeight; dY++)
                        {
                            kY1 = *pTmpVKernelCoefficients;
                            pTmpVKernelCoefficients++;
                            kY2 = *pTmpVKernelCoefficients;
                            pTmpVKernelCoefficients++;
                            kY3 = *pTmpVKernelCoefficients;
                            pTmpVKernelCoefficients++;
                            kY4 = *pTmpVKernelCoefficients;
                            pTmpVKernelCoefficients++;

                            sYStart = *pTmpVKernelY;
                            pTmpVKernelY++;

                            y1 = sYStart;
                            y2 = sYStart + 1;
                            y3 = sYStart + 2;
                            y4 = sYStart + 3;

                            y1 = Math.Max(Math.Min(y1, srcHeight - 1), 0);
                            y2 = Math.Max(Math.Min(y2, srcHeight - 1), 0);
                            y3 = Math.Max(Math.Min(y3, srcHeight - 1), 0);
                            y4 = Math.Max(Math.Min(y4, srcHeight - 1), 0);

                            iY1 = y1 * imageStride;
                            iY2 = y2 * imageStride;
                            iY3 = y3 * imageStride;
                            iY4 = y4 * imageStride;

                            pTmpHKernelCoefficients = pHKernelCoefficients;
                            pTmpHKernelX = pHKernelX;

                            for (dX = 0; dX < dstWidth; dX++)
                            {
                                kX1 = *pTmpHKernelCoefficients;
                                pTmpHKernelCoefficients++;
                                kX2 = *pTmpHKernelCoefficients;
                                pTmpHKernelCoefficients++;
                                kX3 = *pTmpHKernelCoefficients;
                                pTmpHKernelCoefficients++;
                                kX4 = *pTmpHKernelCoefficients;
                                pTmpHKernelCoefficients++;

                                sXStart = *pTmpHKernelX;
                                pTmpHKernelX++;

                                x1 = sXStart;
                                x2 = sXStart + 1;
                                x3 = sXStart + 2;
                                x4 = sXStart + 3;

                                x1 = Math.Max(Math.Min(x1, srcWidth - 1), 0);
                                x2 = Math.Max(Math.Min(x2, srcWidth - 1), 0);
                                x3 = Math.Max(Math.Min(x3, srcWidth - 1), 0);
                                x4 = Math.Max(Math.Min(x4, srcWidth - 1), 0);

                                iX1 = x1 * channels;
                                iX2 = x2 * channels;
                                iX3 = x3 * channels;
                                iX4 = x4 * channels;

                                k11 = kY1 * kX1;
                                k21 = kY2 * kX1;
                                k31 = kY3 * kX1;
                                k41 = kY4 * kX1;
                                k12 = kY1 * kX2;
                                k22 = kY2 * kX2;
                                k32 = kY3 * kX2;
                                k42 = kY4 * kX2;
                                k13 = kY1 * kX3;
                                k23 = kY2 * kX3;
                                k33 = kY3 * kX3;
                                k43 = kY4 * kX3;
                                k14 = kY1 * kX4;
                                k24 = kY2 * kX4;
                                k34 = kY3 * kX4;
                                k44 = kY4 * kX4;

                                ptr11 = imagePtr + iY1 + iX1;
                                ptr21 = imagePtr + iY2 + iX1;
                                ptr31 = imagePtr + iY3 + iX1;
                                ptr41 = imagePtr + iY4 + iX1;
                                ptr12 = imagePtr + iY1 + iX2;
                                ptr22 = imagePtr + iY2 + iX2;
                                ptr32 = imagePtr + iY3 + iX2;
                                ptr42 = imagePtr + iY4 + iX2;
                                ptr13 = imagePtr + iY1 + iX3;
                                ptr23 = imagePtr + iY2 + iX3;
                                ptr33 = imagePtr + iY3 + iX3;
                                ptr43 = imagePtr + iY4 + iX3;
                                ptr14 = imagePtr + iY1 + iX4;
                                ptr24 = imagePtr + iY2 + iX4;
                                ptr34 = imagePtr + iY3 + iX4;
                                ptr44 = imagePtr + iY4 + iX4;

                                for (c = 0; c < channels; c++)
                                {
                                    v = k11 * (ptr11 + c)[0] +
                                        k21 * (ptr21 + c)[0] +
                                        k31 * (ptr31 + c)[0] +
                                        k41 * (ptr41 + c)[0] +
                                        k12 * (ptr12 + c)[0] +
                                        k22 * (ptr22 + c)[0] +
                                        k32 * (ptr32 + c)[0] +
                                        k42 * (ptr42 + c)[0] +
                                        k13 * (ptr13 + c)[0] +
                                        k23 * (ptr23 + c)[0] +
                                        k33 * (ptr33 + c)[0] +
                                        k43 * (ptr43 + c)[0] +
                                        k14 * (ptr14 + c)[0] +
                                        k24 * (ptr24 + c)[0] +
                                        k34 * (ptr34 + c)[0] +
                                        k44 * (ptr44 + c)[0];

                                    v = Math.Min(255.0, Math.Max(0.0, v));
                                    intV = (int) v;
                                    diffV = v - intV;

                                    resultPtr[0] =
                                        (byte) (uint) (diffV > 0.5 || diffV == 0.5 && (intV & 1) != 0
                                            ? intV + 1
                                            : intV);

                                    resultPtr++;
                                }
                            }

                            resultPtr += resultOffset;
                        }
                    }
                }
            }
        }

        return result;
    }
    catch
    {
        return result;
    }
    finally
    {
        if (imageData != null)
        {
            image.UnlockBits(imageData);
        }

        if (result != null && resultData != null)
        {
            result.UnlockBits(resultData);
        }
    }
}


public static unsafe Bitmap OpenCvOtsu(Bitmap inputImage)
{
    try
    {
        if (inputImage.PixelFormat != PixelFormat.Format8bppIndexed) return null;

        var outputImage = (Bitmap)inputImage.Clone();
        int height = outputImage.Height;
        int width = outputImage.Width;

        var outputImageData = outputImage.LockBits(new Rectangle(0, 0, width, height),
            ImageLockMode.ReadWrite,
            outputImage.PixelFormat);

        int stride = outputImageData.Stride;
        int offset = stride - outputImage.Width;
        var scan0 = outputImageData.Scan0;
        byte* outputImageDataPtr = (byte*)(void*)scan0;

        var histogram = new int[256];
        for (var y = 0; y < height; y++)
        {
            for (var x = 0; x < width; x++)
            {
                int i = outputImageDataPtr[0];
                histogram[i]++;
                outputImageDataPtr++;
            }

            outputImageDataPtr += offset;
        }

        double scale = 1.0 / (height * width);
        double mu = histogram.Select((x, i) => x * i).Sum() * scale;

        double q1 = 0;
        var maxVal = 0;
        double maxSigma = -1;
        double mu1 = 0;

        for (int t = 0; t < 256; t++)
        {
            var pt = histogram[t] * scale;
            mu1 *= q1;
            q1 += pt;
            var q2 = 1.0 - q1;

            if (Math.Min(q1, q2) < FloatEpsilon || Math.Max(q1, q2) > 1.0 - FloatEpsilon)
            {
                continue;
            }

            mu1 = (mu1 + t * pt) / q1;
            double mu2 = (mu - q1 * mu1) / q2;

            var sigma = q1 * q2 * (mu1 - mu2) * (mu1 - mu2);
            if (sigma > maxSigma)
            {
                maxSigma = sigma;
                maxVal = t;
            }
        }

        outputImageDataPtr = (byte*)(void*)scan0;
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                outputImageDataPtr[0] = outputImageDataPtr[0] > maxVal ? (byte)255 : (byte)0;
                outputImageDataPtr++;
            }

            outputImageDataPtr += offset;
        }

        outputImage.UnlockBits(outputImageData);
        return outputImage;
    }
    catch
    {
        return null;
    }
}

public Form1()
{
    var x = new Bitmap(@"C:\Users\USER\Desktop\AutoEncoder\test.bmp");
    var y = ResizeBiCubicInterpolation(x, 748, 200);
    y.Save(@"C:\Users\USER\Desktop\AutoEncoder\test-net-framework.bmp", ImageFormat.Bmp);
    InitializeComponent();
}


private Bitmap getBoundariesAndCrop(Bitmap bmp)
{
    int xInt, yInt, xEnd, yEnd;
    xInt = yInt = xEnd = yEnd = 0;
    GenericProcessing gp = new GenericProcessing();
    gp.GetBoundaries2(bmp, ref xInt, ref yInt, ref xEnd, ref yEnd, 1);
    gp.Crop(bmp, xInt, yInt, xEnd, yEnd, ref bmp);
    return bmp;
}

public Bitmap RotateBitmap(Bitmap bmp, float angle)
{
    double radianAngle = angle / 180.0 * Math.PI;
    double cosA = Math.Abs(Math.Cos(radianAngle));
    double sinA = Math.Abs(Math.Sin(radianAngle));

    int newWidth = (int)(cosA * bmp.Width + sinA * bmp.Height);
    int newHeight = (int)(cosA * bmp.Height + sinA * bmp.Width);

    var rotatedBitmap = new Bitmap(newWidth, newHeight);
    rotatedBitmap.SetResolution(bmp.HorizontalResolution, bmp.VerticalResolution);

    using (Graphics g = Graphics.FromImage(rotatedBitmap))
    {
        g.Clear(Color.White);
        g.TranslateTransform(rotatedBitmap.Width / 2, rotatedBitmap.Height / 2);
        g.RotateTransform(angle);
        g.TranslateTransform(-bmp.Width / 2, -bmp.Height / 2);
        g.DrawImage(bmp, new System.Drawing.Point(0, 0));
    }

    return rotatedBitmap;
}

private List<int> getRandomShuffledIndices(int count)
{
    Random random = new Random((int)DateTime.Now.Ticks);
    List<int> indices = Enumerable.Range(0, count).ToList();

    List<int> shuffled = new List<int>();
    while (count > 0)
    {
        int i = random.Next(0, count - 1);
        shuffled.Add(indices[i]);
        indices.RemoveAt(i);
        count--;
    }

    return shuffled;
}

private Bitmap CreatePrintedLine(List<string> textParts, string delimiter, int maxTextParts, int maxWidth)
{
    Random random = new Random((int)DateTime.Now.Ticks);
    GenericProcessing genericProcessing = new GenericProcessing();
    Binarization binarization = new Binarization();

    string[] fontNames = new string[] {
        "Arial",
        "Arial Black" ,
        "Calibri",
        "Microsoft Sans Serif",
        "Tahoma"
    };

    float[] fontSizes = new float[] { 10, 11, 12, 14, 16, 18, 22, 24, 26 };

    int fontStyleId = random.Next(0, 29);
    FontStyle fontStyle;
    if (fontStyleId >= 0 && fontStyleId < 10)
    {
        fontStyle = FontStyle.Regular;
    }
    else if (fontStyleId >= 10 && fontStyleId < 20)
    {
        fontStyle = FontStyle.Bold;
    }
    else
    {
        fontStyle = FontStyle.Italic;
    }
    Font font = new Font(fontNames[random.Next(0, fontNames.Length)],
        fontSizes[random.Next(0, fontSizes.Length)],
        fontStyle);

    string text = null;
    System.Drawing.Size textSize = TextRenderer.MeasureText("", font);

    List<int> indices = getRandomShuffledIndices(textParts.Count);
    foreach (int i in indices)
    {
        textSize = TextRenderer.MeasureText(textParts[i], font);
        if (textSize.Width < maxWidth)
        {
            text = textParts[i];
            break;
        }
    }

    if (text == null)
    {
        return null;
    }

    int numberOfParts = random.Next(1, maxTextParts);

    while (numberOfParts > 1)
    {
        indices = getRandomShuffledIndices(textParts.Count);

        foreach (int i in indices)
        {
            string newText = text + delimiter + textParts[i];
            System.Drawing.Size newTextSize = TextRenderer.MeasureText(newText, font);
            if (newTextSize.Width < maxWidth)
            {
                text = newText;
                textSize = newTextSize;
                break;
            }
        }

        numberOfParts--;
    }

    Bitmap bitmap = new Bitmap(textSize.Width, textSize.Height, PixelFormat.Format24bppRgb);
    using (Graphics graphics = Graphics.FromImage(bitmap))
    {
        graphics.Clear(Color.White);
        graphics.DrawString(text, font, Brushes.Black, 0, 0);
    }

    Bitmap result;
    if (random.Next(0, 9) <= 4)
    {
        result = RotateBitmap(bitmap, random.Next(-3, 3));
    }
    else
    {
        result = bitmap.Clone() as Bitmap;
    }

    genericProcessing.ConvertTo8Bit(result, ref result);
    int thresh = 0;
    result = OpenCvOtsu(result);
    Bitmap cropped = getBoundariesAndCrop(result);

    return cropped;
}


private Bitmap CreatePrintedNumber(int maxDigits)
{
    Random random = new Random((int)DateTime.Now.Ticks);
    GenericProcessing genericProcessing = new GenericProcessing();
    Binarization binarization = new Binarization();

    string[] fontNames = new string[] {
        "Arial",
        "Arial Black" ,
        "Calibri",
        "Microsoft Sans Serif",
        "Tahoma"
    };

    float[] fontSizes = new float[] { 10, 11, 12, 14, 16, 18, 22, 24, 26 };

    int fontStyleId = random.Next(0, 29);
    FontStyle fontStyle;
    if (fontStyleId >= 0 && fontStyleId < 10)
    {
        fontStyle = FontStyle.Regular;
    }
    else if (fontStyleId >= 10 && fontStyleId < 20)
    {
        fontStyle = FontStyle.Bold;
    }
    else
    {
        fontStyle = FontStyle.Italic;
    }
    Font font = new Font(fontNames[random.Next(0, fontNames.Length)],
        fontSizes[random.Next(0, fontSizes.Length)],
        fontStyle);

    int digits = random.Next(1, maxDigits);
    string text = "";

    for (int i = 0; i < digits; i++)
    {
        text += random.Next(0, 9).ToString();
    }

    if (random.Next(0, 9) <= 4)
    {
        text += ".";
        int decimalPlaces = random.Next(1, 3);
        for (int i = 0; i < decimalPlaces; i++)
        {
            text += random.Next(0, 9).ToString();
        }
    }

    if (random.Next(0, 9) <= 4)
    {
        int bracketStyle = random.Next(1, 4);
        string bracket;
        if (bracketStyle == 1)
        {
            bracket = "#";
        }
        else if (bracketStyle == 2)
        {
            bracket = "/";
        }
        else if (bracketStyle == 3)
        {
            bracket = "\\";
        }
        else
        {
            bracket = "|";
        }
        text = bracket + text + bracket;
    }

    System.Drawing.Size textSize = TextRenderer.MeasureText(text, font);

    Bitmap bitmap = new Bitmap(textSize.Width, textSize.Height, PixelFormat.Format24bppRgb);
    using (Graphics graphics = Graphics.FromImage(bitmap))
    {
        graphics.Clear(Color.White);
        graphics.DrawString(text, font, Brushes.Black, 0, 0);
        graphics.Dispose();
    }

    Bitmap result;
    if (random.Next(0, 9) <= 4)
    {
        result = RotateBitmap(bitmap, random.Next(-3, 3));
    }
    else
    {
        result = bitmap.Clone() as Bitmap;
    }

    genericProcessing.ConvertTo8Bit(result, ref result);
    int thresh = 0;
    result = OpenCvOtsu(result);
    Bitmap cropped = getBoundariesAndCrop(result);

    return cropped;
}

private Bitmap CreatePrintedDate()
{
    Random random = new Random((int)DateTime.Now.Ticks);
    GenericProcessing genericProcessing = new GenericProcessing();
    Binarization binarization = new Binarization();

    string[] fontNames = new string[] {
        "Arial",
        "Arial Black" ,
        "Calibri",
        "Microsoft Sans Serif",
        "Tahoma"
    };

    float[] fontSizes = new float[] { 10, 11, 12, 14, 16, 18, 22, 24, 26 };

    int fontStyleId = random.Next(0, 29);
    FontStyle fontStyle;
    if (fontStyleId >= 0 && fontStyleId < 10)
    {
        fontStyle = FontStyle.Regular;
    }
    else if (fontStyleId >= 10 && fontStyleId < 20)
    {
        fontStyle = FontStyle.Bold;
    }
    else
    {
        fontStyle = FontStyle.Italic;
    }
    Font font = new Font(fontNames[random.Next(0, fontNames.Length)],
        fontSizes[random.Next(0, fontSizes.Length)],
        fontStyle);

    string text;

    bool yearFirst = random.Next(0, 9) <= 4;
    bool monthFirst = random.Next(0, 9) <= 4;

    int day = random.Next(1, 30);
    int month = random.Next(1, 12);
    int year = random.Next(1970, 3000);

    text = day.ToString();
    if (monthFirst)
    {
        text = month.ToString() + "/" + text;
    }
    else
    {
        text = text + "/" + month.ToString();
    }
    if (yearFirst)
    {
        text = year.ToString() + "/" + text;
    }
    else
    {
        text = text + "/" + year.ToString();
    }

    System.Drawing.Size textSize = TextRenderer.MeasureText(text, font);

    Bitmap bitmap = new Bitmap(textSize.Width, textSize.Height, PixelFormat.Format24bppRgb);
    using (Graphics graphics = Graphics.FromImage(bitmap))
    {
        graphics.Clear(Color.White);
        graphics.DrawString(text, font, Brushes.Black, 0, 0);
    }

    Bitmap result;
    if (random.Next(0, 9) <= 4)
    {
        result = RotateBitmap(bitmap, random.Next(-3, 3));
    }
    else
    {
        result = bitmap.Clone() as Bitmap;
    }

    genericProcessing.ConvertTo8Bit(result, ref result);
    int thresh = 0;
    result = OpenCvOtsu(result);
    Bitmap cropped = getBoundariesAndCrop(result);

    return cropped;
}

private List<Bitmap> getHandwrittenTextList(int minWidth, int maxWidth, int minHeight, int maxHeight)
{
    List<string> handwrittenText = Directory.GetFiles(@"C:\Users\USER\Desktop\AutoEncoder\Handwritten", "*.tif").ToList();
    List<Bitmap> handwrittenLines = new List<Bitmap>();
    GenericProcessing gp = new GenericProcessing();
    Binarization bin = new Binarization();
    int processedCount = 0;
    int totalCount = handwrittenText.Count;
    Random rnd = new Random();
    while (processedCount < totalCount)
    {
        Bitmap bmp = (Bitmap)Bitmap.FromFile(handwrittenText[rnd.Next(0, totalCount)]);
        if (bmp.Height > 150) continue;
        //bmp = ScaleByHighQualityBicubic(bmp, rnd.Next(400, 700), rnd.Next(10, 40));
        gp.ConvertTo8Bit(bmp, ref bmp);
        bin.CleverBinarization(ref bmp);
        Bitmap cropped = getBoundariesAndCrop(bmp);
        Bitmap resized = ResizeBiCubicInterpolation(cropped, rnd.Next(minWidth, maxWidth), rnd.Next(minHeight, maxHeight));
        handwrittenLines.Add(resized);
        processedCount++;
    }
    return handwrittenLines;
}

private static Bitmap Scale(Bitmap orgBmp, int newWidth, int newHeight, InterpolationMode interpolation, CompositingQuality compositing, SmoothingMode smoothingMode)
{
    Bitmap tmp = new Bitmap(newWidth, newHeight, PixelFormat.Format24bppRgb);
    using (Graphics g = Graphics.FromImage(tmp))
    {
        g.InterpolationMode = interpolation;
        g.CompositingQuality = compositing;
        g.SmoothingMode = smoothingMode;
        g.Clear(Color.White);
        g.DrawImage(orgBmp, 0, 0, newWidth, newHeight);
    }

    Bitmap resizedBmp = Convert24To8(tmp);

    return resizedBmp;
}

public static unsafe Bitmap Convert24To8(Bitmap bmpImg24)
{
    int width = bmpImg24.Width;
    int height = bmpImg24.Height;

    BitmapData bmpData24 = bmpImg24.LockBits(new System.Drawing.Rectangle(0, 0, width, height), ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
    int offset24 = bmpData24.Stride - 3 * width;

    Bitmap eightbitImg = new Bitmap(width, height, PixelFormat.Format8bppIndexed);
    ColorPalette clrPalette = eightbitImg.Palette;
    for (int i = 0; i < 256; i++)
        clrPalette.Entries[i] = System.Drawing.Color.FromArgb(255, i, i, i);
    eightbitImg.Palette = clrPalette;

    BitmapData bmpData8 = eightbitImg.LockBits(new System.Drawing.Rectangle(0, 0, width, height), ImageLockMode.WriteOnly, PixelFormat.Format8bppIndexed);
    int offset8 = bmpData8.Stride - width;

    byte* p24 = (byte*)bmpData24.Scan0.ToPointer();
    byte* p8 = (byte*)bmpData8.Scan0.ToPointer();

    for (int y = 0; y < height; y++, p8 += offset8, p24 += offset24)
        for (int x = 0; x < width; x++, p8++, p24 += 3)
            p8[0] = p24[0];

    bmpImg24.UnlockBits(bmpData24);
    eightbitImg.UnlockBits(bmpData8);

    return eightbitImg;
}

private ImageCodecInfo GetEncoder(ImageFormat format)
{
    ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();
    foreach (ImageCodecInfo codec in codecs)
    {
        if (codec.FormatID == format.Guid)
        {
            return codec;
        }
    }
    return null;
}

private void drawTextOnAreasToolStripMenuItem_Click(object sender, EventArgs e)
{
    List<string> files = Directory.GetFiles(@"C:\Users\USER\Desktop\AutoEncoder\Signatures").ToList();
    int filesCount = files.Count;
    List<string> baseImages = Directory.GetFiles(@"C:\Users\USER\Desktop\AutoEncoder\Cheques").ToList();
    List<Bitmap> handWrittenTexts = getHandwrittenTextList(100, 500, 20, 40);

    int baseImagesCount = baseImages.Count;
    Random rnd = new Random();
    int currentFileIndex = 0;
    Binarization bin = new Binarization();
    GenericProcessing gp = new GenericProcessing();
    int otVal = 0;
    foreach (string file in files)
    {
        for (int i = 0; i < 6; i++)
        {
            Bitmap baseImg = Bitmap.FromFile(baseImages[rnd.Next(0, baseImagesCount)]) as Bitmap;
            gp.ConvertTo8Bit(baseImg, ref baseImg);
            baseImg = ResizeBiCubicInterpolation(baseImg, 748, 200);
            baseImg = OpenCvOtsu(baseImg);
            baseImg.MakeTransparent(Color.White);
            //generate signatures locations
            int signaturesCount = rnd.Next(1, 3);
            List<int> signaturesIndices = new List<int>();
            signaturesIndices.Add(currentFileIndex);
            List<System.Drawing.Point> signaturesLocations = new List<System.Drawing.Point>();
            signaturesLocations.Add(new System.Drawing.Point(rnd.Next(15, 250), rnd.Next(25, baseImg.Height - 100)));
            for (int x = 1; x < signaturesCount; x++)
            {
                signaturesIndices.Add(rnd.Next(0, filesCount));
                signaturesLocations.Add(new System.Drawing.Point(rnd.Next(15, 250), rnd.Next(25, baseImg.Height - 100)));
            }
            //generate name location
            List<int> texualIndices = new List<int>();
            int linesCount = 3; // rnd.Next(2, 4);
            List<System.Drawing.Point> textualLocations = new List<System.Drawing.Point>();
            List<string> baseNames = File.ReadAllLines(@"C:\Users\USER\Desktop\AutoEncoder\names.txt").ToList();

            textualLocations.Add(new System.Drawing.Point(rnd.Next(40, 200), rnd.Next(0, 10)));
            textualLocations.Add(new System.Drawing.Point(rnd.Next(550, 615), rnd.Next(0, 10)));
            textualLocations.Add(new System.Drawing.Point(rnd.Next(340, 430), rnd.Next(50, 75)));


            //Generate training image
            Bitmap trainingImg = new Bitmap(baseImg.Width, baseImg.Height, PixelFormat.Format32bppArgb);
            trainingImg.MakeTransparent(Color.White);
            List<Bitmap> signatures = new List<Bitmap>();
            Bitmap textImage1;
            Bitmap textImage2;
            Bitmap textImage3;

            using (Graphics g = Graphics.FromImage(trainingImg))
            {
                g.DrawImage(baseImg, 0, 0);

                for (int x = 0; x < signaturesCount; x++)
                {
                    Bitmap sig = Bitmap.FromFile(files[signaturesIndices[x]]) as Bitmap;
                    gp.ConvertTo8Bit(sig, ref sig);
                    Bitmap sigResized = null;
                    if (sig.Height > 100 || sig.Width > 320)
                    {
                        sigResized = ResizeBiCubicInterpolation(sig, rnd.Next(160, 320), rnd.Next(50, 100));
                    }
                    else
                    {
                        sigResized = sig.Clone() as Bitmap;
                    }

                    sigResized.MakeTransparent(Color.White);
                    g.DrawImage(sigResized, signaturesLocations[x]);
                    signatures.Add(sigResized);
                }

                if (rnd.Next(0, 9) <= 4)
                {
                    textImage1 = CreatePrintedLine(baseNames, " ", 6, 500);
                }
                else
                {
                    int choice = rnd.Next(0, handWrittenTexts.Count - 1);
                    textImage1 = handWrittenTexts[choice];
                }
                textImage1.MakeTransparent(Color.White);
                g.DrawImage(textImage1, textualLocations[0]);

                if (rnd.Next(0, 9) <= 4)
                {
                    textImage2 = CreatePrintedLine(baseNames, " ", 2, 120);
                }
                else
                {
                    textImage2 = CreatePrintedNumber(6);
                }
                textImage2.MakeTransparent(Color.White);
                g.DrawImage(textImage2, textualLocations[1]);

                if (rnd.Next(0, 9) <= 4)
                {
                    textImage3 = CreatePrintedLine(baseNames, " ", 2, 200);
                }
                else
                {
                    textImage3 = CreatePrintedDate();
                }
                textImage3.MakeTransparent(Color.White);
                g.DrawImage(textImage3, textualLocations[2]);
            }

            //Generate clean image
            Bitmap ResultImg = new Bitmap(baseImg.Width, baseImg.Height, PixelFormat.Format32bppArgb);
            ResultImg.MakeTransparent(Color.White);

            using (Graphics g = Graphics.FromImage(ResultImg))
            {
                g.Clear(Color.White);
                for (int x = 0; x < signaturesCount; x++)
                {
                    Bitmap sig = signatures[x];
                    g.DrawImage(sig, signaturesLocations[x]);
                }
                g.DrawImage(textImage1, textualLocations[0]);
                g.DrawImage(textImage2, textualLocations[1]);
                g.DrawImage(textImage3, textualLocations[2]);
            }

            string name = Guid.NewGuid().ToString().Replace("-", "");

            Bitmap b1 = new Bitmap(trainingImg.Width, trainingImg.Height, PixelFormat.Format24bppRgb);
            using (Graphics g = Graphics.FromImage(b1))
            {
                g.Clear(Color.White);
                g.DrawImage(trainingImg, 0, 0);
            }
            gp.ConvertTo8Bit(b1, ref b1);
            b1 = OpenCvOtsu(b1);
            ImageCodecInfo jpgEncoder = GetEncoder(ImageFormat.Jpeg);
            System.Drawing.Imaging.Encoder myEncoder = System.Drawing.Imaging.Encoder.Quality;
            EncoderParameters myEncoderParameters = new EncoderParameters(1);
            EncoderParameter myEncoderParameter = new EncoderParameter(myEncoder, rnd.Next(60, 100));
            myEncoderParameters.Param[0] = myEncoderParameter;
            b1.Save(@"C:\Users\USER\Desktop\AutoEncoder\Data\Input\" + name + ".jpg", jpgEncoder, myEncoderParameters);

            Bitmap b2 = new Bitmap(trainingImg.Width, trainingImg.Height, PixelFormat.Format24bppRgb);
            using (Graphics g = Graphics.FromImage(b2))
            {
                g.Clear(Color.White);
                g.DrawImage(ResultImg, 0, 0);
            }
            gp.ConvertTo8Bit(b2, ref b2);
            b2 = OpenCvOtsu(b2);
            b2.Save(@"C:\Users\USER\Desktop\AutoEncoder\Data\Output\" + name + ".jpg", ImageFormat.Jpeg);
        }
        currentFileIndex++;
    }
}
}
