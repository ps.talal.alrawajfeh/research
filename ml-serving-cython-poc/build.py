#!/usr/bin/python3.6

from setuptools import setup, Extension

ext_modules = [Extension("pre_processing", ["./app/concerns/pre_processing.py"]),
               Extension("model_loading", ["./app/concerns/model_loading.py"]),
               Extension("model", ["./app/concerns/model.py"]),
               Extension("classification_service", ["./app/controllers/classification_service.py"]),
               Extension("app", ["./app/app.py"])]

for e in ext_modules:
    e.cython_directives = {'always_allow_keywords': True}

setup(ext_modules=ext_modules)
