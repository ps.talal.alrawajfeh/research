#!/usr/bin/python3.6

from app.app import app

if __name__ == '__main__':
    app.run(port=9090)
