#!/usr/bin/python3.6

from flask import Flask

from app.controllers.classification_service import classification_service

app = Flask(__name__)
app.register_blueprint(classification_service)


@app.route('/')
def hello():
    return "ML service is alive and well!"


if __name__ == "__main__":
    app.run(port=9090)
