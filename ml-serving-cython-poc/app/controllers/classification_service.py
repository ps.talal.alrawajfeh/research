#!/usr/bin/python3.6

import base64

from flask import Blueprint, request, jsonify

from app.concerns.model import Model
from app.concerns.pre_processing import read_images_from_bytes

classification_service = Blueprint('classification_service', __name__)


@classification_service.route('/classify/<version>', methods=['POST'])
def classify(version=None):
    if int(version) == 1:
        content = request.json
        images = read_images_from_bytes([base64.decodestring(img.encode('utf-8')) for img in content['images']])
        model = Model('./app/resources/data_8.bin')
        results = model.predict_batch(images)
        return jsonify({'predictions': results, 'errors': {}}), 202, {'Content-Type': 'application/json'}
    else:
        return jsonify({'errors': [f'version {version} is not supported']}), 400, {'Content-Type': 'application/json'}
