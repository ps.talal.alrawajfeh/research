#!/usr/bin/python3.6

import cv2
from keras.models import load_model

from app.concerns.pre_processing import pre_process, pre_process_batch
from app.concerns.model_loading import read_model


class Model:
    class __Model:
        def __init__(self, path):
            self.model = read_model(path)

    instance = None

    def __init__(self, path=None):
        if not Model.instance:
            Model.instance = Model.__Model(path)

    def predict(self, image):
        predictions = Model.instance.model.predict(pre_process(image))
        return int(predictions[0].argmax())

    def predict_batch(self, image):
        predictions = Model.instance.model.predict(pre_process_batch(image))
        return [int(prediction.argmax()) for prediction in predictions]
