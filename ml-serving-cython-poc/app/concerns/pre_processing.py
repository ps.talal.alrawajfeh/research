#!/usr/bin/python3.6

import cv2
import numpy as np
from keras import backend as K

IMG_ROWS, IMG_COLS = 28, 28


def pre_process(image):
    return pre_process_batch([image])


def pre_process_batch(batch):
    model_input = np.array(batch)

    if K.image_data_format() == 'channels_first':
        model_input = model_input.reshape(model_input.shape[0], 1, IMG_ROWS, IMG_COLS)
    else:
        model_input = model_input.reshape(model_input.shape[0], IMG_ROWS, IMG_COLS, 1)

    model_input = model_input.astype('float32')
    model_input /= 255

    return model_input


def read_images_from_bytes(images):
    inputs = [np.fromstring(img, np.uint8) for img in images]
    return [cv2.imdecode(i, 0) for i in inputs]
