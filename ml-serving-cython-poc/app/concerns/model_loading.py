import base64
import os
from functools import reduce
from tempfile import NamedTemporaryFile

import numpy as np
from Crypto.Cipher import AES
from Crypto.Hash import SHA256
from keras.models import load_model

BLOCK_SIZE = 16


def encrypt(message, passphrase):
    iv = b'\xaa\xbb\xcc\xdd\xee\xff\x00\x11' * 2
    i = 0
    while len(message) % 16 != 0:
        message += b'0'
        i += 1
    aes = AES.new(passphrase, AES.MODE_CFB, iv)
    return base64.b64encode(aes.encrypt(message)), i


def decrypt(encrypted, passphrase, padded_bytes=0):
    iv = b'\xaa\xbb\xcc\xdd\xee\xff\x00\x11' * 2
    aes = AES.new(passphrase, AES.MODE_CFB, iv)
    result = aes.decrypt(base64.b64decode(encrypted))
    return result[0:len(result) - padded_bytes]


def f1(x):
    return (x ** 4 + 3 * x ** 3 - 2 * x + 7) % 9277


def f2(x):
    return (x ** 2 - 17 * x + 1) & 3613


def f3(x):
    return (x >> 1 + 6571) % 4973


def generate_key():
    nums = []
    for i in range(0, 1000):
        y1 = f2(f3(f1(i)))
        y2 = f1(f2(f2(i)))
        y3 = f3(f2(f1(i)))
        y4 = f2(f1(f2(i)))
        nums.extend([y1, y2, y3, y4])

    nums = list(filter(lambda x: x > 1, nums))
    m = np.array(nums).min()
    nums = [x - m + 1 for x in nums]

    prev = nums[0]
    new_nums = [prev]
    nums.pop(0)

    for i in nums:
        new_nums.append(i & prev)
        prev = (i + prev) >> 2

    nums = new_nums
    nums = list(filter(lambda x: x > 1, nums))
    m = np.array(nums).min()
    nums = [x - m + 1 for x in nums]
    nums = list(filter(lambda x: x % 2 != 0, nums))

    indexes = np.unique(nums, return_index=True)[1]
    nums = [nums[index] for index in sorted(indexes)]

    return SHA256.new(reduce(lambda x, y: x + y, [str(i) for i in nums]).encode('utf-8')).hexdigest()


def read_model(path):
    with open(path, 'rb') as file:
        encrypted_model = file.read()

    padded_bytes = int(os.path.basename(path).split(os.path.extsep)[0].split('_')[-1])

    key = generate_key()[0:16]
    model = decrypt(encrypted_model, key, padded_bytes)

    temp_file = NamedTemporaryFile()

    with open(temp_file.name, 'wb') as file:
        file.write(model)

    model = load_model(temp_file.name)
    temp_file.close()

    return model
