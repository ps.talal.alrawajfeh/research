#!/bin/bash

find | grep [.]c$ | xargs rm
find | grep [.]so$ | xargs rm

rm -rf ./build

python3 build.py build_ext --inplace

rm -rf ./out
mkdir ./out

mkdir ./out/app
touch ./out/app/__init__.py
mkdir ./out/app/concerns
touch ./out/app/concerns/__init__.py
mkdir ./out/app/controllers
touch ./out/app/controllers/__init__.py

mv app.cpython*.so ./out/app/
mv model.cpython*.so ./out/app/concerns
mv pre_processing.cpython*.so ./out/app/concerns
mv model_loading.cpython*.so ./out/app/concerns
mv classification_service.cpython*.so ./out/app/controllers
cp -r ./app/resources ./out/app/resources
cp run.py ./out/run.py

cd ./app
find | grep [.]c$ | xargs rm
cd ..

rm -rf ./build

cd ./out/
tar -czvf app.tar.gz ./app run.py
rm -rf ./app
rm run.py
cd ..
