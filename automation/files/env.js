(function (window) {
  window.__env = window.__env || {};
  window.__env.baseUrl = "http://localhost:4200";
  //window.__env.gateway = "https://apollo-progressofta-89-using-keycloak-to-asv-core.machine-dev.progressoft.cloud/asv/api/";
  window.__env.gateway = "http://localhost:9090/asv/api/";
  window.__env.applicationId = "ASV";
  window.__env.whiteListChar = [";"];
  window.__env.dashboard = {};
  window.__env.dashboard.enable = true;
  window.__env.dashboard.intervalInSecond = 15;
  window.__env.authConfiguration = {};
  window.__env.authConfiguration.URL = "https://pssec-security.machine-dev-ing.progressoft.cloud/auth";
  window.__env.authConfiguration.Realm = "asv";
  window.__env.authConfiguration.clientId = "asv-client"
}(this));

