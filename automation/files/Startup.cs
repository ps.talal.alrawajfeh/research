using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Progressoft.AppUtils.Core.Logging;
using Progressoft.AppUtils.Core.Web.Filters;
using Progressoft.ASV.Service.Configurations;
using Progressoft.ASV.Service.Swagger;

namespace Progressoft.ASV.Service
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public object Bitmapfactory { get; private set; }

        private readonly SwaggerDocumenter _swaggerDocumenter;
        private readonly AsvConfigurations _asvConfig;

        //public Startup(Microsoft.AspNetCore.Hosting.IHostingEnvironment env, IConfiguration configuration)
        public Startup(Microsoft.AspNetCore.Hosting.IWebHostEnvironment env, IConfiguration configuration)
        {
            Configuration = configuration;
            _asvConfig = AsvConfigurations.Read(env, configuration);
            _swaggerDocumenter = new SwaggerDocumenter(_asvConfig.SwaggerConfig.ApiContactName,
                                         _asvConfig.SwaggerConfig.ApiContactUri,
                                         _asvConfig.SwaggerConfig.ApiContactEmail,
                                         _asvConfig.SwaggerConfig.ApiLicenseName);
            RequestResponseLoggerFilter.Logger = LoggerFactory.Instance.Create(nameof(RequestResponseLoggerFilter)).Value;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddCors(); // Make sure you call this previous to AddMvc
            services.Configure<KestrelServerOptions>(options => options.AllowSynchronousIO = true);

            new AsvServicesInjector(_asvConfig, services).InjectDependencies();
            AddControllers(services);
            AddSwagger(services);
        }

        private void AddControllers(IServiceCollection services)
        {
            int count = 1;
            services.AddControllers(options =>
            {
                options.Filters.Add<RequestResponseLoggerFilter>(count++);
                if (_asvConfig.SecurityConfig.RemoveStackTraceFromResponse)
                    options.Filters.Add<StackTraceRemovalFilter>(count++);
            });
        }

        private void AddSwagger(IServiceCollection services)
        {
            _swaggerDocumenter.AddSwaggerGen(services);
            services.AddApiVersioning(setup =>
            {
                setup.AssumeDefaultVersionWhenUnspecified = true;
                setup.ReportApiVersions = true;
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, Microsoft.Extensions.Logging.ILoggerFactory logger)
        {
            app.UseCors(x => x
              .AllowAnyMethod()
              .AllowAnyHeader()
              .SetIsOriginAllowed(origin => true) // allow any origin
              .AllowCredentials()); // allow credentials

            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();
            _swaggerDocumenter.AddSwaggerUI(app);

            //AsvServicesLogger.Create(_asvConfig.LoggerConfig);
            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            //RepositoriesImpl.DbContext.AsvDbContext dbContext = app.ApplicationServices.GetRequiredService<RepositoriesImpl.DbContext.IAsvContextFactory>().Create().Value;
            //var ids = dbContext.Signatures.ToList().Select(e => new Guid(e.Id).ToString());
        }
    }
}