﻿using System.Drawing;
using Microsoft.Data.SqlClient;
using Npgsql;
using Progressoft.ASV.Domain.Models;
using Progressoft.ASV.Service.RepositoriesImpl.Mappers;
using Progressoft.ML.NeuralNetworkCore.Core;
using Progressoft.ML.SignatureFeaturesNet.Core;

namespace SignatureFeatureVectorsUpdater
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var connectionString = "Server=localhost;port=5432;Database=asvdb;User Id=asv;Password=asv";
                var imagesDirectoryPath = "/home/u764/Downloads/talal/AsvImages";
                var featureVectorsDirectoryPath = "/home/u764/Downloads/talal/AsvFeatures";
                var predictionPlatform = PredictionEnginePlatform.TensorFlow;

                Console.WriteLine("Connecting to database...");
                using var connection = new NpgsqlConnection(connectionString);
                connection.Open();
                Console.WriteLine("Connection established.");

                Console.WriteLine("Retrieving ids...");


                Dictionary<string, string> signatureImageIdFeatureVectorId = new Dictionary<string, string>();

                foreach (var tableName in new[] {"Signatures", "PendingSignatures", "AuditSignatures"})
                {
                    var n = 0;
                    foreach (var keyValuePair in SelectSignatureImageIdFeatureVectorId(connection, tableName))
                    {
                        signatureImageIdFeatureVectorId[keyValuePair.Key] = keyValuePair.Value;
                        n++;
                    }

                    Console.WriteLine($"Found {n} items in table: {tableName}...");
                }

                var totalItems = signatureImageIdFeatureVectorId.Count;
                Console.WriteLine($"Total items found: {totalItems}");

                var tripletOfDenseNet = new TripletOfDenseNet(predictionPlatform);

                Console.WriteLine("Updating feature vectors...");
                var progress = 0;
                foreach (var keyValuePair in signatureImageIdFeatureVectorId)
                {
                    var signatureImageId = keyValuePair.Key;
                    var featureVectorId = keyValuePair.Value;
                    var imagePath = Path.Combine(imagesDirectoryPath, signatureImageId);
                    var tripletOfDenseNetOutput = tripletOfDenseNet
                        .Predict(new[]
                        {
                            TripletOfDenseNetInput.Create(new Bitmap(imagePath)).Value
                        }).Value[0];

                    var featureVector = FeatureVector.Create(new List<float[]>
                    {
                        tripletOfDenseNetOutput.DenseFeatureVector,
                        tripletOfDenseNetOutput.TripletFeatureVector
                    }).Value;

                    var featureVectorByteBuffer = new FeatureVectorToByteBufferMapper().Map(featureVector);
                    File.WriteAllBytes(Path.Combine(featureVectorsDirectoryPath, featureVectorId),
                        featureVectorByteBuffer);

                    progress++;
                    DrawTextProgressBar(progress, totalItems);
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }

            Console.WriteLine("\nDone. Press enter.");
            Console.ReadLine();
        }

        private static Dictionary<string, string> SelectSignatureImageIdFeatureVectorId(NpgsqlConnection connection,
            string tableName)
        {
            Dictionary<string, string> signatureImageIdFeatureVectorId = new Dictionary<string, string>();

            String sql = $"SELECT \"SignatureImageId\", \"FeatureVectorId\" FROM asv_schema.\"{tableName}\"";

            using var command = new NpgsqlCommand(sql, connection);
            using var reader = command.ExecuteReader();

            while (reader.Read())
            {
                byte[] buffer = new byte[16];
                reader.GetBytes(0, 0, buffer, 0, 16);
                var signatureImageId = new Guid(buffer).ToString("n").ToLower();
                reader.GetBytes(1, 0, buffer, 0, 16);
                var featureVectorId = new Guid(buffer).ToString("n").ToLower();
                signatureImageIdFeatureVectorId[signatureImageId] = featureVectorId;
            }

            return signatureImageIdFeatureVectorId;
        }

        private static void DrawTextProgressBar(int progress, int total)
        {
            Console.CursorLeft = 0;
            Console.Write("[");
            Console.CursorLeft = 32;
            Console.Write("]");
            Console.CursorLeft = 1;
            var chunk = 30.0f / total;

            int position = 1;
            for (int i = 0; i < chunk * progress; i++)
            {
                Console.CursorLeft = position++;
                Console.Write("#");
            }

            for (int i = position; i <= 31; i++)
            {
                Console.CursorLeft = position++;
                Console.Write(" ");
            }

            Console.CursorLeft = 35;
            Console.Write(progress + " of " + total + "    ");
        }
    }
}