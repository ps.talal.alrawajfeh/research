#!/usr/bin/bash

dotnet publish src/Progressoft.ASV.Service/Progressoft.ASV.Service.csproj -r linux-x64 --configuration Release --output publish
dotnet publish src/Progressoft.SignatureSystem.Service/Progressoft.SignatureSystem.Service.csproj -r linux-x64 --configuration Release --output publish-sig
dotnet publish src/Progressoft.SignedDocumentSystem.Service/Progressoft.SignedDocumentSystem.Service.csproj -r linux-x64 --configuration Release --output publish-SignedDocument

mkdir publish/models
cp src/Progressoft.ASV.Service/ML.Models/* publish/models

cp src/Progressoft.ASV.Service/RepositoriesImpl/DbScript/Postgresql/* publish/

mkdir publish-sig/DemoSignatureSystem
cp src/Progressoft.SignatureSystem.Service/Controllers/ServicesImpl/DemoSignatureSystem/DataFiles/* publish-sig/DemoSignatureSystem

trap 'kill $(jobs -p)' EXIT

export PROGRESSSOFT_HOME=$(pwd)/publish/models

(
  cd publish-sig
  dotnet Progressoft.SignatureSystem.Service.dll --urls http://0.0.0.0:44350
) &
(
  cd publish-SignedDocument
  dotnet Progressoft.SignedDocumentSystem.Service.dll --urls http://0.0.0.0:44351

) &
(
  while ! curl http://0.0.0.0:44350/asv/api/SignatureSystem/GetCapabilities 2>&1 | grep 'isAmountVerificationSupported'
  do
    sleep 1
  done
  cd publish
  dotnet Progressoft.ASV.Service.dll --urls http://0.0.0.0:9090
)
