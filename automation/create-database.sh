#!/usr/bin/bash

export DB_SYS_URL=sys:sys@localhost:5432
export DB_URL=asv:asv@localhost:5432/asvdb

# trap 'kill $(jobs -p)' EXIT

docker run -p 5432:5432 -e POSTGRES_PASSWORD=sys -e POSTGRES_USER=sys postgres &

while ! curl http://$DB_SYS_URL 2>&1 | grep '52'
do
	sleep 1
done

psql postgresql://$DB_SYS_URL -f ./files/PrepareDatabase.sql
PGPASSWORD='asv' pg_restore -h localhost -p 5432 -U asv -d asvdb -w ./files/asvdb

# echo 'To exit press ctrl+c...'
# while true
# do
	# sleep 1
# done
