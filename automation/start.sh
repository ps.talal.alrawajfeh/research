#!/usr/bin/bash

export ASV_PROJECT_PATH=/home/u764/Development/progressoft/progressoft-asv-core
export UI_PROJECT_PATH=/home/u764/Development/progressoft/asv-core-ui

trap 'kill $(jobs -p)' EXIT

./copy-files.sh

./create-database.sh &
(
    cd $ASV_PROJECT_PATH
    chmod a+rwx ./deploy-services.sh
    ./deploy-services.sh
) &
(
    cd $UI_PROJECT_PATH
    npm install
    ng serve   
)