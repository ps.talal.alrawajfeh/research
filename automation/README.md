# ASV Core & UI Automated Deployment

In the script `start.sh` change the first two environment variables `ASV_PROJECT_PATH`, `UI_PROJECT_PATH` to the paths of the core project and the ui project respectively. Copy the `AsvFeatures`, `AsvImages`, `DemoSignedDocumentImages`, and `jsonContents` folders into the `files` directory. Also, copy `asvdb` database dump into the `files` directory.