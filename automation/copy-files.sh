#!/usr/bin/bash

export FILES_PATH=$(pwd)/files

cp ./files/asv-service-appsettings.json $ASV_PROJECT_PATH/src/Progressoft.ASV.Service/appsettings.json
cp ./files/sig-service-appsettings.json $ASV_PROJECT_PATH/src/Progressoft.SignatureSystem.Service/appsettings.json
cp ./files/signed-document-service-appsettings.json $ASV_PROJECT_PATH/src/Progressoft.SignedDocumentSystem.Service/appsettings.json

export FILE_PATH=$ASV_PROJECT_PATH/src/Progressoft.ASV.Service/appsettings.json
echo "f = open('$FILE_PATH', 'r'); content=f.read(); f.close(); f = open('$FILE_PATH', 'w'); f.write(content.replace('#####', '$FILES_PATH')); f.close()" | python3
export FILE_PATH=$ASV_PROJECT_PATH/src/Progressoft.SignatureSystem.Service/appsettings.json
echo "f = open('$FILE_PATH', 'r'); content=f.read(); f.close(); f = open('$FILE_PATH', 'w'); f.write(content.replace('#####', '$FILES_PATH')); f.close()" | python3
export FILE_PATH=$ASV_PROJECT_PATH/src/Progressoft.SignedDocumentSystem.Service/appsettings.json
echo "f = open('$FILE_PATH', 'r'); content=f.read(); f.close(); f = open('$FILE_PATH', 'w'); f.write(content.replace('#####', '$FILES_PATH')); f.close()" | python3

cp ./files/Startup.cs $ASV_PROJECT_PATH/src/Progressoft.ASV.Service/Startup.cs
cp ./deploy-services.sh $ASV_PROJECT_PATH/
cp ./files/env.js $UI_PROJECT_PATH/src/env.js