#!/usr/bin/python3

import json


def main():
    with open('/home/u764/Downloads/talal/jsonContents/SigDemoSystem.json', 'r') as f:
        content = f.read()

    content = content.replace('\n', '').replace('\t', '').replace(
        ', ', ',').replace(',}', '}').replace(',]', ']')
    content_json = json.loads(content)

    for account in content_json['Accounts']:
        for signatory in account['Signatories']:
            signatory['Notes'] = 'signatory note'

    with open('/home/u764/Downloads/talal/jsonContents/SigDemoSystem-New.json', 'w') as f:
        f.write(json.dumps(content_json, indent=4, ensure_ascii=False))


if __name__ == '__main__':
    main()
