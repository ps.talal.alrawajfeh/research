from email.mime import base
import cv2
import tensorflow as tf
from tensorflow.keras.models import load_model
import numpy as np
import os


def remove_white_border(binary_image):
    mask = (255 - binary_image) > 0

    height, width = binary_image.shape[0:2]
    mask1, mask2 = mask.any(0), mask.any(1)
    x1, x2 = mask1.argmax(), width - mask1[::-1].argmax()
    y1, y2 = mask2.argmax(), height - mask2[::-1].argmax()

    return binary_image[y1:y2, x1:x2]


def change_image_quality(image, quality=50):
    encoded = cv2.imencode(
        '.jpg', image, [int(cv2.IMWRITE_JPEG_QUALITY), quality])[1]
    return cv2.imdecode(encoded, cv2.IMREAD_GRAYSCALE)


def main():
    autoencoder = load_model('autoencoder.h5', compile=False)

    base_dir = '/home/u764/Downloads/arbk-palestine/binarized-73'
    files = os.listdir(base_dir)
    files = filter(lambda x: x.find('-bin') != -1, files)

    for file in files:
        original_image = cv2.imread(os.path.join(
            base_dir, file), cv2.IMREAD_GRAYSCALE)
        original_image = 255 - remove_white_border(255 - original_image)
        # original_image = change_image_quality(original_image)

        image = cv2.resize(original_image, (748, 200),
                           interpolation=cv2.INTER_CUBIC)
        image = cv2.threshold(image, 0, 255, cv2.THRESH_OTSU + cv2.THRESH_BINARY)[1]

        # image[image > 128] = 255
        # image[image <= 128] = 0
        # image = original_image
        image = np.array(255 - image, np.float32) / 255.0
        result = autoencoder.predict(np.array([image.reshape(200, 748, 1)]))[0]
        result = result.reshape(200, 748)
        result *= 255
        result = 255 - np.array(result, np.uint8)
        result = cv2.threshold(
            result, 0, 255, cv2.THRESH_OTSU + cv2.THRESH_BINARY)[1]

        cv2.imwrite(
            f'/home/u764/Downloads/arbk-palestine/autoencoder-73/{file}', original_image)
        cv2.imwrite(
            f'/home/u764/Downloads/arbk-palestine/autoencoder-73/{os.path.splitext(file)[0]}-autoencoder.bmp', result)


if __name__ == '__main__':
    main()
