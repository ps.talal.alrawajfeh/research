import itertools
import logging
import math
import os
import random

import cv2
import numpy as np
from matplotlib import pyplot as plt
from scipy import interpolate

from main import view_multiple_images_with_image
from utils import get_connected_regions, get_region_rectangle, rectangle_area, crop_image, minimal_bounding_rectangle, \
    auto_correct_rectangle, euclidean_distance, union, to_int_ceil, get_region_centers, kmeans_clustering, \
    cluster_regions_by_centers, remove_rectangle, extract_signatures_by_kmeans


def normalize(x):
    if x >= 1.0:
        return 1.0
    if x <= 0.0:
        return 0.0
    return x


def fuzzy_invert(x):
    return 1.0 - normalize(x)


def fuzzy_and(numbers):
    result = 1.0
    n = len(numbers)
    for i in range(n):
        result *= math.pow(normalize(numbers[i]), 1.0 / n)
    return result


def fuzzy_or(numbers):
    inverted_numbers = [1.0 - normalize(x) for x in numbers]
    return 1.0 - fuzzy_and(inverted_numbers)


def truth_fuzzifier(value_mean, value_std, value_max, value_min):
    return interpolate.interp1d([0.0, value_min, value_mean - value_std, value_mean + value_std, value_max, 1.0],
                                [0.0, 0.5, 0.99, 0.99, 0.5, 0.0])


def threshold(image):
    return cv2.threshold(
        image,
        0,
        255,
        cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]


def get_min_x_hist(image):
    x_hist = np.sum(image.T, axis=-1) / 255
    for i in range(len(x_hist)):
        x_hist[i] = x_hist[i] // 5 * 5

    # plt.plot(list(range(image.shape[1])), x_hist)
    # plt.show()

    min_xs = []
    min_value = image.shape[0]
    for x in range(int(image.shape[1] // 4), int((3 * image.shape[1]) // 4)):
        if x_hist[x] < min_value:
            min_value = x_hist[x]
            min_xs = [x]
        elif abs(x_hist[x] - min_value) < 1e-7:
            min_xs.append(x)
    return int(round(float(np.mean(min_xs))))


def center_of_mass(image, black_color=255):
    points = []
    for y in range(image.shape[0]):
        for x in range(image.shape[1]):
            if image[y, x] == black_color:
                points.append([x, y])
    points = np.array(points)
    return int(round(float(np.mean(points[:, :1])))), int(round(float(np.mean(points[:, 1:]))))


def format_number(number):
    return round(float(number), 2)


def is_type1(image):
    if image.shape[0] >= image.shape[1]:
        return 0.0, 0.0

    height_width_ratio = image.shape[0] / image.shape[1]
    v1 = truth_fuzzifier(0.275, 0.05, 0.4, 0.15)(height_width_ratio)

    image = 255 - threshold(image)
    min_x = get_min_x_hist(image)
    v2 = truth_fuzzifier(0.5, 0.05, 0.6, 0.4)(min_x / image.shape[1])

    return format_number(fuzzy_and([v1, v2])), min_x


def is_type2(image):
    if image.shape[0] >= image.shape[1]:
        return 0.0

    height_width_ratio = image.shape[0] / image.shape[1]
    return truth_fuzzifier(0.88, 0.03, 0.95, 0.8)(height_width_ratio)


def is_type3(image):
    if image.shape[0] >= image.shape[1]:
        return 0.0

    height_width_ratio = image.shape[0] / image.shape[1]
    return truth_fuzzifier(0.52, 0.025, 0.6, 0.45)(height_width_ratio)


def view_image(image, title=''):
    plt.imshow(image, cmap=plt.cm.gray)
    plt.title(title)
    plt.show()


def view_images(images, title=''):
    n = int(math.ceil(math.sqrt(len(images))))

    fig, axs = plt.subplots(n, n)

    i = 0
    for r in range(n):
        if i == len(images):
            break
        for c in range(n):
            if i == len(images):
                break
            axs[r, c].imshow(images[i], plt.cm.gray)
            i += 1

    plt.title(title)
    plt.show()


def ensure_directory_exists_and_empty(original_dir, new_dir):
    if os.path.isdir(new_dir):
        files = os.listdir(new_dir)
        for f in files:
            original_path = os.path.join(new_dir, f)
            new_path = os.path.join(original_dir, f)
            os.rename(original_path, new_path)
    else:
        os.mkdir(new_dir)


def remove_lines(image, regions):
    new_regions = []
    for r in regions:
        region = np.array(r)
        x1 = np.min(region[:, :1])
        x2 = np.max(region[:, :1])

        y1 = np.min(region[:, 1:])
        y2 = np.max(region[:, 1:])

        if (x2 - x1) / image.shape[1] > 0.9 and (y2 - y1) / image.shape[0] > 0.9:
            continue

        if (x2 - x1) / image.shape[1] < 0.05 or (y2 - y1) / image.shape[0] < 0.05:
            continue

        if np.sum(255 - image) / (255 * (y2 - y1) * (x2 - x1)) >= 0.9:
            continue

        new_regions.append(region)

    return new_regions


def filter_out_objects(image):
    dilated = 255 - cv2.dilate(255 - image, np.ones((2, 2), np.uint8))
    regions = get_connected_regions(255 - dilated)
    rectangles = [get_region_rectangle(region) for region in regions]

    filtered_regions = []

    for i in range(len(regions)):
        x1, y1, x2, y2 = rectangles[i]

        rect_width = x2 - x1
        rect_height = y2 - y1

        if rect_height / image.shape[0] >= 0.85 or rect_width / image.shape[1] >= 0.85:
            continue

        if rect_width * rect_width / (image.shape[0] * image.shape[1]) >= 0.7:
            continue

        if rect_width / rect_height < 0.05 or rect_height / rect_width < 0.05:
            continue

        if np.sum(255 - dilated[y1:y2, x1:x2]) / (255 * np.prod(image.shape)) >= 0.85:
            continue

        if np.sum(255 - dilated[y1:y2, x1:x2]) / (255 * np.prod(image.shape)) <= 0.00015:
            continue

        filtered_regions.append(regions[i])

    new_image = np.ones(image.shape, np.uint8) * 255

    for region in filtered_regions:
        for x, y in region:
            if image[y, x] == 0:
                new_image[y, x] = 0

    return new_image


def remove_text(image, search_threshold=0.75):
    image = 255 - image
    image = cv2.dilate(image, np.ones((1, 27), np.uint8), iterations=2)
    image = cv2.dilate(image, np.ones((3, 1), np.uint8))
    regions = get_connected_regions(image)
    rectangles = [get_region_rectangle(region) for region in regions]
    filtered_rectangles = []
    max_y = 0
    for i in range(len(regions)):
        x1, y1, x2, y2 = rectangles[i]
        region = regions[i]

        rectangle_fill_ratio = len(region) / rectangle_area(rectangles[i])
        if rectangle_fill_ratio > search_threshold:
            if max_y < y2 < image.shape[0] // 2 and x2 - x1 >= y2 - y1:
                max_y = y2
            continue

        line_ratios = [np.sum(image[y1:y2, x1:x1 + 1]) / (255 * (y2 - y1)),
                       np.sum(image[y1:y2, x2 - 1:x2]) / (255 * (y2 - y1)),
                       np.sum(image[y1:y1 + 1, x1:x2]) / (255 * (x2 - x1)),
                       np.sum(image[y2 - 1:y2, x1:x2]) / (255 * (x2 - x1))]

        if np.max(line_ratios) > search_threshold:
            continue
        filtered_rectangles.append((rectangles[i], region))
    new_image = np.ones(image.shape, np.uint8) * 255
    for rect, region in filtered_rectangles:
        for x, y in region:
            new_image[y, x] = 0
    return new_image, max_y


def extract_type1(image):
    image = cv2.resize(image, (730, 200), interpolation=cv2.INTER_AREA)
    image = threshold(image)
    v, min_x = is_type1(image)
    if v > 0.8:
        test_img = filter_out_objects(image)
        copy_img = np.array(test_img)
        new_image, max_y = remove_text(test_img)

        min_sum = image.shape[1]
        for y in range(max_y, new_image.shape[0] // 2):
            value = np.sum(255 - new_image[y:y + 1, :]) / 255
            if value < min_sum:
                min_sum = value
                max_y = y

        new_image = new_image[max_y:, :]
        first = 255 - cv2.dilate(255 - copy_img[:, :min_x], np.ones((2, 2), np.uint8))
        second = 255 - cv2.dilate(255 - copy_img[:, min_x:], np.ones((2, 2), np.uint8))
        rectangle = minimal_bounding_rectangle(new_image[:, :min_x])
        rectangle[1] += max_y
        rectangle[3] += max_y
        x11, y11, x12, y12 = auto_correct_rectangle(first,
                                                    rectangle)
        rectangle = minimal_bounding_rectangle(new_image[:, min_x:])
        rectangle[1] += max_y
        rectangle[3] += max_y
        x21, y21, x22, y22 = auto_correct_rectangle(second,
                                                    rectangle)
        images = [image[:, :min_x][y11:y12, x11:x12], image[:, min_x:][y21:y22, x21:x22]]

        for i in range(len(images)):
            images[i] = crop_image(images[i])
            dilated = 255 - cv2.dilate(255 - images[i], np.ones((3, 3), np.uint8))
            dilated, r = remove_rectangle(dilated, 0.9)
            images[i] = images[i][r[1]:r[3], r[0]:r[2]]
            images[i] = crop_image(images[i])
        view_multiple_images_with_image(image, images)  # , f'{v}, {min_x}')


def extract_type2(image):
    image = cv2.resize(image, (450, 400), interpolation=cv2.INTER_AREA)
    image = threshold(image)
    test_image = filter_out_objects(image)
    regions = get_connected_regions(255 - test_image)
    rectangles = []
    for region in regions:
        rectangles.append(get_region_rectangle(region))
    while len(rectangles) > 2:
        distances = []
        for r1_i, r2_i in itertools.combinations(list(range(len(rectangles))), 2):
            r1x1, r1y1, r1x2, r1y2 = rectangles[r1_i]
            r2x1, r2y1, r2x2, r2y2 = rectangles[r2_i]

            d = euclidean_distance(np.array([r1x1 * 0.5 + r1x2 * 0.5, r1y1 * 0.5 + r1y2 * 0.5]),
                                   np.array([r2x1 * 0.5 + r2x2 * 0.5, r2y1 * 0.5 + r2y2 * 0.5]))
            distances.append([d, r1_i, r2_i])

        best_i = int(np.argmin(np.array(distances)[:, 0]))
        r1_i = distances[best_i][1]
        r2_i = distances[best_i][2]

        new_rect = union(rectangles[r1_i], rectangles[r2_i])
        new_rectangles = [new_rect]

        for i in range(len(rectangles)):
            if i == r1_i or i == r2_i:
                continue
            new_rectangles.append(rectangles[i])

        rectangles = new_rectangles
    rectangles[0] = auto_correct_rectangle(test_image, rectangles[0])
    rectangles[1] = auto_correct_rectangle(test_image, rectangles[1])
    r1x1, r1y1, r1x2, r1y2 = rectangles[0]
    r2x1, r2y1, r2x2, r2y2 = rectangles[1]
    images = [image[r1y1:r1y2, r1x1:r1x2],
              image[r2y1:r2y2, r2x1:r2x2]]
    # view_image(image)
    # view_images(images)
    view_multiple_images_with_image(image, images)


def extract_type3(image):
    image = threshold(image)
    max_x = None
    max_x_value = 0
    for x in range(image.shape[1] // 3, image.shape[1] * 2 // 3):
        value = np.sum((255 - image[:, x - 1:x + 1]) / 255)
        if value > max_x_value:
            max_x_value = value
            max_x = x
    max_y = None
    max_y_value = 0
    for y in range(image.shape[0] // 3, image.shape[0] * 2 // 3):
        value = np.sum((255 - image[y - 1:y + 1, :]) / 255)
        if value > max_y_value:
            max_y_value = value
            max_y = y
    images = [image[:max_y, :max_x],
              image[max_y:, :max_x],
              image[:max_y, max_x:],
              image[max_y:, max_x:]]
    for i in range(len(images)):
        images[i] = remove_rectangle(images[i])[0]
        images[i] = crop_image(images[i])
    view_multiple_images_with_image(image, images)


def extract_unknown(image):
    image = threshold(image)
    test_image = filter_out_objects(image)
    test_image = remove_text(test_image)[0]

    regions = get_connected_regions(255 - test_image)
    rectangles = []
    for region in regions:
        rectangles.append(get_region_rectangle(region))
    while len(rectangles) > 2:
        distances = []
        for r1_i, r2_i in itertools.combinations(list(range(len(rectangles))), 2):
            r1x1, r1y1, r1x2, r1y2 = rectangles[r1_i]
            r2x1, r2y1, r2x2, r2y2 = rectangles[r2_i]

            d = euclidean_distance(np.array([r1x1 * 0.5 + r1x2 * 0.5, r1y1 * 0.5 + r1y2 * 0.5]),
                                   np.array([r2x1 * 0.5 + r2x2 * 0.5, r2y1 * 0.5 + r2y2 * 0.5]))
            distances.append([d, r1_i, r2_i])

        best_i = int(np.argmin(np.array(distances)[:, 0]))
        r1_i = distances[best_i][1]
        r2_i = distances[best_i][2]

        new_rect = union(rectangles[r1_i], rectangles[r2_i])
        new_rectangles = [new_rect]

        for i in range(len(rectangles)):
            if i == r1_i or i == r2_i:
                continue
            new_rectangles.append(rectangles[i])

        rectangles = new_rectangles
    rectangles[0] = auto_correct_rectangle(test_image, rectangles[0])
    rectangles[1] = auto_correct_rectangle(test_image, rectangles[1])
    r1x1, r1y1, r1x2, r1y2 = rectangles[0]
    r2x1, r2y1, r2x2, r2y2 = rectangles[1]
    images = [image[r1y1:r1y2, r1x1:r1x2],
              image[r2y1:r2y2, r2x1:r2x2]]
    # view_image(image)
    # view_images(images)
    view_multiple_images_with_image(image, images)


def main2():
    base_dir = '/home/u764/Downloads/cbj_images'
    type1_dir = '/home/u764/Downloads/type1'
    type2_dir = '/home/u764/Downloads/type2'
    type3_dir = '/home/u764/Downloads/type3'

    ensure_directory_exists_and_empty(base_dir, type1_dir)
    ensure_directory_exists_and_empty(base_dir, type2_dir)
    ensure_directory_exists_and_empty(base_dir, type3_dir)

    files = os.listdir(base_dir)
    for f in files:
        try:
            path = os.path.join(base_dir, f)
            image = cv2.imread(path, cv2.IMREAD_GRAYSCALE)

            if is_type1(image)[0] >= 0.8:
                os.rename(path, os.path.join(type1_dir, f))
            elif is_type2(image) >= 0.8:
                os.rename(path, os.path.join(type2_dir, f))
            elif is_type3(image) >= 0.8:
                os.rename(path, os.path.join(type3_dir, f))
        except:
            pass


def main1():
    base_dir = '/home/u764/Downloads/type1'
    sample = random.sample(os.listdir(base_dir), 100)
    # sample = os.listdir(base_dir)[0:100]

    for f in sample:
        try:
            image = cv2.imread(os.path.join(base_dir, f), cv2.IMREAD_GRAYSCALE)
            extract_type1(image)
        except Exception as e:
            logging.exception(e)


def main3():
    type3_dir = '/home/u764/Downloads/type3'
    sample = random.sample(os.listdir(type3_dir), 100)
    # sample = os.listdir(type3_dir)[:100]

    for f in sample:
        try:
            path = os.path.join(type3_dir, f)
            image = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
            extract_type3(image)
        except:
            pass


def main4():
    type2_dir = '/home/u764/Downloads/type2'
    sample = random.sample(os.listdir(type2_dir), 100)
    # sample = os.listdir(type2_dir)[:100]

    for f in sample:
        try:
            path = os.path.join(type2_dir, f)
            image = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
            extract_type2(image)
        except:
            pass


def main5():
    base_dir = '/home/u764/Downloads/cbj_images'

    files = random.sample(os.listdir(base_dir), 100)
    for f in files:
        try:
            path = os.path.join(base_dir, f)
            image = cv2.imread(path, cv2.IMREAD_GRAYSCALE)

            if is_type1(image)[0] >= 0.8:
                #extract_type1(image)
                pass
            elif is_type2(image) >= 0.8:
                #extract_type2(image)
                pass
            elif is_type3(image) >= 0.8:
                #extract_type3(image)
                pass
            else:
                print('here')
                # view_multiple_images_with_image(image,
                extract_unknown(image)
        except:
            pass


if __name__ == '__main__':
    # main4()  # type2
    # main3()  # type3
    # main1()  # type1
    main5()
