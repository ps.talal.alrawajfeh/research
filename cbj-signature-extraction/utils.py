import itertools
import os
import random

import cv2
import math
import numpy as np

from main import view_multiple_images, view_image


def threshold(image):
    return cv2.threshold(
        image,
        0,
        255,
        cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]


def l2_norm(np_array):
    return np.sqrt(np.sum(np.square(np_array)))


def euclidean_distance(np_array1, np_array2):
    return l2_norm(np_array1 - np_array2)


def dilate(image, kernel_size=2):
    return cv2.dilate(image, np.ones((kernel_size, kernel_size)))


def erode(image, kernel_size=2):
    return cv2.erode(image, np.ones((kernel_size, kernel_size)))


def get_connected_region(image, start_point, visits_matrix, foreground_color=255):
    if image[start_point[1], start_point[0]] == 0:
        return []

    region = [start_point]
    queue = [start_point]

    while len(queue) > 0:
        parent = queue.pop(0)
        visits_matrix[parent[1], parent[0]] = 1

        for dy in range(-1, 2):
            for dx in range(-1, 2):
                x, y = parent
                x += dx
                y += dy
                if x < 0 or x >= image.shape[1]:
                    continue
                if y < 0 or y >= image.shape[0]:
                    continue
                if visits_matrix[y, x] == 1:
                    continue
                if image[y, x] == foreground_color and (x, y) not in queue:
                    queue.append((x, y))
                    region.append((x, y))

    return region


def get_connected_regions(image, foreground_color=255):
    visits_matrix = np.zeros(image.shape, np.uint8)

    regions = []
    for y in range(image.shape[0]):
        for x in range(image.shape[1]):
            if image[y, x] == foreground_color and visits_matrix[y, x] == 0:
                regions.append(get_connected_region(image,
                                                    (x, y),
                                                    visits_matrix,
                                                    foreground_color))

    return regions


def to_int_ceil(number):
    return int(math.ceil(number))


def to_int_floor(number):
    return int(math.floor(number))


def to_int_round(number):
    return int(round(number))


def pixels_center_of_mass(pixels):
    np_pixels = np.array(pixels)
    mean_x = to_int_round(np.mean(np_pixels[:, :1]))
    mean_y = to_int_round(np.mean(np_pixels[:, 1:]))
    return mean_x, mean_y


def center_of_mass(image, foreground_color=0):
    mean_x = 0
    mean_y = 0
    count = 0

    for y in range(image.shape[0]):
        for x in range(image.shape[1]):
            if image[y, x] == foreground_color:
                mean_x += x
                mean_y += y
                count += 1

    mean_x /= count
    mean_y /= count

    return to_int_round(mean_x), to_int_round(mean_y)


def divide_optimally(image_size, number_of_regions):
    width, height = image_size

    times = to_int_ceil(math.log2(number_of_regions))

    width_divisions = 0
    height_divisions = 0

    for i in range(times):
        if width > height:
            width /= 2
            width_divisions += 1
        if height > width:
            height /= 2
            height_divisions += 1
        if width == height and width_divisions > height_divisions:
            width /= 2
            width_divisions += 1
        if width == height and height_divisions > width_divisions:
            height /= 2
            width_divisions += 1
        if width == height and height_divisions == width_divisions and i < times - 1:
            width /= 2
            width_divisions += 1
        if width == height and height_divisions == width_divisions and i == times - 1:
            width /= 2
            height /= 2
            width_divisions += 1
            height_divisions += 1

    width, height = image_size

    region_width = width / (width_divisions + 1)
    region_height = height / (height_divisions + 1)
    regions = []

    h = 0
    while h < height:
        w = 0
        while w < width:
            x1 = to_int_floor(w)
            y1 = to_int_floor(h)
            x2 = to_int_ceil(w + region_width)
            y2 = to_int_ceil(h + region_height)
            if x2 > width:
                x2 = width
            if y2 > height:
                y2 = height
            regions.append([x1, y1, x2, y2])
            w += region_width
        h += region_height

    return regions


def kmeans_clustering(points,
                      image_size,
                      k=6,
                      iterations=1000,
                      maximum_centroids=4,
                      minimum_relative_distance=0.05,
                      centroids=None):
    if centroids is None:
        centroids = [np.array([to_int_round(random.random() * image_size[0]),
                               to_int_round(random.random() * image_size[1])]) for _ in range(k)]

    centroid_updated = [False] * k

    for _ in range(iterations):
        for x, y in points:
            p = np.array([x, y])
            c_i = int(np.argmin([euclidean_distance(c, p) for c in centroids]))
            centroids[c_i] = (centroids[c_i] + p) / 2
            centroid_updated[c_i] = True

    # remove non-updated centroids
    centroids = list(
        map(lambda t: t[1],
            filter(lambda t: t[0], zip(centroid_updated, centroids))))

    # merge close centroids
    done = False
    while not done:
        done = True
        for c1, c2 in itertools.combinations(range(len(centroids)), 2):
            relative_distance = euclidean_distance(centroids[c1], centroids[c2]) / l2_norm(np.array(image_size))
            if relative_distance < minimum_relative_distance:
                centroids[c2] = (centroids[c1] + centroids[c2]) / 2
                centroids.pop(c1)
                done = False
                break

    # decrease number of centroids, if necessary
    while len(centroids) > maximum_centroids:
        distances = []
        for c1, c2 in itertools.combinations(range(len(centroids)), 2):
            distances.append([euclidean_distance(centroids[c1], centroids[c2]), c1, c2])
        closest_i = int(np.argmin(np.array(distances)[:, :1]))
        c1 = distances[closest_i][1]
        c2 = distances[closest_i][2]
        centroid1 = centroids[c1]
        centroid2 = centroids[c2]
        new_centroids = []
        for i in range(len(centroids)):
            if i == c1 or i == c2:
                continue
            new_centroids.append(centroids[i])
        centroids = new_centroids
        centroids.append((centroid1 + centroid2) / 2)

    return centroids


def cluster_points(centroids, points):
    clusters = []
    for i in range(len(centroids)):
        clusters.append(list())

    for i in range(len(points)):
        point = points[i]
        distances = [euclidean_distance(c, np.array(point)) for c in centroids]
        c_i = int(np.argmin(distances))
        clusters[c_i].append(point)

    return clusters


def cluster_regions_by_centers(centroids, regions, region_centers):
    region_clusters = []
    for i in range(len(centroids)):
        region_clusters.append(list())

    for i in range(len(region_centers)):
        distances = [euclidean_distance(c, np.array(region_centers[i])) for c in centroids]
        c_i = int(np.argmin(distances))
        region_clusters[c_i].append(regions[i])

    return region_clusters


def get_region_rectangle(region):
    np_region = np.array(region)

    x1 = np.min(np_region[:, :1])
    y1 = np.min(np_region[:, 1:])
    x2 = np.max(np_region[:, :1]) + 1
    y2 = np.max(np_region[:, 1:]) + 1

    return x1, y1, x2, y2


def intersection(rectangle1, rectangle2):
    x11, y11, x12, y12 = rectangle1
    x21, y21, x22, y22 = rectangle2

    if x12 <= x21 or x22 <= x11:
        return 0, 0, 0, 0

    if y12 <= y21 or y22 <= y11:
        return 0, 0, 0, 0

    x1 = max(x11, x21)
    x2 = min(x12, x22)
    y1 = max(y11, y21)
    y2 = min(y12, y22)

    return x1, y1, x2, y2


def union(rectangle1, rectangle2):
    x11, y11, x12, y12 = rectangle1
    x21, y21, x22, y22 = rectangle2

    x1 = min(x11, x21)
    x2 = max(x12, x22)
    y1 = min(y11, y21)
    y2 = max(y12, y22)

    return x1, y1, x2, y2


def rectangle_area(rectangle):
    x1, y1, x2, y2 = rectangle
    return (y2 - y1) * (x2 - x1)


def merge_intersecting_regions(regions, intersection_threshold=0.5):
    rectangles = []
    for region in regions:
        rectangles.append(get_region_rectangle(region))

    done = False
    while not done:
        done = True

        for r1_i, r2_i in itertools.combinations(list(range(len(rectangles))), 2):
            r1 = rectangles[r1_i]
            r2 = rectangles[r2_i]
            r3 = intersection(r1, r2)

            area1 = rectangle_area(r1)
            area2 = rectangle_area(r2)
            area3 = rectangle_area(r3)

            ratio = area3 / min(area1, area2)

            if ratio > intersection_threshold:
                new_regions = []
                new_rectangles = []

                for i in range(len(rectangles)):
                    if i == r1_i or i == r2_i:
                        continue
                    new_regions.append(regions[i])
                    new_rectangles.append(rectangles[i])

                new_region = []
                new_region.extend(regions[r1_i])
                new_region.extend(regions[r2_i])
                new_regions.append(new_region)
                new_rectangles.append(union(r1, r2))

                regions = new_regions
                rectangles = new_rectangles
                done = False
                break

    return regions, rectangles


def get_region_centers(regions):
    centers = []
    for region in regions:
        x1, y1, x2, y2 = get_region_rectangle(region)
        centers.append([to_int_round((x1 + x2) / 2),
                        to_int_round((y1 + y2) / 2)])
    return centers


def minimal_bounding_rectangle(binary_image):
    mask = (255 - binary_image) > 0

    height, width = binary_image.shape[0:2]
    mask1, mask2 = mask.any(0), mask.any(1)
    x1, x2 = mask1.argmax(), width - mask1[::-1].argmax()
    y1, y2 = mask2.argmax(), height - mask2[::-1].argmax()

    return [x1, y1, x2, y2]


def crop_image(binary_image):
    x1, y1, x2, y2 = minimal_bounding_rectangle(binary_image)
    return binary_image[y1:y2, x1:x2]


def auto_correct_rectangle(binary_image, rectangle):
    x1, y1, x2, y2 = rectangle
    while x1 > 0:
        if np.all(binary_image[y1:y2, x1 - 1: x1] == 255):
            break
        x1 -= 1

    while y1 > 0:
        if np.all(binary_image[y1 - 1:y1, x1:x2] == 255):
            break
        y1 -= 1

    while x2 < binary_image.shape[1]:
        if np.all(binary_image[y1:y2, x2: x2 + 1] == 255):
            break
        x2 += 1

    while y2 > 0:
        if np.all(binary_image[y2:y2 + 1, x1:x2] == 255):
            break
        y2 += 1

    return x1, y1, x2, y2


def extract_signatures_by_kmeans(image, number_of_signatures):
    resize_factor = 2
    resized = cv2.resize(image,
                         (to_int_ceil(image.shape[1] / resize_factor),
                          to_int_ceil(image.shape[0] / resize_factor)),
                         interpolation=cv2.INTER_AREA)

    binarized = threshold(resized)
    dilated = dilate(255 - binarized)

    regions = get_connected_regions(dilated)
    region_centers = get_region_centers(regions)
    centroids = kmeans_clustering(region_centers,
                                  (dilated.shape[1], dilated.shape[0]),
                                  k=number_of_signatures * 2,
                                  maximum_centroids=number_of_signatures)
    centroids = [c * resize_factor for c in centroids]

    regions = get_connected_regions(255 - threshold(image))
    region_centers = get_region_centers(regions)
    region_clusters = cluster_regions_by_centers(centroids, regions, region_centers)

    images = []
    for region_cluster in region_clusters:
        cluster = []
        for region in region_cluster:
            cluster.extend(region)
        np_cluster = np.array(cluster, np.int).reshape((-1, 2))

        cluster_image = np.ones(image.shape, np.uint8) * 255
        for x, y in np_cluster:
            cluster_image[y, x] = 0

        x1, y1, x2, y2 = minimal_bounding_rectangle(cluster_image)
        images.append(cluster_image[y1:y2, x1:x2])

    return images


def remove_line(image, axis, direction, line_threshold=0.7):
    max_value_i = None
    max_value = 0

    if direction == 1:
        loop_range = range(0, image.shape[axis] // 4)
    elif direction == -1:
        loop_range = range(image.shape[axis] - 1, image.shape[axis] * 3 // 4, -1)
    else:
        raise Exception(f'direction {direction} is invalid')

    value_denominator = 255 * image.shape[1 - axis]
    for i in loop_range:
        if axis == 1:
            strip = image[:, i: i + 1]
        else:
            strip = image[i: i + 1, :]
        value = np.sum(255 - strip) / value_denominator
        if value > line_threshold and value > max_value:
            max_value = value
            max_value_i = i

    if max_value_i is None:
        if direction == 1:
            min_value_i = 0
        else:
            min_value_i = image.shape[axis] - 1
    else:
        min_value_i = None
        min_value = 1.0

        if direction == 1:
            loop_range = range(max_value_i, image.shape[axis] // 4)
        else:
            loop_range = range(max_value_i, image.shape[axis] * 3 // 4, -1)

        for i in loop_range:
            if axis == 1:
                strip = image[:, i: i + 1]
            else:
                strip = image[i: i + 1, :]
            value = np.sum(255 - strip) / value_denominator
            if value < min_value:
                min_value = value
                min_value_i = i

    if direction == 1:
        if axis == 1:
            return image[:, min_value_i:], min_value_i
        else:
            return image[min_value_i:, :], min_value_i
    else:
        if axis == 1:
            return image[:, :min_value_i + 1], min_value_i
        else:
            return image[:min_value_i + 1, :], min_value_i


def remove_rectangle(image, line_threshold=0.7):
    new_image, x1 = remove_line(image, 1, 1, line_threshold)
    new_image, x2 = remove_line(new_image, 1, -1, line_threshold)
    new_image, y1 = remove_line(new_image, 0, 1, line_threshold)
    new_image, y2 = remove_line(new_image, 0, -1, line_threshold)
    return new_image, [x1, y1, x2, y2]


def main1():
    # image = cv2.imread('/home/u764/Downloads/type2/4336', cv2.IMREAD_GRAYSCALE)
    image = cv2.imread('/home/u764/Downloads/2828', cv2.IMREAD_GRAYSCALE)

    resize_factor = 2
    resized = cv2.resize(image,
                         (to_int_ceil(image.shape[1] / resize_factor),
                          to_int_ceil(image.shape[0] / resize_factor)),
                         interpolation=cv2.INTER_AREA)

    binarized = threshold(resized)
    dilated = dilate(255 - binarized)

    regions = get_connected_regions(dilated)
    regions, rectangles = merge_intersecting_regions(regions)

    for r in rectangles:
        x1, y1, x2, y2 = r
        x1 *= resize_factor
        y1 *= resize_factor
        x2 *= resize_factor
        y2 *= resize_factor
        view_image(image[y1:y2, x1:x2])


def main2():
    # image = cv2.imread('/home/u764/Downloads/2828', cv2.IMREAD_GRAYSCALE)
    base_dir = '/home/u764/Downloads/type2'
    files = os.listdir(base_dir)

    for f in files:
        try:
            image = cv2.imread(os.path.join(base_dir, f), cv2.IMREAD_GRAYSCALE)
            view_image(image)
            view_multiple_images(extract_signatures_by_kmeans(image, 2))
        except:
            pass


if __name__ == '__main__':
    main2()
