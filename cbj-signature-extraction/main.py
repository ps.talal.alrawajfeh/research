import itertools

import numpy as np
import cv2
import math
import os
import random
from matplotlib import pyplot as plt
import shutil


def view_image(image, title=''):
    plt.imshow(image, cmap=plt.cm.gray)
    plt.title(title)
    plt.show()


def view_multiple_images(images, title=''):
    n = int(math.ceil(math.sqrt(len(images))))

    fig, axs = plt.subplots(n, n)

    i = 0
    for r in range(n):
        if i == len(images):
            break
        for c in range(n):
            if i == len(images):
                break
            axs[r, c].imshow(images[i], plt.cm.gray)
            i += 1

    plt.title(title)
    plt.show()


def view_multiple_images_with_image(image, images, title=''):
    n = 2

    fig, axs = plt.subplots(n, n + 1)
    gs = axs[0, 0].get_gridspec()

    for ax in axs[0:, 0]:
        ax.remove()

    ax_big = fig.add_subplot(gs[0:, 0])
    ax_big.imshow(image, plt.cm.gray)

    i = 0
    for r in range(n):
        if i == len(images):
            break
        for c in range(n):
            if i == len(images):
                break
            axs[r, 1 + c].imshow(images[i], plt.cm.gray)
            i += 1

    plt.title(title)
    plt.show()


def get_connected_region(image, start_point, visits_matrix):
    if image[start_point[1], start_point[0]] == 0:
        return []

    region = [start_point]
    queue = [start_point]

    while len(queue) > 0:
        parent = queue.pop(0)
        visits_matrix[parent[1], parent[0]] = 1

        for dy in range(-1, 2):
            for dx in range(-1, 2):
                x, y = parent
                x += dx
                y += dy
                if x < 0 or x >= image.shape[1]:
                    continue
                if y < 0 or y >= image.shape[0]:
                    continue
                if visits_matrix[y, x] == 1:
                    continue
                if image[y, x] == 255 and (x, y) not in queue:
                    queue.append((x, y))
                    region.append((x, y))

    return region


def get_connected_regions(image):
    visits_matrix = np.zeros(image.shape, np.uint8)

    regions = []
    for y in range(image.shape[0]):
        for x in range(image.shape[1]):
            if image[y, x] == 255 and visits_matrix[y, x] == 0:
                regions.append(get_connected_region(image, (x, y), visits_matrix))

    return regions


def l2_norm(np_array):
    return np.sqrt(np.sum(np.square(np_array)))


def euclidean_distance(np_array1, np_array2):
    return l2_norm(np_array1 - np_array2)


def dilate(image, kernel_size=2):
    return cv2.dilate(image, np.ones((kernel_size, kernel_size)))


def erode(image, kernel_size=2):
    return cv2.erode(image, np.ones((kernel_size, kernel_size)))


def threshold(image):
    return cv2.threshold(
        image,
        0,
        255,
        cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]


def to_int_ceil(number):
    return int(math.ceil(number))


def to_int_floor(number):
    return int(math.floor(number))


def remove_lines(image, regions):
    new_regions = []
    for r in regions:
        region = np.array(r)
        x1 = np.min(region[:, :1])
        x2 = np.max(region[:, :1])

        y1 = np.min(region[:, 1:])
        y2 = np.max(region[:, 1:])

        if (x2 - x1) / image.shape[1] < 0.05 and (y2 - y1) / image.shape[0] > 0.9:
            continue

        if (y2 - y1) / image.shape[0] < 0.05 and (x2 - x1) / image.shape[1] > 0.9:
            continue

        if (x2 - x1) / image.shape[1] > 0.9 and (y2 - y1) / image.shape[0] > 0.9:
            continue

        if (x2 - x1) / image.shape[1] < 0.05 and (y2 - y1) / image.shape[0] < 0.05:
            continue

        new_regions.append(region)

    return new_regions


def get_region_centers(regions):
    points = []

    for r in regions:
        np_r = np.array(r)
        center_x = np.mean(np_r[:, :1])
        center_y = np.mean(np_r[:, 1:])
        points.append((center_x, center_y))

    return points


def kmeans_clustering(image, points, k=6):
    centroids = [np.array([random.randint(0, image.shape[1] - 1),
                           random.randint(0, image.shape[0] - 1)]) for _ in range(k)]

    changed = [False] * k

    for _ in range(1000):
        for x, y in points:
            p = np.array([x, y])
            distances = [euclidean_distance(c, p) for c in centroids]

            c_i = int(np.argmin(distances))
            centroids[c_i] = (centroids[c_i] + p) / 2
            changed[c_i] = True

    temp = []

    for i in range(k):
        if not changed[i]:
            continue
        temp.append(centroids[i])

    centroids = temp

    done = False
    while not done:
        done = True
        for c1, c2 in itertools.combinations(range(len(centroids)), 2):
            if euclidean_distance(centroids[c1], centroids[c2]) / l2_norm(np.array(image.shape)) < 0.05:
                centroids.pop(c1)
                done = False
                break

    while len(centroids) > 4:
        distances = []
        for c1, c2 in itertools.combinations(range(len(centroids)), 2):
            distances.append([euclidean_distance(centroids[c1], centroids[c2]), c1, c2])
        closest_i = int(np.argmin(np.array(distances)[:, :1]))
        c1 = distances[closest_i][1]
        c2 = distances[closest_i][2]
        centroid1 = centroids[c1]
        centroid2 = centroids[c2]
        new_centroids = []
        for i in range(len(centroids)):
            if i == c1 or i == c2:
                continue
            new_centroids.append(centroids[i])
        centroids = new_centroids
        centroids.append((centroid1 + centroid2) / 2)

    return centroids


def get_region_clusters(centroids, region_centers, regions):
    region_clusters = []
    for i in range(len(centroids)):
        region_clusters.append(list())

    for i in range(len(region_centers)):
        x, y = region_centers[i]
        p = np.array([x, y])
        distances = [euclidean_distance(c, p) for c in centroids]

        c_i = int(np.argmin(distances))
        region_clusters[c_i].append(regions[i])
    return region_clusters


def extract_signatures(image):
    resize_factor = 2
    resized = cv2.resize(image,
                         (to_int_ceil(image.shape[1] / resize_factor),
                          to_int_ceil(image.shape[0] / resize_factor)),
                         interpolation=cv2.INTER_AREA)

    binarized = threshold(resized)
    dilated = dilate(255 - binarized)

    regions = get_connected_regions(dilated)
    regions = remove_lines(dilated, regions)
    region_centers = get_region_centers(regions)

    centroids = kmeans_clustering(dilated, region_centers)
    region_clusters = get_region_clusters(centroids, region_centers, regions)

    images = []
    for region_cluster in region_clusters:
        cluster = []
        for region in region_cluster:
            cluster.extend(region)

        np_cluster = np.array(cluster, np.int).reshape((-1, 2))
        x1 = np.min(np_cluster[:, :1]) * resize_factor
        y1 = np.min(np_cluster[:, 1:]) * resize_factor
        x2 = np.max(np_cluster[:, :1]) * resize_factor
        y2 = np.max(np_cluster[:, 1:]) * resize_factor

        cluster_image = image[y1:y2, x1:x2]

        images.append(cluster_image)

    return images


def extract_signatures2(image):
    resize_factor = 4
    resized = cv2.resize(image,
                         (to_int_ceil(image.shape[1] / resize_factor),
                          to_int_ceil(image.shape[0] / resize_factor)),
                         interpolation=cv2.INTER_AREA)

    binarized = threshold(resized)
    dilated = dilate(255 - binarized)
    view_image(dilated)
    center_x = np.mean(dilated[:, :1])
    center_y = np.mean(dilated[:, 1:])

    hist_x = np.sum(dilated.T, -1) / (255 * dilated.shape[0])
    hist_y = np.sum(dilated, -1) / (255 * dilated.shape[1])

    plt.plot(list(range(dilated.shape[1])), hist_x)
    plt.title('x-histogram')
    plt.show()

    plt.plot(list(range(dilated.shape[0])), hist_y)
    plt.title('y-histogram')
    plt.show()

    hist_x[hist_x > 0.8] = 0
    hist_x[hist_x >= 0.3] = 1
    hist_x[hist_x < 0.3] = 0

    hist_y[hist_y > 0.8] = 0
    hist_y[hist_y >= 0.3] = 1
    hist_y[hist_y < 0.3] = 0

    plt.plot(list(range(dilated.shape[1])), hist_x)
    plt.title('x-signals')
    plt.show()

    plt.plot(list(range(dilated.shape[0])), hist_y)
    plt.title('y-signals')
    plt.show()


def filter_regions(image, regions):
    filtered_regions = []
    for r in regions:
        region = np.array(r)
        x1 = np.min(region[:, :1])
        x2 = np.max(region[:, :1])

        y1 = np.min(region[:, 1:])
        y2 = np.max(region[:, 1:])

        if (x2 - x1) / image.shape[1] < 0.05 and (y2 - y1) / image.shape[0] < 0.05:
            continue

        filtered_regions.append(r)
    return filtered_regions


def center_of_mass(image, black_color=255):
    points = []
    for y in range(image.shape[0]):
        for x in range(image.shape[1]):
            if image[y, x] == black_color:
                points.append([x, y])
    points = np.array(points)

    return int(round(float(np.mean(points[:, :1])))), int(round(float(np.mean(points[:, 1:]))))


def main1():
    extract_signatures(cv2.imread('/home/u764/Downloads/782',
                                  cv2.IMREAD_GRAYSCALE))


def main2():
    base_dir = '/home/u764/Downloads/cbj_images'
    sample = random.sample(os.listdir(base_dir), 100)

    for f in sample:
        try:
            image = cv2.imread(os.path.join(base_dir, f), cv2.IMREAD_GRAYSCALE)
            images = extract_signatures(image)
            # view_image(image)
            # view_multiple_images(images)
            view_multiple_images_with_image(image, images)
        except:
            pass


def main3():
    image = cv2.imread('/home/u764/Downloads/782', cv2.IMREAD_GRAYSCALE)
    view_image(image)
    extract_signatures2(image)


def main4():
    base_dir = '/home/u764/Downloads/cbj_images'
    type1_dir = '/home/u764/Downloads/type1'

    if os.path.isdir(type1_dir):
        files = os.listdir(type1_dir)
        for f in files:
            original_path = os.path.join(type1_dir, f)
            new_path = os.path.join(base_dir, f)
            os.rename(original_path, new_path)
    else:
        os.mkdir(type1_dir)

    files = os.listdir(base_dir)
    for f in files:
        try:
            path = os.path.join(base_dir, f)
            image = cv2.imread(path, cv2.IMREAD_GRAYSCALE)

            if image.shape[0] / image.shape[1] < 0.4:
                os.rename(path, os.path.join(type1_dir, f))
        except:
            pass


def main5():
    type1_dir = '/home/u764/Downloads/type1'

    files = os.listdir(type1_dir)
    for f in random.sample(files, 200):
        path = os.path.join(type1_dir, f)

        shutil.copy(path, os.path.join('/home/u764/Downloads/sample2', f))


def main6():
    base_dir = '/home/u764/Downloads/sample1'

    ratios = []
    files = os.listdir(base_dir)
    for f in files:
        path = os.path.join(base_dir, f)

        image = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
        ratios.append(image.shape[0] / image.shape[1])

    ratios.sort()
    ratios = ratios[int(len(ratios) * 0.01):int(len(ratios) * 0.99)]

    print(np.mean(ratios))
    print(np.std(ratios))
    print(np.max(ratios))
    print(np.min(ratios))


def main7():
    base_dir = '/home/u764/Downloads/sample1'

    xs = []
    files = os.listdir(base_dir)
    for f in files:
        path = os.path.join(base_dir, f)

        image = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
        image = 255 - threshold(image)

        # --- uncomment this section to make further pre-processing -----------
        # image = dilate(image)
        # regions = get_connected_regions(image)
        # filtered_regions = filter_regions(image, regions)
        # new_image = np.zeros(image.shape)
        # for r in filtered_regions:
        #     for x, y in r:
        #         new_image[y, x] = 255
        # image = new_image
        # ---------------------------------------------------------------------

        x_hist = np.sum(image.T, axis=-1) / 255

        for i in range(len(x_hist)):
            x_hist[i] = x_hist[i] // 5 * 5

        min_x = None
        min_xs = []
        min_value = image.shape[0]
        for x in range(int(image.shape[1] // 4), int((3 * image.shape[1]) // 4)):
            if x_hist[x] < min_value:
                min_value = x_hist[x]
                min_x = x
                min_xs = [x]
            elif abs(x_hist[x] - min_value) < 1e-7:
                min_xs.append(x)

        # print(min_xs)
        xs.append(np.mean(min_xs) / image.shape[1])

    xs.sort()

    xs = xs[int(len(xs) * 0.01):int(len(xs) * 0.99)]

    print(np.mean(xs))
    print(np.std(xs))
    print(np.max(xs))
    print(np.min(xs))


def main8():
    base_dir = '/home/u764/Downloads/sample2'

    xs = []
    ys = []

    files = os.listdir(base_dir)
    for f in files:
        path = os.path.join(base_dir, f)

        image = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
        image = 255 - threshold(image)

        # --- uncomment this section to make further pre-processing -----------
        # image = dilate(image)
        # regions = get_connected_regions(image)
        # filtered_regions = filter_regions(image, regions)
        # new_image = np.zeros(image.shape)
        # for r in filtered_regions:
        #     for x, y in r:
        #         new_image[y, x] = 255
        # image = new_image
        # ---------------------------------------------------------------------

        x, y = center_of_mass(image)
        xs.append(x / image.shape[1])
        ys.append(y / image.shape[0])

    xs.sort()
    ys.sort()

    xs = xs[int(len(xs) * 0.1):int(len(xs) * 0.90)]
    ys = ys[int(len(ys) * 0.1):int(len(ys) * 0.90)]

    print(np.mean(xs))
    print(np.std(xs))
    print(np.max(xs))
    print(np.min(xs))

    print(np.mean(ys))
    print(np.std(ys))
    print(np.max(ys))
    print(np.min(ys))


def main9():
    base_dir = '/home/u764/Downloads/cbj_images'
    type2_dir = '/home/u764/Downloads/type2'

    if os.path.isdir(type2_dir):
        files = os.listdir(type2_dir)
        for f in files:
            original_path = os.path.join(type2_dir, f)
            new_path = os.path.join(base_dir, f)
            os.rename(original_path, new_path)
    else:
        os.mkdir(type2_dir)

    files = os.listdir(base_dir)
    for f in files:
        try:
            path = os.path.join(base_dir, f)
            image = cv2.imread(path, cv2.IMREAD_GRAYSCALE)

            if image.shape[0] / image.shape[1] >= 0.8:
                os.rename(path, os.path.join(type2_dir, f))
        except:
            pass


def main10():
    base_dir = '/home/u764/Downloads/type3'
    files = os.listdir(base_dir)

    ratios = []

    for f in files:
        p = os.path.join(base_dir, f)

        image = cv2.imread(p, cv2.IMREAD_GRAYSCALE)

        ratios.append(image.shape[0] / image.shape[1])

    ratios.sort()
    ratios = ratios[int(len(ratios) * 0.01):int(len(ratios) * 0.99)]

    print(np.mean(ratios))
    print(np.std(ratios))
    print(np.max(ratios))
    print(np.min(ratios))


def main11():
    base_dir = '/home/u764/Downloads/cbj_images'
    type3_dir = '/home/u764/Downloads/type3'

    if os.path.isdir(type3_dir):
        files = os.listdir(type3_dir)
        for f in files:
            original_path = os.path.join(type3_dir, f)
            new_path = os.path.join(base_dir, f)
            os.rename(original_path, new_path)
    else:
        os.mkdir(type3_dir)

    files = os.listdir(base_dir)
    for f in files:
        try:
            path = os.path.join(base_dir, f)
            image = cv2.imread(path, cv2.IMREAD_GRAYSCALE)

            ratio = image.shape[0] / image.shape[1]
            if 0.45 <= ratio <= 0.6:
                os.rename(path, os.path.join(type3_dir, f))
        except:
            pass


def main12():
    base_dir = '/home/u764/Downloads/type3'
    files = os.listdir(base_dir)

    widths = []
    heights = []

    for f in files:
        p = os.path.join(base_dir, f)

        image = cv2.imread(p, cv2.IMREAD_GRAYSCALE)

        widths.append(image.shape[1])
        heights.append(image.shape[0])

    widths.sort()
    heights.sort()

    widths = widths[int(len(widths) * 0.3):int(len(widths) * 0.7)]
    heights = heights[int(len(heights) * 0.3):int(len(heights) * 0.7)]

    print(np.median(widths))
    print(np.mean(widths))
    print(np.std(widths))
    print(np.max(widths))
    print(np.min(widths))
    print('--------------------')
    print(np.median(heights))
    print(np.mean(heights))
    print(np.std(heights))
    print(np.max(heights))
    print(np.min(heights))


def main13():
    base_dir = '/home/u764/Downloads/type1'
    out_dir = '/home/u764/Downloads/type1_out'

    if not os.path.isdir(out_dir):
        os.makedirs(out_dir)
        
    files = os.listdir(base_dir)

    for f in files:
        image = cv2.imread(os.path.join(base_dir, f), cv2.IMREAD_GRAYSCALE)
        image = cv2.resize(image, (730, 200), interpolation=cv2.INTER_AREA)
        cv2.imwrite(os.path.join(out_dir, f + '.bmp'), image)


if __name__ == '__main__':
    main13()
