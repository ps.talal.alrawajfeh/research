import 'dart:collection';

import 'package:tuple/tuple.dart';
import 'package:collection/collection.dart';

class IndicesIterator extends Iterator<List<int>> {
  List<int> _current;
  List<int> _shape;
  bool _started = false;
  bool _done = false;

  IndicesIterator(List<int> shape) {
    _shape = shape;
  }

  @override
  bool moveNext() {
    if (_started == false) {
      _current = List<int>.filled(_shape.length, 0);
      _started = true;
      return true;
    }

    if (_done) {
      _current = null;
      return false;
    }

    for (var i = _current.length - 1; i >= 0; i--) {
      _current[i] += 1;
      if (_current[i] >= _shape[i]) {
        _current[i] = 0;
      } else {
        return true;
      }
    }

    _done = true;
    _current = null;
    return false;
  }

  @override
  List<int> get current => _current == null ? null : [..._current];
}

class Tensor<T> {
  List<int> _shape;
  List<T> _data;
  int _size;
  List<int> _strides;
  int Function(List<int>, int) _indexMapper;
  bool _isView;

  List<int> _initializeStrides(List<int> shape) {
    var strides = List<int>(shape.length);

    var currentStride = 1;
    for (var i = shape.length - 1; i >= 0; i--) {
      strides[i] = currentStride;
      currentStride *= shape[i];
    }

    return strides;
  }

  int _initializeSize(List<int> shape) {
    if (shape.isEmpty) {
      return 0;
    }

    var size = 1;
    for (var i = 0; i < shape.length; i++) {
      var dimension = shape[i];
      if (dimension <= 0) {
        throw Exception(
            'given non-positive number as a dimension: ${dimension}');
      }
      size *= dimension;
    }

    return size;
  }

  Tensor(List<int> shape) {
    _shape = [...shape];
    _size = _initializeSize(shape);
    _strides = _initializeStrides(shape);
    _data = List<T>(_size);
    _indexMapper = (List<int> x, int i) => x[i];
    _isView = false;
  }

  Tensor._fullConstructor(this._shape, this._data, this._size, this._strides,
      this._indexMapper, this._isView);

  List<int> get shape => _shape;
  List<T> get data => _data;
  int get size => _size;
  List<int> get strides => _strides;
  bool get isView => _isView;

  int _dataIndex(List<int> indices) {
    if (indices.isEmpty) {
      throw Exception('invalid indices, the indices array is empty');
    }
    if (_shape.isEmpty) {
      throw Exception('index out of bounds');
    }
    var index = 0;
    for (var i = 0; i < indices.length; i++) {
      if (indices[i] < 0 || indices[i] >= _shape[i]) {
        throw Exception('index out of bounds');
      }
      index += _strides[i] * _indexMapper(indices, i);
    }
    return index;
  }

  Iterator<List<int>> get indicesIterator => IndicesIterator(_shape);

  T getItem(List<int> indices) => _data[_dataIndex(indices)];

  void setItem(List<int> indices, T item) {
    _data[_dataIndex(indices)] = item;
  }

  Tensor<T> reshape(List<int> shape) {
    var newSize = _initializeSize(shape);

    if (newSize != _size) {
      throw Exception('given size is not equal to the original size');
    }

    var data = _data;
    var isView = true;

    if (_isView && _size != _data.length) {
      data = List<T>(_size);

      var iterator = indicesIterator;

      var i = 0;
      while (iterator.moveNext()) {
        data[i] = _data[_dataIndex(iterator.current)];
        i++;
      }

      isView = false;
    }

    return Tensor._fullConstructor(shape, data, newSize,
        _initializeStrides(shape), (List<int> x, int i) => x[i], isView);
  }

  static Tuple2<Tensor<A>, Tensor<B>> broadcast<A, B>(
      Tensor<A> tensor1, Tensor<B> tensor2) {
    var shape1 = tensor1.shape;
    var shape2 = tensor2.shape;

    if (ListEquality().equals(shape1, shape2)) {
      return Tuple2<Tensor<A>, Tensor<B>>(tensor1, tensor2);
    }

    if (!areShapesCompatible(shape1, shape2)) {
      throw Exception('could not broadcast operands together');
    }

    var tensorsPair = makeLargerTensorTrailingShapeEqualToSmallerTensorShape(
        tensor1, tensor2);
    var compatibleTensor1 = tensorsPair.item1;
    var compatibleTensor2 = tensorsPair.item2;

    var dimensions1 = shape1.length;
    var dimensions2 = shape2.length;

    if (dimensions2 < dimensions1) {
      return Tuple2<Tensor<A>, Tensor<B>>(compatibleTensor1,
          stretchTensor(compatibleTensor2, compatibleTensor1.shape));
    } else if (dimensions1 < dimensions2) {
      return Tuple2<Tensor<A>, Tensor<B>>(
          stretchTensor(compatibleTensor1, compatibleTensor2.shape),
          compatibleTensor2);
    } else {
      return tensorsPair;
    }
  }

  static bool areShapesCompatible(List<int> shape1, List<int> shape2) {
    List<int> larger;
    List<int> smaller;

    if (shape1.length > shape2.length) {
      larger = shape1;
      smaller = shape2;
    } else {
      larger = shape2;
      smaller = shape1;
    }

    var largerDimensions = larger.length;
    var smallerDimensions = smaller.length;

    var j = smallerDimensions - 1;
    for (var i = largerDimensions - 1;
        i >= largerDimensions - smallerDimensions;
        i--) {
      if (larger[i] != smaller[j] && larger[i] != 1 && smaller[j] != 1) {
        return false;
      }
      j--;
    }

    return true;
  }

  static Tuple2<Tensor<A>, Tensor<B>>
      makeLargerTensorTrailingShapeEqualToSmallerTensorShape<A, B>(
          Tensor<A> tensor1, Tensor<B> tensor2) {
    var shape1 = tensor1.shape;
    var shape2 = tensor2.shape;

    List<int> larger;
    List<int> smaller;

    var isFirstTensorLarger = false;

    if (shape1.length > shape2.length) {
      larger = shape1;
      smaller = shape2;
      isFirstTensorLarger = true;
    } else {
      larger = shape2;
      smaller = shape1;
    }

    var largerDimensions = larger.length;
    var smallerDimensions = smaller.length;

    var stretched1 = tensor1;
    var stretched2 = tensor2;

    var j = smallerDimensions - 1;
    for (var i = largerDimensions - 1;
        i >= largerDimensions - smallerDimensions;
        i--) {
      if (larger[i] == 1) {
        if (isFirstTensorLarger) {
          stretched1 = stretchTensorAlongDimension(stretched1, i, smaller[j]);
        } else {
          stretched2 = stretchTensorAlongDimension(stretched2, i, smaller[j]);
        }
      }
      if (smaller[j] == 1) {
        if (isFirstTensorLarger) {
          stretched2 = stretchTensorAlongDimension(stretched2, j, larger[i]);
        } else {
          stretched1 = stretchTensorAlongDimension(stretched1, j, larger[i]);
        }
      }
      j--;
    }

    return Tuple2<Tensor<A>, Tensor<B>>(stretched1, stretched2);
  }

  static Tensor<A> stretchTensorAlongDimension<A>(
      Tensor<A> tensor, int dimension, int correspondingDimensionShape) {
    var originalShape = tensor.shape;
    var newShape = [...originalShape];
    newShape[dimension] = correspondingDimensionShape;
    var adjusted = Tensor<A>(newShape);

    var iterator = adjusted.indicesIterator;
    while (iterator.moveNext()) {
      var indices = iterator.current;
      var i = indices[dimension];
      indices[dimension] = 0;
      var item = tensor.getItem(indices);

      indices[dimension] = i;
      adjusted.setItem(indices, item);
    }
    return adjusted;
  }

  static Tensor<A> stretchTensor<A>(
      Tensor<A> tensor, List<int> destinationShape) {
    var stretched = Tensor<A>(destinationShape);
    var iterator = stretched.indicesIterator;
    while (iterator.moveNext()) {
      var destinationIndices = iterator.current;
      stretched.setItem(
          destinationIndices,
          tensor.getItem(
              getTrailingIndices(destinationIndices, tensor.shape.length)));
    }
    return stretched;
  }

  static List<int> getTrailingIndices(
      List<int> indices, int trailingDimensions) {
    return indices.sublist(indices.length - trailingDimensions, indices.length);
  }

  Tensor<T> ravel() {
    return reshape([_size]);
  }

  @override
  String toString() {
    if (shape.isEmpty) {
      return '[]';
    }

    var maxNumberLength = -1;
    var raveledData = ravel().data;

    raveledData.forEach((number) {
      var numberLength = number.toString().length;
      if (numberLength > maxNumberLength) {
        maxNumberLength = numberLength;
      }
    });

    var representation = StringBuffer();

    var indices = List<int>.filled(shape.length, 0);
    var openedParentheses = List<bool>.filled(shape.length, false);

    for (var n = 0; n < size; n++) {
      for (var i = 0; i < indices.length; i++) {
        if (!openedParentheses[i]) {
          representation.write('[');
          openedParentheses[i] = true;
        }
      }

      var numberRepresentation = getItem(indices).toString();
      representation
          .write(' ' * (maxNumberLength - numberRepresentation.length));
      representation.write(numberRepresentation);

      var lastDimensionIndex = shape.length - 1;
      if (indices[lastDimensionIndex] < shape[lastDimensionIndex] - 1) {
        representation.write(', ');
      }

      var closedAnyParentheses = false;
      var numberOfClosedParentheses = 0;

      for (var i = indices.length - 1; i >= 0; i--) {
        indices[i] += 1;
        if (indices[i] >= shape[i]) {
          indices[i] = 0;

          representation.write(']');
          if (i > 0 && indices[i - 1] < shape[i - 1] - 1) {
            representation.write(',');
          }

          openedParentheses[i] = false;
          closedAnyParentheses = true;
          numberOfClosedParentheses++;
        } else {
          break;
        }
      }

      if (closedAnyParentheses) {
        if (numberOfClosedParentheses > 1) {
          representation.write('\n');
        }
        representation.write('\n');
        representation.write(' ' * (shape.length - numberOfClosedParentheses));
      }
    }

    return representation.toString().trim();
  }
}
