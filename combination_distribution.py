#!/usr/bin/python3.6

import pickle
from matplotlib import pyplot as plt
import numpy as np
from scipy import interpolate

DISTRIBUTION_INTERVAL_SIZE = 0.01
MAX_OUTPUT_VALUE = 1.0


# serialize python object into a file
def cache_object(obj, file):
    with open(file, 'wb') as f:
        pickle.dump(obj, f)


# deserialize python object from a file
def load_cached(file):
    with open(file, 'rb') as f:
        return pickle.load(f)


def export_distribution(predictions, test_labels):
    forgery_predictions = []
    genuine_predictions = []

    for i in range(len(predictions)):
        prediction = predictions[i][0]
        label = test_labels[i][0]
        if label == 1:
            genuine_predictions.append(prediction)
        elif label == 0:
            forgery_predictions.append(prediction)

    genuine_predictions = np.array(genuine_predictions)
    print(f'genuines >= 99.9 {len(genuine_predictions[genuine_predictions >= 0.999])}/{len(genuine_predictions)}')
    forgery_predictions = np.array(forgery_predictions)
    print(f'max forgery:{np.max(forgery_predictions)}')

    bins = [DISTRIBUTION_INTERVAL_SIZE * i
            for i in range(int(MAX_OUTPUT_VALUE / DISTRIBUTION_INTERVAL_SIZE) + 1)]

    fig = plt.gcf()
    fig.set_size_inches(18.5, 10.5)
    plt.xlim(-0.02, 1.05)
    plt.xticks(np.arange(0, 1.05, step=0.05))
    plt.hist(forgery_predictions, bins, color='red', label='forgery', log=True)
    plt.hist(genuine_predictions, bins,
             color='blue', label='genuine', log=True)
    plt.legend(loc='best')
    fig.savefig('all-distributions.png', dpi=200)
    plt.close('all')

    fig = plt.gcf()
    fig.set_size_inches(18.5, 10.5)
    plt.xlim(-0.02, 1.05)
    plt.xticks(np.arange(0, 1.05, step=0.05))
    plt.hist(forgery_predictions, bins, color='red', label='forgery', log=True)
    plt.legend(loc='best')
    fig.savefig('forgery-distribution.png', dpi=200)
    plt.close('all')

    fig = plt.gcf()
    fig.set_size_inches(18.5, 10.5)
    plt.xlim(-0.02, 1.05)
    plt.xticks(np.arange(0, 1.05, step=0.05))
    plt.hist(genuine_predictions, bins,
             color='blue', label='genuine', log=True)
    plt.legend(loc='best')
    fig.savefig('genuine-distribution.png', dpi=200)
    plt.close('all')

    return genuine_predictions, forgery_predictions


def main():
    data = np.array(load_cached('test_data.pickle'), np.float32)
    labels = np.array(load_cached('test_labels.pickle'), np.uint8)

    export_distribution(data, labels)
    siamese_mapper = interpolate.interp1d([0.0, 0.8, 1.0],
                                          [0.0, 0.6, 1.0],
                                          kind='linear')

    data = 0.45 * siamese_mapper(data[:, 0:1]) + 0.55 * data[:, 2:3]
    # export_distribution(data, labels)


if __name__ == "__main__":
    main()
