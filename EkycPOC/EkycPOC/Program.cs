﻿using System;
using System.Collections.Generic;
using System.Drawing;
using DlibDotNet;
using Tesseract;
using ZXing;
using ZXing.Common;
using ZXing.QrCode;

namespace EkycPOC
{
    class Program
    {
        static void Main(string[] args)
        {
            // TestTessearct();

            TestDlib();

            // TestZXing();
        }

        private static void TestDlib()
        {
            using var frontalFaceDetector = DlibDotNet.Dlib.GetFrontalFaceDetector();
            var image = DlibDotNet.Dlib.LoadImage<RgbPixel>("/home/u764/Development/progressft/research/EkycPOC/EkycPOC/avatar.png");

            var faces = frontalFaceDetector.Operator(image);
            using var dnn =
                DlibDotNet.Dnn.LossMetric.Deserialize("/home/u764/Development/progressft/research/EkycPOC/EkycPOC/dlib_face_recognition_resnet_model_v1.dat");

            using var predictor =
                ShapePredictor.Deserialize("/home/u764/Development/progressft/research/EkycPOC/EkycPOC/shape_predictor_5_face_landmarks.dat");
            var chips = new List<Matrix<RgbPixel>>();

            foreach (var face in faces)
            {
                Console.WriteLine($"{face.Left}, {face.Top}, {face.Right}, {face.Bottom}");

                var shape = predictor.Detect(image, face);

                var faceChipDetail = Dlib.GetFaceChipDetails(shape, 150, 0.25);
                var faceChip = Dlib.ExtractImageChip<RgbPixel>(image, faceChipDetail);
                var matrix = new Matrix<RgbPixel>(faceChip);
                chips.Add(matrix);

                Dlib.DrawRectangle(image, face, new RgbPixel(255, 0, 0), 1U);
            }

            // Dlib.GetFaceChipDetails()
            var descriptors = dnn.Operator(chips);

            // Get the feature vector
            Console.WriteLine(descriptors[0].Size);
            
            // If we had another face we can use the euclidean distance 

            Dlib.SaveBmp(image, "/home/u764/Downloads/result.bmp");
        }

        private static void TestZXing()
        {
            var result = new ZXing.BarcodeReader().Decode(
                new Bitmap("/home/u764/Development/progressft/research/EkycPOC/EkycPOC/UPC-A-barcode-encoding-12-digits.png"));

            Console.WriteLine(result.Text);
            var binaryBitmap = new BinaryBitmap(
                new GlobalHistogramBinarizer(
                    new BitmapLuminanceSource(new Bitmap("/home/u764/Development/progressft/research/EkycPOC/EkycPOC/qr-code.png"))));
            Console.WriteLine(new QRCodeReader().decode(binaryBitmap).Text);
        }

        private static void TestTessearct()
        {
            TesseractEngine tesseractEngine = new TesseractEngine("/usr/share/tesseract/tessdata/", "eng");

            var image = Pix.LoadFromFile("/home/u764/Downloads/ekyc-data/test.jpg");
            Console.WriteLine(tesseractEngine.Process(image, PageSegMode.SingleWord).GetText());

            image.Dispose();
            tesseractEngine.Dispose();
        }
    }
}
