import os
import pickle
import random
from enum import Enum

import lightning as L
import numpy as np
import torch
import torch.utils.data as data
from lightning.pytorch.callbacks import ModelCheckpoint
from torch import nn
from torchmetrics import Metric

random.seed(123456)
np.random.seed(123456)

MODEL_FILE_NAME = 'siamese'
EVALUATION_REPORT_FILE = 'report.txt'
IMAGE_CLASSES_FILE = 'image_classes.pickle'
SPECIAL_FORGERY_IMAGE_CLASSES_FILE = 'special_forgery_image_classes.pickle'
TRAIN_IMAGE_CLASSES_FILE = 'train_image_classes.pickle'
VALIDATION_IMAGE_CLASSES_FILE = 'validation_image_classes.pickle'
TEST_IMAGE_CLASSES_FILE = 'test_image_classes.pickle'
TRAIN_PAIRS_FILE = 'train_pairs.pickle'
VALIDATION_PAIRS_FILE = 'validation_pairs.pickle'
TEST_PAIRS_FILE = 'test_pairs.pickle'
SVC_MODEL_FILE_NAME = 'svc_model.pickle'

MODEL_INPUT_SHAPE = (1, 160, 224)
GENUINE_LABEL = [1, 0]
FORGERY_LABEL = [0, 1]

# average number of genuines per class = VARIATIONS_PER_IMAGE * 3 + 2
# lower bound of genunines per class = VARIATIONS_PER_IMAGE * 2 + 2
REFERENCES_PER_CLASS = 10
FORGERY_CLASSES_PER_REFERENCE = 10
FORGERIES_PER_FORGERY_CLASS = 10

TRAIN_CLASSES_PERCENTAGE = 0.7
VALIDATION_CLASSES_PERCENTAGE = 0.15
TEST_CLASSES_PERCENTAGE = 0.15

EMBEDDING_VECTOR_SIZE = 1920
LEAKY_RELU_ALPHA = 0.05
LABEL_SMOOTHING_ALPHA = 0.1
LEARNING_RATE = 0.001
CONFIDENCE_PENALTY = 0.1
EPSILON = 1e-7

TRAINING_BATCH_SIZE = 32
EVALUATION_BATCH_SIZE = 64
EPOCHS = 20

REPORT_GENUINE_THRESHOLD = 0.5
REPORT_HISTOGRAM_BIN_SIZE = 5

SPECIAL_FORGERIES_ENABLED = False
VARIATIONS_PER_IMAGE = 4

SPECIAL_FORGERY_VARIATIONS = 1
SPECIAL_FORGERY_CLASSES = FORGERY_CLASSES_PER_REFERENCE // 2
SPECIAL_FORGERY_PER_CLASS = 1


class TrainingMode(Enum):
    TrainWithSplittingClasses = 1
    TrainWithoutSplittingClasses = 2


TRAINING_MODE = TrainingMode.TrainWithSplittingClasses


def cache_object(obj, file):
    with open(file, 'wb') as f:
        pickle.dump(obj, f)


def load_cached(file):
    with open(file, 'rb') as f:
        return pickle.load(f)


def file_exists(file):
    return os.path.isfile(file)


class MinimumRecurrenceRateRandomSamplerWithReplacement:
    def __init__(self, array) -> None:
        self.array = array
        self.left_choices = array.copy()

    def next(self):
        if len(self.left_choices) == 0:
            self.left_choices = self.array.copy()
        i = random.randint(0, len(self.left_choices) - 1)
        return self.left_choices.pop(i)


def generate_pairs(image_classes):
    keys = [*image_classes]
    number_of_classes = len(keys)

    references = []
    others = []
    labels = []

    for c in range(number_of_classes - FORGERY_CLASSES_PER_REFERENCE):
        c = random.randint(0, len(keys) - 1)
        key = keys.pop(c)

        reference_indices = random.sample(list(range(len(image_classes[key]))),
                                          min(REFERENCES_PER_CLASS, len(image_classes[key])))

        for r in reference_indices:
            reference = [key, r]
            genuines = [[key, i] for i in range(len(image_classes[key]))]
            references.extend([reference] * len(genuines))
            others.extend(genuines)
            labels.extend([GENUINE_LABEL] * len(genuines))

            forgery_keys = random.sample(keys, min(FORGERY_CLASSES_PER_REFERENCE, len(keys)))
            for forgery_key in forgery_keys:
                forgery_class = image_classes[forgery_key]
                forgery_indices = random.sample(list(range(len(forgery_class))),
                                                min(FORGERIES_PER_FORGERY_CLASS, len(forgery_class)))
                forgeries = [[forgery_key, f] for f in forgery_indices]

                references.extend([reference] * len(forgeries))
                others.extend(forgeries)
                labels.extend([FORGERY_LABEL] * len(forgeries))

    return references, others, labels


def class_train_validation_test_split(image_classes):
    keys = [*image_classes]

    keys_indices = list(range(len(keys)))
    random.shuffle(keys_indices)

    classes = (dict(), dict(), dict())
    percentages = [TRAIN_CLASSES_PERCENTAGE,
                   VALIDATION_CLASSES_PERCENTAGE,
                   TEST_CLASSES_PERCENTAGE]

    for i in range(3):
        chosen_key_indices = keys_indices[:int(len(image_classes) * percentages[i])]
        for j in range(len(chosen_key_indices)):
            key = keys[chosen_key_indices[j]]
            classes[i][key] = image_classes[key]
        keys_indices = keys_indices[len(chosen_key_indices):]

    return classes


def shuffle_pairs(pairs):
    references, others, true_outputs = pairs
    indices = list(range(len(references)))
    random.shuffle(indices)

    shuffled_references = [references[indices[i]] for i in range(len(references))]
    shuffled_others = [others[indices[i]] for i in range(len(references))]
    shuffled_true_outputs = [true_outputs[indices[i]] for i in range(len(references))]

    return shuffled_references, shuffled_others, shuffled_true_outputs


def to_float_array(np_array):
    return np.array(np_array, np.float32)


class SiamesePairs(torch.utils.data.Dataset):
    def __init__(self,
                 image_classes,
                 pairs):
        self.count = len(pairs[0])
        self.image_classes = image_classes
        self.references, self.others, self.true_labels = shuffle_pairs(pairs)

    def __len__(self):
        return self.count

    def __getitem__(self, index):
        reference_key_index = self.references[index]
        other_pair = self.others[index]
        reference_image = self.image_classes[reference_key_index[0]][reference_key_index[1]]
        other_image = self.image_classes[other_pair[0]][other_pair[1]]

        reference_image = reference_image.astype(np.float32).reshape(MODEL_INPUT_SHAPE) / 255.0
        other_image = other_image.astype(np.float32).reshape(MODEL_INPUT_SHAPE) / 255.0

        reference_image = torch.tensor(reference_image, dtype=torch.float32)
        other_image = torch.tensor(other_image, dtype=torch.float32)
        label = torch.tensor(to_float_array(self.true_labels[index]), dtype=torch.float32)

        return reference_image, other_image, label

    def on_epoch_end(self):
        self.references, self.others, self.true_labels = shuffle_pairs([self.references,
                                                                        self.others,
                                                                        self.true_labels])


class ConvBatchNormLeakyReLU(nn.Module):
    def __init__(self, input_filters, output_filters, kernel_size, padding, leaky_relu_alpha):
        super().__init__()
        self.leaky_relu_alpha = leaky_relu_alpha
        self.conv = nn.Conv2d(input_filters,
                              output_filters,
                              kernel_size,
                              padding=padding,
                              bias=False)
        torch.nn.init.xavier_normal_(self.conv.weight)
        self.batch_norm = nn.BatchNorm2d(output_filters)

    def forward(self, x):
        y = self.conv(x)
        y = self.batch_norm(y)
        y = nn.functional.leaky_relu(y, negative_slope=self.leaky_relu_alpha)
        return y


class EmbeddingNet(nn.Module):
    def __init__(self, leaky_relu_alpha):
        super().__init__()

        self.input_batch_norm = nn.BatchNorm2d(1)

        self.conv_batch_norm_leaky_relu1 = ConvBatchNormLeakyReLU(
            1,
            64,
            (3, 3),
            'same',
            leaky_relu_alpha)
        self.conv_batch_norm_leaky_relu2 = ConvBatchNormLeakyReLU(
            64,
            64,
            (3, 3),
            'same',
            leaky_relu_alpha)

        self.conv_batch_norm_leaky_relu3 = ConvBatchNormLeakyReLU(
            64,
            64,
            (3, 3),
            'same',
            leaky_relu_alpha)
        self.conv_batch_norm_leaky_relu4 = ConvBatchNormLeakyReLU(
            64,
            64,
            (3, 3),
            'same',
            leaky_relu_alpha)

        self.conv_batch_norm_leaky_relu5 = ConvBatchNormLeakyReLU(
            64,
            64,
            (3, 3),
            'same',
            leaky_relu_alpha)
        self.conv_batch_norm_leaky_relu6 = ConvBatchNormLeakyReLU(
            64,
            64,
            (3, 3),
            'same',
            leaky_relu_alpha)

        self.conv_batch_norm_leaky_relu7 = ConvBatchNormLeakyReLU(
            64,
            64,
            (3, 3),
            'same',
            leaky_relu_alpha)
        self.conv_batch_norm_leaky_relu8 = ConvBatchNormLeakyReLU(
            64,
            64,
            (3, 3),
            'same',
            leaky_relu_alpha)

        self.conv_batch_norm_leaky_relu9 = ConvBatchNormLeakyReLU(
            64,
            64,
            (3, 3),
            'same',
            leaky_relu_alpha)
        self.conv_batch_norm_leaky_relu10 = ConvBatchNormLeakyReLU(
            64,
            64,
            (3, 3),
            'same',
            leaky_relu_alpha)

        self.conv_batch_norm_leaky_relu11 = ConvBatchNormLeakyReLU(
            64,
            64,
            (2, 2),
            'same',
            leaky_relu_alpha)
        self.conv_batch_norm_leaky_relu12 = ConvBatchNormLeakyReLU(
            64,
            64,
            (2, 2),
            'valid',
            leaky_relu_alpha)

        self.conv_batch_norm_leaky_relu13 = ConvBatchNormLeakyReLU(
            64,
            64,
            (2, 3),
            'same',
            leaky_relu_alpha)
        self.conv_batch_norm_leaky_relu14 = ConvBatchNormLeakyReLU(
            64,
            64,
            (2, 3),
            'valid',
            leaky_relu_alpha)

    def forward(self, x):
        y = self.input_batch_norm(x)

        y = self.conv_batch_norm_leaky_relu1(y)
        conv1 = self.conv_batch_norm_leaky_relu2(y)
        y = nn.functional.avg_pool2d(conv1, (2, 2))

        y = self.conv_batch_norm_leaky_relu3(y)
        conv2 = self.conv_batch_norm_leaky_relu4(y)
        y = nn.functional.avg_pool2d(conv2, (2, 2))

        y = self.conv_batch_norm_leaky_relu5(y)
        conv3 = self.conv_batch_norm_leaky_relu6(y)
        y = nn.functional.avg_pool2d(conv3, (2, 2))

        y = self.conv_batch_norm_leaky_relu7(y)
        conv4 = self.conv_batch_norm_leaky_relu8(y)
        y = nn.functional.avg_pool2d(conv4, (2, 2))

        y = self.conv_batch_norm_leaky_relu9(y)
        conv5 = self.conv_batch_norm_leaky_relu10(y)
        y = nn.functional.avg_pool2d(conv5, (2, 2))

        y = self.conv_batch_norm_leaky_relu11(y)
        conv6 = self.conv_batch_norm_leaky_relu12(y)
        y = nn.functional.avg_pool2d(conv6, (2, 2))

        y = self.conv_batch_norm_leaky_relu13(y)
        conv7 = self.conv_batch_norm_leaky_relu14(y)

        features1 = torch.mean(conv1, dim=(2, 3))
        features2 = torch.mean(conv2, dim=(2, 3))
        features3 = torch.mean(conv3, dim=(2, 3))
        features4 = torch.mean(conv4, dim=(2, 3))
        features5 = torch.mean(conv5, dim=(2, 3))
        features6 = torch.mean(conv6, dim=(2, 3))
        features7 = torch.mean(conv7, dim=(2, 3))

        return torch.concatenate([features1,
                                  features2,
                                  features3,
                                  features4,
                                  features5,
                                  features6,
                                  features7], dim=1)


class SiameseNet(nn.Module):
    def __init__(self):
        super().__init__()

        self.embedding_net = EmbeddingNet(0.05)

        self.dropout = nn.Dropout(p=0.5)

        self.fc = nn.Linear(448, 128)
        torch.nn.init.xavier_normal_(self.fc.weight)

        self.gamma = nn.Parameter(torch.tensor(np.array([0.5], np.float32), dtype=torch.float32), requires_grad=True)
        self.a = nn.Parameter(torch.tensor(np.array([1.0], np.float32), dtype=torch.float32), requires_grad=True)
        self.b = nn.Parameter(torch.tensor(np.array([-1.0], np.float32), dtype=torch.float32), requires_grad=True)

    def forward(self, x1, x2):
        y1 = self.embedding_net(x1)
        y2 = self.embedding_net(x2)

        y = torch.abs(y1 - y2)
        y = self.dropout(y)

        y = self.fc(y)
        y = nn.functional.leaky_relu(y, negative_slope=0.05)

        y = torch.norm(y, dim=-1, keepdim=True)
        y = torch.exp(-torch.pow(self.gamma, 2) * y)
        y = self.a * y + self.b
        y = nn.functional.sigmoid(y)

        return torch.concatenate([y, 1 - y], dim=-1)


class LossMetric(Metric):
    is_differentiable = False
    higher_is_better = False
    full_state_update = True

    def __init__(self):
        super().__init__()
        self.add_state("loss_sum", default=torch.tensor(0.0, dtype=torch.float32), dist_reduce_fx="sum")
        self.add_state("count", default=torch.tensor(0), dist_reduce_fx="sum")

    def update(self, preds, target):
        # loss = torch.mean(torch.maximum(torch.tensor(0, dtype=torch.float32), target + preds - 2.0 * target * preds))
        loss = torch.mean(-torch.sum(torch.log(preds) * target, dim=-1))
        self.loss_sum += loss
        self.count += 1

    def compute(self):
        return self.loss_sum / self.count


class FalseAcceptanceMetric(Metric):
    is_differentiable = False
    higher_is_better = False
    full_state_update = True

    def __init__(self):
        super().__init__()
        self.add_state("negatives", default=torch.tensor(0.0, dtype=torch.float32), dist_reduce_fx="sum")
        self.add_state("false_positives", default=torch.tensor(0.0, dtype=torch.float32), dist_reduce_fx="sum")

    def update(self, preds, target):
        negatives_mask = target[:, 0] <= 0.5
        negatives = torch.sum(negatives_mask.float())
        false_positives = torch.sum(((preds[:, 0] > 0.5) & negatives_mask).float())

        self.false_positives += false_positives
        self.negatives += negatives

    def compute(self):
        return self.false_positives / self.negatives


class FalseRejectionMetric(Metric):
    is_differentiable = False
    higher_is_better = False
    full_state_update = True

    def __init__(self):
        super().__init__()
        self.add_state("positives", default=torch.tensor(0.0, dtype=torch.float32), dist_reduce_fx="sum")
        self.add_state("false_negatives", default=torch.tensor(0.0, dtype=torch.float32), dist_reduce_fx="sum")

    def update(self, preds, target):
        positives_mask = target[:, 0] > 0.5
        positives = torch.sum(positives_mask.float())
        false_negatives = torch.sum(((preds[:, 0] <= 0.5) & positives_mask).float())

        self.false_negatives += false_negatives
        self.positives += positives

    def compute(self):
        return self.false_negatives / self.positives


class AccuracyMetric(Metric):
    is_differentiable = False
    higher_is_better = True
    full_state_update = True

    def __init__(self):
        super().__init__()
        self.add_state("correct_sum", default=torch.tensor(0.0, dtype=torch.float32), dist_reduce_fx="sum")
        self.add_state("count", default=torch.tensor(0), dist_reduce_fx="sum")

    def update(self, preds, target):
        self.correct_sum += torch.mean(((preds[:, 0] > 0.5) == (target[:, 0] > 0.5)).float())
        self.count += 1

    def compute(self):
        return self.correct_sum / self.count


def append_lines_to_report(lines):
    with open(EVALUATION_REPORT_FILE, 'a') as f:
        f.writelines(lines)


def epoch_metrics_log(epoch, logs):
    line = f'epoch: {epoch}'
    line += ' - '
    line += f'loss: {logs["loss"]}'
    line += ' - '
    line += f'accuracy: {logs["accuracy"]}'
    line += ' - '
    line += f'validation loss: {logs["val_loss"]}'
    line += ' - '
    line += f'validation binary accuracy: {logs["val_accuracy"]}'
    return line


class LightningSiameseNet(L.LightningModule):
    def __init__(self, siamese_net):
        super().__init__()
        self.siamese_net = siamese_net

        self.loss_metric = LossMetric()
        self.accuracy_metric = AccuracyMetric()
        self.false_acceptance_metric = FalseAcceptanceMetric()
        self.false_rejection_metric = FalseRejectionMetric()

        self.val_loss_metric = LossMetric()
        self.val_accuracy_metric = AccuracyMetric()
        self.val_false_acceptance_metric = FalseAcceptanceMetric()
        self.val_false_rejection_metric = FalseRejectionMetric()

    def training_step(self, batch, _):
        x1, x2, y = batch
        y_hat = self.siamese_net(x1, x2)
        # loss = nn.functional.cross_entropy(y_hat, y)
        # (1.0 - y_hat) * y + y_hat * (1.0 - y)
        # l = y[:, 0]
        # loss = torch.mean(torch.maximum(torch.tensor(0, dtype=torch.float32), l + y_hat - 2.0 * l * y_hat))

        self.loss_metric.update(y_hat, y)
        self.accuracy_metric.update(y_hat, y)
        self.false_acceptance_metric.update(y_hat, y)
        self.false_rejection_metric.update(y_hat, y)

        self.log('loss', self.loss_metric.compute(), prog_bar=True)
        self.log('accuracy', self.accuracy_metric.compute(), prog_bar=True)
        self.log('far', self.false_acceptance_metric.compute(), prog_bar=True)
        self.log('frr', self.false_rejection_metric.compute(), prog_bar=True)

        return torch.mean(-torch.sum(torch.log(y_hat) * y, dim=-1))

    def validation_step(self, batch, _):
        x1, x2, y = batch
        y_hat = self.siamese_net(x1, x2)

        self.val_loss_metric.update(y_hat, y)
        self.val_accuracy_metric.update(y_hat, y)
        self.val_false_acceptance_metric.update(y_hat, y)
        self.val_false_rejection_metric.update(y_hat, y)

        self.log('val_loss', self.val_loss_metric.compute(), prog_bar=True)
        self.log('val_accuracy', self.val_accuracy_metric.compute(), prog_bar=True)
        self.log('val_far', self.val_false_acceptance_metric.compute(), prog_bar=True)
        self.log('val_frr', self.val_false_rejection_metric.compute(), prog_bar=True)

    def on_validation_end(self):
        append_lines_to_report(
            epoch_metrics_log(self.global_step, {
                'loss': self.loss_metric.compute(),
                'accuracy': self.accuracy_metric.compute(),
                'val_loss': self.val_loss_metric.compute(),
                'val_accuracy': self.val_accuracy_metric.compute()
            }) + '\n'
        )

        self.loss_metric.reset()
        self.accuracy_metric.reset()
        self.false_acceptance_metric.reset()
        self.false_rejection_metric.reset()

        self.val_loss_metric.reset()
        self.val_accuracy_metric.reset()
        self.val_false_acceptance_metric.reset()
        self.val_false_rejection_metric.reset()

    def configure_optimizers(self):
        return torch.optim.Adam(self.parameters(), lr=1e-3)


def train(train_image_classes,
          train_pairs,
          validation_image_classes,
          validation_pairs):
    siamese_net = SiameseNet()

    siamese_train_pairs = SiamesePairs(train_image_classes,
                                       train_pairs)
    siamese_validation_pairs = SiamesePairs(validation_image_classes,
                                            validation_pairs)

    append_lines_to_report('[training log]\n\n')

    model_checkpoint = ModelCheckpoint(
        save_top_k=-1,
        dirpath='./',
        filename='siamese-torch-{epoch:02d}-{val_loss:.2f}-{val_accuracy:.2f}')

    best_model_checkpoint = ModelCheckpoint(
        dirpath='./',
        monitor='val_loss',
        mode='min',
        filename='siamese-torch-best'
    )

    trainer = L.Trainer(max_epochs=20, callbacks=[model_checkpoint, best_model_checkpoint])
    trainer.fit(LightningSiameseNet(siamese_net),
                data.DataLoader(siamese_train_pairs, batch_size=TRAINING_BATCH_SIZE),
                data.DataLoader(siamese_validation_pairs, batch_size=EVALUATION_BATCH_SIZE))


def load_or_supply_and_cache(cache_file, supplier_lambda):
    if file_exists(cache_file):
        return load_cached(cache_file)

    result = supplier_lambda()
    cache_object(result, cache_file)
    return result


def train_model(train_image_classes,
                validation_image_classes):
    train_pairs = load_or_supply_and_cache(TRAIN_PAIRS_FILE,
                                           lambda: generate_pairs(train_image_classes))
    validation_pairs = load_or_supply_and_cache(VALIDATION_PAIRS_FILE,
                                                lambda: generate_pairs(validation_image_classes))

    train_refs, train_others, train_labels = train_pairs
    indices = random.sample(range(len(train_refs)), 100 * 64)

    train_pairs = ([train_refs[i] for i in indices],
                   [train_others[i] for i in indices],
                   [train_labels[i] for i in indices])

    val_refs, val_others, val_labels = validation_pairs
    indices = random.sample(range(len(val_refs)), 100 * 64)

    validation_pairs = ([val_refs[i] for i in indices],
                        [val_others[i] for i in indices],
                        [val_labels[i] for i in indices])

    train(train_image_classes,
          train_pairs,
          validation_image_classes,
          validation_pairs)


def run_pipeline():
    image_classes = None
    train_image_classes = None
    validation_image_classes = None
    test_image_classes = None

    if not file_exists(TRAIN_IMAGE_CLASSES_FILE) or \
            not file_exists(VALIDATION_IMAGE_CLASSES_FILE) or \
            not file_exists(TEST_IMAGE_CLASSES_FILE):
        if image_classes is None:
            image_classes = load_cached(IMAGE_CLASSES_FILE)
        (train_image_classes,
         validation_image_classes,
         test_image_classes) = class_train_validation_test_split(image_classes)

        if TRAINING_MODE == TrainingMode.TrainWithoutSplittingClasses:
            train_image_classes = image_classes

        cache_object(train_image_classes, TRAIN_IMAGE_CLASSES_FILE)
        cache_object(validation_image_classes, VALIDATION_IMAGE_CLASSES_FILE)
        cache_object(test_image_classes, TEST_IMAGE_CLASSES_FILE)

    if train_image_classes is None or \
            validation_image_classes is None:
        train_image_classes = load_cached(TRAIN_IMAGE_CLASSES_FILE)
        validation_image_classes = load_cached(VALIDATION_IMAGE_CLASSES_FILE)
    train_model(train_image_classes,
                validation_image_classes)


if __name__ == '__main__':
    run_pipeline()
