import time

import lightning as L
import numpy as np
import tensorflow as tf
import torch
import torch.nn as nn
import torch.utils.data as data

keras = tf.keras
from keras.datasets import mnist
from torchmetrics import Metric

INPUT_SIZE = (28, 28)


class LossMetric(Metric):
    is_differentiable = False
    higher_is_better = False
    full_state_update = True

    def __init__(self):
        super().__init__()
        self.add_state("loss_sum", default=torch.tensor(0.0, dtype=torch.float32), dist_reduce_fx="sum")
        self.add_state("count", default=torch.tensor(0), dist_reduce_fx="sum")

    def update(self, preds, target):
        loss = torch.mean(-torch.sum(torch.log(preds) * target, dim=-1))
        self.loss_sum += loss
        self.count += 1

    def compute(self):
        return self.loss_sum / self.count


class ConvNet(L.LightningModule):
    def __init__(self):
        super().__init__()

        self.input_batch_norm = nn.BatchNorm2d(1)

        self.conv1 = nn.Conv2d(1,
                               32,
                               (3, 3),
                               padding='same',
                               bias=False)
        torch.nn.init.xavier_uniform_(self.conv1.weight)
        self.batch_norm1 = nn.BatchNorm2d(32)

        self.conv2 = nn.Conv2d(32,
                               64,
                               (3, 3),
                               padding='same',
                               bias=False)
        torch.nn.init.xavier_uniform_(self.conv2.weight)
        self.batch_norm2 = nn.BatchNorm2d(64)

        self.conv3 = nn.Conv2d(64,
                               128,
                               (3, 3),
                               padding='same',
                               bias=False)
        torch.nn.init.xavier_uniform_(self.conv3.weight)
        self.batch_norm3 = nn.BatchNorm2d(128)

        self.fc1 = nn.Linear(128, 128)
        torch.nn.init.xavier_uniform_(self.fc1.weight)
        self.fc2 = nn.Linear(128, 10)
        torch.nn.init.xavier_uniform_(self.fc2.weight)

        self.loss_metric = LossMetric()

    def training_step(self, batch, batch_idx):
        x, y = batch

        y_hat = self.input_batch_norm(x)

        y_hat = self.conv1(y_hat)
        y_hat = self.batch_norm1(y_hat)
        y_hat = nn.functional.relu(y_hat)
        y_hat = nn.functional.avg_pool2d(y_hat, (2, 2))

        y_hat = self.conv2(y_hat)
        y_hat = self.batch_norm2(y_hat)
        y_hat = nn.functional.relu(y_hat)
        y_hat = nn.functional.avg_pool2d(y_hat, (2, 2))

        y_hat = self.conv3(y_hat)
        y_hat = self.batch_norm3(y_hat)
        y_hat = nn.functional.relu(y_hat)
        y_hat = torch.mean(y_hat, dim=(2, 3))

        y_hat = self.fc1(y_hat)
        y_hat = nn.functional.relu(y_hat)

        y_hat = self.fc2(y_hat)
        y_hat = nn.functional.softmax(y_hat)

        loss = torch.mean(-torch.sum(torch.log(y_hat) * y, dim=-1))
        self.loss_metric.update(y_hat, y)
        self.log("train_loss", self.loss_metric.compute(), prog_bar=True)

        return loss

    def configure_optimizers(self):
        return torch.optim.Adam(self.parameters(), lr=1e-3)


class MNISTDataset(torch.utils.data.Dataset):
    def __init__(self, images, labels):
        self.images = torch.tensor(images, dtype=torch.float32)
        self.labels = torch.tensor(labels, dtype=torch.float32)

    def __len__(self):
        return len(self.images)

    def __getitem__(self, idx):
        return self.images[idx], self.labels[idx]


def main():
    (x_train, y_train), (x_test, y_test) = mnist.load_data()

    x_train = x_train.reshape(-1, 1, 28, 28).astype(np.float32) / 255.0
    y_one_hot_train = np.zeros((y_train.shape[0], 10), np.float32)
    for i in range(y_train.shape[0]):
        y_one_hot_train[i, y_train[i]] = 1.0

    x_test = x_test.reshape(-1, 1, 28, 28).astype(np.float32) / 255.0
    y_one_hot_test = np.zeros((y_test.shape[0], 10), np.float32)
    for i in range(y_test.shape[0]):
        y_one_hot_test[i, y_test[i]] = 1.0

    start_time = time.time()

    trainer = L.Trainer(max_epochs=1)
    mnist_dataset = MNISTDataset(x_train, y_one_hot_train)
    data_loader = data.DataLoader(mnist_dataset, batch_size=100)
    trainer.fit(ConvNet(),
                data_loader)

    end_time = time.time()
    print(end_time - start_time)


if __name__ == '__main__':
    main()
