import pickle
import random

import numpy as np
import tensorflow as tf
from tqdm import tqdm

INPUT_IMAGE_SHAPE = (96, 144, 1)
PRE_TRAINED_MODEL_PRE_PROCESSOR = lambda img: img.reshape(INPUT_IMAGE_SHAPE).astype(np.float32) / 255.0
PRE_TRAINED_MODEL = tf.keras.models.load_model('embedding.h5', compile=False)

tf.random.set_seed(123456)
random.seed(123456)
np.random.seed(123456)


def images_to_feature_vectors(image_batch,
                              model=PRE_TRAINED_MODEL,
                              pre_processor=PRE_TRAINED_MODEL_PRE_PROCESSOR):
    return model(np.array([pre_processor(image) for image in image_batch])).numpy()


def cache_object(obj, file):
    with open(file, 'wb') as f:
        pickle.dump(obj, f)


def load_cached(file):
    with open(file, 'rb') as f:
        return pickle.load(f)


class ImageBatchVectorizer:
    def __init__(self, batch_size=128):
        self.__images = []
        self.__classes = []
        self.__feature_vector_classes = dict()
        self.__batch_size = batch_size

    def __predict(self):
        if len(self.__images) == 0:
            return
        feature_vectors = images_to_feature_vectors(self.__images)
        for i in range(len(self.__classes)):
            c = self.__classes[i]
            if c not in self.__feature_vector_classes:
                self.__feature_vector_classes[c] = []
            self.__feature_vector_classes[c].append(feature_vectors[i])
        self.__images = []
        self.__classes = []

    def __predict_if_batch_size_reached(self):
        if len(self.__images) >= self.__batch_size:
            self.__predict()

    def feed_images(self, images, classes):
        for i in range(len(images)):
            self.__images.append(images[i])
            self.__classes.append(classes[i])
            self.__predict_if_batch_size_reached()

    def feed_image(self, image, image_class):
        self.__images.append(image)
        self.__classes.append(image_class)
        self.__predict_if_batch_size_reached()

    def finalize(self):
        self.__predict()

    def get_feature_vectors(self):
        return self.__feature_vector_classes


def generate_feature_vectors(image_classes, file_name):
    progress_bar = tqdm(total=len(image_classes))
    image_batch_vectorizer = ImageBatchVectorizer()
    for c in image_classes:
        batch = image_classes[c]
        image_batch_vectorizer.feed_images(batch, [c] * len(batch))
        progress_bar.update()
    image_batch_vectorizer.finalize()
    progress_bar.close()
    cache_object(image_batch_vectorizer.get_feature_vectors(), file_name)


def run():
    generate_feature_vectors(load_cached('train_image_classes.pickle'),
                             'train_feature_vector_classes.pickle')
    generate_feature_vectors(load_cached('validation_image_classes.pickle'),
                             'validation_feature_vector_classes.pickle')
    generate_feature_vectors(load_cached('test_image_classes.pickle'),
                             'test_feature_vector_classes.pickle')


if __name__ == '__main__':
    run()
