import math
import os
import pickle
import random
import shutil
from enum import Enum

import numpy as np
import tensorflow as tf
from tqdm import tqdm

from reporting import generate_report

keras = tf.keras
from keras.models import Model
from keras.layers import Input, Lambda, Dense, LeakyReLU, \
    Concatenate, Reshape, Add
from keras.losses import binary_crossentropy
from keras.models import load_model
from keras.utils import Sequence
from keras.callbacks import LambdaCallback, ModelCheckpoint
from keras.initializers import GlorotNormal
from keras.optimizers import Adam

tf.random.set_seed(123456)
random.seed(123456)
np.random.seed(123456)

MODEL_FILE_NAME = 'siamese_vit'
EVALUATION_REPORT_FILE = 'siamese_vit_report.txt'
IMAGE_CLASSES_FILE = 'image_classes.pickle'
SPECIAL_FORGERY_IMAGE_CLASSES_FILE = 'special_forgery_image_classes.pickle'
TRAIN_IMAGE_CLASSES_FILE = 'train_image_classes.pickle'
VALIDATION_IMAGE_CLASSES_FILE = 'validation_image_classes.pickle'
TEST_IMAGE_CLASSES_FILE = 'test_image_classes.pickle'
TRAIN_PAIRS_FILE = 'train_pairs.pickle'
VALIDATION_PAIRS_FILE = 'validation_pairs.pickle'
TEST_PAIRS_FILE = 'test_pairs.pickle'
SVC_MODEL_FILE_NAME = 'svc_model.pickle'

MODEL_INPUT_SHAPE = (96, 144, 1)
GENUINE_LABEL = [1, 0]
FORGERY_LABEL = [0, 1]

# average number of genuines per class = VARIATIONS_PER_IMAGE * 3 + 2
# lower bound of genunines per class = VARIATIONS_PER_IMAGE * 2 + 2
REFERENCES_PER_CLASS = 10
FORGERY_CLASSES_PER_REFERENCE = 10
FORGERIES_PER_FORGERY_CLASS = 10

TRAIN_CLASSES_PERCENTAGE = 0.7
VALIDATION_CLASSES_PERCENTAGE = 0.15
TEST_CLASSES_PERCENTAGE = 0.15

EMBEDDING_VECTOR_SIZE = 1920
LEAKY_RELU_ALPHA = 0.05
LABEL_SMOOTHING_ALPHA = 0.1
LEARNING_RATE = 0.001
CONFIDENCE_PENALTY = 0.1
EPSILON = 1e-7

TRAINING_BATCH_SIZE = 64
EVALUATION_BATCH_SIZE = 128
EPOCHS = 20

REPORT_GENUINE_THRESHOLD = 0.5
REPORT_HISTOGRAM_BIN_SIZE = 5

SPECIAL_FORGERIES_ENABLED = False
VARIATIONS_PER_IMAGE = 4

SPECIAL_FORGERY_VARIATIONS = 1
SPECIAL_FORGERY_CLASSES = FORGERY_CLASSES_PER_REFERENCE // 2
SPECIAL_FORGERY_PER_CLASS = 1


class TrainingMode(Enum):
    TrainWithSplittingClasses = 1
    TrainWithoutSplittingClasses = 2


TRAINING_MODE = TrainingMode.TrainWithSplittingClasses


def cache_object(obj, file):
    with open(file, 'wb') as f:
        pickle.dump(obj, f)


def load_cached(file):
    with open(file, 'rb') as f:
        return pickle.load(f)


def file_exists(file):
    return os.path.isfile(file)


class MinimumRecurrenceRateRandomSamplerWithReplacement:
    def __init__(self, array) -> None:
        self.array = array
        self.left_choices = array.copy()

    def next(self):
        if len(self.left_choices) == 0:
            self.left_choices = self.array.copy()
        i = random.randint(0, len(self.left_choices) - 1)
        return self.left_choices.pop(i)


def generate_pairs(image_classes,
                   special_forgery_image_classes,
                   make_number_of_genuines_and_forgeries_equal=False):
    keys = [*image_classes]
    number_of_classes = len(keys)

    references = []
    others = []
    labels = []

    for c in range(number_of_classes - FORGERY_CLASSES_PER_REFERENCE):
        c = random.randint(0, len(keys) - 1)
        key = keys.pop(c)

        reference_indices = random.sample(list(range(len(image_classes[key]))),
                                          min(REFERENCES_PER_CLASS, len(image_classes[key])))

        for r in reference_indices:
            reference = [key, r]
            if not make_number_of_genuines_and_forgeries_equal:
                genuines = [[key, i] for i in range(len(image_classes[key]))]
                references.extend([reference] * len(genuines))
                others.extend(genuines)
                labels.extend([GENUINE_LABEL] * len(genuines))

            forgery_keys = random.sample(keys, min(FORGERY_CLASSES_PER_REFERENCE, len(keys)))
            for forgery_key in forgery_keys:
                forgery_class = image_classes[forgery_key]
                forgery_indices = random.sample(list(range(len(forgery_class))),
                                                min(FORGERIES_PER_FORGERY_CLASS, len(forgery_class)))
                forgeries = [[forgery_key, f] for f in forgery_indices]

                references.extend([reference] * len(forgeries))
                others.extend(forgeries)
                labels.extend([FORGERY_LABEL] * len(forgeries))

            if make_number_of_genuines_and_forgeries_equal:
                sampler = MinimumRecurrenceRateRandomSamplerWithReplacement(
                    list(range(len(image_classes[key]))))
                references.extend([reference] * len(forgery_keys))
                others.extend([key, sampler.next()] for _ in range(len(forgery_keys)))
                labels.extend([GENUINE_LABEL] * len(forgery_keys))

            if special_forgery_image_classes is not None:
                forgery_key = 's' + key
                forgery_class = special_forgery_image_classes[forgery_key]
                forgeries = [[forgery_key, f] for f in list(range(len(forgery_class)))]

                references.extend([reference] * len(forgeries))
                others.extend(forgeries)
                labels.extend([FORGERY_LABEL] * len(forgeries))

                if make_number_of_genuines_and_forgeries_equal:
                    sampler = MinimumRecurrenceRateRandomSamplerWithReplacement(
                        list(range(len(image_classes[key]))))
                    references.extend([reference] * len(forgeries))
                    others.extend([key, sampler.next()] for _ in range(len(forgeries)))
                    labels.extend([GENUINE_LABEL] * len(forgeries))

    return references, others, labels


def class_train_validation_test_split(image_classes):
    keys = [*image_classes]

    keys_indices = list(range(len(keys)))
    random.shuffle(keys_indices)

    classes = (dict(), dict(), dict())
    percentages = [TRAIN_CLASSES_PERCENTAGE,
                   VALIDATION_CLASSES_PERCENTAGE,
                   TEST_CLASSES_PERCENTAGE]

    for i in range(3):
        chosen_key_indices = keys_indices[:int(len(image_classes) * percentages[i])]
        for j in range(len(chosen_key_indices)):
            key = keys[chosen_key_indices[j]]
            classes[i][key] = image_classes[key]
        keys_indices = keys_indices[len(chosen_key_indices):]

    return classes


def shuffle_pairs(pairs):
    references, others, true_outputs = pairs
    indices = list(range(len(references)))
    random.shuffle(indices)

    shuffled_references = [references[indices[i]] for i in range(len(references))]
    shuffled_others = [others[indices[i]] for i in range(len(references))]
    shuffled_true_outputs = [true_outputs[indices[i]] for i in range(len(references))]

    return shuffled_references, shuffled_others, shuffled_true_outputs


def to_float_array(np_array):
    return np.array(np_array, np.float32)


class DataGenerator(Sequence):
    def __init__(self,
                 image_classes,
                 pairs,
                 special_forgery_image_classes,
                 batch_size,
                 count):
        self.batch_size = batch_size
        self.count = count
        self.image_classes = image_classes
        self.references, self.others, self.true_labels = shuffle_pairs(pairs)
        self.special_forgery_image_classes = special_forgery_image_classes

    def __len__(self):
        return math.ceil(self.count / self.batch_size)

    def __getitem__(self, index):
        references, others, true_labels = [], [], []
        for i in range(index * self.batch_size,
                       min((index + 1) * self.batch_size, len(self.references))):
            reference_pair = self.references[i]
            other_pair = self.others[i]
            reference_image = self.image_classes[reference_pair[0]][reference_pair[1]]
            key = other_pair[0]
            if key in self.image_classes:
                other_image = self.image_classes[key][other_pair[1]]
            else:
                other_image = self.image_classes[key][other_pair[1]]
            references.append(reference_image.astype(np.float32).reshape(MODEL_INPUT_SHAPE) / 255.0)
            others.append(other_image.astype(np.float32).reshape(MODEL_INPUT_SHAPE) / 255.0)
            true_labels.append(to_float_array(self.true_labels[i]))

        if len(references) == 0:
            return self.__getitem__(0)

        return [np.array(references), np.array(others)], np.array(true_labels)

    def on_epoch_end(self):
        self.references, self.others, self.true_labels = shuffle_pairs([self.references,
                                                                        self.others,
                                                                        self.true_labels])


def multi_layer_perceptron(x, hidden_units):
    for units in hidden_units:
        x = tf.keras.layers.Dense(units,
                                  kernel_initializer=tf.keras.initializers.GlorotNormal())(x)
        x = LeakyReLU(LEAKY_RELU_ALPHA)(x)
    return x


def embedding_net(input_shape,
                  patch_size=16,
                  patch_stride=8,
                  patch_projection_dimension=64,
                  transformer_layers=8,
                  transformer_heads=4,
                  transformer_mlp_units=(64, 64)):
    embedding_input = Input(input_shape)

    horizontal_strides = (input_shape[1] - patch_size) // patch_stride
    vertical_strides = (input_shape[0] - patch_size) // patch_stride

    patches = []
    for y in range(vertical_strides):
        y1 = y * patch_stride
        y2 = y1 + patch_size

        for x in range(horizontal_strides):
            x1 = x * patch_stride
            x2 = x1 + patch_size
            patch = embedding_input[:, y1:y2, x1:x2, :]
            patches.append(patch)

    number_of_patches = len(patches)

    patches_layer = Concatenate()(patches)

    flattened_patches_layer = Reshape((number_of_patches, patch_size ** 2))(patches_layer)
    projections = tf.keras.layers.Dense(units=patch_projection_dimension)(flattened_patches_layer)

    positions = tf.range(start=0, limit=number_of_patches, delta=1)
    position_embedding = tf.keras.layers.Embedding(input_dim=number_of_patches,
                                                   output_dim=patch_projection_dimension)(positions)
    positions = tf.reshape(position_embedding, (1, number_of_patches, patch_projection_dimension))

    patch_embedding_layer = Add()([projections, positions])

    encoded_patches = patch_embedding_layer
    for _ in range(transformer_layers):
        x1 = tf.keras.layers.LayerNormalization(epsilon=1e-6)(encoded_patches)
        attention_output = tf.keras.layers.MultiHeadAttention(num_heads=transformer_heads,
                                                              key_dim=patch_projection_dimension)(x1, x1)
        x2 = tf.keras.layers.Add()([attention_output, encoded_patches])
        x3 = tf.keras.layers.LayerNormalization(epsilon=1e-6)(x2)
        x3 = multi_layer_perceptron(x3, hidden_units=transformer_mlp_units)
        encoded_patches = tf.keras.layers.Add()([x3, x2])

    representation = tf.keras.layers.LayerNormalization(epsilon=1e-6)(encoded_patches)
    representation = tf.keras.layers.Flatten()(representation)

    return Model(inputs=embedding_input, outputs=representation)


def absolute_difference(tensors):
    x, y = tensors
    return tf.abs(tf.reduce_sum(tf.stack([x, -y], axis=1), axis=1))


def siamese_net(input_shape=MODEL_INPUT_SHAPE):
    input1 = Input(input_shape)
    input2 = Input(input_shape)

    embedding_model = embedding_net(input_shape)
    embedding_model.summary()

    input_embedding1 = embedding_model(input1)
    input_embedding2 = embedding_model(input2)
    dichotomy = Lambda(absolute_difference)([input_embedding1, input_embedding2])
    # dichotomy = BatchNormalization()(dichotomy)
    # dichotomy = Dropout(0.5)(dichotomy)

    dichotomy_fc1 = Dense(128,
                          use_bias=True,
                          kernel_initializer=GlorotNormal())(dichotomy)
    # dichotomy_fc1 = BatchNormalization()(dichotomy_fc1)
    dichotomy_fc1 = LeakyReLU(0.05)(dichotomy_fc1)
    # dichotomy_fc1 = Dropout(0.5)(dichotomy_fc1)

    dichotomy_rbf = Lambda(lambda x: tf.norm(x, ord='euclidean', axis=-1, keepdims=True))(dichotomy_fc1)
    dichotomy_rbf = Dense(1,
                          use_bias=False,
                          kernel_initializer=tf.constant_initializer(value=0.5),
                          kernel_constraint=tf.keras.constraints.NonNeg())(dichotomy_rbf)
    dichotomy_rbf = Lambda(lambda x: tf.exp(-x))(dichotomy_rbf)

    dichotomy_rbf = Dense(1,
                          activation='sigmoid',
                          kernel_initializer=GlorotNormal())(dichotomy_rbf)
    dichotomy_rbf_inverted = Lambda(lambda x: 1.0 - x)(dichotomy_rbf)
    output = Concatenate()([dichotomy_rbf, dichotomy_rbf_inverted])

    return Model(inputs=[input1, input2], outputs=[output])


def binary_accuracy(y_true, y_pred, thresh=REPORT_GENUINE_THRESHOLD):
    return tf.reduce_mean(tf.cast(tf.equal(y_true > thresh, y_pred > thresh), tf.float32))


def entropy(prob_batch):
    log_prob_batch = tf.math.log(prob_batch + EPSILON)
    entropy_batch = tf.reduce_sum(prob_batch * log_prob_batch, axis=-1)
    return -1.0 * tf.reduce_mean(entropy_batch)


def confidence_regularized_binary_crossentropy(y_true,
                                               y_pred,
                                               label_smoothing_alpha=LABEL_SMOOTHING_ALPHA,
                                               confidence_penalty=CONFIDENCE_PENALTY):
    return binary_crossentropy(y_true,
                               y_pred,
                               label_smoothing=label_smoothing_alpha) - \
        confidence_penalty * entropy(y_pred)


def append_lines_to_report(lines):
    with open(EVALUATION_REPORT_FILE, 'a') as f:
        f.writelines(lines)


def epoch_metrics_log(epoch, logs):
    line = f'epoch: {epoch}'
    line += ' - '
    line += f'loss: {logs["loss"]}'
    line += ' - '
    line += f'binary accuracy: {logs["binary_accuracy"]}'
    line += ' - '
    line += f'validation loss: {logs["val_loss"]}'
    line += ' - '
    line += f'validation binary accuracy: {logs["val_binary_accuracy"]}'
    return line


class ModelLoadingMode(Enum):
    MinimumValue = 0
    MaximumValue = 1


def rename_best_model(mode=ModelLoadingMode.MaximumValue):
    all_models = list(filter(lambda x: x[-3:] == '.h5' and x[:len(MODEL_FILE_NAME)] == MODEL_FILE_NAME,
                             os.listdir(os.curdir)))
    values = []

    for model in all_models:
        value_start_index = model.rfind('_')
        value_end_index = model.rfind('.')

        if value_start_index == -1 or value_end_index == -1:
            values.append(None)
            continue

        value = model[value_start_index + 1: value_end_index]

        try:
            values.append(float(value))
        except ValueError:
            values.append(None)

    model_value_pairs = [[all_models[i], values[i]]
                         for i in range(len(all_models)) if values[i] is not None]

    model_value_pairs.sort(key=lambda x: x[1])
    if mode == ModelLoadingMode.MaximumValue:
        best_index = -1
    else:
        best_index = 0

    shutil.copy(model_value_pairs[best_index][0], MODEL_FILE_NAME + '.h5')


def train(train_image_classes,
          train_pairs,
          validation_image_classes,
          validation_pairs,
          special_forgery_image_classes):
    if file_exists('siamese_vit.h5'):
        siamese_model = load_model('siamese_vit.h5', compile=False)
    else:
        siamese_model = siamese_net()

    siamese_model.summary()

    siamese_model.compile(loss=binary_crossentropy,
                          optimizer=Adam(LEARNING_RATE,
                                         clipnorm=4.0),
                          metrics=[binary_accuracy])

    train_data_generator = DataGenerator(train_image_classes,
                                         train_pairs,
                                         special_forgery_image_classes,
                                         TRAINING_BATCH_SIZE,
                                         len(train_pairs[0]))

    validation_data_generator = DataGenerator(validation_image_classes,
                                              validation_pairs,
                                              special_forgery_image_classes,
                                              EVALUATION_BATCH_SIZE,
                                              len(validation_pairs[0]))

    append_lines_to_report('[training log]\n\n')

    update_report_callback = LambdaCallback(
        on_epoch_end=lambda epoch, logs: append_lines_to_report(
            epoch_metrics_log(epoch, logs) + '\n'
        ))

    model_best_checkpoint_callback = ModelCheckpoint(
        filepath=MODEL_FILE_NAME + '_{epoch:02d}_{val_binary_accuracy:0.4f}.h5',
        monitor='val_binary_accuracy',
        mode='max',
        save_best_only=True)

    model_last_checkpoint_callback = ModelCheckpoint(
        filepath=MODEL_FILE_NAME + '_last.h5',
        save_best_only=False)

    siamese_model.fit(train_data_generator,
                      validation_data=validation_data_generator,
                      epochs=EPOCHS,
                      callbacks=[update_report_callback,
                                 model_best_checkpoint_callback,
                                 model_last_checkpoint_callback])

    rename_best_model()


def evaluate(test_image_classes,
             test_pairs,
             special_forgery_image_classes):
    model = load_model(MODEL_FILE_NAME + '.h5', compile=False)
    model.compile(loss='binary_crossentropy',
                  optimizer=Adam(LEARNING_RATE),
                  metrics=[binary_accuracy])

    test_data_generator = DataGenerator(test_image_classes,
                                        test_pairs,
                                        special_forgery_image_classes,
                                        EVALUATION_BATCH_SIZE,
                                        len(test_pairs[0]))

    append_lines_to_report('\n\n[evaluation log]\n')

    results = model.evaluate_generator(generator=test_data_generator,
                                       steps=math.ceil(len(test_pairs[0]) / EVALUATION_BATCH_SIZE))

    append_lines_to_report(f'test loss: {results[0]} - test accuracy: {results[1]}\n')

    keras.backend.clear_session()
    model = load_model(MODEL_FILE_NAME + '.h5', compile=False)
    model.compile(loss='binary_crossentropy',
                  optimizer=Adam(LEARNING_RATE),
                  metrics=[binary_accuracy])

    all_true_labels, all_predicted_labels, all_predicted_outputs = [], [], []
    batches = len(test_data_generator)
    progress_bar = tqdm(total=batches)
    for i in range(batches):
        input_channels, true_labels = test_data_generator[i]
        predicted_outputs = model(input_channels).numpy()
        all_true_labels.extend(list(1 - np.argmax(true_labels, axis=1)))
        all_predicted_outputs.extend(list(predicted_outputs[:, 0]))
        progress_bar.update()
    progress_bar.close()

    histogram_bin_size = 5

    generate_report(np.array(all_predicted_outputs),
                    np.array(all_true_labels, np.int32),
                    histogram_bin_size,
                    EVALUATION_REPORT_FILE,
                    'siamese_vit_genuine_distribution.png',
                    'siamese_vit_forgery_distribution.png',
                    'siamese_vit_mixed_distributions.png')


def load_or_supply_and_cache(cache_file, supplier_lambda):
    if file_exists(cache_file):
        return load_cached(cache_file)
    result = supplier_lambda()
    cache_object(result, cache_file)
    return result


def train_model(train_image_classes,
                validation_image_classes,
                special_forgery_image_classes):
    train_pairs = load_or_supply_and_cache(TRAIN_PAIRS_FILE,
                                           lambda: generate_pairs(train_image_classes,
                                                                  special_forgery_image_classes))
    validation_pairs = load_or_supply_and_cache(VALIDATION_PAIRS_FILE,
                                                lambda: generate_pairs(validation_image_classes,
                                                                       special_forgery_image_classes))

    train(train_image_classes,
          train_pairs,
          validation_image_classes,
          validation_pairs,
          special_forgery_image_classes)


def evaluate_model(test_image_classes,
                   special_forgery_image_classes):
    test_pairs = load_or_supply_and_cache(TEST_PAIRS_FILE,
                                          lambda: generate_pairs(test_image_classes,
                                                                 special_forgery_image_classes))

    evaluate(test_image_classes,
             test_pairs,
             special_forgery_image_classes)


def run_pipeline():
    image_classes = None
    train_image_classes = None
    validation_image_classes = None
    test_image_classes = None
    special_forgery_image_classes = None

    if not file_exists(TRAIN_IMAGE_CLASSES_FILE) or \
            not file_exists(VALIDATION_IMAGE_CLASSES_FILE) or \
            not file_exists(TEST_IMAGE_CLASSES_FILE):
        if image_classes is None:
            image_classes = load_cached(IMAGE_CLASSES_FILE)
        (train_image_classes,
         validation_image_classes,
         test_image_classes) = class_train_validation_test_split(image_classes)

        if TRAINING_MODE == TrainingMode.TrainWithoutSplittingClasses:
            train_image_classes = image_classes

        cache_object(train_image_classes, TRAIN_IMAGE_CLASSES_FILE)
        cache_object(validation_image_classes, VALIDATION_IMAGE_CLASSES_FILE)
        cache_object(test_image_classes, TEST_IMAGE_CLASSES_FILE)

    # if not file_exists(MODEL_FILE_NAME + '.h5'):
    if train_image_classes is None or \
            validation_image_classes is None:
        train_image_classes = load_cached(TRAIN_IMAGE_CLASSES_FILE)
        validation_image_classes = load_cached(VALIDATION_IMAGE_CLASSES_FILE)
    if SPECIAL_FORGERIES_ENABLED and special_forgery_image_classes is None:
        special_forgery_image_classes = load_cached(SPECIAL_FORGERY_IMAGE_CLASSES_FILE)
    train_model(train_image_classes,
                validation_image_classes,
                special_forgery_image_classes)

    if file_exists(MODEL_FILE_NAME + '.h5'):
        if test_image_classes is None:
            test_image_classes = load_cached(TEST_IMAGE_CLASSES_FILE)
        if SPECIAL_FORGERIES_ENABLED and special_forgery_image_classes is None:
            special_forgery_image_classes = load_cached(SPECIAL_FORGERY_IMAGE_CLASSES_FILE)
        evaluate_model(test_image_classes, special_forgery_image_classes)


def main():
    run_pipeline()


if __name__ == '__main__':
    main()
