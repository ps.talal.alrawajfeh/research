import numpy as np
from matplotlib import pyplot as plt
from sklearn.metrics import accuracy_score, confusion_matrix, classification_report
from terminaltables import AsciiTable


def calculate_false_acceptance_rate_per_threshold(forgeries_count,
                                                  histogram_bins,
                                                  forgery_probabilities):
    false_acceptances = forgery_probabilities
    false_acceptances_histogram = np.histogram(false_acceptances, histogram_bins)[0]
    false_acceptance_per_threshold = np.cumsum(false_acceptances_histogram)
    false_acceptance_per_threshold = forgeries_count - false_acceptance_per_threshold
    false_acceptance_rate_per_threshold = false_acceptance_per_threshold / forgeries_count
    return false_acceptance_rate_per_threshold


def calculate_false_rejection_rate_per_threshold(genuines_count,
                                                 histogram_bins,
                                                 genuine_probabilities):
    false_rejections = genuine_probabilities
    false_rejections_histogram = np.histogram(false_rejections, histogram_bins)[0]
    false_rejection_per_threshold = np.cumsum(false_rejections_histogram)
    false_rejection_rate_per_threshold = false_rejection_per_threshold / genuines_count
    return false_rejection_rate_per_threshold


def append_lines_to_report(file_path, lines):
    with open(file_path, 'a') as f:
        if isinstance(lines, list):
            f.writelines([line + '\n' for line in lines])
        elif isinstance(lines, str):
            f.writelines(lines + '\n')
        else:
            f.writelines(lines)


def write_distributions_table(report_file_path,
                              false_acceptance_rate_per_threshold,
                              false_rejection_rate_per_threshold,
                              histogram_bin_size):
    table = [['Threshold', 'FA Rate', 'FR Rate']]
    for i in range(0, 100 // histogram_bin_size):
        if i == 0:
            far = 100.0
            frr = 0.0
        else:
            far = round(false_acceptance_rate_per_threshold[i - 1] * 100, 2)
            frr = round(false_rejection_rate_per_threshold[i - 1] * 100, 2)
        table.append([f'{i * histogram_bin_size}%', f'{far}%', f'{frr}%'])
    table.append([f'{100}%', f'{0.0}%', f'{100.0}%'])
    table = AsciiTable(table).table
    print(table)
    append_lines_to_report(report_file_path, table)


def write_expected_false_acceptances_table(report_file_path,
                                           false_acceptance_rate_per_threshold,
                                           histogram_bin_size):
    table = [['Threshold',
              'FA Count per 10K verification',
              'FA Count per 100K verification',
              'FA Count per 1M verification']]
    for i in range(0, 100 // histogram_bin_size):
        if i == 0:
            far = 1.0
        else:
            far = false_acceptance_rate_per_threshold[i - 1]
        table.append([f'{i * histogram_bin_size}%',
                      f'{int(far * 10000)}',
                      f'{int(far * 100000)}',
                      f'{int(far * 1000000)}'])
    table.append([f'{100}%', f'{0}', f'{0}', f'{0}'])
    table = AsciiTable(table).table
    print(table)
    append_lines_to_report(report_file_path, table)


def write_expected_false_rejections_table(report_file_path,
                                          false_rejection_rate_per_threshold,
                                          histogram_bin_size):
    table = [['Threshold',
              'FR Count per 10K verification',
              'FR Count per 100K verification',
              'FR Count per 1M verification']]
    for i in range(0, 100 // histogram_bin_size):
        if i == 0:
            frr = 0.0
        else:
            frr = false_rejection_rate_per_threshold[i - 1]
        table.append([f'{i * histogram_bin_size}%',
                      f'{int(frr * 10000)}',
                      f'{int(frr * 100000)}',
                      f'{int(frr * 1000000)}'])
    table.append([f'{100}%', f'{10000}', f'{100000}', f'{1000000}'])
    table = AsciiTable(table).table
    print(table)
    append_lines_to_report(report_file_path, table)


def plot_distributions(genuine_distribution_file_path,
                       forgery_distribution_file_path,
                       mixed_distributions_file_path,
                       genuine_probabilities,
                       forgery_probabilities,
                       histogram_bins):
    plt.hist(genuine_probabilities,
             histogram_bins,
             color='blue',
             label='genuine')
    plt.legend(loc='best')
    plt.savefig(genuine_distribution_file_path, dpi=200)
    plt.close('all')

    plt.hist(forgery_probabilities,
             histogram_bins,
             color='red',
             label='forgery')
    plt.legend(loc='best')
    plt.savefig(forgery_distribution_file_path, dpi=200)
    plt.close('all')

    plt.hist(genuine_probabilities,
             histogram_bins,
             color='blue',
             label='genuine')
    plt.legend(loc='best')
    plt.hist(forgery_probabilities,
             histogram_bins,
             color='red',
             label='forgery')
    plt.legend(loc='best')
    plt.savefig(mixed_distributions_file_path, dpi=200)
    plt.close('all')


def write_classification_report(report_file_path,
                                all_true_labels,
                                all_predicted_labels):
    accuracy = accuracy_score(1 - all_true_labels, 1 - all_predicted_labels)
    append_lines_to_report(report_file_path,
                           f'\ntest accuracy: {round(accuracy * 100, 2)}%\n')

    append_lines_to_report(report_file_path,
                           'confusion matrix:')
    matrix = confusion_matrix(1 - all_true_labels, 1 - all_predicted_labels)
    append_lines_to_report(report_file_path,
                           f'{matrix}')

    frr = round((matrix[0, 1] / np.sum(matrix[0])) * 100, 2)
    far = round((matrix[1, 0] / np.sum(matrix[1])) * 100, 2)
    append_lines_to_report(report_file_path,
                           f'FRR: {frr}%')
    append_lines_to_report(report_file_path,
                           f'FAR: {far}%')

    append_lines_to_report(report_file_path,
                           '\nclassification report:')
    append_lines_to_report(report_file_path,
                           f'{classification_report(1 - all_true_labels, 1 - all_predicted_labels)}')


def generate_report(predictions,
                    labels,
                    histogram_bin_size,
                    report_file_path,
                    genuine_distribution_file_path,
                    forgery_distribution_file_path,
                    mixed_distributions_file_path):
    labels_mask = labels.ravel() > 0.5
    genuine_probabilities = predictions[labels_mask] * 100
    forgery_probabilities = predictions[~labels_mask] * 100
    genuines_count = np.count_nonzero(labels_mask)
    forgeries_count = np.count_nonzero(~labels_mask)

    histogram_bins = list(range(0, 101, histogram_bin_size))

    false_acceptance_rate_per_threshold = calculate_false_acceptance_rate_per_threshold(forgeries_count,
                                                                                        histogram_bins,
                                                                                        forgery_probabilities)

    false_rejection_rate_per_threshold = calculate_false_rejection_rate_per_threshold(genuines_count,
                                                                                      histogram_bins,
                                                                                      genuine_probabilities)

    write_classification_report(report_file_path,
                                np.array(labels_mask, np.int32),
                                np.array(predictions.ravel() > 0.5, np.int32))

    write_distributions_table(report_file_path,
                              false_acceptance_rate_per_threshold,
                              false_rejection_rate_per_threshold,
                              histogram_bin_size)
    append_lines_to_report(report_file_path, '')

    write_expected_false_acceptances_table(report_file_path,
                                           false_acceptance_rate_per_threshold,
                                           histogram_bin_size)
    append_lines_to_report(report_file_path, '')

    write_expected_false_rejections_table(report_file_path,
                                          false_rejection_rate_per_threshold,
                                          histogram_bin_size)

    plot_distributions(genuine_distribution_file_path,
                       forgery_distribution_file_path,
                       mixed_distributions_file_path,
                       genuine_probabilities,
                       forgery_probabilities,
                       histogram_bins)
