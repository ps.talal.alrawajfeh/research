import os
import pickle
import random

import cv2
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from scipy.interpolate import interp1d
from tqdm import tqdm

keras = tf.keras
from keras.models import load_model

TEST_FEATURE_VECTOR_CLASSES_FILE = 'test_feature_vector_classes.pickle'
TRIPLET_MODEL_FILE_NAME = 'triplet_embedding_network.h5'
SIAMESE_MODEL_FILE_NAME = 'siamese.h5'
FEATURES_MODEL_FILE_NAME = 'embedding.h5'
EPSILON = 1e-6


def euclidean_distance(x, y):
    squared_euclidean_distance = np.sum(np.square(np.subtract(x, y)), axis=-1)
    return np.sqrt(np.maximum(squared_euclidean_distance, 0.0))


def predict(features_net,
            triplet_embedding_net,
            siamese_net,
            input_batch1,
            input_batch2):
    distance_to_probability = interp1d([0.0, 0.6, 1.0, 1.3, 2.0],
                                       [1.0, 0.9, 0.5, 0.1, 0.0])

    input_features1 = features_net(input_batch1).numpy()
    input_features2 = features_net(input_batch2).numpy()

    embeddings1 = triplet_embedding_net(input_features1).numpy()
    embeddings2 = triplet_embedding_net(input_features2).numpy()
    siamese_output = siamese_net([input_features1, input_features2]).numpy()[:, 0]

    distances = euclidean_distance(embeddings1, embeddings2)
    triplet_probabilities = distance_to_probability(distances)
    triplet_probabilities[triplet_probabilities >= 1.0 - EPSILON] = 1.0

    combined_values = 0.5 * triplet_probabilities + 0.5 * siamese_output
    combined_values[combined_values < 0.35] = 0.0
    return combined_values


def to_float_array(np_array):
    return np.array(np_array, np.float32)


def deserialize_object(file):
    with open(file, 'rb') as f:
        return pickle.load(f)


def main():
    features_net = load_model(FEATURES_MODEL_FILE_NAME, compile=False)
    triplet_embedding_net = load_model(TRIPLET_MODEL_FILE_NAME, compile=False)
    siamese_net = load_model(SIAMESE_MODEL_FILE_NAME, compile=False)

    image_classes = deserialize_object('image_classes.pickle')

    classes = [*image_classes]
    random.shuffle(classes)

    references_per_class = 2
    negative_classes_per_reference = 10
    negatives_per_class = 1

    progress_bar = tqdm(
        total=len(classes) * references_per_class * negative_classes_per_reference * negatives_per_class)
    for c in classes:
        references = random.sample(image_classes[c], references_per_class)
        for r in references:
            negative_classes = random.sample(classes, negative_classes_per_reference + 1)
            if c in negative_classes:
                negative_classes.pop(negative_classes.index(c))
            if len(negative_classes) > negative_classes_per_reference:
                negative_classes = negative_classes[:negative_classes_per_reference]

            sig1 = r.astype(np.float32).reshape((1, 96, 144, 1)) / 255.0

            for nc in negative_classes:
                negatives = random.sample(image_classes[nc], negatives_per_class)
                for n in negatives:
                    sig2 = n.astype(np.float32).reshape((1, 96, 144, 1)) / 255.0

                    matching_percentage = float(predict(features_net,
                                                        triplet_embedding_net,
                                                        siamese_net,
                                                        sig1,
                                                        sig2)[0])

                    if matching_percentage > 0.8:
                        print(matching_percentage)
                        fig, axs = plt.subplots(2, 1)
                        axs[0].imshow(255 - r, plt.cm.gray)
                        axs[1].imshow(255 - n, plt.cm.gray)
                        plt.show()
                    progress_bar.update()
    progress_bar.close()


def main2():
    features_net = load_model('./result/' + FEATURES_MODEL_FILE_NAME, compile=False)
    image1 = cv2.imread('7_0.png', cv2.IMREAD_GRAYSCALE)
    image2 = cv2.imread('7_1.png', cv2.IMREAD_GRAYSCALE)

    sig1 = image1.astype(np.float32).reshape((1, 96, 144, 1)) / 255.0
    sig2 = image2.astype(np.float32).reshape((1, 96, 144, 1)) / 255.0

    input_layer = features_net.get_layer('input_3').input
    layer_name = 'conv2d_13'
    output_layer = features_net.get_layer(layer_name).output

    model = tf.keras.models.Model(inputs=input_layer, outputs=output_layer)

    filters = model(sig1).numpy()[0]
    for i in range(filters.shape[-1]):
        f = filters[:, :, i]

        max_intensity = np.max(f)
        min_intensity = np.min(f)
        denominator = max_intensity - min_intensity
        if abs(denominator) < EPSILON:
            denominator = 1.0
        f = (f - min_intensity) / denominator * 255.0

        f[f >= 255] = 255
        f[f <= 0] = 0
        f = f.astype(np.uint8)

        base_path = f'/home/u764/Downloads/filters/{layer_name}/1'
        if not os.path.isdir(base_path):
            os.makedirs(base_path)
        cv2.imwrite(f'{base_path}/{i}.png', f)

    filters = model(sig2).numpy()[0]
    for i in range(filters.shape[-1]):
        f = filters[:, :, i]

        max_intensity = np.max(f)
        min_intensity = np.min(f)
        denominator = max_intensity - min_intensity
        if abs(denominator) < EPSILON:
            denominator = 1.0
        f = (f - min_intensity) / denominator * 255.0

        f[f >= 255] = 255
        f[f <= 0] = 0
        f = f.astype(np.uint8)

        base_path = f'/home/u764/Downloads/filters/{layer_name}/2'
        if not os.path.isdir(base_path):
            os.makedirs(base_path)
        cv2.imwrite(f'{base_path}/{i}.png', f)


if __name__ == '__main__':
    main2()
