import time

import numpy as np
import tensorflow as tf

keras = tf.keras
from keras.layers import BatchNormalization, Conv2D, GlobalAveragePooling2D, AveragePooling2D, ReLU, Input, Activation, \
    Dense
from keras import Model
from keras.datasets import mnist
from keras.optimizers import Adam
from keras.losses import categorical_crossentropy

INPUT_SIZE = (28, 28)


def custom_loss(y_true, y_pred):
    return tf.reduce_mean(-tf.reduce_sum(y_true * tf.math.log(y_pred), axis=-1))


def main():
    (x_train, y_train), (x_test, y_test) = mnist.load_data()

    x_train = x_train.reshape(-1, 28, 28, 1).astype(np.float32) / 255.0
    y_one_hot_train = np.zeros((y_train.shape[0], 10), np.float32)
    for i in range(y_train.shape[0]):
        y_one_hot_train[i, y_train[i]] = 1.0

    x_test = x_test.reshape(-1, 28, 28, 1).astype(np.float32) / 255.0
    y_one_hot_test = np.zeros((y_test.shape[0], 10), np.float32)
    for i in range(y_test.shape[0]):
        y_one_hot_test[i, y_test[i]] = 1.0

    input_layer = Input((28, 28, 1))
    x = BatchNormalization()(input_layer)

    x = Conv2D(32,
               (3, 3),
               padding='same',
               use_bias=False)(x)
    x = BatchNormalization()(x)
    x = ReLU()(x)
    x = AveragePooling2D()(x)

    x = Conv2D(64,
               (3, 3),
               padding='same',
               use_bias=False)(x)
    x = BatchNormalization()(x)
    x = ReLU()(x)
    x = AveragePooling2D()(x)

    x = Conv2D(128,
               (3, 3),
               padding='same',
               use_bias=False)(x)
    x = BatchNormalization()(x)
    x = ReLU()(x)
    x = GlobalAveragePooling2D()(x)

    x = Dense(128)(x)
    x = ReLU()(x)

    x = Dense(10)(x)
    x = Activation(tf.nn.softmax)(x)

    model = Model(inputs=input_layer, outputs=x)

    model.compile(optimizer=Adam(),
                  loss=custom_loss)
    start_time = time.time()
    model.fit(x_train,
              y_one_hot_train,
              batch_size=100,
              epochs=1)

    end_time = time.time()
    print(end_time - start_time)


if __name__ == '__main__':
    main()
