import os
import pickle
import random
import sys
from enum import Enum

import numpy as np
import tensorflow as tf
from scipy.interpolate import interp1d

keras = tf.keras
from keras import Model
from keras.callbacks import LambdaCallback, ModelCheckpoint
from keras.layers import Input, Dense, Lambda
from keras.models import load_model
from keras.initializers import GlorotNormal
from keras.optimizers import Adam
from keras.utils import Sequence
from tqdm import tqdm

from reporting import generate_report

FEATURE_VECTOR_CLASSES_FILE = 'feature_vector_classes.pickle'
TRAIN_FEATURE_VECTOR_CLASSES_FILE = 'train_feature_vector_classes.pickle'
VALIDATION_FEATURE_VECTOR_CLASSES_FILE = 'validation_feature_vector_classes.pickle'
TEST_FEATURE_VECTOR_CLASSES_FILE = 'test_feature_vector_classes.pickle'
TEST_EMBEDDING_CLASSES_FILE = 'triplet_test_embeddings.pickle'
TEST_PAIRS_FILE = 'triplet_test_pairs.pickle'

EVALUATION_REPORT_FILE = 'triplet_report.txt'
EMBEDDING_NETWORK_FILE_NAME = 'triplet_embedding_network.h5'

MARGIN = 0.5
EMBEDDING_LAYER_SIZE = 128
EMBEDDING_VECTOR_SIZE = 448
LEAKY_RELU_ALPHA = 0.05
EPSILON = 1e-6

TRAIN_CLASSES_PER_BATCH = 100
TRAIN_VECTORS_PER_CLASS = 10
TRAIN_BATCHES = 100000

VALIDATION_CLASSES_PER_BATCH = 100
VALIDATION_VECTORS_PER_CLASS = 10
VALIDATION_BATCHES = 21500

EPOCHS = 100

tf.random.set_seed(123456)
random.seed(123456)
np.random.seed(123456)


class TrainingMode(Enum):
    TrainWithSplittingClasses = 1
    TrainWithoutSplittingClasses = 2


TRAINING_MODE = TrainingMode.TrainWithSplittingClasses


def triplet_loss(y_true, y_predict, margin=MARGIN):
    labels = tf.reshape(y_true, [-1])

    class_labels_table_rows = tf.expand_dims(labels, axis=1)
    class_labels_table_columns = tf.expand_dims(labels, axis=0)

    same_class_mask = tf.equal(class_labels_table_rows, class_labels_table_columns)

    vectors_table_rows = tf.expand_dims(y_predict, axis=1)
    vectors_table_columns = tf.expand_dims(y_predict, axis=0)

    square_pairwise_distances = tf.square(tf.subtract(vectors_table_rows, vectors_table_columns))
    distances = tf.sqrt(tf.maximum(tf.reduce_sum(square_pairwise_distances, axis=-1), 1e-12))

    same_class_multiplier = tf.cast(same_class_mask, tf.float32)
    anchor_positive_distances = distances * same_class_multiplier
    hardest_positive = tf.reduce_max(anchor_positive_distances, axis=-1)

    different_class_multiplier = tf.cast(~same_class_mask, np.float32)
    anchor_negative_distances = distances * different_class_multiplier + same_class_multiplier * 1000.0
    hardest_negative = tf.reduce_min(anchor_negative_distances, axis=-1)

    return tf.reduce_mean(tf.maximum(hardest_positive + margin - hardest_negative, 0.0))


def accuracy(y_true, y_predict):
    class_labels_table_rows = y_true
    class_labels_table_columns = tf.reshape(y_true, (1, -1))

    same_class_mask = tf.equal(class_labels_table_rows, class_labels_table_columns)

    vectors_table_rows = tf.expand_dims(y_predict, axis=1)
    vectors_table_columns = tf.expand_dims(y_predict, axis=0)

    square_pairwise_distances = tf.square(tf.subtract(vectors_table_rows, vectors_table_columns))
    distances = tf.sqrt(tf.maximum(tf.reduce_sum(square_pairwise_distances, axis=-1), 1e-12))

    same_vector_mask = tf.eye(tf.shape(distances)[0], dtype=bool)
    same_class_multiplier = tf.cast(same_class_mask, tf.float32)
    anchor_positive_distances = distances * same_class_multiplier

    different_class_multiplier = tf.cast(~same_class_mask, np.float32)
    anchor_negative_distances = distances * different_class_multiplier + same_class_multiplier * 1000.0

    anchor_positive_distances = tf.expand_dims(anchor_positive_distances, axis=-1)
    anchor_negative_distances = tf.expand_dims(anchor_negative_distances, axis=1)

    accuracy_mask = tf.expand_dims(same_class_mask & ~same_vector_mask, axis=-1)
    correct_triplets = tf.less(anchor_positive_distances, anchor_negative_distances)
    accuracy_mask = tf.broadcast_to(accuracy_mask, tf.shape(correct_triplets)) & ~same_class_mask

    return tf.reduce_mean(tf.cast(correct_triplets[accuracy_mask], tf.float32))


def embedding_network(input_shape=(EMBEDDING_VECTOR_SIZE,)):
    input_layer = Input(input_shape)

    output = Dense(EMBEDDING_LAYER_SIZE,
                   activation='tanh',
                   kernel_initializer=GlorotNormal())(input_layer)

    output = Lambda(lambda x: tf.math.l2_normalize(x, axis=-1))(output)

    return Model(inputs=[input_layer], outputs=[output])


class DataGenerator(Sequence):
    def __init__(self,
                 feature_vector_classes,
                 classes_per_batch,
                 vectors_per_class,
                 batches):
        self.feature_vector_classes = feature_vector_classes
        self.classes_per_batch = classes_per_batch
        self.vectors_per_class = vectors_per_class
        self.batches = batches
        self.classes = [*feature_vector_classes]

    def __len__(self):
        return self.batches

    def __getitem__(self, _):
        batch_classes = random.sample(self.classes, self.classes_per_batch)
        vectors = []
        labels = []

        i = 1
        for c in batch_classes:
            number_of_feature_vectors_in_class = len(self.feature_vector_classes[c])
            n = self.vectors_per_class // number_of_feature_vectors_in_class
            remainder = self.vectors_per_class % number_of_feature_vectors_in_class
            vectors.extend(self.feature_vector_classes[c] * n)
            vectors.extend(random.sample(self.feature_vector_classes[c], remainder))
            labels.extend([i] * self.vectors_per_class)
            i += 1

        return np.array(vectors), np.expand_dims(np.array(labels), axis=-1)


def append_lines_to_report(lines):
    with open(EVALUATION_REPORT_FILE, 'a') as f:
        f.writelines(lines)


def epoch_metrics_log(epoch, logs):
    line = f'epoch: {epoch}'
    line += ' - '
    line += f'loss: {logs["loss"]}'
    line += ' - '
    line += f'accuracy: {logs["accuracy"]}'
    line += ' - '
    line += f'validation loss: {logs["val_loss"]}'
    line += ' - '
    line += f'validation accuracy: {logs["val_accuracy"]}'
    return line


def train(train_feature_vector_classes,
          validation_feature_vector_classes):
    if os.path.isfile(EMBEDDING_NETWORK_FILE_NAME):
        embedding_net = load_model(EMBEDDING_NETWORK_FILE_NAME, compile=False)
    else:
        embedding_net = embedding_network()
    embedding_net.summary()

    embedding_net.compile(loss=triplet_loss,
                          optimizer=Adam(learning_rate=0.001, clipnorm=6),
                          metrics=[accuracy])

    train_data_generator = DataGenerator(train_feature_vector_classes,
                                         TRAIN_CLASSES_PER_BATCH,
                                         TRAIN_VECTORS_PER_CLASS,
                                         TRAIN_BATCHES)

    validation_data_generator = DataGenerator(validation_feature_vector_classes,
                                              VALIDATION_CLASSES_PER_BATCH,
                                              VALIDATION_VECTORS_PER_CLASS,
                                              VALIDATION_BATCHES)

    append_lines_to_report('[training log]\n\n')

    update_report_callback = LambdaCallback(
        on_epoch_end=lambda epoch, logs: append_lines_to_report(
            epoch_metrics_log(epoch, logs) + '\n'
        ))

    last_checkpoint_callback = ModelCheckpoint(
        filepath=EMBEDDING_NETWORK_FILE_NAME + '_checkpoint_last.h5',
        save_best_only=False)

    best_checkpoint_callback = ModelCheckpoint(
        filepath=EMBEDDING_NETWORK_FILE_NAME + '_{epoch:02d}_{val_accuracy:0.4f}.h5',
        monitor='val_accuracy',
        mode='max',
        save_best_only=True)

    embedding_net.fit_generator(generator=train_data_generator,
                                steps_per_epoch=TRAIN_BATCHES,
                                validation_data=validation_data_generator,
                                validation_steps=VALIDATION_BATCHES,
                                epochs=EPOCHS,
                                callbacks=[update_report_callback,
                                           last_checkpoint_callback,
                                           best_checkpoint_callback])


def evaluate(test_feature_vector_classes):
    embedding_net = load_model(EMBEDDING_NETWORK_FILE_NAME, compile=False)

    embedding_net.summary()

    embedding_net.compile(loss=triplet_loss,
                          optimizer=Adam(learning_rate=0.001, clipnorm=6),
                          metrics=[accuracy])

    test_data_generator = DataGenerator(test_feature_vector_classes,
                                        VALIDATION_CLASSES_PER_BATCH,
                                        VALIDATION_VECTORS_PER_CLASS,
                                        VALIDATION_BATCHES)

    append_lines_to_report('\n[evaluation log]\n')

    results = embedding_net.evaluate_generator(generator=test_data_generator,
                                               steps=VALIDATION_BATCHES)

    append_lines_to_report(f'test loss: {results[0]} - test accuracy: {results[1]}\n')


def file_exists(file):
    return os.path.isfile(file)


def serialize_object(obj, file):
    with open(file, 'wb') as f:
        pickle.dump(obj, f)


def deserialize_object(file):
    with open(file, 'rb') as f:
        return pickle.load(f)


def predict_batch_feature_vectors(batch,
                                  model):
    return model.predict(np.array(batch, np.float32))


class BatchVectorizer:
    def __init__(self, model, batch_size=512):
        self.__items = []
        self.__classes = []
        self.__feature_vector_classes = dict()
        self.__batch_size = batch_size
        self.__model = model

    def __predict(self):
        if len(self.__items) == 0:
            return
        feature_vectors = predict_batch_feature_vectors(self.__items, self.__model)
        for i in range(len(self.__classes)):
            c = self.__classes[i]
            if c not in self.__feature_vector_classes:
                self.__feature_vector_classes[c] = []
            self.__feature_vector_classes[c].append(feature_vectors[i])
        self.__items = []
        self.__classes = []

    def __predict_if_batch_size_reached(self):
        if len(self.__items) >= self.__batch_size:
            self.__predict()

    def feed_inputs(self, items, classes):
        for i in range(len(items)):
            self.__items.append(items[i])
            self.__classes.append(classes[i])
            self.__predict_if_batch_size_reached()

    def feed_input(self, item, item_class):
        self.__items.append(item)
        self.__classes.append(item_class)
        self.__predict_if_batch_size_reached()

    def finalize(self):
        self.__predict()

    def get_feature_vectors(self):
        return self.__feature_vector_classes


def generate_feature_vectors(model,
                             feature_vector_classes,
                             output_file_name):
    progress_bar = tqdm(total=len(feature_vector_classes))
    image_batch_vectorizer = BatchVectorizer(model)
    for c in feature_vector_classes:
        batch = feature_vector_classes[c]
        image_batch_vectorizer.feed_inputs(batch, [c] * len(batch))
        progress_bar.update()
    image_batch_vectorizer.finalize()
    progress_bar.close()
    serialize_object(image_batch_vectorizer.get_feature_vectors(), output_file_name)


def generate_or_load_test_embedding_classes():
    if not file_exists(TEST_EMBEDDING_CLASSES_FILE):
        test_feature_vector_classes = deserialize_object(TEST_FEATURE_VECTOR_CLASSES_FILE)
        batch_vectorizer = BatchVectorizer(tf.keras.models.load_model(EMBEDDING_NETWORK_FILE_NAME, compile=False))

        classes = [*test_feature_vector_classes]
        classes_count = len(test_feature_vector_classes)
        progress_bar = tqdm(total=classes_count)
        for c in classes:
            feature_vectors = test_feature_vector_classes[c]
            batch_vectorizer.feed_inputs(feature_vectors, [c] * len(feature_vectors))
            progress_bar.update()
        batch_vectorizer.finalize()
        progress_bar.close()

        test_embedding_classes = batch_vectorizer.get_feature_vectors()
        serialize_object(test_embedding_classes, TEST_EMBEDDING_CLASSES_FILE)
        return test_embedding_classes

    return deserialize_object(TEST_EMBEDDING_CLASSES_FILE)


def generate_pairs(feature_vector_classes,
                   references_per_class=10,
                   forgery_classes_per_reference=10,
                   forgeries_per_forgery_class=10):
    keys = [*feature_vector_classes]
    number_of_classes = len(keys)

    references = []
    others = []
    labels = []

    for c in range(number_of_classes - forgery_classes_per_reference):
        c = random.randint(0, len(keys) - 1)
        key = keys.pop(c)

        reference_indices = random.sample(list(range(len(feature_vector_classes[key]))),
                                          min(references_per_class, len(feature_vector_classes[key])))

        for r in reference_indices:
            reference = [key, r]
            genuines = [[key, i] for i in range(len(feature_vector_classes[key]))]
            references.extend([reference] * len(genuines))
            others.extend(genuines)
            labels.extend([1] * len(genuines))

            forgery_keys = random.sample(keys, min(forgery_classes_per_reference, len(keys)))
            for forgery_key in forgery_keys:
                forgery_class = feature_vector_classes[forgery_key]
                forgery_indices = random.sample(list(range(len(forgery_class))),
                                                min(forgeries_per_forgery_class, len(forgery_class)))
                forgeries = [[forgery_key, f] for f in forgery_indices]

                references.extend([reference] * len(forgeries))
                others.extend(forgeries)
                labels.extend([0] * len(forgeries))

    return references, others, labels


def distance(x, y):
    squared_euclidean_distance = np.sum(np.square(np.subtract(x, y)))
    return np.sqrt(np.maximum(squared_euclidean_distance, 0.0))


def distance_batch(x, y):
    squared_euclidean_distance = np.sum(np.square(np.subtract(x, y)), axis=-1)
    return np.sqrt(np.maximum(squared_euclidean_distance, 0.0))


def run_pipeline():
    train_feature_vector_classes = None
    validation_feature_vector_classes = None
    test_feature_vector_classes = None

    if not file_exists(EMBEDDING_NETWORK_FILE_NAME):
        if train_feature_vector_classes is None:
            train_feature_vector_classes = deserialize_object(TRAIN_FEATURE_VECTOR_CLASSES_FILE)
        if validation_feature_vector_classes is None:
            validation_feature_vector_classes = deserialize_object(VALIDATION_FEATURE_VECTOR_CLASSES_FILE)

        train(train_feature_vector_classes,
              validation_feature_vector_classes)

    if file_exists(EMBEDDING_NETWORK_FILE_NAME):
        if test_feature_vector_classes is None:
            test_feature_vector_classes = deserialize_object(TEST_FEATURE_VECTOR_CLASSES_FILE)

        evaluate(test_feature_vector_classes)

        test_embedding_classes = generate_or_load_test_embedding_classes()
        if not file_exists(TEST_PAIRS_FILE):
            test_pairs = generate_pairs(test_embedding_classes)
            serialize_object(test_pairs, TEST_PAIRS_FILE)
        else:
            test_pairs = deserialize_object(TEST_PAIRS_FILE)

        references, others, labels = test_pairs

        distances = []
        genuine_distances = []
        forgery_distances = []
        for i in range(len(labels)):
            reference = references[i]
            other = others[i]
            label = labels[i]

            rc, ri = reference
            oc, oi = other

            reference_vector = test_embedding_classes[rc][ri]
            other_vector = test_embedding_classes[oc][oi]

            euclidean_distance = distance(reference_vector, other_vector)
            distances.append(euclidean_distance)
            if label == 1:
                genuine_distances.append(euclidean_distance)
            else:
                forgery_distances.append(euclidean_distance)

        max_distance = 2.0
        bins_count = 100
        bin_size = max_distance / bins_count
        bins = [i * bin_size for i in range(101)]

        forgery_distances_histogram = np.histogram(forgery_distances, bins)[0]
        genuine_distances_histogram = np.histogram(genuine_distances, bins)[0]

        min_difference = sys.float_info.max
        min_difference_i = -1
        for i in range(len(bins) // 4, len(bins) * 3 // 4):
            difference = abs(forgery_distances_histogram[i] - genuine_distances_histogram[i])
            if difference < min_difference:
                min_difference = difference
                min_difference_i = i

        equal_on_distance = min_difference_i * bin_size
        # distance_to_prob = interp1d([0.0, equal_on_distance, max_distance], [1.0, 0.5, 0.0])
        # probabilities = distance_to_prob(distances)

        distance_to_probability = interp1d([0.0, 0.6, 1.0, 1.3, 2.0],
                                           [1.0, 0.9, 0.5, 0.1, 0.0])
        probabilities = distance_to_probability(distances)
        probabilities[probabilities >= 1.0 - EPSILON] = 1.0

        labels = np.array(labels)
        histogram_bin_size = 5

        generate_report(probabilities,
                        labels,
                        histogram_bin_size,
                        EVALUATION_REPORT_FILE,
                        'triplet_genuine_distribution.png',
                        'triplet_forgery_distribution.png',
                        'triplet_mixed_distributions.png')


def main():
    run_pipeline()


if __name__ == '__main__':
    main()
