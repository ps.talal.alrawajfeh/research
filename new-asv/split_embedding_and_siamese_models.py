import cv2
import numpy as np
import tensorflow as tf

from data_generator import pre_process_image


def absolute_difference(tensors):
    x, y = tensors
    return tf.abs(tf.reduce_sum(tf.stack([x, -y], axis=1), axis=1))


def main1():
    model = tf.keras.models.load_model('./result/siamese_04_0.9957.h5', compile=False)

    embedding_model = model.get_layer('model')
    input_layer = embedding_model.get_layer('input_3').input
    output_layer = embedding_model.get_layer('concatenate').output
    separate_embedding_model = tf.keras.models.Model(inputs=input_layer, outputs=output_layer)
    separate_embedding_model.save('./result/embedding.h5')

    input1 = tf.keras.layers.Input(shape=(448,))
    input2 = tf.keras.layers.Input(shape=(448,))

    dichotomy = tf.keras.layers.Lambda(absolute_difference)([input1, input2])

    dichotomy_fc1 = tf.keras.layers.Dense(128,
                                          use_bias=True)(dichotomy)
    dichotomy_fc1 = tf.keras.layers.LeakyReLU(0.05)(dichotomy_fc1)

    dichotomy_rbf = tf.keras.layers.Lambda(lambda x: tf.norm(x, ord='euclidean', axis=-1, keepdims=True))(dichotomy_fc1)
    dichotomy_rbf = tf.keras.layers.Dense(1,
                                          use_bias=False,
                                          kernel_constraint=tf.keras.constraints.NonNeg())(dichotomy_rbf)
    dichotomy_rbf = tf.keras.layers.Lambda(lambda x: tf.exp(-x))(dichotomy_rbf)

    dichotomy_rbf = tf.keras.layers.Dense(1,
                                          activation='sigmoid')(dichotomy_rbf)
    dichotomy_rbf_inverted = tf.keras.layers.Lambda(lambda x: 1.0 - x)(dichotomy_rbf)
    output = tf.keras.layers.Concatenate()([dichotomy_rbf, dichotomy_rbf_inverted])

    separate_siamese_model = tf.keras.models.Model(inputs=[input1, input2], outputs=[output])

    for i, layer in enumerate(model.layers[3:]):
        separate_siamese_model.layers[2 + i].set_weights(layer.get_weights())

    separate_siamese_model.save('./result/siamese.h5')


def main2():
    model = tf.keras.models.load_model('./result/siamese_04_0.9957.h5', compile=False)
    separate_embedding_model = tf.keras.models.load_model('./result/embedding.h5', compile=False)
    separate_siamese_model = tf.keras.models.load_model('./result/siamese.h5', compile=False)

    sig1 = cv2.imread('/home/u764/Development/data/signatures/______001.jpg', cv2.IMREAD_GRAYSCALE)
    # sig2 = cv2.imread('/home/u764/Development/data/signatures/______001_03.jpg', cv2.IMREAD_GRAYSCALE)
    sig2 = cv2.imread('/home/u764/Development/data/signatures/0a4c48343_1.bmp', cv2.IMREAD_GRAYSCALE)

    sig1 = pre_process_image(sig1).astype(np.float32).reshape((1, 96, 144, 1)) / 255.0
    sig2 = pre_process_image(sig2).astype(np.float32).reshape((1, 96, 144, 1)) / 255.0

    print(model([sig1, sig2]))

    embedding1 = separate_embedding_model(sig1)
    embedding2 = separate_embedding_model(sig2)

    print(separate_siamese_model([embedding1, embedding2]))


if __name__ == '__main__':
    main2()
