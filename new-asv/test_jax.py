import time

import jax
import numpy as np
import tensorflow as tf
from flax import linen as nn
from jax import random, numpy as jnp

keras = tf.keras
from keras.datasets import mnist
import optax

INPUT_SIZE = (28, 28)


class SimpleConv(nn.Module):
    def setup(self):
        self.input_batch_norm = nn.BatchNorm(use_running_average=False, momentum=0.9, epsilon=1e-5)

        self.conv1 = nn.Conv(features=32,
                             kernel_size=(3, 3),
                             strides=(1, 1),
                             padding='SAME',
                             use_bias=False)

        self.batch_norm1 = nn.BatchNorm(use_running_average=False, momentum=0.9, epsilon=1e-5)

        self.conv2 = nn.Conv(features=64,
                             kernel_size=(3, 3),
                             strides=(1, 1),
                             padding='SAME',
                             use_bias=False)

        self.batch_norm2 = nn.BatchNorm(use_running_average=False, momentum=0.9, epsilon=1e-5)

        self.conv3 = nn.Conv(features=128,
                             kernel_size=(3, 3),
                             strides=(1, 1),
                             padding='SAME',
                             use_bias=False)

        self.batch_norm3 = nn.BatchNorm(use_running_average=False, momentum=0.9, epsilon=1e-5)

    def __call__(self, inputs):
        x = inputs
        x = self.input_batch_norm(x)

        x = self.conv1(x)
        x = self.batch_norm1(x)
        x = nn.relu(x)
        x = nn.avg_pool(x, window_shape=(2, 2))

        x = self.conv2(x)
        x = self.batch_norm2(x)
        x = nn.relu(x)
        x = nn.avg_pool(x, window_shape=(2, 2))

        x = self.conv3(x)
        x = self.batch_norm3(x)
        x = nn.relu(x)
        x = jnp.sum(x, axis=1)
        x = jnp.sum(x, axis=1)
        x /= (INPUT_SIZE[0] * INPUT_SIZE[1])

        return x


class SimpleClassifier(nn.Module):
    def setup(self):
        self.fc1 = nn.Dense(features=128)
        self.fc2 = nn.Dense(features=10)

    def __call__(self, inputs):
        x = inputs

        x = self.fc1(x)
        x = nn.relu(x)

        x = self.fc2(x)
        x = nn.softmax(x)

        return x


class MNISTClassifier(nn.Module):
    def setup(self):
        self.simple_conv = SimpleConv()
        self.simple_classifier = SimpleClassifier()

    def __call__(self, inputs):
        x = inputs

        x = self.simple_conv(x)
        x = self.simple_classifier(x)

        return x


@jax.jit
def categorical_cross_entropy(y_true_batched, y_pred_batched):
    def categorical_cross_entropy_single(y_true, y_pred):
        return -jnp.inner(y_true, jnp.log(y_pred)) / 2.0

    return jnp.mean(jax.vmap(categorical_cross_entropy_single)(y_true_batched, y_pred_batched), axis=0)


def main():
    (x_train, y_train), (x_test, y_test) = mnist.load_data()

    x_train = x_train.reshape(-1, 28, 28, 1).astype(np.float32) / 255.0
    y_one_hot_train = np.zeros((y_train.shape[0], 10), np.float32)
    for i in range(y_train.shape[0]):
        y_one_hot_train[i, y_train[i]] = 1.0

    x_test = x_test.reshape(-1, 28, 28, 1).astype(np.float32) / 255.0
    y_one_hot_test = np.zeros((y_test.shape[0], 10), np.float32)
    for i in range(y_test.shape[0]):
        y_one_hot_test[i, y_test[i]] = 1.0

    model = MNISTClassifier()

    key1, key2 = random.split(random.key(0), 2)
    x = random.uniform(key1, (100, 28, 28, 1))
    params = model.init(key2, x)

    @jax.jit
    def loss(model_params, inputs, labels):
        pred = model.apply(model_params, inputs, mutable=['batch_stats'])
        return categorical_cross_entropy(labels, pred[0])

    tx = optax.adam(learning_rate=0.001)
    opt_state = tx.init(params)
    loss_grad_fn = jax.value_and_grad(loss)

    @jax.jit
    def train(model_params, optimizer_state, inputs, y_true):
        for i in range(x_train.shape[0] // 100):
            loss_val, grads = loss_grad_fn(model_params, inputs[i * 100:(i + 1) * 100],
                                           y_true[i * 100:(i + 1) * 100])
            updates, optimizer_state = tx.update(grads, optimizer_state)
            model_params = optax.apply_updates(model_params, updates)

    start_time = time.time()
    train(params, opt_state, x_train, y_one_hot_train)
    end_time = time.time()
    print(end_time - start_time)


if __name__ == '__main__':
    main()
