import mimetypes
import os
import pickle
import random

import cv2
import numpy as np
from tqdm import tqdm

# DATA_BASE_PATH = '/home/user/Development/data/'
DATA_BASE_PATH = '/home/user/Development/data/'
# SIGNATURE_CLASSES_PATHS = [DATA_BASE_PATH + 'CABHBTF',
#                            DATA_BASE_PATH + 'trainingSignatures']
SIGNATURE_CLASSES_PATHS = [DATA_BASE_PATH + 'signatures']

CLASS_NUMBER_LENGTH = 9
NUMBER_OF_IMAGES_PER_CLASS_THRESHOLD = 2
NUMBER_OF_IMAGES_TO_AUGMENTATIONS_PER_IMAGE = 4
NUMBER_OF_SPECIAL_FORGERIES_AUGMENTED_IMAGES = 1

MODEL_INPUT_IMAGE_SIZE = (144, 96)
MODEL_INPUT_IMAGE_SHAPE = (96, 144, 1)

# Augmentation Parameters
SPECKLES_FRACTION = 1e-3
MIN_ROTATION_ANGLE = 1
MAX_ROTATION_ANGLE = 5
MIN_QUALITY = 75
MAX_QUALITY = 95
QUALITY_RANDOMIZATION_STEP_SIZE = 5
MORPHOLOGY_VARIATION_MIN_SIZE = 250 * 250
MIN_RESIZE_PERCENTAGE = 90
VARIATIONS_PER_IMAGE = 4
DPI_LIST = [50, 60, 70, 80, 90]
MIN_PADDING_WIDTH = 1
MAX_PADDING_WIDTH = 100
STARTING_DPI_CHANGE_MIN_HEIGHT = 200
STARTING_DPI_CHANGE_MIN_WIDTH = 200
NUMBER_OF_SPECIAL_VARIATIONS = 4
AUGMENT_DATA = True
GENERATE_SPECIAL_FORGERIES = False
SAVE_IMAGES = False

OUTPUT_DIRECTORY = './data'

random.seed(123456)
np.random.seed(123456)


# -----------------------------------------------------------------------------

# Returns all the extensions for a given mime type.
def get_extensions_for_type(general_type):
    for ext in mimetypes.types_map:
        if mimetypes.types_map[ext].split('/')[0] == general_type:
            yield ext.lower()


# Function decorator for caching outputs.
def memoize(function):
    memo = {}

    def wrapper(*args):
        if args in memo:
            return memo[args]
        else:
            rv = function(*args)
            memo[args] = rv
            return rv

    return wrapper


# Returns all image extensions, e.g. '.jpg'.
@memoize
def get_image_extensions():
    return tuple(get_extensions_for_type('image'))


def file_extension(path):
    return os.path.splitext(os.path.basename(path))[1].lower()


def is_image(path):
    return file_extension(path) in get_image_extensions()


def to_int(number):
    return int(round(number))


# -----------------------------------------------------------------------------


def read_image_grayscale(image_path):
    return cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)


def resize_image_high_quality(image, new_size):
    if new_size[0] == image.shape[1] and new_size[1] == image.shape[0]:
        return np.array(image)
    return cv2.resize(image,
                      new_size,
                      interpolation=cv2.INTER_CUBIC)


def threshold(image, value=128):
    image_copy = np.array(image)
    image_copy[image_copy >= value] = 255
    image_copy[image_copy < value] = 0
    return image_copy


def threshold_otsu(image):
    return cv2.threshold(image,
                         0,
                         255,
                         cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]


def remove_white_border(binary_image):
    mask = (255 - binary_image) > 0

    height, width = binary_image.shape[0:2]
    mask1, mask2 = mask.any(0), mask.any(1)
    x1, x2 = mask1.argmax(), width - mask1[::-1].argmax()
    y1, y2 = mask2.argmax(), height - mask2[::-1].argmax()

    return binary_image[y1:y2, x1:x2]


def remove_white_border_non_binary(image):
    binary_image = threshold_otsu(image)

    mask = (255 - binary_image) > 0

    height, width = binary_image.shape[0:2]
    mask1, mask2 = mask.any(0), mask.any(1)
    x1, x2 = mask1.argmax(), width - mask1[::-1].argmax()
    y1, y2 = mask2.argmax(), height - mask2[::-1].argmax()

    return image[y1:y2, x1:x2]


def rotate(image, angle):
    (h, w) = image.shape
    center_x, center_y = w / 2, h / 2

    rotation_matrix = cv2.getRotationMatrix2D((center_x, center_y),
                                              angle,
                                              1.0)
    cos = np.abs(rotation_matrix[0, 0])
    sin = np.abs(rotation_matrix[0, 1])

    new_w = h * sin + w * cos
    new_h = h * cos + w * sin

    rotation_matrix[0, 2] += new_w / 2 - center_x
    rotation_matrix[1, 2] += new_h / 2 - center_y

    return cv2.warpAffine(image,
                          rotation_matrix,
                          (to_int(new_w), to_int(new_h)),
                          borderValue=(255),
                          flags=cv2.INTER_CUBIC)


def change_image_quality(image, quality):
    encoded = cv2.imencode('.jpg', image, [int(cv2.IMWRITE_JPEG_QUALITY), quality])[1]
    return cv2.imdecode(encoded, cv2.IMREAD_GRAYSCALE)


def random_coordinate(image):
    return random.randint(0, image.shape[0] - 1), random.randint(0, image.shape[1] - 1)


def speckle(image, fraction, color=0):
    coordinates = [random_coordinate(image)
                   for _ in range(to_int(image.size * fraction))]

    image_copy = np.array(image)
    for coord in coordinates:
        image_copy[coord[0], coord[1]] = color

    return image_copy


def random_bool(probability=0.5):
    return random.random() <= probability


def change_image_dpi(image, dpi):
    factor = dpi / 100
    return cv2.resize(image, (0, 0), fx=factor, fy=factor, interpolation=cv2.INTER_CUBIC)


def pad_image(image, top=0, bottom=0, left=0, right=0, color=255):
    return np.pad(image,
                  [[top, bottom], [left, right]],
                  mode='constant',
                  constant_values=((color, color), (color, color)))


def generate_variation(image):
    variation = remove_white_border_non_binary(image)

    change_dpi = random_bool()
    change_dpi_first = False

    if change_dpi:
        if variation.shape[0] >= STARTING_DPI_CHANGE_MIN_HEIGHT and variation.shape[1] >= STARTING_DPI_CHANGE_MIN_WIDTH:
            change_dpi_first = random_bool()
        if change_dpi_first:
            variation = change_image_dpi(variation, random.choice(DPI_LIST))

    if not change_dpi and random_bool():
        x_factor = random.randint(MIN_RESIZE_PERCENTAGE, 99) / 100
        y_factor = random.randint(MIN_RESIZE_PERCENTAGE, 99) / 100
        variation = cv2.resize(variation,
                               (0, 0),
                               fx=x_factor,
                               fy=y_factor,
                               interpolation=cv2.INTER_CUBIC)

    if random_bool():
        variation = change_image_quality(variation,
                                         random.randrange(MIN_QUALITY,
                                                          MAX_QUALITY + 1,
                                                          QUALITY_RANDOMIZATION_STEP_SIZE))

    if random_bool():
        if random_bool():
            sign = 1
        else:
            sign = -1
        variation = rotate(variation, sign * random.randint(MIN_ROTATION_ANGLE,
                                                            MAX_ROTATION_ANGLE))

    if change_dpi and not change_dpi_first:
        variation = change_image_dpi(variation, random.choice(DPI_LIST))

    if random_bool():
        color = 0
        if random_bool():
            color = 255
        variation = speckle(variation,
                            SPECKLES_FRACTION,
                            color)
        if random_bool():
            variation = speckle(variation,
                                SPECKLES_FRACTION,
                                255 - color)
    return variation


def erase_image_randomly(image, percentage):
    binary_image = threshold_otsu(image)
    image_copy = np.array(image)

    number_of_pixels_to_remove = int(np.count_nonzero(binary_image == 0) * percentage / 100)
    number_of_pixels_removed = 0

    labels_count, labeled_image, stats, centroids = cv2.connectedComponentsWithStats(255 - binary_image, 8, cv2.CV_32S)

    labels = list(range(labels_count))

    while number_of_pixels_removed < number_of_pixels_to_remove:
        label = random.choice(labels)
        labels.remove(label)

        if np.all(binary_image[labeled_image == label] == 255):
            continue

        area = stats[label, cv2.CC_STAT_AREA]
        black_pixels_left = number_of_pixels_to_remove - number_of_pixels_removed
        if area <= black_pixels_left:
            image_copy[labeled_image == label] = 255
            number_of_pixels_removed += area
        else:
            transpose_image = random_bool()
            if transpose_image:
                image_copy = np.transpose(image_copy)
                labeled_image = np.transpose(labeled_image)

            black_pixels = np.transpose(np.where(labeled_image == label))
            i = random.randint(0, len(black_pixels) - black_pixels_left)
            black_pixels_to_remove = black_pixels[i:i + black_pixels_left]
            for pixel in black_pixels_to_remove:
                image_copy[pixel[0], pixel[1]] = 255

            if transpose_image:
                image_copy = np.transpose(image_copy)
                labeled_image = np.transpose(labeled_image)
            number_of_pixels_removed = number_of_pixels_to_remove

    return image_copy


def generate_random_white_image():
    return np.ones((random.randint(122, 366), random.randint(122, 366)), np.uint8) * 255


def generate_random_noisy_image():
    image = generate_random_white_image()

    if random_bool():
        image = speckle(image,
                        SPECKLES_FRACTION,
                        0)

    if random_bool(probability=0.25):
        for _ in range(random.randint(1, 10)):
            radius = random.randint(3, 20)
            x = random.randint(radius, image.shape[1] - radius)
            y = random.randint(radius, image.shape[0] - radius)
            if random_bool():
                cv2.circle(image, (x, y), radius, 0, -1)
            else:
                x1 = x - radius
                x2 = x + radius
                y1 = y - radius
                y2 = y + radius
                image[y1:y2, x1:x2] = 0

    if random_bool(probability=0.25):
        y1 = random.randint(0, image.shape[0] - 1)
        y2 = random.randint(max(0, y1 - 3), min(image.shape[0] - 1, y1 + 3))
        cv2.line(image, (0, y1), (image.shape[1], y2), color=0, thickness=1)

    if random_bool():
        image = speckle(image,
                        SPECKLES_FRACTION,
                        255)

    return image


def pad_to_height(image, height):
    difference = height - image.shape[0]
    top_padding = random.randint(0, difference)
    return pad_image(image,
                     top=top_padding,
                     bottom=difference - top_padding, )


def concatenate_images_with_random_spacing_and_height_padding(left_image, right_image):
    if left_image.shape[0] < right_image.shape[0]:
        left_image = pad_to_height(left_image, right_image.shape[0])
    if right_image.shape[0] < left_image.shape[0]:
        right_image = pad_to_height(right_image, left_image.shape[0])
    left_image = pad_image(left_image, right=random.randint(0, 50))
    concatenated = np.concatenate([left_image, right_image], axis=-1)
    return concatenated


def generate_special_forgery_class(genuines,
                                   forgeries):
    erased_images = [erase_image_randomly(image, random.randint(40, 100))
                     for image in random.sample(genuines, min(NUMBER_OF_SPECIAL_VARIATIONS, len(genuines)))]

    white_image = generate_random_white_image()
    random_white_images = [generate_random_noisy_image() for _ in range(NUMBER_OF_SPECIAL_VARIATIONS)] + [white_image]

    concatenated_genuines = []
    genuine_sample1 = random.sample(genuines, min(NUMBER_OF_SPECIAL_VARIATIONS, len(genuines)))
    genuine_sample2 = random.sample(genuines, min(NUMBER_OF_SPECIAL_VARIATIONS, len(genuines)))
    for i in range(len(genuine_sample1)):
        concatenated = concatenate_images_with_random_spacing_and_height_padding(genuine_sample1[i], genuine_sample2[i])
        concatenated_genuines.append(concatenated)

    concatenated_forgeries = []
    genuine_sample = random.sample(genuines, min(NUMBER_OF_SPECIAL_VARIATIONS, min(len(genuines), len(forgeries))))
    forgery_sample = random.sample(forgeries, min(NUMBER_OF_SPECIAL_VARIATIONS, min(len(genuines), len(forgeries))))
    for i in range(len(genuine_sample)):
        left_image = genuine_sample[i]
        right_image = forgery_sample[i]
        if random_bool():
            left_image, right_image = right_image, left_image
        concatenated = concatenate_images_with_random_spacing_and_height_padding(left_image, right_image)
        concatenated_forgeries.append(concatenated)

    return erased_images + random_white_images + concatenated_genuines + concatenated_forgeries


def pre_process_image(image):
    cropped_resized = resize_image_high_quality(remove_white_border(image),
                                                MODEL_INPUT_IMAGE_SIZE)
    return 255 - threshold_otsu(cropped_resized)


def class_from_file_name(file_name):
    return file_name[0:CLASS_NUMBER_LENGTH]


def extract_classes(iterable,
                    class_lambda,
                    item_mapper_lambda=lambda x: x):
    classes = dict()
    for item in iterable:
        c = class_lambda(item)
        if c not in classes:
            classes[c] = []
        classes[c].append(item_mapper_lambda(item))
    return classes


def extract_classes_from_file_names(paths,
                                    file_name_class_lambda=class_from_file_name):
    image_classes = dict()
    for path in paths:
        image_classes.update(extract_classes(os.listdir(path),
                                             file_name_class_lambda,
                                             lambda f: os.path.join(path, f)))
    return image_classes


def image_classes_from_paths(image_paths):
    image_paths_classes = extract_classes_from_file_names(image_paths, class_from_file_name)

    image_classes = dict()
    for c in image_paths_classes:
        image_classes[c] = [read_image_grayscale(image_path)
                            for image_path in image_paths_classes[c]]
    return image_classes


def cache_object(obj, file):
    with open(file, 'wb') as f:
        pickle.dump(obj, f)


def load_cached(file):
    with open(file, 'rb') as f:
        return pickle.load(f)


def load_image_classes():
    image_classes = None
    if not os.path.isfile('image_classes.pickle'):
        image_classes = image_classes_from_paths(SIGNATURE_CLASSES_PATHS)
        cache_object(image_classes, 'image_classes.pickle')
    if image_classes is None:
        image_classes = load_cached('image_classes.pickle')
    return image_classes


def generate_image_variations(image_classes):
    for c in image_classes:
        image_class = image_classes[c]
        variations = []
        for image in image_class:
            variations.extend([pre_process_image(generate_variation(image)) for _ in range(VARIATIONS_PER_IMAGE)])
        for i in range(len(image_class)):
            image_class[i] = pre_process_image(image_class[i])
        image_class.extend(variations)
    return image_classes


def save_images(image_classes, file_name):
    progress_bar = tqdm(total=len(image_classes))
    for c in image_classes:
        image_class = image_classes[c]
        directory = os.path.join(OUTPUT_DIRECTORY, c)
        if not os.path.isdir(directory):
            os.makedirs(directory)
        for i in range(len(image_class)):
            cv2.imwrite(os.path.join(directory, f'sig_{i}.png'), image_class[i])
        progress_bar.update()
    progress_bar.close()


def run():
    print('loading image classes...')
    image_classes = load_image_classes()

    print('augmenting data...')
    image_classes = generate_image_variations(image_classes)
    cache_object(image_classes, 'image_classes.pickle')

    if SAVE_IMAGES:
        save_images(image_classes, 'image_classes.pickle')

    # if GENERATE_SPECIAL_FORGERIES:
    #     print('generating special forgeries...')
    #
    #     special_forgery_classes = dict()
    #     classes = [*image_classes]
    #
    #     for c in classes:
    #         forgeries = []
    #         forgery_classes = random.sample(classes, NUMBER_OF_SPECIAL_VARIATIONS + 1)
    #         if forgery_classes.count(c) > 0:
    #             forgery_classes.remove(c)
    #         if len(forgery_classes) > NUMBER_OF_SPECIAL_VARIATIONS:
    #             forgery_classes.pop(0)
    #         for forgery_c in forgery_classes:
    #             forgeries.extend(image_classes[forgery_c])
    #         special_forgery_classes['s' + c] = generate_special_forgery_class(image_classes[c],
    #                                                                           forgeries)
    #
    #     generate_feature_vectors(special_forgery_classes, 'special_forgery_feature_vector_classes.pickle')


if __name__ == '__main__':
    run()
