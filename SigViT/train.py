import math
import os
import pickle
import random
import sys
from enum import Enum

import cv2
import numpy as np
import tensorflow as tf
from scipy.interpolate import interp1d
from tqdm import tqdm

from reporting import generate_report, generate_verification_simulation_report

keras = tf.keras
from keras.utils import Sequence
from keras.callbacks import LambdaCallback, ModelCheckpoint

INPUT_IMAGE_SIZE = (144, 96)
INPUT_SHAPE = (INPUT_IMAGE_SIZE[1], INPUT_IMAGE_SIZE[0], 1)
MARGIN = 0.5

TRAIN_IMAGE_CLASSES_FILE = 'train_image_classes.pickle'
VALIDATION_IMAGE_CLASSES_FILE = 'validation_image_classes.pickle'
TEST_IMAGE_CLASSES_FILE = 'test_image_classes.pickle'

TRAIN_CLASSES_PERCENTAGE = 0.7
VALIDATION_CLASSES_PERCENTAGE = 0.15
TEST_CLASSES_PERCENTAGE = 0.15

TRAIN_CLASSES_PER_BATCH = 40
TRAIN_VECTORS_PER_CLASS = 5
TRAIN_BATCHES = 100000

VALIDATION_CLASSES_PER_BATCH = 40
VALIDATION_VECTORS_PER_CLASS = 5
VALIDATION_BATCHES = 21500

EPOCHS = 100

EMBEDDING_NETWORK_FILE_NAME = 'embedding_network.h5'
EVALUATION_REPORT_FILE = 'triplet_report.txt'
TEST_EMBEDDING_CLASSES_FILE = 'test_embeddings.pickle'
TEST_PAIRS_FILE = 'test_pairs.pickle'


class TrainingMode(Enum):
    TrainWithSplittingClasses = 1
    TrainWithoutSplittingClasses = 2


TRAINING_MODE = TrainingMode.TrainWithSplittingClasses


def cache_object(obj, file):
    with open(file, 'wb') as f:
        pickle.dump(obj, f)


def load_cached(file):
    with open(file, 'rb') as f:
        return pickle.load(f)


def threshold_otsu(image):
    return cv2.threshold(image,
                         0,
                         255,
                         cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]


def resize_image_high_quality(image, new_size):
    if new_size[0] == image.shape[1] and new_size[1] == image.shape[0]:
        return np.array(image)
    return cv2.resize(image,
                      new_size,
                      interpolation=cv2.INTER_CUBIC)


def remove_white_border(binary_image):
    mask = (255 - binary_image) > 0

    height, width = binary_image.shape[0:2]
    mask1, mask2 = mask.any(0), mask.any(1)
    x1, x2 = mask1.argmax(), width - mask1[::-1].argmax()
    y1, y2 = mask2.argmax(), height - mask2[::-1].argmax()

    return binary_image[y1:y2, x1:x2]


def pre_process(image):
    cropped_resized = resize_image_high_quality(remove_white_border(image),
                                                INPUT_IMAGE_SIZE)
    binary = threshold_otsu(cropped_resized)
    inverted = 255.0 - binary.astype(np.float32)
    normalized = inverted / 255.0
    return np.expand_dims(normalized, axis=-1)


def file_exists(file):
    return os.path.isfile(file)


def class_train_validation_test_split(image_classes):
    keys = [*image_classes]

    keys_indices = list(range(len(keys)))
    random.shuffle(keys_indices)

    classes = ([], [], [])
    percentages = [TRAIN_CLASSES_PERCENTAGE,
                   VALIDATION_CLASSES_PERCENTAGE,
                   TEST_CLASSES_PERCENTAGE]

    for i in range(3):
        chosen_key_indices = keys_indices[:int(math.ceil(len(keys) * percentages[i]))]
        for j in range(len(chosen_key_indices)):
            key = keys[chosen_key_indices[j]]
            classes[i].append(key)
        keys_indices = keys_indices[len(chosen_key_indices):]

    return classes


class DataGenerator(Sequence):
    def __init__(self,
                 image_classes,
                 class_keys,
                 classes_per_batch,
                 images_per_class,
                 batches):
        self.image_classes = image_classes
        self.class_keys = class_keys
        self.classes_per_batch = classes_per_batch
        self.images_per_class = images_per_class
        self.batches = batches

    def __len__(self):
        return self.batches

    def __getitem__(self, _):
        batch_classes = random.sample(self.class_keys, self.classes_per_batch)
        images = []
        labels = []

        i = 1
        for c in batch_classes:
            number_of_images_in_class = len(self.image_classes[c])
            n = self.images_per_class // number_of_images_in_class
            remainder = self.images_per_class % number_of_images_in_class
            images.extend(self.image_classes[c] * n)
            images.extend(random.sample(self.image_classes[c], remainder))
            labels.extend([i] * self.images_per_class)
            i += 1

        return np.array(images), np.expand_dims(np.array(labels), axis=-1)


def multi_layer_perceptron(x, hidden_units, dropout_rate):
    for units in hidden_units:
        x = tf.keras.layers.Dense(units,
                                  kernel_regularizer=keras.regularizers.l2(1e-4),
                                  # bias_regularizer=keras.regularizers.l2(1e-4),
                                  activation=tf.nn.gelu)(x)
        x = tf.keras.layers.Dropout(dropout_rate)(x)
    return x


class Patches(tf.keras.layers.Layer):
    def __init__(self, patch_size, strides, **kwargs):
        super(Patches, self).__init__(**kwargs)
        self.patch_size = patch_size
        self.strides = strides

    def call(self, images):
        batch_size = tf.shape(images)[0]
        patches = tf.image.extract_patches(
            images=images,
            sizes=[1, self.patch_size, self.patch_size, 1],
            strides=[1, self.strides, self.strides, 1],
            rates=[1, 1, 1, 1],
            padding="VALID",
        )
        patch_dims = patches.shape[-1]
        # print(images.shape)
        # print(patches.shape)
        patches = tf.reshape(patches, [batch_size, -1, patch_dims])
        return patches


class PatchEncoder(tf.keras.layers.Layer):
    def __init__(self, num_patches, projection_dim, **kwargs):
        super(PatchEncoder, self).__init__(**kwargs)
        self.num_patches = num_patches
        self.projection = tf.keras.layers.Dense(units=projection_dim,
                                                # bias_regularizer=keras.regularizers.l2(1e-4),
                                                kernel_regularizer=keras.regularizers.l2(1e-4))
        self.position_embedding = tf.keras.layers.Embedding(input_dim=num_patches,
                                                            output_dim=projection_dim,
                                                            embeddings_regularizer=keras.regularizers.l2(1e-4))

    def call(self, patch):
        positions = tf.range(start=0, limit=self.num_patches, delta=1)
        encoded = self.projection(patch) + self.position_embedding(positions)
        return encoded


def create_vit_embedding_net(input_shape,
                             patch_size,
                             strides,
                             transformer_layers,
                             projection_dim,
                             num_heads,
                             transformer_units,
                             mlp_head_units,
                             embedding_size):
    input_layer = tf.keras.layers.Input(shape=input_shape)
    patches = Patches(patch_size, strides)(input_layer)
    num_patches = ((INPUT_IMAGE_SIZE[1] - patch_size) // strides + 1) * (
            (INPUT_IMAGE_SIZE[0] - patch_size) // strides + 1)

    encoded_patches = PatchEncoder(num_patches, projection_dim)(patches)

    for _ in range(transformer_layers):
        x1 = tf.keras.layers.LayerNormalization(epsilon=1e-6)(encoded_patches)
        attention_output = tf.keras.layers.MultiHeadAttention(num_heads=num_heads,
                                                              key_dim=projection_dim,
                                                              kernel_regularizer=keras.regularizers.l2(1e-4),
                                                              # bias_regularizer=keras.regularizers.l2(1e-4),
                                                              dropout=0.1)(x1, x1)
        x2 = tf.keras.layers.Add()([attention_output, encoded_patches])
        x3 = tf.keras.layers.LayerNormalization(epsilon=1e-6)(x2)
        x3 = multi_layer_perceptron(x3,
                                    hidden_units=transformer_units,
                                    dropout_rate=0.1)
        encoded_patches = tf.keras.layers.Add()([x3, x2])

    representation = tf.keras.layers.LayerNormalization(epsilon=1e-6)(encoded_patches)
    representation = tf.keras.layers.Flatten()(representation)
    # representation = tf.keras.layers.Lambda(lambda x: tf.reduce_mean(x, axis=1))(representation)
    representation = tf.keras.layers.Dropout(0.5)(representation)

    features = multi_layer_perceptron(representation,
                                      hidden_units=mlp_head_units,
                                      dropout_rate=0.1)

    features = tf.keras.layers.Dense(embedding_size,
                                     kernel_regularizer=keras.regularizers.l2(1e-4),
                                     # bias_regularizer=keras.regularizers.l2(1e-4),
                                     activation=tf.nn.tanh)(features)
    output = tf.keras.layers.Lambda(lambda x: tf.math.l2_normalize(x, axis=-1))(features)

    return tf.keras.Model(inputs=input_layer, outputs=output)


def triplet_loss(y_true, y_predict, margin=MARGIN):
    labels = tf.reshape(y_true, [-1])

    class_labels_table_rows = tf.expand_dims(labels, axis=1)
    class_labels_table_columns = tf.expand_dims(labels, axis=0)

    same_class_mask = tf.equal(class_labels_table_rows, class_labels_table_columns)

    vectors_table_rows = tf.expand_dims(y_predict, axis=1)
    vectors_table_columns = tf.expand_dims(y_predict, axis=0)

    square_pairwise_distances = tf.square(tf.subtract(vectors_table_rows, vectors_table_columns))
    distances = tf.sqrt(tf.maximum(tf.reduce_sum(square_pairwise_distances, axis=-1), 0.0) + 1e-12)

    same_class_multiplier = tf.cast(same_class_mask, tf.float32)
    anchor_positive_distances = distances * same_class_multiplier
    hardest_positive = tf.reduce_max(anchor_positive_distances, axis=-1)

    different_class_multiplier = tf.cast(~same_class_mask, np.float32)
    anchor_negative_distances = distances * different_class_multiplier + same_class_multiplier * 1000.0
    hardest_negative = tf.reduce_min(anchor_negative_distances, axis=-1)

    return tf.reduce_mean(tf.maximum(hardest_positive + margin - hardest_negative, 0.0))


def accuracy(y_true, y_predict):
    class_labels_table_rows = y_true
    class_labels_table_columns = tf.reshape(y_true, (1, -1))

    same_class_mask = tf.equal(class_labels_table_rows, class_labels_table_columns)

    vectors_table_rows = tf.expand_dims(y_predict, axis=1)
    vectors_table_columns = tf.expand_dims(y_predict, axis=0)

    square_pairwise_distances = tf.square(tf.subtract(vectors_table_rows, vectors_table_columns))
    distances = tf.sqrt(tf.maximum(tf.reduce_sum(square_pairwise_distances, axis=-1), 0.0) + 1e-12)

    same_vector_mask = tf.eye(tf.shape(distances)[0], dtype=bool)
    same_class_multiplier = tf.cast(same_class_mask, tf.float32)
    anchor_positive_distances = distances * same_class_multiplier

    different_class_multiplier = tf.cast(~same_class_mask, np.float32)
    anchor_negative_distances = distances * different_class_multiplier + same_class_multiplier * 1000.0

    anchor_positive_distances = tf.expand_dims(anchor_positive_distances, axis=-1)
    anchor_negative_distances = tf.expand_dims(anchor_negative_distances, axis=1)

    accuracy_mask = tf.expand_dims(same_class_mask & ~same_vector_mask, axis=-1)
    correct_triplets = tf.less(anchor_positive_distances, anchor_negative_distances)
    accuracy_mask = tf.broadcast_to(accuracy_mask, tf.shape(correct_triplets)) & ~same_class_mask

    return tf.reduce_mean(tf.cast(correct_triplets[accuracy_mask], tf.float32))


def append_lines_to_report(lines):
    with open(EVALUATION_REPORT_FILE, 'a') as f:
        f.writelines(lines)


def epoch_metrics_log(epoch, logs):
    line = f'epoch: {epoch}'
    line += ' - '
    line += f'loss: {logs["loss"]}'
    line += ' - '
    line += f'accuracy: {logs["accuracy"]}'
    line += ' - '
    line += f'validation loss: {logs["val_loss"]}'
    line += ' - '
    line += f'validation accuracy: {logs["val_accuracy"]}'
    return line


def train(image_classes,
          train_image_classes,
          validation_image_classes):
    patch_size = 16
    strides = patch_size // 2
    projection_dim = 64
    num_heads = 8
    transformer_units = [projection_dim * 2, projection_dim]
    transformer_layers = 8
    mlp_head_units = [64, 32]
    embedding_size = 32

    if os.path.isfile('embedding_network.h5'):
        embedding_net = tf.keras.layers.load_model('embedding_network.h5', compile=False)
    else:
        embedding_net = create_vit_embedding_net(INPUT_SHAPE,
                                                 patch_size,
                                                 strides,
                                                 transformer_layers,
                                                 projection_dim,
                                                 num_heads,
                                                 transformer_units,
                                                 mlp_head_units,
                                                 embedding_size)
    embedding_net.summary()

    embedding_net.compile(loss=triplet_loss,
                          optimizer=tf.keras.optimizers.Adam(),
                          metrics=[accuracy])

    train_data_generator = DataGenerator(image_classes,
                                         train_image_classes,
                                         TRAIN_CLASSES_PER_BATCH,
                                         TRAIN_VECTORS_PER_CLASS,
                                         TRAIN_BATCHES)

    validation_data_generator = DataGenerator(image_classes,
                                              validation_image_classes,
                                              VALIDATION_CLASSES_PER_BATCH,
                                              VALIDATION_VECTORS_PER_CLASS,
                                              VALIDATION_BATCHES)

    append_lines_to_report('[training log]\n\n')

    update_report_callback = LambdaCallback(
        on_epoch_end=lambda epoch, logs: append_lines_to_report(
            epoch_metrics_log(epoch, logs) + '\n'
        ))

    last_checkpoint_callback = ModelCheckpoint(
        filepath=EMBEDDING_NETWORK_FILE_NAME + '_checkpoint_last.h5',
        save_best_only=False)

    best_checkpoint_callback = ModelCheckpoint(
        filepath=EMBEDDING_NETWORK_FILE_NAME + '_{epoch:02d}_{val_accuracy:0.4f}.h5',
        monitor='val_accuracy',
        mode='max',
        save_best_only=True)

    embedding_net.fit_generator(generator=train_data_generator,
                                steps_per_epoch=TRAIN_BATCHES,
                                validation_data=validation_data_generator,
                                validation_steps=VALIDATION_BATCHES,
                                epochs=EPOCHS,
                                callbacks=[update_report_callback,
                                           last_checkpoint_callback,
                                           best_checkpoint_callback])


def evaluate(image_classes,
             test_image_classes):
    embedding_net = keras.models.load_model(EMBEDDING_NETWORK_FILE_NAME,
                                            compile=False,
                                            custom_objects={'Patches': Patches, 'PatchEncoder': PatchEncoder})

    embedding_net.summary()

    embedding_net.compile(loss=triplet_loss,
                          optimizer=keras.optimizers.Adam(clipvalue=6),
                          metrics=[accuracy])

    test_data_generator = DataGenerator(image_classes,
                                        test_image_classes,
                                        VALIDATION_CLASSES_PER_BATCH,
                                        VALIDATION_VECTORS_PER_CLASS,
                                        VALIDATION_BATCHES)

    append_lines_to_report('\n[evaluation log]\n')

    results = embedding_net.evaluate_generator(generator=test_data_generator,
                                               steps=VALIDATION_BATCHES)

    append_lines_to_report(f'test loss: {results[0]} - test accuracy: {results[1]}\n')


def predict_batch_feature_vectors(batch,
                                  model):
    return model.predict(np.array(batch, np.float32))


class BatchVectorizer:
    def __init__(self, model, batch_size=512):
        self.__items = []
        self.__classes = []
        self.__feature_vector_classes = dict()
        self.__batch_size = batch_size
        self.__model = model

    def __predict(self):
        if len(self.__items) == 0:
            return
        feature_vectors = predict_batch_feature_vectors(self.__items, self.__model)
        for i in range(len(self.__classes)):
            c = self.__classes[i]
            if c not in self.__feature_vector_classes:
                self.__feature_vector_classes[c] = []
            self.__feature_vector_classes[c].append(feature_vectors[i])
        self.__items = []
        self.__classes = []

    def __predict_if_batch_size_reached(self):
        if len(self.__items) >= self.__batch_size:
            self.__predict()

    def feed_inputs(self, items, classes):
        for i in range(len(items)):
            self.__items.append(items[i])
            self.__classes.append(classes[i])
            self.__predict_if_batch_size_reached()

    def feed_input(self, item, item_class):
        self.__items.append(item)
        self.__classes.append(item_class)
        self.__predict_if_batch_size_reached()

    def finalize(self):
        self.__predict()

    def get_feature_vectors(self):
        return self.__feature_vector_classes


def generate_or_load_test_embedding_classes(image_classes, test_image_classes):
    if not file_exists(TEST_EMBEDDING_CLASSES_FILE):
        batch_vectorizer = BatchVectorizer(tf.keras.models.load_model(EMBEDDING_NETWORK_FILE_NAME,
                                                                      compile=False,
                                                                      custom_objects={'Patches': Patches,
                                                                                      'PatchEncoder': PatchEncoder}))

        classes_count = len(test_image_classes)
        progress_bar = tqdm(total=classes_count)
        for c in test_image_classes:
            feature_vectors = image_classes[c]
            batch_vectorizer.feed_inputs(feature_vectors, [c] * len(feature_vectors))
            progress_bar.update()
        batch_vectorizer.finalize()
        progress_bar.close()

        test_embedding_classes = batch_vectorizer.get_feature_vectors()
        cache_object(test_embedding_classes, TEST_EMBEDDING_CLASSES_FILE)
        return test_embedding_classes

    return load_cached(TEST_EMBEDDING_CLASSES_FILE)


def generate_pairs(feature_vector_classes,
                   references_per_class=14,
                   forgery_classes_per_reference=4,
                   forgeries_per_forgery_class=4):
    keys = [*feature_vector_classes]
    number_of_classes = len(keys)

    references = []
    others = []
    labels = []

    for c in range(number_of_classes - forgery_classes_per_reference):
        c = random.randint(0, len(keys) - 1)
        key = keys.pop(c)

        reference_indices = random.sample(list(range(len(feature_vector_classes[key]))),
                                          min(references_per_class, len(feature_vector_classes[key])))

        for r in reference_indices:
            reference = [key, r]
            genuines = [[key, i] for i in range(len(feature_vector_classes[key]))]
            references.extend([reference] * len(genuines))
            others.extend(genuines)
            labels.extend([1] * len(genuines))

            forgery_keys = random.sample(keys, min(forgery_classes_per_reference, len(keys)))
            for forgery_key in forgery_keys:
                forgery_class = feature_vector_classes[forgery_key]
                forgery_indices = random.sample(list(range(len(forgery_class))),
                                                min(forgeries_per_forgery_class, len(forgery_class)))
                forgeries = [[forgery_key, f] for f in forgery_indices]

                references.extend([reference] * len(forgeries))
                others.extend(forgeries)
                labels.extend([0] * len(forgeries))

    return references, others, labels


def distance(x, y):
    squared_euclidean_distance = np.sum(np.square(np.subtract(x, y)))
    return np.sqrt(np.maximum(squared_euclidean_distance, 0.0))


def distance_batch(x, y):
    squared_euclidean_distance = np.sum(np.square(np.subtract(x, y)), axis=-1)
    return np.sqrt(np.maximum(squared_euclidean_distance, 0.0))


def run():
    if not file_exists('pre_processed_image_classes.pickle'):
        image_classes = load_cached('image_classes.pickle')
        for c in image_classes:
            image_classes[c] = [pre_process(image) for image in image_classes[c]]
        cache_object(image_classes, 'pre_processed_image_classes.pickle')
    else:
        image_classes = load_cached('pre_processed_image_classes.pickle')

    # if not file_exists('pre_processed_special_forgery_classes.pickle'):
    #     special_forgery_classes = load_cached('special_forgery_classes.pickle')
    #     for c in special_forgery_classes:
    #         special_forgery_classes[c] = [special_forgery_classes(image) for image in special_forgery_classes[c]]
    #     cache_object(special_forgery_classes, 'pre_processed_special_forgery_classes.pickle')
    # else:
    #     special_forgery_classes = load_cached('pre_processed_special_forgery_classes.pickle')

    train_image_classes = None
    validation_image_classes = None
    test_image_classes = None

    if not file_exists(TRAIN_IMAGE_CLASSES_FILE) or \
            not file_exists(VALIDATION_IMAGE_CLASSES_FILE) or \
            not file_exists(TEST_IMAGE_CLASSES_FILE):

        train_image_classes, \
            validation_image_classes, \
            test_image_classes = class_train_validation_test_split(image_classes)

        if TRAINING_MODE == TrainingMode.TrainWithoutSplittingClasses:
            train_image_classes = [*image_classes]

        cache_object(train_image_classes, TRAIN_IMAGE_CLASSES_FILE)
        cache_object(validation_image_classes, VALIDATION_IMAGE_CLASSES_FILE)
        cache_object(test_image_classes, TEST_IMAGE_CLASSES_FILE)

    if not file_exists(EMBEDDING_NETWORK_FILE_NAME):
        if train_image_classes is None:
            train_image_classes = load_cached(TRAIN_IMAGE_CLASSES_FILE)
        if validation_image_classes is None:
            validation_image_classes = load_cached(VALIDATION_IMAGE_CLASSES_FILE)

        train(image_classes,
              train_image_classes,
              validation_image_classes)

    if file_exists(EMBEDDING_NETWORK_FILE_NAME):
        if test_image_classes is None:
            test_image_classes = load_cached(TEST_IMAGE_CLASSES_FILE)

        # evaluate(image_classes,
        #          test_image_classes)

        test_embedding_classes = generate_or_load_test_embedding_classes(image_classes,
                                                                         test_image_classes)
        if not file_exists(TEST_PAIRS_FILE):
            test_pairs = generate_pairs(test_embedding_classes)
            cache_object(test_pairs, TEST_PAIRS_FILE)
        else:
            test_pairs = load_cached(TEST_PAIRS_FILE)

        references, others, labels = test_pairs

        distances = []
        genuine_distances = []
        forgery_distances = []
        for i in range(len(labels)):
            reference = references[i]
            other = others[i]
            label = labels[i]

            rc, ri = reference
            oc, oi = other

            reference_vector = test_embedding_classes[rc][ri]
            other_vector = test_embedding_classes[oc][oi]

            euclidean_distance = distance(reference_vector, other_vector)
            distances.append(euclidean_distance)
            if label == 1:
                genuine_distances.append(euclidean_distance)
            else:
                forgery_distances.append(euclidean_distance)

        max_distance = 2.0
        bins_count = 100
        bin_size = max_distance / bins_count
        bins = [i * bin_size for i in range(101)]

        forgery_distances_histogram = np.histogram(forgery_distances, bins)[0]
        genuine_distances_histogram = np.histogram(genuine_distances, bins)[0]

        min_difference = sys.float_info.max
        min_difference_i = -1
        for i in range(len(bins) // 4, len(bins) * 3 // 4):
            difference = abs(forgery_distances_histogram[i] - genuine_distances_histogram[i])
            if difference < min_difference:
                min_difference = difference
                min_difference_i = i

        equal_on_distance = min_difference_i * bin_size
        distance_to_prob = interp1d([0.0, equal_on_distance, max_distance], [1.0, 0.5, 0.0])
        probabilities = distance_to_prob(distances)

        labels = np.array(labels)
        histogram_bin_size = 5

        generate_report(probabilities,
                        labels,
                        histogram_bin_size,
                        EVALUATION_REPORT_FILE,
                        'triplet_genuine_distribution.png',
                        'triplet_forgery_distribution.png',
                        'triplet_mixed_distributions.png')

        generate_verification_simulation_report(test_embedding_classes,
                                                lambda x, y: distance_to_prob(distance_batch(x, y)),
                                                EVALUATION_REPORT_FILE)


if __name__ == '__main__':
    run()
