package com.progressoft;

import org.json.JSONObject;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class Main {
    private static void setRequestHeader(HttpsURLConnection connection,
                                         String bearerToken) throws ProtocolException {
        connection.setRequestProperty("Authorization", "Bearer " + bearerToken);
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestMethod("POST");
        connection.setDoOutput(true);
    }

    private static void sendRequest(HttpsURLConnection connection,
                                    String request) throws IOException {
        OutputStream outputStream = connection.getOutputStream();
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, StandardCharsets.UTF_8);
        outputStreamWriter.write(request);
        outputStreamWriter.flush();
        outputStreamWriter.close();
        outputStream.close();
    }

    private static String getResponse(HttpsURLConnection connection) throws IOException {
        InputStream inputStream = connection.getInputStream();
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

        String inputLine;
        StringBuilder stringBuilder = new StringBuilder();
        while ((inputLine = bufferedReader.readLine()) != null) {
            stringBuilder.append(inputLine).append("\n");
        }

        bufferedReader.close();
        inputStreamReader.close();
        inputStream.close();

        return stringBuilder.toString();
    }

    public static void main(String[] args) throws Exception {
        URL tccServerAnomalyAnalysisApiUrl = new URL("https://apollo-pstctc-master-ps-tctc.machine-dev.progressoft.cloud/tctc/api/AnomalityAnalysis/Analyze");
        HttpsURLConnection connection = (HttpsURLConnection) tccServerAnomalyAnalysisApiUrl.openConnection();
        setRequestHeader(connection, "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOiIyMDIzLTA3LTE3VDE0OjMzOjQ4IiwianRpIjoiNTJmYWMyYTJjY2E5NDZiMjk2YTY2Y2QxNzgzNjE1ZDYiLCJuYW1lIjoiUFMtRUNDIiwic3ViIjoiUFMtRUNDIiwiZXhwIjoyMTMxMDk3NjI4LCJpc3MiOiJQUy1UQ1RDIiwiYXVkIjoiUFMtVENUQyJ9.Qr6_ao74VssBU-y6jNW36GA3BVHAtN6vCK_mVD_NR3o");
        String request = """
                {
                  "oid": "123",
                  "serial": "123456789",
                  "bfdAccount": "4",
                  "payAccount": "4",
                  "date": "2022-08-17T11:34:20.515Z",
                  "presentmentDate": "2022-08-17T11:34:20.515Z",
                  "amount": 12000,
                  "sequence": 1
                }
                """;
        sendRequest(connection, request);
        String response = getResponse(connection);
        JSONObject jsonObject = new JSONObject(response);
        System.out.println(jsonObject.toString(2));
    }
}