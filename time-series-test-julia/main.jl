function main()
    time_series = Vector{Float64}()
    open("/home/u764/Development/data/tsa/fadi-tsa/fadi11.csv", "r") do f
        while !eof(f)
            line = strip(readline(f))
            if isempty(line)
                continue
            end
            push!(time_series, parse(Float64, line))
        end
    end

    @time begin
        max_period = min(365, floor(Int, length(time_series) / 2.0))

        inputs = 0:length(time_series)-1
        inputs_2_pi = (2 * pi) .* inputs

        min_error = 0.0
        min_error_is_set = false

        for period in 1:max_period
            frequencies = reshape(inputs_2_pi ./ period, (1, length(time_series)))

            for number_of_coefficients in 1:period
                orders = reshape(0:number_of_coefficients-1, (number_of_coefficients, 1))
                angles = frequencies .* orders
                design_matrix = [reshape(ones(length(time_series)), (1, :)); reshape(copy(inputs), (1, :)); sin.(angles); cos.(angles)]
                design_matrix = transpose(design_matrix)
                try
                    coefficients = reshape(design_matrix \ time_series, (1, :))
                    error = dropdims(sum(design_matrix .* coefficients, dims=2), dims=2) .- time_series
                    error = error .* error
                    error = sum(error) ./ length(time_series)
                    if !min_error_is_set || min_error > error
                        min_error = error
                        min_error_is_set = true
                    end
                catch
                end
            end
        end
    end
end

main()