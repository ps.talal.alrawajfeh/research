#!/usr/bin/python3.6

import os
from matplotlib import pyplot as plt
import numpy as np


def plot_report(file_path):
    with open(file_path, 'r') as f:
        splitted = list(map(lambda line:
                            list(map(lambda l: l.split(':')[1].strip(),
                                     line.split('-'))),
                            filter(lambda line: line.strip() != '', f.readlines())))

    epochs = [None for _ in range(30)]

    for line in splitted:
        epoch = int(line[0])

        metrics = dict()
        metrics['epoch'] = epoch
        metrics['loss'] = float(line[1])
        metrics['accuracy'] = float(line[2])
        metrics['val_loss'] = float(line[3])
        metrics['val_accuracy'] = float(line[4])

        epochs[epoch] = metrics

    x = [metrics['epoch'] for metrics in epochs if metrics is not None]
    accuracies = [epochs[i]['accuracy'] for i in x if epochs[i] is not None]
    val_accuracies = [epochs[i]['val_accuracy']
                      for i in x if epochs[i] is not None]

    fig = plt.gcf()
    fig.set_size_inches(18.5, 10.5)

    plt.xlim(-1, 30)
    plt.ylim(0.5, 1.02)
    plt.xticks(np.arange(0, 30))
    plt.yticks(np.arange(0.5, 1.02, step=0.02))
    plt.plot(x, accuracies, c='blue')
    plt.plot(x, val_accuracies, c='red')

    if not os.path.isdir('./plots'):
        os.makedirs('./plots')
    fig.savefig('./plots/' + os.path.basename(file_path)
                [:-3] + '.png', dpi=200)
    plt.close('all')


def main():
    all_reports = os.listdir('./reports')
    all_reports = [f for f in all_reports if f[-7:] == '.report']
    for f in all_reports:
        plot_report('./reports/' + f)


if __name__ == "__main__":
    main()
