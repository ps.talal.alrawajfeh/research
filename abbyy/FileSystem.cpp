// (c) 2018 ABBYY Production LLC 
// SAMPLES code is property of ABBYY, exclusive rights are reserved.
//
// DEVELOPER is allowed to incorporate SAMPLES into his own APPLICATION and modify it under
// the  terms of  License Agreement between  ABBYY and DEVELOPER.

// ABBYY FineReader Engine 12 Sample

// Helper functions for filesystem operations

#include <FileSystem.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>

// Checks if directory exists in a file system
bool FileSystem::CheckIfDirExists( const CBstr& path )
{
	struct stat st;
	return ( stat( path.ConvertToString(), &st ) == 0 ) && S_ISDIR( st.st_mode );
}

// Creates directory. Directory should not exist before this call.
bool FileSystem::CreateDirectory( const CBstr& path )
{
	return mkdir( path.ConvertToString(), 0777 ) == 0;	
}

void FileSystem::GetFileNames( CBstr directoryPath, IStringsCollection* strings )
{
	DIR* dir;
	struct dirent *ent;
	dir = opendir( directoryPath.ConvertToString() );
	if( !dir ) { 
		return;
	}
	while( ( ent = readdir( dir ) ) != 0 ) {
		CBstr filePath( ent->d_name );
		if( filePath.Length() > 3) {
			strings->Add( filePath );
		}
	}
	closedir( dir );
}

