// (c) 2018 ABBYY Production LLC 
// SAMPLES code is property of ABBYY, exclusive rights are reserved.
//
// DEVELOPER is allowed to incorporate SAMPLES into his own APPLICATION and modify it under
// the  terms of  License Agreement between  ABBYY and DEVELOPER.

// ABBYY FineReader Engine 12 Sample

// Helper functions for filesystem operations

#ifndef FilesystemHelpers_h
#define FilesystemHelpers_h
#include <BstrWrap.h>

namespace FileSystem {
	// Checks if directory exists in a file system
	bool CheckIfDirExists( const CBstr& path );

	// Creates directory. Directory should not exist before this call.
	bool CreateDirectory( const CBstr& path );	

	// Get file names collection in the specified directory
	void GetFileNames( CBstr imagesPath, IStringsCollection* strings );
}

#endif
