#ifndef _CONFIG_H_
#define _CONFIG_H_

static wchar_t* GetFreProjectId() {
	static wchar_t FreProjectId[] = L"VvTxzkYarHRXmqN6UtXB";
	return FreProjectId;
}

static wchar_t* GetLicensePath() {
	static wchar_t LicensePath[] = L"";
	return LicensePath;
}

static wchar_t* GetLicensePassword() {
	static wchar_t LicensePassword[] = L"";
	return LicensePassword;
}

static wchar_t* GetFreBinariesPath() {
	static wchar_t FreBinariesPath[] = L"/opt/ABBYY/FREngine12/Bin";
	return FreBinariesPath;
}

#endif // _CONFIG_H_