// (c) 2018 ABBYY Production LLC 
// SAMPLES code is property of ABBYY, exclusive rights are reserved.
//
// DEVELOPER is allowed to incorporate SAMPLES into his own APPLICATION and modify it under
// the  terms of  License Agreement between  ABBYY and DEVELOPER.

// ABBYY FineReader Engine 12 Sample

// Smart pointer to any FineReader Engine interface.

#ifndef FineInterfacePtr_h
#define FineInterfacePtr_h

template<class T>
class CSafePtr {
public:
	// Constructors
	CSafePtr( T* _ptr = 0 ) { attach( _ptr ); }
	CSafePtr( const CSafePtr& other ) { attach( other.ptr ); }

	// Destructor
	~CSafePtr() { release(); }

	// Returns true if pointer is null
	bool IsNull() const { return ptr == 0; }

	// Returns pointer to the internal pointer to the interface object
	T** GetBuffer();	
	void** GetQIBuffer();

	// Extractors
	T& operator *() const { return *ptr; }
	T* operator ->() const { return ptr; }
	T** operator &() { return GetBuffer(); }

	void CopyTo( T** result ) const;
	operator T*() const { return ptr; }

	// Assigment operators
	CSafePtr& operator = ( const CSafePtr& other );
	CSafePtr& operator = ( T* _ptr );

private:
	// Functions for internal use
	void attach( T* _ptr );
	void release();

	T* ptr;
};

template<class T>
inline T** CSafePtr<T>::GetBuffer()
{	
	release();
	return &ptr;	
}


template<class T>
inline void** CSafePtr<T>::GetQIBuffer()
{
	release();
	return reinterpret_cast<void**>( &ptr );
}

template<class T>
inline CSafePtr<T>& CSafePtr<T>::operator = ( const CSafePtr<T>& other )
{
	if( other.ptr != ptr ) {
		release();
		attach( other.ptr );	
	}
	return *this;
}

template<class T>
inline CSafePtr<T>& CSafePtr<T>::operator = ( T* _ptr )
{
	if( _ptr != ptr ) {
		release();
		attach( _ptr );
	}
	return *this;
}

template<class T>
void CSafePtr<T>::CopyTo( T** result ) const
{
	*result = ptr;
	if( (*result) != 0 ) {
		 (*result)->AddRef();
	}
}

template<class T>
inline void CSafePtr<T>::attach( T* _ptr )
{
	if( _ptr != 0 ) {
		_ptr->AddRef();
	}
	ptr = _ptr;
}

template<class T>
inline void CSafePtr<T>::release()
{
	if( ptr != 0 ) {
		ptr->Release();
	}
	ptr = 0;
}

#endif // FineInterfacePtr_h
