// (c) 2018 ABBYY Production LLC 
// SAMPLES code is property of ABBYY, exclusive rights are reserved.
//
// DEVELOPER is allowed to incorporate SAMPLES into his own APPLICATION and modify it under
// the  terms of  License Agreement between  ABBYY and DEVELOPER.

// ABBYY FineReader Engine 12 Sample

// Helper functions for BSTR handling

#include "BstrWrap.h"
#include "AbbyyException.h"
#include <wchar.h>
#include <locale.h>
#include <stdarg.h>

CBstr::CBstr():
	convertedStringBuffer( 0 )
{ 
	stringBuffer = ::FREngineAllocString( L"" ); 
}

CBstr::CBstr( const wchar_t* str ):
	convertedStringBuffer( 0 )
{
	stringBuffer = ::FREngineAllocString( str ); 
	
}

CBstr::CBstr( const CBstr& other ):
	convertedStringBuffer( 0 )
{ 
	stringBuffer = ::FREngineAllocString( other.Ptr() );
}

CBstr::CBstr( const char* str ):
	convertedStringBuffer( 0 )
{
	mbstate_t state;
	memset( &state, 0, sizeof(state) );
	size_t length = mbsrtowcs( 0, &str, 0, &state );
	if( length == (size_t) -1 ) {
		throw CAbbyyException( L"Invalid char to wide char conversion." ); 
	}
	stringBuffer = ::FREngineAllocStringLen( 0, length ); // Allocates length + 1 chars
	memset( &state, 0, sizeof(state) );
	mbsrtowcs( stringBuffer, &str, length + 1, &state );
}

CBstr::~CBstr() 
{
	freeBuffer(); 
	freeConversionBuffer(); 
}

int CBstr::Length() const
{
	return int( wcslen( stringBuffer ) );
}

BSTR* CBstr::GetBuffer()
{
	freeBuffer();
	return &stringBuffer;
}

CBstr::operator const wchar_t*() const
{
	return stringBuffer;
}

const wchar_t* CBstr::Ptr() const
{
	return stringBuffer;
}

CBstr::operator wchar_t*()
{
	return stringBuffer;
}

wchar_t* CBstr::Ptr()
{
	return stringBuffer;
}

const char* CBstr::ConvertToString() const 
{
	mbstate_t state;
	memset( &state, 0, sizeof(state) );
	const wchar_t* src = (const wchar_t*)stringBuffer;
	int lengthRequired = wcsrtombs( 0, &src, 0, &state);
	if( lengthRequired < 0 ) {
		throw CAbbyyException( L"Invalid wide char to char conversion." ); 
	}
	freeConversionBuffer();
	convertedStringBuffer = new char[lengthRequired + 1];
	memset( &state, 0, sizeof(state) );
	wcsrtombs( convertedStringBuffer, &src, lengthRequired + 1, &state );
	return convertedStringBuffer;
}

CBstr& CBstr::operator += ( const CBstr& string )
{
	*this = Concatenate( this->stringBuffer, string.stringBuffer );
	return *this;
}

CBstr CBstr::operator + ( const CBstr& string ) const
{
	return Concatenate( this->stringBuffer, string.stringBuffer );
}

CBstr& CBstr::operator = ( const wchar_t* string )
{
	freeBuffer();
	stringBuffer = ::FREngineAllocString( string );
	return *this;
}

CBstr& CBstr::operator = ( const CBstr& string )
{
	freeBuffer();
	stringBuffer = ::FREngineAllocString( string );
	return *this;
}

bool CBstr::operator != ( const CBstr& string ) const
{
	if( stringBuffer == 0 || string.stringBuffer == 0 ) {
		return stringBuffer != string.stringBuffer;
	}
	return wcscmp( this->stringBuffer, string.stringBuffer ) != 0;
}

bool CBstr::operator == ( const CBstr& string ) const
{
	if( stringBuffer == 0 || string.stringBuffer == 0 ) {
		return stringBuffer == string.stringBuffer;
	}
	return wcscmp( this->stringBuffer, string.stringBuffer ) == 0;
}

wchar_t CBstr::operator [] ( int index ) const
{
	return stringBuffer[index];
}

void CBstr::freeBuffer()
{
	if( stringBuffer != 0 ) {
		::FREngineFreeString( stringBuffer );
		stringBuffer = 0;
	}
}

void CBstr::freeConversionBuffer() const 
{
	if( convertedStringBuffer != 0 ) {
		delete[] convertedStringBuffer;
		convertedStringBuffer = 0;
	}
}

CBstr Concatenate( const wchar_t* string1, const wchar_t* string2 )
{
	CBstr bstr;
	const size_t length = wcslen( string1 ) + wcslen( string2 );
	BSTR* bstrBuffer = bstr.GetBuffer();
	if( bstrBuffer != 0 ) {
		*bstrBuffer = ::FREngineAllocStringLen( 0, length ); // Allocates length + 1 chars
		if( *bstrBuffer != 0 ) {
			wcscpy( *bstrBuffer, string1 );
			wcscat( *bstrBuffer, string2 );
		}
	}
	return bstr;
}

CBstr Format( const wchar_t* _Format, ... )
{
    int n = 100;
    wchar_t* buf = 0;
    va_list _Arglist;
    int res = -1;
    CBstr bstr;
    va_start( _Arglist, _Format );
    //swprintf
    while( res < 0 ){
        delete[] buf;
        buf = new wchar_t[n];
        res = vswprintf( buf, n, _Format, _Arglist );
        n *= 2;
    }
    va_end( _Arglist );
    bstr = buf;
    delete[] buf;
    return bstr;
}

