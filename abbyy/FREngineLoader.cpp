// (c) 2018 ABBYY Production LLC 
// SAMPLES code is property of ABBYY, exclusive rights are reserved.
//
// DEVELOPER is allowed to incorporate SAMPLES into his own APPLICATION and modify it under
// the  terms of  License Agreement between  ABBYY and DEVELOPER.

// ABBYY FineReader Engine 12 Sample

// Helper functions for loading and unloading FREngine

#include "FREngineLoader.h"
#include "AbbyyException.h"
#include "Config.h"

// Global FineReader Engine object.
CSafePtr<IEngine> FREngine;

//	This function loads and initializes FineReader Engine.
void LoadFREngine()
{
	if( FREngine != 0 ) {
		// Already loaded
		return;
	}

	CheckResult( InitializeEngine( CBstr( GetFreProjectId() ), CBstr( GetLicensePath() ),
		CBstr( GetLicensePassword() ), CBstr( L"" ), CBstr( L"" ), false, FREngine.GetBuffer() ) );
}

//	This function deinitializes and unloads FineReader Engine
void UnloadFREngine()
{
	// Release Engine object
	FREngine = 0;

	if( FAILED( DeinitializeEngine() ) ) {
		throw CAbbyyException( L"Error while unloading ABBYY FineReader Engine" );
	}
}

