// (c) 2018 ABBYY Production LLC 
// SAMPLES code is property of ABBYY, exclusive rights are reserved.
//
// DEVELOPER is allowed to incorporate SAMPLES into his own APPLICATION and modify it under
// the  terms of  License Agreement between  ABBYY and DEVELOPER.

// ABBYY FineReader Engine 12 Sample

// Helper functions for loading and unloading FREngine

#ifndef FREngineLoader_h
#define FREngineLoader_h

#include "FREngine.h"
#include "SafePtr.h"

// Global FineReader Engine object.
extern CSafePtr<IEngine> FREngine;

//	This function loads and initializes FineReader Engine.
void LoadFREngine();

//	This function deinitializes and unloads FineReader Engine.
void UnloadFREngine();

#endif // FREngineLoader_h
