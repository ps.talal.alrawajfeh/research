// (c) 2018 ABBYY Production LLC 
// SAMPLES code is property of ABBYY, exclusive rights are reserved.
//
// DEVELOPER is allowed to incorporate SAMPLES into his own APPLICATION and modify it under
// the  terms of  License Agreement between  ABBYY and DEVELOPER.

// ABBYY FineReader Engine 12 Sample

// Helper functions for BSTR handling

#ifndef StringHelpers_h
#define StringHelpers_h

#include <wtypes.h>
#include <wchar.h>
#include <FREngine.h>
#include <FREngineData.h>


// Simple wrapper class for BSTR
class CBstr {
public:
	// Constructors
	CBstr();
	CBstr( const wchar_t* str );
	CBstr( const CBstr& other );
	CBstr( const char* str );

	// Destructor
	~CBstr();
	
	// Length of the string
	int Length() const; 

	// Pointer to the internal buffer
	BSTR* GetBuffer();

	// Extractors
	operator const wchar_t*() const;
	const wchar_t* Ptr() const;
	operator wchar_t*();
	wchar_t* Ptr();

	// Conversions 
	const char* ConvertToString() const;

	// Operators
	CBstr& operator += ( const CBstr& string );
	CBstr operator + ( const CBstr& string ) const;
	CBstr& operator = ( const wchar_t* string );
	CBstr& operator = ( const CBstr& string );
	bool operator != ( const CBstr& string ) const;
	bool operator == ( const CBstr& string ) const;
	wchar_t operator [] ( int index ) const;

private:
	// Free string buffer
	void freeBuffer();
	BSTR stringBuffer;

	// Conversion buffer
	void freeConversionBuffer() const;
	mutable char* convertedStringBuffer;
};

// Concatenates two strings
CBstr Concatenate( const wchar_t* string1, const wchar_t* string2 );
// wsprintf analogue
CBstr Format( const wchar_t* _Format, ... );

#endif // StringHelpers_h

