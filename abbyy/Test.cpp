
#include "FREngineLoader.h"
#include "BstrWrap.h"
#include "AbbyyException.h"
#include "Config.h"

void displayMessage( const wchar_t* text );
void processImage();

void executeTask()
{
	displayMessage( L"Initializing Engine..." );
	LoadFREngine();

	try {
		processImage();
	} catch( CAbbyyException& e ) {
		displayMessage( e.Description() );
	}

	displayMessage( L"Deinitializing Engine..." );
	UnloadFREngine();
}

void processImage()
{	
	displayMessage( L"Loading image..." );
	CBstr imagePath(L"/home/u764/Downloads/extracted-id-card.jpg");
	CSafePtr<IFRDocument> frDocument = 0;
	CheckResult( FREngine->CreateFRDocumentFromImage( imagePath, 0, frDocument.GetBuffer() ) );

	displayMessage( L"Recognizing..." );
	CheckResult( frDocument->Process() );

	displayMessage( L"Saving results..." );
	CBstr exportPath(L"./finereader-original.docx");
	CheckResult( frDocument->Export(  exportPath, FEF_DOCX, 0  ) );
}

void displayMessage( const wchar_t* text )
{
	wprintf( text );
	wprintf( L"\n" );
}

int main(int argc, char **argv)
{
	try {
		executeTask();
	} catch( CAbbyyException& e ) {
		displayMessage( e.Description() );
	}

	return 0;
}

