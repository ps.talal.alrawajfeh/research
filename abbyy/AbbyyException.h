// (c) 2018 ABBYY Production LLC 
// SAMPLES code is property of ABBYY, exclusive rights are reserved.
//
// DEVELOPER is allowed to incorporate SAMPLES into his own APPLICATION and modify it under
// the  terms of  License Agreement between  ABBYY and DEVELOPER.

// ABBYY FineReader Engine 12 Sample

// Simple exception class

#ifndef AbbyyException_h
#define AbbyyException_h

#include "BstrWrap.h"
#include "SafePtr.h"

class CAbbyyException {
public:
	// Constructor
	CAbbyyException( const wchar_t* _description ) { description = _description ; }
	CAbbyyException( const CAbbyyException& other ) : description( other.description ) {}

	// Returns error information.
	const wchar_t* Description() { return description; }

	// Copy operator
	CAbbyyException& operator = ( const CAbbyyException& other ) { description = other.description; return *this; } 

private:
	CBstr description;
};

// Check HRESULT. If not succeed try to obtain error description and throw exception
inline void CheckResult( HRESULT res )
{
	if( FAILED( res ) ) {
		// Try to obtain error info
		CSafePtr<IErrorInfo> errorInfo;
		if( ::GetFREngineErrorInfo( 0, &errorInfo ) == S_OK ) {
			// Get error description and throw exception
			CBstr description;
			errorInfo->GetDescription( description.GetBuffer() );
			throw CAbbyyException( description );
		} else {
			// Throw exception without description
			throw CAbbyyException( L"Unknown error" );
		}
	}
}

// Check HRESULT. If not succeed try to obtain error description and throw exception
inline void Check( bool condition, const wchar_t* message )
{
	if( ! condition ) {
		throw CAbbyyException( message );

	}
}

#endif // AbbyyException_h

