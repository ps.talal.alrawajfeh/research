import math
import mimetypes
import os
import random
import keras.backend as K
from alt_model_checkpoint.keras import AltModelCheckpoint
from keras import Input, Model
from keras.engine.saving import load_model
from keras.layers import Dense, BatchNormalization, PReLU, Dropout, Lambda, concatenate
from keras.optimizers import SGD, Adam
import numpy as np
from keras.utils import Sequence
import pickle
from tqdm import tqdm
import tensorflow as tf

TRAIN_DATA_PERCENTAGE = 0.7
VALIDATION_DATA_PERCENTAGE = 0.1
TEST_DATA_PERCENTAGE = 0.2

TRAIN_DATA_COUNT = 8000000
VALIDATION_DATA_COUNT = int(TRAIN_DATA_COUNT / TRAIN_DATA_PERCENTAGE * VALIDATION_DATA_PERCENTAGE)
TEST_DATA_COUNT = int(TRAIN_DATA_COUNT / TRAIN_DATA_PERCENTAGE * TEST_DATA_PERCENTAGE)

IMAGES_DIR = "C:\\fadi\\Final\\Assemble"
FEATURES_FILE = "C:\\fadi\\Final\\Assemble.tsv"
FEATURES_DIR = "C:\\Users\\user\\Desktop\\Talal\\detection-similarity-one-shot\\feature_vectors"
EMBEDDINGS_DIR = "C:\\Users\\user\\Desktop\\Talal\\detection-similarity-one-shot\\embeddings"

EMBEDDING_SIZE = 1024
BATCH_SIZE = 128
EPOCHS = 20
ALPHA = 0.4
MODEL_FILE_NAME = 'triplet_siamese'


def get_extensions_for_type(general_type):
    for ext in mimetypes.types_map:
        if mimetypes.types_map[ext].split('/')[0] == general_type:
            yield ext.lower()


def get_image_extensions():
    return tuple(get_extensions_for_type('image'))


def file_extension(path):
    return os.path.splitext(os.path.basename(path))[1].lower()


def is_image(path):
    return os.path.isfile(path) and file_extension(path) in get_image_extensions()


def load_images(dir_path):
    return list(filter(lambda f: is_image(os.path.join(dir_path, f)),
                       os.listdir(dir_path)))


def extract_classes(array, class_lambda=lambda f: f[0:12]):
    classes = dict()
    for item in array:
        c = class_lambda(item)
        if c not in classes:
            classes[c] = []
        classes[c].append(item)
    return classes


def extract_feature_vectors(features_file):
    image_features = dict()
    with open(features_file, 'r') as f:
        line = f.readline()
        while line != '':
            image, features = line.split('\t')
            image_features[image.strip()] = [float(x) for x in features.split(',')]
            line = f.readline()
    return image_features


def train_validation_test_split(classes):
    keys = [*classes]

    train_keys = random.sample(keys, int(len(classes) * TRAIN_DATA_PERCENTAGE))
    train_classes = dict()

    for key in train_keys:
        train_classes[key] = classes[key]
        keys.remove(key)

    validation_keys = random.sample(keys, int(len(classes) * VALIDATION_DATA_PERCENTAGE))
    validation_classes = dict()

    for key in validation_keys:
        validation_classes[key] = classes[key]
        keys.remove(key)

    test_classes = dict()
    for key in keys:
        test_classes[key] = classes[key]

    return train_classes, validation_classes, test_classes


def generate_triplets(classes, count, transformer_lambda):
    keys = [*classes]
    number_of_classes = len(keys)

    anchors = []
    positives = []
    negatives = []

    while count > 0:
        i = random.randint(0, number_of_classes - 1)
        j = (i + random.randint(1, number_of_classes - 1)) % number_of_classes

        anchor = random.choice(classes[keys[i]])
        positive = random.choice(classes[keys[i]])
        negative = random.choice(classes[keys[j]])

        anchors.append(transformer_lambda(anchor))
        positives.append(transformer_lambda(positive))
        negatives.append(transformer_lambda(negative))

        count -= 1

    return anchors, positives, negatives


# def triplet_loss(_, y_pred):
#     total_length = y_pred.shape.as_list()[-1]
#
#     anchor = y_pred[:, 0:int(total_length * 1 / 3)]
#     positive = y_pred[:, int(total_length * 1 / 3):int(total_length * 2 / 3)]
#     negative = y_pred[:, int(total_length * 2 / 3):int(total_length * 3 / 3)]
#
#     positive_dist = K.sum(K.square(anchor - positive), axis=-1)
#     negative_dist = K.sum(K.square(anchor - negative), axis=-1)
#     tertiary_dist = K.sum(K.square(positive - negative))
#
#     return K.mean(K.maximum(K.constant(0), positive_dist - 0.5 * negative_dist + tertiary_dist + K.constant(1)))


def triplet_loss(_, y_pred, alpha=ALPHA):
    total_length = y_pred.shape.as_list()[-1]

    anchor = y_pred[:, 0:int(total_length * 1 / 3)]
    positive = y_pred[:, int(total_length * 1 / 3):int(total_length * 2 / 3)]
    negative = y_pred[:, int(total_length * 2 / 3):int(total_length * 3 / 3)]

    positive_dist = K.sum(K.square(anchor - positive), axis=-1)
    negative_dist = K.sum(K.square(anchor - negative), axis=-1)
    # tertiary_dist = K.sum(K.square(positive - negative))

    return K.mean(K.maximum(K.constant(0), positive_dist - negative_dist + K.constant(alpha)))


def accuracy(_, y_pred):
    total_length = y_pred.shape.as_list()[-1]

    anchor = y_pred[:, 0:int(total_length * 1 / 3)]
    positive = y_pred[:, int(total_length * 1 / 3):int(total_length * 2 / 3)]
    negative = y_pred[:, int(total_length * 2 / 3):int(total_length * 3 / 3)]

    positive_dist = K.sum(K.square(anchor - positive), axis=-1)
    negative_dist = K.sum(K.square(anchor - negative), axis=-1)

    return K.mean(positive_dist + ALPHA < negative_dist)


def base_network(features_shape):
    features = Input(features_shape)

    embedding = BatchNormalization()(features)

    embedding = Dense(EMBEDDING_SIZE)(embedding)
    embedding = BatchNormalization()(embedding)
    embedding = Dropout(0.5)(embedding)
    embedding = PReLU()(embedding)

    embedding = Dense(EMBEDDING_SIZE)(embedding)
    embedding = PReLU()(embedding)
    # embedding = Lambda(lambda x: K.l2_normalize(x, axis=-1))(embedding)

    return Model(inputs=features, outputs=embedding)


class DataGenerator(Sequence):
    def __init__(self, classes, batch_size, count):
        self.classes = classes
        self.batch_size = batch_size
        self.count = count

    def __len__(self):
        return math.ceil(self.count / self.batch_size)

    def __getitem__(self, index):
        anchors, positives, negatives = generate_triplets(self.classes,
                                                          self.batch_size,
                                                          lambda x: load_cached(os.path.join(FEATURES_DIR, x)))

        return [np.array(anchors),
                np.array(positives),
                np.array(negatives)], np.zeros(shape=(self.batch_size, EMBEDDING_SIZE))


def build_model(features_shape):
    anchor_features = Input(features_shape)
    positive_features = Input(features_shape)
    negative_features = Input(features_shape)

    embedding_network = base_network(features_shape)

    anchor_embedding = embedding_network(anchor_features)
    positive_embedding = embedding_network(positive_features)
    negative_embedding = embedding_network(negative_features)

    merged_vector = concatenate([anchor_embedding, positive_embedding, negative_embedding], axis=-1)
    return Model(inputs=[anchor_features, positive_features, negative_features], outputs=merged_vector), \
           embedding_network


def cache_object(obj, file):
    with open(file, 'wb') as f:
        pickle.dump(obj, f)


def load_cached(file):
    with open(file, 'rb') as f:
        return pickle.load(f)


def cache_sample_data(train_classes, validation_classes, test_classes, feature_vectors):
    train_keys = [*train_classes]
    validation_keys = [*validation_classes]
    test_keys = [*test_classes]

    train_keys = random.sample(train_keys, 700)
    validation_keys = random.sample(validation_keys, 100)
    test_keys = random.sample(test_keys, 200)

    train_save = dict()
    validation_save = dict()
    test_save = dict()
    features_save = dict()

    for key in train_keys:
        train_save[key] = train_classes[key]
        for f in train_save[key]:
            features_save[f] = feature_vectors[f]

    for key in validation_keys:
        validation_save[key] = validation_classes[key]
        for f in validation_save[key]:
            features_save[f] = feature_vectors[f]

    for key in test_keys:
        test_save[key] = test_classes[key]
        for f in test_save[key]:
            features_save[f] = feature_vectors[f]

    cache_object(train_save, 'train_classes.pickle')
    cache_object(validation_save, 'validation_classes.pickle')
    cache_object(test_save, 'test_classes.pickle')
    cache_object(features_save, 'feature_vectors.pickle')


def load_sample_data():
    train_classes = load_cached('train_classes.pickle')
    validation_classes = load_cached('validation_classes.pickle')
    test_classes = load_cached('test_classes.pickle')
    feature_vectors = load_cached('feature_vectors.pickle')

    return train_classes, validation_classes, test_classes, feature_vectors


def save_features_vectors_as_files(feature_vectors):
    keys = [*feature_vectors]

    if not os.path.isdir(FEATURES_DIR):
        os.makedirs(FEATURES_DIR)

    progress = tqdm(total=len(feature_vectors))
    for key in keys:
        cache_object(feature_vectors[key], os.path.join(FEATURES_DIR, key))
        progress.update()
    progress.close()


def save_embeddings_as_files(model, batch_size=BATCH_SIZE):
    feature_files = load_images(FEATURES_DIR)

    if not os.path.isdir(EMBEDDINGS_DIR):
        os.makedirs(EMBEDDINGS_DIR)

    batches_count = math.ceil(len(feature_files) / batch_size)
    progress = tqdm(total=batches_count)

    for count in range(batches_count):
        batch = feature_files[count * batch_size: (count + 1) * batch_size]
        features = np.array([load_cached(os.path.join(FEATURES_DIR, f)) for f in batch])
        predictions = model.predict(features)
        for i in range(len(batch)):
            cache_object(predictions[i],
                         os.path.join(EMBEDDINGS_DIR, batch[i]))
        progress.update()
    progress.close()


def random_within_range_except(i, number):
    return (i + random.randint(1, number - 1)) % number


def distances(similar, different):
    result = []
    for s in range(len(similar)):
        for s2 in range(s + 1, len(similar)):
            tensor1 = np.array(similar[s])
            tensor2 = np.array(similar[s2])

            result.append([np.sum(np.square(tensor1 - tensor2)), 1])  # 1 means similar and 0 means different

    for s in range(len(similar)):
        for d in range(len(different)):
            tensor1 = np.array(similar[s])
            tensor2 = np.array(different[d])

            result.append([np.sum(np.square(tensor1 - tensor2)), 0])  # 1 means similar and 0 means different

    return result


def evaluate_on_threshold(threshold=0.5, batch_size=BATCH_SIZE):
    images = load_images(IMAGES_DIR)
    classes = extract_classes(images)

    _, _, test_classes = train_validation_test_split(classes)

    batches_count = math.ceil(TEST_DATA_COUNT / batch_size)

    keys = [*test_classes]

    genuine_acceptance_accuracy = 0
    genuine_count = 0
    forgery_rejection_accuracy = 0
    forgery_count = 0

    for count in range(batches_count):
        batch = keys[count * batch_size: (count + 1) * batch_size]
        for i in range(len(batch)):
            key = keys[i]
            similar = test_classes[key]
            different = [random.choice(classes[keys[random_within_range_except(i, batch_size)]]) for _ in range(len(similar))]

            similar = [load_cached(os.path.join(EMBEDDINGS_DIR, f)) for f in similar]
            different = [load_cached(os.path.join(EMBEDDINGS_DIR, f)) for f in different]

            result = distances(similar, different)

            for dist, match in result:
                if dist < threshold and match == 1:
                    genuine_acceptance_accuracy += 1
                if dist > threshold and match == 0:
                    forgery_rejection_accuracy += 1
                if match == 1:
                    genuine_count += 1
                if match == 0:
                    forgery_count += 1

    forgery_rejection_accuracy /= forgery_count
    genuine_acceptance_accuracy /= genuine_count
    return forgery_rejection_accuracy, genuine_acceptance_accuracy


def train():
    model, embedding_network = build_model(features_shape=(1920,))
    model.summary()

    optimizer = SGD(lr=0.0001, decay=0.0, momentum=0.9, nesterov=True)
    model.compile(loss=triplet_loss, optimizer=optimizer, metrics=[accuracy])

    images = load_images(IMAGES_DIR)
    classes = extract_classes(images)

    train_classes, validation_classes, test_classes = train_validation_test_split(classes)
    # train_classes, validation_classes, test_classes, feature_vectors = load_sample_data()
    # feature_vectors = extract_feature_vectors(FEATURES_FILE)

    train_data_generator = DataGenerator(train_classes, BATCH_SIZE, TRAIN_DATA_COUNT)
    validation_data_generator = DataGenerator(validation_classes, BATCH_SIZE, VALIDATION_DATA_COUNT)

    model.fit_generator(generator=train_data_generator,
                        steps_per_epoch=math.ceil(TRAIN_DATA_COUNT / BATCH_SIZE),
                        validation_data=validation_data_generator,
                        validation_steps=math.ceil(VALIDATION_DATA_COUNT / BATCH_SIZE),
                        epochs=EPOCHS,
                        use_multiprocessing=True,
                        workers=4,
                        max_queue_size=1024,
                        callbacks=[AltModelCheckpoint(MODEL_FILE_NAME + '_checkpoint_{epoch:02d}.h5',
                                                      embedding_network)])

    model.save(MODEL_FILE_NAME + '.h5')

    test_data_generator = DataGenerator(test_classes, BATCH_SIZE, TRAIN_DATA_COUNT)

    print("\n\nevaluating model...")
    results = model.evaluate_generator(generator=test_data_generator,
                                       steps=math.ceil(TEST_DATA_COUNT / BATCH_SIZE),
                                       use_multiprocessing=True,
                                       workers=4,
                                       max_queue_size=1024)

    print('\nloss:', results, '\n')


if __name__ == '__main__':
    # train()
    loaded_model = load_model(MODEL_FILE_NAME + '.h5')
    # save_embeddings_as_files(loaded_model)
    fra, gaa = evaluate_on_threshold(1990)
    print("FRA:", fra)
    print("GAA:", gaa)
