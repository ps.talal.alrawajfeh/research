#!/usr/bin/python3.6
import math
import random

from keras import Input, Model
from keras.datasets import mnist
from keras.layers import Conv2D, BatchNormalization, Activation, Flatten, Dense, concatenate, MaxPooling2D, Dropout
import keras.backend as K
import numpy as np
from keras.losses import logcosh, binary_crossentropy, mean_absolute_error
from keras.optimizers import SGD, RMSprop
from keras.utils import plot_model, Sequence
from keras.engine.saving import load_model
from matplotlib import pyplot as plt

REFERENCE_IMAGE_WIDTH = 28
REFERENCE_IMAGE_HEIGHT = 28

IMAGE_WIDTH = REFERENCE_IMAGE_WIDTH * 3
IMAGE_HEIGHT = REFERENCE_IMAGE_HEIGHT * 3

TRAINING_DATA_COUNT = 100000
EPOCHS = 10
BATCH_SIZE = 64


def get_input_shape(width, height):
    if K.image_data_format() == 'channels_first':
        input_shape = (1, height, width)
    else:
        input_shape = (height, width, 1)
    return input_shape


def pre_process(image):
    return image.astype(np.float).reshape(get_input_shape(image.shape[1], image.shape[0])) / 255.0


def embedding_network(input_shape):
    input_layer = Input(shape=input_shape)

    x = Conv2D(16, (3, 3), padding='same',
               kernel_initializer='he_normal')(input_layer)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)

    x = Conv2D(16, (3, 3), padding='same',
               kernel_initializer='he_normal')(x)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)

    x = MaxPooling2D(pool_size=(2, 2))(x)
    x = Dropout(0.5)(x)

    x = Conv2D(32, (3, 3), padding='same',
               kernel_initializer='he_normal')(x)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)

    x = Conv2D(32, (3, 3), padding='same',
               kernel_initializer='he_normal')(x)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)

    x = MaxPooling2D(pool_size=(2, 2))(x)
    x = Dropout(0.5)(x)

    x = Conv2D(64, (3, 3), padding='same',
               kernel_initializer='he_normal')(x)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)

    x = Conv2D(64, (3, 3), padding='same',
               kernel_initializer='he_normal')(x)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)

    x = MaxPooling2D(pool_size=(2, 2))(x)
    x = Dropout(0.5)(x)

    x = Flatten()(x)

    embedding = Model(input_layer, x)

    embedding.summary()
    plot_model(embedding, 'embedding.png')

    return embedding


def build_model():
    image_shape = get_input_shape(IMAGE_WIDTH, IMAGE_HEIGHT)
    reference_image_shape = get_input_shape(REFERENCE_IMAGE_WIDTH, REFERENCE_IMAGE_HEIGHT)

    image = Input(shape=image_shape)
    reference_image = Input(shape=reference_image_shape)

    image_embedding = embedding_network(image_shape)(image)
    reference_embedding = embedding_network(reference_image_shape)(reference_image)

    concat = concatenate([image_embedding, reference_embedding], axis=1)

    location = Dense(128)(concat)
    location = BatchNormalization()(location)
    location = Activation('relu')(location)
    location = Dropout(0.5)(location)

    location = Dense(128)(location)
    location = BatchNormalization()(location)
    location = Activation('relu')(location)
    location = Dropout(0.5)(location)

    location = Dense(4)(location)
    location = Activation('sigmoid', name='location')(location)

    similarity = Dense(128)(concat)
    similarity = BatchNormalization()(similarity)
    similarity = Activation('relu')(similarity)
    similarity = Dropout(0.5)(similarity)

    similarity = Dense(128)(similarity)
    similarity = BatchNormalization()(similarity)
    similarity = Activation('relu')(similarity)
    similarity = Dropout(0.5)(similarity)

    similarity = Dense(1)(similarity)
    similarity = Activation('sigmoid', name='similarity')(similarity)

    model = Model([image, reference_image], [location, similarity])

    model.summary()
    plot_model(model, 'model.png')

    return model


def generate_image(source_image):
    image = np.zeros(shape=(IMAGE_HEIGHT, IMAGE_WIDTH))

    x = random.randint(0, image.shape[1] - source_image.shape[1])
    y = random.randint(0, image.shape[0] - source_image.shape[0])

    image[y:y + source_image.shape[0], x:x + source_image.shape[1]] = source_image

    return image, x, y


def find_minimal_bounding_box(image):
    image_height = image.shape[0]
    image_width = image.shape[1]

    y1 = 0
    while y1 < image_height and not np.any(image[y1]):
        y1 += 1
    if y1 == image_width:
        y1 = 0

    y2 = image_height - 1
    while y2 >= 0 and not np.any(image[y2]):
        y2 -= 1
    if y2 == 0:
        y2 = image_height - 1

    temp = image.T

    x1 = 0
    while x1 < image_width and not np.any(temp[x1]):
        x1 += 1
    if x1 == image_width:
        x1 = 0

    x2 = image_width - 1
    while x2 >= 0 and not np.any(temp[x2]):
        x2 -= 1
    if x2 == 0:
        x2 = image_width - 1

    return x1, y1, x2 + 1, y2 + 1


def generate_image_with_info(other, are_similar):
    image, x, y = generate_image(other)

    if are_similar:
        return image, \
               x / image.shape[1], \
               y / image.shape[0], \
               other.shape[1] / image.shape[1], \
               other.shape[0] / image.shape[0],
    else:
        return image, \
               0.0, \
               0.0, \
               0.0, \
               0.0,


def generate_similar_and_different_tuples(classes, transformer_lambda, count=BATCH_SIZE):
    keys = [*classes]
    number_of_classes = len(keys)

    left = []
    right = []
    location = []
    similarity = []

    count //= 2
    while count > 0:
        i = random.randint(0, number_of_classes - 1)
        j = (i + random.randint(1, number_of_classes - 1)) % number_of_classes
        count -= 1

        choice = random.choice(classes[keys[i]])
        right_choice_similar = random.choice(classes[keys[i]])
        right_choice_different = random.choice(classes[keys[j]])

        image, x, y, w, h = generate_image_with_info(right_choice_similar, True)

        left.append(transformer_lambda(image))
        right.append(transformer_lambda(choice))
        location.append([x, y, w, h])

        image, x, y, w, h = generate_image_with_info(right_choice_different, False)

        left.append(transformer_lambda(image))
        right.append(transformer_lambda(choice))
        location.append([x, y, w, h])

        similarity.extend([1.0, 0.0])

    return left, right, location, similarity


class DataGenerator(Sequence):
    def __init__(self, batch_size=BATCH_SIZE, max_samples=TRAINING_DATA_COUNT):
        self.classes = extract_mnist_classes()
        self.batch_size = batch_size
        self.max_samples = max_samples

    def __len__(self):
        return math.ceil(self.max_samples / self.batch_size)

    def __getitem__(self, index):
        left, right, location, similarity = generate_similar_and_different_tuples(self.classes,
                                                                                  lambda x: pre_process(x))
        return [np.array(left), np.array(right)], [np.array(location), np.array(similarity)]


def extract_mnist_classes():
    (x_train, y_train), _ = mnist.load_data()

    classes = dict()
    for i in range(len(x_train)):
        label = y_train[i]
        if label not in classes:
            classes[label] = []
        classes[label].append(x_train[i])

    return classes


def similarity_accuracy(y_true, y_pred):
    return K.mean(K.equal(y_true, K.cast(y_pred > 0.5, y_true.dtype)))


def location_accuracy(y_true, y_pred):
    inter_x1 = K.max([y_true[:, 0], y_pred[:, 0]], axis=0)
    inter_y1 = K.max([y_true[:, 1], y_pred[:, 1]], axis=0)
    inter_x2 = K.min([y_true[:, 0] + y_true[:, 2], y_pred[:, 0] + y_pred[:, 2]], axis=0)
    inter_y2 = K.min([y_true[:, 1] + y_true[:, 3], y_pred[:, 1] + y_pred[:, 3]], axis=0)

    union_x1 = K.min([y_true[:, 0], y_pred[:, 0]], axis=0)
    union_y1 = K.min([y_true[:, 1], y_pred[:, 1]], axis=0)
    union_x2 = K.max([y_true[:, 0] + y_true[:, 2], y_pred[:, 0] + y_pred[:, 2]], axis=0)
    union_y2 = K.max([y_true[:, 1] + y_true[:, 3], y_pred[:, 1] + y_pred[:, 3]], axis=0)

    inter_area = (inter_x2 - inter_x1) * (inter_y2 - inter_y1)
    union_area = (union_x2 - union_x1) * (union_y2 - union_y1)

    return K.mean((inter_area + K.epsilon()) / (union_area + K.epsilon()))


def retrieve_image(image):
    if K.image_data_format() == 'channels_first':
        return np.reshape(image * 255, (image.shape[1], image.shape[2]))
    else:
        return np.reshape(image * 255, (image.shape[0], image.shape[1]))


def main():
    model = build_model()

    train_data_generator = DataGenerator()

    validation_data_count = int(TRAINING_DATA_COUNT * 0.2)
    validation_data_generator = DataGenerator(max_samples=validation_data_count)

    optimizer = RMSprop()  # SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)

    model.compile(loss=binary_crossentropy,
                  # loss_weights={'location': 1.0, 'similarity': 1.0},
                  optimizer=optimizer,
                  metrics={'location': location_accuracy, 'similarity': similarity_accuracy})

    model.fit_generator(generator=train_data_generator,
                        steps_per_epoch=math.ceil(TRAINING_DATA_COUNT / BATCH_SIZE),
                        validation_data=validation_data_generator,
                        validation_steps=math.ceil(validation_data_count / BATCH_SIZE),
                        epochs=EPOCHS,
                        use_multiprocessing=True,
                        workers=4,
                        max_queue_size=4)

    model.save('model.h5')

    # model = load_model('model.h5', custom_objects={'similarity_accuracy': similarity_accuracy})
    # model.compile(loss=logcosh,
    #               optimizer=optimizer,
    #               metrics=[similarity_accuracy])

    view_example(model)


def format_number(number):
    return "%.2f" % round(number, 2)


def view_example(model):
    left, right, _, _ = generate_similar_and_different_tuples(extract_mnist_classes(),
                                                              lambda x: pre_process(x), count=4)
    locations, similarities = model.predict([np.array(left), np.array(right)])
    for i in range(len(left) // 2):
        x1, y1, w1, h1 = locations[2 * i]
        similarity1 = similarities[2 * i]

        x2, y2, w2, h2 = locations[2 * i + 1]
        similarity2 = similarities[2 * i + 1]

        image1 = retrieve_image(left[2 * i])
        reference1 = retrieve_image(right[2 * i])

        image2 = retrieve_image(left[2 * i + 1])
        reference2 = retrieve_image(right[2 * i + 1])

        x1 *= image1.shape[1]
        y1 *= image1.shape[0]
        w1 *= image1.shape[1]
        h1 *= image1.shape[0]

        x2 *= image2.shape[1]
        y2 *= image2.shape[0]
        w2 *= image2.shape[1]
        h2 *= image2.shape[0]

        text1 = 'x: ' + format_number(x1) + ', y: ' + format_number(y1) + \
                ', width: ' + format_number(w1) + \
                ', height: ' + format_number(h1) + \
                ', matching percentage: ' + format_number(similarity1[0] * 100) + '%'

        text2 = 'x: ' + format_number(x2) + ', y: ' + format_number(y2) + \
                ', width: ' + format_number(w2) + \
                ', height: ' + format_number(h2) + \
                ', matching percentage: ' + format_number(similarity2[0] * 100) + '%'

        fig, ax = plt.subplots(nrows=2, ncols=2)

        ax[0, 0].imshow(image1, plt.cm.gray)
        ax[0, 1].imshow(reference1, plt.cm.gray)
        ax[0, 0].set_title(text1)

        ax[1, 0].imshow(image2, plt.cm.gray)
        ax[1, 1].imshow(reference2, plt.cm.gray)
        ax[1, 0].set_title(text2)

        plt.show()


if __name__ == '__main__':
    main()
