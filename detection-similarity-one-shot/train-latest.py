import math
import random

import cv2
import keras.backend as K
import numpy as np
from keras import Model, Input
from keras.applications import DenseNet121
from keras.applications.densenet import preprocess_input
from keras.datasets import mnist
from keras.layers import Flatten, Dense, BatchNormalization, PReLU, Dropout, concatenate, Activation, Lambda
from keras.losses import binary_crossentropy
from keras.optimizers import SGD
from keras.utils import Sequence
from matplotlib import pyplot as plt
from keras.metrics import binary_accuracy

REFERENCE_IMAGE_SHAPE = (28, 28)
LEFT_INPUT_SHAPE = (128, 128)
RIGHT_INPUT_SHAPE = (128, 128)


def resize_image(image, shape):
    return cv2.resize(image, shape, cv2.INTER_AREA)


def view_image(image):
    plt.imshow(image, plt.cm.gray)
    plt.show()


# image1 and image2 should be inverted images, i.e. 0 for white and 255 for black
def generate_reference_image(image1, image2):
    image = np.zeros(RIGHT_INPUT_SHAPE)

    input_width = RIGHT_INPUT_SHAPE[1]
    input_height = RIGHT_INPUT_SHAPE[0]

    center_x = input_width // 2
    vertical_line_x = random.randint(center_x - 4, center_x + 4)
    cv2.line(image, (vertical_line_x, 0), (vertical_line_x, input_height), 255)

    reference_width = REFERENCE_IMAGE_SHAPE[1]
    reference_height = REFERENCE_IMAGE_SHAPE[0]

    image1_x = random.randint(0, vertical_line_x - reference_width)
    image1_y = random.randint(0, input_height - reference_height)

    image[image1_y: image1_y + reference_height, image1_x: image1_x + reference_width] = image1

    image2_x = random.randint(vertical_line_x + 1, input_width - reference_width)
    image2_y = random.randint(0, input_height - reference_height)

    image[image2_y: image2_y + reference_height, image2_x: image2_x + reference_width] = image2

    return image


def generate_corresponding_image(image):
    x = random.randint(0, LEFT_INPUT_SHAPE[1] - image.shape[1])
    y = random.randint(0, LEFT_INPUT_SHAPE[0] - image.shape[0])

    corresponding = np.zeros(LEFT_INPUT_SHAPE)
    corresponding[y:y + image.shape[0], x:x + image.shape[1]] = image

    return corresponding, \
           x / LEFT_INPUT_SHAPE[1], \
           y / LEFT_INPUT_SHAPE[0], \
           image.shape[1] / LEFT_INPUT_SHAPE[1], \
           image.shape[0] / LEFT_INPUT_SHAPE[0]


def extract_classes(images, labels):
    classes = dict()

    for i in range(len(images)):
        image = images[i]
        label = labels[i]

        if label not in classes:
            classes[label] = []
        classes[label].append(image)

    return classes


def generate_left_right_data(classes,
                             left_transformer_lambda,
                             right_transformer_lambda,
                             count):
    keys = [*classes]
    number_of_classes = len(keys)

    left = []
    right = []
    location_similarity = []

    count //= 2
    while count > 0:
        i = random.randint(0, number_of_classes - 1)
        j = (i + random.randint(1, number_of_classes - 1)) % number_of_classes
        count -= 1

        choice1 = random.choice(classes[keys[i]])
        choice2 = random.choice(classes[keys[i]])
        reference = generate_reference_image(choice1, choice2)

        right.append(right_transformer_lambda(reference))
        similar = random.choice(classes[keys[i]])
        corresponding1, x, y, w, h = generate_corresponding_image(similar)
        left.append(left_transformer_lambda(corresponding1))
        location_similarity.append([x, y, w, h, 1.0])

        right.append(right_transformer_lambda(reference))
        different = random.choice(classes[keys[j]])
        corresponding2, _, _, _, _ = generate_corresponding_image(different)
        left.append(left_transformer_lambda(corresponding2))
        location_similarity.append([0.0, 0.0, 0.0, 0.0, 0.0])

    return left, right, location_similarity


def conv_input_shape(input_shape):
    if K.image_data_format() == 'channels_first':
        return 3, input_shape[0], input_shape[1]
    else:
        return input_shape[0], input_shape[1], 3


def location_accuracy(y_true, y_pred):
    inter_x1 = K.max([y_true[:, 0], y_pred[:, 0]], axis=0)
    inter_y1 = K.max([y_true[:, 1], y_pred[:, 1]], axis=0)
    inter_x2 = K.min([y_true[:, 0] + y_true[:, 2], y_pred[:, 0] + y_pred[:, 2]], axis=0)
    inter_y2 = K.min([y_true[:, 1] + y_true[:, 3], y_pred[:, 1] + y_pred[:, 3]], axis=0)

    union_x1 = K.min([y_true[:, 0], y_pred[:, 0]], axis=0)
    union_y1 = K.min([y_true[:, 1], y_pred[:, 1]], axis=0)
    union_x2 = K.max([y_true[:, 0] + y_true[:, 2], y_pred[:, 0] + y_pred[:, 2]], axis=0)
    union_y2 = K.max([y_true[:, 1] + y_true[:, 3], y_pred[:, 1] + y_pred[:, 3]], axis=0)

    inter_area = (inter_x2 - inter_x1) * (inter_y2 - inter_y1)
    union_area = (union_x2 - union_x1) * (union_y2 - union_y1)

    return K.mean((inter_area + K.epsilon()) / (union_area + K.epsilon()))


def similarity_accuracy(y_true, y_pred):
    return K.mean(K.equal(y_true[:, 4:5], K.round(y_pred[:, 4:5])), axis=-1)


def location_loss(y_true, y_pred):
    return 1 - location_accuracy(y_true, y_pred)


def overall_loss(y_true, y_pred):
    return 0.5 * (location_loss(y_true[:, 0:4], y_pred[:, 0:4]) +
                  binary_crossentropy(y_true[:, 4:5], y_pred[:, 4:5]))


def location_similarity_activation(x):
    return K.concatenate([K.relu(x[:, 0:4]), (K.softsign(x[:, 4:5]) + 1) / 2], axis=-1)


def expand_rgb(image):
    result = np.expand_dims(image, -1)
    return np.concatenate((result, result, result), axis=-1)


def pre_process(image, input_shape):
    result = expand_rgb(image.astype(np.float32)).reshape(conv_input_shape(input_shape))
    return preprocess_input(result)


class DataGenerator(Sequence):
    def __init__(self, classes, batch_size, count):
        self.classes = classes
        self.batch_size = batch_size
        self.count = count

    def __len__(self):
        return math.ceil(self.count / self.batch_size)

    def __getitem__(self, index):
        left, right, location_similarity = generate_left_right_data(self.classes,
                                                                    lambda x: pre_process(x, LEFT_INPUT_SHAPE),
                                                                    lambda x: pre_process(x, RIGHT_INPUT_SHAPE),
                                                                    self.batch_size)

        return [np.array(left), np.array(right)], np.array(location_similarity, np.float32)


def build_conv_model(input_shape, pretrained_model, name_prefix):
    input_layer = Input(conv_input_shape(input_shape))

    embedding = Flatten(name=name_prefix + '_embedding_flatten')(pretrained_model(input_layer))

    return input_layer, embedding


def build_model():
    dense_net = DenseNet121(include_top=False, weights='imagenet')

    for layer in dense_net.layers:
        layer.trainable = False

    left_input, left_embedding = build_conv_model(LEFT_INPUT_SHAPE, dense_net, 'left')
    right_input, right_embedding = build_conv_model(RIGHT_INPUT_SHAPE, dense_net, 'right')

    concatenation = concatenate([left_embedding, right_embedding])

    location = Dense(1024, name='location__dense1')(concatenation)
    location = BatchNormalization(name='location__batch_norm1')(location)
    location = PReLU(name='location__prelu1')(location)
    location = Dropout(0.5, name='location__dropout1')(location)

    location = Dense(1024, name='location__dense2')(location)
    location = BatchNormalization(name='location__batch_norm2')(location)
    location = PReLU(name='location__prelu2')(location)
    location = Dropout(0.5, name='location__dropout2')(location)

    location = Dense(4, name='location__dense3')(location)
    location = Activation('relu', name='location')(location)

    similarity = Lambda(lambda x: left_input[x[0]: x[0] + x[2], x[1]: x[1] + x[3], :])(location)
    similarity = concatenation([similarity, right_embedding])

    similarity = Dense(1024, name='similarity__dense1')(concatenation)
    similarity = BatchNormalization(name='similarity__batch_norm1')(similarity)
    similarity = PReLU(name='similarity__prelu1')(similarity)
    similarity = Dropout(0.5, name='similarity__dropout1')(similarity)

    similarity = Dense(1024, name='similarity__dense2')(similarity)
    similarity = BatchNormalization(name='similarity__batch_norm2')(similarity)
    similarity = PReLU(name='similarity__prelu2')(similarity)
    similarity = Dropout(0.5, name='similarity__dropout2')(similarity)

    similarity = Dense(1, name='similarity__dense3')(similarity)
    similarity = Activation('sigmoid', name='similarity')(similarity)

    return Model(input=[left_input, right_input], output=[location, similarity])


def train():
    model = build_model()
    model.summary()

    optimizer = SGD(lr=0.1, decay=1e-6, momentum=0.9, nesterov=True)
    model.compile(loss=overall_loss,
                  optimizer=optimizer,
                  metrics=[location_accuracy, similarity_accuracy])

    (train_x, train_y), (test_x, test_y) = mnist.load_data()
    train_classes = extract_classes(train_x, train_y)

    train_data_count = 100000
    batch_size = 64
    validation_data_percentage = 0.2

    train_data_generator = DataGenerator(train_classes, batch_size, train_data_count)
    validation_data_generator = DataGenerator(train_classes, batch_size,
                                              math.ceil(train_data_count * validation_data_percentage))
    model.fit_generator(generator=train_data_generator,
                        steps_per_epoch=math.ceil(train_data_count / batch_size),
                        validation_data=validation_data_generator,
                        validation_steps=math.ceil(train_data_count * validation_data_percentage / batch_size),
                        epochs=10,
                        use_multiprocessing=True,
                        workers=4,
                        max_queue_size=4)

    model.save('model.h5')

    test_classes = extract_classes(test_x, test_y)
    test_data_percentage = 0.5

    test_data_generator = DataGenerator(test_classes, batch_size, math.ceil(train_data_count * test_data_percentage))
    model.evaluate_generator(generator=test_data_generator,
                             steps=math.ceil(train_data_count * test_data_percentage / batch_size),
                             use_multiprocessing=True,
                             workers=4,
                             max_queue_size=4)


if __name__ == '__main__':
    train()
