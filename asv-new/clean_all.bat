rmdir /s /q data\output
rmdir /s /q data\generated
rmdir /s /q data\new_signatures

del *.h5
del *.pickle
del *.txt
