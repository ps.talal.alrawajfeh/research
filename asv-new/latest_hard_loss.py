#!/usr/bin/python3.6
import math
import os
import pickle
import random
import shutil
from enum import Enum

import numpy as np
from alt_model_checkpoint.keras import AltModelCheckpoint
from keras import Input, Model
from keras.callbacks import ModelCheckpoint, LearningRateScheduler, LambdaCallback
from keras.engine.saving import load_model
from keras.layers import BatchNormalization, Dense, Dropout, PReLU, Lambda, Concatenate
from keras.optimizers import SGD, Adam
from keras.regularizers import l2
from keras.utils import Sequence
import keras.backend as K
import tensorflow as tf
from scipy.interpolate import PchipInterpolator

# -----------------------------------------------------------------------------


FEATURE_FILES = ["C:\\fadi\\Final\\trainingSignaturesWithNoises.tsv",
                 "C:\\fadi\\Final\\trainingSignaturesWithNoisesCABHBTF.tsv"]

MODEL_FILE_NAME = 'features_embedding'
EVALUATION_REPORT_FILE = 'report.txt'

IMAGE_FEATURES_FILE = 'image_features.pickle'
IMAGE_CLASSES_FILE = 'image_classes.pickle'

TRAIN_DATA_FILE = 'train_data.pickle'
VALIDATION_DATA_FILE = 'validation_data.pickle'
TEST_DATA_FILE = 'test_data.pickle'

TRAIN_IMAGE_CLASSES_FILE = 'train_image_classes.pickle'
VALIDATION_IMAGE_CLASSES_FILE = 'validation_image_classes.pickle'
TEST_IMAGE_CLASSES_FILE = 'test_image_classes.pickle'

PIPELINE_STAGE_FILE = 'pipeline_stage.txt'

# -----------------------------------------------------------------------------

TRAIN_SPLIT_PERCENTAGE = 0.7
VALIDATION_SPLIT_PERCENTAGE = 0.15
TEST_SPLIT_PERCENTAGE = 1 - (TRAIN_SPLIT_PERCENTAGE + VALIDATION_SPLIT_PERCENTAGE)
DENSE_NET_FEATURES_LENGTH = 1920

# -------------------------- model hyper-parameters --------------------------

TRAIN_BATCHES = 50000
TRAIN_CLASSES_PER_BATCH = 30
TRAIN_IMAGES_PER_CLASS = 15

EVALUATION_DATA_PERCENTAGE = 0.3
EVALUATION_BATCHES = int(TRAIN_BATCHES /
                         (1 - EVALUATION_DATA_PERCENTAGE) *
                         EVALUATION_DATA_PERCENTAGE)
EVALUATION_CLASSES_PER_BATCH = TRAIN_CLASSES_PER_BATCH
EVALUATION_IMAGES_PER_CLASS = TRAIN_IMAGES_PER_CLASS

# the batch size is 1050
BATCH_SIZE = TRAIN_CLASSES_PER_BATCH * TRAIN_IMAGES_PER_CLASS
MODEL_INPUT_SHAPE = (DENSE_NET_FEATURES_LENGTH + 1,)

NUMBER_OF_HIDDEN_LAYERS = 2
HIDDEN_LAYER_SIZE = 1024
EMBEDDING_LAYER_SIZE = 512
DROPOUT_RATE = 0.5
L2_REGULARIZATION = 0.0
MARGIN = 0.3

EPOCHS = 6

INITIAL_LEARNING_RATE = 0.1
DECAY_RATE = 0.5
LEARNING_RATE_EPSILON = 1e-2


def calculate_learning_rates(epoch_learning_rate_mappings):
    epochs = [int(e) for e in epoch_learning_rate_mappings]
    learning_rates = [epoch_learning_rate_mappings[e] for e in epoch_learning_rate_mappings]
    interpolator = PchipInterpolator(np.array(epochs), np.array(learning_rates))

    x = np.arange(min(epochs), max(epochs) + 1)
    y = interpolator(x)

    epoch_learning_rate_map = dict()
    for i in range(len(x)):
        epoch_learning_rate_map[x[i]] = y[i]

    return epoch_learning_rate_map


LEARNING_RATE_DELTA = INITIAL_LEARNING_RATE - LEARNING_RATE_EPSILON
EPOCH_LEARNING_RATE_MAP = calculate_learning_rates({
    0: INITIAL_LEARNING_RATE,
    1: LEARNING_RATE_DELTA * DECAY_RATE,
    2: INITIAL_LEARNING_RATE * DECAY_RATE ** 2,
    3: LEARNING_RATE_DELTA * DECAY_RATE ** 3,
    4: INITIAL_LEARNING_RATE * DECAY_RATE ** 4,
    5: LEARNING_RATE_DELTA * DECAY_RATE ** 5,
    6: INITIAL_LEARNING_RATE * DECAY_RATE ** 6
})


# -----------------------------------------------------------------------------


class TensorType(Enum):
    TF_TENSOR = 1
    NP_TENSOR = 2


def euclidean_distance(x, y, tensor_type=TensorType.TF_TENSOR):
    if tensor_type == TensorType.TF_TENSOR:
        return K.sum(K.square(x - y), axis=-1)
    elif tensor_type == TensorType.NP_TENSOR:
        return np.sum(np.square(x - y))
    raise Exception(f'not implemented for tensor_type={tensor_type}')


def array_hashcode(array):
    hashcode = 0
    for i in array:
        hashcode += 31 * i
    return hashcode


# serialize python object into a file
def cache_object(obj, file):
    with open(file, 'wb') as f:
        pickle.dump(obj, f)


# deserialize python object from a file
def load_cached(file):
    with open(file, 'rb') as f:
        return pickle.load(f)


def parse_feature_files(feature_files):
    image_features_dict = dict()
    for features_file in feature_files:
        with open(features_file, 'r') as f:
            line = f.readline().strip()
            while line != '':
                image, features = line.split('\t')
                image_features_dict[image.strip()] = [float(x) for x in features.split(',')]
                line = f.readline()
    return image_features_dict


# returns a dictionary of the classes of items in `array` according to `class_lambda`
def extract_classes(array, class_lambda=lambda f: f[0:9]):
    classes = dict()
    for item in array:
        c = class_lambda(item)
        if c not in classes:
            classes[c] = []
        classes[c].append(item)
    return classes


def extract_feature_classes(image_features, image_classes):
    class_features_dicts = dict()
    for c in image_classes:
        images = image_classes[c]
        class_features_dicts[c] = [image_features[image] for image in images]
    return class_features_dicts


# append lines to the report
def append_lines_to_report(lines):
    with open(EVALUATION_REPORT_FILE, 'a') as f:
        f.writelines(lines)


class MinimumRecurrenceRateRandomSamplerWithReplacement:
    def __init__(self, array) -> None:
        self.array = array
        self.left_choices = array.copy()

    def next(self):
        if len(self.left_choices) == 0:
            self.left_choices = self.array.copy()
        i = random.randint(0, len(self.left_choices) - 1)
        return self.left_choices.pop(i)


def generate_batch_data(image_classes,
                        number_of_batches=TRAIN_BATCHES,
                        number_of_classes_per_batch=TRAIN_CLASSES_PER_BATCH,
                        number_of_images_per_class=TRAIN_IMAGES_PER_CLASS):
    tuples_hash_map = dict()
    classes = [*image_classes]

    batches = []
    while len(batches) < number_of_batches:
        selected_classes = set()
        while len(selected_classes) < number_of_classes_per_batch:
            class_index = random.randint(0, len(classes) - 1)
            selected_classes.add(class_index)

        hashcode = array_hashcode(selected_classes)
        if hashcode not in tuples_hash_map:
            tuples_hash_map[hashcode] = [selected_classes]
        else:
            if selected_classes in tuples_hash_map[hashcode]:
                continue
            else:
                tuples_hash_map[hashcode].append(selected_classes)

        batch = []
        for class_index in selected_classes:
            c = classes[class_index]
            class_images_indices = list(range(len(image_classes[c])))
            sampler = MinimumRecurrenceRateRandomSamplerWithReplacement(class_images_indices)
            batch.append([c, [sampler.next() for _ in range(number_of_images_per_class)]])
        batches.append(batch)

    return batches


class DataGenerator(Sequence):
    def __init__(self, image_features, image_classes, batches, batch_size):
        self.batch_size = batch_size
        self.count = len(batches)
        self.image_features = image_features
        self.image_classes = image_classes
        self.batches = batches
        self.dummy_output = np.zeros(shape=(self.batch_size, EMBEDDING_LAYER_SIZE))

    def __len__(self):
        return self.count

    def __getitem__(self, index):
        batch = self.batches[index]
        inputs = []
        label = 0
        for group in batch:
            c, images_indices = group
            for i in images_indices:
                image = self.image_classes[c][i]
                inputs.append(self.image_features[image] + [label])
            label += 1

        return np.array(inputs, np.float32).reshape(BATCH_SIZE, MODEL_INPUT_SHAPE[0]), self.dummy_output


def calculate_squared_distances_matrix(embeddings):
    # from this we will obtain the dot-product of each pair of embeddings
    # say we have the dot product of X and Y denoted as <X, Y> then we can
    # calculate the euclidean distance ||X - Y|| using the well-known formula
    # ||X - Y||^2 = ||X||^2 - 2 <X, Y> + ||Y||^2
    # the first row of the matrix below contains the dot product of the first
    # embedding with all the other embeddings, the second row contains the
    # dot product of the second embedding with all the other embeddings, etc.
    dot_products = tf.matmul(embeddings, tf.transpose(embeddings))

    # here we obtain the squared l2 norms from the diagonal of the matrix above
    squared_norms = tf.linalg.diag_part(dot_products)

    # now we calculate ||X||^2 - 2<X, Y> + ||Y||^2
    # the norms are repeated row-wise and column-wise using different
    # shapes of squared_norms, one where the shape is (batch_size, 1)
    # and the other where the shape is (1, batch_size) this forces
    # broadcasting the tensors together where to achieve the shape
    # (batch_size, batch_size) one has to repeat the columns and the
    # other has to repeat the rows
    squared_distances = tf.expand_dims(squared_norms, -1) - 2 * dot_products + tf.expand_dims(squared_norms, 0)
    squared_distances = tf.maximum(squared_distances, 0.0)

    zeros_mask = tf.cast(squared_distances == 0, tf.float32)
    squared_distances += zeros_mask * 1e-16

    return tf.sqrt(squared_distances) * (1 - zeros_mask)


def get_anchor_positives_mask(labels, batch_size=BATCH_SIZE):
    positive_labels = tf.cast(labels == tf.transpose(labels), tf.float32)
    same_labels = tf.eye(batch_size)
    return positive_labels - same_labels


def get_anchor_negatives_mask(labels):
    return tf.cast(labels != tf.transpose(labels), tf.float32)


# y_pred is a tensor containing embeddings with their labels (class)
def batch_hard_triplet_loss(_, y_pred, batch_size=BATCH_SIZE):
    embeddings_length = y_pred.shape[1] - 1

    embeddings = y_pred[:, :embeddings_length]
    labels = y_pred[:, embeddings_length:]

    squared_distances = calculate_squared_distances_matrix(embeddings)

    anchor_positives_mask = get_anchor_positives_mask(labels, batch_size)
    anchor_negatives_mask = get_anchor_negatives_mask(labels)

    # get the matrix containing only all anchor-positive distances and zero elsewhere
    anchor_positive_distances = anchor_positives_mask * squared_distances

    # get the matrix containing only all anchor-negative distances and zero elsewhere
    anchor_negative_distances = anchor_negatives_mask * squared_distances

    # get the maximum distance from each row which represents the
    # hardest positive and since each row represents the distances for the same
    # anchor we only apply the maximum function on each row
    hardest_positive = tf.reduce_max(anchor_positive_distances, axis=-1, keepdims=True)

    # the same here except since we filled all the entries in the matrix except
    # the anchor-negative distances with zero, when we take the minimum
    # distance it will always be zero, so to fix this we replace the zeros with
    # the maximum distance which does not effect finding the minimum
    maximum_value = tf.reduce_max(anchor_negative_distances, axis=-1, keepdims=True)
    non_zero_negative_distances = anchor_negative_distances + (1.0 - anchor_negatives_mask) * maximum_value
    hardest_negative = tf.reduce_min(non_zero_negative_distances, axis=-1, keepdims=True)

    # calculate the triplet loss for each hardest_positive/hardest_negative
    # pair, remember that the original triplet loss was written as
    # loss(a, p, n) = d(a, p) - d(a, n) + margin
    # since the loss must be positive, all negative values are replaced with 0
    triplet_loss = tf.maximum(hardest_positive - hardest_negative + MARGIN, 0.0)

    return tf.reduce_mean(triplet_loss)


def accuracy(_, y_pred, batch_size=BATCH_SIZE):
    embeddings_length = y_pred.shape[1] - 1

    embeddings = y_pred[:, :embeddings_length]
    labels = y_pred[:, embeddings_length:]

    squared_distances = calculate_squared_distances_matrix(embeddings)

    anchor_negatives_mask = get_anchor_negatives_mask(labels)
    anchor_positives_mask = get_anchor_positives_mask(labels, batch_size)

    # through broadcasting, each column in the matrix is repeated by batch_size
    # which represents d(a, p) but is still not the anchor_positive distances
    # since it needs to corrected and then each row is repeated by batch_size
    # which represents d(a, n) and also needs correction
    anchor_positive_distances = tf.expand_dims(squared_distances, -1)
    anchor_negative_distances = tf.expand_dims(squared_distances, 1)

    all_triplets_loss = anchor_positive_distances - anchor_negative_distances + MARGIN

    # the correction processes is here since only the valid anchor positive
    # and anchor negative distances are used and all the others are zero
    anchor_positive_anchor_negative_mask = tf.expand_dims(anchor_positives_mask, -1) * \
                                           tf.expand_dims(anchor_negatives_mask, 1)

    valid_triplets_loss = all_triplets_loss * anchor_positive_anchor_negative_mask

    number_of_valid_triplets = tf.reduce_sum(anchor_positive_anchor_negative_mask)
    error_rate = tf.reduce_sum(tf.cast(valid_triplets_loss > 0, tf.float32)) / number_of_valid_triplets

    return 1.0 - error_rate


def features_embedding_model(features_shape):
    input_layer = Input(features_shape)
    embedding = BatchNormalization()(input_layer)

    for _ in range(NUMBER_OF_HIDDEN_LAYERS):
        embedding = Dense(HIDDEN_LAYER_SIZE,
                          kernel_initializer='he_normal',
                          kernel_regularizer=l2(L2_REGULARIZATION),
                          use_bias=False)(embedding)
        embedding = BatchNormalization()(embedding)
        embedding = Dropout(DROPOUT_RATE)(embedding)
        embedding = PReLU()(embedding)

    embedding = Dense(EMBEDDING_LAYER_SIZE,
                      kernel_initializer='he_normal',
                      kernel_regularizer=l2(L2_REGULARIZATION))(embedding)
    embedding = PReLU()(embedding)

    output_layer = Lambda(lambda x: K.l2_normalize(x, axis=-1))(embedding)

    return Model(inputs=input_layer, outputs=output_layer)


def wrap_features_embedding_model(features_embedding, features_shape):
    input_layer = Input(features_shape)

    labels = Lambda(lambda x: x[:, DENSE_NET_FEATURES_LENGTH:])(input_layer)
    features = Lambda(lambda x: x[:, :DENSE_NET_FEATURES_LENGTH])(input_layer)

    embedding = features_embedding(features)

    output_layer = Concatenate(axis=-1)([embedding, labels])
    return Model(inputs=input_layer, outputs=output_layer)


def class_train_validation_test_split(image_classes):
    keys = [*image_classes]

    keys_indices = list(range(len(keys)))
    random.shuffle(keys_indices)

    train_keys_indices = keys_indices[:int(len(image_classes) * TRAIN_SPLIT_PERCENTAGE)]
    train_image_classes = dict()
    for i in range(len(train_keys_indices)):
        key = keys[train_keys_indices[i]]
        train_image_classes[key] = image_classes[key]
    keys_indices = keys_indices[len(train_keys_indices):]

    validation_keys_indices = keys_indices[:int(len(image_classes) * VALIDATION_SPLIT_PERCENTAGE)]
    validation_image_classes = dict()
    for i in range(len(validation_keys_indices)):
        key = keys[validation_keys_indices[i]]
        validation_image_classes[key] = image_classes[key]
    keys_indices = keys_indices[len(validation_keys_indices):]

    test_image_classes = dict()
    for i in range(len(keys_indices)):
        key = keys[keys_indices[i]]
        test_image_classes[key] = image_classes[key]

    return train_image_classes, validation_image_classes, test_image_classes


class PipelineStage(Enum):
    PARSE_FEATURES_FILES = 1
    GENERATE_DATA = 2
    TRAIN_MODEL = 3
    EVALUATE_MODEL = 4
    DONE = 5


def get_pipeline_stage():
    if not os.path.isfile(PIPELINE_STAGE_FILE):
        return PipelineStage.PARSE_FEATURES_FILES
    with open(PIPELINE_STAGE_FILE, 'r') as f:
        return PipelineStage(int(f.read()))


def update_pipeline_stage(new_stage):
    with open(PIPELINE_STAGE_FILE, 'w') as f:
        f.write(str(new_stage.value))


def next_pipeline_stage(pipeline_stage):
    stage = PipelineStage(pipeline_stage.value + 1)
    update_pipeline_stage(stage)
    return stage


def extract_image_features(pipeline_stage):
    if pipeline_stage == PipelineStage.PARSE_FEATURES_FILES or \
            not os.path.isfile(IMAGE_FEATURES_FILE):
        image_features = parse_feature_files(FEATURE_FILES)
        cache_object(image_features, IMAGE_FEATURES_FILE)

        pipeline_stage = next_pipeline_stage(pipeline_stage)
    elif pipeline_stage.value > PipelineStage.PARSE_FEATURES_FILES.value:
        image_features = load_cached(IMAGE_FEATURES_FILE)
    else:
        image_features = None

    return image_features, pipeline_stage


def generate_train_validation_test_data(image_features, pipeline_stage):
    if pipeline_stage == PipelineStage.GENERATE_DATA or \
            not os.path.isfile(TRAIN_DATA_FILE) or \
            not os.path.isfile(VALIDATION_DATA_FILE) or \
            not os.path.isfile(TEST_DATA_FILE):
        image_classes = extract_classes(image_features.keys())
        cache_object(image_classes, IMAGE_CLASSES_FILE)

        train_image_classes, validation_image_classes, test_image_classes = \
            class_train_validation_test_split(image_classes)

        cache_object(train_image_classes, TRAIN_IMAGE_CLASSES_FILE)
        cache_object(validation_image_classes, VALIDATION_IMAGE_CLASSES_FILE)
        cache_object(test_image_classes, TEST_IMAGE_CLASSES_FILE)

        train_data = generate_batch_data(train_image_classes)
        validation_data = generate_batch_data(validation_image_classes,
                                              EVALUATION_BATCHES,
                                              EVALUATION_CLASSES_PER_BATCH,
                                              EVALUATION_IMAGES_PER_CLASS)
        test_data = generate_batch_data(test_image_classes,
                                        EVALUATION_BATCHES,
                                        EVALUATION_CLASSES_PER_BATCH,
                                        EVALUATION_IMAGES_PER_CLASS)

        cache_object(train_data, TRAIN_DATA_FILE)
        cache_object(validation_data, VALIDATION_DATA_FILE)
        cache_object(test_data, TEST_DATA_FILE)

        pipeline_stage = next_pipeline_stage(pipeline_stage)
    elif pipeline_stage.value > PipelineStage.GENERATE_DATA.value:
        train_image_classes = load_cached(TRAIN_IMAGE_CLASSES_FILE)
        validation_image_classes = load_cached(VALIDATION_IMAGE_CLASSES_FILE)
        test_image_classes = load_cached(TEST_IMAGE_CLASSES_FILE)

        train_data = load_cached(TRAIN_DATA_FILE)
        validation_data = load_cached(VALIDATION_DATA_FILE)
        test_data = load_cached(TEST_DATA_FILE)
    else:
        train_image_classes = None
        validation_image_classes = None
        test_image_classes = None

        train_data = None
        validation_data = None
        test_data = None

    return (train_image_classes, validation_image_classes, test_image_classes), \
           (train_data, validation_data, test_data), pipeline_stage


class ModelLoadingMode(Enum):
    MinimumMetricValue = 0
    MaximumMetricValue = 1


def rename_best_model(mode=ModelLoadingMode.MinimumMetricValue):
    all_models = list(filter(lambda x: x[-3:] == '.h5', os.listdir(os.curdir)))
    all_metrics = []

    for model in all_models:
        i1 = model.rfind('_')
        i2 = model.rfind('.')

        if i1 == -1 or i2 == -1:
            all_metrics.append(None)
            continue

        metric = model[i1 + 1: i2]

        try:
            all_metrics.append(float(metric))
        except ValueError:
            all_metrics.append(None)

    model_metric_pairs = [[all_models[i], all_metrics[i]] for i in range(len(all_models)) if all_metrics[i] is not None]
    if mode == ModelLoadingMode.MaximumMetricValue:
        best_index = np.array(model_metric_pairs)[:, 1:2].flatten().argmax()
    else:
        best_index = np.array(model_metric_pairs)[:, 1:2].flatten().argmin()

    shutil.copy(model_metric_pairs[best_index][0], MODEL_FILE_NAME + '.h5')


def configure_model_attributes(model):
    model.loss = None
    model._compile_metrics = None
    model._compile_weighted_metrics = None
    model.sample_weight_mode = None
    model.loss_weights = None


def train(image_features,
          train_image_classes,
          train_data,
          validation_image_classes,
          validation_data):
    features_embedding = features_embedding_model(features_shape=(DENSE_NET_FEATURES_LENGTH,))
    wrapped_model = wrap_features_embedding_model(features_embedding, MODEL_INPUT_SHAPE)
    configure_model_attributes(features_embedding)

    features_embedding.summary()
    wrapped_model.summary()

    wrapped_model.compile(loss=batch_hard_triplet_loss,
                          optimizer=SGD(lr=INITIAL_LEARNING_RATE,
                                        decay=0,
                                        momentum=0.9,
                                        nesterov=True),
                          metrics=[accuracy])

    train_data_generator = DataGenerator(image_features,
                                         train_image_classes,
                                         train_data,
                                         BATCH_SIZE)

    validation_data_generator = DataGenerator(image_features,
                                              validation_image_classes,
                                              validation_data,
                                              BATCH_SIZE)

    last_model_checkpoint = AltModelCheckpoint(MODEL_FILE_NAME + '_checkpoint_last.h5',
                                               features_embedding)
    best_model_checkpoint = AltModelCheckpoint(MODEL_FILE_NAME + '_checkpoint_{epoch:02d}_{val_loss:0.4f}.h5',
                                               features_embedding,
                                               monitor='val_loss',
                                               save_best_only=True,
                                               mode='min')

    learning_rate_scheduler = LearningRateScheduler(lambda e: EPOCH_LEARNING_RATE_MAP[e], verbose=0)

    append_lines_to_report('======= training results =======\n')

    update_report_callback = LambdaCallback(on_epoch_end=lambda epoch, logs: append_lines_to_report(
        f'epoch: {epoch} - loss: {logs["loss"]} - loss: {logs["accuracy"]} - validation loss: {logs["val_loss"]} - validation accuracy: {logs["val_accuracy"]}\n'
    ))

    wrapped_model.fit_generator(generator=train_data_generator,
                                steps_per_epoch=math.ceil(len(train_data)),
                                validation_data=validation_data_generator,
                                validation_steps=math.ceil(len(validation_data)),
                                epochs=EPOCHS,
                                use_multiprocessing=False,
                                callbacks=[last_model_checkpoint,
                                           best_model_checkpoint,
                                           learning_rate_scheduler,
                                           update_report_callback])

    rename_best_model()

    return features_embedding


def load_features_embedding_model():
    return load_model(MODEL_FILE_NAME + '.h5', compile=False)


def evaluate_model(features_embedding,
                   image_features,
                   test_image_classes,
                   test_data):
    wrapped_model = wrap_features_embedding_model(features_embedding, MODEL_INPUT_SHAPE)

    features_embedding.compile(loss=batch_hard_triplet_loss,
                               optimizer=SGD(),
                               metrics=[accuracy])

    test_data_generator = DataGenerator(image_features,
                                        test_image_classes,
                                        test_data,
                                        BATCH_SIZE)

    append_lines_to_report('======= evaluation results =======\n')

    results = wrapped_model.evaluate_generator(generator=test_data_generator,
                                               steps=math.ceil(len(test_data)),
                                               use_multiprocessing=False)

    append_lines_to_report(f'test loss: {results[0]} - test accuracy: {results[1]}\n\n')


def train_features_embedding_model(pipeline_stage,
                                   image_features,
                                   train_image_classes,
                                   train_data,
                                   validation_image_classes,
                                   validation_data):
    if pipeline_stage == PipelineStage.TRAIN_MODEL:
        features_embedding = train(image_features,
                                   train_image_classes,
                                   train_data,
                                   validation_image_classes,
                                   validation_data)

        pipeline_stage = next_pipeline_stage(pipeline_stage)
    elif pipeline_stage.value > PipelineStage.TRAIN_MODEL.value:
        features_embedding = load_features_embedding_model()
    else:
        features_embedding = None

    return features_embedding, pipeline_stage


def evaluate_features_embedding_model(pipeline_stage,
                                      features_embedding,
                                      image_features,
                                      test_image_classes,
                                      test_data):
    if pipeline_stage == PipelineStage.EVALUATE_MODEL:
        evaluate_model(features_embedding,
                       image_features,
                       test_image_classes,
                       test_data)

        pipeline_stage = next_pipeline_stage(pipeline_stage)

    return pipeline_stage


def run_pipeline():
    # -------------- load the last stage the pipeline stopped at -------------- #
    pipeline_stage = get_pipeline_stage()

    # ------------------ extract features from features files ------------------ #
    image_features, pipeline_stage = extract_image_features(pipeline_stage)

    # --------------- generate train, validation, and test data --------------- #
    (train_image_classes, validation_image_classes, test_image_classes), \
    (train_data, validation_data, test_data), pipeline_stage = generate_train_validation_test_data(image_features,
                                                                                                   pipeline_stage)

    # ------------------------------ train model ------------------------------ #
    features_embedding, pipeline_stage = train_features_embedding_model(pipeline_stage,
                                                                        image_features,
                                                                        train_image_classes,
                                                                        train_data,
                                                                        validation_image_classes,
                                                                        validation_data)

    # ----------------------------- evaluate model ----------------------------- #
    pipeline_stage = evaluate_features_embedding_model(pipeline_stage, features_embedding, image_features,
                                                       test_image_classes, test_data)

    if pipeline_stage == PipelineStage.DONE:
        print('done!')


if __name__ == '__main__':
    run_pipeline()

