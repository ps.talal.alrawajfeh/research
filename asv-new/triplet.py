import math
import os
import pickle
import random
import shutil
from collections import Sequence
from enum import Enum

import keras.backend as K
import numpy as np
import tensorflow as tf
from alt_model_checkpoint.keras import AltModelCheckpoint
from keras import Input, Model
from keras.engine.saving import load_model
from keras.layers import Dense, Lambda, PReLU, BatchNormalization, Dropout, concatenate
from keras.optimizers import SGD
from keras.regularizers import l2
from matplotlib import pyplot as plt
from numpy import random as np_random
from scipy import interpolate
from tensorflow.python.keras.callbacks import LambdaCallback, LearningRateScheduler
from tqdm import tqdm

# -----------------------------------------------------------------------------

DATA_PATH = '.\\data\\generated'

MODEL_FILE_NAME = 'embedding-net'
EMBEDDING_NET_FILE_NAME = 'embedding-net'
EVALUATION_REPORT_FILE = 'embedding-net-report.txt'

IMAGE_FEATURES_FILE = 'image_features.pickle'
IMAGE_CLASSES_FILE = 'image_classes.pickle'

TRAIN_TRIPLETS_FILE = 'train_triplets.pickle'
VALIDATION_TRIPLETS_FILE = 'validation_triplets.pickle'
TEST_TRIPLETS_FILE = 'test_triplets.pickle'

TRAIN_IMAGE_CLASSES_FILE = 'train_image_classes.pickle'
VALIDATION_IMAGE_CLASSES_FILE = 'validation_image_classes.pickle'
TEST_IMAGE_CLASSES_FILE = 'test_image_classes.pickle'

IMAGE_EMBEDDINGS_FILE = 'image_embeddings.pickle'
EMBEDDINGS_IMAGE_CLASSES_FILE = 'embeddings_image_classes.pickle'

DISTANCES_CACHE_FILE = 'distances.pickle'
GENUINE_ACCEPTANCE_ACCURACIES_CACHE_FILE = 'genuine_acceptance_accuracies.pickle'
FORGERY_REJECTION_ACCURACIES_CACHE_FILE = 'forgery_rejection_accuracies.pickle'

TRAIN_DATA_PERCENTAGE = 0.7
VALIDATION_DATA_PERCENTAGE = 0.15

FORGERIES_PER_CLASS = 10
FORGERY_CLASSES_PER_REFERENCE = 40
USE_ALL_ANCHORS = False
NUMBER_OF_ANCHORS = 10

DENSE_NET_FEATURES_SHAPE = (1920,)

NUMBER_OF_HIDDEN_LAYERS = 1
EMBEDDING_LAYER_SIZE = 1024
HIDDEN_LAYER_SIZE = 1024
DROPOUT_RATE = 0.5

MARGIN = 0.2
INITIAL_LEARNING_RATE = 0.1
TERMINAL_LEARNING_RATE = 0.001
TRAINING_BATCH_SIZE = 128
EVALUATION_BATCH_SIZE = 1024
EPOCHS = 10

LIMIT_DISTANCE = 2
DISTANCE_MULTIPLIER = 0.01


class TensorType(Enum):
    TF_TENSOR = 1
    NP_TENSOR = 2


def euclidean_distance(x, y, tensor_type=TensorType.TF_TENSOR):
    if tensor_type == TensorType.TF_TENSOR:
        return K.sum(K.square(x - y), axis=-1)
    elif tensor_type == TensorType.NP_TENSOR:
        return np.sqrt(np.sum(np.square(x - y)))
    raise Exception(f'not implemented for tensor_type={tensor_type}')


DISTANCE_FUNCTION = euclidean_distance


def exponential_decay(initial_lr=INITIAL_LEARNING_RATE, end_lr=TERMINAL_LEARNING_RATE, epochs=EPOCHS):
    x0 = 0
    y0 = initial_lr
    x1 = epochs - 1
    y1 = end_lr

    a = (math.log(y1) - math.log(y0)) / (x1 - x0)
    b = math.log(y1) - a * x1

    return lambda epoch: math.exp(a * epoch + b)


# ------------- fix the random seed to get reproducible results -------------- #

RANDOM_SEED = 666

random.seed(RANDOM_SEED)
np_random.seed(RANDOM_SEED)
tf.random.set_seed(RANDOM_SEED)


# ---------------------------------------------------------------------------- #


def embedding_network(features_shape=DENSE_NET_FEATURES_SHAPE):
    features = Input(features_shape)
    embedding = BatchNormalization(momentum=0.99, epsilon=1.001e-5)(features)

    for _ in range(NUMBER_OF_HIDDEN_LAYERS):
        embedding = Dense(HIDDEN_LAYER_SIZE,
                          kernel_initializer='he_normal',
                          kernel_regularizer=l2(1e-4),
                          use_bias=False)(embedding)
        embedding = BatchNormalization(momentum=0.99, epsilon=1.001e-5)(embedding)
        embedding = Dropout(DROPOUT_RATE)(embedding)
        embedding = PReLU()(embedding)

    embedding = Dense(EMBEDDING_LAYER_SIZE,
                      kernel_initializer='he_normal',
                      kernel_regularizer=l2(1e-4))(embedding)
    embedding = PReLU()(embedding)
    embedding = Lambda(lambda x: K.l2_normalize(x, axis=-1))(embedding)

    return Model(inputs=features, outputs=embedding)


def training_wrapped_model(features_shape=DENSE_NET_FEATURES_SHAPE,
                           embedding_net=embedding_network()):
    anchor = Input(features_shape)
    positive = Input(features_shape)
    negative = Input(features_shape)

    anchor_embedding = embedding_net(anchor)
    positive_embedding = embedding_net(positive)
    negative_embedding = embedding_net(negative)

    merged_vector = concatenate(
        [anchor_embedding,
         positive_embedding,
         negative_embedding], axis=-1)

    return Model(inputs=[anchor, positive, negative], outputs=merged_vector)


def custom_triplet_loss(_, y_pred, distance_fun=DISTANCE_FUNCTION):
    total_length = y_pred.shape.as_list()[-1]

    # the output vector contains all the embeddings concatenated
    anchor = y_pred[:, 0:int(total_length * 1 / 3)]
    positive = y_pred[:, int(total_length * 1 / 3):int(total_length * 2 / 3)]
    negative = y_pred[:, int(total_length * 2 / 3):int(total_length * 3 / 3)]

    # calculate distances according to the distance function
    positive_dist = distance_fun(anchor, positive)
    negative_dist = distance_fun(anchor, negative)

    # calculate each embedding loss; note that a loss should not be less than 0 (negative)
    losses = K.maximum(K.constant(0), positive_dist - negative_dist + K.constant(MARGIN))

    # obtain a matrix containing 1.0s in the entries where the anchor negative distance is less than the anchor
    # positive distance, i.e. the hard negatives; and containing 0.0s otherwise
    hard_distances_mask = K.cast(negative_dist < positive_dist, dtype='float32')

    # obtain the number of hard negatives by summing the hard_distances_mask matrix; note that the minimum count is 1
    # even if there is no hard negatives to mitigate division by zero
    hard_distances_count = K.maximum(K.constant(1.0), K.sum(hard_distances_mask))

    # use the mask to obtain the hard negatives losses only
    hard_losses = hard_distances_mask * losses

    # calculate the average loss of the hard negatives
    hard_average_loss = K.sum(hard_losses, -1) / hard_distances_count

    # similarly, obtain a matrix containing 1.0s in the entries where the anchor negative distance greater than the
    # anchor positive distance but within the additional margin, i.e. the semi-hard negatives; and containing 0.0s
    # otherwise
    semi_hard_distances_mask = K.cast(negative_dist >= positive_dist, dtype='float32') * \
                               K.cast(negative_dist < positive_dist + K.constant(MARGIN), dtype='float32')

    # obtain the number of semi-hard negatives
    semi_hard_distances_count = K.maximum(K.constant(1.0), K.sum(semi_hard_distances_mask))

    # use the mask to obtain the semi-hard negatives losses only
    semi_hard_losses = semi_hard_distances_mask * losses

    # calculate the average loss of the semi-hard negatives
    semi_hard_average_loss = K.sum(semi_hard_losses, -1) / semi_hard_distances_count

    # the total loss is the sum of the average hard negatives loss and the sum of the average semi-hard negatives loss
    # this gives an equal weight to both instead of calculating one average for both
    total_loss = 0.5 * hard_average_loss + 0.5 * semi_hard_average_loss

    return K.maximum(K.constant(0), total_loss)


def accuracy(_, y_pred, distance_fun=DISTANCE_FUNCTION):
    anchor = y_pred[:, 0:EMBEDDING_LAYER_SIZE]
    positive = y_pred[:, EMBEDDING_LAYER_SIZE: EMBEDDING_LAYER_SIZE * 2]
    negative = y_pred[:, EMBEDDING_LAYER_SIZE * 2: EMBEDDING_LAYER_SIZE * 3]

    positive_dist = distance_fun(anchor, positive)
    negative_dist = distance_fun(anchor, negative)

    return K.mean(positive_dist + MARGIN < negative_dist)


# -----------------------------------------------------------------------------


def load_image_features():
    images = os.listdir(DATA_PATH)
    image_features = dict()

    for image in images:
        image_features[image] = load_cached(os.path.join(DATA_PATH, image))

    return image_features


# splits the classes into training, validation, and testing classes
def class_train_validation_test_split(image_classes):
    keys = [*image_classes]

    keys_indices = list(range(len(keys)))
    random.shuffle(keys_indices)

    train_keys_indices = keys_indices[:int(len(image_classes) * TRAIN_DATA_PERCENTAGE)]
    train_image_classes = dict()
    for i in range(len(train_keys_indices)):
        key = keys[train_keys_indices[i]]
        train_image_classes[key] = image_classes[key]
    keys_indices = keys_indices[len(train_keys_indices):]

    validation_keys_indices = keys_indices[:int(len(image_classes) * VALIDATION_DATA_PERCENTAGE)]
    validation_image_classes = dict()
    for i in range(len(validation_keys_indices)):
        key = keys[validation_keys_indices[i]]
        validation_image_classes[key] = image_classes[key]
    keys_indices = keys_indices[len(validation_keys_indices):]

    test_image_classes = dict()
    for i in range(len(keys_indices)):
        key = keys[keys_indices[i]]
        test_image_classes[key] = image_classes[key]

    return train_image_classes, validation_image_classes, test_image_classes


# serialize python object into a file
def cache_object(obj, file):
    with open(file, 'wb') as f:
        pickle.dump(obj, f)


# deserialize python object from a file
def load_cached(file):
    with open(file, 'rb') as f:
        return pickle.load(f)


# returns a dictionary of the classes of items in `array` according to `class_lambda`
def extract_classes(array, class_lambda=lambda f: f[0:9]):
    classes = dict()
    for item in array:
        c = class_lambda(item)
        if c not in classes:
            classes[c] = []
        classes[c].append(item)
    return classes


class MinimumRecurrenceRateRandomSamplerWithReplacement:
    def __init__(self, array) -> None:
        self.array = array
        self.left_choices = array.copy()

    def next(self):
        if len(self.left_choices) == 0:
            self.left_choices = self.array.copy()
        i = random.randint(0, len(self.left_choices) - 1)
        return self.left_choices.pop(i)


# generates lists of anchors, positives, and negatives that represent triplets to train the triplet siamese model
def generate_triplets(classes,
                      use_all_anchors=USE_ALL_ANCHORS,
                      number_of_anchors=NUMBER_OF_ANCHORS,
                      forgery_classes_per_reference=FORGERY_CLASSES_PER_REFERENCE,
                      forgeries_per_class=FORGERIES_PER_CLASS):
    keys = [*classes]
    number_of_classes = len(keys)

    anchors = []
    positives = []
    negatives = []

    for c in range(number_of_classes):
        key = keys[random.randint(0, len(keys) - 1)]
        keys_except_c = [k for k in keys if k != c]

        if use_all_anchors:
            number_of_anchors = len(classes[key])
        else:
            number_of_anchors = min(number_of_anchors, len(classes[key]))

        anchor_indices_sampler = MinimumRecurrenceRateRandomSamplerWithReplacement(list(range(len(classes[key]))))

        for r in range(number_of_anchors):
            anchor_index = anchor_indices_sampler.next()
            reference = [key, anchor_index]
            genuine_indices = list(range(len(classes[key])))
            genuine_indices.pop(anchor_index)
            forgery_keys = random.sample(keys_except_c, min(forgery_classes_per_reference, len(keys_except_c)))

            for forgery_key in forgery_keys:
                forgery_class = classes[forgery_key]
                forgery_indices_sampler = MinimumRecurrenceRateRandomSamplerWithReplacement(
                    list(range(len(forgery_class))))
                forgeries = [[forgery_key, forgery_indices_sampler.next()] for _ in
                             range(min(forgeries_per_class, len(forgery_class)))]

                anchors.extend([reference] * len(forgeries))
                genuine_indices_sampler = MinimumRecurrenceRateRandomSamplerWithReplacement(genuine_indices)
                positives.extend([[key, genuine_indices_sampler.next()] for _ in range(len(forgeries))])
                negatives.extend(forgeries)

    return anchors, positives, negatives


def shuffle_triplets(triplets):
    anchors, positives, negatives = triplets
    indices = list(range(len(anchors)))
    random.shuffle(indices)

    shuffled_anchors = [anchors[indices[i]] for i in range(len(anchors))]
    shuffled_positives = [positives[indices[i]] for i in range(len(anchors))]
    shuffled_true_negatives = [negatives[indices[i]] for i in range(len(anchors))]

    return shuffled_anchors, shuffled_positives, shuffled_true_negatives


class DataGenerator(Sequence):
    def __init__(self, image_features, image_classes, triplets, batch_size, count):
        self.batch_size = batch_size
        self.count = count
        self.image_features = image_features
        self.image_classes = image_classes
        self.anchors, self.positives, self.negatives = shuffle_triplets(triplets)
        self.true_output = np.array(
            [[0] for _ in range(batch_size)])  # np.array([[1, 0, 0, 1] for _ in range(batch_size)], np.float32)

    def __len__(self):
        return math.ceil(self.count / self.batch_size)

    def __getitem__(self, index):
        anchors, positives, negatives = [], [], []
        for i in range(index * self.batch_size, min((index + 1) * self.batch_size, len(self.anchors))):
            anchor_pair = self.anchors[i]
            positive_pair = self.positives[i]
            negative_pair = self.negatives[i]
            anchors.append(self.image_features[self.image_classes[anchor_pair[0]][anchor_pair[1]]])
            positives.append(self.image_features[self.image_classes[positive_pair[0]][positive_pair[1]]])
            negatives.append(self.image_features[self.image_classes[negative_pair[0]][negative_pair[1]]])

        if len(anchors) == 0:
            return self.__getitem__(0)

        true_output = self.true_output
        if len(anchors) != self.batch_size:
            true_output = np.array([[0] for _ in range(len(anchors))])

        return [np.array(anchors),
                np.array(positives),
                np.array(negatives)], true_output

    def on_epoch_end(self):
        self.anchors, self.positives, self.negatives = shuffle_triplets([self.anchors, self.positives, self.negatives])


class ModelLoadingMode(Enum):
    MinimumMetricValue = 0
    MaximumMetricValue = 1


def rename_best_model(model_name, mode=ModelLoadingMode.MaximumMetricValue):
    all_models = list(filter(lambda x: x[-3:] == '.h5' and x.startswith(model_name), os.listdir(os.curdir)))
    all_metrics = []

    for model in all_models:
        i1 = model.rfind('_')
        i2 = model.rfind('.')

        if i1 == -1 or i2 == -1:
            all_metrics.append(None)
            continue

        metric = model[i1 + 1: i2]

        try:
            all_metrics.append(float(metric))
        except ValueError:
            all_metrics.append(None)

    model_metric_pairs = [[all_models[i], all_metrics[i]] for i in range(len(all_models)) if all_metrics[i] is not None]
    if mode == ModelLoadingMode.MaximumMetricValue:
        best_index = np.array(model_metric_pairs)[:, 1:2].flatten().argmax()
    else:
        best_index = np.array(model_metric_pairs)[:, 1:2].flatten().argmin()

    shutil.copy(model_metric_pairs[best_index][0], model_name + '.h5')


# append lines to the report
def append_lines_to_report(lines):
    with open(EVALUATION_REPORT_FILE, 'a') as f:
        f.writelines(lines)


def configure_model_attributes(model):
    model.loss = None
    model._compile_metrics = None
    model._compile_weighted_metrics = None
    model.sample_weight_mode = None
    model.loss_weights = None


def train(image_features,
          train_image_classes,
          train_triplets,
          validation_image_classes,
          validation_triplets):
    if os.path.isfile(EMBEDDING_NET_FILE_NAME + '.h5'):
        embedding_net = load_model(EMBEDDING_NET_FILE_NAME + '.h5', compile=False)
    else:
        embedding_net = embedding_network()
    configure_model_attributes(embedding_net)
    embedding_net.summary()

    model = training_wrapped_model(features_shape=DENSE_NET_FEATURES_SHAPE,
                                   embedding_net=embedding_net)
    model.summary()

    model.compile(loss=custom_triplet_loss,
                  optimizer=SGD(lr=INITIAL_LEARNING_RATE,
                                decay=0.0,
                                momentum=0.9,
                                nesterov=True),
                  metrics=[accuracy])

    train_data_generator = DataGenerator(image_features,
                                         train_image_classes,
                                         train_triplets,
                                         TRAINING_BATCH_SIZE,
                                         len(train_triplets[0]))

    validation_data_generator = DataGenerator(image_features,
                                              validation_image_classes,
                                              validation_triplets,
                                              EVALUATION_BATCH_SIZE,
                                              len(validation_triplets[0]))

    en_last_model_checkpoint = AltModelCheckpoint(EMBEDDING_NET_FILE_NAME + '_checkpoint_last.h5',
                                                  embedding_net)
    en_best_model_checkpoint = AltModelCheckpoint(
        EMBEDDING_NET_FILE_NAME + '_checkpoint_{epoch:02d}_{val_accuracy:0.4f}.h5',
        embedding_net,
        monitor='val_accuracy',
        save_best_only=True,
        mode='max')

    def wrapper(decay_func):
        def wrapped_decay_func(e):
            new_lr = decay_func(e)
            print(f'lr: {new_lr}')
            append_lines_to_report(f'lr: {new_lr}\n')
            return new_lr

        return wrapped_decay_func

    learning_rate_scheduler = LearningRateScheduler(wrapper(exponential_decay()), verbose=0)

    append_lines_to_report('======= training results =======\n')

    update_report_callback = LambdaCallback(on_epoch_end=lambda epoch, logs: append_lines_to_report(
        f'epoch: {epoch} - loss: {logs["loss"]} - accuracy: {logs["accuracy"]} - validation loss: {logs["val_loss"]} - validation accuracy: {logs["val_accuracy"]}\n'
    ))

    model.fit(train_data_generator,
              steps_per_epoch=math.ceil(len(train_triplets[0]) / TRAINING_BATCH_SIZE),
              validation_data=validation_data_generator,
              validation_steps=math.ceil(len(validation_triplets[0]) / EVALUATION_BATCH_SIZE),
              epochs=EPOCHS,
              callbacks=[
                  en_last_model_checkpoint,
                  en_best_model_checkpoint,
                  learning_rate_scheduler,
                  update_report_callback])

    rename_best_model(EMBEDDING_NET_FILE_NAME)


def evaluate_model(image_features,
                   test_image_classes,
                   test_triplets):
    embedding_net = load_model(EMBEDDING_NET_FILE_NAME + '.h5', compile=False)
    model = training_wrapped_model(features_shape=DENSE_NET_FEATURES_SHAPE,
                                   embedding_net=embedding_net)
    model.summary()

    model.compile(loss=custom_triplet_loss,
                  optimizer=SGD(lr=INITIAL_LEARNING_RATE,
                                decay=0.0,
                                momentum=0.9,
                                nesterov=True),
                  metrics=[accuracy])

    test_data_generator = DataGenerator(image_features,
                                        test_image_classes,
                                        test_triplets,
                                        EVALUATION_BATCH_SIZE,
                                        len(test_triplets[0]))

    append_lines_to_report('======= evaluation results =======\n')

    results = model.evaluate_generator(generator=test_data_generator,
                                       steps=math.ceil(len(test_triplets[0]) / EVALUATION_BATCH_SIZE),
                                       use_multiprocessing=False)

    append_lines_to_report(f'test loss: {results[0]} - test accuracy: {results[1]}\n\n')


def generate_image_embeddings(model,
                              image_features,
                              image_classes,
                              triplets,
                              batch_size=EVALUATION_BATCH_SIZE):
    anchors, positives, negatives = triplets

    embeddings_image_classes = dict()
    for pairs in [anchors, negatives]:
        for x in pairs:
            key = x[0]
            if key not in embeddings_image_classes:
                embeddings_image_classes[key] = image_classes[key]

    all_images = []
    for images in embeddings_image_classes.values():
        all_images.extend(images)

    image_embeddings = dict()
    batches = math.floor(len(all_images) / batch_size)
    progress = tqdm(total=batches)
    for i in range(batches):
        images_batch = all_images[i * batch_size: (i + 1) * batch_size]
        features_batch = [image_features[image] for image in images_batch]
        embeddings_batch = model.predict(np.array(features_batch))
        for j in range(batch_size):
            image_embeddings[images_batch[j]] = embeddings_batch[j]
        progress.update()
    progress.close()

    remainder = len(all_images) % batch_size
    if remainder != 0:
        images_batch = all_images[batches * batch_size:]
        features_batch = [image_features[image] for image in images_batch]
        embeddings_batch = model.predict(np.array(features_batch))
        for j in range(len(images_batch)):
            image_embeddings[images_batch[j]] = embeddings_batch[j]

    return image_embeddings, embeddings_image_classes


def evaluate_on_threshold(distances, threshold=100.0):
    genuine_acceptance_accuracy = 0
    genuine_count = 0
    forgery_rejection_accuracy = 0
    forgery_count = 0

    for dist, match in distances:
        if dist <= threshold and match == 1:
            genuine_acceptance_accuracy += 1
        if dist > threshold and match == 0:
            forgery_rejection_accuracy += 1
        if match == 1:
            genuine_count += 1
        if match == 0:
            forgery_count += 1

    forgery_rejection_accuracy /= forgery_count
    genuine_acceptance_accuracy /= genuine_count
    return forgery_rejection_accuracy, genuine_acceptance_accuracy


def cache_distances(image_embeddings,
                    embeddings_image_classes,
                    test_triplets,
                    file_prefix='',
                    distance_fun=DISTANCE_FUNCTION):
    anchors, positives, negatives = test_triplets
    distances = []

    for i in range(len(anchors)):
        anchor_pair = anchors[i]
        positive_pair = positives[i]
        negative_pair = negatives[i]
        reference = image_embeddings[embeddings_image_classes[anchor_pair[0]][anchor_pair[1]]]
        genuine = image_embeddings[embeddings_image_classes[positive_pair[0]][positive_pair[1]]]
        forgery = image_embeddings[embeddings_image_classes[negative_pair[0]][negative_pair[1]]]

        # 1 means similar and 0 means different
        distances.append([distance_fun(reference, genuine, TensorType.NP_TENSOR), 1])
        distances.append([distance_fun(reference, forgery, TensorType.NP_TENSOR), 0])

    cache_object(distances, file_prefix + DISTANCES_CACHE_FILE)


def evaluate_model_and_generate_reports(image_features,
                                        test_image_classes,
                                        test_triplets,
                                        reports_prefix=''):
    evaluate_model(image_features,
                   test_image_classes,
                   test_triplets)

    # ------------- cache the embeddings of the test feature vectors ------------- #
    if not os.path.isfile(reports_prefix + IMAGE_EMBEDDINGS_FILE):
        image_embeddings, embeddings_image_classes = generate_image_embeddings(
            load_model(MODEL_FILE_NAME + '.h5', compile=False),
            image_features,
            test_image_classes,
            test_triplets)

        cache_object(image_embeddings, reports_prefix + IMAGE_EMBEDDINGS_FILE)
        cache_object(embeddings_image_classes, reports_prefix + EMBEDDINGS_IMAGE_CLASSES_FILE)

        del image_embeddings
        del embeddings_image_classes
    del image_features

    # ------ calculate and cache the distances between the test embeddings ------- #
    if not os.path.isfile(os.path.join(os.path.curdir, reports_prefix + DISTANCES_CACHE_FILE)):
        image_embeddings = load_cached(reports_prefix + IMAGE_EMBEDDINGS_FILE)
        embeddings_image_classes = load_cached(reports_prefix + EMBEDDINGS_IMAGE_CLASSES_FILE)

        cache_distances(image_embeddings,
                        embeddings_image_classes,
                        test_triplets,
                        reports_prefix)

        del image_embeddings
        del embeddings_image_classes

    # ------------------ plot the distribution of the distances ------------------ #
    distances = load_cached(reports_prefix + DISTANCES_CACHE_FILE)
    forgery_distances = []
    genuine_distances = []
    for dist, match in distances:
        if dist == 0:
            continue
        if match == 1:
            genuine_distances.append(dist)
        if match == 0:
            forgery_distances.append(dist)
    bins = [DISTANCE_MULTIPLIER * i for i in range(int(LIMIT_DISTANCE / DISTANCE_MULTIPLIER) + 1)]
    plt.hist(forgery_distances, bins, color='red', label='forgery')
    plt.hist(genuine_distances, bins, color='blue', label='genuine')
    plt.legend(loc='best')
    plt.savefig(reports_prefix + 'distributions.png', dpi=200)
    plt.close('all')

    # ----------- calculate and cache forgery rejection and genuine acceptance accuracies ------------ #
    if not os.path.isfile(os.path.join(os.path.curdir, reports_prefix + FORGERY_REJECTION_ACCURACIES_CACHE_FILE)) or \
            not os.path.isfile(os.path.join(os.path.curdir, reports_prefix + GENUINE_ACCEPTANCE_ACCURACIES_CACHE_FILE)):
        forgery_rejection_accuracies = []
        genuine_acceptance_accuracies = []
        progress = tqdm(total=int(LIMIT_DISTANCE / DISTANCE_MULTIPLIER))
        for i in range(1, int(LIMIT_DISTANCE / DISTANCE_MULTIPLIER) + 1):
            fra, gaa = evaluate_on_threshold(distances, i * DISTANCE_MULTIPLIER)
            forgery_rejection_accuracies.append(fra)
            genuine_acceptance_accuracies.append(gaa)
            progress.update()
        progress.close()

        cache_object(forgery_rejection_accuracies,
                     reports_prefix + FORGERY_REJECTION_ACCURACIES_CACHE_FILE)
        cache_object(genuine_acceptance_accuracies,
                     reports_prefix + GENUINE_ACCEPTANCE_ACCURACIES_CACHE_FILE)

    # ------------------------------------------------------------------------------------------------
    #  find the threshold where the genuine acceptance accuracy equals the forgery rejection accuracy
    forgery_rejection_accuracies = load_cached(reports_prefix + FORGERY_REJECTION_ACCURACIES_CACHE_FILE)
    genuine_acceptance_accuracies = load_cached(reports_prefix + GENUINE_ACCEPTANCE_ACCURACIES_CACHE_FILE)
    minimum_difference = 1
    equal_threshold = None
    for i in range(len(forgery_rejection_accuracies)):
        fra = forgery_rejection_accuracies[i]
        gaa = genuine_acceptance_accuracies[i]

        difference = abs(fra - gaa)
        if difference < minimum_difference:
            minimum_difference = difference
            equal_threshold = (i + 1) * DISTANCE_MULTIPLIER
    equal_threshold_index = int(equal_threshold / DISTANCE_MULTIPLIER) - 1
    append_lines_to_report([f'equal on threshold: {equal_threshold}\n',
                            f'maximum threshold: {LIMIT_DISTANCE}\n',
                            f'genuine acceptance accuracy: {genuine_acceptance_accuracies[equal_threshold_index]}\n',
                            f'forgery rejection accuracy: {forgery_rejection_accuracies[equal_threshold_index]}\n'])

    # ------------------------------------------------------------------------------------------------
    #  find the thresholds where the genuine acceptance accuracy and the forgery rejection accuracy
    #  are most probable
    forgery_most_probable_distance = np.argmax(np.histogram(forgery_distances, bins)[0]) * DISTANCE_MULTIPLIER
    genuine_most_probable_distance = np.argmax(np.histogram(genuine_distances, bins)[0]) * DISTANCE_MULTIPLIER
    append_lines_to_report([f'genuine most probable distance: {genuine_most_probable_distance}\n',
                            f'forgery most probable distance: {forgery_most_probable_distance}\n'])

    # ----------------------------- plot accuracies ------------------------------ #
    plt.plot(bins[1:],
             forgery_rejection_accuracies, color='red',
             label='forgery detection accuracy')
    plt.plot(bins[1:],
             genuine_acceptance_accuracies, color='blue',
             label='genuine detection accuracy')
    plt.legend(loc='best')
    plt.savefig(reports_prefix + 'accuracies.png', dpi=200)
    plt.close('all')

    # ----------------------------- plot error rates ----------------------------- #
    plt.plot(bins[1:],
             1 - np.array(forgery_rejection_accuracies),
             color='red',
             label='FAR')
    plt.plot(bins[1:],
             1 - np.array(genuine_acceptance_accuracies),
             color='blue',
             label='FRR')
    plt.legend(loc='best')
    plt.savefig(reports_prefix + 'error_rates.png', dpi=200)
    plt.close('all')

    # ----- map the distances to values between 0 and 100 where equal_threshold is mapped to 50 ------ #
    x = np.arange(0, 101)
    f = interpolate.interp1d([101, 50, 0], [0, equal_threshold, LIMIT_DISTANCE], kind='linear')

    # --------------------------------- plot the mapped error rates ---------------------------------- #
    plt.plot(x, [1 - forgery_rejection_accuracies[int(f(i) / DISTANCE_MULTIPLIER) - 1] for i in x],
             color='red',
             label='FAR')
    plt.plot(x, [1 - genuine_acceptance_accuracies[int(f(i) / DISTANCE_MULTIPLIER) - 1] for i in x],
             color='blue',
             label='FRR')
    plt.legend(loc='best')
    plt.savefig(reports_prefix + 'mapped_error_rates.png', dpi=200)
    plt.close('all')

    # ------------- write the mapped error rates to the report file -------------- #
    x = np.arange(0, 101, 5)
    false_acceptance_rates = [1 - forgery_rejection_accuracies[int(f(i) / DISTANCE_MULTIPLIER) - 1] for i in x]
    false_rejection_rates = [1 - genuine_acceptance_accuracies[int(f(i) / DISTANCE_MULTIPLIER) - 1] for i in x]

    append_lines_to_report(f'\nTRESH\t\tFAR\t\tFRR\n')
    for i in x:
        far = format(false_acceptance_rates[i // 5] * 100, "2.2f")
        frr = format(false_rejection_rates[i // 5] * 100, "2.2f")
        append_lines_to_report(f'{i}\t\t{far}%\t\t{frr}%\n')


def prepare_data():
    image_features = load_cached(IMAGE_FEATURES_FILE)
    image_classes = extract_classes(image_features.keys())
    cache_object(image_classes, IMAGE_CLASSES_FILE)

    train_image_classes, validation_image_classes, test_image_classes = class_train_validation_test_split(
        image_classes)

    cache_object(train_image_classes, TRAIN_IMAGE_CLASSES_FILE)
    cache_object(validation_image_classes, VALIDATION_IMAGE_CLASSES_FILE)
    cache_object(test_image_classes, TEST_IMAGE_CLASSES_FILE)

    train_triplets = generate_triplets(train_image_classes)
    validation_triplets = generate_triplets(validation_image_classes)
    test_triplets = generate_triplets(test_image_classes)

    del train_image_classes
    del validation_image_classes
    del test_image_classes

    cache_object(train_triplets, TRAIN_TRIPLETS_FILE)
    cache_object(validation_triplets, VALIDATION_TRIPLETS_FILE)
    cache_object(test_triplets, TEST_TRIPLETS_FILE)

    del image_features
    del image_classes
    del train_triplets
    del validation_triplets
    del test_triplets


def evaluate_200_dpi():
    test_image_classes = load_cached(TEST_IMAGE_CLASSES_FILE)

    refined_image_classes = dict()

    for c in test_image_classes:
        refined_image_classes[c] = []
        for image_name in test_image_classes[c]:
            if os.path.splitext(image_name)[0].endswith('_200'):
                refined_image_classes[c].append(image_name)

    test_triplets = generate_triplets(refined_image_classes)
    image_features = load_cached(IMAGE_FEATURES_FILE)

    evaluate_model_and_generate_reports(image_features,
                                        refined_image_classes,
                                        test_triplets,
                                        '200_dpi_')


def run_pipeline():
    # ----------- extract feature vectors into the features directory ------------ #
    if not os.path.isfile(IMAGE_FEATURES_FILE):
        image_features = load_image_features()
        cache_object(image_features, IMAGE_FEATURES_FILE)
        del image_features

    if not os.path.isfile(TRAIN_TRIPLETS_FILE) or \
            not os.path.isfile(VALIDATION_TRIPLETS_FILE) or \
            not os.path.isfile(TEST_TRIPLETS_FILE):
        prepare_data()

    # ------------------------------- train model -------------------------------- #
    if not os.path.isfile(os.path.join(os.path.curdir, MODEL_FILE_NAME + '.h5')):
        image_features = load_cached(IMAGE_FEATURES_FILE)
        train_triplets = load_cached(TRAIN_TRIPLETS_FILE)
        validation_triplets = load_cached(VALIDATION_TRIPLETS_FILE)

        train_image_classes = load_cached(TRAIN_IMAGE_CLASSES_FILE)
        validation_image_classes = load_cached(VALIDATION_IMAGE_CLASSES_FILE)

        train(image_features,
              train_image_classes,
              train_triplets,
              validation_image_classes,
              validation_triplets)

        del image_features
        del train_triplets
        del validation_triplets

    # ---------------- evaluate model on the test feature vectors ---------------- #
    evaluate_model_and_generate_reports(load_cached(IMAGE_FEATURES_FILE),
                                        load_cached(TEST_IMAGE_CLASSES_FILE),
                                        load_cached(TEST_TRIPLETS_FILE))


if __name__ == '__main__':
    run_pipeline()
    evaluate_200_dpi()
