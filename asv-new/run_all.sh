python3 data_generator.py

python3 siamese.py

if [[ -d siamese ]]
then
    rm -rf siamese
fi
mkdir siamese

mv *.h5 siamese
mv *.txt siamese
mv *.pickle siamese

python3 triplet.py

if [[ -d triplet ]]
then
    rm -rf triplet
fi
mkdir triplet

mv *.h5 triplet
mv *.txt triplet
mv *.pickle triplet
