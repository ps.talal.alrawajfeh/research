#!/usr/bin/python3.6

import random

import cv2
import numpy as np
from PIL import Image, ImageDraw, ImageFont


# Returns 1 or -1 randomly.
def random_sign():
    return (-1) ** random.randint(0, 9)


# Returns True or False randomly.
def random_bool():
    return True if random_sign() == 1 else False


# Returns a random real number within a range.
def random_within_range(min_random, max_random):
    return (max_random - min_random) * random.random() + min_random


# Returns the smallest rectangle that contains all non-zero (gray) pixels.
def find_minimal_bounding_box(image):
    image_height = image.shape[0]
    image_width = image.shape[1]

    y1 = 0
    while y1 < image_height and not np.any(image[y1]):
        y1 += 1
    if y1 == image_width:
        y1 = 0

    y2 = image_height - 1
    while y2 >= 0 and not np.any(image[y2]):
        y2 -= 1
    if y2 == 0:
        y2 = image_height - 1

    temp = image.T

    x1 = 0
    while x1 < image_width and not np.any(temp[x1]):
        x1 += 1
    if x1 == image_width:
        x1 = 0

    x2 = image_width - 1
    while x2 >= 0 and not np.any(temp[x2]):
        x2 -= 1
    if x2 == 0:
        x2 = image_width - 1

    return x1, y1, x2 + 1, y2 + 1


def crop_image(image):
    x1, y1, x2, y2 = find_minimal_bounding_box(image)
    return image[y1:y2, x1:x2]


# Returns the image resized by a factor.
def resize(image, resize_factor=1):
    if resize_factor == 1:
        return image
    resize_width = int(image.shape[1] * resize_factor)
    if resize_width < 2:
        resize_width = 2
    resize_height = int(image.shape[0] * resize_factor)
    if resize_height < 2:
        resize_height = 2
    return cv2.resize(image,
                      (resize_width,
                       resize_height),
                      interpolation=cv2.INTER_AREA)


def print_text(text,
               font_path,
               desired_font_size,
               foreground_color=255,
               start_shape=(50, 150),
               safe_margin_upper_bound_percentages=(0.5, 0.5)):
    safe_height = int(start_shape[0] * (1 + safe_margin_upper_bound_percentages[0]))
    safe_width = int(start_shape[1] * (1 + safe_margin_upper_bound_percentages[1]))

    while True:
        image = np.zeros((safe_height, safe_width), np.uint8)
        image = Image.fromarray(image, 'L')
        draw = ImageDraw.Draw(image)

        font = ImageFont.truetype(font_path, desired_font_size)
        width, height = draw.textsize(text, font=font)
        if height > safe_height or width > safe_width:
            safe_height = int(height * (1 + safe_margin_upper_bound_percentages[0]))
            safe_width = int(width * (1 + safe_margin_upper_bound_percentages[1]))
            continue

        draw.text((0, 0), text, fill=foreground_color, font=font)
        return crop_image(np.asarray(image).astype(np.uint8))

    return image


# Capitalizes letters at random.
def capitalize_randomly(label):
    result = ''
    for c in label:
        if ord('a') <= ord(c) <= ord('z'):
            if random_bool():
                result += c.upper()
            else:
                result += c
        else:
            result += c
    return result


# Capitalizes the first letter of each word.
def capitalize_first(label):
    result = ''
    if ord('a') <= ord(label[0]) <= ord('z'):
        result += label[0].upper()
    else:
        result += label[0]
    for i in range(1, len(label)):
        before = ord(label[i - 1])
        if (ord('a') <= ord(label[i]) <= ord('z')) and (before < ord('a') or before > ord('z')):
            result += label[i].upper()
        else:
            result += label[i]
    return result
