import mimetypes
import os
import pickle
import random

import cv2
import numpy as np
from keras.applications.densenet import DenseNet201, preprocess_input
from matplotlib import pyplot as plt
from tqdm import tqdm

#
# Constants
#

DATA_BASE_PATH = 'C:\\Users\\USER\\Development\\Data\\ASV\\Raw\\'
NEW_SIGNATURES_PATH = '.\\data\\new_signatures'
SIGNATURE_CLASSES_PATHS = [DATA_BASE_PATH + 'CABHBTF',
                           # NEW_SIGNATURES_PATH,
                           DATA_BASE_PATH + 'OriginalSignatures']
GENERATED_DATA_PATH = '.\\data\\generated'
AUGMENTED_IMAGES_PATH = '.\\data\\output'

CLASS_NUMBER_LENGTH = 9
NUMBER_OF_IMAGES_PER_CLASS_THRESHOLD = 2
NUMBER_OF_IMAGES_TO_AUGMENTATIONS_PER_IMAGE = 4
# average number of images per class (n) = (3 * x + 2) * 2


PRE_TRAINED_MODEL_INPUT_IMAGE_SIZE = (96, 48)
PRE_TRAINED_MODEL_INPUT_IMAGE_SHAPE = (48, 96, 1)
PRE_TRAINED_MODEL_PRE_PROCESSOR = preprocess_input
PRE_TRAINED_MODEL = DenseNet201(include_top=False,
                                weights='imagenet',
                                input_shape=(224, 224, 3),
                                pooling='avg')

MAX_TRANSLATION_PERCENTAGE = 40
MIN_TRANSLATION_PERCENTAGE = 0
IMAGE_TRANSLATION_RANDOMIZATION_STEP_SIZE = 5

DEFAULT_DPI = 200

MIN_ROTATION_ANGLE = 1
MAX_ROTATION_ANGLE = 7

MAX_SPECKLE_COUNT = 10
MAX_SPECKLE_SIZE = 2

MIN_IMAGE_DPI = 100
MAX_IMAGE_DPI = 175
DPI_RANDOMIZATION_STEP_SIZE = 25

MIN_IMAGE_QUALITY = 50
MAX_IMAGE_QUALITY = 95
IMAGE_QUALITY_RANDOMIZATION_STEP_SIZE = 5

IMAGE_RESIZE_MIN_PERCENTAGE = 1
IMAGE_RESIZE_MAX_PERCENTAGE = 10

IMAGE_CUTTING_MIN_PERCENTAGE = 1
IMAGE_CUTTING_MAX_PERCENTAGE = 10

MIN_BINARIZATION_THRESHOLD = 128
MAX_BINARIZATION_THRESHOLD = 200
BINARIZATION_THRESHOLD_STEP_SIZE = 5

IMAGE_FILTERS = (lambda x: cv2.erode(x, np.ones((2, 2), np.uint8)),
                 lambda x: cv2.dilate(x, np.ones((2, 2), np.uint8)))

IS_CUT_IMAGE_RANDOMLY_ON = False


# * Note that all functions in this module are stateless and don't alter the input. *

# -----------------------------------------------------------------------------

# Returns all the extensions for a given mime type.
def get_extensions_for_type(general_type):
    for ext in mimetypes.types_map:
        if mimetypes.types_map[ext].split('/')[0] == general_type:
            yield ext.lower()


# Function decorator for caching outputs.
def memoize(function):
    memo = {}

    def wrapper(*args):
        if args in memo:
            return memo[args]
        else:
            rv = function(*args)
            memo[args] = rv
            return rv

    return wrapper


# Returns all image extensions, e.g. '.jpg'.
@memoize
def get_image_extensions():
    return tuple(get_extensions_for_type('image'))


def file_extension(path):
    return os.path.splitext(os.path.basename(path))[1].lower()


def is_image(path):
    return file_extension(path) in get_image_extensions()


def random_sign():
    if random.random() > 0.5:
        return +1
    else:
        return -1


def random_bool():
    if random_sign() == +1:
        return True
    else:
        return False


def to_int(number):
    return int(round(number))


# -----------------------------------------------------------------------------


def read_image_grayscale(image_path):
    return cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)


def resize_image_high_quality(image, new_size):
    if new_size[0] == image.shape[1] and new_size[1] == image.shape[0]:
        return np.array(image)
    return cv2.resize(image,
                      new_size,
                      interpolation=cv2.INTER_CUBIC)


def rescale_image(image, percentage, interpolation=cv2.INTER_CUBIC):
    new_width = to_int(image.shape[1] * percentage)
    new_height = to_int(image.shape[0] * percentage)

    return cv2.resize(image,
                      (new_width, new_height),
                      interpolation=interpolation)


def binarize(image, threshold=128):
    image_copy = np.array(image)
    image_copy[image_copy >= threshold] = 255
    image_copy[image_copy < threshold] = 0
    return image_copy


def binarize_global_adaptive(image):
    mu = np.mean(image)
    sigma = np.std(image)
    return binarize(image, to_int(mu - sigma / 2))


def binarize_adaptive_gaussian(image):
    return cv2.adaptiveThreshold(image,
                                 255,
                                 cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
                                 cv2.THRESH_BINARY,
                                 11,
                                 15)


def binarize_adaptive_mean(image):
    return cv2.adaptiveThreshold(image,
                                 255,
                                 cv2.ADAPTIVE_THRESH_MEAN_C,
                                 cv2.THRESH_BINARY,
                                 11,
                                 15)


def binarize_otsu(image):
    return cv2.threshold(image,
                         0,
                         255,
                         cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]


def pad(image, horizontal_padding=(0, 0), vertical_padding=(0, 0), color=255):
    return np.pad(image,
                  (vertical_padding, horizontal_padding),
                  'constant',
                  constant_values=((color, color), (color, color)))


# Returns the correct minimal bounding box if the image is binarized.
# The background must be black and the foreground must be white.
def foreground_bounding_box(image):
    image_height = image.shape[0]
    image_width = image.shape[1]

    y1 = 0
    while y1 < image_height and not np.any(image[y1]):
        y1 += 1
    if y1 == image_width:
        y1 = 0

    y2 = image_height - 1
    while y2 >= 0 and not np.any(image[y2]):
        y2 -= 1
    if y2 == 0:
        y2 = image_height - 1

    temp = image.T

    x1 = 0
    while x1 < image_width and not np.any(temp[x1]):
        x1 += 1
    if x1 == image_width:
        x1 = 0

    x2 = image_width - 1
    while x2 >= 0 and not np.any(temp[x2]):
        x2 -= 1
    if x2 == 0:
        x2 = image_width - 1

    return x1, y1, x2 + 1, y2 + 1


# The image doesn't have to be binarized.
# The background must be white and the foreground must be black.
# The function uses Otsu's method for binarization.
def foreground_bounding_box_clever(image):
    return foreground_bounding_box(255 - binarize_otsu(image))


def crop_foreground_clever(image):
    x1, y1, x2, y2 = foreground_bounding_box_clever(image)
    return np.array(image[y1:y2, x1:x2])


def change_image_quality(image, quality=50):
    encoded = cv2.imencode('.jpg', image, [int(cv2.IMWRITE_JPEG_QUALITY), quality])[1]
    return cv2.imdecode(encoded, cv2.IMREAD_GRAYSCALE)


def rotate(image, angle):
    (h, w) = image.shape[:2]
    center_x, center_y = w / 2, h / 2

    rotation_matrix = cv2.getRotationMatrix2D((center_x, center_y),
                                              angle,
                                              1.0)
    cos = np.abs(rotation_matrix[0, 0])
    sin = np.abs(rotation_matrix[0, 1])

    new_w = h * sin + w * cos
    new_h = h * cos + w * sin

    rotation_matrix[0, 2] += new_w / 2 - center_x
    rotation_matrix[1, 2] += new_h / 2 - center_y

    return cv2.warpAffine(image,
                          rotation_matrix,
                          (to_int(new_w), to_int(new_h)),
                          borderValue=(255, 255, 255),
                          flags=cv2.INTER_CUBIC)


def change_image_dpi(image, dpi=100):
    return rescale_image(image,
                         dpi / DEFAULT_DPI,
                         cv2.INTER_AREA)


# View an image for debugging purposes.
def view_image(image, title=''):
    plt.imshow(image, plt.cm.gray)
    plt.title(title)
    plt.show()


# -----------------------------------------------------------------------------

def rotate_randomly(image,
                    min_angle=MIN_ROTATION_ANGLE,
                    max_angle=MAX_ROTATION_ANGLE):
    sign = random_sign()
    angle = random.randint(min_angle, max_angle)
    return rotate(image, sign * angle)


def speckle_randomly(image,
                     max_speckle_count=MAX_SPECKLE_COUNT,
                     max_speckle_size=MAX_SPECKLE_SIZE):
    speckled = np.array(image)

    for _ in range(random.randint(1, max_speckle_count)):
        speckle_size_x = random.randint(1, max_speckle_size)
        speckle_size_y = random.randint(1, max_speckle_size)
        speckle_x = random.randint(0, speckled.shape[1] - speckle_size_x)
        speckle_y = random.randint(0, speckled.shape[0] - speckle_size_y)

        cv2.rectangle(speckled,
                      (speckle_x, speckle_y),
                      (speckle_x + speckle_size_x, speckle_y + speckle_size_y),
                      (0, 0, 0),
                      -1)

    return speckled


def change_quality_randomly(image,
                            min_quality=MIN_IMAGE_QUALITY,
                            max_quality=MAX_IMAGE_QUALITY,
                            step_size=IMAGE_QUALITY_RANDOMIZATION_STEP_SIZE):
    return change_image_quality(image,
                                random.randrange(min_quality,
                                                 max_quality + 1,
                                                 step_size))


def apply_random_filter(image, filters=IMAGE_FILTERS):
    return random.choice(filters)(image)


def change_image_dpi_randomly(image,
                              min_dpi=MIN_IMAGE_DPI,
                              max_dpi=MAX_IMAGE_DPI,
                              dpi_step_size=DPI_RANDOMIZATION_STEP_SIZE):
    dpi = random.randrange(min_dpi, max_dpi + 1, dpi_step_size)
    return change_image_dpi(image, dpi), dpi


def translate_randomly(image,
                       min_percentage=MIN_TRANSLATION_PERCENTAGE,
                       max_percentage=MAX_TRANSLATION_PERCENTAGE,
                       step_size=IMAGE_TRANSLATION_RANDOMIZATION_STEP_SIZE):
    translate_x = random.randrange(min_percentage, max_percentage + 1, step_size)
    translate_y = random.randrange(min_percentage, max_percentage + 1, step_size)

    width = image.shape[1]
    height = image.shape[0]
    new_width = to_int(width * (1 + translate_x / 100))
    new_height = to_int(height * (1 + translate_y / 100))

    width_difference = new_width - width
    left = random.randint(0, width_difference)
    right = width_difference - left

    height_difference = new_height - height
    top = random.randint(0, height_difference)
    bottom = height_difference - top

    return pad(image,
               (left, right),
               (top, bottom))


def resize_randomly(image,
                    min_percentage=IMAGE_RESIZE_MIN_PERCENTAGE,
                    max_percentage=IMAGE_RESIZE_MAX_PERCENTAGE):
    new_width = to_int(image.shape[1] * (1 - random.randint(min_percentage, max_percentage) / 100))
    new_height = to_int(image.shape[0] * (1 - random.randint(min_percentage, max_percentage) / 100))

    return cv2.resize(image,
                      (new_width, new_height),
                      cv2.INTER_AREA)


def cut_randomly(image,
                 min_percentage=IMAGE_CUTTING_MIN_PERCENTAGE,
                 max_percentage=IMAGE_CUTTING_MAX_PERCENTAGE):
    width_cut_percentage = random.randint(min_percentage, max_percentage)
    height_cut_percentage = random.randint(min_percentage, max_percentage)

    width_cut = to_int(image.shape[1] * width_cut_percentage / 100)
    height_cut = to_int(image.shape[0] * height_cut_percentage / 100)

    left_cut = random.randint(0, width_cut)
    right_cut = random.randint(0, width_cut - left_cut)

    top_cut = random.randint(0, height_cut)
    bottom_cut = random.randint(0, height_cut - top_cut)

    cut_image = np.array(image)
    cut_image = cut_image[top_cut:image.shape[0] - bottom_cut,
                left_cut:image.shape[1] - right_cut]
    return cut_image


def random_binarization(image):
    return random_effect(image,
                         False,
                         binarize,
                         binarize_otsu)


# -----------------------------------------------------------------------------

def random_effect(image, include_identity, *effects):
    if include_identity:
        return random.choice([*effects] + [lambda x: x])(image)
    else:
        return random.choice([*effects])(image)


def alter_image_randomly(image):
    new_image = random_effect(image,
                              True,
                              binarize_otsu)

    new_image = rotate_randomly(new_image)

    new_image = random_effect(new_image,
                              True,
                              resize_randomly)

    if IS_CUT_IMAGE_RANDOMLY_ON:
        new_image = random_effect(new_image,
                                  True,
                                  lambda x: cut_randomly(crop_foreground_clever(x)))

    new_image = random_effect(new_image,
                              True,
                              lambda x: speckle_randomly(translate_randomly(x)),
                              speckle_randomly)

    new_image = random_effect(new_image,
                              True,
                              apply_random_filter,
                              lambda x: apply_random_filter(binarize_otsu(x)))

    new_image = random_effect(new_image,
                              True,
                              change_quality_randomly)

    return new_image


def augment_image(image,
                  number_of_images=NUMBER_OF_IMAGES_TO_AUGMENTATIONS_PER_IMAGE,
                  generate_random_dpi_image=False):
    if generate_random_dpi_image:
        new_image, dpi = change_image_dpi_randomly(image)
        augmented_images = [(image, 200), (new_image, dpi)]
    else:
        augmented_images = [(image, 200)]

    for _ in range(number_of_images):
        altered = alter_image_randomly(image)
        augmented_images.append((altered, 200))
        if generate_random_dpi_image:
            new_image, dpi = change_image_dpi_randomly(altered)
            augmented_images.append((new_image, dpi))

    return augmented_images


# -----------------------------------------------------------------------------

def pre_process_image(image, use_random_binarization_method=True):
    cropped_resized = resize_image_high_quality(crop_foreground_clever(image),
                                                PRE_TRAINED_MODEL_INPUT_IMAGE_SIZE)
    if use_random_binarization_method:
        binarized = random_binarization(cropped_resized)
    else:
        binarized = binarize(cropped_resized)

    return (255.0 - binarized) / 255.0


def replicate_channel(image, replicas=3):
    return np.concatenate([(np.expand_dims(image, axis=-1)) for _ in range(replicas)],
                          axis=-1)


def images_to_feature_vectors(image_batch,
                              image_pre_processor=pre_process_image):
    return np.array([image_pre_processor(image).reshape(PRE_TRAINED_MODEL_INPUT_IMAGE_SHAPE)
                     for image in image_batch])


def image_class_from_file_name(file_name):
    return file_name[0:CLASS_NUMBER_LENGTH]


def image_classes_from_path(path, current_image_classes=None):
    image_files = [f for f in os.listdir(path) if is_image(f)]

    class_id_table = dict()

    if current_image_classes is None:
        image_classes = dict()
        last_id = -1
    else:
        image_classes = current_image_classes
        last_id = max([*image_classes])

    for f in image_files:
        c = image_class_from_file_name(f)
        if c not in class_id_table:
            class_id = last_id + 1
            class_id_table[c] = class_id
            image_classes[class_id] = []
            last_id = class_id

        image_classes[class_id_table[c]].append(os.path.join(path, f))

    if NUMBER_OF_IMAGES_PER_CLASS_THRESHOLD > 1:
        for c in class_id_table:
            class_id = class_id_table[c]
            if len(image_classes[class_id]) < NUMBER_OF_IMAGES_PER_CLASS_THRESHOLD:
                image_classes.pop(class_id)

    return image_classes


def image_classes_from_paths(paths):
    image_classes = None
    for path in paths:
        image_classes = image_classes_from_path(path, image_classes)
    return image_classes


def cache_object(obj, file):
    with open(file, 'wb') as f:
        pickle.dump(obj, f)


class ImageBatchVectorizer:
    def __init__(self, batch_size=1024):
        self.images = []
        self.files = []
        self.batch_size = batch_size

    def __predict(self):
        while len(self.images) >= self.batch_size:
            feature_vectors = images_to_feature_vectors(self.images[:self.batch_size])
            self.images = self.images[self.batch_size:]
            files = self.files[:self.batch_size]
            self.files = self.files[self.batch_size:]

            for i in range(len(files)):
                cache_object(feature_vectors[i], files[i])

    def feed(self, single_image, output_file):
        self.images.append(single_image)
        self.files.append(output_file)
        self.__predict()

    def feed_all(self, images, output_files):
        self.images.extend(images)
        self.files.extend(output_files)
        self.__predict()

    def finalize(self):
        feature_vectors = images_to_feature_vectors(self.images)

        for i in range(len(self.files)):
            cache_object(feature_vectors[i], self.files[i])

        self.images = []
        self.files = []


def print_statistics(image_classes):
    print(f'total classes: {len(image_classes)}')
    average = 0
    for c in image_classes:
        average += len(image_classes[c])
    average /= len(image_classes)
    print(f'average number of images per class: {average}')


def save_feature_vectors():
    image_classes = image_classes_from_paths(SIGNATURE_CLASSES_PATHS)
    print_statistics(image_classes)

    if not os.path.isdir(GENERATED_DATA_PATH):
        os.makedirs(GENERATED_DATA_PATH)

    batch_vectorizer = ImageBatchVectorizer()

    progress_bar = tqdm(total=len(image_classes))
    for c in image_classes:
        i = 0
        for image in image_classes[c]:
            new_images = augment_image(read_image_grayscale(image))

            new_files = []
            for _, dpi in new_images:
                vector_class = str(c)
                zeros = '0' * (CLASS_NUMBER_LENGTH - len(vector_class))
                vector_class = zeros + vector_class

                new_files.append(os.path.join(GENERATED_DATA_PATH,
                                              f'{vector_class}_{i}_{dpi}.pickle'))
                i += 1

            batch_vectorizer.feed_all([im for im, _ in new_images], new_files)
        progress_bar.update()

    batch_vectorizer.finalize()
    progress_bar.close()


def write_augmented_images(enable_pre_processing=False):
    image_classes = image_classes_from_paths(SIGNATURE_CLASSES_PATHS)
    print_statistics(image_classes)

    progress_bar = tqdm(total=len(image_classes))
    for c in image_classes:
        temp_images = []
        for image in image_classes[c]:
            new_images = augment_image(read_image_grayscale(image))
            temp_images.extend(new_images)

        dir_name = os.path.join(AUGMENTED_IMAGES_PATH, str(c))
        if not os.path.isdir(dir_name):
            os.makedirs(dir_name)

        i = 0
        for image, dpi in temp_images:
            if enable_pre_processing:
                cv2.imwrite(os.path.join(dir_name, f'{i}_{dpi}.bmp'),
                            255 - pre_process_image(image))
            else:
                cv2.imwrite(os.path.join(dir_name, f'{i}_{dpi}.bmp'),
                            image)
            i += 1
        progress_bar.update()
    progress_bar.close()


if __name__ == '__main__':
    # generate_name_signatures()
    write_augmented_images(True)
    save_feature_vectors()
