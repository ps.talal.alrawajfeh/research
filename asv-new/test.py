import cv2
import numpy as np
from keras.engine.saving import load_model
from scipy import interpolate

from data_generator import images_to_feature_vectors

PIVOT_DISTANCE = 1.1
FORGERY_MOST_PROBABLE_DISTANCE = 1.43
GENUINE_MOST_PROBABLE_DISTANCE = 0.7

SIAMESE_MAPPED_VALUE = 0.8
SIAMESE_MAPPING_VALUE = 60.0

TRIPLET_ACCEPTANCE_THRESHOLD = 80.0
SIAMESE_ACCEPTANCE_THRESHOLD = 80.0
ZERO_THRESHOLD = 35.0

EPSILON = 1e-7


def euclidean_distance(image1_features_embeddings, image2_features_embeddings):
    return np.sqrt(np.sum(np.square(image1_features_embeddings - image2_features_embeddings)))


def calculate_output(image1,
                     image2,
                     siamese_model,
                     triplet_model):
    feature_vectors = images_to_feature_vectors([image1,
                                                 image2])

    siamese_prediction = siamese_model.predict([feature_vectors[0].reshape((1, 1920)),
                                                feature_vectors[1].reshape((1, 1920))])[0][0]
    triplet_predictions = triplet_model.predict(feature_vectors)

    distance = euclidean_distance(triplet_predictions[0],
                                  triplet_predictions[1])
    if distance > 2.0:
        distance = 2.0

    siamese_mapper = interpolate.interp1d([0.0, SIAMESE_MAPPED_VALUE, 1.0],
                                          [0.0, SIAMESE_MAPPING_VALUE, 100.0],
                                          kind='linear')
    triplet_mapper = interpolate.interp1d(
        [0.0, GENUINE_MOST_PROBABLE_DISTANCE, PIVOT_DISTANCE, FORGERY_MOST_PROBABLE_DISTANCE, 2.0],
        [100.0, 90.0, 50.0, 10.0, 0.0],
        kind='linear')

    siamese_value = siamese_mapper(siamese_prediction)
    triplet_value = triplet_mapper(distance)

    if 100.0 - EPSILON <= triplet_value <= 1.00 + EPSILON:
        return triplet_value
    if triplet_value >= TRIPLET_ACCEPTANCE_THRESHOLD and \
            siamese_value >= SIAMESE_ACCEPTANCE_THRESHOLD:
        return triplet_value

    combined = siamese_value * 0.5 + triplet_value * 0.5
    if combined >= ZERO_THRESHOLD:
        return combined
    else:
        return 0.0


def main():
    siamese_model = load_model('siamese_mixed_dpi.h5', compile=False)
    triplet_model = load_model('triplet_mixed_dpi.h5', compile=False)

    image1 = cv2.imread('/home/u764/Downloads/finalCleanset/9e6a0eb3a_2.bmp', cv2.IMREAD_GRAYSCALE)
    image2 = cv2.imread('/home/u764/Downloads/finalCleanset/PSUT00692_7', cv2.IMREAD_GRAYSCALE)

    print(calculate_output(image1,
                           image2,
                           siamese_model,
                           triplet_model))


if __name__ == '__main__':
    main()
