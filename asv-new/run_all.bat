call python data_generator.py

call python siamese.py

if exist siamese rmdir siamese /s /q
mkdir siamese

move *.h5 siamese
move *.txt siamese
move *.pickle siamese

call python triplet.py

if exist triplet rmdir triplet /s /q
mkdir triplet

move *.h5 triplet
move *.txt triplet
move *.pickle triplet
