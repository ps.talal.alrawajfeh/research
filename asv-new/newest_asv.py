import math
import os
import pickle
import random
import shutil
from collections import Sequence
from enum import Enum

import keras.backend as K
import numpy as np
from alt_model_checkpoint.keras import AltModelCheckpoint
from keras import Input, Model
from keras.engine.saving import load_model
from keras.layers import Dense, Lambda, PReLU, BatchNormalization, Dropout, concatenate
from keras.optimizers import SGD
from keras.regularizers import l2
from tensorflow.python.keras.callbacks import LambdaCallback

# ============================================================================

DATA_PATH = '.\\data\\generated'

MODEL_FILE_NAME = 'asv-net'
EMBEDDING_NET_FILE_NAME = 'embedding-net'
SIAMESE_NET_FILE_NAME = 'siamese-net'

EVALUATION_REPORT_FILE = 'asv-net-report.txt'

IMAGE_FEATURES_FILE = 'image_features.pickle'
IMAGE_CLASSES_FILE = 'image_classes.pickle'

TRAIN_TRIPLETS_FILE = 'train_triplets.pickle'
VALIDATION_TRIPLETS_FILE = 'validation_triplets.pickle'
TEST_TRIPLETS_FILE = 'test_triplets.pickle'

TRAIN_IMAGE_CLASSES_FILE = 'train_image_classes.pickle'
VALIDATION_IMAGE_CLASSES_FILE = 'validation_image_classes.pickle'
TEST_IMAGE_CLASSES_FILE = 'test_image_classes.pickle'

TRAIN_DATA_PERCENTAGE = 0.7
VALIDATION_DATA_PERCENTAGE = 0.15

USE_ALL_ANCHORS = False
NUMBER_OF_ANCHORS = 5
FORGERY_CLASSES_PER_REFERENCE = 5
FORGERIES_PER_CLASS = 5

DENSE_NET_FEATURES_SHAPE = (1920,)

EN_NUMBER_OF_HIDDEN_LAYERS = 1
EN_EMBEDDING_LAYER_SIZE = 1024
EN_HIDDEN_LAYER_SIZE = 1024
DROPOUT_RATE = 0.5

SN_HIDDEN_LAYER_SIZE = 1024
SN_NUMBER_OF_HIDDEN_LAYERS = 2
SN_OUTPUT_LAYER_SIZE = 2

MARGIN = 0.2
INITIAL_LEARNING_RATE = 0.1
TRAINING_BATCH_SIZE = 128
EVALUATION_BATCH_SIZE = 1024
EPOCHS = 1


class TensorType(Enum):
    TF_TENSOR = 1
    NP_TENSOR = 2


def euclidean_distance(x, y, tensor_type=TensorType.TF_TENSOR):
    if tensor_type == TensorType.TF_TENSOR:
        return K.sum(K.square(x - y), axis=-1)
    elif tensor_type == TensorType.NP_TENSOR:
        return np.sum(np.square(x - y))
    raise Exception(f'not implemented for tensor_type={tensor_type}')


DISTANCE_FUNCTION = euclidean_distance


# ============================================================================


def embedding_network(features_shape=DENSE_NET_FEATURES_SHAPE):
    features = Input(features_shape)
    embedding = BatchNormalization(momentum=0.9)(features)

    for _ in range(EN_NUMBER_OF_HIDDEN_LAYERS):
        embedding = Dense(EN_HIDDEN_LAYER_SIZE,
                          kernel_initializer='he_normal',
                          kernel_regularizer=l2(1e-4),
                          use_bias=False)(embedding)
        embedding = BatchNormalization(momentum=0.9)(embedding)
        embedding = Dropout(DROPOUT_RATE)(embedding)
        embedding = PReLU()(embedding)

    embedding = Dense(EN_EMBEDDING_LAYER_SIZE,
                      kernel_initializer='he_normal',
                      kernel_regularizer=l2(1e-4))(embedding)
    embedding = PReLU()(embedding)
    embedding = Lambda(lambda x: K.l2_normalize(x, axis=-1))(embedding)

    return Model(inputs=features, outputs=embedding)


def siamese_network(features_shape=(EN_EMBEDDING_LAYER_SIZE,)):
    input1 = Input(shape=features_shape)
    input2 = Input(shape=features_shape)

    merged = Lambda(lambda x: K.abs(x[0] - x[1]),
                    output_shape=(EN_EMBEDDING_LAYER_SIZE,))([input1, input2])

    fc = BatchNormalization(momentum=0.9)(merged)

    for _ in range(SN_NUMBER_OF_HIDDEN_LAYERS):
        fc = Dense(SN_HIDDEN_LAYER_SIZE,
                   use_bias=False)(fc)
        fc = BatchNormalization(momentum=0.9)(fc)
        fc = PReLU()(fc)
        fc = Dropout(DROPOUT_RATE)(fc)

    prediction = Dense(SN_OUTPUT_LAYER_SIZE,
                       activation='sigmoid',
                       use_bias=False)(fc)

    return Model(inputs=[input1, input2], outputs=prediction)


def training_wrapped_model(features_shape=DENSE_NET_FEATURES_SHAPE,
                           embedding_net=embedding_network(),
                           siamese_net=siamese_network()):
    anchor = Input(features_shape)
    positive = Input(features_shape)
    negative = Input(features_shape)

    anchor_embedding = embedding_net(anchor)
    positive_embedding = embedding_net(positive)
    negative_embedding = embedding_net(negative)

    anchor_positive_siamese_prediction = siamese_net([anchor_embedding, positive_embedding])
    anchor_negative_siamese_prediction = siamese_net([anchor_embedding, negative_embedding])

    merged_vector = concatenate(
        [anchor_embedding,
         positive_embedding,
         negative_embedding,
         anchor_positive_siamese_prediction,
         anchor_negative_siamese_prediction], axis=-1)

    return Model(inputs=[anchor, positive, negative], outputs=merged_vector)


def custom_triplet_loss(_, y_pred, distance_fun=DISTANCE_FUNCTION):
    anchor = y_pred[:, 0:EN_EMBEDDING_LAYER_SIZE]
    positive = y_pred[:, EN_EMBEDDING_LAYER_SIZE: EN_EMBEDDING_LAYER_SIZE * 2]
    negative = y_pred[:, EN_EMBEDDING_LAYER_SIZE * 2: EN_EMBEDDING_LAYER_SIZE * 3]

    positive_dist = distance_fun(anchor, positive)
    negative_dist = distance_fun(anchor, negative)

    losses = K.maximum(K.constant(0), positive_dist - negative_dist + K.constant(MARGIN))

    hard_distances_mask = K.cast(negative_dist < positive_dist, dtype='float32')
    hard_distances_count = K.maximum(K.constant(1.0), K.sum(hard_distances_mask))
    hard_losses = hard_distances_mask * losses
    hard_average_loss = K.sum(hard_losses, -1) / hard_distances_count

    semi_hard_distances_mask = K.cast(negative_dist >= positive_dist, dtype='float32') * \
                               K.cast(negative_dist < positive_dist + K.constant(MARGIN), dtype='float32')
    semi_hard_distances_count = K.maximum(K.constant(1.0), K.sum(semi_hard_distances_mask))
    semi_hard_losses = semi_hard_distances_mask * losses
    semi_hard_average_loss = K.sum(semi_hard_losses, -1) / semi_hard_distances_count

    return K.maximum(K.constant(0), 0.5 * hard_average_loss + 0.5 * semi_hard_average_loss)


def hard_binary_cross_entropy_loss(y_true, y_pred):
    losses = K.binary_crossentropy(y_true, y_pred)
    return K.sum(losses, -1) / (
            K.sum(K.cast(losses > K.constant(0.0), 'float'), -1) + K.epsilon())


def total_loss(y_true, y_pred):
    apn_size = EN_EMBEDDING_LAYER_SIZE * 3
    anchor_positive_negative = y_pred[:, :apn_size]
    anchor_positive_prediction = y_pred[:, apn_size: apn_size + 2]
    anchor_negative_prediction = y_pred[:, apn_size + 2:]
    return (hard_binary_cross_entropy_loss(y_true[:, 0:2], anchor_positive_prediction) +
            hard_binary_cross_entropy_loss(y_true[:, 2:], anchor_negative_prediction) +
            custom_triplet_loss(y_true, anchor_positive_negative)) / 3


def accuracy(_, y_pred, distance_fun=DISTANCE_FUNCTION, siamese_threshold=0.5):
    anchor = y_pred[:, 0:EN_EMBEDDING_LAYER_SIZE]
    positive = y_pred[:, EN_EMBEDDING_LAYER_SIZE: EN_EMBEDDING_LAYER_SIZE * 2]
    negative = y_pred[:, EN_EMBEDDING_LAYER_SIZE * 2: EN_EMBEDDING_LAYER_SIZE * 3]

    apn_size = EN_EMBEDDING_LAYER_SIZE * 3
    anchor_positive_prediction = y_pred[:, apn_size: apn_size + 2]
    anchor_negative_prediction = y_pred[:, apn_size + 2:]

    anchor_positive_binary = K.cast(anchor_positive_prediction >= 0.5, dtype='float32')[:, :1]
    anchor_negative_binary = K.cast(anchor_negative_prediction < 0.5, dtype='float32')[:, :1]

    positive_dist = distance_fun(anchor, positive)
    negative_dist = distance_fun(anchor, negative)

    return (K.mean(positive_dist + MARGIN < negative_dist) +
            K.mean(anchor_positive_binary) +
            K.mean(anchor_negative_binary)) / 3


# ============================================================================


def load_image_features():
    images = os.listdir(DATA_PATH)
    image_features = dict()

    for image in images:
        image_features[image] = load_cached(os.path.join(DATA_PATH, image))

    return image_features


# splits the classes into training, validation, and testing classes
def class_train_validation_test_split(image_classes):
    keys = [*image_classes]

    keys_indices = list(range(len(keys)))
    random.shuffle(keys_indices)

    train_keys_indices = keys_indices[:int(len(image_classes) * TRAIN_DATA_PERCENTAGE)]
    train_image_classes = dict()
    for i in range(len(train_keys_indices)):
        key = keys[train_keys_indices[i]]
        train_image_classes[key] = image_classes[key]
    keys_indices = keys_indices[len(train_keys_indices):]

    validation_keys_indices = keys_indices[:int(len(image_classes) * VALIDATION_DATA_PERCENTAGE)]
    validation_image_classes = dict()
    for i in range(len(validation_keys_indices)):
        key = keys[validation_keys_indices[i]]
        validation_image_classes[key] = image_classes[key]
    keys_indices = keys_indices[len(validation_keys_indices):]

    test_image_classes = dict()
    for i in range(len(keys_indices)):
        key = keys[keys_indices[i]]
        test_image_classes[key] = image_classes[key]

    return train_image_classes, validation_image_classes, test_image_classes


# serialize python object into a file
def cache_object(obj, file):
    with open(file, 'wb') as f:
        pickle.dump(obj, f)


# deserialize python object from a file
def load_cached(file):
    with open(file, 'rb') as f:
        return pickle.load(f)


# returns a dictionary of the classes of items in `array` according to `class_lambda`
def extract_classes(array, class_lambda=lambda f: f[0:9]):
    classes = dict()
    for item in array:
        c = class_lambda(item)
        if c not in classes:
            classes[c] = []
        classes[c].append(item)
    return classes


class MinimumRecurrenceRateRandomSamplerWithReplacement:
    def __init__(self, array) -> None:
        self.array = array
        self.left_choices = array.copy()

    def next(self):
        if len(self.left_choices) == 0:
            self.left_choices = self.array.copy()
        i = random.randint(0, len(self.left_choices) - 1)
        return self.left_choices.pop(i)


# generates lists of anchors, positives, and negatives that represent triplets to train the triplet siamese model
def generate_triplets(classes,
                      use_all_anchors=USE_ALL_ANCHORS,
                      number_of_anchors=NUMBER_OF_ANCHORS,
                      forgery_classes_per_reference=FORGERY_CLASSES_PER_REFERENCE,
                      forgeries_per_class=FORGERIES_PER_CLASS):
    keys = [*classes]
    number_of_classes = len(keys)

    anchors = []
    positives = []
    negatives = []

    for c in range(number_of_classes):
        key = keys[random.randint(0, len(keys) - 1)]
        keys_except_c = [k for k in keys if k != c]

        if use_all_anchors:
            number_of_anchors = len(classes[key])
        else:
            number_of_anchors = min(number_of_anchors, len(classes[key]))

        anchor_indices_sampler = MinimumRecurrenceRateRandomSamplerWithReplacement(list(range(len(classes[key]))))

        for r in range(number_of_anchors):
            anchor_index = anchor_indices_sampler.next()
            reference = [key, anchor_index]
            genuine_indices = list(range(len(classes[key])))
            genuine_indices.pop(anchor_index)
            forgery_keys = random.sample(keys_except_c, min(forgery_classes_per_reference, len(keys_except_c)))

            for forgery_key in forgery_keys:
                forgery_class = classes[forgery_key]
                forgery_indices_sampler = MinimumRecurrenceRateRandomSamplerWithReplacement(
                    list(range(len(forgery_class))))
                forgeries = [[forgery_key, forgery_indices_sampler.next()] for _ in
                             range(min(forgeries_per_class, len(forgery_class)))]

                anchors.extend([reference] * len(forgeries))
                genuine_indices_sampler = MinimumRecurrenceRateRandomSamplerWithReplacement(genuine_indices)
                positives.extend([[key, genuine_indices_sampler.next()] for _ in range(len(forgeries))])
                negatives.extend(forgeries)

    return anchors, positives, negatives


def shuffle_triplets(triplets):
    anchors, positives, negatives = triplets
    indices = list(range(len(anchors)))
    random.shuffle(indices)

    shuffled_anchors = [anchors[indices[i]] for i in range(len(anchors))]
    shuffled_positives = [positives[indices[i]] for i in range(len(anchors))]
    shuffled_true_negatives = [negatives[indices[i]] for i in range(len(anchors))]

    return shuffled_anchors, shuffled_positives, shuffled_true_negatives


class DataGenerator(Sequence):
    def __init__(self, image_features, image_classes, triplets, batch_size, count):
        self.batch_size = batch_size
        self.count = count
        self.image_features = image_features
        self.image_classes = image_classes
        self.anchors, self.positives, self.negatives = shuffle_triplets(triplets)
        self.true_output = np.array(
            [[0] for _ in range(batch_size)])  # np.array([[1, 0, 0, 1] for _ in range(batch_size)], np.float32)

    def __len__(self):
        return math.ceil(self.count / self.batch_size)

    def __getitem__(self, index):
        anchors, positives, negatives = [], [], []
        for i in range(index * self.batch_size, min((index + 1) * self.batch_size, len(self.anchors))):
            anchor_pair = self.anchors[i]
            positive_pair = self.positives[i]
            negative_pair = self.negatives[i]
            anchors.append(self.image_features[self.image_classes[anchor_pair[0]][anchor_pair[1]]])
            positives.append(self.image_features[self.image_classes[positive_pair[0]][positive_pair[1]]])
            negatives.append(self.image_features[self.image_classes[negative_pair[0]][negative_pair[1]]])

        if len(anchors) == 0:
            return self.__getitem__(0)

        true_output = self.true_output
        if len(anchors) != self.batch_size:
            true_output = np.array([[0] for _ in range(len(anchors))])

        return [np.array(anchors),
                np.array(positives),
                np.array(negatives)], true_output

    def on_epoch_end(self):
        self.anchors, self.positives, self.negatives = shuffle_triplets([self.anchors, self.positives, self.negatives])


class ModelLoadingMode(Enum):
    MinimumMetricValue = 0
    MaximumMetricValue = 1


def rename_best_model(model_name, mode=ModelLoadingMode.MaximumMetricValue):
    all_models = list(filter(lambda x: x[-3:] == '.h5' and x.contains(model_name), os.listdir(os.curdir)))
    all_metrics = []

    for model in all_models:
        i1 = model.rfind('_')
        i2 = model.rfind('.')

        if i1 == -1 or i2 == -1:
            all_metrics.append(None)
            continue

        metric = model[i1 + 1: i2]

        try:
            all_metrics.append(float(metric))
        except ValueError:
            all_metrics.append(None)

    model_metric_pairs = [[all_models[i], all_metrics[i]] for i in range(len(all_models)) if all_metrics[i] is not None]
    if mode == ModelLoadingMode.MaximumMetricValue:
        best_index = np.array(model_metric_pairs)[:, 1:2].flatten().argmax()
    else:
        best_index = np.array(model_metric_pairs)[:, 1:2].flatten().argmin()

    shutil.copy(model_metric_pairs[best_index][0], model_name + '.h5')


# append lines to the report
def append_lines_to_report(lines):
    with open(EVALUATION_REPORT_FILE, 'a') as f:
        f.writelines(lines)


def train(image_features,
          train_image_classes,
          train_triplets,
          validation_image_classes,
          validation_triplets):
    if os.path.isfile(EMBEDDING_NET_FILE_NAME + '.h5'):
        embedding_net = load_model(EMBEDDING_NET_FILE_NAME + '.h5')
    else:
        embedding_net = embedding_network()
    embedding_net.summary()

    if os.path.isfile(SIAMESE_NET_FILE_NAME + '.h5'):
        siamese_net = load_model(SIAMESE_NET_FILE_NAME + '.h5')
    else:
        siamese_net = siamese_network()
    siamese_net.summary()

    model = training_wrapped_model(features_shape=DENSE_NET_FEATURES_SHAPE,
                                   embedding_net=embedding_net,
                                   siamese_net=siamese_net)
    model.summary()

    model.compile(loss=custom_triplet_loss,
                  optimizer=SGD(lr=INITIAL_LEARNING_RATE,
                                decay=0.0,
                                momentum=0.9,
                                nesterov=True),
                  metrics=[accuracy])

    train_data_generator = DataGenerator(image_features,
                                         train_image_classes,
                                         train_triplets,
                                         TRAINING_BATCH_SIZE,
                                         len(train_triplets[0]))

    validation_data_generator = DataGenerator(image_features,
                                              validation_image_classes,
                                              validation_triplets,
                                              EVALUATION_BATCH_SIZE,
                                              len(validation_triplets[0]))

    # en_last_model_checkpoint = AltModelCheckpoint(EMBEDDING_NET_FILE_NAME + '_checkpoint_last.h5',
    #                                               embedding_net)
    # sn_last_model_checkpoint = AltModelCheckpoint(SIAMESE_NET_FILE_NAME + '_checkpoint_last.h5',
    #                                               siamese_net)
    #
    # en_best_model_checkpoint = AltModelCheckpoint(
    #     EMBEDDING_NET_FILE_NAME + '_checkpoint_{epoch:02d}_{val_accuracy:0.4f}.h5',
    #     embedding_net,
    #     monitor='val_accuracy',
    #     save_best_only=True,
    #     mode='max')
    # sn_best_model_checkpoint = AltModelCheckpoint(
    #     SIAMESE_NET_FILE_NAME + '_checkpoint_{epoch:02d}_{val_accuracy:0.4f}.h5',
    #     siamese_net,
    #     monitor='val_accuracy',
    #     save_best_only=True,
    #     mode='max')

    append_lines_to_report('======= training results =======\n')

    update_report_callback = LambdaCallback(on_epoch_end=lambda epoch, logs: append_lines_to_report(
        f'epoch: {epoch} - loss: {logs["loss"]} - accuracy: {logs["accuracy"]} - validation loss: {logs["val_loss"]} - validation accuracy: {logs["val_accuracy"]}\n'
    ))

    model.fit(train_data_generator,
              steps_per_epoch=math.ceil(len(train_triplets[0]) / TRAINING_BATCH_SIZE),
              validation_data=validation_data_generator,
              validation_steps=math.ceil(len(validation_triplets[0]) / EVALUATION_BATCH_SIZE),
              epochs=EPOCHS,
              callbacks=[
                  # en_last_model_checkpoint,
                  #        en_best_model_checkpoint,
                  #        sn_last_model_checkpoint,
                  #        sn_best_model_checkpoint,
                         update_report_callback])

    rename_best_model(EMBEDDING_NET_FILE_NAME)
    rename_best_model(SIAMESE_NET_FILE_NAME)


def run_pipeline():
    # ----------- extract feature vectors into the features directory ------------ #
    if not os.path.isfile(IMAGE_FEATURES_FILE):
        image_features = load_image_features()
        cache_object(image_features, IMAGE_FEATURES_FILE)
        del image_features

    if not os.path.isfile(TRAIN_TRIPLETS_FILE) or \
            not os.path.isfile(VALIDATION_TRIPLETS_FILE) or \
            not os.path.isfile(TEST_TRIPLETS_FILE):
        image_features = load_cached(IMAGE_FEATURES_FILE)
        image_classes = extract_classes(image_features.keys())
        cache_object(image_classes, IMAGE_CLASSES_FILE)

        train_image_classes, validation_image_classes, test_image_classes = class_train_validation_test_split(
            image_classes)

        cache_object(train_image_classes, TRAIN_IMAGE_CLASSES_FILE)
        cache_object(validation_image_classes, VALIDATION_IMAGE_CLASSES_FILE)
        cache_object(test_image_classes, TEST_IMAGE_CLASSES_FILE)

        train_triplets = generate_triplets(train_image_classes)
        validation_triplets = generate_triplets(validation_image_classes)
        test_triplets = generate_triplets(test_image_classes)

        del train_image_classes
        del validation_image_classes
        del test_image_classes

        cache_object(train_triplets, TRAIN_TRIPLETS_FILE)
        cache_object(validation_triplets, VALIDATION_TRIPLETS_FILE)
        cache_object(test_triplets, TEST_TRIPLETS_FILE)

        del image_features
        del image_classes
        del train_triplets
        del validation_triplets
        del test_triplets

    # ------------------------------- train model -------------------------------- #
    if not os.path.isfile(os.path.join(os.path.curdir, MODEL_FILE_NAME + '.h5')):
        image_features = load_cached(IMAGE_FEATURES_FILE)
        train_triplets = load_cached(TRAIN_TRIPLETS_FILE)
        validation_triplets = load_cached(VALIDATION_TRIPLETS_FILE)

        train_image_classes = load_cached(TRAIN_IMAGE_CLASSES_FILE)
        validation_image_classes = load_cached(VALIDATION_IMAGE_CLASSES_FILE)

        train(image_features,
              train_image_classes,
              train_triplets,
              validation_image_classes,
              validation_triplets)

        del image_features
        del train_triplets
        del validation_triplets

    # # ---------------- evaluate model on the test feature vectors ---------------- #
    # test_image_classes = load_cached(TEST_IMAGE_CLASSES_FILE)
    #
    # evaluate_model(load_cached(IMAGE_FEATURES_FILE),
    #                test_image_classes,
    #                load_cached(TEST_TRIPLETS_FILE))


if __name__ == '__main__':
    run_pipeline()
