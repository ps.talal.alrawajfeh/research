import os
import random

import cv2
import numpy as np
from matplotlib import pyplot as plt
from tqdm import tqdm

from emnist_data_loader import load_classes_combined
from generation_utils import random_sign, resize, crop_image, random_within_range, random_bool, print_text

# -----------------------------------------------------------------------------

LAST_NAMES_FILE = './data/us_last_names.txt'
FIRST_NAMES_FILE = './data/us_first_names.txt'
OUTPUT_DIRECTORY = './data/new_signatures'

HANDWRITTEN_FONTS_PATH = './data/handwritten_fonts'
NORMAL_FONTS_PATH = './data/fonts'

NUMBER_OF_CLASSES = 9330
EMNIST_DATASET_PERCENTAGE = 0.1
HANDWRITTEN_FONTS_PERCENTAGE = 0.1
NORMAL_FONTS_PERCENTAGE = 0.1

WIDTH_THRESHOLD = 450
HEIGHT_THRESHOLD = 150
SIGNATURES_PER_CLASS = 3
CLASS_NAME_LENGTH = 9

CHAR_MIN_SHIFT_UP = 0
CHAR_MAX_SHIFT_UP = 6
CHAR_MIN_SHIFT_X = 0
CHAR_MAX_SHIFT_X = 6
SPACE_MIN_SHIFT_X = 12
SPACE_MAX_SHIFT_X = 32
MIN_SHRINK_PERCENTAGE = 0.4
MAX_SHRINK_PERCENTAGE = 1
MIN_ENLARGE_PERCENTAGE = 1
MAX_ENLARGE_PERCENTAGE = 1
MIN_FONT_SIZE = 30
MAX_FONT_SIZE = 50


# -----------------------------------------------------------------------------

def view_image(image, title=''):
    plt.imshow(image, plt.cm.gray)
    plt.title(title)
    plt.show()


def generate_sequence_coordinates(image_label_pairs):
    sequence_length = len(image_label_pairs)

    x_offsets = []
    y_offsets = []

    sequence_width = 0

    min_y1 = 999999
    max_y2 = 0

    previous_label = None
    for image, label in image_label_pairs:
        if label == ' ':
            previous_label = ' '
            x_offsets.append(None)
            y_offsets.append(None)
            continue

        digit_height = image.shape[0]
        digit_width = image.shape[1]

        y_offset = -digit_height

        shift_sign = random_sign()
        if shift_sign == -1:
            y_offset -= random.randint(CHAR_MIN_SHIFT_UP,
                                       CHAR_MAX_SHIFT_UP)

        x_offset = random.randint(CHAR_MIN_SHIFT_X,
                                  CHAR_MAX_SHIFT_X)
        if previous_label == ' ':
            x_offset += random.randint(SPACE_MIN_SHIFT_X,
                                       SPACE_MAX_SHIFT_X)

        y_offsets.append(y_offset)
        x_offsets.append(x_offset)

        sequence_width += x_offset + digit_width

        if min_y1 > y_offset:
            min_y1 = y_offset

        y2 = y_offset + digit_height

        if max_y2 < y2:
            max_y2 = y2

        previous_label = label

    sequence_height = abs(max_y2 - min_y1)

    baseline_x = 0
    baseline_y = sequence_height

    sequence_label = ''

    coordinates = []

    digit_x = baseline_x
    for i in range(sequence_length):
        image, label = image_label_pairs[i]
        if label == ' ':
            sequence_label += ' '
            coordinates.append([None, None])
            continue

        digit_width = image.shape[1]

        digit_y = baseline_y + y_offsets[i]
        digit_x += x_offsets[i]

        if digit_y < 0:
            digit_y = 0

        if digit_x < 0:
            digit_x = 0

        sequence_label += str(label)
        coordinates.append([digit_x, digit_y])
        digit_x += digit_width

    if sequence_label[-1:] == ' ':
        coordinates = coordinates[:-1]

    return coordinates, (sequence_width, sequence_height)


def print_label(image_label_pairs):
    coordinates, (w, h) = generate_sequence_coordinates(image_label_pairs)
    background = np.ones((h, w))

    for i in range(len(image_label_pairs)):
        if i >= len(coordinates):
            break
        if coordinates[i][0] is None:
            continue

        coordinate = coordinates[i]
        image, label = image_label_pairs[i]

        image_height = image.shape[0]
        image_width = image.shape[1]

        if image_height + coordinate[1] >= h:
            image_height = h - coordinate[1]

        if image_height <= 0:
            continue

        image = image[:image_height]

        background[coordinate[1]:coordinate[1] + image_height, coordinate[0]: coordinate[0] + image_width] = image

    return background


def random_resize_factor():
    if random_bool():
        resize_factor = random_within_range(MIN_SHRINK_PERCENTAGE,
                                            MAX_SHRINK_PERCENTAGE)
    else:
        resize_factor = random_within_range(MIN_ENLARGE_PERCENTAGE,
                                            MAX_ENLARGE_PERCENTAGE)
    return resize_factor


def get_image_label_pairs(sequence_label, classes):
    resize_factor = random_resize_factor()
    image_label_pairs = []

    for char in sequence_label:
        if char == ' ':
            image_label_pairs.append([None, char])
            continue

        image = resize(crop_image(random.choice(classes[char])), resize_factor)
        image_label_pairs.append([image, char])

    return image_label_pairs


def load_first_names():
    with open(FIRST_NAMES_FILE) as f:
        return [n.strip().lower() for n in f.readlines()]


def load_last_names():
    with open(LAST_NAMES_FILE) as f:
        return [n.strip().lower() for n in f.readlines()]


def random_name(first_names, last_names):
    if random.choice([0, 1]) == 0:
        return random.choice(first_names) + ' ' + random.choice(last_names)
    return random.choice(first_names)


def generate_emnist_handwritten_signatures(letter_classes, first_names, last_names):
    name = random_name(first_names, last_names)
    sequence_pairs = get_image_label_pairs(name, letter_classes)
    image = print_label(sequence_pairs)

    while image.shape[1] > WIDTH_THRESHOLD or image.shape[0] > HEIGHT_THRESHOLD:
        sequence_pairs = get_image_label_pairs(name, letter_classes)
        image = print_label(sequence_pairs)

    return [255 - image] + [255 - print_label(sequence_pairs) for _ in range(SIGNATURES_PER_CLASS - 1)]


def generate_printed_text_signatures(font, first_names, last_names):
    name = random_name(first_names, last_names)
    image = print_text(name,
                       font,
                       random.randint(MIN_FONT_SIZE, MAX_FONT_SIZE)).astype(np.float)
    while image.shape[1] > WIDTH_THRESHOLD or image.shape[0] > HEIGHT_THRESHOLD:
        name = random_name(first_names, last_names)
        image = print_text(name,
                           font,
                           random.randint(MIN_FONT_SIZE, MAX_FONT_SIZE)).astype(np.float)
    return [255 - image] + [255 - print_text(name,
                                             font,
                                             random.randint(MIN_FONT_SIZE, MAX_FONT_SIZE)).astype(np.float)
                            for _ in range(SIGNATURES_PER_CLASS - 1)]


def class_string_from_id(class_id):
    return '0' * (CLASS_NAME_LENGTH - len(str(class_id))) + str(class_id)


def generate_name_signatures():
    if not os.path.isdir(OUTPUT_DIRECTORY):
        os.makedirs(OUTPUT_DIRECTORY)

    classes = load_classes_combined()
    first_names = load_first_names()
    last_names = load_last_names()

    signature_id = 0
    number_of_emnist_signatures = int(NUMBER_OF_CLASSES * EMNIST_DATASET_PERCENTAGE)
    number_of_handwritten_font_signatures = int(NUMBER_OF_CLASSES * HANDWRITTEN_FONTS_PERCENTAGE)
    number_of_normal_font_signatures = int(NUMBER_OF_CLASSES * NORMAL_FONTS_PERCENTAGE)
    progress_bar = tqdm(total=number_of_emnist_signatures +
                              number_of_handwritten_font_signatures +
                              number_of_normal_font_signatures)

    for _ in range(number_of_emnist_signatures):
        i = 0
        for sig in generate_emnist_handwritten_signatures(classes, first_names, last_names):
            path = os.path.join(OUTPUT_DIRECTORY, class_string_from_id(signature_id) + f'_{i}.bmp')
            cv2.imwrite(path, sig)
            i += 1
        progress_bar.update()
        signature_id += 1

    hand_written_fonts = [os.path.join(HANDWRITTEN_FONTS_PATH, f) for f in os.listdir(HANDWRITTEN_FONTS_PATH)]
    for _ in range(number_of_handwritten_font_signatures):
        i = 0
        for sig in generate_printed_text_signatures(random.choice(hand_written_fonts), first_names, last_names):
            path = os.path.join(OUTPUT_DIRECTORY, class_string_from_id(signature_id) + f'_{i}.bmp')
            cv2.imwrite(path, sig)
            i += 1
        progress_bar.update()
        signature_id += 1

    normal_fonts = [os.path.join(NORMAL_FONTS_PATH, f) for f in os.listdir(NORMAL_FONTS_PATH)]
    for _ in range(number_of_normal_font_signatures):
        i = 0
        for sig in generate_printed_text_signatures(random.choice(normal_fonts), first_names, last_names):
            path = os.path.join(OUTPUT_DIRECTORY, class_string_from_id(signature_id) + f'_{i}.bmp')
            cv2.imwrite(path, sig)
            i += 1
        progress_bar.update()
        signature_id += 1

    progress_bar.close()


if __name__ == '__main__':
    generate_name_signatures()
