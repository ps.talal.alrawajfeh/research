import math
import os
import pickle
import random

import numpy as np
import tensorflow as tf
from keras.engine.saving import load_model
from keras.utils import Sequence
from numpy import random as np_random
from scipy import interpolate
from tqdm import tqdm

# -----------------------------------------------------------------------------

DATA_PATH = '.\\data\\generated'

MODEL_FILE_NAME = 'siamese'
EVALUATION_REPORT_FILE = 'siamese-report.txt'

IMAGE_FEATURES_FILE = 'image_features.pickle'
IMAGE_CLASSES_FILE = 'image_classes.pickle'
PAIRS_FILE = 'pairs.pickle'

REFERENCES_PER_CLASS = 2
FORGERY_CLASSES_PER_REFERENCE = 5
FORGERIES_PER_FORGERY_CLASS = 2

BATCH_SIZE = 1024

FORGERY_LABEL = [0, 1]
GENUINE_LABEL = [1, 0]

PIVOT_DISTANCE = 1.0
FORGERY_MOST_PROBABLE_DISTANCE = 1.4
GENUINE_MOST_PROBABLE_DISTANCE = 0.65

# ------------- fix the random seed to get reproducible results -------------- #

RANDOM_SEED = 666

random.seed(RANDOM_SEED)
np_random.seed(RANDOM_SEED)
tf.random.set_seed(RANDOM_SEED)


# ---------------------------------------------------------------------------- #

# serialize python object into a file
def cache_object(obj, file):
    with open(file, 'wb') as f:
        pickle.dump(obj, f)


# deserialize python object from a file
def load_cached(file):
    with open(file, 'rb') as f:
        return pickle.load(f)


def parse_feature_files(feature_files):
    image_features_dict = dict()
    for features_file in feature_files:
        with open(features_file, 'r') as f:
            line = f.readline().strip()
            while line != '':
                image, features = line.split('\t')
                image_features_dict[image.strip()] = [float(x) for x in features.split(',')]
                line = f.readline()
    return image_features_dict


# returns a dictionary of the classes of items in `array` according to `class_lambda`
def extract_classes(array, class_lambda=lambda f: f[0:9]):
    classes = dict()
    for item in array:
        c = class_lambda(item)
        if c not in classes:
            classes[c] = []
        classes[c].append(item)
    return classes


def extract_feature_classes(image_features, image_classes):
    class_features_dicts = dict()
    for c in image_classes:
        images = image_classes[c]
        class_features_dicts[c] = [image_features[image] for image in images]
    return class_features_dicts


def generate_pairs(classes):
    keys = [*classes]
    number_of_classes = len(keys)

    references = []
    others = []
    true_outputs = []

    for c in range(number_of_classes - FORGERY_CLASSES_PER_REFERENCE):
        c = random.randint(0, len(keys) - 1)
        key = keys.pop(c)

        reference_indices = random.sample(list(range(len(classes[key]))),
                                          min(REFERENCES_PER_CLASS, len(classes[key])))

        for r in reference_indices:
            reference = [key, r]

            genuines = [[key, i] for i in range(len(classes[key]))]
            references.extend([reference] * len(genuines))
            others.extend(genuines)
            true_outputs.extend([GENUINE_LABEL] * len(genuines))

            forgery_keys = random.sample(keys, min(FORGERY_CLASSES_PER_REFERENCE, len(keys)))
            for forgery_key in forgery_keys:
                forgery_class = classes[forgery_key]
                forgery_indices = random.sample(list(range(len(forgery_class))),
                                                min(FORGERIES_PER_FORGERY_CLASS, len(forgery_class)))

                forgeries = [[forgery_key, f] for f in forgery_indices]
                references.extend([reference] * len(forgeries))
                others.extend(forgeries)
                true_outputs.extend([FORGERY_LABEL] * len(forgeries))

    return references, others, true_outputs


def load_siamese_model():
    return load_model('siamese.h5', compile=False)


def load_triplet_model():
    return load_model('embedding-net.h5', compile=False)


class DataGenerator(Sequence):
    def __init__(self, image_features, image_classes, pairs, batch_size, count):
        self.batch_size = batch_size
        self.count = count
        self.image_features = image_features
        self.image_classes = image_classes
        self.references, self.others, self.true_outputs = pairs

    def __len__(self):
        return math.ceil(self.count / self.batch_size)

    def __getitem__(self, index):
        references, others = [], []
        for i in range(index * self.batch_size, min((index + 1) * self.batch_size, len(self.references))):
            reference_pair = self.references[i]
            other_pair = self.others[i]
            references.append(self.image_features[self.image_classes[reference_pair[0]][reference_pair[1]]])
            others.append(self.image_features[self.image_classes[other_pair[0]][other_pair[1]]])

        if len(references) == 0:
            return self.__getitem__(0)

        true_outputs = self.true_outputs[index * self.batch_size:(index + 1) * self.batch_size]
        return [np.array(references), np.array(others)], np.array(true_outputs)


def euclidean_distance(image1_features_embeddings, image2_features_embeddings):
    return np.sqrt(np.sum(np.square(image1_features_embeddings - image2_features_embeddings)))


def generate_outputs(image_features,
                     image_classes,
                     pairs):
    siamese_model = load_siamese_model()
    triplet_model = load_triplet_model()
    triplet_mapper = interpolate.interp1d(
        [0.0, GENUINE_MOST_PROBABLE_DISTANCE, PIVOT_DISTANCE, FORGERY_MOST_PROBABLE_DISTANCE, 2.0],
        [1.0, 0.9, 0.5, 0.1, 0.0],
        kind='linear')

    siamese_mapper = interpolate.interp1d([0.0, 0.8, 1.0], [0.0, 0.6, 1.0], kind='linear')

    test_data_generator = DataGenerator(image_features,
                                        image_classes,
                                        pairs,
                                        BATCH_SIZE,
                                        len(pairs[0]))

    batches = len(test_data_generator)
    progress_bar = tqdm(total=batches)
    all_predictions = []
    all_truths = []
    for i in range(batches):
        [references, others], true_outputs = test_data_generator[i]
        siamese_predicted_outputs = siamese_model.predict([references, others])
        references_embeddings = triplet_model.predict(references)
        others_embeddings = triplet_model.predict(others)

        y_true = true_outputs[:, 0]
        all_truths.extend(y_true)

        siamese_predictions = siamese_mapper(siamese_predicted_outputs[:, 0])
        triplet_predictions = np.array([triplet_mapper(euclidean_distance(r, o))
                               for r, o in zip(references_embeddings, others_embeddings)])

        all_predictions.extend(siamese_predictions * 1.0 + triplet_predictions * 0.0)

        progress_bar.update()
    progress_bar.close()

    all_truths = np.array(all_truths, np.float)
    all_predictions = np.array(all_predictions)

    for i in range(5, 100, 5):
        expected = np.array(all_predictions >= (i / 100), np.float)
        far_mask = (all_truths < 0.5)
        far = np.count_nonzero(expected[far_mask] > 0.5) / np.count_nonzero(far_mask)
        frr_mask = (all_truths > 0.5)
        frr = np.count_nonzero(expected[frr_mask] < 0.5) / np.count_nonzero(frr_mask)

        accuracy = np.count_nonzero(np.array(expected > 0.5, np.uint8) == np.array(all_truths > 0.5, np.uint8)) / len(expected)

        print(f'{i}%\t{far * 100}%\t{frr * 100}%\t{accuracy * 100}%')


def load_image_features():
    images = os.listdir(DATA_PATH)
    image_features = dict()

    for image in images:
        image_features[image] = load_cached(os.path.join(DATA_PATH, image))

    return image_features


def print_statistics(image_classes):
    total = 0
    for c in image_classes:
        total += len(image_classes[c])

    print(f'total number of images: {total}')
    print(f'total number of classes: {len(image_classes)}')
    print(f'average number of images per class: {int(total / len(image_classes))}')


def prepare_data():
    image_features = load_cached(IMAGE_FEATURES_FILE)
    image_classes = extract_classes(image_features.keys())
    cache_object(image_classes, IMAGE_CLASSES_FILE)

    print_statistics(image_classes)

    pairs = generate_pairs(image_classes)
    cache_object(pairs, PAIRS_FILE)

    del image_features
    del image_classes
    del pairs


def run_pipeline():
    # ----------- extract feature vectors into the features directory ------------ #
    if not os.path.isfile(IMAGE_FEATURES_FILE):
        image_features = load_image_features()
        cache_object(image_features, IMAGE_FEATURES_FILE)
        del image_features

    if not os.path.isfile(PAIRS_FILE):
        prepare_data()

    # ---------------- evaluate model on the test feature vectors ---------------- #
    generate_outputs(load_cached(IMAGE_FEATURES_FILE),
                     load_cached(IMAGE_CLASSES_FILE),
                     load_cached(PAIRS_FILE))


if __name__ == '__main__':
    run_pipeline()
