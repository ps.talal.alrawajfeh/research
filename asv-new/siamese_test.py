import math
import os
import pickle
import random
import shutil
from enum import Enum

import keras.backend as K
import numpy as np
import tensorflow as tf
from alt_model_checkpoint.keras import AltModelCheckpoint
from keras import Input, Model
from keras.callbacks import LearningRateScheduler, LambdaCallback
from keras.engine.saving import load_model
from keras.layers import BatchNormalization, Dense, Dropout, PReLU, Concatenate
from keras.optimizers import SGD
from keras.utils import Sequence
from numpy import random as np_random
from scipy.interpolate import PchipInterpolator
from sklearn.metrics import accuracy_score, confusion_matrix, classification_report
from tqdm import tqdm

# ----------------------------- configurations ----------------------------- #


FEATURE_FILES = ["C:\\fadi\\final\\trainingSignaturesWithNoises.tsv",
                 "C:\\fadi\\final\\trainingSignaturesWithNoisesCABHBTF.tsv"]

PATH1 = "C:\\fadi\\trainingSignaturesWithNoises"
PATH2 = "C:\\fadi\\trainingSignaturesWithNoisesCABHBTF"

MODEL_FILE_NAME = 'siamese'
EVALUATION_REPORT_FILE = 'siamese-report.txt'

IMAGE_FEATURES_FILE = 'image_features.pickle'
IMAGE_CLASSES_FILE = 'image_classes.pickle'

TRAIN_PAIRS_FILE = 'train_pairs.pickle'
VALIDATION_PAIRS_FILE = 'validation_pairs.pickle'
TEST_PAIRS_FILE = 'test_pairs.pickle'

TRAIN_IMAGE_CLASSES_FILE = 'train_image_classes.pickle'
VALIDATION_IMAGE_CLASSES_FILE = 'validation_image_classes.pickle'
TEST_IMAGE_CLASSES_FILE = 'test_image_classes.pickle'


# ---------------------------- hyper-parameters ---------------------------- #

class TrainValidationTestSplitMode(Enum):
    ClassSplit = 1
    PairsSplit = 2


TRAIN_VALIDATION_TEST_SPLIT_MODE = TrainValidationTestSplitMode.ClassSplit

TRAIN_DATA_PERCENTAGE = 0.7
VALIDATION_DATA_PERCENTAGE = 0.15
TEST_DATA_PERCENTAGE = 0.15

REFERENCES_PER_CLASS = 10
FORGERY_CLASSES_PER_REFERENCE = 20
FORGERIES_PER_FORGERY_CLASS = 10

DENSE_NET_FEATURES_SHAPE = (1920,)
HIDDEN_LAYER_SIZE = 1024
EMBEDDING_LAYER_SIZE = 1024

OUTPUT_LAYER_SIZE = 2
DROPOUT_RATE = 0.5

TRAINING_BATCH_SIZE = 256
EVALUATION_BATCH_SIZE = 1024
EPOCHS = 6

INITIAL_LEARNING_RATE = 0.1
DECAY_RATE = 0.1
LEARNING_RATE_EPSILON = 1e-2

FORGERY_LABEL = [0, 1]
GENUINE_LABEL = [1, 0]


def calculate_learning_rates(epoch_learning_rate_mappings):
    epochs = [int(e) for e in epoch_learning_rate_mappings]
    learning_rates = [epoch_learning_rate_mappings[e] for e in epoch_learning_rate_mappings]
    interpolator = PchipInterpolator(np.array(epochs), np.array(learning_rates))

    x = np.arange(min(epochs), max(epochs) + 1)
    y = interpolator(x)

    epoch_learning_rate_map = dict()
    for i in range(len(x)):
        epoch_learning_rate_map[x[i]] = y[i]

    return epoch_learning_rate_map


# the best strategy for training the model according to experimentation seems to be starting with a learning rate of 0.1
# and training for 2-3 epochs and then decaying the learning rate by a factor of 0.1, i.e. obtaining a learning rate of
# 0.01 and training for another 2-3 epochs and repeating this pattern until there is no further improvement with the
# training/validation accuracy; this enables faster convergence with maintaining high accuracy.
LEARNING_RATE_DELTA = INITIAL_LEARNING_RATE - LEARNING_RATE_EPSILON
EPOCH_LEARNING_RATE_MAP = calculate_learning_rates({
    0: INITIAL_LEARNING_RATE,
    1: LEARNING_RATE_DELTA,

    2: INITIAL_LEARNING_RATE * DECAY_RATE,
    3: LEARNING_RATE_DELTA * DECAY_RATE,

    4: INITIAL_LEARNING_RATE * DECAY_RATE ** 2,
    5: LEARNING_RATE_DELTA * DECAY_RATE ** 2,

    6: INITIAL_LEARNING_RATE * DECAY_RATE ** 3
})

# ------------- fix the random seed to get reproducible results -------------- #

RANDOM_SEED = 666

random.seed(RANDOM_SEED)
np_random.seed(RANDOM_SEED)
tf.random.set_seed(RANDOM_SEED)


# ---------------------------------------------------------------------------- #

# serialize python object into a file
def cache_object(obj, file):
    with open(file, 'wb') as f:
        pickle.dump(obj, f)


# deserialize python object from a file
def load_cached(file):
    with open(file, 'rb') as f:
        return pickle.load(f)


def parse_feature_files(feature_files):
    image_features_dict = dict()
    for features_file in feature_files:
        with open(features_file, 'r') as f:
            line = f.readline().strip()
            while line != '':
                image, features = line.split('\t')
                image_features_dict[image.strip()] = [float(x) for x in features.split(',')]
                line = f.readline()
    return image_features_dict


# returns a dictionary of the classes of items in `array` according to `class_lambda`
def extract_classes(array, class_lambda=lambda f: f[0:9]):
    classes = dict()
    for item in array:
        c = class_lambda(item)
        if c not in classes:
            classes[c] = []
        classes[c].append(item)
    return classes


def extract_feature_classes(image_features, image_classes):
    class_features_dicts = dict()
    for c in image_classes:
        images = image_classes[c]
        class_features_dicts[c] = [image_features[image] for image in images]
    return class_features_dicts


class MinimumRecurrenceRateRandomSamplerWithReplacement:
    def __init__(self, array) -> None:
        self.array = array
        self.left_choices = array.copy()

    def next(self):
        if len(self.left_choices) == 0:
            self.left_choices = self.array.copy()
        i = random.randint(0, len(self.left_choices) - 1)
        return self.left_choices.pop(i)


def generate_pairs(classes):
    keys = [*classes]
    number_of_classes = len(keys)

    references = []
    others = []
    true_outputs = []

    for c in range(number_of_classes - FORGERY_CLASSES_PER_REFERENCE):
        c = random.randint(0, len(keys) - 1)
        key = keys.pop(c)

        reference_indices = random.sample(list(range(len(classes[key]))),
                                          min(REFERENCES_PER_CLASS, len(classes[key])))

        for r in reference_indices:
            reference = [key, r]

            genuines = [[key, i] for i in range(len(classes[key]))]
            references.extend([reference] * len(genuines))
            others.extend(genuines)
            true_outputs.extend([GENUINE_LABEL] * len(genuines))

            forgery_keys = random.sample(keys, min(FORGERY_CLASSES_PER_REFERENCE, len(keys)))
            for forgery_key in forgery_keys:
                forgery_class = classes[forgery_key]
                forgery_indices = random.sample(list(range(len(forgery_class))),
                                                min(FORGERIES_PER_FORGERY_CLASS, len(forgery_class)))

                forgeries = [[forgery_key, f] for f in forgery_indices]
                references.extend([reference] * len(forgeries))
                others.extend(forgeries)
                true_outputs.extend([FORGERY_LABEL] * len(forgeries))

    return references, others, true_outputs


# splits the classes into training, validation, and testing classes
def class_train_validation_test_split(image_classes):
    keys = [*image_classes]

    keys_indices = list(range(len(keys)))
    random.shuffle(keys_indices)

    train_keys_indices = keys_indices[:int(len(image_classes) * TRAIN_DATA_PERCENTAGE)]
    train_image_classes = dict()
    for i in range(len(train_keys_indices)):
        key = keys[train_keys_indices[i]]
        train_image_classes[key] = image_classes[key]
    keys_indices = keys_indices[len(train_keys_indices):]

    validation_keys_indices = keys_indices[:int(len(image_classes) * VALIDATION_DATA_PERCENTAGE)]
    validation_image_classes = dict()
    for i in range(len(validation_keys_indices)):
        key = keys[validation_keys_indices[i]]
        validation_image_classes[key] = image_classes[key]
    keys_indices = keys_indices[len(validation_keys_indices):]

    test_image_classes = dict()
    for i in range(len(keys_indices)):
        key = keys[keys_indices[i]]
        test_image_classes[key] = image_classes[key]

    return train_image_classes, validation_image_classes, test_image_classes


# splits the pairs into training, validation, and testing pairs
def pairs_train_validation_test_split(pairs):
    references, others, true_outputs = pairs
    indices = list(range(len(references)))
    random.shuffle(indices)

    train_data_length = int(math.ceil(len(references) * TRAIN_DATA_PERCENTAGE))
    train_indices = indices[0:train_data_length]
    train_pairs = [[references[i] for i in train_indices],
                   [others[i] for i in train_indices],
                   [true_outputs[i] for i in train_indices]]

    indices = indices[train_data_length:]
    validation_data_length = int(math.ceil(len(references) * VALIDATION_DATA_PERCENTAGE))
    validation_indices = indices[0:validation_data_length]
    validation_pairs = [[references[i] for i in validation_indices],
                        [others[i] for i in validation_indices],
                        [true_outputs[i] for i in validation_indices]]

    indices = indices[validation_data_length:]
    test_pairs = [[references[i] for i in indices],
                  [others[i] for i in indices],
                  [true_outputs[i] for i in indices]]

    return train_pairs, validation_pairs, test_pairs


# append lines to the report
def append_lines_to_report(lines):
    with open(EVALUATION_REPORT_FILE, 'a') as f:
        f.writelines(lines)


def shuffle_pairs(pairs):
    references, others, true_outputs = pairs
    indices = list(range(len(references)))
    random.shuffle(indices)

    shuffled_references = [references[indices[i]] for i in range(len(references))]
    shuffled_others = [others[indices[i]] for i in range(len(references))]
    shuffled_true_outputs = [true_outputs[indices[i]] for i in range(len(references))]

    return shuffled_references, shuffled_others, shuffled_true_outputs


def build_siamese_model(features_shape):
    if os.path.isfile(os.path.join(os.path.curdir, MODEL_FILE_NAME + '.h5')):
        return load_model(MODEL_FILE_NAME + '.h5', compile=False)

    input1 = Input(shape=features_shape)
    input2 = Input(shape=features_shape)

    merged = Concatenate(axis=-1)([input1, input2])
    fc1 = BatchNormalization(momentum=0.9)(merged)

    fc1 = Dense(HIDDEN_LAYER_SIZE,
                use_bias=False)(fc1)
    fc1 = BatchNormalization(momentum=0.9)(fc1)
    fc1 = PReLU()(fc1)
    fc1 = Dropout(DROPOUT_RATE)(fc1)

    fc2 = Dense(EMBEDDING_LAYER_SIZE,
                use_bias=False)(fc1)
    fc2 = BatchNormalization(momentum=0.9)(fc2)
    fc2 = PReLU()(fc2)
    fc2 = Dropout(DROPOUT_RATE)(fc2)

    prediction = Dense(OUTPUT_LAYER_SIZE,
                       activation='sigmoid',
                       use_bias=False)(fc2)

    return Model(inputs=[input1, input2], outputs=prediction)


def hard_binary_cross_entropy_loss(y_true, y_pred):
    losses = K.binary_crossentropy(y_true, y_pred)
    return K.sum(losses, -1) / (
            K.sum(K.cast(losses > K.constant(0.0), 'float'), -1) + K.epsilon())


def configure_model_attributes(model):
    model.loss = None
    model._compile_metrics = None
    model._compile_weighted_metrics = None
    model.sample_weight_mode = None
    model.loss_weights = None


class DataGenerator(Sequence):
    def __init__(self, image_features, image_classes, pairs, batch_size, count):
        self.batch_size = batch_size
        self.count = count
        self.image_features = image_features
        self.image_classes = image_classes
        self.references, self.others, self.true_outputs = shuffle_pairs(pairs)

    def __len__(self):
        return math.ceil(self.count / self.batch_size)

    def __getitem__(self, index):
        references, others = [], []
        for i in range(index * self.batch_size, min((index + 1) * self.batch_size, len(self.references))):
            reference_pair = self.references[i]
            other_pair = self.others[i]
            references.append(self.image_features[self.image_classes[reference_pair[0]][reference_pair[1]]])
            others.append(self.image_features[self.image_classes[other_pair[0]][other_pair[1]]])

        if len(references) == 0:
            return self.__getitem__(0)

        true_outputs = self.true_outputs[index * self.batch_size:(index + 1) * self.batch_size]
        return [np.array(references), np.array(others)], np.array(true_outputs)

    def on_epoch_end(self):
        self.references, self.others, self.true_outputs = shuffle_pairs([self.references,
                                                                         self.others,
                                                                         self.true_outputs])


class ModelLoadingMode(Enum):
    MinimumMetricValue = 0
    MaximumMetricValue = 1


def rename_best_model(mode=ModelLoadingMode.MaximumMetricValue):
    all_models = list(filter(lambda x: x[-3:] == '.h5', os.listdir(os.curdir)))
    all_metrics = []

    for model in all_models:
        i1 = model.rfind('_')
        i2 = model.rfind('.')

        if i1 == -1 or i2 == -1:
            all_metrics.append(None)
            continue

        metric = model[i1 + 1: i2]

        try:
            all_metrics.append(float(metric))
        except ValueError:
            all_metrics.append(None)

    model_metric_pairs = [[all_models[i], all_metrics[i]] for i in range(len(all_models)) if all_metrics[i] is not None]
    if mode == ModelLoadingMode.MaximumMetricValue:
        best_index = np.array(model_metric_pairs)[:, 1:2].flatten().argmax()
    else:
        best_index = np.array(model_metric_pairs)[:, 1:2].flatten().argmin()

    shutil.copy(model_metric_pairs[best_index][0], MODEL_FILE_NAME + '.h5')


def train(image_features,
          train_image_classes,
          train_pairs,
          validation_image_classes,
          validation_pairs):
    model = build_siamese_model(features_shape=DENSE_NET_FEATURES_SHAPE)
    configure_model_attributes(model)
    model.summary()

    model.compile(loss=hard_binary_cross_entropy_loss,
                  optimizer=SGD(lr=INITIAL_LEARNING_RATE,
                                decay=0.0,
                                momentum=0.9,
                                nesterov=True),
                  metrics=['binary_accuracy'])

    train_data_generator = DataGenerator(image_features,
                                         train_image_classes,
                                         train_pairs,
                                         TRAINING_BATCH_SIZE,
                                         len(train_pairs[0]))

    validation_data_generator = DataGenerator(image_features,
                                              validation_image_classes,
                                              validation_pairs,
                                              EVALUATION_BATCH_SIZE,
                                              len(validation_pairs[0]))

    last_model_checkpoint = AltModelCheckpoint(MODEL_FILE_NAME + '_checkpoint_last.h5',
                                               model)
    best_model_checkpoint = AltModelCheckpoint(
        MODEL_FILE_NAME + '_checkpoint_{epoch:02d}_{val_binary_accuracy:0.4f}.h5',
        model,
        monitor='val_binary_accuracy',
        save_best_only=True,
        mode='max')

    learning_rate_scheduler = LearningRateScheduler(lambda e: EPOCH_LEARNING_RATE_MAP[e], verbose=0)

    append_lines_to_report('======= training results =======\n')

    update_report_callback = LambdaCallback(on_epoch_end=lambda epoch, logs: append_lines_to_report(
        f'epoch: {epoch} - loss: {logs["loss"]} - binary accuracy: {logs["binary_accuracy"]} - validation loss: {logs["val_loss"]} - validation binary accuracy: {logs["val_binary_accuracy"]}\n'
    ))

    model.fit_generator(generator=train_data_generator,
                        steps_per_epoch=math.ceil(len(train_pairs[0]) / TRAINING_BATCH_SIZE),
                        validation_data=validation_data_generator,
                        validation_steps=math.ceil(len(validation_pairs[0]) / EVALUATION_BATCH_SIZE),
                        epochs=EPOCHS,
                        use_multiprocessing=False,
                        callbacks=[last_model_checkpoint,
                                   best_model_checkpoint,
                                   learning_rate_scheduler,
                                   update_report_callback])

    rename_best_model()


def evaluate_model(image_features,
                   test_classes,
                   test_pairs):
    model = build_siamese_model(features_shape=DENSE_NET_FEATURES_SHAPE)
    model.summary()

    model.compile(loss=hard_binary_cross_entropy_loss,
                  optimizer=SGD(lr=INITIAL_LEARNING_RATE,
                                decay=0.0,
                                momentum=0.9,
                                nesterov=True),
                  metrics=['binary_accuracy'])

    test_data_generator = DataGenerator(image_features,
                                        test_classes,
                                        test_pairs,
                                        EVALUATION_BATCH_SIZE,
                                        len(test_pairs[0]))

    append_lines_to_report('======= evaluation results =======\n')

    results = model.evaluate_generator(generator=test_data_generator,
                                       steps=math.ceil(len(test_pairs[0]) / EVALUATION_BATCH_SIZE),
                                       use_multiprocessing=False)

    append_lines_to_report(f'test loss: {results[0]} - test accuracy: {results[1]}\n\n')

    y_true, y_pred = [], []
    batches = len(test_data_generator)
    progress_bar = tqdm(total=batches)
    for i in range(batches):
        [references, others], true_outputs = test_data_generator[i]
        predicted_outputs = model.predict([references, others])
        y_true.extend(np.argmax(true_outputs, axis=1).tolist())
        y_pred.extend(np.argmax(predicted_outputs, axis=1).tolist())
        progress_bar.update()
    progress_bar.close()

    append_lines_to_report('\ntest accuracy: {:.3f}'.format(accuracy_score(y_true, y_pred)) + '\n')
    append_lines_to_report('\nconfusion matrix:\n')
    append_lines_to_report(f'{confusion_matrix(y_true, y_pred)}\n')
    append_lines_to_report('\nclassification report:\n')
    append_lines_to_report(f'{classification_report(y_true, y_pred)}\n')


def run_pipeline():
    # ----------- extract feature vectors into the features directory ------------ #
    if not os.path.isfile(IMAGE_FEATURES_FILE):
        image_features = parse_feature_files(FEATURE_FILES)
        cache_object(image_features, IMAGE_FEATURES_FILE)
        del image_features

    if not os.path.isfile(TEST_PAIRS_FILE):
        image_features = load_cached(IMAGE_FEATURES_FILE)
        image_classes = extract_classes(image_features.keys())
        cache_object(image_classes, IMAGE_CLASSES_FILE)

        test_pairs = generate_pairs(image_classes)
        cache_object(test_pairs, TEST_PAIRS_FILE)

        del image_features
        del image_classes
        del test_pairs

    evaluate_model(load_cached(IMAGE_FEATURES_FILE),
                   load_cached(IMAGE_CLASSES_FILE),
                   load_cached(TEST_PAIRS_FILE))


def find_mis_classifications(image_features,
                             test_classes,
                             test_pairs):
    model = build_siamese_model(features_shape=DENSE_NET_FEATURES_SHAPE)
    model.summary()

    model.compile(loss=hard_binary_cross_entropy_loss,
                  optimizer=SGD(lr=INITIAL_LEARNING_RATE,
                                decay=0.0,
                                momentum=0.9,
                                nesterov=True),
                  metrics=['binary_accuracy'])

    references, others, true_outputs = test_pairs

    if not os.path.isdir('mis_classifications'):
        os.makedirs('mis_classifications')

    mis_id = 0
    for i in range(len(references)):
        ref_c, ref_index = references[i]
        other_c, other_index = others[i]
        expected = true_outputs[i]

        output = model.predict([np.array(image_features[test_classes[ref_c][ref_index]]).reshape(1, 1920),
                                np.array(image_features[test_classes[other_c][other_index]]).reshape(1, 1920)])[0]
        if np.argmax(output) != np.argmax(expected):
            file1 = test_classes[ref_c][ref_index]
            file2 = test_classes[other_c][other_index]

            if os.path.isfile(os.path.join(PATH1, file1)):
                path1 = os.path.join(PATH1, file1)
            else:
                path1 = os.path.join(PATH2, file1)

            if os.path.isfile(os.path.join(PATH1, file2)):
                path2 = os.path.join(PATH1, file2)
            else:
                path2 = os.path.join(PATH2, file2)

            dir_path = os.path.join('mis_classifications', str(mis_id))

            if np.argmax(output) == 0:
                dir_path += '_FA'
            else:
                dir_path += '_FR'

            os.makedirs(dir_path)
            shutil.copy(path1, os.path.join(dir_path, file1))
            shutil.copy(path2, os.path.join(dir_path, file2))

            mis_id += 1


if __name__ == '__main__':
    run_pipeline()
    # find_mis_classifications(load_cached(IMAGE_FEATURES_FILE),
    #                          load_cached(IMAGE_CLASSES_FILE),
    #                          load_cached(TEST_PAIRS_FILE))
