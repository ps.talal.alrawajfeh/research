import mimetypes
import os
import random

import cv2
import numpy as np
from tqdm import tqdm

SIGNATURES_PATH = '/home/u764/Development/data/signatures'


# Returns all the extensions for a given mime type.
def get_extensions_for_type(general_type):
    for ext in mimetypes.types_map:
        if mimetypes.types_map[ext].split('/')[0] == general_type:
            yield ext.lower()


# Function decorator for caching outputs.
def memoize(function):
    memo = {}

    def wrapper(*args):
        if args in memo:
            return memo[args]
        else:
            rv = function(*args)
            memo[args] = rv
            return rv

    return wrapper


# Returns all image extensions, e.g. '.jpg'.
@memoize
def get_image_extensions():
    return tuple(get_extensions_for_type('image'))


def file_extension(path):
    return os.path.splitext(os.path.basename(path))[1].lower()


def is_image(path):
    return os.path.isfile(path) and file_extension(path) in get_image_extensions()


def to_int(number):
    return int(round(number))


def change_image_dpi(image, dpi):
    if dpi == 200:
        return image
    factor = dpi / 200
    return cv2.resize(image, (0, 0), fx=factor, fy=factor, interpolation=cv2.INTER_CUBIC)


def change_image_quality(image, quality):
    encoded = cv2.imencode('.jpg', image, [int(cv2.IMWRITE_JPEG_QUALITY), quality])[1]
    return cv2.imdecode(encoded, cv2.IMREAD_GRAYSCALE)


def random_coordinate(image):
    return random.randint(0, image.shape[0] - 1), random.randint(0, image.shape[1] - 1)


def speckle(image, fraction, color=0):
    coordinates = [random_coordinate(image)
                   for _ in range(to_int(image.size * fraction))]

    image_copy = np.array(image)
    for coord in coordinates:
        image_copy[coord[0], coord[1]] = color

    return image_copy


def remove_white_border_non_binary(image):
    binary_image = threshold_otsu(image)

    mask = (255 - binary_image) > 0

    height, width = binary_image.shape[0:2]
    mask1, mask2 = mask.any(0), mask.any(1)
    x1, x2 = mask1.argmax(), width - mask1[::-1].argmax()
    y1, y2 = mask2.argmax(), height - mask2[::-1].argmax()

    return image[y1:y2, x1:x2]


def rotate(image, angle):
    (h, w) = image.shape
    center_x, center_y = w / 2, h / 2

    rotation_matrix = cv2.getRotationMatrix2D((center_x, center_y),
                                              angle,
                                              1.0)
    cos = np.abs(rotation_matrix[0, 0])
    sin = np.abs(rotation_matrix[0, 1])

    new_w = h * sin + w * cos
    new_h = h * cos + w * sin

    rotation_matrix[0, 2] += new_w / 2 - center_x
    rotation_matrix[1, 2] += new_h / 2 - center_y

    return cv2.warpAffine(image,
                          rotation_matrix,
                          (to_int(new_w), to_int(new_h)),
                          borderValue=(255),
                          flags=cv2.INTER_CUBIC)


def threshold(image, value=128):
    image_copy = np.array(image)
    image_copy[image_copy >= value] = 255
    image_copy[image_copy < value] = 0
    return image_copy


def threshold_otsu(image):
    return cv2.threshold(image,
                         0,
                         255,
                         cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]


def remove_white_border(binary_image):
    mask = (255 - binary_image) > 0

    height, width = binary_image.shape[0:2]
    mask1, mask2 = mask.any(0), mask.any(1)
    x1, x2 = mask1.argmax(), width - mask1[::-1].argmax()
    y1, y2 = mask2.argmax(), height - mask2[::-1].argmax()

    return binary_image[y1:y2, x1:x2]


def read_image_grayscale(image_path):
    return cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)


def change_image_brightness(image, delta):
    result = image.astype(np.float32)
    result += delta
    result[result > 255] = 255
    result[result < 0] = 0
    return result.astype(np.uint8)


def change_image_contrast(image, alpha):
    result = image.astype(np.float32)
    result *= alpha
    result[result > 255] = 255
    result[result < 0] = 0
    return result.astype(np.uint8)


def blur_image(image, blur_type=0):
    if blur_type == 0:
        return cv2.blur(image, (3, 3))
    elif blur_type == 1:
        return cv2.blur(image, (5, 5))
    elif blur_type == 2:
        return cv2.medianBlur(image, 3)
    elif blur_type == 3:
        return cv2.medianBlur(image, 5)
    elif blur_type == 4:
        return cv2.GaussianBlur(image, (3, 3), 0)
    elif blur_type == 5:
        return cv2.GaussianBlur(image, (5, 5), 0)
    raise Exception('not implemented')


def draw_line(image, point1, point2, color):
    result = np.array(image)
    cv2.line(result, point1, point2, color=color, thickness=1)
    return result


def load_signatures():
    files = [os.path.join(SIGNATURES_PATH, f) for f in os.listdir(SIGNATURES_PATH)]
    return [read_image_grayscale(f) for f in files if is_image(f)]


def draw_transparent(larger_image, smaller_image, location, transparency_color=255):
    mask = (smaller_image == transparency_color).astype(np.float32)

    image1 = larger_image.astype(np.float32)
    image2 = smaller_image.astype(np.float32)

    x, y = location
    height = image2.shape[0]
    width = image2.shape[1]

    image1[y: y + height, x: x + width] = mask * image1[y: y + height, x: x + width] + (1.0 - mask) * image2

    image1[image1 > 255] = 255
    image1[image1 < 0] = 0
    return image1.astype(np.uint8)


def main():
    output_path = '/home/u764/Downloads/cheques'
    signature_area_width_100_dpi = 400
    signature_area_height_100_dpi = 200

    if not os.path.isdir(output_path):
        os.makedirs(output_path)

    template1 = cv2.imread('template1.png', cv2.IMREAD_GRAYSCALE)
    template2 = cv2.imread('template2.png', cv2.IMREAD_GRAYSCALE)
    signatures = load_signatures()

    progress_bar = tqdm(total=1000)
    for i in range(1, 1001):
        if random.choice([0, 1]) == 0:
            template = np.array(template1)
        else:
            template = np.array(template2)

        if random.choice([0, 1]) == 0:
            for _ in range(random.randint(1, 5)):
                y = random.randint(0, template.shape[0] - 2)
                template = draw_line(template, (0, y), (template.shape[1], y), random.randint(0, 50))

        dpi = random.choice([100, 200])

        signature = random.choice(signatures)
        max_height = signature_area_height_100_dpi * 2
        max_width = signature_area_width_100_dpi * 2
        if signature.shape[0] > max_height or \
                signature.shape[1] > max_width:
            resize_factor = max(signature.shape[0] / max_height, signature.shape[1] / max_width)
            signature = cv2.resize(signature,
                                   (0, 0),
                                   fx=1.0 / resize_factor,
                                   fy=1.0 / resize_factor,
                                   interpolation=cv2.INTER_CUBIC)

        if random.choice([0, 1]) == 0:
            template_delta = random.randint(-127, 30)
            template = change_image_brightness(template, template_delta)
            signature_delta = random.randint(-10, 127)
            signature = change_image_brightness(signature, signature_delta)
            transparent_color = 255 + signature_delta
            if transparent_color > 255:
                transparent_color = 255
        else:
            transparent_color = 255

        x = random.randint(template.shape[1] - max_width, template.shape[1] - signature.shape[1])
        y = random.randint(template.shape[0] - max_height, template.shape[0] - signature.shape[0])

        result = draw_transparent(template, signature, (x, y), transparent_color)

        if random.choice([0, 1]) == 0:
            result = speckle(result, 0.01, random.randint(0, 255))

        if random.choice([0, 1]) == 0:
            result = change_image_contrast(result, random.uniform(0.75, 1.25))

        if random.choice([0, 1]) == 0:
            result = blur_image(result, random.randint(0, 5))

        result = change_image_dpi(result, dpi)

        if random.choice([0, 1]) == 0:
            result = change_image_quality(result, random.randint(50, 95))

        if random.choice([0, 1]) == 0:
            result = rotate(result, random.randint(-3, 3))

        cv2.imwrite(os.path.join(output_path, f'cheque_{i}.png'), result)
        progress_bar.update()
    progress_bar.close()


if __name__ == '__main__':
    main()
